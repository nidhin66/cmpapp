CREATE DATABASE  IF NOT EXISTS `cpm-db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cpm-db`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: cpm-db
-- ------------------------------------------------------
-- Server version	5.5.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `checklistitem`
--

DROP TABLE IF EXISTS `checklistitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checklistitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isCompleted` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checklistitem`
--

LOCK TABLES `checklistitem` WRITE;
/*!40000 ALTER TABLE `checklistitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `checklistitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cost`
--

DROP TABLE IF EXISTS `cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cost` (
  `id` bigint(20) NOT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cost`
--

LOCK TABLES `cost` WRITE;
/*!40000 ALTER TABLE `cost` DISABLE KEYS */;
/*!40000 ALTER TABLE `cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_biw7tevwc07g3iutlbmkes0cm` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_person`
--

DROP TABLE IF EXISTS `department_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_person` (
  `Department_id` int(11) NOT NULL,
  `users_id` varchar(255) NOT NULL,
  KEY `FK_1tgmlvqq615uw0vpt9nc4ahri` (`users_id`),
  KEY `FK_tcmgeqbt0t3kywj1s68nb6sai` (`Department_id`),
  CONSTRAINT `FK_tcmgeqbt0t3kywj1s68nb6sai` FOREIGN KEY (`Department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_1tgmlvqq615uw0vpt9nc4ahri` FOREIGN KEY (`users_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_person`
--

LOCK TABLES `department_person` WRITE;
/*!40000 ALTER TABLE `department_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencyjobs`
--

DROP TABLE IF EXISTS `dependencyjobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencyjobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `endToEnd_id` bigint(20) DEFAULT NULL,
  `endToStart_id` bigint(20) DEFAULT NULL,
  `startToEnd_id` bigint(20) DEFAULT NULL,
  `startToStart_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_i4c1ps10ybl6tb0se1agegre8` (`endToEnd_id`),
  KEY `FK_5jb1g2tgks76497m84rlmlti1` (`endToStart_id`),
  KEY `FK_tjw11isgw9cvrn5n1hgvbq0kd` (`startToEnd_id`),
  KEY `FK_hd1no556rnuj488b0l3i03y6a` (`startToStart_id`),
  CONSTRAINT `FK_hd1no556rnuj488b0l3i03y6a` FOREIGN KEY (`startToStart_id`) REFERENCES `jobgroup` (`id`),
  CONSTRAINT `FK_5jb1g2tgks76497m84rlmlti1` FOREIGN KEY (`endToStart_id`) REFERENCES `jobgroup` (`id`),
  CONSTRAINT `FK_i4c1ps10ybl6tb0se1agegre8` FOREIGN KEY (`endToEnd_id`) REFERENCES `jobgroup` (`id`),
  CONSTRAINT `FK_tjw11isgw9cvrn5n1hgvbq0kd` FOREIGN KEY (`startToEnd_id`) REFERENCES `jobgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencyjobs`
--

LOCK TABLES `dependencyjobs` WRITE;
/*!40000 ALTER TABLE `dependencyjobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `dependencyjobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequences`
--

DROP TABLE IF EXISTS `hibernate_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequences`
--

LOCK TABLES `hibernate_sequences` WRITE;
/*!40000 ALTER TABLE `hibernate_sequences` DISABLE KEYS */;
INSERT INTO `hibernate_sequences` VALUES ('StandardHierarchyNode',2),('StandardJob',1);
/*!40000 ALTER TABLE `hibernate_sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hierarchylevel`
--

DROP TABLE IF EXISTS `hierarchylevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hierarchylevel` (
  `levelName` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`levelName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hierarchylevel`
--

LOCK TABLES `hierarchylevel` WRITE;
/*!40000 ALTER TABLE `hierarchylevel` DISABLE KEYS */;
INSERT INTO `hierarchylevel` VALUES ('Loop/Equipments',4),('Plant',0),('Sub system',3),('System',2),('Unit',1);
/*!40000 ALTER TABLE `hierarchylevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hierarchylevel_hierarchylevel`
--

DROP TABLE IF EXISTS `hierarchylevel_hierarchylevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hierarchylevel_hierarchylevel` (
  `HierarchyLevel_levelName` varchar(255) NOT NULL,
  `children_levelName` varchar(255) NOT NULL,
  UNIQUE KEY `UK_f4lfnkoyx68r2memilrtu45kr` (`children_levelName`),
  KEY `FK_cfgcm3aiou25yafi9ss3vrop8` (`HierarchyLevel_levelName`),
  CONSTRAINT `FK_cfgcm3aiou25yafi9ss3vrop8` FOREIGN KEY (`HierarchyLevel_levelName`) REFERENCES `hierarchylevel` (`levelName`),
  CONSTRAINT `FK_f4lfnkoyx68r2memilrtu45kr` FOREIGN KEY (`children_levelName`) REFERENCES `hierarchylevel` (`levelName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hierarchylevel_hierarchylevel`
--

LOCK TABLES `hierarchylevel_hierarchylevel` WRITE;
/*!40000 ALTER TABLE `hierarchylevel_hierarchylevel` DISABLE KEYS */;
INSERT INTO `hierarchylevel_hierarchylevel` VALUES ('Plant','Unit'),('Sub system','Loop/Equipments'),('System','Sub system'),('Unit','System');
/*!40000 ALTER TABLE `hierarchylevel_hierarchylevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hierarchynode`
--

DROP TABLE IF EXISTS `hierarchynode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hierarchynode` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parentId` bigint(20) NOT NULL,
  `hierarchyLevel_levelName` varchar(255) DEFAULT NULL,
  `delayStatus` int(11) DEFAULT NULL,
  `isInpunchList` bit(1) NOT NULL,
  `projectId` int(11) NOT NULL,
  `useStatus` int(11) DEFAULT NULL,
  `weightage` double NOT NULL,
  `workStatus` int(11) DEFAULT NULL,
  `jobGroup_id` bigint(20) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dnpngwoyox1dllxce21uh54y7` (`name`),
  KEY `FK_p1q5p8hagx4fx39hlf5s3tmde` (`jobGroup_id`),
  KEY `FK_mc4b41quvywrqidatgfvmk9o7` (`owner_id`),
  KEY `FK_gr20943qg6tcaj1iya1wrfmpv` (`hierarchyLevel_levelName`),
  CONSTRAINT `FK_gr20943qg6tcaj1iya1wrfmpv` FOREIGN KEY (`hierarchyLevel_levelName`) REFERENCES `hierarchylevel` (`levelName`),
  CONSTRAINT `FK_mc4b41quvywrqidatgfvmk9o7` FOREIGN KEY (`owner_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK_p1q5p8hagx4fx39hlf5s3tmde` FOREIGN KEY (`jobGroup_id`) REFERENCES `jobgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hierarchynode`
--

LOCK TABLES `hierarchynode` WRITE;
/*!40000 ALTER TABLE `hierarchynode` DISABLE KEYS */;
INSERT INTO `hierarchynode` VALUES (32768,'','Plant1',0,'Plant',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32769,'','Utilities',32768,'Unit',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32770,'','UB-12, UB-13',32769,'System',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32771,'','PLC',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32772,'','Soot blowing system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32773,'','Sampling system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32774,'','Blow down system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32775,'','HP dozing system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32776,'','Electricalsubstation and power',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32777,'','Fuel gas system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32778,'Natural gas KOD','IUS-LZ-T-103A',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32779,'Flare KOD condensate pump','IUS-LZ-P-101AA',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32780,'Flare KOD condensate pump','IUS-LZ-PM-101AAA',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32781,'Flare KOD condensate pump','IUS-LZ-PM-101B5',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32782,'HP Fuel gas KOD','IUS-LZ-T-103B',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32783,'Flare KOD condensate pump','IUS-LZ-P-101BBBB',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32784,'Flare KOD','IUS-LZ-T-102',32777,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32785,'','Amine dozing system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32786,'','Control power',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32787,'','Hydrazine dozing system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32788,'','Cooling water system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32789,'','Fire water system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32790,'','Boiler feed water system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32791,'UB boiler feed water pump motor','IUS-BFPU-PM101A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32792,'UB boiler feed water pump','IUS-BFPU-P101D',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32793,'HP Dozing pump motor- UB13','IUS-UB-13-PM-02A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32794,'Sample cooler- Feed Water-UB12','Sample cooler-1',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32795,'UB boiler feed water pump','IUS-BFPU-P101C',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32796,'CBD Tank','IUS-UB-13-T-01',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32797,'IBD tank','IUS-UB-12-T-02',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32798,'Sample cooler- Sat steam-UB12','Sample cooler-2',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32799,'HRSG Boiler feed water pump','IUS-BFPH-P101A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32800,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32801,'HP Dozing pump- UB13','IUS-UB-13-P-02A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32802,'HP Dozing pump- UB12','IUS-UB-12-P-02A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32803,'BOILER FEED WATER HEATER','IUS-LZ-E-106A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32804,'UB boiler feed water pump motor','IUS-BFPU-PM101B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32805,'HP dozing mixing tank agitator-UB13','TEMP12',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32806,'HP dozing mixing tank agitator-UB12','TEMP11',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32807,'UB boiler feed water pump','IUS-BFPU-P101A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32808,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101D',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32809,'HP Dozing pump motor- UB13','IUS-UB-13-PM-02B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32810,'De aerator-1','TEMP9',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32811,'De aerator storage tank-1','IUS-DEA-T-101A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32812,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32813,'Sample cooler- Blow down-UB12','Sample cooler-3',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32814,'HRSG Boiler feed water pump','IUS-BFPH-P101C',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32815,'BOILER FEED WATER HEATER','IUS-LZ-E-106B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32816,'HP Dozing system Metering Tank- UB13','IUS-UB-13-T03',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32817,'HP Dozing system Mixing Tank- UB13','IUS-UB-13-T04',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32818,'Vent condenser-1','IUS-DEA-E-101A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32819,'UB boiler feed water pump turbine','IUS-BFPU-PT101D',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32820,'HP Dozing system Mixing Tank- UB12','IUS-UB-12-T04',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32821,'HP dozing mixing tank agitator motor- UB13','IUS-UB-12-TM-04',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32822,'De aerator-2','TEMP10',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32823,'HRSG Boiler feed water pump','IUS-BFPH-P101D',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32824,'HP Dozing pump- UB13','IUS-UB-13-P-02B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32825,'Sample cooler- SH steam-UB12','Sample cooler-4',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32826,'HP Dozing system Metering Tank- UB12','IUS-UB-12-T03',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32827,'HP dozing mixing tank agitator motor- UB12','IUS-UB-13-TM-04',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32828,'HP Dozing pump motor- UB12','IUS-UB-12-PM-02B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32829,'UB boiler feed water pump','IUS-BFPU-P101B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32830,'UB boiler feed water pump turbine','IUS-BFPU-PT101C',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32831,'HP Dozing pump- UB12','IUS-UB-12-P-02B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32832,'CBD Tank','IUS-UB-12-T-01',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32833,'HP Dozing pump motor- UB12','IUS-UB-12-PM-02A',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32834,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101C',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32835,'De aerator storage tank-2','IUS-DEA-T-101B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32836,'IBD tank','IUS-UB-13-T-02',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32837,'vent condenser-2','IUS-DEA-E-101B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32838,'HRSG Boiler feed water pump','IUS-BFPH-P101B',32790,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32839,'','Fuel oil system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32840,'Fuel oil circulation pump','IUS-LZ-P-103A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32841,'Fuel oil circulation pump','IUS-LZ-P-103D',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32842,'UB-13 Burner-1','IUS--UB-13-FB-01A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32843,'Fuel oil circulation pump motor','IUS-LZ-PM-103A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32844,'FO CBD tank pump motor','IUS-LZ-PM-102B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32845,'UB-12 Burner-5','IUS--UB-12-FB-01E',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32846,'UB-13 Burner-5','IUS--UB-13-FB-01E',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32847,'FO CBD tank pump motor','IUS-LZ-PM-102A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32848,'FO Closed blow down tank','IUS-LZ-T-101A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32849,'FO CBD tank pump','IUS-LZ-P-102B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32850,'Fuel oil circulation pump','IUS-LZ-P-103C',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32851,'FO CBD tank pump','IUS-LZ-P-102C',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32852,'FO CBD tank pump motor','IUS-LZ-PM-102D',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32853,'FO CBD tank pump','IUS-LZ-P-102A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32854,'FO CBD tank pump','IUS-LZ-P-102D',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32855,'Fuel oil circulation pump turbine','IUS-LZ-PT-103B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32856,'UB-12 Burner-3','IUS--UB-12-FB-01C',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32857,'UB-13 Burner-3','IUS--UB-13-FB-01C',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32858,'Fuel oil circulation pump turbine','IUS-LZ-PT-103D',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32859,'FO CBD tank pump motor','IUS-LZ-PM-102C',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32860,'UB-12 Burner-2','IUS--UB-12-FB-01B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32861,'Fuel oil surge tank heating coil','IUS-LZ-TM-2401',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32862,'UB-12 Burner-4','IUS--UB-12-FB-01D',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32863,'Fuel oil circulation pump suction strainer','IUS-ST-2301',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32864,'UB-12 Burner-6','IUS--UB-12-FB-01F',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32865,'UB-13 Burner-2','IUS--UB-13-FB-01B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32866,'Fuel oil circulation pump suction strainer','IUS-ST-2302',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32867,'Fuel oil circulation pump','IUS-LZ-P-103B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32868,'UB-12 Burner-1','IUS--UB-12-FB-01A',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32869,'Fuel oil heater','IUS-E-2',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32870,'Fuel oil circulation pump motor','IUS-LZ-PM-103C',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32871,'FO Closed blow down tank','IUS-LZ-T-101B',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32872,'Fuel oil heater','IUS-E-1',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32873,'UB-13 Burner-4','IUS--UB-13-FB-01D',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32874,'UB-13 Burner-6','IUS--UB-13-FB-01F',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32875,'Fuel oil surge tank','IUS-LZ-T-101',32839,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32876,'','DCS system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32877,'','Gas analysers/ monitors',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32878,NULL,'CEMS system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32879,'','Inert gas system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32880,'','SWAS system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32881,'SWAS system for UB12','TEMP22',32880,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32882,'SWAS system for UB13','TEMP23',32880,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32883,'','Condensate system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32884,'Condensate transfer pump to MUH, motor','IUS-LZ-PM-104A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32885,'Amine Dozing System-2 mixing tank','IUS-DEA-T-106',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32886,'Amine Dozing system-2 Agitator motor','IUS-DEA-TM-106',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32887,'Amine Dozing system-2 dozing pump motor','IUS-DEA-PM-106A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32888,'Hydrazine Dozing System-2 mixing tank','IUS-DEA-T-104',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32889,'Hydrazine Dozing ystem-2 dozing pump','IUS-DEA-P-102A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32890,'Condensate tank-2','IUS-LZ-T-104B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32891,'Amine Dozing system-1 Metering Tank','IUS-DEA-T-103',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32892,'Condensate tank-1','IUS-LZ-T-104A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32893,'Suspect Condensate transfer Pump','IUS-LZ-P-101AAA',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32894,'Amine Dozing system-2 Metering Tank','IUS-DEA-T-107',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32895,'Amine Dozing system-1 Agitator','TEMP19',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32896,'Suspect Condensate transfer Pump Motor','IUS-LZ-PM-101B3',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32897,'Dump condenser condensate pump','IUS-LZ-P-101A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32898,'Suspect Condensate Tank','IUS-LZ-T-111',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32899,'LP steam dump condenser fan motor','IUS-LZ-EM01A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32900,'LP steam dump condenser fan','IUS-LZ-E01D',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32901,'LP steam dump condenser fan motor','IUS-LZ-EM01B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32902,'Atmospheric tank','IUS-LZ-T-112',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32903,'Condensate transfer pump to De aerator, motor','IUS-LZ-PM-105A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32904,'LP steam dump condenser fan','IUS-LZ-E01C',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32905,'LP steam dump condenser fan','IUS-LZ-E01B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32906,'Hydrazine Dozing system-2 dozing pump motor','IUS-DEA-PM-102B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32907,'Condensate transfer pump to De aerator','IUS-LZ-P-105B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32908,'Amine Dozing system-1 dozing pump motor','IUS-DEA-PM-101A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32909,'Amine Dozing System-1 mixing tank','IUS-DEA-T-102',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32910,'Amine Dozing system-2 dozing pump','IUS-DEA-P-106B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32911,'Hydrazine Dozing system-2 Metering Tank','IUS-DEA-T-105',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32912,'Suspect Condensate transfer Pump Motor','IUS-LZ-PM-101A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32913,'Amine Dozing system-1 dozing pump motor','IUS-DEA-PM-101B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32914,'Hydrazine Dozing system-2 dozing pump','IUS-DEA-P-102B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32915,'LP steam dump condenser fan motor','IUS-LZ-EM01D',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32916,'Amine Dozing system-2 dozing pump motor','IUS-DEA-PM-106B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32917,'Hydrazine Dozing system-2 Agitator','TEMP21',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32918,'Condensate transfer pump to MUH','IUS-LZ-P-104A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32919,'LP steam dump condenser fan motor','IUS-LZ-EM01C',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32920,'Condensate transfer pump to De aerator, turbine','IUS-LZ-PT-105C',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32921,'Condensate transfer pump to De aerator','IUS-LZ-P-105A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32922,'LP steam dump condenser','IUS-LZ-101-E01',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32923,'Condensate transfer pump to MUH','IUS-LZ-P-104B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32924,'Dump condenser condensate pump motor','IUS-LZ-PM-101B4',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32925,'Dump condenser condensate pump motor','IUS-LZ-PM-101AA',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32926,'Amine Dozing system-1 dozing pump','IUS-DEA-P-101A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32927,'Suspect Condensate transfer Pump','IUS-LZ-P-101BB',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32928,'Amine Dozing system-2 Agitator','TEMP20',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32929,'Dump condenser condensate pump','IUS-LZ-P-101BBB',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32930,'Condensate transfer pump to MUH','IUS-LZ-PM-104B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32931,'Hydrazine Dozingsystem-2 Agitator motor','IUS-DEA-TM-104',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32932,'Amine Dozing system-1 Agitator motor','IUS-DEA-TM-102',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32933,'Hydrazine Dozing system-2 dozing pump motor','IUS-DEA-PM-102C',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32934,'Amine Dozing system-2 dozing pump','IUS-DEA-P-106A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32935,'LP steam dump condenser fan','IUS-LZ-E01A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32936,'Dump condenser condensate tank','IUS-LZ-T-108',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32937,'Hydrazine Dozing system-2 dozing pump motor','IUS-DEA-PM-102A',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32938,'Hydrazine Dozing system-2 dozing pump','IUS-DEA-P-102C',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32939,'Condensate transfer pump to De aerator','IUS-LZ-P-105C',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32940,'Condensate transfer pump to De aerator, motor','IUS-LZ-PM-105B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32941,'Amine Dozing system-1 dozing pump','IUS-DEA-P-101B',32883,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32942,NULL,'Flare system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32943,NULL,'Plant steam system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32944,'','Instrument air and plant air system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32945,'','Air and flue gas system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32946,'FD fan turbine LO cooler','IUS-UB--12-E-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32947,'FD fan turbineAOP-UB13','IUS-UB-13-FM-03',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32948,'FD fan- UB12','IUS-UB-12-F-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32949,'FD fan 2 way clutch motor side, UB13','TEMP16',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32950,'FD fan drive turbine-UB12','IUS-UB-12-FT-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32951,'FD fan drive motor-UB13','IUS-UB-13-FM-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32952,'FD fan drive motor-UB12','IUS-UB-12-FM-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32953,'FD fan turbineAOP-UB12','IUS-UB-12FM-03',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32954,'FD fan 2 way clutch motor side, UB12','TEMP14',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32955,'Air registers, UB12','TEMP17',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32956,'Air pre heater,UB12','IUS-UB-12-AH-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32957,'Air registers, UB13','TEMP18',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32958,'FD fan drive turbine-UB13','IUS-UB-13-FT-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32959,'Air pre heater, UB13','IUS-UB-13-AH-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32960,'FD fan 2 way clutch turbine side, UB13','TEMP15',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32961,'FD fan 2 way clutch turbine side, UB12','TEMP13',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32962,'FD fan turbine LO cooler','IUS-UB--13-E-01',32945,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32963,'','UPS',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32964,'','Boiler pressure parts',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32965,'Economiser-UB13','TEMP7',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32966,'Boiler drum and internals','IUS-UB-12-SD-01',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32967,'Primary super heater-1, UB13','TEMP4',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32968,'Secondary super heater, UB13','TEMP6',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32969,'Economiser-UB12','IUS-UB-12-FF-B01',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32970,'Secondary super heater, UB12','TEMP3',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32971,'Primary super heater-2,UB13','TEMP5',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32972,'Primary super heater-1, UB12','TEMP1',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32973,'Primary super heater-2,UB12','TEMP2',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32974,'Boiler drum and internals','IUS-UB-13-SD-01',32964,'Loop/Equipments',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32975,'','Sealing/ cooling air system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001'),(32976,NULL,'Steam distribution system',32770,'Sub system',NULL,'\0',1,0,0,NULL,NULL,'GVG001');
/*!40000 ALTER TABLE `hierarchynode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hierarchynode_documents`
--

DROP TABLE IF EXISTS `hierarchynode_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hierarchynode_documents` (
  `HierarchyNode_id` bigint(20) NOT NULL,
  `documents` varchar(255) DEFAULT NULL,
  `documents_KEY` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`HierarchyNode_id`,`documents_KEY`),
  CONSTRAINT `FK_3aib472gju139v26l9a6tgj8p` FOREIGN KEY (`HierarchyNode_id`) REFERENCES `hierarchynode` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hierarchynode_documents`
--

LOCK TABLES `hierarchynode_documents` WRITE;
/*!40000 ALTER TABLE `hierarchynode_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `hierarchynode_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hierarchynode_drawings`
--

DROP TABLE IF EXISTS `hierarchynode_drawings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hierarchynode_drawings` (
  `HierarchyNode_id` bigint(20) NOT NULL,
  `drawings` varchar(255) DEFAULT NULL,
  `drawings_KEY` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`HierarchyNode_id`,`drawings_KEY`),
  CONSTRAINT `FK_6aow7wh0mrugp60gt8gl7h4w4` FOREIGN KEY (`HierarchyNode_id`) REFERENCES `hierarchynode` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hierarchynode_drawings`
--

LOCK TABLES `hierarchynode_drawings` WRITE;
/*!40000 ALTER TABLE `hierarchynode_drawings` DISABLE KEYS */;
/*!40000 ALTER TABLE `hierarchynode_drawings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `usageStatus` int(11) DEFAULT NULL,
  `estimatedCost_id` bigint(20) DEFAULT NULL,
  `delayStatus` int(11) DEFAULT NULL,
  `isInPunchList` bit(1) NOT NULL,
  `jobName` varchar(255) NOT NULL,
  `parentJobGroupId` bigint(20) NOT NULL,
  `workStatus` int(11) DEFAULT NULL,
  `AfterEnd_id` bigint(20) DEFAULT NULL,
  `AfterStart_id` bigint(20) DEFAULT NULL,
  `actualCost_id` bigint(20) DEFAULT NULL,
  `currentSchedule_id` bigint(20) DEFAULT NULL,
  `dependencyJobs_id` bigint(20) DEFAULT NULL,
  `initiator_id` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_kbxm4s8j62fg5khtwj3ljmiwb` (`name`),
  KEY `FK_3vnlussuxj2mucraov9afk0jn` (`AfterEnd_id`),
  KEY `FK_to17ilt9c6354nekyhhbmbmg8` (`AfterStart_id`),
  KEY `FK_935a336xacjuko2sf54mkglgs` (`actualCost_id`),
  KEY `FK_t27ipnwqugevxp69tx3q2n25g` (`currentSchedule_id`),
  KEY `FK_edh6sb31fo4tb1h2itew0htaw` (`dependencyJobs_id`),
  KEY `FK_aitg37nml82c8pre4v8d3va7k` (`initiator_id`),
  KEY `FK_qng5xxu6woi6fvmpfhmtqwt0e` (`owner_id`),
  KEY `FK_1layl32c5u3ucg2wj2lmxptot` (`estimatedCost_id`),
  CONSTRAINT `FK_1layl32c5u3ucg2wj2lmxptot` FOREIGN KEY (`estimatedCost_id`) REFERENCES `cost` (`id`),
  CONSTRAINT `FK_3vnlussuxj2mucraov9afk0jn` FOREIGN KEY (`AfterEnd_id`) REFERENCES `jobgroup` (`id`),
  CONSTRAINT `FK_935a336xacjuko2sf54mkglgs` FOREIGN KEY (`actualCost_id`) REFERENCES `cost` (`id`),
  CONSTRAINT `FK_aitg37nml82c8pre4v8d3va7k` FOREIGN KEY (`initiator_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK_edh6sb31fo4tb1h2itew0htaw` FOREIGN KEY (`dependencyJobs_id`) REFERENCES `dependencyjobs` (`id`),
  CONSTRAINT `FK_qng5xxu6woi6fvmpfhmtqwt0e` FOREIGN KEY (`owner_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK_t27ipnwqugevxp69tx3q2n25g` FOREIGN KEY (`currentSchedule_id`) REFERENCES `timeschedule` (`id`),
  CONSTRAINT `FK_to17ilt9c6354nekyhhbmbmg8` FOREIGN KEY (`AfterStart_id`) REFERENCES `jobgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobgroup`
--

DROP TABLE IF EXISTS `jobgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobgroup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delayStatus` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parentId` bigint(20) NOT NULL,
  `workStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobgroup`
--

LOCK TABLES `jobgroup` WRITE;
/*!40000 ALTER TABLE `jobgroup` DISABLE KEYS */;
INSERT INTO `jobgroup` VALUES (1,NULL,'Activity',0,NULL),(2,NULL,'Pre commissioning and commissioning activities and specific tasks',0,NULL),(3,NULL,'Mechanical completion',0,NULL),(4,NULL,'Generation of formats/ check lists',0,NULL),(5,NULL,'Completion of checklists points',0,NULL),(6,NULL,'Training',0,NULL),(7,NULL,'Pre commissioning audits',0,NULL),(8,NULL,'Statutory audits and approvals',0,NULL),(9,NULL,'Commissioning of pipe lines',0,NULL),(10,NULL,'Commissioning of Vessels',0,NULL),(11,NULL,'Commissioning of Instruments',0,NULL),(12,NULL,'Commissioning of Electrical equipments',0,NULL),(13,NULL,'Commissioning of Rotating equipments',0,NULL),(14,NULL,'Commissioning of boiler',0,NULL);
/*!40000 ALTER TABLE `jobgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobgroup_jobgroup`
--

DROP TABLE IF EXISTS `jobgroup_jobgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobgroup_jobgroup` (
  `JobGroup_id` bigint(20) NOT NULL,
  `jobGroups_id` bigint(20) NOT NULL,
  PRIMARY KEY (`JobGroup_id`,`jobGroups_id`),
  KEY `FK_o5xmna83qjfilgwg3ty73puo9` (`jobGroups_id`),
  CONSTRAINT `FK_6nf5fymipubl3f8c74twfxevh` FOREIGN KEY (`JobGroup_id`) REFERENCES `jobgroup` (`id`),
  CONSTRAINT `FK_o5xmna83qjfilgwg3ty73puo9` FOREIGN KEY (`jobGroups_id`) REFERENCES `jobgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobgroup_jobgroup`
--

LOCK TABLES `jobgroup_jobgroup` WRITE;
/*!40000 ALTER TABLE `jobgroup_jobgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobgroup_jobgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobgroup_standardjob`
--

DROP TABLE IF EXISTS `jobgroup_standardjob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobgroup_standardjob` (
  `JobGroup_id` bigint(20) NOT NULL,
  `jobs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`JobGroup_id`,`jobs_id`),
  CONSTRAINT `FK_6rgtnmwloduolh7wl4xr3ibky` FOREIGN KEY (`JobGroup_id`) REFERENCES `jobgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobgroup_standardjob`
--

LOCK TABLES `jobgroup_standardjob` WRITE;
/*!40000 ALTER TABLE `jobgroup_standardjob` DISABLE KEYS */;
INSERT INTO `jobgroup_standardjob` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(9,10),(9,11),(9,12),(9,13),(9,14),(9,15),(9,16),(9,17),(9,18),(9,19),(9,20),(9,21),(9,22),(9,23),(9,24),(9,25),(9,26),(9,27),(10,24),(10,28),(10,29),(10,30),(10,31),(10,32),(10,33),(10,34),(10,35),(11,36),(11,37),(11,38),(11,39),(12,40),(12,41),(12,42),(12,43),(12,44),(12,45),(12,46),(13,44),(13,47),(13,48),(13,49),(13,50),(13,51),(13,52),(13,53),(13,54),(13,55),(13,56),(13,57),(13,58),(13,59),(13,60),(14,26),(14,61),(14,62),(14,63),(14,64),(14,65),(14,66),(14,67);
/*!40000 ALTER TABLE `jobgroup_standardjob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_griwilufaypfq6nxhupb1jfrv` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization_department`
--

DROP TABLE IF EXISTS `organization_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_department` (
  `Organization_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  UNIQUE KEY `UK_e1eb32r7yc95j7i96bxe4fwm7` (`departments_id`),
  KEY `FK_3p389itxcbngkqxt9csihklhi` (`Organization_id`),
  CONSTRAINT `FK_3p389itxcbngkqxt9csihklhi` FOREIGN KEY (`Organization_id`) REFERENCES `organization` (`id`),
  CONSTRAINT `FK_e1eb32r7yc95j7i96bxe4fwm7` FOREIGN KEY (`departments_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization_department`
--

LOCK TABLES `organization_department` WRITE;
/*!40000 ALTER TABLE `organization_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `organization_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dipkk7lan2vfsi3ivnm1mkjag` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES ('GVG001',NULL,NULL);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_role`
--

DROP TABLE IF EXISTS `person_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_role` (
  `Person_id` varchar(255) NOT NULL,
  `rolesInDepartment_roleId` varchar(255) NOT NULL,
  `rolesInDepartment_KEY` int(11) NOT NULL,
  PRIMARY KEY (`Person_id`,`rolesInDepartment_KEY`),
  KEY `FK_dbtg6ucuf3xpoxlsdsxh42i12` (`rolesInDepartment_roleId`),
  KEY `FK_t462g793a0jawre122hu2v9ke` (`rolesInDepartment_KEY`),
  CONSTRAINT `FK_3eewln3pp9wvfoftgg1od5fud` FOREIGN KEY (`Person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK_dbtg6ucuf3xpoxlsdsxh42i12` FOREIGN KEY (`rolesInDepartment_roleId`) REFERENCES `role` (`roleId`),
  CONSTRAINT `FK_t462g793a0jawre122hu2v9ke` FOREIGN KEY (`rolesInDepartment_KEY`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_role`
--

LOCK TABLES `person_role` WRITE;
/*!40000 ALTER TABLE `person_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persondays`
--

DROP TABLE IF EXISTS `persondays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persondays` (
  `numberofDays` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_pvdn4o4pgsli9srfn9bk0wsw1` FOREIGN KEY (`id`) REFERENCES `worktime` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persondays`
--

LOCK TABLES `persondays` WRITE;
/*!40000 ALTER TABLE `persondays` DISABLE KEYS */;
/*!40000 ALTER TABLE `persondays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `roleId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_person`
--

DROP TABLE IF EXISTS `role_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_person` (
  `Role_roleId` varchar(255) NOT NULL,
  `users_id` varchar(255) NOT NULL,
  KEY `FK_i9ucb2y33jaqh8ufp2majq0t6` (`users_id`),
  KEY `FK_60bboev00swudflmo98b2iu5v` (`Role_roleId`),
  CONSTRAINT `FK_60bboev00swudflmo98b2iu5v` FOREIGN KEY (`Role_roleId`) REFERENCES `role` (`roleId`),
  CONSTRAINT `FK_i9ucb2y33jaqh8ufp2majq0t6` FOREIGN KEY (`users_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_person`
--

LOCK TABLES `role_person` WRITE;
/*!40000 ALTER TABLE `role_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduledstage`
--

DROP TABLE IF EXISTS `scheduledstage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduledstage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actualEndDate` datetime DEFAULT NULL,
  `actualStartDate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `estimatedEendDate` datetime DEFAULT NULL,
  `estimatedStartDate` datetime DEFAULT NULL,
  `percentageOfWork` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduledstage`
--

LOCK TABLES `scheduledstage` WRITE;
/*!40000 ALTER TABLE `scheduledstage` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduledstage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standardhierarchynode`
--

DROP TABLE IF EXISTS `standardhierarchynode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standardhierarchynode` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parentId` bigint(20) NOT NULL,
  `hierarchyLevel_levelName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pjpl5kc9eav6dju76l7arft4h` (`name`),
  KEY `FK_1e87mfm84qo79p119urmbtyw1` (`hierarchyLevel_levelName`),
  CONSTRAINT `FK_1e87mfm84qo79p119urmbtyw1` FOREIGN KEY (`hierarchyLevel_levelName`) REFERENCES `hierarchylevel` (`levelName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standardhierarchynode`
--

LOCK TABLES `standardhierarchynode` WRITE;
/*!40000 ALTER TABLE `standardhierarchynode` DISABLE KEYS */;
INSERT INTO `standardhierarchynode` VALUES (1,'Boiler drum and internals','IUS-UB-12-SD-01',11,'Loop/Equipments'),(2,'Boiler drum and internals','IUS-UB-13-SD-01',11,'Loop/Equipments'),(3,'Primary super heater-1, UB12','TEMP1',11,'Loop/Equipments'),(4,'Primary super heater-2,UB12','TEMP2',11,'Loop/Equipments'),(5,'Secondary super heater, UB12','TEMP3',11,'Loop/Equipments'),(6,'Primary super heater-1, UB13','TEMP4',11,'Loop/Equipments'),(7,'Primary super heater-2,UB13','TEMP5',11,'Loop/Equipments'),(8,'Secondary super heater, UB13','TEMP6',11,'Loop/Equipments'),(9,'Economiser-UB12','IUS-UB-12-FF-B01',11,'Loop/Equipments'),(10,'Economiser-UB13','TEMP7',11,'Loop/Equipments'),(11,'','Boiler pressure parts',207,'Sub system'),(12,'De aerator-1','TEMP9',60,'Loop/Equipments'),(13,'De aerator-2','TEMP10',60,'Loop/Equipments'),(14,'De aerator storage tank-1','IUS-DEA-T-101A',60,'Loop/Equipments'),(15,'De aerator storage tank-2','IUS-DEA-T-101B',60,'Loop/Equipments'),(16,'Vent condenser-1','IUS-DEA-E-101A',60,'Loop/Equipments'),(17,'vent condenser-2','IUS-DEA-E-101B',60,'Loop/Equipments'),(18,'UB boiler feed water pump','IUS-BFPU-P101A',60,'Loop/Equipments'),(19,'UB boiler feed water pump motor','IUS-BFPU-PM101A',60,'Loop/Equipments'),(20,'UB boiler feed water pump','IUS-BFPU-P101B',60,'Loop/Equipments'),(21,'UB boiler feed water pump motor','IUS-BFPU-PM101B',60,'Loop/Equipments'),(22,'UB boiler feed water pump','IUS-BFPU-P101C',60,'Loop/Equipments'),(23,'UB boiler feed water pump turbine','IUS-BFPU-PT101C',60,'Loop/Equipments'),(24,'UB boiler feed water pump','IUS-BFPU-P101D',60,'Loop/Equipments'),(25,'UB boiler feed water pump turbine','IUS-BFPU-PT101D',60,'Loop/Equipments'),(26,'HRSG Boiler feed water pump','IUS-BFPH-P101A',60,'Loop/Equipments'),(27,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101A',60,'Loop/Equipments'),(28,'HRSG Boiler feed water pump','IUS-BFPH-P101B',60,'Loop/Equipments'),(29,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101B',60,'Loop/Equipments'),(30,'HRSG Boiler feed water pump','IUS-BFPH-P101C',60,'Loop/Equipments'),(31,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101C',60,'Loop/Equipments'),(32,'HRSG Boiler feed water pump','IUS-BFPH-P101D',60,'Loop/Equipments'),(33,'HRSG Boiler feed water pump MOTOR','IUS-BFPH-PM101D',60,'Loop/Equipments'),(34,'BOILER FEED WATER HEATER','IUS-LZ-E-106A',60,'Loop/Equipments'),(35,'BOILER FEED WATER HEATER','IUS-LZ-E-106B',60,'Loop/Equipments'),(36,'CBD Tank','IUS-UB-12-T-01',60,'Loop/Equipments'),(37,'CBD Tank','IUS-UB-13-T-01',60,'Loop/Equipments'),(38,'IBD tank','IUS-UB-12-T-02',60,'Loop/Equipments'),(39,'IBD tank','IUS-UB-13-T-02',60,'Loop/Equipments'),(40,'HP Dozing system Mixing Tank- UB12','IUS-UB-12-T04',60,'Loop/Equipments'),(41,'HP Dozing system Mixing Tank- UB13','IUS-UB-13-T04',60,'Loop/Equipments'),(42,'HP Dozing system Metering Tank- UB12','IUS-UB-12-T03',60,'Loop/Equipments'),(43,'HP Dozing system Metering Tank- UB13','IUS-UB-13-T03',60,'Loop/Equipments'),(44,'HP Dozing pump- UB12','IUS-UB-12-P-02A',60,'Loop/Equipments'),(45,'HP Dozing pump- UB12','IUS-UB-12-P-02B',60,'Loop/Equipments'),(46,'HP Dozing pump motor- UB12','IUS-UB-12-PM-02A',60,'Loop/Equipments'),(47,'HP Dozing pump motor- UB12','IUS-UB-12-PM-02B',60,'Loop/Equipments'),(48,'HP dozing mixing tank agitator-UB12','TEMP11',60,'Loop/Equipments'),(49,'HP dozing mixing tank agitator motor- UB12','IUS-UB-13-TM-04',60,'Loop/Equipments'),(50,'HP Dozing pump- UB13','IUS-UB-13-P-02A',60,'Loop/Equipments'),(51,'HP Dozing pump- UB13','IUS-UB-13-P-02B',60,'Loop/Equipments'),(52,'HP Dozing pump motor- UB13','IUS-UB-13-PM-02A',60,'Loop/Equipments'),(53,'HP Dozing pump motor- UB13','IUS-UB-13-PM-02B',60,'Loop/Equipments'),(54,'HP dozing mixing tank agitator-UB13','TEMP12',60,'Loop/Equipments'),(55,'HP dozing mixing tank agitator motor- UB13','IUS-UB-12-TM-04',60,'Loop/Equipments'),(56,'Sample cooler- Feed Water-UB12','Sample cooler-1',60,'Loop/Equipments'),(57,'Sample cooler- Sat steam-UB12','Sample cooler-2',60,'Loop/Equipments'),(58,'Sample cooler- Blow down-UB12','Sample cooler-3',60,'Loop/Equipments'),(59,'Sample cooler- SH steam-UB12','Sample cooler-4',60,'Loop/Equipments'),(60,'','Boiler feed water system',207,'Sub system'),(61,'FD fan- UB12','IUS-UB-12-F-01',78,'Loop/Equipments'),(62,'FD fan drive motor-UB12','IUS-UB-12-FM-01',78,'Loop/Equipments'),(63,'FD fan drive turbine-UB12','IUS-UB-12-FT-01',78,'Loop/Equipments'),(64,'FD fan turbineAOP-UB12','IUS-UB-12FM-03',78,'Loop/Equipments'),(65,'FD fan turbine LO cooler','IUS-UB--12-E-01',78,'Loop/Equipments'),(66,'FD fan 2 way clutch turbine side, UB12','TEMP13',78,'Loop/Equipments'),(67,'FD fan 2 way clutch motor side, UB12','TEMP14',78,'Loop/Equipments'),(68,'FD fan drive motor-UB13','IUS-UB-13-FM-01',78,'Loop/Equipments'),(69,'FD fan drive turbine-UB13','IUS-UB-13-FT-01',78,'Loop/Equipments'),(70,'FD fan turbineAOP-UB13','IUS-UB-13-FM-03',78,'Loop/Equipments'),(71,'FD fan turbine LO cooler','IUS-UB--13-E-01',78,'Loop/Equipments'),(72,'FD fan 2 way clutch turbine side, UB13','TEMP15',78,'Loop/Equipments'),(73,'FD fan 2 way clutch motor side, UB13','TEMP16',78,'Loop/Equipments'),(74,'Air pre heater,UB12','IUS-UB-12-AH-01',78,'Loop/Equipments'),(75,'Air pre heater, UB13','IUS-UB-13-AH-01',78,'Loop/Equipments'),(76,'Air registers, UB12','TEMP17',78,'Loop/Equipments'),(77,'Air registers, UB13','TEMP18',78,'Loop/Equipments'),(78,'','Air and flue gas system',207,'Sub system'),(79,'Fuel oil surge tank','IUS-LZ-T-101',115,'Loop/Equipments'),(80,'Fuel oil surge tank heating coil','IUS-LZ-TM-2401',115,'Loop/Equipments'),(81,'Fuel oil circulation pump','IUS-LZ-P-103A',115,'Loop/Equipments'),(82,'Fuel oil circulation pump','IUS-LZ-P-103B',115,'Loop/Equipments'),(83,'Fuel oil circulation pump','IUS-LZ-P-103C',115,'Loop/Equipments'),(84,'Fuel oil circulation pump','IUS-LZ-P-103D',115,'Loop/Equipments'),(85,'Fuel oil circulation pump motor','IUS-LZ-PM-103A',115,'Loop/Equipments'),(86,'Fuel oil circulation pump turbine','IUS-LZ-PT-103B',115,'Loop/Equipments'),(87,'Fuel oil circulation pump motor','IUS-LZ-PM-103C',115,'Loop/Equipments'),(88,'Fuel oil circulation pump turbine','IUS-LZ-PT-103D',115,'Loop/Equipments'),(89,'Fuel oil circulation pump suction strainer','IUS-ST-2301',115,'Loop/Equipments'),(90,'Fuel oil circulation pump suction strainer','IUS-ST-2302',115,'Loop/Equipments'),(91,'Fuel oil heater','IUS-E-1',115,'Loop/Equipments'),(92,'Fuel oil heater','IUS-E-2',115,'Loop/Equipments'),(93,'FO Closed blow down tank','IUS-LZ-T-101A',115,'Loop/Equipments'),(94,'FO Closed blow down tank','IUS-LZ-T-101B',115,'Loop/Equipments'),(95,'FO CBD tank pump','IUS-LZ-P-102A',115,'Loop/Equipments'),(96,'FO CBD tank pump','IUS-LZ-P-102B',115,'Loop/Equipments'),(97,'FO CBD tank pump motor','IUS-LZ-PM-102A',115,'Loop/Equipments'),(98,'FO CBD tank pump motor','IUS-LZ-PM-102B',115,'Loop/Equipments'),(99,'FO CBD tank pump','IUS-LZ-P-102C',115,'Loop/Equipments'),(100,'FO CBD tank pump','IUS-LZ-P-102D',115,'Loop/Equipments'),(101,'FO CBD tank pump motor','IUS-LZ-PM-102C',115,'Loop/Equipments'),(102,'FO CBD tank pump motor','IUS-LZ-PM-102D',115,'Loop/Equipments'),(103,'UB-12 Burner-1','IUS--UB-12-FB-01A',115,'Loop/Equipments'),(104,'UB-12 Burner-2','IUS--UB-12-FB-01B',115,'Loop/Equipments'),(105,'UB-12 Burner-3','IUS--UB-12-FB-01C',115,'Loop/Equipments'),(106,'UB-12 Burner-4','IUS--UB-12-FB-01D',115,'Loop/Equipments'),(107,'UB-12 Burner-5','IUS--UB-12-FB-01E',115,'Loop/Equipments'),(108,'UB-12 Burner-6','IUS--UB-12-FB-01F',115,'Loop/Equipments'),(109,'UB-13 Burner-1','IUS--UB-13-FB-01A',115,'Loop/Equipments'),(110,'UB-13 Burner-2','IUS--UB-13-FB-01B',115,'Loop/Equipments'),(111,'UB-13 Burner-3','IUS--UB-13-FB-01C',115,'Loop/Equipments'),(112,'UB-13 Burner-4','IUS--UB-13-FB-01D',115,'Loop/Equipments'),(113,'UB-13 Burner-5','IUS--UB-13-FB-01E',115,'Loop/Equipments'),(114,'UB-13 Burner-6','IUS--UB-13-FB-01F',115,'Loop/Equipments'),(115,'','Fuel oil system',207,'Sub system'),(116,'Natural gas KOD','IUS-LZ-T-103A',123,'Loop/Equipments'),(117,'HP Fuel gas KOD','IUS-LZ-T-103B',123,'Loop/Equipments'),(118,'Flare KOD','IUS-LZ-T-102',123,'Loop/Equipments'),(119,'Flare KOD condensate pump','IUS-LZ-P-101AA',123,'Loop/Equipments'),(120,'Flare KOD condensate pump','IUS-LZ-P-101BBBB',123,'Loop/Equipments'),(121,'Flare KOD condensate pump','IUS-LZ-PM-101AAA',123,'Loop/Equipments'),(122,'Flare KOD condensate pump','IUS-LZ-PM-101B5',123,'Loop/Equipments'),(123,'','Fuel gas system',207,'Sub system'),(124,'','HP dozing system',207,'Sub system'),(125,'','Hydrazine dozing system',207,'Sub system'),(126,'','Amine dozing system',207,'Sub system'),(127,'Suspect Condensate Tank','IUS-LZ-T-111',185,'Loop/Equipments'),(128,'Suspect Condensate transfer Pump','IUS-LZ-P-101AAA',185,'Loop/Equipments'),(129,'Suspect Condensate transfer Pump','IUS-LZ-P-101BB',185,'Loop/Equipments'),(130,'Suspect Condensate transfer Pump Motor','IUS-LZ-PM-101A',185,'Loop/Equipments'),(131,'Suspect Condensate transfer Pump Motor','IUS-LZ-PM-101B3',185,'Loop/Equipments'),(132,'Condensate tank-1','IUS-LZ-T-104A',185,'Loop/Equipments'),(133,'Condensate tank-2','IUS-LZ-T-104B',185,'Loop/Equipments'),(134,'Condensate transfer pump to MUH','IUS-LZ-P-104A',185,'Loop/Equipments'),(135,'Condensate transfer pump to MUH','IUS-LZ-P-104B',185,'Loop/Equipments'),(136,'Condensate transfer pump to MUH, motor','IUS-LZ-PM-104A',185,'Loop/Equipments'),(137,'Condensate transfer pump to MUH','IUS-LZ-PM-104B',185,'Loop/Equipments'),(138,'Condensate transfer pump to De aerator','IUS-LZ-P-105A',185,'Loop/Equipments'),(139,'Condensate transfer pump to De aerator','IUS-LZ-P-105B',185,'Loop/Equipments'),(140,'Condensate transfer pump to De aerator','IUS-LZ-P-105C',185,'Loop/Equipments'),(141,'Condensate transfer pump to De aerator, motor','IUS-LZ-PM-105A',185,'Loop/Equipments'),(142,'Condensate transfer pump to De aerator, motor','IUS-LZ-PM-105B',185,'Loop/Equipments'),(143,'Condensate transfer pump to De aerator, turbine','IUS-LZ-PT-105C',185,'Loop/Equipments'),(144,'LP steam dump condenser','IUS-LZ-101-E01',185,'Loop/Equipments'),(145,'LP steam dump condenser fan','IUS-LZ-E01A',185,'Loop/Equipments'),(146,'LP steam dump condenser fan','IUS-LZ-E01B',185,'Loop/Equipments'),(147,'LP steam dump condenser fan','IUS-LZ-E01C',185,'Loop/Equipments'),(148,'LP steam dump condenser fan','IUS-LZ-E01D',185,'Loop/Equipments'),(149,'LP steam dump condenser fan motor','IUS-LZ-EM01A',185,'Loop/Equipments'),(150,'LP steam dump condenser fan motor','IUS-LZ-EM01B',185,'Loop/Equipments'),(151,'LP steam dump condenser fan motor','IUS-LZ-EM01C',185,'Loop/Equipments'),(152,'LP steam dump condenser fan motor','IUS-LZ-EM01D',185,'Loop/Equipments'),(153,'Dump condenser condensate tank','IUS-LZ-T-108',185,'Loop/Equipments'),(154,'Atmospheric tank','IUS-LZ-T-112',185,'Loop/Equipments'),(155,'Dump condenser condensate pump','IUS-LZ-P-101A',185,'Loop/Equipments'),(156,'Dump condenser condensate pump','IUS-LZ-P-101BBB',185,'Loop/Equipments'),(157,'Dump condenser condensate pump motor','IUS-LZ-PM-101AA',185,'Loop/Equipments'),(158,'Dump condenser condensate pump motor','IUS-LZ-PM-101B4',185,'Loop/Equipments'),(159,'Amine Dozing System-1 mixing tank','IUS-DEA-T-102',185,'Loop/Equipments'),(160,'Amine Dozing system-1 Metering Tank','IUS-DEA-T-103',185,'Loop/Equipments'),(161,'Amine Dozing system-1 Agitator','TEMP19',185,'Loop/Equipments'),(162,'Amine Dozing system-1 Agitator motor','IUS-DEA-TM-102',185,'Loop/Equipments'),(163,'Amine Dozing system-1 dozing pump','IUS-DEA-P-101A',185,'Loop/Equipments'),(164,'Amine Dozing system-1 dozing pump motor','IUS-DEA-PM-101A',185,'Loop/Equipments'),(165,'Amine Dozing system-1 dozing pump','IUS-DEA-P-101B',185,'Loop/Equipments'),(166,'Amine Dozing system-1 dozing pump motor','IUS-DEA-PM-101B',185,'Loop/Equipments'),(167,'Amine Dozing System-2 mixing tank','IUS-DEA-T-106',185,'Loop/Equipments'),(168,'Amine Dozing system-2 Metering Tank','IUS-DEA-T-107',185,'Loop/Equipments'),(169,'Amine Dozing system-2 Agitator','TEMP20',185,'Loop/Equipments'),(170,'Amine Dozing system-2 Agitator motor','IUS-DEA-TM-106',185,'Loop/Equipments'),(171,'Amine Dozing system-2 dozing pump','IUS-DEA-P-106A',185,'Loop/Equipments'),(172,'Amine Dozing system-2 dozing pump motor','IUS-DEA-PM-106A',185,'Loop/Equipments'),(173,'Amine Dozing system-2 dozing pump','IUS-DEA-P-106B',185,'Loop/Equipments'),(174,'Amine Dozing system-2 dozing pump motor','IUS-DEA-PM-106B',185,'Loop/Equipments'),(175,'Hydrazine Dozing System-2 mixing tank','IUS-DEA-T-104',185,'Loop/Equipments'),(176,'Hydrazine Dozing system-2 Metering Tank','IUS-DEA-T-105',185,'Loop/Equipments'),(177,'Hydrazine Dozing system-2 Agitator','TEMP21',185,'Loop/Equipments'),(178,'Hydrazine Dozingsystem-2 Agitator motor','IUS-DEA-TM-104',185,'Loop/Equipments'),(179,'Hydrazine Dozing ystem-2 dozing pump','IUS-DEA-P-102A',185,'Loop/Equipments'),(180,'Hydrazine Dozing system-2 dozing pump motor','IUS-DEA-PM-102A',185,'Loop/Equipments'),(181,'Hydrazine Dozing system-2 dozing pump','IUS-DEA-P-102B',185,'Loop/Equipments'),(182,'Hydrazine Dozing system-2 dozing pump motor','IUS-DEA-PM-102B',185,'Loop/Equipments'),(183,'Hydrazine Dozing system-2 dozing pump','IUS-DEA-P-102C',185,'Loop/Equipments'),(184,'Hydrazine Dozing system-2 dozing pump motor','IUS-DEA-PM-102C',185,'Loop/Equipments'),(185,'','Condensate system',207,'Sub system'),(186,'','Inert gas system',207,'Sub system'),(187,'','Instrument air and plant air system',207,'Sub system'),(188,'','Cooling water system',207,'Sub system'),(189,'','Soot blowing system',207,'Sub system'),(190,'','Sealing/ cooling air system',207,'Sub system'),(191,'','Blow down system',207,'Sub system'),(192,'','Fire water system',207,'Sub system'),(193,'','Gas analysers/ monitors',207,'Sub system'),(194,'','Electricalsubstation and power',207,'Sub system'),(195,'','Control power',207,'Sub system'),(196,'','UPS',207,'Sub system'),(197,'','DCS system',207,'Sub system'),(198,'','PLC',207,'Sub system'),(199,'','Sampling system',207,'Sub system'),(200,'SWAS system for UB12','TEMP22',202,'Loop/Equipments'),(201,'SWAS system for UB13','TEMP23',202,'Loop/Equipments'),(202,'','SWAS system',207,'Sub system'),(203,NULL,'CEMS system',207,'Sub system'),(204,NULL,'Flare system',207,'Sub system'),(205,NULL,'Plant steam system',207,'Sub system'),(206,NULL,'Steam distribution system',207,'Sub system'),(207,'','UB-12, UB-13',208,'System'),(208,'','Utilities',209,'Unit'),(209,'','Plant1',0,'Plant');
/*!40000 ALTER TABLE `standardhierarchynode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standardhierarchynode_images`
--

DROP TABLE IF EXISTS `standardhierarchynode_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standardhierarchynode_images` (
  `StandardHierarchyNode_id` bigint(20) NOT NULL,
  `images` varchar(255) DEFAULT NULL,
  `images_KEY` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`StandardHierarchyNode_id`,`images_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standardhierarchynode_images`
--

LOCK TABLES `standardhierarchynode_images` WRITE;
/*!40000 ALTER TABLE `standardhierarchynode_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `standardhierarchynode_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standardhierarchynode_standardhierarchynode`
--

DROP TABLE IF EXISTS `standardhierarchynode_standardhierarchynode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standardhierarchynode_standardhierarchynode` (
  `StandardHierarchyNode_id` bigint(20) NOT NULL,
  `children_id` bigint(20) NOT NULL,
  PRIMARY KEY (`StandardHierarchyNode_id`,`children_id`),
  UNIQUE KEY `UK_13siqwftm5lf6pnpamw77hjpx` (`children_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standardhierarchynode_standardhierarchynode`
--

LOCK TABLES `standardhierarchynode_standardhierarchynode` WRITE;
/*!40000 ALTER TABLE `standardhierarchynode_standardhierarchynode` DISABLE KEYS */;
INSERT INTO `standardhierarchynode_standardhierarchynode` VALUES (11,1),(11,2),(11,3),(11,4),(11,5),(11,6),(11,7),(11,8),(11,9),(11,10),(207,11),(60,12),(60,13),(60,14),(60,15),(60,16),(60,17),(60,18),(60,19),(60,20),(60,21),(60,22),(60,23),(60,24),(60,25),(60,26),(60,27),(60,28),(60,29),(60,30),(60,31),(60,32),(60,33),(60,34),(60,35),(60,36),(60,37),(60,38),(60,39),(60,40),(60,41),(60,42),(60,43),(60,44),(60,45),(60,46),(60,47),(60,48),(60,49),(60,50),(60,51),(60,52),(60,53),(60,54),(60,55),(60,56),(60,57),(60,58),(60,59),(207,60),(78,61),(78,62),(78,63),(78,64),(78,65),(78,66),(78,67),(78,68),(78,69),(78,70),(78,71),(78,72),(78,73),(78,74),(78,75),(78,76),(78,77),(207,78),(115,79),(115,80),(115,81),(115,82),(115,83),(115,84),(115,85),(115,86),(115,87),(115,88),(115,89),(115,90),(115,91),(115,92),(115,93),(115,94),(115,95),(115,96),(115,97),(115,98),(115,99),(115,100),(115,101),(115,102),(115,103),(115,104),(115,105),(115,106),(115,107),(115,108),(115,109),(115,110),(115,111),(115,112),(115,113),(115,114),(207,115),(123,116),(123,117),(123,118),(123,119),(123,120),(123,121),(123,122),(207,123),(207,124),(207,125),(207,126),(185,127),(185,128),(185,129),(185,130),(185,131),(185,132),(185,133),(185,134),(185,135),(185,136),(185,137),(185,138),(185,139),(185,140),(185,141),(185,142),(185,143),(185,144),(185,145),(185,146),(185,147),(185,148),(185,149),(185,150),(185,151),(185,152),(185,153),(185,154),(185,155),(185,156),(185,157),(185,158),(185,159),(185,160),(185,161),(185,162),(185,163),(185,164),(185,165),(185,166),(185,167),(185,168),(185,169),(185,170),(185,171),(185,172),(185,173),(185,174),(185,175),(185,176),(185,177),(185,178),(185,179),(185,180),(185,181),(185,182),(185,183),(185,184),(207,185),(207,186),(207,187),(207,188),(207,189),(207,190),(207,191),(207,192),(207,193),(207,194),(207,195),(207,196),(207,197),(207,198),(207,199),(202,200),(202,201),(207,202),(207,203),(207,204),(207,205),(207,206),(208,207),(209,208),(32768,32769),(32769,32770),(32770,32771),(32770,32772),(32770,32773),(32770,32774),(32770,32775),(32770,32776),(32770,32777),(32777,32778),(32777,32779),(32777,32780),(32777,32781),(32777,32782),(32777,32783),(32777,32784),(32770,32785),(32770,32786),(32770,32787),(32770,32788),(32770,32789),(32770,32790),(32790,32791),(32790,32792),(32790,32793),(32790,32794),(32790,32795),(32790,32796),(32790,32797),(32790,32798),(32790,32799),(32790,32800),(32790,32801),(32790,32802),(32790,32803),(32790,32804),(32790,32805),(32790,32806),(32790,32807),(32790,32808),(32790,32809),(32790,32810),(32790,32811),(32790,32812),(32790,32813),(32790,32814),(32790,32815),(32790,32816),(32790,32817),(32790,32818),(32790,32819),(32790,32820),(32790,32821),(32790,32822),(32790,32823),(32790,32824),(32790,32825),(32790,32826),(32790,32827),(32790,32828),(32790,32829),(32790,32830),(32790,32831),(32790,32832),(32790,32833),(32790,32834),(32790,32835),(32790,32836),(32790,32837),(32790,32838),(32770,32839),(32839,32840),(32839,32841),(32839,32842),(32839,32843),(32839,32844),(32839,32845),(32839,32846),(32839,32847),(32839,32848),(32839,32849),(32839,32850),(32839,32851),(32839,32852),(32839,32853),(32839,32854),(32839,32855),(32839,32856),(32839,32857),(32839,32858),(32839,32859),(32839,32860),(32839,32861),(32839,32862),(32839,32863),(32839,32864),(32839,32865),(32839,32866),(32839,32867),(32839,32868),(32839,32869),(32839,32870),(32839,32871),(32839,32872),(32839,32873),(32839,32874),(32839,32875),(32770,32876),(32770,32877),(32770,32878),(32770,32879),(32770,32880),(32880,32881),(32880,32882),(32770,32883),(32883,32884),(32883,32885),(32883,32886),(32883,32887),(32883,32888),(32883,32889),(32883,32890),(32883,32891),(32883,32892),(32883,32893),(32883,32894),(32883,32895),(32883,32896),(32883,32897),(32883,32898),(32883,32899),(32883,32900),(32883,32901),(32883,32902),(32883,32903),(32883,32904),(32883,32905),(32883,32906),(32883,32907),(32883,32908),(32883,32909),(32883,32910),(32883,32911),(32883,32912),(32883,32913),(32883,32914),(32883,32915),(32883,32916),(32883,32917),(32883,32918),(32883,32919),(32883,32920),(32883,32921),(32883,32922),(32883,32923),(32883,32924),(32883,32925),(32883,32926),(32883,32927),(32883,32928),(32883,32929),(32883,32930),(32883,32931),(32883,32932),(32883,32933),(32883,32934),(32883,32935),(32883,32936),(32883,32937),(32883,32938),(32883,32939),(32883,32940),(32883,32941),(32770,32942),(32770,32943),(32770,32944),(32770,32945),(32945,32946),(32945,32947),(32945,32948),(32945,32949),(32945,32950),(32945,32951),(32945,32952),(32945,32953),(32945,32954),(32945,32955),(32945,32956),(32945,32957),(32945,32958),(32945,32959),(32945,32960),(32945,32961),(32945,32962),(32770,32963),(32770,32964),(32964,32965),(32964,32966),(32964,32967),(32964,32968),(32964,32969),(32964,32970),(32964,32971),(32964,32972),(32964,32973),(32964,32974),(32770,32975),(32770,32976);
/*!40000 ALTER TABLE `standardhierarchynode_standardhierarchynode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standardjob`
--

DROP TABLE IF EXISTS `standardjob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standardjob` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `usageStatus` int(11) DEFAULT NULL,
  `estimatedCost_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fxv6ok0n73puokahi6ng5h2un` (`name`),
  KEY `FK_21vp48wm41sdbe863tlry6veu` (`estimatedCost_id`),
  CONSTRAINT `FK_21vp48wm41sdbe863tlry6veu` FOREIGN KEY (`estimatedCost_id`) REFERENCES `cost` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standardjob`
--

LOCK TABLES `standardjob` WRITE;
/*!40000 ALTER TABLE `standardjob` DISABLE KEYS */;
INSERT INTO `standardjob` VALUES (1,'Task','Task',NULL,NULL),(2,'Pre commissioning and commissioning activities and specific tasks','Pre commissioning and commissioning activities and specific tasks',NULL,NULL),(3,'Mechanical completion','Mechanical completion',NULL,NULL),(4,'Generation of formats/ check lists','Generation of formats/ check lists',NULL,NULL),(5,'Completion of checklists points','Completion of checklists points',NULL,NULL),(6,'Training','Training',NULL,NULL),(7,'Pre commissioning audits','Pre commissioning audits',NULL,NULL),(8,'Statutory audits and approvals','Statutory audits and approvals',NULL,NULL),(9,'Checking integrity of the Vents','Checking integrity of the Vents',NULL,NULL),(10,'Checking integrity of the Drains','Checking integrity of the Drains',NULL,NULL),(11,'Checking integrity of the Expansion loops','Checking integrity of the Expansion loops',NULL,NULL),(12,'Checking integrity of the Supports','Checking integrity of the Supports',NULL,NULL),(13,'Checking integrity of the Gaskets','Checking integrity of the Gaskets',NULL,NULL),(14,'Checking integrity of the Re glanding','Checking integrity of the Re glanding',NULL,NULL),(15,'Checking integrity of the Leak testing','Checking integrity of the Leak testing',NULL,NULL),(16,'Checking integrity of the Bonding','Checking integrity of the Bonding',NULL,NULL),(17,'Flushing','Flushing',NULL,NULL),(18,'Chemical cleaning_passivation','Chemical cleaning_passivation',NULL,NULL),(19,'Card board blasting','Card board blasting',NULL,NULL),(20,'purging','purging',NULL,NULL),(21,'Drying','Drying',NULL,NULL),(22,'PDP_procedure','PDP_procedure',NULL,NULL),(23,'Sampling for ensuring completion criteria','Sampling for ensuring completion criteria',NULL,NULL),(24,'Charging process fluid/ preservation','Charging process fluid/ preservation',NULL,NULL),(25,'Steam flushing','Steam flushing',NULL,NULL),(26,'Steam blowing','Steam blowing',NULL,NULL),(27,'Target steam blowing','Target steam blowing',NULL,NULL),(28,'Vessel Cleaning','Vessel Cleaning',NULL,NULL),(29,'Process Inspection','Process Inspection',NULL,NULL),(30,'Vessel flushing','Vessel flushing',NULL,NULL),(31,'Chemical cleaning/passivation','Chemical cleaning/passivation',NULL,NULL),(32,'Safety valve testing','Safety valve testing',NULL,NULL),(33,'Thickness/ base data recording','Thickness/ base data recording',NULL,NULL),(34,'Checking integrity of internals','Checking integrity of internals',NULL,NULL),(35,'PDPprocedure','PDPprocedure',NULL,NULL),(36,'Instrument loop testing','Instrument loop testing',NULL,NULL),(37,'Stroking of control valves','Stroking of control valves',NULL,NULL),(38,'Testing interlocks and shut down systems','Testing interlocks and shut down systems',NULL,NULL),(39,'Tuning of instruments','Tuning of instruments',NULL,NULL),(40,'Electrical panels testing','Electrical panels testing',NULL,NULL),(41,'Cable and motor IR testing','Cable and motor IR testing',NULL,NULL),(42,'Panel charging','Panel charging',NULL,NULL),(43,'Solo run of motors and recording of no load readings','Solo run of motors and recording of no load readings',NULL,NULL),(44,'No load trial and vibration recording','No load trial and vibration recording',NULL,NULL),(45,'Noload only for motor','Noload only for motor',NULL,NULL),(46,'Direction checking and correction','Direction checking and correction',NULL,NULL),(47,'Alignment without piping','Alignment without piping',NULL,NULL),(48,'Alignment with piping','Alignment with piping',NULL,NULL),(49,'Correction of piping strain, if required','Correction of piping strain, if required',NULL,NULL),(50,'Bearing oil flushing','Bearing oil flushing',NULL,NULL),(51,'Oil level indicator checking','Oil level indicator checking',NULL,NULL),(52,'Seal pot flushing','Seal pot flushing',NULL,NULL),(53,'Seal pot level switch checking','Seal pot level switch checking',NULL,NULL),(54,'Seal pot instrumentation checking','Seal pot instrumentation checking',NULL,NULL),(55,'Seal and piping flushing','Seal and piping flushing',NULL,NULL),(56,'Establishing seal cooling water & quench supplies','Establishing seal cooling water & quench supplies',NULL,NULL),(57,'Suction strainer - fine mesh/normal operation','Suction strainer - fine mesh/normal operation',NULL,NULL),(58,'Pump vent/ drain checking','Pump vent/ drain checking',NULL,NULL),(59,'Min circulation system checking','Min circulation system checking',NULL,NULL),(60,'Balance leak of system checking','Balance leak of system checking',NULL,NULL),(61,'Furnace leak testing','Furnace leak testing',NULL,NULL),(62,'Pressure parts flushing','Pressure parts flushing',NULL,NULL),(63,'Refractory curing','Refractory curing',NULL,NULL),(64,'Alkali boil out','Alkali boil out',NULL,NULL),(65,'Flushing/ Cleaning','Flushing/ Cleaning',NULL,NULL),(66,'Drum internals fixing','Drum internals fixing',NULL,NULL),(67,'Safety valve floating','Safety valve floating',NULL,NULL);
/*!40000 ALTER TABLE `standardjob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standardjob_checklistitem`
--

DROP TABLE IF EXISTS `standardjob_checklistitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standardjob_checklistitem` (
  `StandardJob_id` bigint(20) NOT NULL,
  `checkListItems_id` bigint(20) NOT NULL,
  PRIMARY KEY (`StandardJob_id`,`checkListItems_id`),
  UNIQUE KEY `UK_kysgoq3u4sj0ec0lpgyikhht6` (`checkListItems_id`),
  CONSTRAINT `FK_kysgoq3u4sj0ec0lpgyikhht6` FOREIGN KEY (`checkListItems_id`) REFERENCES `checklistitem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standardjob_checklistitem`
--

LOCK TABLES `standardjob_checklistitem` WRITE;
/*!40000 ALTER TABLE `standardjob_checklistitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `standardjob_checklistitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standardjob_photopaths`
--

DROP TABLE IF EXISTS `standardjob_photopaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standardjob_photopaths` (
  `StandardJob_id` bigint(20) NOT NULL,
  `photoPaths` varchar(255) DEFAULT NULL,
  `photoPaths_KEY` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`StandardJob_id`,`photoPaths_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standardjob_photopaths`
--

LOCK TABLES `standardjob_photopaths` WRITE;
/*!40000 ALTER TABLE `standardjob_photopaths` DISABLE KEYS */;
/*!40000 ALTER TABLE `standardjob_photopaths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeschedule`
--

DROP TABLE IF EXISTS `timeschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeschedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sds04cowlh06mralqcredxx3r` (`time_id`),
  CONSTRAINT `FK_sds04cowlh06mralqcredxx3r` FOREIGN KEY (`time_id`) REFERENCES `worktime` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeschedule`
--

LOCK TABLES `timeschedule` WRITE;
/*!40000 ALTER TABLE `timeschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeschedule_scheduledstage`
--

DROP TABLE IF EXISTS `timeschedule_scheduledstage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeschedule_scheduledstage` (
  `TimeSchedule_id` bigint(20) NOT NULL,
  `scheduledstages_id` bigint(20) NOT NULL,
  PRIMARY KEY (`TimeSchedule_id`,`scheduledstages_id`),
  UNIQUE KEY `UK_jrv8neispwbivo7b4seuw09g1` (`scheduledstages_id`),
  CONSTRAINT `FK_qa1mak9g4grb229fma7j2heqv` FOREIGN KEY (`TimeSchedule_id`) REFERENCES `timeschedule` (`id`),
  CONSTRAINT `FK_jrv8neispwbivo7b4seuw09g1` FOREIGN KEY (`scheduledstages_id`) REFERENCES `scheduledstage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeschedule_scheduledstage`
--

LOCK TABLES `timeschedule_scheduledstage` WRITE;
/*!40000 ALTER TABLE `timeschedule_scheduledstage` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeschedule_scheduledstage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worktime`
--

DROP TABLE IF EXISTS `worktime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worktime` (
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worktime`
--

LOCK TABLES `worktime` WRITE;
/*!40000 ALTER TABLE `worktime` DISABLE KEYS */;
/*!40000 ALTER TABLE `worktime` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-05 20:00:04
