import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.services.HierarchyManagement;

//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, RestApplicationBeans.class })
@WebAppConfiguration
public class RestControllerTest {

	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Before
	public void setup() {

		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getLinkedHierarchyNodes() throws Exception {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("parentId", 0);
		attributes.put("pageNumber", 0);
		attributes.put("pageSize", 10);
		RequestBuilder requestBuilder = get("/hierarchyManagement/getLinkedHierarchyNodes", "").contentType(MediaType.APPLICATION_JSON_VALUE).flashAttrs(attributes);
		mockMvc.perform(requestBuilder).andReturn();

	}

}