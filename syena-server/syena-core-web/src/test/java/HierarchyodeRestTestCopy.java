import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class HierarchyodeRestTestCopy {

	private MockMvc mockMvc;

	private int projectId = 1;

	private String ownerId = "Rahul1";

	@Resource
	WebApplicationContext webApplicationContext;
	@Autowired
	PersonRepository personRepository;
	@Autowired
	HierarchyManagement hierarchyManagement;

	private HierarchyLevel hierarchyLevel1;

	private HierarchyNode hierarchyNode1;

	@Before
	public void before() {

		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		this.hierarchyLevel1 = new HierarchyLevel("Hlevel1");

		HierarchyLevel hierarchyLevel2 = new HierarchyLevel("Hlevel2");
		HierarchyLevel hierarchyLevel3 = new HierarchyLevel("Hlevel3");

		hierarchyLevel1.addChild(hierarchyLevel2);
		hierarchyLevel2.addChild(hierarchyLevel3);

		this.hierarchyNode1 = new HierarchyNode("hierarchyNode2", "Dec2", hierarchyLevel1, projectId);
		HierarchyNode hierarchyNode2 = new HierarchyNode("hierarchyNode1", "Dec2", hierarchyLevel2, projectId);
		HierarchyNode hierarchyNode3 = new HierarchyNode("hierarchyNode2", "Dec2", hierarchyLevel2, projectId);
		HierarchyNode hierarchyNode4 = new HierarchyNode("hierarchyNode3", "Dec2", hierarchyLevel3, projectId);

		hierarchyNode1.addChild(hierarchyNode2);
		hierarchyNode1.addChild(hierarchyNode3);
		hierarchyNode2.addChild(hierarchyNode4);

		
	}

	@Test
	public void createMultipleHierarchyNodeHierarchy() throws Exception {

		ObjectMapper objectmapper = new ObjectMapper();

//		String hierarchyLevelcontent = objectmapper.writeValueAsString(hierarchyLevel1);
//		RequestBuilder hierarchyLevelRequestBuilder = put("/hierarchyManagement/createHierarchyLevel").contentType(MediaType.APPLICATION_JSON_VALUE).content(hierarchyLevelcontent);
//		MockHttpServletResponse responce = mockMvc.perform(hierarchyLevelRequestBuilder).andReturn().getResponse();
//
//		Person owner = new Person(ownerId);
//		personRepository.save(owner);

		String content = objectmapper.writeValueAsString(hierarchyNode1);
		RequestBuilder requestBuilder = put("/hierarchyManagement/createHierarchyNode").contentType(MediaType.APPLICATION_JSON_VALUE).content(content).param("owner", ownerId);
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
		System.out.println(responce);

	}

}
