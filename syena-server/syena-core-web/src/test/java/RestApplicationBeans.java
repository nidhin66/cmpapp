

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.api.services.UserManagemnet;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.services.HierarchyManagementImpl;
import com.gvg.syena.core.services.JobManagementImpl;
import com.gvg.syena.core.services.dataupload.DataUpLoader;
import com.gvg.syena.core.services.dataupload.DataUpLoaderImpl;
import com.gvg.syena.core.services.organization.OrganizationManagemnetImpl;
import com.gvg.syena.core.services.schedule.ScheduleManagmentImpl;

@Configuration
public class RestApplicationBeans {

	@Bean
	public HierarchyManagementImpl hierarchyManagement() {

		return new HierarchyManagementImpl();
	}

	@Bean
	public UserManagemnet organizationManagemnet() {
		return new OrganizationManagemnetImpl();

	}

	@Bean
	public ScheduleManagment ScheduleManagment() {
		return new ScheduleManagmentImpl();

	}

	@Bean(name = "jobmanagement")
	public JobManagement jobManagement() {
		return new JobManagementImpl();
	}

	@Bean(name = "dataUpLoad")
	public DataUpLoader dataUpLoad() {
		return new DataUpLoaderImpl();

	}
}
