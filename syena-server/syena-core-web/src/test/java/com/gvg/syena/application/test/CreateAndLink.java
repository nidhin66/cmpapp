package com.gvg.syena.application.test;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class CreateAndLink {

	@Resource
	WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Autowired
	HierarchyManagement hierarchyManagement;

	@Before
	public void before() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

	}

	@Test
	public void hierarchy() throws Exception {

		String owner = "GVG001";

		HierarchyLevel hierarchyLevel = new HierarchyLevel("level1");
		hierarchyManagement.createHierarchyLevel(hierarchyLevel);
		StandardHierarchyNode standardHierarchyNode = new StandardHierarchyNode<>("StandardHierarchyNode_test_1", "StandardHierarchyNode_test_1", hierarchyLevel);
		hierarchyManagement.createStandardHierarchyNode(standardHierarchyNode, "");
		HierarchyNode hierarchyNode = new HierarchyNode(standardHierarchyNode, 1, false);
		hierarchyManagement.createHierarchyNode(hierarchyNode, owner);
		hierarchyManagement.createAndLinkHierarchyNodes(hierarchyNode.getId(), standardHierarchyNode.getId());

	}

}
