package com.gvg.syena.core.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.util.Configuration;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class StandardJobManagementTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	public void createJob() throws Exception {
		Configuration.childEnable = true;
		ObjectMapper objectMapper = new ObjectMapper();
		StandardJob standardJob = new StandardJob("st2", "st2");
		String string = objectMapper.writeValueAsString(standardJob);
		RequestBuilder requestBuilder = put("/jobManagement/createStandardJob").contentType(MediaType.APPLICATION_JSON_VALUE).content(string);
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
	}

//	@Test
	public void createJobGroup() throws Exception {
		Configuration.childEnable = true;
		ObjectMapper objectMapper = new ObjectMapper();
		JobGroup<StandardJob> standardJobGroup = new JobGroup<StandardJob>("JobGroup1");
		String string = objectMapper.writeValueAsString(standardJobGroup);
		RequestBuilder requestBuilder = put("/jobManagement/createStandardJobGroup").contentType(MediaType.APPLICATION_JSON_VALUE).content(string);
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
	}
	
//	@Test
	public void searchJob() throws Exception {
		Configuration.childEnable = true;
		ObjectMapper objectMapper = new ObjectMapper();
		JobProperties jobSearchOption = new JobProperties();	
		jobSearchOption.setName("%Dry%");
		String string = objectMapper.writeValueAsString(jobSearchOption);
		RequestBuilder requestBuilder = get("/jobManagement/searchJobWithProperties").contentType(MediaType.APPLICATION_JSON_VALUE).content(string).param("pageNumber", "0").param("pageSize", "1");
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
	}
	


}
