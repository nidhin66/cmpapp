package com.gvg.syena.core.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class ScheduleManagmnetTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	private ObjectMapper objectMapper;

	@Autowired
	private ScheduleManagment scheduleManagment;

	@Autowired
	private JobManagement jobManagement;

	private long jobId = 32768;


	@Before
	public void before() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		this.objectMapper = new ObjectMapper();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		objectMapper.setDateFormat(dateFormat);

	}

	public void createAndLinkJob() throws Exception {
		long hierarchyNodeId = 214;
		long standardJobId = 1885;
		RequestBuilder getJobRequestBuilder = put("/hierarchyManagement/createAndlinkJob").contentType(MediaType.APPLICATION_JSON_VALUE)
				.param("hierarchyNodeId", "" + hierarchyNodeId).param("standardJobIds", "" + standardJobId);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();
	}


	public void scheduleJob() throws Exception {
		Calendar calendar = Calendar.getInstance();
		Date planedTime1 = calendar.getTime();
		WorkTime worktime = new WorkTime(planedTime1, new PersonDays(30));
		WorkSchedule workSchedule = new WorkSchedule(worktime);

		ScheduledStage first = workSchedule.getScheduledStages().first();
		first.setPlannedTime(planedTime1);
		calendar.add(Calendar.DAY_OF_YEAR, 30);
		Date planedTime2 = calendar.getTime();
		ScheduledStage last = workSchedule.getScheduledStages().last();
		last.setPlannedTime(planedTime2);

		String workSchedulecontent = objectMapper.writeValueAsString(workSchedule);
		RequestBuilder getJobRequestBuilder = put("/scheduleManagment/setUnApprovedJobSchedule").contentType(MediaType.APPLICATION_JSON_VALUE).param("jobId", "" + jobId)
				.content(workSchedulecontent);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();
	}
	@Test
	public void aprroveJob() throws Exception {
		Job job = jobManagement.getJob(jobId);

		
		RequestBuilder getJobRequestBuilder = get("/scheduleManagment/getUnApprovedJobSchedules").contentType(MediaType.APPLICATION_JSON_VALUE).param("pageNumber", "" + 0)
				.param("pageSize", "" + 1);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();

		WorkSchedule workSchedule = scheduleManagment.getPendingForApprovelJobSchedules(job.getId());
		String workSchedulecontent = objectMapper.writeValueAsString(workSchedule);
		RequestBuilder jobRequestBuilder = put("/scheduleManagment/setJobSchedule").contentType(MediaType.APPLICATION_JSON_VALUE).param("jobId", "" + job.getId())
				.content(workSchedulecontent);
		MockHttpServletResponse responced = mockMvc.perform(jobRequestBuilder).andReturn().getResponse();
	}

}
