package com.gvg.syena.application.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.util.View;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class HierarchyodeRestTest {

	@Resource
	WebApplicationContext webApplicationContext;
	private MockMvc mockMvc;
	private HierarchyLevel hierarchyLevel1;
	private String ownerId = "Rahul1";

	@Autowired
	PersonRepository personRepository;

	@Autowired
	HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	HierarchyManagement hierarchyManagement;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		hierarchyLevel1 = new HierarchyLevel("Hlevel1");
	}

	@Test
	public void createMultipleHierarchyNodeHierarchy() throws Exception {

		Person owner = new Person(ownerId);
		personRepository.save(owner);

		ObjectMapper hierarchyLevelobjectMapper = new ObjectMapper();
		String hierarchyLevelcontent = hierarchyLevelobjectMapper.writeValueAsString(hierarchyLevel1);
		RequestBuilder hierarchyLevelRequestBuilder = put("/hierarchyManagement/createHierarchyLevel").contentType(MediaType.APPLICATION_JSON_VALUE).content(hierarchyLevelcontent);
		MockHttpServletResponse responce = mockMvc.perform(hierarchyLevelRequestBuilder).andReturn().getResponse();

		StandardHierarchyNode<StandardHierarchyNode> hierarchyNode1 = new StandardHierarchyNode<StandardHierarchyNode>("HierarchyNode1", "desc1", hierarchyLevel1);
		StandardHierarchyNode<StandardHierarchyNode> hierarchyNode2 = new StandardHierarchyNode<StandardHierarchyNode>("hierarchyNode2", "Dec2", hierarchyLevel1);
		StandardHierarchyNode<StandardHierarchyNode> hierarchyNode21 = new StandardHierarchyNode<StandardHierarchyNode>("hierarchyNode2.1", "Dec2.1", hierarchyLevel1);
		StandardHierarchyNode<StandardHierarchyNode> hierarchyNode22 = new StandardHierarchyNode<StandardHierarchyNode>("hierarchyNode2.2", "Dec2.2", hierarchyLevel1);
		StandardHierarchyNode<StandardHierarchyNode> hierarchyNode3 = new StandardHierarchyNode<StandardHierarchyNode>("hierarchyNode3", "Dec3", hierarchyLevel1);
		hierarchyNode1.addChild(hierarchyNode2);
		List<StandardHierarchyNode> childNodes = new ArrayList<StandardHierarchyNode>();
		childNodes.add(hierarchyNode21);
		childNodes.add(hierarchyNode22);
		hierarchyNode2.addChildren(childNodes);
		hierarchyNode1.addChild(hierarchyNode3);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
		ObjectWriter writer = objectMapper.writerWithView(View.ClientSerilize.class);
		String content = writer.writeValueAsString(new StandardHierarchyNode[] { hierarchyNode1 });
		System.out.println(content);
		RequestBuilder requestBuilder = put("/hierarchyManagement/createStandardHierarchyNodes").contentType(MediaType.APPLICATION_JSON_VALUE).content(content).param("ownerId", owner.getId());
		responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
		System.out.println(responce);

	}
}
