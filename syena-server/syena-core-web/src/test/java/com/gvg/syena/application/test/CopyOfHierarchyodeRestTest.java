package com.gvg.syena.application.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class CopyOfHierarchyodeRestTest {

	@Resource
	WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	private HierarchyLevel hierarchyLevel1;
	private HierarchyLevel hierarchyLevel2;
	private HierarchyLevel hierarchyLevel3;

	private int projectId = 1;
	private String ownerId = "Rahul1";

	@Autowired
	PersonRepository personRepository;

	@Autowired
	HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	HierarchyManagement hierarchyManagement;

	@Before
	public void setup() {

		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		hierarchyLevel1 = new HierarchyLevel("Hlevel1");
		hierarchyLevel2 = new HierarchyLevel("Hlevel2");
		hierarchyLevel3 = new HierarchyLevel("Hlevel3");
		// hierarchyLevel1.addChild(hierarchyLevel2);
		// hierarchyLevel2.addChild(hierarchyLevel3);

	}

	@Test
	public void createMultipleHierarchyNodeHierarchy() throws Exception {

		Person owner = new Person(ownerId);
		personRepository.save(owner);

		/* Level1 */
		ObjectMapper hierarchyLevelobjectMapper = new ObjectMapper();
		String hierarchyLevelcontent = hierarchyLevelobjectMapper.writeValueAsString(hierarchyLevel1);
		RequestBuilder hierarchyLevelRequestBuilder = put("/hierarchyManagement/createHierarchyLevel").contentType(MediaType.APPLICATION_JSON_VALUE).content(hierarchyLevelcontent);
		MockHttpServletResponse responce = mockMvc.perform(hierarchyLevelRequestBuilder).andReturn().getResponse();

		/* Hierarchynode1 */

		HierarchyNode hierarchyNode = new HierarchyNode("HierarchyNode1", "desc1", hierarchyLevel1, projectId);

		ObjectMapper objectMapper = new ObjectMapper();
		String content = objectMapper.writeValueAsString(hierarchyNode);
		RequestBuilder requestBuilder = put("/hierarchyManagement/createHierarchyNode").contentType(MediaType.APPLICATION_JSON_VALUE).content(content).param("owner", ownerId);
		responce.flushBuffer();
		responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
		System.out.println(responce);

		/* Hierarchynode2 */
		HierarchyNode hierarchyNode2 = new HierarchyNode("hierarchyNode2", "Dec2", hierarchyLevel1, projectId);
		hierarchyNode2.setParent(hierarchyNode);
		ObjectMapper objectMapper2 = new ObjectMapper();
		String content2 = objectMapper2.writeValueAsString(hierarchyNode2);
		RequestBuilder requestBuilder2 = put("/hierarchyManagement/createHierarchyNode").contentType(MediaType.APPLICATION_JSON_VALUE).content(content2).param("owner", ownerId);
		responce.flushBuffer();
		responce = mockMvc.perform(requestBuilder2).andReturn().getResponse();
		System.out.println(responce);
		hierarchyNode2.setParent(hierarchyNode);

		/* Hierarchynode2.1 */
		HierarchyNode hierarchyNode21 = new HierarchyNode("hierarchyNode2.1", "Dec2.1", hierarchyLevel1, projectId);
		hierarchyNode21.setParent(hierarchyNode2);
		ObjectMapper objectMapper21 = new ObjectMapper();
		String content21 = objectMapper21.writeValueAsString(hierarchyNode21);
		RequestBuilder requestBuilder21 = put("/hierarchyManagement/createHierarchyNode").contentType(MediaType.APPLICATION_JSON_VALUE).content(content21).param("owner", ownerId);
		responce.flushBuffer();
		responce = mockMvc.perform(requestBuilder21).andReturn().getResponse();
		System.out.println(responce);
		hierarchyNode21.setParent(hierarchyNode2);

		/* Hierarchynode3 */
		HierarchyNode hierarchyNode3 = new HierarchyNode("hierarchyNode3", "Dec3", hierarchyLevel1, projectId);
		hierarchyNode3.setParent(hierarchyNode);
		ObjectMapper objectMapper3 = new ObjectMapper();
		String content3 = objectMapper3.writeValueAsString(hierarchyNode3);
		RequestBuilder requestBuilder3 = put("/hierarchyManagement/createHierarchyNode").contentType(MediaType.APPLICATION_JSON_VALUE).content(content3).param("owner", ownerId);
		responce.flushBuffer();
		responce = mockMvc.perform(requestBuilder3).andReturn().getResponse();
		System.out.println(responce);
		hierarchyNode3.setParent(hierarchyNode);
		
		
		

		System.out.println(hierarchyNode.getChildren());

	}

}
