package com.gvg.syena.core.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class DependencyJobTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void addPredecessorJobs() throws Exception {

		// public List<Job> addPredecessorJobs(@RequestParam(required = true)
		// long jobId, @RequestParam(required = true) long[] predecessorJobIds,
		// @RequestParam(required = true) NodeCategory nodeCategory)

		String jobId = "32777";
		String predecessorJobIds = "32786";
		RequestBuilder requestBuilder = put("/dependencyManagmnet/addPredecessorJobs").contentType(MediaType.APPLICATION_JSON_VALUE).param("jobId", jobId)
				.param("predecessorJobIds", predecessorJobIds).param("nodeCategory", "job");
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();

	}
}
