package com.gvg.syena.core.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.util.Configuration;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class HierarchyodeTest {
	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	// @Test
	public void createMultiLevelNodes() throws Exception {
		Configuration.childEnable = true;
		HierarchyLevel hierarchyLevel = new HierarchyLevel("Plant");
		StandardHierarchyNode<StandardHierarchyNode> node1 = new StandardHierarchyNode<>("1", "1", hierarchyLevel);
		StandardHierarchyNode<StandardHierarchyNode> node11 = new StandardHierarchyNode<>("11", "11", hierarchyLevel);
		StandardHierarchyNode<StandardHierarchyNode> node111 = new StandardHierarchyNode<>("111", "111", hierarchyLevel);
		node1.addChild(node11);
		node11.addChild(node111);
		List<StandardHierarchyNode<StandardHierarchyNode>> list = new ArrayList<>();
		list.add(node1);
		ObjectMapper objectMapper = new ObjectMapper();
		String nodes = objectMapper.writeValueAsString(list);
		String ownerId = "GVG001";
		RequestBuilder requestBuilder = put("/hierarchyManagement/createStandardHierarchyNodes").contentType(MediaType.APPLICATION_JSON_VALUE).content(nodes)
				.param("ownerId", ownerId);
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();

	}

	@Test
	public void imageUpload() throws Exception {
		String file = "C:\\Users\\geeva_000\\Desktop\\28f119f.jpg";
		FileInputStream inputStream = new FileInputStream(file);
		byte[] bytes = new byte[1024 * 100];
		inputStream.read(bytes);
		RequestBuilder requestBuilder = put("/hierarchyManagement/uploadHierarchyNodeImage").contentType(MediaType.APPLICATION_JSON_VALUE).content(bytes)
				.param("hierarchyNodeId", "219").param("documentType", DocumentType.Images.name()).param("imageName", "imageName.jpg");
		MockHttpServletResponse responce = mockMvc.perform(requestBuilder).andReturn().getResponse();
	}
}
