package com.gvg.syena.core.web.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.application.ApplicationConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationConfig.class })
@WebAppConfiguration
public class ScheduleTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	private ObjectMapper mapper;

	@Before
	public void before() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		this.mapper = new ObjectMapper();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		mapper.setDateFormat(dateFormat);

	}

	// @Test
	public void scheduleJob() throws Exception {

		// RequestBuilder getJobRequestBuilder =
		// get("/jobManagement/getJob").contentType(MediaType.APPLICATION_JSON_VALUE).param("jobId",
		// "" + jobid);
		// MockHttpServletResponse responce =
		// mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();
		//
		// Job job = mapper.readValue(responce.getContentAsByteArray(),
		// Job.class);
		// System.out.println(job);
		long jobid = 65536;
		int days = 30;

		WorkSchedule workSchedule = ApplicationUtil.getSystemDefaultWorkSchedule(new WorkTime());
		Calendar calendar = Calendar.getInstance();
		workSchedule.changePlannedStartTime(calendar.getTime());
		workSchedule.getScheduledStages().first().setActualTime(calendar.getTime());

		calendar.add(Calendar.DATE, days);
		workSchedule.changePlannedFinishTime(calendar.getTime());
		String workSchedulecontent = mapper.writeValueAsString(workSchedule);
		RequestBuilder getJobRequestBuilder = put("/scheduleManagment/setJobSchedule").contentType(MediaType.APPLICATION_JSON_VALUE).content(workSchedulecontent)
				.param("jobId", "" + jobid).param("personDays", "" + days);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();

	}

	// @Test
	public void hierarchy() throws Exception {

		long hierarchyId = 212;

		ObjectMapper mapper = new ObjectMapper();

		WorkTime workTime = new WorkTime(new PersonDays(20));
		Calendar calendar = Calendar.getInstance();
		workTime.setPlanedStartTime(calendar.getTime());
		String workTimecontent = mapper.writeValueAsString(workTime);

		RequestBuilder getJobRequestBuilder = put("/scheduleManagment/setHierarchybSchedule").contentType(MediaType.APPLICATION_JSON_VALUE).content(workTimecontent)
				.param("hierarchyId", "" + hierarchyId);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();

	}

	// @Test
	public void createAndLinkFromStandardCheckListToJob() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		long jobId = 65536;
		String standardCheckListItems = "1";
		RequestBuilder getJobRequestBuilder = put("/jobManagement/createAndLinkFromStandardCheckListToJob").contentType(MediaType.APPLICATION_JSON_VALUE)
				.param("jobId", "" + jobId).param("standardCheckListItems", standardCheckListItems);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();

	}

	// @Test
	public void linkHierarchy() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<StandardHierarchyNode> standardHierarchyNodes = new ArrayList<StandardHierarchyNode>();
		HierarchyLevel hierarchyLevel = new HierarchyLevel("Loop/Equipments");
		StandardHierarchyNode node1 = new StandardHierarchyNode<>("Loop1", "description", hierarchyLevel);
		standardHierarchyNodes.add(node1);
		String lisNodecont = mapper.writeValueAsString(standardHierarchyNodes);
		RequestBuilder createStdHir = put("/hierarchyManagement/createStandardHierarchyNodes").contentType(MediaType.APPLICATION_JSON_VALUE).content(lisNodecont);
		MockHttpServletResponse responce = mockMvc.perform(createStdHir).andReturn().getResponse();

		// long parentNodeId = 32979;
		// String nodeIds = "65536" ;
		// RequestBuilder getJobRequestBuilder =
		// put("/hierarchyManagement/createAndLinkHierarchyNodes").contentType(MediaType.APPLICATION_JSON_VALUE).param("parentNodeId",
		// "" + parentNodeId)
		// .param("standardHierarchyNodeIds", nodeIds );
		// MockHttpServletResponse responce =
		// mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();

	}

	@Test
	public void scheduleJobApproved() throws Exception {

		long jobid = 32777;
		int days = 30;

		WorkSchedule workSchedule = ApplicationUtil.getSystemDefaultWorkSchedule(new WorkTime());
		Calendar calendar = Calendar.getInstance();
		workSchedule.changePlannedStartTime(calendar.getTime());
		workSchedule.getScheduledStages().first().setActualTime(calendar.getTime());

		calendar.add(Calendar.DATE, days);
		workSchedule.changePlannedFinishTime(calendar.getTime());
		String workSchedulecontent = mapper.writeValueAsString(workSchedule);
		RequestBuilder getJobRequestBuilder = put("/scheduleManagment/setJobSchedule").contentType(MediaType.APPLICATION_JSON_VALUE).content(workSchedulecontent)
				.param("jobId", "" + jobid).param("personDays", "" + days);
		MockHttpServletResponse responce = mockMvc.perform(getJobRequestBuilder).andReturn().getResponse();

	}
	

}
