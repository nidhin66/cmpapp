package com.gvg.syena.application.test;

import java.util.Set;

import org.springframework.web.client.RestTemplate;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.project.Project;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.names.HierarchyManagementServiceNames;

public class HierarchyManagementRestClient {

	private RestTemplate restTemplate;
	private String url = "http://localhost:8080/cpm/hierarchyManagement";

	public HierarchyManagementRestClient() {

		this.restTemplate = new RestTemplate();

	}

	public void createProject(Project project) {
		restTemplate.postForObject(url + HierarchyManagementServiceNames.createProject, project, Void.class);

	}

	public void createHierarchyLevel(HierarchyLevel hierarchyLevel) {
		// TODO Auto-generated method stub

	}

	public HierarchyLevel getHierarchyLevels(String hierarchyName) throws HierarchyLevelNotFoundException, ServiceLevelValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<StandardHierarchyNode<StandardHierarchyNode>> getAllStandardHierarchyNodes(HierarchyLevel hierarchyLevel, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<HierarchyNode> getAllHierarchyNodes(HierarchyLevel hierarchyLevel, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<HierarchyNode> getHierarchyNodes(long[] nodeIds, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<HierarchyNode> getChildHierarchyNodes(long[] parentIds, int pageNumber, int pageSize) throws ServiceLevelValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<HierarchyNode> getLinkedHierarchyNodes(long parentId, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public void createStandardHierarchyNode(StandardHierarchyNode hierarchyNode) {
		// TODO Auto-generated method stub

	}

	public void createHierarchy(HierarchyNode hierarchyNode, String owner) throws PersonNotFoundException, ServiceLevelValidationException {
		// TODO Auto-generated method stub

	}

	public void updateHierarchyNode(HierarchyNode hierarchyNode) {
		// TODO Auto-generated method stub

	}

	public void obsoleteHierarchyNode(long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		// TODO Auto-generated method stub

	}

	public void linkHierarchyNodes(long parentNodeId, long... nodeId) throws HierarchyNodeNotFoundException, AlreadyLinkedException {
		// TODO Auto-generated method stub

	}

	public void unlinkHierarchyNodes(long parentNodeId, long nodeId) throws HierarchyNodeNotFoundException {
		// TODO Auto-generated method stub

	}

	public PageItr<HierarchyNode> getObsoleteHierarchyNodes(int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public long[] getHierarchyNodeIds(HierarchyLevel hierarchyLevel) throws ServiceLevelValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<HierarchyNode> searchHierarchyNodesInLevel(String hierarchyNodename, HierarchyLevel hierarchyLevel, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public PageItr<HierarchyNode> searchHierarchyNodesInChild(String hierarchyNodename, long parentId, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public void linkJobs(long hierarchyNodeId, long... jobIds) throws HierarchyNodeNotFoundException {
		// TODO Auto-generated method stub

	}

	public Set<Job> getLinkedJob(long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	public StandardHierarchyNode<StandardHierarchyNode> getStandardHierarchyNode(long nodeId) {
		// TODO Auto-generated method stub
		return null;
	}

}
