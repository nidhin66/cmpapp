import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.organization.Department;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;

public class HiDe {

	@Test
	public void testName() throws Exception {
		HierarchyNode hierarchyNode = new HierarchyNode();
		Person owner = new Person();
		Map<Department, Role> rolesInDepartment = new HashMap<Department, Role>();
		Department dep1 = new Department();
		dep1.setDescription("description1");
		dep1.setName("name1");
		Role role1 = new Role();
		role1.setRoleId("roleId1");
		role1.setDescription("description1");
		rolesInDepartment.put(dep1, role1);
		owner.setRolesInDepartment(rolesInDepartment);
		hierarchyNode.setOwner(owner);
		ObjectMapper mapper = new ObjectMapper();
		String content = mapper.writeValueAsString(hierarchyNode);

		System.out.println(content);

		HierarchyNode node = mapper.readValue(content, HierarchyNode.class);

		Set<Department> keys = node.getOwner().getRolesInDepartment().keySet();
		for (Department key : keys) {
			System.out.println(key.getId());
			System.out.println(key.getDescription());
		}

		Role gRole = node.getOwner().getRolesInDepartment().get(dep1);
		String content2 = mapper.writeValueAsString(node);

		System.out.println(content2);

	}
}
