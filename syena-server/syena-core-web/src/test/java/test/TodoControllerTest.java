package test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfigTest.class, Beanz.class })
@WebAppConfiguration
public class TodoControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private TodoService todoService;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() {

		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void todo() throws Exception {
		TodoDTO dto = new TodoDTO();
		RequestBuilder requestBuilder = post("/api/todo").contentType(MediaType.APPLICATION_JSON_VALUE).content(convertObjectToJsonBytes(dto));
		mockMvc.perform(requestBuilder).andReturn().getResponse();

	}

	// @Test
	// public void
	// add_TitleAndDescriptionAreTooLong_ShouldReturnValidationErrorsForTitleAndDescription()
	// throws Exception {
	// TodoDTO dto = new TodoDTO();
	// mockMvc.perform(post("/api/todo").contentType(MediaType.APPLICATION_JSON_VALUE).content(convertObjectToJsonBytes(dto)));
	// // String title = TestUtil.createStringWithLength(101);
	// // String description = TestUtil.createStringWithLength(501);
	// //
	// // TodoDTO dto = new
	// // TodoDTOBuilder().description(description).title(title).build();
	//
	// //
	// mockMvc.perform(post("/api/todo").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(dto))).andExpect(status().isBadRequest())
	// //
	// .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.fieldErrors",
	// // hasSize(2)))
	// // .andExpect(jsonPath("$.fieldErrors[*].path",
	// // containsInAnyOrder("title", "description")))
	// // .andExpect(jsonPath("$.fieldErrors[*].message",
	// //
	// containsInAnyOrder("The maximum length of the description is 500 characters.",
	// // "The maximum length of the title is 100 characters.")));
	// //
	// // verifyZeroInteractions(todoServiceMock);
	// }

	private byte[] convertObjectToJsonBytes(TodoDTO dto) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsBytes(dto);
	}
}