package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.gvg.syena.core.api.entity.job.checklist.CheckList;

public class RestTestClient {

	@Test
	public void testCreateEmployee() {
		RestTemplate restTemplate = new RestTemplate();
		List<CheckList> data = new ArrayList<CheckList>();
		CheckList checkList1 = new CheckList();
		data.add(checkList1);
		Void response = restTemplate.postForObject("http://localhost:8080/cpm/checkListManagement/createCheckLists", data, void.class);
		System.out.println(response);
	}

}
