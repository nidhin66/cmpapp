package test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TodoController {

	private TodoService service;

	@RequestMapping(value = "/api/todo", method = RequestMethod.POST)
	@ResponseBody
	public TodoDTO add(@RequestBody TodoDTO dto) {
		System.out.println(dto);
		return dto;

	}

}