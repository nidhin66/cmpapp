package com.gvg.syena.web.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.JobsBarrierException;
import com.gvg.syena.core.api.exception.RollBackException;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.ScheduleLogService;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.api.services.names.ScheduleManagmentServiceNames;

@RestController
@RequestMapping(value = { "scheduleManagment" })
public class ScheduleManagmentController {

	@Autowired
	private ScheduleManagment scheduleManagment;

	@Autowired
	private ScheduleLogService scheduleLogService;

	@RequestMapping(value = ScheduleManagmentServiceNames.setJobSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setJobSchedule(@RequestParam(required = true) long jobId, @RequestBody(required = true) WorkSchedule workSchedule, @RequestParam(required = true) int personDays)
			throws SchedulingException, JobNotFoundException, JobScheduleFailed, InvalidDataException, JobsBarrierException {
		ServiceLock.getInstance().lock();
		try {
			Set<ScheduleLog> changes = scheduleManagment.setJobSchedule(jobId, workSchedule, personDays);
			scheduleLogService.logSchedule(jobId, NodeCategory.job, changes);
			removeUnApprovedJobSchedule(jobId);
		} finally {
			ServiceLock.getInstance().unLock();
		}
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.validateJobSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void validateJobSchedule(@RequestParam(required = true) long jobId, @RequestBody(required = true) WorkSchedule workSchedule,
			@RequestParam(required = true) int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed, InvalidDataException, JobsBarrierException,
			RollBackException {
		ServiceLock.getInstance().lock();
		try {
			scheduleManagment.validateJobSchedule(jobId, workSchedule, personDays);
		} finally {
			ServiceLock.getInstance().unLock();
		}
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.setHierarchybSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setHierarchybSchedule(@RequestParam(required = true) long hierarchyId, @RequestBody(required = true) WorkTime workTime) throws JobScheduleFailed,
			HierarchyNodeNotFoundException, SchedulingException, InvalidDataException {
		ServiceLock.getInstance().lock();
		try {
			Set<ScheduleLog> changes = scheduleManagment.setHierarchybSchedule(hierarchyId, workTime);
			scheduleLogService.logSchedule(hierarchyId, NodeCategory.hierarchy, changes);
			removeUnApprovedHierarchybSchedule(hierarchyId);
		} finally {
			ServiceLock.getInstance().unLock();
		}
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.validateHierarchybSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void validateHierarchybSchedule(@RequestParam(required = true) long hierarchyId, @RequestBody(required = true) WorkTime workTime) throws JobScheduleFailed,
			HierarchyNodeNotFoundException, SchedulingException, InvalidDataException, RollBackException {
		ServiceLock.getInstance().lock();
		try {
			Set<ScheduleLog> changes = scheduleManagment.validateHierarchybSchedule(hierarchyId, workTime);
			scheduleLogService.logSchedule(hierarchyId, NodeCategory.hierarchy, changes);
		} finally {
			ServiceLock.getInstance().unLock();
		}
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.setJobGroupSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setJobGroupSchedule(@RequestParam long jobGrouId, @RequestBody(required = true) WorkSchedule workSchedule, @RequestParam(required = true) int personDays)
			throws JobScheduleFailed, SchedulingException, InvalidDataException, JobNotFoundException, JobsBarrierException {
		ServiceLock.getInstance().lock();
		try {
			Set<ScheduleLog> changes = scheduleManagment.setJobGroupSchedule(jobGrouId, workSchedule, personDays);
			scheduleLogService.logSchedule(jobGrouId, NodeCategory.jobgroup, changes);
			removeUnApprovedJobGroupSchedule(jobGrouId);
		} finally {
			ServiceLock.getInstance().unLock();
		}
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.validateJobGroupWorkSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void validateJobGroupWorkSchedule(@RequestParam long jobGrouId, @RequestBody(required = true) WorkSchedule workSchedule, @RequestParam(required = true) int personDays)
			throws JobScheduleFailed, SchedulingException, InvalidDataException, JobNotFoundException, JobsBarrierException, RollBackException {
		ServiceLock.getInstance().lock();
		try {
			scheduleManagment.validateJobGroupWorkSchedule(jobGrouId, workSchedule, personDays);
		} finally {
			ServiceLock.getInstance().unLock();
		}
	}

	// @RequestMapping(value =
	// ScheduleManagmentServiceNames.setJobGroupSchedule, method = {
	// RequestMethod.PUT, RequestMethod.POST })
	// public void setJobGroupSchedule(@RequestParam long jobGrouId,
	// @RequestBody(required = true) WorkTime workTime) throws
	// JobScheduleFailed,
	// SchedulingException, InvalidDataException {
	// scheduleManagment.setJobGroupSchedule(jobGrouId, workTime);
	// removeUnApprovedJobGroupSchedule(jobGrouId);
	// }

	@RequestMapping(value = ScheduleManagmentServiceNames.setUnApprovedJobSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setUnApprovedJobSchedule(@RequestParam(required = true) long jobId, @RequestBody(required = true) WorkSchedule workSchedule,
			@RequestParam(required = true) int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed {
		scheduleManagment.setUnApprovedJobSchedule(jobId, workSchedule, personDays);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.setUnApprovedHierarchybSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setUnApprovedHierarchybSchedule(@RequestParam(required = true) long hierarchyId, @RequestBody(required = true) WorkTime workTime) throws JobScheduleFailed,
			HierarchyNodeNotFoundException {
		scheduleManagment.setUnApprovedHierarchybSchedule(hierarchyId, workTime);
	}

	// @RequestMapping(value =
	// ScheduleManagmentServiceNames.setUnApprovedJobGroupSchedule, method = {
	// RequestMethod.PUT, RequestMethod.POST })
	// public void setUnApprovedJobGroupSchedule(@RequestParam long jobGrouId,
	// @RequestBody(required = true) WorkTime workTime) throws
	// JobScheduleFailed,
	// JobNotFoundException {
	// scheduleManagment.setUnApprovedJobGroupSchedule(jobGrouId, workTime);
	// }

	@RequestMapping(value = ScheduleManagmentServiceNames.setUnApprovedJobGroupSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setUnApprovedJobGroupSchedule(@RequestParam(required = true) long jobGrouId, @RequestBody(required = true) WorkSchedule workSchedule,
			@RequestParam(required = true) int personDays) throws JobScheduleFailed, JobNotFoundException {
		scheduleManagment.setUnApprovedJobGroupSchedule(jobGrouId, workSchedule, personDays);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.getUnApprovedHierarchySchedules, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getUnApprovedHierarchySchedules(String level, int pageNumber, int pageSize) throws JobNotFoundException {
		return scheduleManagment.getPendingForApprovelSchedules(level, pageNumber, pageSize);
	}

	// @RequestMapping(value =
	// ScheduleManagmentServiceNames.getUnApprovedJobGroupSchedules, method = {
	// RequestMethod.GET, RequestMethod.POST })
	// public PageItr<JobGroup> getUnApprovedJobGroupSchedules(int pageNumber,
	// int pageSize) throws JobNotFoundException {
	// return
	// scheduleManagment.getPendingForApprovelJobGroupSchedules(pageNumber,
	// pageSize);
	// }

	@RequestMapping(value = ScheduleManagmentServiceNames.getUnApprovedJobGroupSchedules, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> getPendingForApprovelJobGroupWorkSchedules(int pageNumber, int pageSize) throws JobNotFoundException {
		return scheduleManagment.getPendingForApprovelJobGroupWorkSchedules(pageNumber, pageSize);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.getUnApprovedJobSchedules, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getUnApprovedJobSchedules(int pageNumber, int pageSize) throws JobNotFoundException {
		return scheduleManagment.getPendingForApprovelJobSchedules(pageNumber, pageSize);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.removeUnApprovedJobSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removeUnApprovedJobSchedule(@RequestParam(required = true) long jobId) throws SchedulingException, JobNotFoundException, JobScheduleFailed {
		scheduleManagment.removeUnApprovedJobSchedule(jobId);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.removeUnApprovedHierarchybSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removeUnApprovedHierarchybSchedule(@RequestParam(required = true) long hierarchyId) throws JobScheduleFailed, HierarchyNodeNotFoundException {
		scheduleManagment.removeUnApprovedHierarchybSchedule(hierarchyId);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.removeUnApprovedJobGroupSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removeUnApprovedJobGroupSchedule(@RequestParam long jobGrouId) throws JobScheduleFailed {
		scheduleManagment.removeUnApprovedJobGroupSchedule(jobGrouId);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.rejectUnApprovedJobSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void rejectUnApprovedJobSchedule(@RequestParam(required = true) long jobId) throws SchedulingException, JobNotFoundException, JobScheduleFailed {
		scheduleManagment.rejectUnApprovedJobSchedule(jobId);
	}

	@RequestMapping(value = ScheduleManagmentServiceNames.rejectUnApprovedHierarchybSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void rejectUnApprovedHierarchybSchedule(@RequestParam(required = true) long hierarchyId) throws JobScheduleFailed, HierarchyNodeNotFoundException {
		scheduleManagment.rejectUnApprovedHierarchybSchedule(hierarchyId);
	}

	// @RequestMapping(value =
	// ScheduleManagmentServiceNames.rejectUnApprovedJobGroupSchedule, method =
	// { RequestMethod.PUT, RequestMethod.POST })
	// public void rejectUnApprovedJobGroupSchedule(@RequestParam long
	// jobGrouId) throws JobScheduleFailed {
	// scheduleManagment.rejectUnApprovedJobGroupSchedule(jobGrouId);
	// }

	@RequestMapping(value = ScheduleManagmentServiceNames.rejectUnApprovedJobGroupSchedule, method = { RequestMethod.PUT, RequestMethod.POST })
	public void rejectUnApprovedJobGroupSchedule(@RequestParam long jobGrouId) throws JobScheduleFailed {
		scheduleManagment.rejectUnApprovedJobGroupWorkSchedule(jobGrouId);
	}

}
