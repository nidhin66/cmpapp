package com.gvg.syena.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.services.DiaryManagementService;
import com.gvg.syena.core.api.services.diary.DiaryFilterProperties;
import com.gvg.syena.core.api.services.names.ProjectDiaryServiceNames;

@RestController
@RequestMapping(value = { "projectDiary" })
public class ProjectDiaryController {
	
	@Autowired
	DiaryManagementService diaryManagementService;
	
	@RequestMapping(value = ProjectDiaryServiceNames.createNewThread, method = { RequestMethod.POST })
	public DiaryThread createNewThread(@RequestBody(required = true) DiaryThread diaryThread) {
		return diaryManagementService.createNewThread(diaryThread);
	}
	
	@RequestMapping(value = ProjectDiaryServiceNames.addDiscussion, method = { RequestMethod.POST })
	public void addDiscussion(@RequestParam(required = true) long threadId, @RequestBody(required = true) ThreadDiscussion threadDiscussion){
		diaryManagementService.addDiscussion(threadId, threadDiscussion);
	}
	
	@RequestMapping(value = ProjectDiaryServiceNames.getLatestThreads, method = { RequestMethod.GET })
	public PageItr<DiaryThread> getLatestThreads(@RequestParam(required = true)int pageNumber,@RequestParam(required = true)int pageSize){
		return diaryManagementService.getLatestThreads(pageNumber, pageSize);
	}
	
	@RequestMapping(value = ProjectDiaryServiceNames.getDiscussions, method = { RequestMethod.GET })
	public PageItr<ThreadDiscussion> getDiscussions(@RequestParam(required = true)long threadId,@RequestParam(required = true)int pageNumber, @RequestParam(required = true)int pageSize){
		return diaryManagementService.getDiscussions(threadId, pageNumber, pageSize);
	}
	
	@RequestMapping(value = ProjectDiaryServiceNames.searchDiscussionThreads, method = {RequestMethod.POST })
	public PageItr<DiaryThread> searchDiscussionThreads( @RequestBody(required = true) DiaryFilterProperties filterProperties,@RequestParam(required = true)int pageNumber,@RequestParam(required = true)int pageSize) {
		return diaryManagementService.searchDiscussionThreads(filterProperties, pageNumber, pageSize);
	}

}
