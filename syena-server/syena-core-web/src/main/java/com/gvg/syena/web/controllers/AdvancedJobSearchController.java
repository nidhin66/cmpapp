package com.gvg.syena.web.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.services.names.SearchServiceNames;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;

@RestController
@RequestMapping(value = { "searchJob" })
public class AdvancedJobSearchController {

	private static final String nameSearchdefaultValue = "%";

	@Autowired
	private AdvancedJobSearch advancedJobSearch;

	@RequestMapping(value = SearchServiceNames.onGoingJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> onGoingJobs(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.getOnGoingJobs(date, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.completedJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> completedJobs(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.getCompletedJobs(date, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.plannedJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> plannedJobs(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.getPlannedJobs(date, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.delayedJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> delayedJobs(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.getDelayedJobs(date, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsInitiatedBy, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> jobsInitiatedBy(@RequestParam(required = true) String personId, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobSearch.getJobsInitiatedBy(personId, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsOwnedBy, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> jobsOwnedBy(@RequestParam(required = true) String personId, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobSearch.getJobsOwnedBy(personId, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsToBeScheduled, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> jobsToBeScheduled(@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.jobsToBeScheduled(jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsWithRevisedDates, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> jobsWithRevisedDates(@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.jobsWithRevisedDates(jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.nodeWithDelayed, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> nodeWithDelayed(String nodeLevel, @DateTimeFormat(iso = ISO.DATE) Date when, int delayedPercentage, ValueComparitor valueComparitor,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws HierarchyLevelNotFoundException {
		return advancedJobSearch.nodeWithDelayed(delayedPercentage, when, valueComparitor, nodeLevel, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.searchJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> searchJobs(@RequestBody(required = true) JobProperties jobSearchOption, @RequestParam(required = true) int pageNaumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.searchJobs(jobSearchOption, pageNaumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsInCriticalPath, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> jobsInCriticalPath(@RequestParam(required = true) long hierarchyNodeId,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.jobsInCriticalPath(hierarchyNodeId, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsStartInNDays, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<Job> jobsStartInNDays(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date fromDate,
			@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date toDate, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobSearch.jobsStartInNDays(fromDate, toDate, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsCompleteInNDays, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<Job> jobsCompleteInNDays(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date fromDate,
			@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date toDate, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobSearch.jobsCompleteInNDays(fromDate, toDate, jobName, pageNumber, pageSize);

	}
	
	@RequestMapping(value = SearchServiceNames.JobsInDelyedStartJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<Job> jobJobsInDelyedStartJobGroups(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) ValueComparitor valueComparitor,
			@RequestParam(required = true) float percentage, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobSearch.jobJobsInDelyedStartJobGroups(hierarchyNodeId, valueComparitor, percentage, jobName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.JobsInIncreasedDurationJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<Job> JobsInIncreasedDurationJobGroups(@RequestParam(required = true) long hierarchyNodeId,
			@RequestParam(required = true) ValueComparitor valueComparitor, @RequestParam(required = true) float percentage,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobSearch.JobsInIncreasedDurationJobGroups(hierarchyNodeId, valueComparitor, percentage, jobName, pageNumber, pageSize);
	}


}
