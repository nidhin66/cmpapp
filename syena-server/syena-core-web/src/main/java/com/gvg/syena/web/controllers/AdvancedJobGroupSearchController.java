package com.gvg.syena.web.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.services.names.SearchServiceNames;
import com.gvg.syena.core.api.services.serach.AdvancedJobGroupSearch;
import com.gvg.syena.core.api.services.serach.DateRange;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;

@RestController
@RequestMapping(value = { "searchJobGroup" })
public class AdvancedJobGroupSearchController {

	private static final String nameSearchdefaultValue = "%";
	@Autowired
	private AdvancedJobGroupSearch advancedJobGroupSearchService;

	@RequestMapping(value = SearchServiceNames.getOnGoingJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> getOnGoingJobGroups(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.getOnGoingJobGroups(date, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.completedJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> completedJobGroups(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.completedJobGroups(date, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.plannedJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> plannedJobGroups(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.plannedJobGroups(date, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.delayedJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> delayedJobGroups(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date date,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.delayedJobGroups(date, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsInitiatedBy, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> jobsInitiatedBy(@RequestParam(required = true) String personId,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.getJobsInitiatedBy(personId, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsOwnedBy, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> jobsOwnedBy(@RequestParam(required = true) String personId,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.getJobsOwnedBy(personId, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsToBeScheduled, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> jobsToBeScheduled(@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobsToBeScheduled(jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsWithRevisedDates, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> jobsWithRevisedDates(@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobsWithRevisedDates(jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.searchJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> searchJobs(@RequestBody(required = true) JobProperties jobSearchOption, @RequestParam(required = true) int pageNaumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.searchJobs(jobSearchOption, pageNaumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobGroupwithDelyedStartJobs, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<JobGroup> jobGroupwithDelyedStartJobs(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) ValueComparitor valueComparitor,
			@RequestParam(required = true) float percentage, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobGroupwithDelyedStartJobs(hierarchyNodeId, valueComparitor, percentage, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobGroupwithIncreasedDurationJobs, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<JobGroup> jobGroupwithIncreasedDurationJobs(@RequestParam(required = true) long hierarchyNodeId,
			@RequestParam(required = true) ValueComparitor valueComparitor, @RequestParam(required = true) float percentage,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobGroupwithIncreasedDurationJobs(hierarchyNodeId, valueComparitor, percentage, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobGroupsInCriticalPath, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> jobGroupsInCriticalPath(@RequestParam(required = true) long hierarchyNodeId,
			@RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobGroupsInCriticalPath(hierarchyNodeId, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsGroupsStartInNDays, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<JobGroup> jobsGroupsStartInNDays(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date fromDate,
			@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date toDate, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobsGroupsStartInNDays(fromDate, toDate, jobGroupName, pageNumber, pageSize);
	}

	@RequestMapping(value = SearchServiceNames.jobsGroupsCompleteInNDays, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<JobGroup> jobsGroupsCompleteInNDays(@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date fromDate,
			@RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date toDate, @RequestParam(required = false, defaultValue = nameSearchdefaultValue) String jobGroupName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return advancedJobGroupSearchService.jobsGroupsCompleteInNDays(fromDate, toDate, jobGroupName, pageNumber, pageSize);

	}
}
