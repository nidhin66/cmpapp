package com.gvg.syena.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.services.ApplicationService;
import com.gvg.syena.core.api.services.names.ApplicationServiceServiceNames;

@RestController
@RequestMapping(value = { "application" })
public class ApplicationController {

	@Autowired
	private ApplicationService applicationService;

	@RequestMapping(value = ApplicationServiceServiceNames.getPropertyValue, method = { RequestMethod.GET, RequestMethod.POST })
	public String getPropertyValue(@RequestParam(required = true) String property) {
		return applicationService.getPropertyValue(property);
	}
	
	@RequestMapping(value = ApplicationServiceServiceNames.getApplicationConfigs, method = { RequestMethod.GET })
	public List<ApplicationConfiguration> getApplicationConfigs() {
		return applicationService.getApplicationConfigs();
	}
}
