package com.gvg.syena.web.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.project.Project;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.DocIOException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.ImageIOException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.RemovalBarrierException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.services.HierarchyDoc;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.names.HierarchyManagementServiceNames;

@RestController
@RequestMapping(value = { "hierarchyManagement" })
public class HierarchyManagementController {

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@RequestMapping(value = HierarchyManagementServiceNames.createProject, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createProject(@RequestParam(required = true) Project project) {
		hierarchyManagement.createProject(project);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createHierarchyLevel, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createHierarchyLevel(@RequestBody(required = true) HierarchyLevel hierarchyLevel) {
		hierarchyManagement.createHierarchyLevel(hierarchyLevel);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getHierarchyLevels, method = { RequestMethod.GET, RequestMethod.POST })
	public HierarchyLevel getHierarchyLevels(@RequestParam(required = true) String hierarchyName) throws HierarchyLevelNotFoundException, ServiceLevelValidationException {
		return hierarchyManagement.getHierarchyLevels(hierarchyName);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createStandardHierarchyNodes, method = { RequestMethod.PUT, RequestMethod.POST })
	public StandardHierarchyNode[] createStandardHierarchyNodes(@RequestBody(required = true) StandardHierarchyNode[] hierarchyNodes, @RequestParam(required = true) String ownerId)
			throws ServiceLevelValidationException, PersonNotFoundException {
		return hierarchyManagement.createStandardHierarchyNodes(hierarchyNodes, ownerId);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createStandardHierarchyNode, method = { RequestMethod.PUT, RequestMethod.POST })
	public StandardHierarchyNode createStandardHierarchyNode(@RequestBody(required = true) StandardHierarchyNode hierarchyNode, @RequestParam(required = true) String ownerId)
			throws ServiceLevelValidationException, PersonNotFoundException {
		return hierarchyManagement.createStandardHierarchyNode(hierarchyNode, ownerId);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getAllStandardHierarchyNodes, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<StandardHierarchyNode<StandardHierarchyNode>> getAllStandardHierarchyNodes(@RequestParam(required = true) String hierarchyLevelName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws HierarchyLevelNotFoundException {
		return hierarchyManagement.getAllStandardHierarchyNodes(hierarchyLevelName, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getAllHierarchyNodes, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getAllHierarchyNodes(@RequestParam(required = true) String hierarchyLevelName, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws HierarchyLevelNotFoundException {
		return hierarchyManagement.getAllHierarchyNodes(hierarchyLevelName, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createHierarchyNode, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createHierarchyNode(@RequestBody(required = true) HierarchyNode hierarchyNode, @RequestParam(required = true) String owner) throws PersonNotFoundException,
			ServiceLevelValidationException {
		hierarchyManagement.createHierarchyNode(hierarchyNode, owner);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createHierarchyNodes, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createHierarchyNodes(@RequestBody(required = true) List<HierarchyNode> hierarchyNodes, @RequestParam(required = true) String owner) throws PersonNotFoundException,
			ServiceLevelValidationException {
		hierarchyManagement.createHierarchyNode(hierarchyNodes);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getHierarchyNodes, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getHierarchyNodes(@RequestParam(required = true) long[] nodeIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return hierarchyManagement.getHierarchyNodes(nodeIds, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getLinkedHierarchyNodes, method = { RequestMethod.POST, RequestMethod.GET })
	public PageItr<HierarchyNode> getLinkedHierarchyNodes(@RequestParam(required = true) long parentId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return hierarchyManagement.getLinkedHierarchyNodes(parentId, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getLinkedStandardHierarchyNodes, method = { RequestMethod.POST, RequestMethod.GET })
	public PageItr<StandardHierarchyNode> getLinkedStandardHierarchyNodes(@RequestParam(required = true) long parentId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return hierarchyManagement.getLinkedStandardHierarchyNodes(parentId, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.updateHierarchyNode, method = { RequestMethod.PUT, RequestMethod.POST })
	public void updateHierarchyNode(@RequestBody(required = true) List<HierarchyNode> hierarchyNodes) throws HierarchyNodeNotFoundException {
		hierarchyManagement.updateHierarchyNode(hierarchyNodes);

	}

	// @RequestMapping(value =
	// HierarchyManagementServiceNames.unlinkHierarchyNodes, method = {
	// RequestMethod.PUT, RequestMethod.POST })
	// public void unlinkHierarchyNodes(@RequestParam(required = true) long
	// parentNodeId, @RequestParam(required = true) long nodeId) throws
	// HierarchyNodeNotFoundException {
	// hierarchyManagement.unlinkHierarchyNodes(parentNodeId, nodeId);
	// }

	// @RequestMapping(value =
	// HierarchyManagementServiceNames.obsoleteHierarchyNode, method = {
	// RequestMethod.PUT, RequestMethod.POST })
	// public void obsoleteHierarchyNode(@RequestParam(required = true) long
	// hierarchyNodeId) throws HierarchyNodeNotFoundException {
	// hierarchyManagement.obsoleteHierarchyNode(hierarchyNodeId);
	//
	// }

	// @RequestMapping(value =
	// HierarchyManagementServiceNames.getObsoleteHierarchyNodes, method = {
	// RequestMethod.GET, RequestMethod.POST })
	// public PageItr<HierarchyNode>
	// getObsoleteHierarchyNodes(@RequestParam(required = true) int pageNumber,
	// @RequestParam(required = true) int pageSize) {
	// return hierarchyManagement.getObsoleteHierarchyNodes(pageNumber,
	// pageSize);
	// }

	@RequestMapping(value = HierarchyManagementServiceNames.searchHierarchyNodesInLevel, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> searchHierarchyNodesInLevel(@RequestParam(required = true) String hierarchyNodename, @RequestParam(required = true) String hierarchyLevelName,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws HierarchyLevelNotFoundException {
		return hierarchyManagement.searchHierarchyNodesInLevel(hierarchyNodename, hierarchyLevelName, pageNumber, pageSize);

	}

	@RequestMapping(value = HierarchyManagementServiceNames.searchStandardHierarchyNodesInLevel, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<StandardHierarchyNode> searchStandardHierarchyNodesInLevel(@RequestParam(required = true) String hierarchyNodename,
			@RequestParam(required = true) String hierarchyLevelName, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize)
			throws HierarchyLevelNotFoundException {
		return hierarchyManagement.searchStandardHierarchyNodesInLevel(hierarchyNodename, hierarchyLevelName, pageNumber, pageSize);

	}

	@RequestMapping(value = HierarchyManagementServiceNames.searchHierarchyNodesInChild, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> searchHierarchyNodesInChild(@RequestParam(required = true) String hierarchyNodename, @RequestParam(required = true) long parentId,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return hierarchyManagement.searchHierarchyNodesInChild(hierarchyNodename, parentId, pageNumber, pageSize);

	}

	@RequestMapping(value = HierarchyManagementServiceNames.getChildHierarchyNodeIds, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getChildHierarchyNodeIds(@RequestParam(required = true) long[] parentIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws ServiceLevelValidationException {
		return hierarchyManagement.getChildHierarchyNodes(parentIds, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getChildHierarchyNodes, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getChildHierarchyNodes(@RequestParam(required = true) long[] parentIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws ServiceLevelValidationException {
		return hierarchyManagement.getChildHierarchyNodes(parentIds, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getParentHierarchyNodes, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getParentHierarchyNodes(@RequestParam(required = true) long[] childIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws ServiceLevelValidationException {
		return hierarchyManagement.getParentHierarchyNodes(childIds, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.linkHierarchyNodes, method = { RequestMethod.PUT, RequestMethod.POST })
	public void linkHierarchyNodes(@RequestParam(required = true) long parentNodeId, @RequestParam(required = true) long... nodeIds) throws HierarchyNodeNotFoundException,
			AlreadyLinkedException {
		hierarchyManagement.linkHierarchyNodes(parentNodeId, nodeIds);

	}

	@RequestMapping(value = HierarchyManagementServiceNames.getHierarchyNodeIds, method = { RequestMethod.GET, RequestMethod.POST })
	public long[] getHierarchyNodeIds(@RequestParam(required = true) String hierarchyLevelName) throws ServiceLevelValidationException, HierarchyLevelNotFoundException {
		return hierarchyManagement.getHierarchyNodeIds(hierarchyLevelName);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.linkJobs, method = { RequestMethod.PUT, RequestMethod.POST })
	public void linkJobs(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) long... jobIds) throws HierarchyNodeNotFoundException {
		hierarchyManagement.linkJobs(hierarchyNodeId, jobIds);
	}

	// void removeLinkedJobs(long hierarchyNodeId, long... jobIds) throws
	// HierarchyNodeNotFoundException;
	@RequestMapping(value = HierarchyManagementServiceNames.removeLinkedJobs, method = { RequestMethod.DELETE, RequestMethod.POST })
	public void removeLinkedJobs(@RequestParam(required = true) long... jobIds) throws HierarchyNodeNotFoundException {
		hierarchyManagement.removeLinkedJobs(jobIds);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.removeLinkedJobGroups, method = { RequestMethod.DELETE, RequestMethod.POST })
	public void removeLinkedJobGroups(@RequestParam(required = true) long... jobGroupIds) throws HierarchyNodeNotFoundException, RemovalBarrierException {
		hierarchyManagement.removeLinkedJobGroups(jobGroupIds);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getLinkedJob, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getLinkedJob(@RequestParam(required = true) long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		return hierarchyManagement.getLinkedJob(hierarchyNodeId);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getLinkedJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup<Job>> getLinkedJobGroups(@RequestParam(required = true) long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		return hierarchyManagement.getLinkedJobGroups(hierarchyNodeId);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getChildStandardHierarchyNode, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<StandardHierarchyNode<StandardHierarchyNode>> getChildStandardHierarchyNode(@RequestParam(required = true) long[] parentIds, int pageNumber, int pageSize) {
		return hierarchyManagement.getChildStandardHierarchyNode(parentIds, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.saveAsHierarchyNode, method = { RequestMethod.PUT, RequestMethod.POST })
	public void saveAsHierarchyNode(@RequestParam(required = true) long parentId, @RequestParam(required = true) String ownerId) throws PersonNotFoundException,
			ServiceLevelValidationException, HierarchyNodeNotFoundException, AlreadyLinkedException {
		hierarchyManagement.saveAsHierarchyNode(parentId, ownerId);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createAndlinkJobGroups, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<JobGroup<Job>> createAndlinkJobGroups(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) long... standardJobGroupIds)
			throws HierarchyNodeNotFoundException, JobNotFoundException {
		return hierarchyManagement.createAndlinkJobGroups(hierarchyNodeId, standardJobGroupIds);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createAndlinkJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<Job> createAndlinkJob(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) long... standardJobIds)
			throws HierarchyNodeNotFoundException, JobNotFoundException, InvalidDataException {
		return hierarchyManagement.createAndlinkJob(hierarchyNodeId, standardJobIds);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createAndLinkHierarchyNodes, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createAndLinkHierarchyNodes(@RequestParam(required = true) long parentNodeId, @RequestParam(required = true) long... standardHierarchyNodeIds)
			throws HierarchyNodeNotFoundException, AlreadyLinkedException {
		hierarchyManagement.createAndLinkHierarchyNodes(parentNodeId, standardHierarchyNodeIds);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getHierarchyNodeImage, method = { RequestMethod.GET, RequestMethod.POST })
	public HierarchyDoc getHierarchyNodeImage(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) String imageName,
			@RequestParam(required = true) DocumentType documentType) throws HierarchyNodeNotFoundException, ImageIOException {
		return hierarchyManagement.getHierarchyNodeImage(hierarchyNodeId, imageName, documentType);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.uploadHierarchyNodeImage, method = { RequestMethod.PUT, RequestMethod.POST })
	public void uploadHierarchyNodeImage(@RequestParam(required = true) long hierarchyNodeId, @RequestBody(required = true) HierarchyDoc image)
			throws HierarchyNodeNotFoundException, ImageIOException, FileNotFoundException, IOException {
		hierarchyManagement.uploadHierarchyNodeImage(hierarchyNodeId, image.getDocumentName(), /*
																								 * file
																								 * .
																								 * getBytes
																								 * (
																								 * )
																								 */image.getContent(), image.getDocumentType());
	}

	@RequestMapping(value = HierarchyManagementServiceNames.uploadHierarchyNodeDoc, method = { RequestMethod.PUT, RequestMethod.POST })
	public void uploadHierarchyDoc(@RequestParam(required = true) long hierarchyNodeId, @RequestBody(required = true) HierarchyDoc hierarchyDoc)
			throws HierarchyNodeNotFoundException, DocIOException, FileNotFoundException, IOException {
		hierarchyManagement.uploadHierarchyNodeDoc(hierarchyNodeId, hierarchyDoc.getDocumentName(), /*
																									 * file
																									 * .
																									 * getBytes
																									 * (
																									 * )
																									 */hierarchyDoc.getContent(), hierarchyDoc.getDocumentType());
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getHierarchyNodeDocs, method = { RequestMethod.GET, RequestMethod.POST })
	public List<HierarchyDoc> getHierarchyNodeDocs(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) DocumentType documentType)
			throws HierarchyNodeNotFoundException {
		return hierarchyManagement.getHierarchyNodeDocs(hierarchyNodeId, documentType);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.getJobGroupssByConnectedNodeIdsOtherThan, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getJobByConnectedNodeIdsOtherThan(@RequestParam(required = true) long parentIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws HierarchyNodeNotFoundException {
		return hierarchyManagement.getJobsByConnectedInchildNodeIdsOtherThan(parentIds, pageNumber, pageSize);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createPuchListJob, method = { RequestMethod.GET, RequestMethod.POST })
	public Job createPuchListJob(@RequestParam(required = true) long parentIds, @RequestBody Job job) throws HierarchyNodeNotFoundException, InvalidDataException {
		return hierarchyManagement.createPuchListJob(parentIds, job);
	}

	@RequestMapping(value = HierarchyManagementServiceNames.createPuchListJobGroup, method = { RequestMethod.GET, RequestMethod.POST })
	public JobGroup<Job> createPuchListJobGroup(@RequestParam(required = true) long parentIds, @RequestBody JobGroup<Job> jobGroup) throws HierarchyNodeNotFoundException,
			InvalidDataException {
		return hierarchyManagement.createPuchListJobGroup(parentIds, jobGroup);
	}

	// void removeHierarchyNodeLink(long hierarchyNodeId) throws
	// HierarchyNodeNotFoundException, HierarchyNodeRemovalBarrierException;
	@RequestMapping(value = HierarchyManagementServiceNames.removeHierarchyNodeLink, method = { RequestMethod.DELETE, RequestMethod.POST })
	public void removeHierarchyNodeLink(@RequestParam(required = true) long[] hierarchyNodeIds) throws HierarchyNodeNotFoundException, RemovalBarrierException {
		hierarchyManagement.removeHierarchyNodeLink(hierarchyNodeIds);
	}
}
