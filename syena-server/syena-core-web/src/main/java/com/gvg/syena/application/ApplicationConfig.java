package com.gvg.syena.application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.audit.AuditorAwareImpl;

@Configuration
@ImportResource("classpath*:/dao-config.xml")
@EnableJpaRepositories(basePackages = { "com.gvg.syena.core.datarepository" })
@EnableWebMvc
@ComponentScan(basePackages = { "com.gvg.syena.core.services", "com.gvg.syena.web.controllers" })
@EnableJpaAuditing
// @EnableCaching
public class ApplicationConfig {

	@Bean
	public AuditorAware<Person> auditorProvider() {
		return new AuditorAwareImpl();
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(mapper);
		return converter;
	}

}
