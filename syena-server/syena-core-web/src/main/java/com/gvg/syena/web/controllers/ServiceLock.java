package com.gvg.syena.web.controllers;

import java.util.concurrent.locks.ReentrantLock;

public class ServiceLock {

	private static ServiceLock me;
	private ReentrantLock lock;

	private ServiceLock() {
		this.lock = new ReentrantLock();
	}

	public static ServiceLock getInstance() {
		if (me == null) {
			me = new ServiceLock();
		}
		return me;
	}

	public void lock() {
		lock.lock();

	}

	public void unLock() {
		lock.unlock();

	}

}
