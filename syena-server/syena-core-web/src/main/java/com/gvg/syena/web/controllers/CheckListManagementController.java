package com.gvg.syena.web.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;
import com.gvg.syena.core.api.exception.CheckListNotFound;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ValidationException;
import com.gvg.syena.core.api.services.CheckListManagement;
import com.gvg.syena.core.api.services.names.CheckListManagementServiceNames;

@RestController
@RequestMapping(value = { "checkListManagement" })
public class CheckListManagementController {

	@Autowired
	private CheckListManagement checkListManagement;

	// @RequestMapping(value = CheckListManagementServiceNames.createCheckLists,
	// method = { RequestMethod.PUT, RequestMethod.POST }, consumes = {
	// MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	// public void createCheckLists(@RequestParam(required = true)
	// List<CheckList> checkLists) {
	// checkListManagement.createCheckLists(checkLists);
	// }

	@RequestMapping(value = CheckListManagementServiceNames.createCheckLists, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createCheckLists(@RequestBody List<CheckList> checkLists) throws ValidationException {
		checkListManagement.createCheckLists(checkLists);
	}

	@RequestMapping(value = CheckListManagementServiceNames.addCheckListItemsInCheckList, method = { RequestMethod.PUT, RequestMethod.POST })
	public void addCheckListItemsInCheckList(@RequestParam(required = true) long checkListId, @RequestBody(required = true) List<CheckListItem> checkListItems)
			throws CheckListNotFound {
		checkListManagement.addCheckListItemsInCheckList(checkListId, checkListItems);

	}

	@RequestMapping(value = CheckListManagementServiceNames.updateCheckList, method = { RequestMethod.PUT, RequestMethod.POST })
	public CheckListItem updateCheckList(@RequestParam(required = true) long checkListId, @RequestParam(required = true) long checkListItemId,
			@RequestParam(required = true) boolean status, @RequestParam(required = true) @DateTimeFormat(iso = ISO.DATE) Date completedDate,
			@RequestParam(required = true) String completedPersonId) throws CheckListNotFound, PersonNotFoundException, ValidationException, InvalidDataException {
		return checkListManagement.updateCheckList(checkListId, checkListItemId, status, completedDate, completedPersonId);
	}

	@RequestMapping(value = CheckListManagementServiceNames.getAllStandardCheckList, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> getAllStandardCheckList(@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return checkListManagement.getAllStandardCheckList(pageNumber, pageSize);
	}

	@RequestMapping(value = CheckListManagementServiceNames.getAllCheckList, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> getAllCheckList(@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return checkListManagement.getAllCheckList(pageNumber, pageSize);
	}

	@RequestMapping(value = CheckListManagementServiceNames.getCheckListByParent, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> getCheckListByParent(@RequestParam(required = true) long parentId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return checkListManagement.getCheckListByParent(parentId, pageNumber, pageSize);
	}

	@RequestMapping(value = CheckListManagementServiceNames.findInStandardCheckList, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> findInStandardCheckList(@RequestParam(required = true) String name, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return checkListManagement.findInStandardCheckList(name, pageNumber, pageSize);
	}

	@RequestMapping(value = CheckListManagementServiceNames.findInCheckList, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> findInCheckList(@RequestParam(required = true) String name, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return checkListManagement.findInCheckList(name, pageNumber, pageSize);
	}

	@RequestMapping(value = CheckListManagementServiceNames.findInCheckListWithParent, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> findInCheckListWithParent(@RequestParam(required = true) long parentId, @RequestParam(required = true) String name,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return checkListManagement.findInCheckListWithParent(parentId, name, pageNumber, pageSize);
	}

}
