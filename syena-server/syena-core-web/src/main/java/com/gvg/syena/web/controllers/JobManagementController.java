package com.gvg.syena.web.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.api.services.names.JobManagementServiceNames;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;

@RestController
@RequestMapping(value = { "jobManagement" })
public class JobManagementController {

	@Autowired
	private JobManagement jobManagement;

	@RequestMapping(value = JobManagementServiceNames.createStandardJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createStandardJob(@RequestBody(required = true) StandardJob job) throws InvalidDataException {
		jobManagement.createStandardJob(job);
	}

	@RequestMapping(value = JobManagementServiceNames.createStandardJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createStandardJobGroup(@RequestBody(required = true) JobGroup<StandardJob> jobGroup) throws InvalidDataException {
		jobManagement.createStandardJobGroup(jobGroup);
	}

	@RequestMapping(value = JobManagementServiceNames.getStandardJob, method = { RequestMethod.GET, RequestMethod.POST })
	public StandardJob getStandardJob(@RequestParam(required = true) long jobId) {
		return jobManagement.getStandardJob(jobId);
	}

	@RequestMapping(value = JobManagementServiceNames.updateStandardJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public void updateStandardJob(@RequestBody(required = true) StandardJob job) throws JobNotFoundException, InvalidDataException {
		jobManagement.updateStandardJob(job);
	}

	@RequestMapping(value = JobManagementServiceNames.updateStandardJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public void updateStandardJobGroup(@RequestBody(required = true) JobGroup<StandardJob> jobGroup) throws JobNotFoundException, InvalidDataException {
		jobManagement.updateStandardJobGroup(jobGroup);
	}

	@RequestMapping(value = JobManagementServiceNames.addStandardJobToStandardJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public void addStandardJobToStandardJobGroup(@RequestParam(required = true) long jobId, @RequestParam(required = true) long jobGroupId) throws JobNotFoundException {
		jobManagement.addStandardJobToStandardJobGroup(jobId, jobGroupId);
	}

	@RequestMapping(value = JobManagementServiceNames.createJobFromStandardJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public Job createJobFromStandardJob(@RequestParam(required = true) long standardJobId) throws JobNotFoundException {
		return jobManagement.createJobFromStandardJob(standardJobId);
	}

	// @RequestMapping(value = JobManagementServiceNames.createJobAndConnect,
	// method = { RequestMethod.PUT, RequestMethod.POST })
	// public Job createJobAndConnect(long hierarchyNodeId,
	// @RequestBody(required = true) Job job) {
	// return jobManagement.createJobAndConnect(hierarchyNodeId, job);
	// }
	//
	// @RequestMapping(value =
	// JobManagementServiceNames.createJobGroupAndConnect, method = {
	// RequestMethod.PUT, RequestMethod.POST })
	// public JobGroup createJobGroupAndConnect(long hierarchyNodeId,
	// @RequestBody(required = true) JobGroup jobGroup) {
	// return jobManagement.createJobGroupAndConnect(hierarchyNodeId, jobGroup);
	// }

	@RequestMapping(value = JobManagementServiceNames.getJob, method = { RequestMethod.GET, RequestMethod.POST })
	public Job getJob(@RequestParam(required = true) long jobId) {
		return jobManagement.getJob(jobId);
	}

	@RequestMapping(value = JobManagementServiceNames.getJobsInJobGroup, method = { RequestMethod.GET, RequestMethod.POST })
	public Set<Job> getJobsInJobGroup(@RequestParam(required = true) long jobGroupId) throws JobNotFoundException {
		return jobManagement.getJobsInJobGroup(jobGroupId);
	}
	
	@RequestMapping(value = JobManagementServiceNames.getStandardJobInJobGroup, method = { RequestMethod.GET, RequestMethod.POST })
	public Set<StandardJob> getStandardJobInJobGroup(@RequestParam(required = true) long jobGroupId) throws JobNotFoundException {
		return jobManagement.getStandardJobInJobGroup(jobGroupId);
	}

	@RequestMapping(value = JobManagementServiceNames.updateJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public Job updateJob(@RequestBody(required = true) Job job) throws JobNotFoundException, InvalidDataException {
		return jobManagement.updateJob(job);
	}

	@RequestMapping(value = JobManagementServiceNames.createJobGroupFromStandardJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<JobGroup<Job>> createJobGroupFromStandardJobGroup(@RequestParam(required = true) long standardJobGroupId) throws JobNotFoundException {
		return jobManagement.createJobGroupFromStandardJobGroup(standardJobGroupId);
	}

	@RequestMapping(value = JobManagementServiceNames.createJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createJobGroup(@RequestBody(required = true) JobGroup<Job> jobGroup) throws InvalidDataException {
		jobManagement.createJobGroup(jobGroup);
	}

	@RequestMapping(value = JobManagementServiceNames.updateJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public void updateJobGroup(@RequestBody(required = true) JobGroup<Job> jobGroup) throws JobNotFoundException, InvalidDataException {
		jobManagement.updateJobGroup(jobGroup);
	}

	@RequestMapping(value = JobManagementServiceNames.addJobToJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public void addJobToJobGroup(@RequestParam(required = true) long jobId, @RequestParam(required = true) long jobGroupId) throws JobNotFoundException {
		jobManagement.addJobToJobGroup(jobId, jobGroupId);
	}

	@RequestMapping(value = JobManagementServiceNames.getAllStandardJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<StandardJob> getAllStandardJobs(@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return jobManagement.getAllStandardJobs(pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getAllJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getAllJobs(@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return jobManagement.getAllJobs(pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getJobs(@RequestParam(required = true) long[] jobIds, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return jobManagement.getJobs(jobIds, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getAllStandardJobGroup, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> getAllStandardJobGroup(@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return jobManagement.getAllStandardJobGroup(pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getAllJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> getAllJobGroups(@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return jobManagement.getAllJobGroups(pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.abandonJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public void abandonJob(@RequestParam(required = true) long jobId) throws JobNotFoundException {
		jobManagement.abandonJob(jobId);
	}

	@RequestMapping(value = JobManagementServiceNames.abandonJobGrop, method = { RequestMethod.PUT, RequestMethod.POST })
	public void abandonJobGrop(@RequestParam(required = true) long jogGroupId) throws JobNotFoundException {
		jobManagement.abandonJobGrop(jogGroupId);
	}

	@RequestMapping(value = JobManagementServiceNames.createAndLinkFromStandardCheckListWithTypeToJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<CheckList> createAndLinkFromStandardCheckListWithTypeToJob(@RequestParam(required = true) long jobId, @RequestParam(required = true) CheckListType checkListType,
			@RequestParam(required = true) long... standardCheckListItems) throws JobNotFoundException {
		return jobManagement.createAndLinkFromStandardCheckListToJob(jobId, checkListType, standardCheckListItems);
	}

	@RequestMapping(value = JobManagementServiceNames.createAndLinkFromStandardCheckListWithTypeToJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<CheckList> createAndLinkFromStandardCheckListWithTypeToJobGroup(@RequestParam(required = true) long jobGroupId,
			@RequestParam(required = true) CheckListType checkListType, @RequestParam(required = true) long[] standardCheckListItems) throws JobNotFoundException {
		return jobManagement.createAndLinkFromStandardCheckListToJobGroup(jobGroupId, checkListType, standardCheckListItems);
	}

	@RequestMapping(value = JobManagementServiceNames.createAndLinkFromStandardCheckListToJob, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<CheckList> createAndLinkFromStandardCheckListToJob(@RequestParam(required = true) long jobId, @RequestParam(required = true) long... standardCheckListItems)
			throws JobNotFoundException {
		return jobManagement.createAndLinkFromStandardCheckListToJob(jobId, standardCheckListItems);
	}

	@RequestMapping(value = JobManagementServiceNames.createAndLinkFromStandardCheckListToJobGroup, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<CheckList> createAndLinkFromStandardCheckListToJobGroup(@RequestParam(required = true) long jobGroupId,
			@RequestParam(required = true) long... standardCheckListItems) throws JobNotFoundException {
		return jobManagement.createAndLinkFromStandardCheckListToJobGroup(jobGroupId, standardCheckListItems);
	}

	@RequestMapping(value = JobManagementServiceNames.getCheckListsOfJob, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> getCheckListsOfJob(@RequestParam(required = true) long jobId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws JobNotFoundException {
		return jobManagement.getCheckListsOfJob(jobId, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getCheckListsOfJobGroup, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<CheckList> getCheckListsOfJobGroup(@RequestParam(required = true) long jobId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws JobNotFoundException {
		return jobManagement.getCheckListsOfJobGroup(jobId, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.unlinkCheckList, method = { RequestMethod.PUT, RequestMethod.POST })
	public void unlinkCheckListItem(@RequestParam(required = true) long jobId, @RequestParam(required = true) long... checkListItemId) throws JobNotFoundException {
		jobManagement.unlinkCheckList(jobId, checkListItemId);
	}

	@RequestMapping(value = JobManagementServiceNames.searchJobWithProperties, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> searchJobWithProperties(@RequestBody(required = true) JobProperties jobSearchOptions, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return jobManagement.searchJobWithProperties(jobSearchOptions, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchJobGroupWithProperties, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> searchJobGroupWithProperties(@RequestBody(required = true) JobGroupProperties jobGroupProperties, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return jobManagement.searchJobGroupWithProperties(jobGroupProperties, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getPredecessorJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getPredecessorJobs(@RequestParam(required = true) long[] jobIds, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize)
			throws JobNotFoundException {
		return jobManagement.getPredecessorJobs(jobIds, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getSuccessorJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getSuccessorJobs(@RequestParam(required = true) long[] jobIds, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize)
			throws JobNotFoundException {
		return jobManagement.getSuccessorJobs(jobIds, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getSuccessorJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> getSuccessorJobGroups(@RequestParam(required = true) long[] jobGroupIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws JobNotFoundException {
		return jobManagement.getSuccessorJobGroups(jobGroupIds, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getPredecessorJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> getPredecessorJobGroups(@RequestParam(required = true) long[] jobGroupIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws JobNotFoundException {
		return jobManagement.getPredecessorJobGroups(jobGroupIds, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getConnectedHierarchyNode_JoB, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getConnectedHierarchyNode(@RequestParam(required = true) long[] jobIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return jobManagement.getConnectedHierarchyNode(jobIds, pageNumber, pageSize);

	}
	
	@RequestMapping(value = JobManagementServiceNames.getConnectedHierarchyNode_JobGroup, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getConnectedHierarchyNodeJobGroup(@RequestParam(required = true) long[] jobGroupIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return jobManagement.getConnectedHierarchyNodeJobGroup(jobGroupIds, pageNumber, pageSize);

	}

	@RequestMapping(value = JobManagementServiceNames.getJobsByConnectedNodeIds, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getJobsByConnectedNodeIds(@RequestParam(required = true) long[] parentIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return jobManagement.getJobsByConnectedNodeIds(parentIds, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getJobGroupsByConnectedNodeIds, method = { RequestMethod.GET, RequestMethod.POST })
	private PageItr<JobGroup<Job>> getJobGroupssByConnectedNodeIds(@RequestParam(required = true) long[] parentIds, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) {
		return jobManagement.getJobGroupssByConnectedNodeIds(parentIds, pageNumber, pageSize);
	}

}
