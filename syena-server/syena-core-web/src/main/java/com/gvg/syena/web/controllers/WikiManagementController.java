package com.gvg.syena.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.wiki.HierarchyItemWiki;
import com.gvg.syena.core.api.services.WikiManagementService;
import com.gvg.syena.core.api.services.names.WikiManagementServiceNames;

@RestController
@RequestMapping(value = { "wiki" })
public class WikiManagementController {
	
	@Autowired
	WikiManagementService wikiManagementService;
	
	@RequestMapping(value = WikiManagementServiceNames.createOrUpdateWiki, method = { RequestMethod.POST })
	public void createOrUpdateWiki(@RequestBody(required = true) HierarchyItemWiki wiki) {
		wikiManagementService.createOrUpdateWiki(wiki);
	}
	
	@RequestMapping(value = WikiManagementServiceNames.getProjectWiki, method = { RequestMethod.GET })
	public HierarchyItemWiki getProjectWiki(@RequestParam(required = true) long hierarchyNodeId){
		return wikiManagementService.getProjectWiki(hierarchyNodeId);
	}


}
