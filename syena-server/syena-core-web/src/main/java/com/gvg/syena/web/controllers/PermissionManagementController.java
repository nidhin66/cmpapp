package com.gvg.syena.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;
import com.gvg.syena.core.api.services.PermisssionManagmnet;
import com.gvg.syena.core.api.services.names.PermissionManagementServiceNames;

@RestController
@RequestMapping(value = { "permissionManagement" })
public class PermissionManagementController {

	@Autowired
	private PermisssionManagmnet permisssionManagmnet;

	@RequestMapping(value = PermissionManagementServiceNames.setPermissionUser, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setPermissionUser(@RequestParam(required = true) String userId, @RequestParam(required = true) ActivityArea activityArea,
			@RequestParam(required = true) List<ActivityOperation> activityOperation) throws PersonNotFoundException {
		permisssionManagmnet.setPermissionUser(userId, activityArea, activityOperation);
	}

	@RequestMapping(value = PermissionManagementServiceNames.setPermissionRole, method = { RequestMethod.PUT, RequestMethod.POST })
	public void setPermissionRole(@RequestParam(required = true) String roleId, @RequestParam(required = true) ActivityArea activityArea,
			@RequestParam(required = true) List<ActivityOperation> activityOperations) {
		permisssionManagmnet.setPermissionRole(roleId, activityArea, activityOperations);

	}

	@RequestMapping(value = PermissionManagementServiceNames.checkPermission, method = { RequestMethod.PUT, RequestMethod.POST })
	public boolean checkPermission(@RequestParam(required = true) String userId, @RequestParam(required = true) String departmentId,
			@RequestParam(required = true) ActivityArea activityArea, @RequestParam(required = true) ActivityOperation activityOperations) throws PersonNotFoundException {
		return permisssionManagmnet.checkPermission(userId, departmentId, activityArea, activityOperations);
	}

}
