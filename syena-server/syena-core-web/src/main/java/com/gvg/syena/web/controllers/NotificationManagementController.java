package com.gvg.syena.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.services.NotificationManagementService;
import com.gvg.syena.core.api.services.names.NotificationManagementServiceNames;
import com.gvg.syena.core.api.services.notification.Notification;
import com.gvg.syena.core.api.services.notification.NotificationType;

@RestController
@RequestMapping(value = { "notificationManagement" })
public class NotificationManagementController {
	
	@Autowired
	NotificationManagementService notificationManagementService;
	
	@RequestMapping(value = NotificationManagementServiceNames.getNotifications, method = { RequestMethod.GET })
	public PageItr<Notification> getNotifications(@RequestParam(required = true)String userId,@RequestParam(required = true) NotificationType notificationType, @RequestParam(required = true) int pageNumber,@RequestParam(required = true) int pageSize){
		return notificationManagementService.getNotifications(userId,notificationType ,pageNumber, pageSize);
	}
	
	@RequestMapping(value = NotificationManagementServiceNames.getNotificationCount, method = { RequestMethod.GET })
	public long getNotificationCount(@RequestParam(required = true)String userId){
		return notificationManagementService.getNotificationCount(userId);
	}
	
}
