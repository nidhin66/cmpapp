package com.gvg.syena.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.exception.LoginFailedException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.services.UserManagemnet;
import com.gvg.syena.core.api.services.names.OrganizationManagemnetServiceNames;

@RestController
@RequestMapping(value = { "userManagement" })
public class UserManagementController {
	@Autowired
	private UserManagemnet userManagemnet;

	@RequestMapping(value = OrganizationManagemnetServiceNames.createPerson, method = { RequestMethod.PUT, RequestMethod.POST })
	public void createPerson(@RequestParam(required = true) Person person) {
		userManagemnet.createPerson(person);
	}

	@RequestMapping(value = OrganizationManagemnetServiceNames.searchPerson, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Person> searchPerson(@RequestParam(required = true) String personName, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) {
		return userManagemnet.searchPerson(personName, pageNumber, pageSize);

	}

	@RequestMapping(value = OrganizationManagemnetServiceNames.login, method = { RequestMethod.GET, RequestMethod.POST })
	private void loginUser(@RequestParam(required = true) String userId, @RequestBody(required = true) String password) throws PersonNotFoundException, LoginFailedException {
		userManagemnet.loginUser(userId, password);
	}

	@RequestMapping(value = OrganizationManagemnetServiceNames.getPerson, method = { RequestMethod.GET, RequestMethod.POST })
	private Person getPerson(@RequestParam(required = true) String userId) throws PersonNotFoundException {
		return userManagemnet.getPerson(userId);
	}
}
