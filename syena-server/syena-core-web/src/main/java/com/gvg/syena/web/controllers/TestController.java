package com.gvg.syena.web.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.services.dataupload.DataUpLoader;
import com.gvg.syena.core.services.dataupload.TestDataInsertService;

@RestController
@RequestMapping(value = { "test" })
public class TestController {

	@Autowired
	public TestDataInsertService testDataInsertService;
	
	@Autowired
	public DataUpLoader dataUpLoad;

	@Autowired
	public HierarchyManagement hierarchyManagement;

	@Autowired
	public PersonRepository personRepository;

	private int projectId = 1;

	Person owner = null;

	private final String filePath = "/MappedData_DataUpLoad1.xlsx";
	
	@RequestMapping(value = "loaddata", method = { RequestMethod.PUT, RequestMethod.POST })
	public String loaddata() throws IOException, AlreadyLinkedException, PersonNotFoundException, ServiceLevelValidationException, HierarchyNodeNotFoundException, HierarchyLevelNotFoundException {
		uploadHierarchyLevel();
		uploadStandardHierarchyNodes();
		addStandardHierarchyNodesToParoject();
		uploadJobAndTask();
		return "Success";
	}

	@RequestMapping(value = "insertData", method = { RequestMethod.PUT, RequestMethod.POST })
	public void insertData(@RequestParam(required = true) int plant, @RequestParam(required = true) int unit, @RequestParam(required = true) int system, @RequestParam(required = true) int substem,
			@RequestParam(required = true) int loopEqu) {
		testDataInsertService.insertData(plant, unit, system, substem, loopEqu);
	}

	@RequestMapping(value = "addJob", method = { RequestMethod.PUT, RequestMethod.POST })
	public void addJob(@RequestParam(required = true) String levelName, @RequestParam(required = true) int pagenumber, @RequestParam(required = true) int num, @RequestParam(required = true) String id) {
		testDataInsertService.addJob(levelName, pagenumber, num, id);
	}
	private void uploadHierarchyLevel() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filePath);
		dataUpLoad.uploadHierarchyLevel(in);
	}

	private void uploadStandardHierarchyNodes() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filePath);
		dataUpLoad.uploadStandardHierarchyNodes(in);
	}

	private void addStandardHierarchyNodesToParoject() throws PersonNotFoundException, ServiceLevelValidationException, HierarchyNodeNotFoundException, AlreadyLinkedException,
			HierarchyLevelNotFoundException {
		Person person = new Person("GVG001");
		personRepository.save(person);
		String hierarchyLevel = "Plant";
		List<StandardHierarchyNode<StandardHierarchyNode>> topNodes = hierarchyManagement.getAllStandardHierarchyNodes(hierarchyLevel, 0, 1).getContent();
		for (StandardHierarchyNode standardHierarchyNode : topNodes) {
			hierarchyManagement.saveAsHierarchyNode(standardHierarchyNode.getId(), person.getId());
		}

	}

	private void uploadJobAndTask() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filePath);
		dataUpLoad.uploadJobAndTask(in);
	}
}
