package com.gvg.syena.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.NotScheduledException;
import com.gvg.syena.core.api.services.names.CriticalPathServiceNames;
import com.gvg.syena.core.services.criticalpath.CriticalAndNonCriticalPath;
import com.gvg.syena.core.services.criticalpath.CriticalPathFinder;

@RestController
@RequestMapping(value = { "criticalPath" })
public class CriticalPathController {

	@Autowired
	private CriticalPathFinder criticalPathFinder;

	// @RequestMapping(value = CriticalPathServiceNames.findCriticalPath, method
	// = { RequestMethod.GET, RequestMethod.POST })
	// public List<JobData> findCriticalPath(@RequestParam(required = true) long
	// hierarchyNodeId) throws HierarchyNodeNotFoundException,
	// NotScheduledException {
	// return criticalPathFinder.findCriticalPath(hierarchyNodeId);
	// }
	//
	// @RequestMapping(value = CriticalPathServiceNames.findNonCriticalPath,
	// method = { RequestMethod.GET, RequestMethod.POST })
	// public List<JobData> findNonCriticalPath(@RequestParam(required = true)
	// long hierarchyNodeId) throws HierarchyNodeNotFoundException,
	// NotScheduledException {
	// return criticalPathFinder.findNonCriticalPath(hierarchyNodeId);
	// }
	//
	@RequestMapping(value = CriticalPathServiceNames.findCriticalAndNonCriticalPath, method = { RequestMethod.GET, RequestMethod.POST })
	public CriticalAndNonCriticalPath findCriticalAndNonCriticalPath(@RequestParam(required = true) long hierarchyNodeId) throws HierarchyNodeNotFoundException, NotScheduledException {
		return criticalPathFinder.findCriticalAndNonCriticalPath(hierarchyNodeId);
	}


}
