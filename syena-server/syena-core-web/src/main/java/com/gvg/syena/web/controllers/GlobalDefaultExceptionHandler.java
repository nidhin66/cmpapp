package com.gvg.syena.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.MessageNotFoundException;
import com.gvg.syena.core.api.exception.RollBackException;
import com.gvg.syena.core.api.services.MessageService;

@ControllerAdvice("com.gvg.syena.web.controllers")
class GlobalDefaultExceptionHandler {

	@Autowired
	private MessageService messageService;

	@ExceptionHandler(value = ApplicationException.class)
	@ResponseBody
	public Message applicationExceptionErrorHandler(ApplicationException e) throws Exception {
		e.printStackTrace();
		try {
			return messageService.getMessage(e.getMessageId());
		} catch (MessageNotFoundException em) {
			Message message = new Message();
			message.setMessage(e.getMessage());
			message.setDescription(e.getMessageString());
			return message;
		}
	}

	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	public Message defaultErrorHandler(Exception e) throws Exception {
		e.printStackTrace();
		Message message = new Message();
		message.setDescription("Application Error");
		message.setStrackTrace(e.getMessage());
		return message;
	}

	@ExceptionHandler(value = RollBackException.class)
	@ResponseBody
	public void rollBackExceptionHandler(Exception e) throws Exception {

	}
}
