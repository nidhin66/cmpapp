package com.gvg.syena.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeDependencySchedulingException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.DependencyManagmnet;
import com.gvg.syena.core.api.services.HierarchyNodeDependencyManegmanet;
import com.gvg.syena.core.api.services.names.JobManagementServiceNames;

@RestController
@RequestMapping(value = { "dependencyManagmnet" })
public class DependencyJobController {

	@Autowired
	private DependencyManagmnet dependencyManagmnet;

	@Autowired
	private HierarchyNodeDependencyManegmanet hierarchyNodeDependencyManegmanet;

	@RequestMapping(value = JobManagementServiceNames.addPredecessorJobs, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<Job> addPredecessorJobs(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] predecessorJobIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		return dependencyManagmnet.addPredecessorJobs(jobId, predecessorJobIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.addPredecessorJobGroups, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<JobGroup<Job>> addPredecessorJobGroups(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] predecessorJobGroupIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		return dependencyManagmnet.addPredecessorJobGroups(jobId, predecessorJobGroupIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.addSuccessorJobs, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<Job> addSuccessorJobs(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] successorJobIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		return dependencyManagmnet.addSuccessorJobs(jobId, successorJobIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.addSuccessorJobGroups, method = { RequestMethod.PUT, RequestMethod.POST })
	public List<JobGroup<Job>> addSuccessorJobGroups(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] successorJobIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		return dependencyManagmnet.addSuccessorJobGroups(jobId, successorJobIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.removePredecessorJobs, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removePredecessorJobs(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] predecessorJobIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException {
		dependencyManagmnet.removePredecessorJobs(jobId, predecessorJobIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.removePredecessorJobGroups, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removePredecessorJobGroups(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] predecessorJobGroupIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException {
		dependencyManagmnet.removePredecessorJobGroups(jobId, predecessorJobGroupIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.removeSuccessorJobs, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removeSuccessorJobs(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] successorJobIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException {
		dependencyManagmnet.removeSuccessorJobs(jobId, successorJobIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.removeSuccessorJobGroups, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removeSuccessorJobGroups(@RequestParam(required = true) long jobId, @RequestParam(required = true) long[] successorJobIds,
			@RequestParam(required = true) NodeCategory nodeCategory) throws JobNotFoundException, DependencyManagerNotFoundException {
		dependencyManagmnet.removeSuccessorJobGroups(jobId, successorJobIds, DependencyType.FinishToStart, nodeCategory);
	}

	@RequestMapping(value = JobManagementServiceNames.getSuccessorJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getSuccessorJobs(@RequestParam(required = true) long jobId, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException {
		return dependencyManagmnet.getSuccessorJobs(jobId, nodeCategory, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getPredecessorJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> getPredecessorJobs(@RequestParam(required = true) long jobId, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException {
		return dependencyManagmnet.getPredecessorJobs(jobId, nodeCategory, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getSuccessorJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> getSuccessorJobGroups(@RequestParam(required = true) long id, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException {
		return dependencyManagmnet.getSuccessorJobGroups(id, nodeCategory, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getPredecessorJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> getPredecessorJobGroups(@RequestParam(required = true) long id, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException {
		return dependencyManagmnet.getPredecessorJobGroups(id, nodeCategory, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchSuccessorJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> searchSuccessorJobs(@RequestParam(required = true) long id, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) String name, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return dependencyManagmnet.searchSuccessorJobs(id, nodeCategory, name, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchPredecessorJobs, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<Job> searchPredecessorJobs(@RequestParam(required = true) long id, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) String name, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return dependencyManagmnet.searchPredecessorJobs(id, nodeCategory, name, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchSuccessorJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> searchSuccessorJobGroups(@RequestParam(required = true) long id, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) String name, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return dependencyManagmnet.searchSuccessorJobGroups(id, nodeCategory, name, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchPredecessorJobGroups, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<JobGroup> searchPredecessorJobGroups(@RequestParam(required = true) long id, @RequestParam(required = true) NodeCategory nodeCategory,
			@RequestParam(required = true) String name, @RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return dependencyManagmnet.searchPredecessorJobGroups(id, nodeCategory, name, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.addPredecessorHierarchyNodeDependency, method = { RequestMethod.PUT, RequestMethod.POST })
	public void addPredecessorHierarchyNodeDependency(long hierarchyNodeId, long[] dependencyNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException {
		hierarchyNodeDependencyManegmanet.addPredecessorHierarchyNodeDependency(hierarchyNodeId, dependencyNodeIds);

	}

	@RequestMapping(value = JobManagementServiceNames.addSuccessorHierarchyNodeDependency, method = { RequestMethod.PUT, RequestMethod.POST })
	public void addSuccessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) long[] dependencyNodeIds)
			throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, SchedulingException,
			HierarchyNodeDependencySchedulingException, InvalidDataException {
		hierarchyNodeDependencyManegmanet.addSuccessorHierarchyNodeDependency(hierarchyNodeId, dependencyNodeIds);

	}

	@RequestMapping(value = JobManagementServiceNames.removePredecessorHierarchyNodeDependency, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removePredecessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) long[] dependencyNodeIds)
			throws SchedulingException, ApplicationException {
		hierarchyNodeDependencyManegmanet.removePredecessorHierarchyNodeDependency(hierarchyNodeId, dependencyNodeIds);

	}

	@RequestMapping(value = JobManagementServiceNames.removeSuccessorHierarchyNodeDependency, method = { RequestMethod.PUT, RequestMethod.POST })
	public void removeSuccessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) long[] dependencyNodeIds)
			throws SchedulingException, ApplicationException {
		hierarchyNodeDependencyManegmanet.removeSuccessorHierarchyNodeDependency(hierarchyNodeId, dependencyNodeIds);

	}

	@RequestMapping(value = JobManagementServiceNames.getPredecessorHierarchyNodeDependency, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getPredecessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws HierarchyNodeNotFoundException {
		return hierarchyNodeDependencyManegmanet.getPredecessorHierarchyNodeDependency(hierarchyNodeId, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.getSuccessorHierarchyNodeDependency, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> getSuccessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) int pageNumber,
			@RequestParam(required = true) int pageSize) throws HierarchyNodeNotFoundException {
		return hierarchyNodeDependencyManegmanet.getSuccessorHierarchyNodeDependency(hierarchyNodeId, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchPredecessorHierarchyNodeDependency, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> searchPredecessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) String name,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws HierarchyNodeNotFoundException {
		return hierarchyNodeDependencyManegmanet.searchPredecessorHierarchyNodeDependency(hierarchyNodeId, name, pageNumber, pageSize);
	}

	@RequestMapping(value = JobManagementServiceNames.searchSuccessorHierarchyNodeDependency, method = { RequestMethod.GET, RequestMethod.POST })
	public PageItr<HierarchyNode> searchSuccessorHierarchyNodeDependency(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) String name,
			@RequestParam(required = true) int pageNumber, @RequestParam(required = true) int pageSize) throws HierarchyNodeNotFoundException {
		return hierarchyNodeDependencyManegmanet.searchSuccessorHierarchyNodeDependency(hierarchyNodeId, name, pageNumber, pageSize);
	}

}
