package com.gvg.syena.web.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.NotScheduledException;
import com.gvg.syena.core.api.services.chart.ChartsDataBuilder;
import com.gvg.syena.core.api.services.names.ChartsDataManagementServiceNames;

@RestController
@RequestMapping(value = { "chartsData" })
public class ChartsDataManagementController {

	@Autowired
	private ChartsDataBuilder chartsDataBuilder;

	@RequestMapping(value = ChartsDataManagementServiceNames.getChartsDatas, method = { RequestMethod.GET, RequestMethod.POST })
	public ChartData<Date> getChartsData(@RequestParam(required = true) long hierarchyNodeId, @RequestParam(required = true) ChartType[] chartTypes)
			throws HierarchyNodeNotFoundException, NotScheduledException {
		return chartsDataBuilder.getChartsDataOfHierarchyNode(hierarchyNodeId, chartTypes);
	}

}
