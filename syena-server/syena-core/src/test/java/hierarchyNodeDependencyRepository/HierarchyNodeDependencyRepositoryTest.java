package hierarchyNodeDependencyRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.dependency.HierarchyNodeDependency;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.datarepository.dependency.HierarchyNodeDependencyRepository;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class HierarchyNodeDependencyRepositoryTest {

	@Autowired
	private HierarchyNodeDependencyRepository dependencyRepository;
	
	@Test
	public void testName() throws Exception {
		HierarchyNodeDependency entity = new HierarchyNodeDependency();
		dependencyRepository.save(entity );
	}
	
}
