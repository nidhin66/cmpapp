package com.gvg.syena.core.services;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.services.serach.AdvancedJobGroupSearch;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class AdvancedJobGroupSearchServiceImplTest {

	@Autowired
	private AdvancedJobGroupSearch advancedJobGroupSearchService;

//	@Test
	public void getOnGoingJobGroups() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.getOnGoingJobGroups(date, "%", 0, 10);
		jobgroups.getContent();
	}

//	@Test
	public void completedJobGroups() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.completedJobGroups(date, "%", 0, 10);
		jobgroups.getContent();
	}

//	@Test
	public void plannedJobGroups() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.plannedJobGroups(date, "%", 0, 10);
		jobgroups.getContent();

	}

//	@Test
	public void delayedJobGroups() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.delayedJobGroups(date, "%", 0, 10);
		jobgroups.getContent();

	}

//	@Test
	public void getJobsInitiatedBy() {
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.getJobsInitiatedBy("", "%", 0, 10);
		jobgroups.getContent();

	}

//	@Test
	public void getJobsOwnedBy() {
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.getJobsOwnedBy("", "%", 0, 10);
		jobgroups.getContent();
	}

//	@Test
	public void jobsToBeScheduled() {
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.jobsToBeScheduled("%", 0, 10);
		jobgroups.getContent();
	}

//	@Test
	public void jobsWithRevisedDates() {
		PageItr<JobGroup<Job>> jobgroups = advancedJobGroupSearchService.jobsWithRevisedDates("%", 0, 10);
		jobgroups.getContent();
	}

//	@Test
	public void searchJobs() {
		JobProperties advancedSearchOptions = new JobProperties();
		advancedSearchOptions.setDescription("haha");
		advancedJobGroupSearchService.searchJobs(advancedSearchOptions, 0, 10);

	}

	@Test
	public void jobGroupsInCriticalPath() {
		advancedJobGroupSearchService.jobGroupsInCriticalPath(219, "%", 0, 10);


	}
}
