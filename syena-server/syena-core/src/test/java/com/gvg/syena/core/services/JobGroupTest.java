package com.gvg.syena.core.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class JobGroupTest {

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Autowired
	private JobManagement jobManagement;

	// @Test
	public void createStandardJobGroup() throws InvalidDataException {
		JobGroup<StandardJob> jobGroup = new JobGroup<StandardJob>("Group1");
		StandardJob stnJob1 = new StandardJob();
		jobGroup.addJob(stnJob1);
		jobManagement.createStandardJobGroup(jobGroup);

	}

	@Test
	public void createAndLink() throws HierarchyNodeNotFoundException, InvalidDataException {
		long hierarchyNodeId = 416;
		long[] standardJobIds = { 1885 };
		hierarchyManagement.createAndlinkJob(hierarchyNodeId, standardJobIds);

	}

	// @Test
	public void createAndlinkJobGroupsTest() throws HierarchyNodeNotFoundException, JobNotFoundException {

		long hierarchyNodeId = 215;
		long standardJobGroupIds = 4;
		hierarchyManagement.createAndlinkJobGroups(hierarchyNodeId, standardJobGroupIds);
	}

}
