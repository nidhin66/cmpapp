package com.gvg.syena.core.services;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class JobManagementIplTest {

	@Autowired
	private JobManagement jobmanagement;

	// @Test
	@Transactional
	public void testName() throws Exception {

		StandardJob standardJob1 = new StandardJob("standardJob1", "standardJob 1 Description");

		jobmanagement.createStandardJob(standardJob1);

		StandardJob standardJob2 = new StandardJob("standardJob2", "standardJob 2 Description");

		jobmanagement.createStandardJob(standardJob2);

		Job job1 = new Job(standardJob1, "job1");

		jobmanagement.createJob(job1);

		Job job2 = new Job(standardJob1, "job2");

		jobmanagement.createJob(job2);

		JobGroup<StandardJob> standardJobGroup1 = new JobGroup<StandardJob>("1");

		jobmanagement.createStandardJobGroup(standardJobGroup1);

		standardJobGroup1.addJob(standardJob1);

		standardJobGroup1.addJob(standardJob2);

		jobmanagement.updateStandardJobGroup(standardJobGroup1);

		List<JobGroup<Job>> jobGroupList = jobmanagement.createJobGroupFromStandardJobGroup(standardJobGroup1.getId());

		JobGroup<Job> jobGroup1 = jobGroupList.get(0);

		jobGroup1.addJob(job1);

		jobGroup1.addJob(job2);

		jobmanagement.updateJobGroup(jobGroup1);

		JobGroup<StandardJob> standardJobGroup2 = new JobGroup<StandardJob>("2");

		jobmanagement.createStandardJobGroup(standardJobGroup2);

		StandardJob standardJob3 = new StandardJob(standardJob2, "standardJob3");

		standardJobGroup2.addJob(standardJob3);

		StandardJob standardJob4 = new StandardJob(standardJob3, "standardJob3");

		standardJobGroup2.addJob(standardJob4);

		jobmanagement.updateStandardJobGroup(standardJobGroup2);

		List<JobGroup<Job>> jobGroup2 = jobmanagement.createJobGroupFromStandardJobGroup(standardJobGroup1.getId());

		//
		// JobGroup<StandardJob> standardJobGroup2 = new
		// JobGroup<StandardJob>();
		//
		// jobmanagement.addDependencyJob(job1.getId(), job2.getId());
		//
		// Job givenJob1 = jobmanagement.getJob(job1.getId());
		//
		// StandardJob givenJobStn = jobmanagement.getJob(standardJob.getId());
		//
		// Set<Job> givenjobs1 =
		// givenJob1.getDependencyJobs().getEndToStart().getJobs();
		// // givenjobs1.contains(job2);
		//
		// Job job3 = new Job("job3", "job3");
		// jobmanagement.createJob(job3);
		//
		// job2.addDependentJob(job3);
		//
		// jobmanagement.updateJob(job2);
		//
		// jobmanagement.addDependencyJob(job3.getId(), job2.getId());

	}

	private void getJobGroupssByConnectedNodeIds() {
		long[] parentIds = new long[] { 244 };
		jobmanagement.getJobGroupssByConnectedNodeIds(parentIds, 0, 1);

	}

	@Test
	@Transactional
	public void getJobsInJobGroup() throws JobNotFoundException, IOException {
		Set<Job> jobs = jobmanagement.getJobsInJobGroup(32785);
		ObjectMapper mapper = new ObjectMapper();
		String str = mapper.writeValueAsString(jobs);
		mapper.readValue(str, Job.class);

	}
}
