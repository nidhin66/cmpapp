package com.gvg.syena.core.services.dependency;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.HierarchyNodeDependencyManegmanet;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class HierarchyNodeDependencyImplTest {

	@Autowired
	private HierarchyNodeDependencyManegmanet hierarchyNodeDependencyManegmanet;

	@Test
	public void testAddPredecessorHierarchyNodeDependency() throws SchedulingException, ApplicationException {
		long hierarchyNodeId = 213;
		long[] dependencyNodeIds = {252};
//		hierarchyNodeDependencyManegmanet.addPredecessorHierarchyNodeDependency(hierarchyNodeId, dependencyNodeIds);
		hierarchyNodeDependencyManegmanet.removePredecessorHierarchyNodeDependency(hierarchyNodeId, dependencyNodeIds);
	}

//	@Test
	public void testAddSuccessorHierarchyNodeDependency() {
		fail("Not yet implemented");
	}

//	@Test
	public void testRemovePredecessorHierarchyNodeDependency() {
		fail("Not yet implemented");
	}

//	@Test
	public void testRemoveSuccessorHierarchyNodeDependency() {
		fail("Not yet implemented");
	}

//	@Test
	public void testGetPredecessorHierarchyNodeDependency() {
		fail("Not yet implemented");
	}

//	@Test
	public void testGetSuccessorHierarchyNodeDependency() {
		fail("Not yet implemented");
	}

}
