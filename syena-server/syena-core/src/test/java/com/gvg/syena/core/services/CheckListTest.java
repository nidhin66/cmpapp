package com.gvg.syena.core.services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;
import com.gvg.syena.core.api.services.CheckListManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class CheckListTest {

	@Autowired
	private CheckListManagement checkListManagement;

	@Test
	public void testName() throws Exception {

		ArrayList<CheckList> checkListsb = new ArrayList<CheckList>();
		CheckList verfication = new CheckList("varification", true);
		List<CheckListItem> chkLists = new ArrayList<CheckListItem>();
		chkLists.add(new CheckListItem("Inspected"));
		chkLists.add(new CheckListItem("Approved"));
		verfication.addCheckListItems(chkLists);
		checkListsb.add(verfication);
		checkListManagement.createCheckLists(checkListsb);

		PageItr<CheckList> gCheckLists = checkListManagement.getAllStandardCheckList(0, 1);
		CheckList item1 = gCheckLists.getContent().get(0);

	}
}
