package com.gvg.syena.core.services.dependency;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.services.dependency.analysis.DependencyAnalyzerImpl;
import com.gvg.syena.core.services.dependency.api.DependencyAnalyzer;

public class DependencyAnalyzerTest2 {

	private DependencyAnalyzer dependencyAnalyzer;

	@Before
	public void before() {
		dependencyAnalyzer = new DependencyAnalyzerImpl();
	}

	private Job getJob(long id) {
		Job job = new Job("Jo2" + id, "job" + id);
		job.setId(id);
		return job;
	}

	private void addDependencyJobs(Job job, Job job1) {
		job.getPredecessorDependency().getFinishToStart().addJob(job1);
	}
	
	private void addDependencyJobsWithanalyze(Job job, Job job1) throws DependencyFailureException {
		List<DependencyAttachableHierarchy> dependents = new ArrayList<>();
		dependents.add(job1);
		DependencyType dependencyType = DependencyType.FinishToStart;
		dependencyAnalyzer.analyzeDependency(job, dependents, dependencyType);
	}
	
	@Test
	public void testName() throws Exception {
		
	}
}
