package com.gvg.syena.core.datarepository.hierarchy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class HierarchyNodeRepositoryTest {

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	private StandardHierarchyNodeRepository standardHierarchyNodeRepository;

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Test
	public void test() throws AlreadyLinkedException, HierarchyNodeNotFoundException, HierarchyLevelNotFoundException {

		// HierarchyLevel hierarchyLevel = new HierarchyLevel("Unit");
		//
		// StandardHierarchyNode standardHierarchy = new
		// StandardHierarchyNode<>("", "", hierarchyLevel);
		// standardHierarchyNodeRepository.save(standardHierarchy);
		//
		// HierarchyNode hierarchyNode = new HierarchyNode(standardHierarchy, 1,
		// false);
		// hierarchyNodeRepository.save(hierarchyNode);

		// hierarchyManagement.createAndLinkHierarchyNodes(213,209);

		PageItr<StandardHierarchyNode> data = hierarchyManagement.searchStandardHierarchyNodesInLevel("%IUS%", "Loop/Equipments", 0, 10);
		System.out.println();
	}

}
