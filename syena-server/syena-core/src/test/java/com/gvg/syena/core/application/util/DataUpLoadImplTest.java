package com.gvg.syena.core.application.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.services.dataupload.DataUpLoader;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class DataUpLoadImplTest {
	@Autowired
	private DataUpLoader dataUpLoad;

	@Autowired
	HierarchyManagement hierarchyManagement;

	@Autowired
	PersonRepository personRepository;

	private int projectId = 1;

	Person owner = null;

	private final String filePath = "/MappedData_DataUpLoad1.xlsx";

	@Test
	public void run() throws IOException, ApplicationException, ServiceLevelValidationException, HierarchyNodeNotFoundException, AlreadyLinkedException {
		uploadHierarchyLevel();
		uploadStandardHierarchyNodes();
		addStandardHierarchyNodesToParoject();
		uploadJobAndTask();

	}

	private void uploadHierarchyLevel() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filePath);
		dataUpLoad.uploadHierarchyLevel(in);
	}

	private void uploadStandardHierarchyNodes() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filePath);
		dataUpLoad.uploadStandardHierarchyNodes(in);
	}

	private void addStandardHierarchyNodesToParoject() throws PersonNotFoundException, ServiceLevelValidationException, HierarchyNodeNotFoundException, AlreadyLinkedException,
			HierarchyLevelNotFoundException {
		Person person = new Person("GVG001");
		personRepository.save(person);
		String hierarchyLevel = "Plant";
		List<StandardHierarchyNode<StandardHierarchyNode>> topNodes = hierarchyManagement.getAllStandardHierarchyNodes(hierarchyLevel, 0, 1).getContent();
		for (StandardHierarchyNode standardHierarchyNode : topNodes) {
			hierarchyManagement.saveAsHierarchyNode(standardHierarchyNode.getId(), person.getId());
		}

	}

	private void uploadJobAndTask() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filePath);
		dataUpLoad.uploadJobAndTask(in);
	}
}
