package com.gvg.syena.core.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.services.dataupload.TestDataInsertService;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class TestDataInsertServiceTest {

	@Autowired
	private TestDataInsertService testDataInsertService;

	@Test
	public void test() {
		// testDataInsertService.insertData(1, 2, 2, 2, 2);
		testDataInsertService.addJob("Unit", 1, 1, "1");

	}

}
