package com.gvg.syena.util;

import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;

public class ReadExcel {

	private XSSFSheet sheet;

	public ReadExcel() throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook("E://MappedData1.xlsx");
		this.sheet = wb.getSheetAt(5);
	}

	public void Read() {
		Boolean isHeader = true;
		HierarchyLevel parent = null;
		Iterator<Row> rowItrator = sheet.iterator();
		while (rowItrator.hasNext()) {
			XSSFRow row = (XSSFRow) rowItrator.next();
			short cellCount = row.getLastCellNum();
			if (cellCount == 0) {
				isHeader = false;
			} else {
				if (isHeader) {
					HierarchyLevel hierarchyLevel = readHeaderRow(row);
					if (parent == null) {
						parent = hierarchyLevel;
					} else {
						parent.addChild(hierarchyLevel);
					}
				} else {
					readDataRow(row);
				}
			}

		}
	}

	private HierarchyLevel readHeaderRow(XSSFRow row) {
		Iterator<Cell> cellIterator = row.cellIterator();
		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
			String value = cell.getStringCellValue();
			if (value != null && value != "") {
				HierarchyLevel hierarchyLevel = new HierarchyLevel(value);
				return hierarchyLevel;
			}

		}
		return null;
	}

	private void readDataRow(XSSFRow row) {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) throws IOException {
		ReadExcel ob = new ReadExcel();
		ob.Read();
	}
}
