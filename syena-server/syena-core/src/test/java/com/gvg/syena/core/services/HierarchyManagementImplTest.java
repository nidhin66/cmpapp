package com.gvg.syena.core.services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.project.Project;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.UserManagemnet;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class HierarchyManagementImplTest {

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Autowired
	private UserManagemnet organizationManagemnet;

	public void testName() throws Exception {

		HierarchyLevel plant = new HierarchyLevel("Plant");
		HierarchyLevel unit = new HierarchyLevel("unit");
		plant.addChild(unit);
		HierarchyLevel system = new HierarchyLevel("system");
		unit.addChild(system);
		HierarchyLevel subsytem = new HierarchyLevel("subsytem");
		system.addChild(subsytem);
		HierarchyLevel loop = new HierarchyLevel("loop");
		subsytem.addChild(loop);
		HierarchyLevel equipment = new HierarchyLevel("equipment");
		subsytem.addChild(equipment);
		hierarchyManagement.createHierarchyLevel(plant);
		HierarchyLevel givenLevels = hierarchyManagement.getHierarchyLevels("Plant");
		// TODO check

		StandardHierarchyNode standardHierarchyNode = new StandardHierarchyNode("Plant1", "Plant 1", plant);
		hierarchyManagement.createStandardHierarchyNode(standardHierarchyNode, "GEE");

		hierarchyManagement.getAllStandardHierarchyNodes(plant.getLevelName(), 0, 1);
		hierarchyManagement.getAllHierarchyNodes(plant.getLevelName(), 0, 1);

		Project project = new Project("BPCL", "BPCL");
		hierarchyManagement.createProject(project);

		Person person = new Person("geevarughesejohn");
		organizationManagemnet.createPerson(person);

		HierarchyNode hierarchyNode1 = new HierarchyNode(standardHierarchyNode, project.getId(), false);
		hierarchyManagement.createHierarchyNode(hierarchyNode1, person.getId());

		HierarchyNode hierarchyNode2 = new HierarchyNode("name2", "description2", unit, project.getId());
		hierarchyManagement.createHierarchyNode(hierarchyNode2, person.getId());

		HierarchyNode hierarchyNode3 = new HierarchyNode("name3", "description3", unit, project.getId());
		hierarchyManagement.createHierarchyNode(hierarchyNode3, person.getId());

		hierarchyManagement.linkHierarchyNodes(hierarchyNode1.getId(), hierarchyNode2.getId());
		hierarchyManagement.linkHierarchyNodes(hierarchyNode1.getId(), hierarchyNode3.getId());

		long[] nodeIds = hierarchyManagement.getHierarchyNodeIds(unit.getLevelName());
		hierarchyManagement.getHierarchyNodes(nodeIds, 0, 1);

		List<HierarchyNode> hierarchyNodes = (List) hierarchyManagement.getLinkedHierarchyNodes(hierarchyNode1.getId(), 0, 10).getContent();
		// TODO check

		HierarchyNode node = hierarchyNodes.get(0);

		node.setDescription("updated description");

//		hierarchyManagement.updateHierarchyNode(node);
		// TODO check

		hierarchyManagement.unlinkHierarchyNodes(hierarchyNode1.getId(), node.getId());

		hierarchyManagement.obsoleteHierarchyNode(node.getId());

		Collection<HierarchyNode> hierarchyNodesAfterObsalte = hierarchyManagement.getLinkedHierarchyNodes(hierarchyNode1.getId(), 0, 10).getContent();

		hierarchyManagement.getObsoleteHierarchyNodes(0, 10);

		System.out.println();
	}

	// @Test
	public void getLinkedHierarchyNodes() throws JsonProcessingException {
		Collection<HierarchyNode> hierarchyNodes = hierarchyManagement.getLinkedHierarchyNodes(0, 0, 10).getContent();
		ObjectMapper objectMapper = new ObjectMapper();
		String str = objectMapper.writeValueAsString(hierarchyNodes);
		System.out.println(str);

	}

//	@Test
	public void search() throws HierarchyLevelNotFoundException {
		String hierarchyNodename = "%LZ-P%";
		int pageNumber = 0;
		int pageSize = 10;
		long parentId = 32839;
		PageItr<HierarchyNode> result = hierarchyManagement.searchHierarchyNodesInChild(hierarchyNodename, parentId, pageNumber, pageSize);
		System.out.println();
	}

	@Test
	public void createMultipleLevel() throws ServiceLevelValidationException, PersonNotFoundException {
		HierarchyLevel plant = new HierarchyLevel("Plant");
		StandardHierarchyNode standardHierarchyNode1 = new StandardHierarchyNode("Plant1", "Plant 1", plant);
		StandardHierarchyNode standardHierarchyNode2 = new StandardHierarchyNode("Plant2", "Plant 2", plant);
		standardHierarchyNode1.addChild(standardHierarchyNode2);
		hierarchyManagement.createStandardHierarchyNode(standardHierarchyNode1, "GEE");

	}
}
