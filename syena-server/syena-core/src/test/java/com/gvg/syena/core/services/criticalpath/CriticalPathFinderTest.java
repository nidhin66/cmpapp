package com.gvg.syena.core.services.criticalpath;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.NotScheduledException;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class CriticalPathFinderTest {

	@Autowired
	private CriticalPathFinder criticalPathFinder;

	@Test
	public void testFindCriticalPathJob() throws HierarchyNodeNotFoundException, NotScheduledException {
		CriticalAndNonCriticalPath criticalAndNonCriticalPath = criticalPathFinder.findCriticalAndNonCriticalPath(210);
		Date fromTime = new Date();
		Date toTime = new Date();
		JobDataSet criticalPathPeriodSet = new JobDataSet(fromTime, toTime);
		criticalPathPeriodSet.add(criticalAndNonCriticalPath.getCriticalPath());
		criticalPathPeriodSet.add(criticalAndNonCriticalPath.getNonCriticalPath());
		System.out.println();
	}

}
