package com.gvg.syena.core.services.schedule;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class HierarchybScheduleTest {

	@Autowired
	private ScheduleManagment scheduleManagment;

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Autowired
	private HierarchyScheduler hierarchyScheduler;

	public void setJobSchedule() {
		// scheduleManagment.setJobSchedule(jobId, timeSchedule);
	}

	@Test
	public void setHierarchybSchedule() throws ServiceLevelValidationException, HierarchyLevelNotFoundException, JobScheduleFailed, HierarchyNodeNotFoundException,
			SchedulingException, InvalidDataException {
		// long[] hierarchyNode =
		// hierarchyManagement.getHierarchyNodeIds("Plant");
		// WorkTime timeSchedule = new WorkTime(new Date(), new PersonDays(5));
		// scheduleManagment.setHierarchybSchedule(hierarchyNode[0],
		// timeSchedule);
		//
		// long[] hierarchyNode =
		// hierarchyManagement.getHierarchyNodeIds("System");
		Calendar calendar = Calendar.getInstance();
		// calendar.add(Calendar.DAY_OF_YEAR, 5);
		Date today = new Date();
		calendar.setTime(today);
		calendar.add(Calendar.MONTH, -3);
		WorkTime timeSchedule = new WorkTime(calendar.getTime(), new PersonDays(5));
		// scheduleManagment.setHierarchybSchedule(hierarchyNode[0],
		// timeSchedule);
		scheduleManagment.setHierarchybSchedule(215, timeSchedule);
	}

	// @Test
	public void schedule() throws JobScheduleFailed, InvalidDataException {

		HierarchyLevel hierarchyLevel = new HierarchyLevel("1");
		HierarchyNode hierarchyNode = new HierarchyNode("name", "description", hierarchyLevel, 1);
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		hierarchyScheduler.schedule(hierarchyNode, new Date(), new Date(), changes);

	}

}
