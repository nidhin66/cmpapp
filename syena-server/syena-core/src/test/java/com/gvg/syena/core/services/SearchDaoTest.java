package com.gvg.syena.core.services;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.exception.SearchException;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.api.services.serach.SearchCriteria;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.services.search.SearchDao;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class SearchDaoTest {

	@Autowired
	private SearchDao searchDao;

	@Autowired
	private AdvancedJobSearch advancedJobSearch;

	// @Test
	public void testSearch() throws SearchException {

		SearchCriteria<HierarchyNode> searchCriteria = new SearchCriteria<HierarchyNode>(HierarchyNode.class);
		List<HierarchyNode> hierarchyNodes = searchDao.search(searchCriteria, 0, 10);
	}

	@Test
	public void jobsInCriticalPath() {
		long hierarchyId = 210;
		advancedJobSearch.jobsInCriticalPath(hierarchyId, "%", 0, 10);

	}

}
