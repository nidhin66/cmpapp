package com.gvg.syena.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

public class CopyOfReadData1 {
	private XSSFSheet sheet;

	private HierarchyLevel[] hierarchyLevel = new HierarchyLevel[6];
	private HierarchyNode[] HierarchyNodeParents = new HierarchyNode[hierarchyLevel.length];

	HierarchyLevel parentLevel;

	private HierarchyLevel childLevel;

	ArrayList<String> f = new ArrayList<String>();

	private HierarchyNode parentNode;

	public CopyOfReadData1() throws IOException {

		XSSFWorkbook wb = new XSSFWorkbook("E://MappedData1.xlsx");
		this.sheet = wb.getSheetAt(5);

		// hierarchyLevel[0] = new HierarchyLevel("Plant");
		// hierarchyLevel[1] = new HierarchyLevel("Unit");
		// hierarchyLevel[2] = new HierarchyLevel("System");
		// hierarchyLevel[3] = new HierarchyLevel("Equipments");
		// hierarchyLevel[4] = new HierarchyLevel("loops");
		// hierarchyLevel[5] = new HierarchyLevel("subloops");

	}

	public void read() {

		Iterator<Row> rowItrator = sheet.iterator();
		if (rowItrator.hasNext()) {
			Row header = rowItrator.next();
			Iterator<Cell> cellIterator = header.cellIterator();
			if (cellIterator.hasNext()) {
				XSSFCell cell = (XSSFCell) cellIterator.next();
				String value = cell.getStringCellValue();
				parentLevel = new HierarchyLevel(value);

			} else {
				return;
			}
			if (cellIterator.hasNext()) {
				XSSFCell cell = (XSSFCell) cellIterator.next();
				String value = cell.getStringCellValue();
				this.childLevel = new HierarchyLevel(value);

			} else {
				return;
			}

			while (cellIterator.hasNext()) {
				XSSFCell cell = (XSSFCell) cellIterator.next();
				String value = cell.getStringCellValue();
				f.add(value);
			}
		}
		while (rowItrator.hasNext()) {
			Row header = rowItrator.next();
			Iterator<Cell> cellIterator = header.cellIterator();

			XSSFCell cell = (XSSFCell) cellIterator.next();
			String value = cell.getStringCellValue();
			if (value != null && value != "") {
				this.parentNode = new HierarchyNode();
			}
			cell = (XSSFCell) cellIterator.next();
			value = cell.getStringCellValue();

			HierarchyNode childNode = null;
			if (value != null && value != "") {
				childNode = new HierarchyNode();
				parentNode.addChild(childNode);
			}
			if (childNode != null) {
				while (cellIterator.hasNext()) {
					cell = (XSSFCell) cellIterator.next();
					value = cell.getStringCellValue();
//					childNode.getClass().getMethod(name, parameterTypes);
				}
			}

		}
	}

	public static void main(String[] args) throws IOException {
		CopyOfReadData1 r = new CopyOfReadData1();
		r.read();

	}
}
