package com.gvg.syena.core.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class LinkedStandardHierarchyNodeTest {
	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Test
	public void test() {
		long parentId = 0;
		int pageNumber = 0;
		int pageSize = 0;
		PageItr<StandardHierarchyNode> page = hierarchyManagement.getLinkedStandardHierarchyNodes(parentId, pageNumber, pageSize);
		System.out.println(page.getContent().size());
	}

}
