package com.gvg.syena.core.services.dependency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.DependencyManagmnet;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class DependencyManagmnetTest {

	@Autowired
	private DependencyManagmnet dependencyManagmnet;

	@Test
	public void getDependency() throws JobNotFoundException, DependencyManagerNotFoundException, DependencyFailureException {
//		dependencyManagmnet.getPredecessorJobs(65536, NodeCategory.job, 0, 10);
		dependencyManagmnet.getSuccessorJobs(32777, NodeCategory.job, 0, 10);

		// dependencyManagmnet.addPredecessorJobs(65536, new long[] { 65545 },
		// DependencyType.FinishToStart, NodeCategory.job);

	}
}
