package com.gvg.syena.util;

import junit.framework.Assert;

import org.junit.Test;

import com.gvg.syena.core.utilities.UtilServices;

public class PasswordTest {
	
	@Test
	public void testEncodePassword() throws Exception {
		String password = "admin123";
		String encoded = UtilServices.getHashPassword(password);
		System.out.println(encoded);
		Assert.assertNotNull(encoded);
	}
	
	@Test
	public void testVerifyCorrectPassword() throws Exception {
		String password = "admin123";
		String encoded = UtilServices.getHashPassword(password);
		boolean result = UtilServices.verifyHashPassword(password, encoded);
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void testVerifyWrongPassword() throws Exception {
		String password = "admin123";
		String encoded = UtilServices.getHashPassword(password);
		boolean result = UtilServices.verifyHashPassword("123", encoded);
		Assert.assertEquals(false, result);
	}

}
