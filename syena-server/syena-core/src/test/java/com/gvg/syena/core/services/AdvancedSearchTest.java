package com.gvg.syena.core.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.api.services.serach.HierarchyNodeProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class AdvancedSearchTest {

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Autowired
	private AdvancedJobSearch advancedJobSearch;

	// @Test
	public void hierarchyTest() {
		HierarchyNodeProperties hierarchyNodeProperties = new HierarchyNodeProperties();
		hierarchyNodeProperties.setDescription("");
		hierarchyNodeProperties.setName("Plant1");
		int pageNumber = 0;
		int pageSize = 10;
		PageItr<HierarchyNode> page = hierarchyManagement.searchWithProperties(hierarchyNodeProperties, pageNumber, pageSize);
		System.out.println();
	}

	@Test
	public void SearchJob() {
		JobProperties advancedSearchOptions = new JobProperties();

		// advancedSearchOptions.setOwnerId("gvg001");
		// advancedSearchOptions.setName("task");

//		WorkStatus workStatus = WorkStatusType.Completed_on_schedule.getWorkStatus();
//		WorkStatus workStatus = WorkStatusType.Completed_with_delays.getWorkStatus();
//		advancedSearchOptions.setWorkStatus(workStatus);
		advancedSearchOptions.setMilestoneDescription("aa");
		PageItr<Job> jobs = advancedJobSearch.searchJobs(advancedSearchOptions, 0, 10);

		System.out.println(jobs.getContent().size());
	}

	// @Test
	public void SearchParent() {
		long[] childIds = { 213, 214, 215 };
		PageItr<HierarchyNode> parent = hierarchyManagement.getParentHierarchyNodes(childIds, 0, 10);

		System.out.println(parent);
	}
}
