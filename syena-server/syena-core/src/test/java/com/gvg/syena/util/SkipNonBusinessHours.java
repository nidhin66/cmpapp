package com.gvg.syena.util;

// To Calculate Review Time by skipping Non-Business Hours and Holidays

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SkipNonBusinessHours {

	public static final int BUSINESS_START_HOUR = 9; // 9 AM
	public static final int BUSINESS_END_HOUR = 18; // 6 PM

	public static void main(String[] args) {
		// One Working Day means 9 Hrs
		final int WORKING_MINS_PER_DAY = 540; // Total Working Minutes per day
		final int MINUTES_PER_HOUR = 60;

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");

		int addTime = 1;
		String sDaysOrHours = "hours"; // "days";

		if (sDaysOrHours.equalsIgnoreCase("days")) {

			addTime = addTime * WORKING_MINS_PER_DAY; // Converting Days to Mins
		} else if (sDaysOrHours.equalsIgnoreCase("hours")) {

			addTime = addTime * MINUTES_PER_HOUR; // Converting Hours to Mins
		}
		// Getting Present Date and Time
		Calendar n_givenDateTime = Calendar.getInstance();
		System.out.println("Present Date:\t\t" + sdf.format(n_givenDateTime.getTime()));

		SkipNonBusinessHours time = new SkipNonBusinessHours();
		time.addBusinessTime(n_givenDateTime, addTime);
		System.out.println("\nFinal Date :\t\t" + sdf.format(n_givenDateTime.getTime()));
	}

	// Method to add Review Time (in minutes) to Given Date

	public void addBusinessTime(Calendar givenDateTime, int addTimeMins) {
		// To get Present Working Day & Hour excluding Holidays
		adjustToBusinessHours(givenDateTime);

		while (addTimeMins > 0) {
			// Finding Business End Time On That Day
			Calendar businessEndTime = Calendar.getInstance();
			businessEndTime.setTime(givenDateTime.getTime());
			businessEndTime.set(Calendar.HOUR_OF_DAY, BUSINESS_END_HOUR);
			businessEndTime.clear(Calendar.MINUTE);

			// Difference of Time in Minutes AND Conversion Milli-Seconds to
			// Minutes
			int diffMins = (int) ((businessEndTime.getTimeInMillis() - givenDateTime.getTimeInMillis()) / (1000 * 60));

			if (addTimeMins < diffMins) // Adding Total Minutes to the Same Day
			{

				givenDateTime.add(Calendar.MINUTE, addTimeMins);
				break;
			} else // Adding available Minutes to the Same Day
			{

				givenDateTime.add(Calendar.MINUTE, diffMins);
				adjustToBusinessHours(givenDateTime);
				addTimeMins = addTimeMins - diffMins;
			}
		}
	}

	// Method to skip Non-Business Hours (Before 9 AM and After 6 PM)

	public void adjustToBusinessHours(Calendar givenDateTime) {

		int hourOfDay = givenDateTime.get(Calendar.HOUR_OF_DAY);

		if (hourOfDay < BUSINESS_START_HOUR) // Skip to 9 AM on that Day
		{

			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}

		if (hourOfDay >= BUSINESS_END_HOUR) // Skip to Next Day 9 AM
		{

			givenDateTime.add(Calendar.DATE, 1);
			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}

		skipHolidays(givenDateTime); // skip Holidays
	}

	// Method to skip Holidays ( Weekends and General )

	public void skipHolidays(Calendar givenDateTime) {
		skipGeneralHolidays(givenDateTime); // General Holidays before Weekends

		if (givenDateTime.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {

			givenDateTime.add(Calendar.DATE, 2); // Saturday and Sunday
													// in case of Business Hour
			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}

		if (givenDateTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {

			givenDateTime.add(Calendar.DATE, 1); // Sunday
													// in case of Business Hour
			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}

		skipGeneralHolidays(givenDateTime); // General Holidays after Weekends
	}

	// Method to skip General Holidays

	public void skipGeneralHolidays(Calendar givenDateTime) {
		// Considering only given Date without Time (Temp)
		Calendar givenDate = Calendar.getInstance();
		givenDate.setTime(givenDateTime.getTime());
		givenDate.set(Calendar.HOUR_OF_DAY, 0);
		givenDate.set(Calendar.MINUTE, 0);
		givenDate.set(Calendar.SECOND, 0);
		givenDate.set(Calendar.MILLISECOND, 0);

		// List of General Holidays
		GregorianCalendar[] generalHolidays = getGeneralHolidays(givenDate);

		for (int i = 0; i < generalHolidays.length; i++) // Check for all
															// Holidays
		{

			if (givenDate.equals(generalHolidays[i])) {

				givenDateTime.add(Calendar.DATE, 1); // Add one date to Actual
														// Date & Time
														// in case of Business
														// Hour
				givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
				givenDateTime.clear(Calendar.MINUTE);

				givenDate.add(Calendar.DATE, 1); // Add one date to Temp Date
			}
		}
	}

	// General Holidays list

	public GregorianCalendar[] getGeneralHolidays(Calendar givenDate) {

		GregorianCalendar[] genHolidays = {

		new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.JANUARY, 14), new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.JANUARY, 26),
				new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.MAY, 1), new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.AUGUST, 15),
				new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.OCTOBER, 2), new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.NOVEMBER, 1),
				new GregorianCalendar(givenDate.get(Calendar.YEAR), Calendar.DECEMBER, 25)

		};

		return genHolidays;
	}
}