package com.gvg.syena.core.services.dependency;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.services.dependency.analysis.DependencyAnalyzerImpl;
import com.gvg.syena.core.services.dependency.api.DependencyAnalyzer;

public class DependencyAnalyzerTest {

	private DependencyAnalyzer dependencyAnalyzer;

	@Before
	public void before() {
		dependencyAnalyzer = new DependencyAnalyzerImpl();
	}

	private Job getJob(long id) {
		Job job = new Job("Jo2" + id, "job" + id);
		job.setId(id);
		return job;
	}

	private void addDependencyJobs(Job job, Job job1) {
		job.getPredecessorDependency().getFinishToStart().addJob(job1);
	}

	private void addDependencyJobsWithanalyze(Job job, Job job1) throws DependencyFailureException {
		List<DependencyAttachableHierarchy> dependents = new ArrayList<>();
		dependents.add(job1);
		DependencyType dependencyType = DependencyType.FinishToStart;
		dependencyAnalyzer.analyzeDependency(job, dependents, dependencyType);
	}

	@Test
	public void test1() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobsWithanalyze(job1, job2);

	}

	@Test
	public void test2() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		Job job3 = getJob(3);
		addDependencyJobsWithanalyze(job2, job3);
	}

	@Test(expected = DependencyFailureException.class)
	public void test3() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		addDependencyJobsWithanalyze(job2, job1);
	}

	@Test
	public void test4() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		Job job3 = getJob(3);
		addDependencyJobsWithanalyze(job1, job3);
	}

	@Test
	public void test5() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		Job job3 = getJob(3);
		addDependencyJobs(job1, job3);
		addDependencyJobsWithanalyze(job3, job2);
	}

	@Test(expected = DependencyFailureException.class)
	public void test6() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		Job job3 = getJob(3);
		addDependencyJobs(job2, job3);
		addDependencyJobsWithanalyze(job3, job1);
	}

	@Test(expected = DependencyFailureException.class)
	public void test7() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		Job job3 = getJob(3);
		addDependencyJobs(job1, job3);
		addDependencyJobsWithanalyze(job3, job1);
	}

	@Test(expected = DependencyFailureException.class)
	public void test8() throws DependencyFailureException {
		Job job1 = getJob(1);
		Job job2 = getJob(2);
		addDependencyJobs(job1, job2);
		Job job3 = getJob(3);
		addDependencyJobs(job2, job3);
		addDependencyJobsWithanalyze(job3, job2);
	}

}
