package com.gvg.syena.util;

import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

public class ReadData {
	private XSSFSheet sheet;

	private HierarchyLevel[] hierarchyLevel = new HierarchyLevel[6];
	private HierarchyNode[] HierarchyNodeParents = new HierarchyNode[hierarchyLevel.length];

	public ReadData() throws IOException {

		XSSFWorkbook wb = new XSSFWorkbook("E://MappedData1.xlsx");
		this.sheet = wb.getSheetAt(5);

		hierarchyLevel[0] = new HierarchyLevel("Plant");
		hierarchyLevel[1] = new HierarchyLevel("Unit");
		hierarchyLevel[2] = new HierarchyLevel("System");
		hierarchyLevel[3] = new HierarchyLevel("Equipments");
		hierarchyLevel[4] = new HierarchyLevel("loops");
		hierarchyLevel[5] = new HierarchyLevel("subloops");

	}

	public void read() {

		Iterator<Row> rowItrator = sheet.iterator();
		while (rowItrator.hasNext()) {
			XSSFRow row = (XSSFRow) rowItrator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				XSSFCell cell = (XSSFCell) cellIterator.next();
				String name = cell.getStringCellValue();
				if (name != null && name != "") {
					int columnIndex = cell.getColumnIndex();
					HierarchyNode hierarchyNode = new HierarchyNode(name, name, hierarchyLevel[columnIndex], 0);
					System.out.println(hierarchyNode.getHierarchyLevel().getLevelName() + " : " + hierarchyNode.getName());
					if (columnIndex > 0) {
						HierarchyNodeParents[columnIndex - 1].addChild(hierarchyNode);
					}
					HierarchyNodeParents[cell.getColumnIndex()] = hierarchyNode;
				}
			}
		}
	}

	public static void main(String[] args) throws IOException {
		ReadData r = new ReadData();
		r.read();

	}
}
