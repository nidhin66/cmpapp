package com.gvg.syena.util;

import java.util.List;

import org.junit.Test;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;

public class TestDataInsert {

	private int projectId = 1;

	private HierarchyNodeRepository hierarchyNodeRepository;

	@Test
	public void testName() throws Exception {

		HierarchyLevel plantLevel = new HierarchyLevel("plant");
		HierarchyLevel unitLevel = new HierarchyLevel("unit");
		plantLevel.addChild(unitLevel);
		HierarchyLevel systemLevel = new HierarchyLevel("system");
		unitLevel.addChild(systemLevel);
		HierarchyLevel subSystemLevel = new HierarchyLevel("subSystem");
		systemLevel.addChild(subSystemLevel);
		HierarchyLevel loopEquLevel = new HierarchyLevel("loopEqu");
		subSystemLevel.addChild(loopEquLevel);

		HierarchyNode plant = new HierarchyNode("plant", "plant", plantLevel, projectId);

		createUnit(plant.getId(), 1, plantLevel);

	}

	private void createUnit(long id, int num, HierarchyLevel level) {
		// HierarchyNode node = hierarchyNodeRepository.findOne(id);
		long[] ids = new long[num];
		for (int i = 0; i < num; i++) {
			HierarchyNode unit = new HierarchyNode(level.getLevelName() + i, level.getLevelName(), level, projectId);
			// node.addChild(unit);
			ids[i] = unit.getId();

		}
		System.out.println(level.getLevelName());
		for (long id1 : ids) {
			List<HierarchyLevel> children = level.getChildren();
			if (children != null) {
				for (HierarchyLevel child : children) {
					createUnit(id1, num * 10, child);
				}
			}
		}

	}

	// private void createSystem() {
	// for (int s = 0; s < 10; s++) {
	// HierarchyNode system = new HierarchyNode("system" + s, "system",
	// plantLevel, projectId);
	// for (int b = 0; b < 100; b++) {
	// HierarchyNode subSystem = new HierarchyNode("subSystem" + b, "subSystem",
	// plantLevel, projectId);
	// for (int l = 0; l < 100; l++) {
	// HierarchyNode loopEqu = new HierarchyNode("loopEqu" + l, "loopEqu",
	// plantLevel, projectId);
	// for (int j = 0; j < 10; j++) {
	// Job job = new Job("job" + 1, "job");
	// loopEqu.addJob(job);
	// System.out.println("Job");
	// }
	// subSystem.addChild(loopEqu);
	// System.out.println("loopEqu");
	// }
	// system.addChild(subSystem);
	// System.out.println("subSystem");
	// }
	// unit.addChild(system);
	// System.out.println("system");
	// }
	//
	// }

}
