package com.gvg.syena.core.application;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JPAConfig.class)
public class ApplicationConfigTest {

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void test() {
		// fail("Not yet implemented");
	}

}
