package com.gvg.syena.core.services.schedule;

import java.util.Calendar;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.JobsBarrierException;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class ScheduleManagmentImplTest {

	@Autowired
	private ScheduleManagment scheduleManagment;

	@Autowired
	private JobManagement jobManagement;

	// @Test
	@Transactional
	public void testSetSchedule() throws SchedulingException, JobNotFoundException, JobScheduleFailed, InvalidDataException, JobsBarrierException {
		// jobManagement.createJobFromStandardJob(1885);

		long jobId = 131072;
		Job job = jobManagement.getJob(jobId);
		WorkSchedule timeSchedule = job.getWorkSchedule();
		ScheduledStage first = timeSchedule.getScheduledStages().first();
		Calendar calendar = Calendar.getInstance();
		timeSchedule.changePlannedStartTime(calendar.getTime());
		calendar.add(Calendar.DAY_OF_YEAR, 10);
		timeSchedule.changePlannedFinishTime(calendar.getTime());
		scheduleManagment.setJobSchedule(jobId, timeSchedule, 5);
	}

	@Test
	@Transactional
	public void setJobGroupSchedule() throws SchedulingException, JobNotFoundException, JobScheduleFailed, InvalidDataException, JobsBarrierException {

		Calendar calendar = Calendar.getInstance();
		WorkTime workTime = new WorkTime(calendar.getTime(), new PersonDays(5));
		WorkSchedule timeSchedule = new WorkSchedule(workTime);
		timeSchedule.changePlannedStartTime(calendar.getTime());
		calendar.add(Calendar.DAY_OF_YEAR, 10);
		timeSchedule.changePlannedFinishTime(calendar.getTime());
		long jobGroupId = 32769;
		scheduleManagment.setJobGroupSchedule(jobGroupId, timeSchedule, 10);
	}
}
