package com.gvg.syena.core.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class SearchJob_JobGroupTest {

	@Autowired
	private JobManagement jobManagement;

	// @Test
	public void searchStandardJob() {
		JobProperties jobSearchOptions = new JobProperties();
		jobSearchOptions.setStandard(true);
		jobSearchOptions.setName("tas%");
		PageItr<Job> res = jobManagement.searchJobWithProperties(jobSearchOptions, 0, 10);
		System.out.println();

	}

	// @Test
	public void searchJob() {
		JobProperties jobSearchOptions = new JobProperties();
		jobSearchOptions.setStandard(false);
		jobSearchOptions.setName("PUR%");
		jobSearchOptions.setConnectedNodeId(367);
		PageItr<Job> res = jobManagement.searchJobWithProperties(jobSearchOptions, 0, 10);
		System.out.println();

	}

	// @Test
	public void searchStandardJobGroup() {
		JobGroupProperties jobGroupProperties = new JobGroupProperties();
		jobGroupProperties.setStandard(true);
		jobGroupProperties.setName("new%");
		PageItr<JobGroup> res = jobManagement.searchJobGroupWithProperties(jobGroupProperties, 0, 10);
		System.out.println();

	}

//	@Test
	public void searchJobGroup() {
		JobGroupProperties jobGroupProperties = new JobGroupProperties();
		jobGroupProperties.setStandard(false);
		jobGroupProperties.setName("new%");
		jobGroupProperties.setConnectedNodeId(335);
		PageItr<JobGroup> res = jobManagement.searchJobGroupWithProperties(jobGroupProperties, 0, 10);
		System.out.println();

	}

	@Test
	public void searchjobhier() {
		long[] jobIds = { 98304, 98313, 98322 };
		jobManagement.getConnectedHierarchyNode(jobIds, 0, 10);

	}

}
