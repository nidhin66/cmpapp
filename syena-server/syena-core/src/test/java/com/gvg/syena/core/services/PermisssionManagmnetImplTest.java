package com.gvg.syena.core.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.JobsBarrierException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;
import com.gvg.syena.core.api.services.PermisssionManagmnet;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class PermisssionManagmnetImplTest {

	@Autowired
	private PermisssionManagmnet permisssionManagmnet;

	@Autowired
	private ScheduleManagment scheduleManagment;

	@Autowired
	private JobRepository jobRepository;

	public void testCheckPermission() throws PersonNotFoundException, HierarchyNodeNotFoundException, JobNotFoundException {

		String userId = "GVG001";

		ActivityArea activityArea = ActivityArea.schedule;
		List<ActivityOperation> activityOperations = new ArrayList<ActivityOperation>();
		activityOperations.add(ActivityOperation.create_new);
		activityOperations.add(ActivityOperation.create_approve);
		permisssionManagmnet.setPermissionUser(userId, activityArea, activityOperations);

		String departmentId = "1";
		boolean permission = permisssionManagmnet.checkPermission(userId, departmentId, activityArea, ActivityOperation.create_approve);
		System.out.println(permission);

		int pageNumber = 0;
		int pageSize = 10;
		scheduleManagment.getPendingForApprovelJobSchedules(pageNumber, pageSize);
		scheduleManagment.getPendingForApprovelJobGroupSchedules(pageNumber, pageSize);
		String level = "Unit";
		scheduleManagment.getPendingForApprovelSchedules(level, pageNumber, pageSize);

		long hierarchyId = 211;
		WorkTime workTime = new WorkTime(new Date(), new PersonDays(10));
		scheduleManagment.setUnApprovedHierarchybSchedule(hierarchyId, workTime);
		PageItr<HierarchyNode> hierarchyNodes = scheduleManagment.getPendingForApprovelSchedules(level, pageNumber, pageSize);
		System.out.println();
		// long jobGrouId;
		// scheduleManagment.setUnApprovedJobGroupSchedule(jobGrouId, workTime);
		// long jobId;
		// WorkSchedule workSchedule = new WorkSchedule(workTime);
		// scheduleManagment.setUnApprovedJobSchedule(jobId, workSchedule);

	}

	// @Test
	@Transactional
	public void a2() throws JobNotFoundException {
		Job job = jobRepository.findOne(32768L);
		WorkTime workTime = new WorkTime(new Date(), new PersonDays(10));
		WorkSchedule workSchedule = new WorkSchedule(workTime);
		// WorkSchedule workSchedule = job.getWorkSchedule();
		scheduleManagment.setUnApprovedJobSchedule(job.getId(), workSchedule, 10);
	}

//	@Test
	@Transactional
	public void a3() throws JobNotFoundException, SchedulingException, JobScheduleFailed, InvalidDataException, JobsBarrierException {
		// PageItr<Job> jobSchedules =
		// scheduleManagment.getPendingForApprovelJobSchedules(0, 10);
		// Job job = jobSchedules.getContent().get(0);
		// int personDays = 5;
		Job job = jobRepository.findOne(32768L);
		WorkTime workTime = new WorkTime(new Date(), new PersonDays(15));
		WorkSchedule workSchedule = new WorkSchedule(workTime);
		scheduleManagment.setJobSchedule(job.getId(), workSchedule, 10);
	}


	@Test
	@Transactional
	public void a4() throws JobNotFoundException, SchedulingException, JobScheduleFailed, InvalidDataException, JobsBarrierException {

		WorkTime workTime = new WorkTime(new Date(), new PersonDays(15));
		scheduleManagment.setJobGroupSchedule(32768, workTime);
	}
}
