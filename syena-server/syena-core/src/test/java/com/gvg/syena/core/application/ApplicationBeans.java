package com.gvg.syena.core.application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gvg.syena.core.api.services.ApplicationService;
import com.gvg.syena.core.api.services.CheckListManagement;
import com.gvg.syena.core.api.services.DependencyManagmnet;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.HierarchyNodeDependencyManegmanet;
import com.gvg.syena.core.api.services.PermisssionManagmnet;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.api.services.StatusUpdationService;
import com.gvg.syena.core.api.services.UserManagemnet;
import com.gvg.syena.core.api.services.chart.ChartsDataBuilder;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.api.services.serach.AdvancedJobGroupSearch;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.services.ApplicationServiceImpl;
import com.gvg.syena.core.services.HierarchyManagementImpl;
import com.gvg.syena.core.services.JobManagementImpl;
import com.gvg.syena.core.services.chart.ChartsDataBuilderImpl;
import com.gvg.syena.core.services.checklist.CheckListManagementImpl;
import com.gvg.syena.core.services.criticalpath.CriticalPathFinder;
import com.gvg.syena.core.services.criticalpath.CriticalPathFinderImpl;
import com.gvg.syena.core.services.dataupload.DataUpLoader;
import com.gvg.syena.core.services.dataupload.DataUpLoaderImpl;
import com.gvg.syena.core.services.dataupload.TestDataInsertService;
import com.gvg.syena.core.services.dataupload.TestDataInsertServiceImpl;
import com.gvg.syena.core.services.dependency.DependencyManagmnetImpl;
import com.gvg.syena.core.services.dependency.HierarchyNodeDependencyConnector;
import com.gvg.syena.core.services.dependency.HierarchyNodeDependencyManegmanetImpl;
import com.gvg.syena.core.services.dependency.analysis.DependencyAnalyzerImpl;
import com.gvg.syena.core.services.dependency.api.DependencyAnalyzer;
import com.gvg.syena.core.services.organization.OrganizationManagemnetImpl;
import com.gvg.syena.core.services.organization.PermisssionManagmnetImpl;
import com.gvg.syena.core.services.schedule.HierarchyScheduler;
import com.gvg.syena.core.services.schedule.JobScheduler;
import com.gvg.syena.core.services.schedule.ScheduleManagmentImpl;
import com.gvg.syena.core.services.schedule.StatusUpdationServiceImpl;
import com.gvg.syena.core.services.search.AdvancedJobGroupSearchDao;
import com.gvg.syena.core.services.search.AdvancedJobGroupSearchImpl;
import com.gvg.syena.core.services.search.AdvancedJobSearchDao;
import com.gvg.syena.core.services.search.AdvancedJobSearchImpl;
import com.gvg.syena.core.services.search.SearchDao;

@Configuration
public class ApplicationBeans {

	@Bean(name = "hierarchyManagement")
	public HierarchyManagement hierarchyManagement() {

		return new HierarchyManagementImpl();
	}

	@Bean
	public UserManagemnet organizationManagemnet() {
		return new OrganizationManagemnetImpl();
	}

	@Bean
	public ScheduleManagment ScheduleManagment() {
		return new ScheduleManagmentImpl();

	}

	@Bean(name = "jobmanagement")
	public JobManagement jobManagement() {
		return new JobManagementImpl();
	}

	@Bean(name = "dataUpLoad")
	public DataUpLoader dataUpLoad() {
		return new DataUpLoaderImpl();

	}

	@Bean(name = "searchDao")
	public SearchDao searchDao() {
		return new SearchDao();

	}

	@Bean(name = "advancedJobSearch")
	public AdvancedJobSearch preDefinedSearch() {
		return new AdvancedJobSearchImpl();
	}

	@Bean(name = "checkListManagement")
	public CheckListManagement checkListManagement() {
		return new CheckListManagementImpl();

	}

	// @Bean(name = "statusComputationService")
	// public StatusUpdationService statusComputationService() {
	// return new StatusUpdationService(StatusComputationFactor.Least);
	//
	// }

	@Bean(name = "dependencyAnalyzer")
	public DependencyAnalyzer dependencyAnalyzer() {
		return new DependencyAnalyzerImpl();
	}

	@Bean(name = "scheduleManagment")
	public ScheduleManagment scheduleManagment() {
		return new ScheduleManagmentImpl();

	}

	@Bean(name = "advancedJobSearch")
	public AdvancedJobSearch advancedSearch() {
		return new AdvancedJobSearchImpl();

	}

	@Bean(name = "testDataInsertService")
	public TestDataInsertService testDataInsertService() {
		return new TestDataInsertServiceImpl();

	}

	@Bean(name = "chartsDataBuilder")
	public ChartsDataBuilder chartsDataManagement() {
		return new ChartsDataBuilderImpl();

	}

	@Bean(name = "criticalPathFinder")
	public CriticalPathFinder criticalPathFinder() {
		return new CriticalPathFinderImpl();
	}

	@Bean(name = "statusUpdationService")
	public StatusUpdationService statusUpdationService() {
		return new StatusUpdationServiceImpl();
	}

	@Bean(name = "permisssionManagmnet")
	public PermisssionManagmnet permisssionManagmnet() {
		return new PermisssionManagmnetImpl();

	}

	@Bean(name = "hierarchyScheduler")
	public HierarchyScheduler HierarchyScheduler() {
		return new HierarchyScheduler();
	}

	@Bean(name = "hierarchyNodeDependencyManegmanet")
	public HierarchyNodeDependencyManegmanet hierarchyNodeDependencyManegmanet() {
		return new HierarchyNodeDependencyManegmanetImpl();
	}

	@Bean(name = "hierarchyNodeDependencyAnalyzer")
	public HierarchyNodeDependencyConnector hierarchyNodeDependencyAnalyzer() {
		return new HierarchyNodeDependencyConnector();
	}

	@Bean(name = "dependencyManagmnet")
	public DependencyManagmnet DependencyManagmnet() {
		return new DependencyManagmnetImpl();
	}

	@Bean(name = "jobScheduler")
	public JobScheduler jobScheduler() {
		return new JobScheduler();
	}

	@Bean(name = "advancedJobSearchDao")
	public AdvancedJobSearchDao advancedSearchDao() {
		return new AdvancedJobSearchDao();
	}

	@Bean(name = "advancedJobGroupSearchService")
	public AdvancedJobGroupSearch advancedJobGroupSearchService() {
		return new AdvancedJobGroupSearchImpl();
	}

	@Bean(name = "aplicationService")
	public ApplicationService ApplicationService() {
		return new ApplicationServiceImpl();
	}

	@Bean(name = "advancedJobGroupSearchDao")
	public AdvancedJobGroupSearchDao AdvancedJobGroupSearchDao() {
		return new AdvancedJobGroupSearchDao();
	}
}
