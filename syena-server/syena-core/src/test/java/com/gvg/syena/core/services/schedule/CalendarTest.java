package com.gvg.syena.core.services.schedule;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import net.objectlab.kit.datecalc.common.DateCalculator;
import net.objectlab.kit.datecalc.common.DefaultHolidayCalendar;
import net.objectlab.kit.datecalc.common.HolidayCalendar;
import net.objectlab.kit.datecalc.common.StandardTenor;
import net.objectlab.kit.datecalc.jdk8.LocalDateKitCalculatorsFactory;

import org.junit.Test;

public class CalendarTest {

	@Test
	public void test() {
		// create or get the Holidays
		final Set<LocalDate> holidays = new HashSet<LocalDate>();
		// holidays.add(LocalDate.parse("2006-08-28"));
		// ... keep adding all holidays for 2006

		// create the HolidayCalendar ASSUMING that the set covers 2006!
		final HolidayCalendar<LocalDate> calendar = new DefaultHolidayCalendar<LocalDate>(holidays, LocalDate.parse("2010-01-01"), LocalDate.parse("2020-12-31"));

		// register the holidays, any calculator with name "UK"
		// asked from now on will receive an IMMUTABLE reference to this
		// calendar
		LocalDateKitCalculatorsFactory.getDefaultInstance().registerHolidays("UK", calendar);

		// ask for a LocalDate calculator for "UK"
		// (even if a new set of holidays is registered, this one calculator is
		// not affected
		DateCalculator<LocalDate> cal = LocalDateKitCalculatorsFactory.forwardCalculator("UK");

		// set startDate, this will also set the current business date.
		// cal.setStartDate(LocalDate.parse("2006-08-28"));

		// startDate stays 28 Aug 06 BUT the currentDate has moved,
		// according to Forward handler to 29 Aug 2006.
		LocalDate start = cal.getStartDate(); // 28 Aug 06
		LocalDate current = cal.getCurrentBusinessDate(); // 29 Aug 06

		LocalDate newCurrent = cal.moveByDays(-4).getCurrentBusinessDate(); // 4
																			// Sept
																			// 06
																			// due
																			// to
																			// weekend!
		System.out.println(newCurrent);
		// Example with Tenor, 1W with a 2 day spot lag
		LocalDate date1WeekFromSpot = cal.moveByTenor(StandardTenor.T_1W, 2).getCurrentBusinessDate();
	}

}
