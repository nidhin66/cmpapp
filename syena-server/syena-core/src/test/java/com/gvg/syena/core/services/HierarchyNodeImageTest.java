package com.gvg.syena.core.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.ImageIOException;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class HierarchyNodeImageTest {

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Test
	public void test() throws HierarchyNodeNotFoundException, ImageIOException, IOException {

		long hierarchyNodeId = 210;
		String imageName = "image1";
		File file = new File("C:\\Users\\geeva_000\\Desktop\\28f119f.jpg");

		FileInputStream inputStream = new FileInputStream(file);
		byte[] bytes = new byte[1024 * 100];
		inputStream.read(bytes);
		DocumentType documentType = DocumentType.Images;
		hierarchyManagement.uploadHierarchyNodeImage(hierarchyNodeId, imageName, bytes, documentType);

		hierarchyManagement.getHierarchyNodeImage(hierarchyNodeId, imageName, documentType);
	}
}
