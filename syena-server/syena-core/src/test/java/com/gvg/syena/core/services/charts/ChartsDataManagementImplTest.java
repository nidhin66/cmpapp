
package com.gvg.syena.core.services.charts;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.NotScheduledException;
import com.gvg.syena.core.api.services.chart.ChartsDataBuilder;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class ChartsDataManagementImplTest {

	@Autowired
	private ChartsDataBuilder chartsDataBuilder;

	@Test
	public void testGetChartsDataLongChartTypeArrayDateDate() throws HierarchyNodeNotFoundException, NotScheduledException {
		long hierarchyNodeId = 218;
		ChartType[] chartTypes = { ChartType.Planned, ChartType.Actual, ChartType.Revised };
		Calendar calendar = Calendar.getInstance();
		Date fromTime = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, 32);
		Date toTime = calendar.getTime();
		ChartData<Date> data = chartsDataBuilder.getChartsDataOfHierarchyNode(hierarchyNodeId, chartTypes);
		System.out.println(data);
	}

//	// @Test
//	public void testGetPredictionChartTypes() throws HierarchyNodeNotFoundException, NotScheduledException {
//		long hierarchyNodeId = 211;
//		Calendar calendar = Calendar.getInstance();
//		Date fromTime = calendar.getTime();
//		calendar.add(Calendar.DAY_OF_YEAR, 32);
//		Date toTime = calendar.getTime();
//		PredictionChartType[] predictionChartTypes = { PredictionChartType.workefficiency };
//		ChartData<Date> data = chartsDataManagement.getChartsData(hierarchyNodeId, predictionChartTypes);
//		System.out.println(data);
//	}
}
