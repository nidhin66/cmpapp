package com.gvg.syena.core.services;

import org.junit.Test;

import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

public class StatusComputationServiceTest {

	// private StatusUpdationService statusComputationService = new
	// StatusUpdationService(StatusComputationFactor.Least);

	// @Test
	public void test() {

		// Job job = new Job("job1", "Job1");
		// statusComputationService.computeUpwardStatus(job,
		// StatusComputationFactor.Least);

		HierarchyNode hierarchyNode = new HierarchyNode("name", "description", new HierarchyLevel("system"), 1);
		HierarchyNode childNode1 = new HierarchyNode("childNode1", "childNode1", new HierarchyLevel("SubSystem"), 1);
		childNode1.setWorkStatus(new WorkStatus(false, false, RunningStatus.completed));
		hierarchyNode.addChild(childNode1);
		HierarchyNode childNode2 = new HierarchyNode("childNode2", "childNode2", new HierarchyLevel("SubSystem"), 1);
		childNode2.setWorkStatus(new WorkStatus(false, true, RunningStatus.completed));
		hierarchyNode.addChild(childNode2);

		// statusComputationService.computeUpwardStatus(hierarchyNode, new
		// WorkStatus(false, false, RunningStatus.completed),
		// StatusComputationFactor.Least);
	}

	@Test
	public void test2() {
		HierarchyNode HN1 = new HierarchyNode("name", "description", new HierarchyLevel("1"), 1);
		HierarchyNode HN2 = new HierarchyNode("name", "description", new HierarchyLevel("2"), 1);
		HierarchyNode HN3 = new HierarchyNode("name", "description", new HierarchyLevel("3"), 1);
		HierarchyNode HN4 = new HierarchyNode("name", "description", new HierarchyLevel("3"), 1);
		JobGroup<Job> group1 = new JobGroup<Job>("1");
		JobGroup<Job> group2 = new JobGroup<Job>("2");
		Job job1 = new Job("1", "1");
		Job job2 = new Job("2", "2");
		Job job3 = new Job("3", "3");
		Job job4 = new Job("4", "4");

		HN1.addChild(HN2);
		HN2.addChild(HN3);
		HN2.addChild(HN4);

		HN3.addJobGroup(group1);
		HN4.addJobGroup(group2);

		HN3.addJob(job1);
		HN3.addJob(job2);

		HN4.addJob(job3);
		group2.addJob(job4);

		WorkStatus workStatus = new WorkStatus(true, false, RunningStatus.completed);
		// StatusComputationService.computeUpwardStatus(job4);
		HN1.getWorkStatus();

		WorkStatus workStatus2 = new WorkStatus(true, false, RunningStatus.notStarted);
		// statusComputationService.computeUpwardStatus(job4);
		HN1.getWorkStatus();

	}

}
