package com.gvg.syena.core.services.search;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.api.services.serach.ValueComparitor;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class AdvancedSearchImplTest {

	@Autowired
	private AdvancedJobSearch advancedJobSearch;

	// @Test
	public void test() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		Date date = c.getTime();
		// advancedSearch.getCompletedJobs(date, 0, 10);
		// advancedSearch.getJobsInitiatedBy("GVG001", 0, 10);
		advancedJobSearch.getJobsOwnedBy("GVG001", "", 0, 10);

		// JobProperties advancedSearchOptions = new JobProperties();
		// // advancedSearchOptions.setNamePart("");
		//
		// PageItr<Job> jobs = advancedSearch.searchJobs(advancedSearchOptions,
		// 0, 5);
		// System.out.println(jobs);
	}

	// @Test
	public void jobsToBeScheduled() {
		advancedJobSearch.jobsToBeScheduled("%", 0, 10);

	}

	// @Test
	public void jobsWithRevisedDates() {
		advancedJobSearch.jobsWithRevisedDates("%", 0, 10);

	}
	//
	// @Test
	// public void jobGroupwithDelyedStartJobs() {
	// advancedSearch.jobGroupwithDelyedStartJobs(212,
	// ValueComparitor.greaterOrEqual, 25, 0, 10);
	//
	// }
}
