package com.gvg.syena.core.management;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class CheckListTest {

	@Autowired
	private JobManagement jobManagement;
	

	@Test
	public void test() throws JobNotFoundException {

		long jobGroupId = 1;
		long[] standardCheckListItems = {1, 2};
		jobManagement.createAndLinkFromStandardCheckListToJobGroup(jobGroupId, standardCheckListItems );
	}

}
