package com.gvg.syena.core.services.schedule;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.exception.JobScheduleFailed;

public class JobSchedulerTest {

	private DependencyType dependencyType = DependencyType.FinishToStart;
	private Date today;
	private Date tomorrow;

	@Before
	public void before() {
		Calendar calendar = Calendar.getInstance();
		Date thu = new Date();
		thu.setHours(9);
		thu.setMinutes(30);
		calendar.setTime(thu);
		today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		tomorrow = calendar.getTime();

	}

	// @Test
	public void test1() throws JobScheduleFailed {

		Job job1 = new Job("1", "1");
		Job job2 = new Job("2", "2");

		// job1.setWorkTime(new Date(), new PersonDays(2));
		// job2.setWorkTime(new Date(), new PersonDays(3));

		job1.addSuccessorDependency(job2, dependencyType);

//		JobScheduler.changeStartDate(job1, tomorrow);

//		System.out.println(job1.getWorkTime());
//		System.out.println(job2.getWorkTime());
	}

	@Test
	public void test() throws JobScheduleFailed {

		Job job1 = new Job("1", "1");
		Job job2 = new Job("2", "2");
		Job job3 = new Job("3", "3");

		// job1.setWorkTime(new Date(), new PersonDays(2));
		// job2.setWorkTime(new Date(), new PersonDays(3));
		// job3.setWorkTime(new Date(), new PersonDays(5));

		job3.addPredecessorDependencyJob(job2, dependencyType);
		job2.addPredecessorDependencyJob(job1, dependencyType);

//		JobScheduler.changeStartDate(job2, tomorrow);

//		System.out.println(job1.getWorkTime());
//		System.out.println(job2.getWorkTime());
//		System.out.println(job3.getWorkTime());
	}

}
