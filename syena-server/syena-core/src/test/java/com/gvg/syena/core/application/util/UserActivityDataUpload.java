package com.gvg.syena.core.application.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.organization.Department;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.services.dataupload.DataUpLoader;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class UserActivityDataUpload {
	
	@Autowired
	private DataUpLoader dataUpLoad;
	
	@Test
	public void uploadData(){
//		UserActivityPermission actitivyPermission = new UserActivityPermission();
//		actitivyPermission.setActivityArea(ActivityArea.approval);
//		List<ActivityOperation> operations = new ArrayList<>();
//		operations.add(ActivityOperation.create_approve);
//		operations.add(ActivityOperation.update_approve);
//		operations.add(ActivityOperation.delete_approve);
//		actitivyPermission.setActivityOperations(operations);
//		dataUpLoad.uploadUserActivityPermission(actitivyPermission);
		
		Person person = new Person();
		person.setId("sudeepcm");
		person.setPassword("ModmkiSvgStMAdlmx61XmQ==");
		person.setEmailAddress("sudeep.mohandas@gamillusvaluegen.com");
		Map<Department, Role> rolesInDepartment = new HashMap<>();
		Department department = new Department();
		department.setId(1);
		department.setName("Plant1");
		department.setDescription("PLANT");
		List<Person> persons = new ArrayList<>();
		persons.add(person);
		department.setUsers(persons);
		Role role1 = new Role();
		role1.setDescription("Unit head");
		role1.setRoleId("UNIT_HEAD");
		role1.setUsers(persons);		
		rolesInDepartment.put(department, role1);
		person.setRolesInDepartment(rolesInDepartment);
	}
	

}
