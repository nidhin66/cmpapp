package com.gvg.syena.core.services.search;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.application.ApplicationBeans;
import com.gvg.syena.core.utilities.JPAConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfig.class, ApplicationBeans.class })
public class SearchServiceImplTest {

	@Autowired
	private AdvancedJobSearch advancedJobSearch;

	@Test
	public void onGoingJobs() {
		PageItr<Job> res = advancedJobSearch.getOnGoingJobs(new Date(), "%", 0, 10);
		System.out.println(res);
	}

	@Test
	public void completedJobs() {
		advancedJobSearch.getCompletedJobs(new Date(), "%", 0, 10);
	}

	@Test
	public void plannedJobs() {
		PageItr<Job> res = advancedJobSearch.getPlannedJobs(new Date(), "%", 0, 10);
		System.out.println(res);
	}

	@Test
	public void delayedJobs() {
		Calendar c = Calendar.getInstance();
		c.set(2015, 8, 13);
		advancedJobSearch.getDelayedJobs(c.getTime(), "%", 0, 10);
	}

	@Test
	public void jobsInitiatedBy() {
		String personId = "gvg001";
		advancedJobSearch.getJobsInitiatedBy(personId, "%", 0, 10);
	}

	@Test
	public void jobsOwnedBy() {
		String personId = "gvg001";
		advancedJobSearch.getJobsOwnedBy(personId, "%", 0, 10);
	}

}
