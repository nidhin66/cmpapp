package com.gvg.syena.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TimeZoneTest {

	@Test
	public void testName() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		// mapper.getSerializationConfig().
		mapper.setDateFormat(dateFormat);

		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String dateStr = mapper.writeValueAsString(date);
		System.out.println(dateStr);
	}

}
