package com.gvg.syena.core.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.datarepository.checklist.CheckListItemRepository;
import com.gvg.syena.core.datarepository.checklist.CheckListRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.datarepository.job.StandardJobRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.AppUtil;

@Service("jobmanagement")
public class JobManagementImpl implements JobManagement {

	@Autowired
	private StandardJobRepository standardJobRepository;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private CheckListRepository checkListRepository;

	@Autowired
	private CheckListItemRepository checkListItemRepository;

	@Autowired
	private PersonRepository personRepository;

	@Override
	@Transactional
	public void createStandardJob(StandardJob job) throws InvalidDataException {
		job.validateOnCreate();
		StandardJob newJob = new StandardJob(job.getName(), job.getDescription());
		update(job, newJob);
		newJob.setNodeType(NodeType.standard);
		standardJobRepository.save(newJob);
	}

	@Transactional
	public void createStandardJobGroup(JobGroup<StandardJob> jobGroup) throws InvalidDataException {
		jobGroup.validateOnCreate();
		JobGroup<StandardJob> newJobGroup = new JobGroup<>(jobGroup.getName());
		update(jobGroup, newJobGroup);
		newJobGroup.setNodeType(NodeType.standard);
		jobGroupRepository.save(newJobGroup);

	}

	@Transactional
	public Job createJobFromStandardJob(long standardJobId) throws JobNotFoundException {
		StandardJob standardJob = standardJobRepository.findOne(standardJobId);
		if (standardJob == null) {
			throw new JobNotFoundException(standardJobId, JobNotFoundException.JOB, "JobNotFound");
		}
		Job job = new Job(standardJob, standardJob.getDescription());
		job.setNodeType(NodeType.instance);
		jobRepository.save(job);
		return job;
	}

	@Transactional
	public List<JobGroup<Job>> createJobGroupFromStandardJobGroup(long... standardJobGroupIds)
			throws JobNotFoundException {

		List<Long> ids = new ArrayList<Long>();
		for (long id : standardJobGroupIds) {
			ids.add(id);
		}
		Iterable<JobGroup> standardJobGroups = jobGroupRepository.findAll(ids);

		List<JobGroup<Job>> newJobs = new ArrayList<JobGroup<Job>>();
		for (JobGroup<StandardJob> standardJobGroup : standardJobGroups) {
			JobGroup<Job> jobGroup = new JobGroup<Job>(standardJobGroup);
			jobGroup.setNodeType(NodeType.instance);
			newJobs.add(jobGroup);
		}
		jobGroupRepository.save(newJobs);
		return newJobs;
	}

	@Transactional(rollbackOn = { Exception.class })
	public Job createJob(Job job) throws InvalidDataException {
		job.validateOnCreate();
		Job newJob = new Job(job.getName(), job.getDescription());
		update(job, newJob);
		newJob.setNodeType(NodeType.instance);
		job=	jobRepository.save(newJob);
		return job;
	}

	@Transactional(rollbackOn = { Exception.class })
	public JobGroup<Job> createJobGroup(JobGroup<Job> jobGroup) throws InvalidDataException {
		jobGroup.validateOnCreate();
		JobGroup<Job> newJobGroup = new JobGroup<>(jobGroup.getName());
		update(jobGroup, newJobGroup);
		newJobGroup.setNodeType(NodeType.instance);
		newJobGroup = jobGroupRepository.save(newJobGroup);
		return newJobGroup;
	}

	@Transactional
	public Job updateJob(Job job) throws JobNotFoundException, InvalidDataException {
		job.validateOnCreate();
		Job exJob = jobRepository.findOne(job.getId());
		if (exJob == null) {
			throw new JobNotFoundException(job.getId(), JobNotFoundException.JOB, "JobNotFound");
		}
		update(job, exJob);
		exJob.setNodeType(NodeType.instance);
		jobRepository.save(exJob);
		return exJob;
	}

	@Transactional
	public void updateJobGroup(JobGroup<Job> jobGroup) throws JobNotFoundException, InvalidDataException {
		jobGroup.validateOnCreate();
		JobGroup<Job> exJobGroup = jobGroupRepository.findOne(jobGroup.getId());
		if (exJobGroup == null) {
			throw new JobNotFoundException(jobGroup.getId(), JobNotFoundException.GROUP, "JobNotFound");
		}
		update(jobGroup, exJobGroup);
		exJobGroup.setNodeType(NodeType.instance);
		jobGroupRepository.save(exJobGroup);
	}

	@Transactional(rollbackOn = { Exception.class })
	public List<CheckList> createAndLinkFromStandardCheckListToJob(long jobId, long... standardCheckListItems)
			throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long id : standardCheckListItems) {
			ids.add(id);
		}
		Iterable<CheckList> checkLists = checkListRepository.findAll(ids);
		for (CheckList checkList : checkLists) {
			CheckList newCheckList = new CheckList(checkList);
			newCheckList.setAllStandard(false);
			newCheckList.setParent(job);
			job.addCheckList(newCheckList);
		}
		jobRepository.save(job);
		return AppUtil.convertToList(checkLists);
	}

	@Override
	@Transactional
	public List<CheckList> createAndLinkFromStandardCheckListToJob(long jobId, CheckListType checkListType,
			long[] standardCheckListItems) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long id : standardCheckListItems) {
			ids.add(id);
		}
		Iterable<CheckList> checkLists = checkListRepository.findAll(ids);
		for (CheckList checkList : checkLists) {
			CheckList newCheckList = new CheckList(checkList);
			newCheckList.setCheckListType(checkListType);
			newCheckList.setAllStandard(false);
			newCheckList.setParent(job);
			job.addCheckList(newCheckList);
		}
		jobRepository.save(job);
		return AppUtil.convertToList(checkLists);

	}

	@Transactional
	public void updateStandardJob(StandardJob job) throws JobNotFoundException, InvalidDataException {
		job.validateOnCreate();
		StandardJob exjob = standardJobRepository.findOne(job.getId());
		if (exjob == null) {
			throw new JobNotFoundException(job.getId(), JobNotFoundException.JOB, "JobNotFound");
		}
		update(job, exjob);
		exjob.setNodeType(NodeType.standard);
		standardJobRepository.save(exjob);
	}

	@Transactional
	public void updateStandardJobGroup(JobGroup<StandardJob> jobGroup)
			throws JobNotFoundException, InvalidDataException {
		jobGroup.validateOnCreate();
		JobGroup exJobGroup = jobGroupRepository.findOne(jobGroup.getId());
		if (exJobGroup == null) {
			throw new JobNotFoundException(jobGroup.getId(), JobNotFoundException.GROUP, "JobNotFound");
		}
		update(jobGroup, exJobGroup);
		exJobGroup.setNodeType(NodeType.standard);
		jobGroupRepository.save(exJobGroup);
	}

	@Transactional
	private <T extends StandardJob> void update(JobGroup<T> fromJobGroup, JobGroup<T> toJobGroup) {
		toJobGroup.setDescription(fromJobGroup.getDescription());
		toJobGroup.setOwner(fromJobGroup.getOwner());
		toJobGroup.setInitiator(fromJobGroup.getInitiator());
		toJobGroup.setExecutor(fromJobGroup.getExecutor());
		toJobGroup.setApplicableForComplete(fromJobGroup.isApplicableForComplete());
		toJobGroup.setWeightage(fromJobGroup.getWeightage());
		Set<T> jobs = fromJobGroup.getJobs();
		Set<T> ujobs = new TreeSet<>();
		if (jobs != null) {
			for (T job : jobs) {
				T newO = null;
				if (job instanceof Job) {
					if (job.getId() != 0) {
						newO = (T) jobRepository.findOne(job.getId());
					} else {
						newO = (T) new Job(job.getName(), job.getDescription());
						Job jo = (Job) newO;
						jo.setConnectedNode(toJobGroup.getConnectedNode());
						jo.getWorkSchedule().setScheduledStages(ApplicationUtil.getSystemDefaultScheduledStages());

						jobRepository.save(jo);
						toJobGroup.addJob(newO);

					}

				} else if (job instanceof StandardJob) {
					if (job.getId() != 0) {
						newO = (T) standardJobRepository.findOne(job.getId());
					} else {
						newO = (T) new StandardJob(job.getName(), job.getDescription());
						toJobGroup.addJob(newO);
						standardJobRepository.save(newO);
					}
				}
				if (job instanceof Job) {
					update((Job) job, (Job) newO);
				} else if (job instanceof StandardJob) {
					update((StandardJob) job, (StandardJob) newO);
				}
				ujobs.add(newO);
			}
		}
		toJobGroup.getJobs().clear();
		toJobGroup.getJobs().addAll(ujobs);
		jobGroupRepository.save(toJobGroup);

		Set<JobGroup<T>> jobGroups = fromJobGroup.getJobGroups();
		if (jobGroups != null) {
			for (JobGroup<T> jobGroup : jobGroups) {
				JobGroup<T> newJobGroup = null;
				if (jobGroup.getId() != 0) {
					newJobGroup = jobGroupRepository.findOne(jobGroup.getId());
				} else {
					newJobGroup = new JobGroup<T>(jobGroup.getName());
				}
				update(jobGroup, newJobGroup);
			}
		}

	}

	@Transactional
	public void addStandardJobToStandardJobGroup(long jobId, long jobGroupId) throws JobNotFoundException {

		JobGroup<StandardJob> jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobNotFound");
		}
		StandardJob job = standardJobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.JOB, "JobNotFound");
		}
		jobGroup.addJob(job);
		jobGroupRepository.save(jobGroup);

	}

	@Transactional
	public StandardJob getStandardJob(long jobId) {
		return standardJobRepository.findOne(jobId);
	}

	@Transactional
	public Job getJob(long jobId) {
		return jobRepository.findOne(jobId);
	}

	@Transactional
	private void update(StandardJob fromJob, StandardJob toJob) {
		toJob.setDescription(fromJob.getDescription());
		toJob.setWeightage(fromJob.getWeightage());
		toJob.setOwner(fromJob.getOwner());
		toJob.setApplicableForComplete(fromJob.isApplicableForComplete());

	}

	@Transactional
	private void update(Job fromJob, Job toJob) {
		toJob.setDescription(fromJob.getDescription());
		toJob.setWeightage(fromJob.getWeightage());
		toJob.setOwner(fromJob.getOwner());
		toJob.setApplicableForComplete(fromJob.isApplicableForComplete());

		toJob.setExecutor(fromJob.getExecutor());
		toJob.setInitiator(fromJob.getInitiator());
	}

	@Transactional
	public void addJobToJobGroup(long jobId, long jobGroupId) throws JobNotFoundException {
		JobGroup<Job> jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobNotFound");
		}
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.JOB, "JobNotFound");
		}
		jobGroup.addJob(job);
		job.setConnectedNode(jobGroup.getConnectedNode());
		jobGroupRepository.save(jobGroup);

	}

	@Transactional
	public PageItr<StandardJob> getAllStandardJobs(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(standardJobRepository.findAll(pageable));
	}

	@Transactional
	public PageItr<Job> getAllJobs(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobRepository.findByNodeType(NodeType.instance, pageable));
	}

	@Transactional
	public PageItr<JobGroup> getAllStandardJobGroup(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobGroupRepository.findAllByNodeType(NodeType.standard, pageable));
	}

	@Transactional
	public PageItr<JobGroup> getAllJobGroups(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobGroupRepository.findAllByNodeType(NodeType.instance, pageable));
	}

	@Transactional
	public void abandonJob(long jobId) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		jobRepository.save(job);
	}

	@Transactional
	public void abandonJobGrop(long jogGroupId) throws JobNotFoundException {
		JobGroup<StandardJob> jobGroup = jobGroupRepository.findOne(jogGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jogGroupId, JobNotFoundException.GROUP, "JobNotFound");
		}
		jobGroupRepository.save(jobGroup);
	}

	@Transactional
	public DependencyJobs getDependencyJobs(long jobId) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		DependencyJobs dependencyJobs = job.getPredecessorDependency();
		return dependencyJobs;
	}

	@Transactional
	public PageItr<CheckList> getCheckListsOfJob(long jobId, int pageNumber, int pageSize) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<CheckList> checkLists = checkListRepository.findAllByParentId(jobId, pageable);
		return AppUtil.convert(checkLists);

	}

	@Transactional
	public void unlinkCheckList(long jobId, long... checkListItemId) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long id : checkListItemId) {
			ids.add(id);
		}
		Iterable<CheckList> checkLists = checkListRepository.findAll(ids);
		for (CheckList checkListItem : checkLists) {
			job.removeCheckList(checkListItem);
		}
		checkListRepository.delete(checkLists);
		jobRepository.save(job);

	}

	@Override
	@Transactional
	public PageItr<Job> searchJobWithProperties(JobProperties jobSearchOptions, int pageNumber, int pageSize) {

		long id = jobSearchOptions.getJobId();
		String name = jobSearchOptions.getName();
		name = name == null ? "#?#" : name;
		String description = jobSearchOptions.getDescription();
		description = description == null ? "#?#" : description;
		String ownerId = jobSearchOptions.getOwnerId();
		ownerId = ownerId == null ? "#?#" : ownerId;
		String initiatorId = jobSearchOptions.getInitiatorId();
		initiatorId = initiatorId == null ? "#?#" : initiatorId;
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		long connectedNodeId = jobSearchOptions.getConnectedNodeId();

		Page<Job> res;

		if (jobSearchOptions.isStandard()) {
			res = standardJobRepository.findByIdOrNameLikeOrDescriptionLike(id, name, description, pageable);
		} else {
			res = jobRepository
					.findByIdAndConnectedNodeIdOrJobNameLikeIgnoreCaseOrNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrOwnerIdLikeIgnoreCaseOrInitiatorIdLikeIgnoreCaseAndNodeType(
							id, connectedNodeId, name, name, description, ownerId, initiatorId, NodeType.instance,
							pageable);
		}
		return AppUtil.convert(res);
	}

	@Transactional
	public List<JobGroup<Job>> onlyCreateJobGroupFromStandardJobGroup(long... standardJobGroupIds)
			throws JobNotFoundException {
		List<Long> ids = new ArrayList<Long>();
		for (long id : standardJobGroupIds) {
			ids.add(id);
		}
		Iterable<JobGroup> standardJobGroups = jobGroupRepository.findAll(ids);

		List<JobGroup<Job>> newJobs = new ArrayList<JobGroup<Job>>();
		for (JobGroup<StandardJob> standardJobGroup : standardJobGroups) {
			JobGroup<Job> jobGroup = new JobGroup<Job>(standardJobGroup);
			jobGroup.setNodeType(NodeType.instance);
			newJobs.add(jobGroup);
		}
		return newJobs;
	}

	@Transactional
	public List<CheckList> createAndLinkFromStandardCheckListToJobGroup(long jobGroupId, long[] standardCheckListItems)
			throws JobNotFoundException {
		JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long id : standardCheckListItems) {
			ids.add(id);
		}
		Iterable<CheckList> checkLists = checkListRepository.findAll(ids);
		for (CheckList checkList : checkLists) {
			CheckList newCheckList = new CheckList(checkList);
			newCheckList.setAllStandard(false);
			newCheckList.setParent(jobGroup);
			jobGroup.addCheckList(newCheckList);
		}
		jobGroupRepository.save(jobGroup);
		return AppUtil.convertToList(checkLists);
	}

	@Override
	@Transactional
	public List<CheckList> createAndLinkFromStandardCheckListToJobGroup(long jobGroupId, CheckListType checkListType,
			long[] standardCheckListItems) throws JobNotFoundException {
		JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long id : standardCheckListItems) {
			ids.add(id);
		}
		Iterable<CheckList> checkLists = checkListRepository.findAll(ids);
		for (CheckList checkList : checkLists) {
			CheckList newCheckList = new CheckList(checkList);
			newCheckList.setCheckListType(checkListType);
			newCheckList.setAllStandard(false);
			newCheckList.setParent(jobGroup);
			jobGroup.addCheckList(newCheckList);
		}
		jobGroupRepository.save(jobGroup);
		return AppUtil.convertToList(checkLists);
	}

	@Transactional
	public PageItr<CheckList> getCheckListsOfJobGroup(long jobGroupId, int pageNumber, int pageSize)
			throws JobNotFoundException {
		JobGroup<Job> jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.JOB, "JobNotFound");
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<CheckList> checkLists = checkListRepository.findAllByParentId(jobGroupId, pageable);
		return AppUtil.convert(checkLists);
	}

	public List<Job> onlyCreateJobFromStandardJob(long[] ids) {
		List<StandardJob> standardJobs = standardJobRepository.findByIdIn(ids);
		List<Job> jobs = new ArrayList<Job>();
		for (StandardJob standardJob : standardJobs) {
			jobs.add(new Job(standardJob, standardJob.getDescription()));
		}
		return jobs;
	}

	@Override
	public PageItr<JobGroup> searchJobGroupWithProperties(JobGroupProperties jobGroupProperties, int pageNumber,
			int pageSize) {
		String name = jobGroupProperties.getName();
		// name = name == null ? "" : name;
		String description = jobGroupProperties.getDescription();
		description = description == null ? "#?#" : description;
		NodeType nodeType = jobGroupProperties.getNodeType();
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		long connectedNodeId = jobGroupProperties.getConnectedNodeId();

		Page<JobGroup> jobGroups;
		if (jobGroupProperties.isStandard()) {
			nodeType = NodeType.standard;
			jobGroups = jobGroupRepository.findByNameLikeOrDescriptionLikeOrNodeType(name, description, nodeType,
					pageable);
		} else {
			nodeType = NodeType.instance;
			if (connectedNodeId == 0) {
				jobGroups = jobGroupRepository.findByNameLikeOrDescriptionLikeOrNodeType(name, description, nodeType,
						pageable);
			} else {
				jobGroups = jobGroupRepository.findByNameLikeOrDescriptionLikeOrNodeTypeAndConnectedNodeId(name,
						description, nodeType, connectedNodeId, pageable);
			}

		}

		return AppUtil.convert(jobGroups);
	}

	@Override
	public PageItr<Job> getSuccessorJobs(long[] jobIds, int pageNumber, int pageSize) throws JobNotFoundException {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobIds) {
			ids.add(id);
		}
		Iterable<Job> jobs = jobRepository.findAll(ids);
		List<Long> jobParentId = new ArrayList<>();
		for (Job job : jobs) {
			jobParentId.add(job.getSuccessorDependency().getStartToStart().getId());
			jobParentId.add(job.getSuccessorDependency().getStartToFinish().getId());
			jobParentId.add(job.getSuccessorDependency().getFinishToStart().getId());
			jobParentId.add(job.getSuccessorDependency().getFinishToFinish().getId());

		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobRepository.findByParentIdIn(jobParentId, pageable));
	}

	@Override
	public PageItr<Job> getPredecessorJobs(long[] jobIds, int pageNumber, int pageSize) throws JobNotFoundException {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobIds) {
			ids.add(id);
		}
		Iterable<Job> jobs = jobRepository.findAll(ids);
		List<Long> jobParentId = new ArrayList<>();
		for (Job job : jobs) {
			jobParentId.add(job.getPredecessorDependency().getStartToStart().getId());
			jobParentId.add(job.getPredecessorDependency().getStartToFinish().getId());
			jobParentId.add(job.getPredecessorDependency().getFinishToStart().getId());
			jobParentId.add(job.getPredecessorDependency().getFinishToFinish().getId());

		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobRepository.findByParentIdIn(jobParentId, pageable));
	}

	@Override
	public PageItr<JobGroup> getSuccessorJobGroups(long[] jobGroupIds, int pageNumber, int pageSize)
			throws JobNotFoundException {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobGroupIds) {
			ids.add(id);
		}
		Iterable<JobGroup> jobGroups = jobGroupRepository.findAll(ids);
		List<Long> jobParentId = new ArrayList<>();
		for (JobGroup job : jobGroups) {
			jobParentId.add(job.getSuccessorDependency().getStartToStart().getId());
			jobParentId.add(job.getSuccessorDependency().getStartToFinish().getId());
			jobParentId.add(job.getSuccessorDependency().getFinishToStart().getId());
			jobParentId.add(job.getSuccessorDependency().getFinishToFinish().getId());

		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobGroupRepository.findByParentIdIn(jobParentId, pageable));
	}

	@Override
	public PageItr<JobGroup> getPredecessorJobGroups(long[] jobGroupIds, int pageNumber, int pageSize)
			throws JobNotFoundException {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobGroupIds) {
			ids.add(id);
		}
		Iterable<JobGroup> jobGroups = jobGroupRepository.findAll(ids);
		List<Long> jobParentId = new ArrayList<>();
		for (JobGroup job : jobGroups) {
			jobParentId.add(job.getPredecessorDependency().getStartToStart().getId());
			jobParentId.add(job.getPredecessorDependency().getStartToFinish().getId());
			jobParentId.add(job.getPredecessorDependency().getFinishToStart().getId());
			jobParentId.add(job.getPredecessorDependency().getFinishToFinish().getId());

		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobGroupRepository.findByParentIdIn(jobParentId, pageable));
	}

	@Override
	public PageItr<HierarchyNode> getConnectedHierarchyNode(long[] jobIds, int pageNumber, int pageSize) {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobIds) {
			ids.add(id);
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNode> page = jobRepository.findParentHierarchyNodes(ids, pageable);

		return AppUtil.convert(page);

	}

	@Override
	public PageItr<Job> getJobsByConnectedNodeIds(long[] parentIds, int pageNumber, int pageSize) {
		List<Long> ids = new ArrayList<Long>();
		for (long id : parentIds) {
			ids.add(id);

		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobRepository.findByconnectedNodeIdIn(ids, pageable));
	}

	@Override
	public PageItr<JobGroup<Job>> getJobGroupssByConnectedNodeIds(long[] parentIds, int pageNumber, int pageSize) {
		List<Long> ids = new ArrayList<Long>();
		for (long id : parentIds) {
			ids.add(id);
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobGroupRepository.findByConnectedNodeIdInAndNodeType(ids, NodeType.instance, pageable));
	}

	@Override
	public PageItr<Job> getJobs(long[] jobIds, int pageNumber, int pageSize) {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobIds) {
			ids.add(id);
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(jobRepository.findByIds(ids, pageable));
	}

	@Override
	public Set<Job> getJobsInJobGroup(long jobGroupId) throws JobNotFoundException {
		JobGroup<Job> jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobGroup Not found");
		}
		return jobGroup.getJobs();
	}

	@Override
	public Set<StandardJob> getStandardJobInJobGroup(long jobGroupId) throws JobNotFoundException {
		JobGroup<StandardJob> jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobGroup Not found");
		}
		return jobGroup.getJobs();
	}

	@Override
	public PageItr<HierarchyNode> getConnectedHierarchyNodeJobGroup(long[] jobGroupIds, int pageNumber, int pageSize) {
		List<Long> ids = new ArrayList<Long>();
		for (long id : jobGroupIds) {
			ids.add(id);
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNode> page = jobGroupRepository.findParentHierarchyNodes(ids, pageable);

		return AppUtil.convert(page);
	}

	// public Job addDependencyJob(long jobId, long[]
	// predecessorDependentJobIds, long[] successorDependentJobIds) throws
	// JobNotFoundException {
	// Job parentJob = jobRepository.findOne(jobId);
	// if (parentJob == null) {
	// throw new JobNotFoundException(jobId, JobNotFoundException.JOB,
	// "JobNotFound", "JobNotFound");
	// }
	// List<Long> ids = new ArrayList<Long>();
	// for (long id : successorDependentJobIds) {
	// ids.add(id);
	// }
	// List<Job> predecessorDependentJobs = jobRepository.findByIdIn(ids);
	// parentJob.getpaddDependentJob(predecessorDependentJobs);
	// jobRepository.save(parentJob);
	// return parentJob;
	// }

}
