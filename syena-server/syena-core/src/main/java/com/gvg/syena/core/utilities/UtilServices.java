package com.gvg.syena.core.utilities;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.ActionTime;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.exception.NotScheduledException;

public class UtilServices {

	public static Date getMinFromTime(HierarchyNode hierarchyNode) throws NotScheduledException {
		WorkTime workTime = getWorkTime(hierarchyNode);
		ActionTime startTime = workTime.getStartTime();
		if (startTime == null) {
			throw new NotScheduledException(hierarchyNode, "Not Scheduled");
		}
		Date time = startTime.getEstimatedTime();
		Date revisedTime = startTime.getRevisedTime();
		Date actualTime = startTime.getRevisedTime();
		if (DateUtil.before(revisedTime, time)) {
			time = revisedTime;
		}
		if (DateUtil.before(actualTime, time)) {
			time = actualTime;
		}
		return time;

	}

	public static Date getMaxToTime(HierarchyNode hierarchyNode) throws NotScheduledException {
		WorkTime workTime = getWorkTime(hierarchyNode);

		ActionTime finishTime = workTime.getFinishTime();
		if (finishTime == null) {
			throw new NotScheduledException(hierarchyNode, "Not Scheduled");
		}

		Date time = finishTime.getEstimatedTime();
		Date toTime = finishTime.getActualTime();
		toTime = finishTime.getRevisedTime();
		if (DateUtil.after(toTime, time)) {
			time = toTime;
		}
	
		if (DateUtil.after(toTime, time)) {
			time = toTime;
		}
		return time;
	}

	private static WorkTime getWorkTime(HierarchyNode hierarchyNode) throws NotScheduledException {
		WorkTime workTime = hierarchyNode.getWorkTime();
		if (workTime == null) {
			throw new NotScheduledException(hierarchyNode, "Not Scheduled");
		}
		return workTime;
	}

	public static void getChildNodeIds(HierarchyNode hierarchyNode, List<Long> hierarchyNodeIds) {
		if (hierarchyNode != null) {
			hierarchyNodeIds.add(hierarchyNode.getId());
			for (HierarchyNode child : hierarchyNode.getChildren()) {
				getChildNodeIds(child, hierarchyNodeIds);
			}
		}
	}

	/**
	 * Is the first date is after the second date
	 * 
	 * @param firstDate
	 * @param secondDate
	 * @return
	 */

	public static boolean dateIsAfter(Date firstDate, Date secondDate) {
		DateTime firstDateTime = new DateTime(firstDate);
		DateTime secondDatetime = new DateTime(secondDate);
		return firstDateTime.isAfter(secondDatetime);
	}

	/**
	 * Is the First date is before second date
	 * 
	 * @param firstDate
	 * @param secondDate
	 * @return
	 */
	public static boolean dateIsbefore(Date firstDate, Date secondDate) {
		DateTime firstDateTime = new DateTime(firstDate);
		DateTime secondDatetime = new DateTime(secondDate);
		return firstDateTime.isBefore(secondDatetime);
	}

	/**
	 * Difference in days between the first date and second date
	 * 
	 * @param firstDate
	 * @param secondDate
	 * @return
	 */
	public static int dateDifferenceInDays(Date firstDate, Date secondDate) {
		DateTime firstDateTime = new DateTime(firstDate);
		DateTime secondDatetime = new DateTime(secondDate);
		Days days = Days.daysBetween(firstDateTime, secondDatetime);
		return days.getDays();
	}

	public static Date nextDate(Date date, int dateDeference) {
		DateTime dateTime = new DateTime(date);
		return dateTime.plusDays(dateDeference).toDate();
	}

	public static String getHashPassword(String password) {
		String encoded = null;
		try {
			encoded = EncryptionUtil.EncryptText(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return encoded;
	}

	public static boolean verifyHashPassword(String password, String digestPAssword) {
		boolean authenticated = false;
		try {
			String encoded = EncryptionUtil.DecryptText(digestPAssword);
			if (encoded.equalsIgnoreCase(password)) {
				authenticated = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return authenticated;
	}

}
