package com.gvg.syena.core.services.schedulerjob.jobs;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.entity.scheduler.ScheduleVarianceLog;
import com.gvg.syena.core.api.services.NotificationService;
import com.gvg.syena.core.api.services.notification.NotificationRecord;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;
import com.gvg.syena.core.api.services.notification.PlaceHolderTableData;
import com.gvg.syena.core.api.services.notification.PlaceHolderType;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.notification.NotificationConfigRepository;
import com.gvg.syena.core.datarepository.scheduler.ScheduleVarianceLogRepository;
import com.gvg.syena.core.services.notification.NotificationServiceImpl;

@Service("GreaterScheduleVarianceJob")
public class GreaterScheduleVarianceJob extends QuartzJobBean implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		GreaterScheduleVarianceJob.applicationContext = applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		ScheduleVarianceLogRepository scheduleVarianceLogRepository = applicationContext.getBean(ScheduleVarianceLogRepository.class);
		HierarchyNodeRepository nodeRepository = applicationContext.getBean(HierarchyNodeRepository.class);
		
		NotificationConfigRepository notificationConfigRepository = applicationContext.getBean(NotificationConfigRepository.class);	
		NotificationConfig notificationConfig = notificationConfigRepository.findOne("SCHEDULE_VARIANCE_LESS_10");
		List<ScheduleVarianceLog> logs = scheduleVarianceLogRepository.findByCurrentVarianceGreaterThan(0.1);
		if(null != logs){
			List<NotificationRecord> records = new ArrayList<>();
			for(ScheduleVarianceLog log : logs){
				if(log.getNotifiedVariance() == 0 || log.getNotifiedVariance() > 0.1){
					double diff = (log.getCurrentVariance() * 100 ) - (log.getNotifiedVariance() * 100);
					if(diff >= 5){
						long id = log.getHierarchyId();
						HierarchyNode node = nodeRepository.findOne(id);
						
						NotificationRecord record = new NotificationRecord();
						record.setRecordId(notificationConfig.getConfigId());
						record.setSubject(notificationConfig.getName());	
						
						List<PlaceHolderData> placeHolders = new ArrayList<>();
						PlaceHolderData tablePlaceHolder = new PlaceHolderData("DATA", PlaceHolderType.TABLE);
						PlaceHolderTableData tableData = new PlaceHolderTableData(3);
						String[] headers = new String[3];
						headers[0] = "Name";
						headers[1] = "Description";
						headers[2] = "Variance";			
						tableData.setHeader(headers);
						
						String[] row = new String[3];
						row[0] = node.getName();
						row[1] = node.getDescription();
						row[2] = log.getCurrentVariance() * 100 + "% ";
						tableData.addRow(row);
						tablePlaceHolder.setValue(tableData);
						placeHolders.add(tablePlaceHolder);
						record.setPlaceHolderDatas(placeHolders);
						records.add(record);	
						
						log.setNotifiedVariance(log.getCurrentVariance());
					}
			
				}
				
			}
			NotificationService notificationService  = applicationContext.getBean(NotificationServiceImpl.class);
			notificationService.notify(notificationConfig,records);
			scheduleVarianceLogRepository.save(logs);
		}
	}

}
