package com.gvg.syena.core.services.schedule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.entity.util.DependencyBarrier;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobExecutionException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.JobsBarrierException;
import com.gvg.syena.core.api.exception.RollBackException;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.ScheduleManagment;
import com.gvg.syena.core.api.services.StatusUpdationService;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.datarepository.util.WorkScheduleWrapperRepository;
import com.gvg.syena.core.datarepository.util.WorkTimeWrapperRepository;
import com.gvg.syena.core.wrapperentity.ApprovalStatus;
import com.gvg.syena.core.wrapperentity.WorkScheduleWrapper;
import com.gvg.syena.core.wrapperentity.WorkTimeWrapper;

@Service("scheduleManagment")
public class ScheduleManagmentImpl implements ScheduleManagment {

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private StatusUpdationService statusUpdationService;

	@Autowired
	private WorkScheduleWrapperRepository workScheduleWrapperRepository;

	@Autowired
	private WorkTimeWrapperRepository workTimeWrapperRepository;

	@Autowired
	private HierarchyScheduler hierarchyScheduler;

	// @Override
	// @Transactional
	// public WorkStatus startValidationJob(long jobId) throws
	// JobNotFoundException, JobExecutionException, JobsBarrierException {
	// Job job = jobRepository.findOne(jobId);
	// if (job == null) {
	// throw new JobNotFoundException(jobId, JobNotFoundException.JOB,
	// "JobNotFound");
	// }
	//
	// WorkStatus workStatus = job.getWorkStatus();
	// if (workStatus.isStarted()) {
	// throw new JobExecutionException(job.getId(),
	// MessageKeys.JOB_ALERADY_STARTED, "Job already started");
	// }
	//
	// DependencyJobs predecessorDependency = job.getPredecessorDependency();
	// DependencyBarrier barriers =
	// predecessorDependency.analyseBarriersToStart();
	// if (barriers.isEmpty()) {
	// workStatus.setRunningStatus(RunningStatus.onprogress);
	// } else {
	// throw new JobsBarrierException(job.getId(), barriers,
	// "Barriers to start");
	// }
	//
	// return workStatus;
	// }

	@Override
	@Transactional
	public WorkStatus finishValidationJob(long jobId) throws JobNotFoundException, JobExecutionException, JobsBarrierException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}
		WorkStatus workStatus = job.getWorkStatus();
		if (!workStatus.isStarted()) {
			throw new JobExecutionException(job.getId(), MessageKeys.JOB_ALERADY_STARTED, "Start the job before finish");
		}
		DependencyJobs predecessorDependency = job.getPredecessorDependency();
		DependencyBarrier barriers = predecessorDependency.analyseBarriersToComplete();

		if (barriers.isEmpty()) {
			workStatus.setRunningStatus(RunningStatus.completed);
		} else {
			throw new JobsBarrierException(job.getId(), barriers, "Barriers to start");
		}

		return workStatus;
	}

	@Override
	@Transactional(rollbackOn = { JobScheduleFailed.class, SchedulingException.class, InvalidDataException.class, JobsBarrierException.class })
	public Set<ScheduleLog> setJobSchedule(long jobId, WorkSchedule timeSchedule, int personDays) throws JobNotFoundException, JobScheduleFailed, SchedulingException,
			InvalidDataException, JobsBarrierException {

		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}

		job.validateWorkSchedule(timeSchedule);

		timeSchedule.validate(job.getDuration().getCalender());

		job.getTotalEffert().setNumberOfDays(personDays);

		job.getDuration().setNumberOfDays(DateUtil.getNumberOfDays(timeSchedule));

		Date startTime = timeSchedule.getPlannedStartTime();

		Date finishTime = timeSchedule.getPlannedFinishTime();

		Set<ScheduleLog> changes = new TreeSet<>();

		hierarchyScheduler.schedule(job, startTime, finishTime, changes);

		update(job.getWorkSchedule(), timeSchedule);

		job.getWorkTime().getStartTime().setActualTime(timeSchedule.getActualStartTime());

		job.getWorkTime().getFinishTime().setActualTime(timeSchedule.getActualFinishTime());

		statusUpdationService.computeUpwardStatus(job);

		jobRepository.save(job);

		return changes;

		// if (!startTime.equals(job.getStartDate())) {
		// JobScheduler.changeStartDate(job, startTime);
		// }
		//
		// if (!finishTime.equals(job.getFinishTime())) {
		// JobScheduler.changeStartDate(job, startTime);
		// }

	}

	private void update(WorkSchedule exWorkSchedule, WorkSchedule newWorkSchedule) {

		exWorkSchedule.getScheduledStages().clear();

		exWorkSchedule.setScheduledStages(newWorkSchedule.getScheduledStages());

		// TODO remove schedule stage form table

		// boolean find = false;
		// List<ScheduledStage> removeList = new ArrayList<ScheduledStage>();
		// for (ScheduledStage scheduledStage1 : scheduledstages) {
		// for (ScheduledStage scheduledStage2 :
		// timeSchedule.getScheduledStages()) {
		// if (scheduledStage1.getId() == scheduledStage2.getId()) {
		// scheduledStage1.merge(scheduledStage2);
		// find = true;
		// break;
		// }
		// }
		// if (!find) {
		// removeList.add(scheduledStage1);
		// find = false;
		// }
		// }
		//
		// scheduledstages.removeAll(removeList);
		//
		// for (ScheduledStage scheduledStage2 :
		// timeSchedule.getScheduledStages()) {
		// if (scheduledStage2.getId() == 0) {
		// scheduledstages.add(scheduledStage2);
		// } else {
		// // TODO
		// scheduledstages.add(scheduledStage2);
		// }
		// }

	}

	// private void setWorkSchedulewithWorkTime(WorkTime workTime, WorkSchedule
	// timeSchedule) {
	// ScheduledStage first = timeSchedule.getScheduledstages().first();
	// if (first.getEstimatedTime() == null) {
	// timeSchedule.changeEstimatedStartDate(workTime.getEstimatedStartTime());
	// }
	// ScheduledStage last = timeSchedule.getScheduledstages().last();
	// if (last.getEstimatedTime() == null) {
	// timeSchedule.changeEstimatedFinishDate(workTime.getEstmatedFinishTime());
	// }
	//
	// }

	@Override
	@Transactional(rollbackOn = { JobScheduleFailed.class, SchedulingException.class })
	public Set<ScheduleLog> setHierarchybSchedule(long hierarchyId, WorkTime workTime) throws JobScheduleFailed, HierarchyNodeNotFoundException, SchedulingException,
			InvalidDataException {
		workTime.validate();
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyId, "Hierarchy Node Not Found");
		}
		workTime.enrich();
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		hierarchyScheduler.schedule(hierarchyNode, workTime.getPlannedStartTime(), workTime.getPlannedFinishTime(), changes);
		statusUpdationService.computeUpwardStatus(hierarchyNode);
		hierarchyNodeRepository.save(hierarchyNode);
		return changes;

	}

	// public void setSchedule(long jobId, TimeSchedule timeSchedule) throws
	// TimeScheduleException {
	// Job job = jobRepository.findOne(jobId);
	// DependencyJobs dependencyJobs = job.getPredecessorDependency();
	// dependencyJobs.validateTimeScheduleEndToEnd(timeSchedule);
	// dependencyJobs.validateTimeScheduleStartToEnd(timeSchedule);
	// dependencyJobs.validateTimeScheduleStartToStart(timeSchedule);
	// dependencyJobs.validateTimeScheduleEndToStart(timeSchedule);
	// job.setTimeSchedule(timeSchedule);
	// }

	@Override
	@Transactional(rollbackOn = { JobScheduleFailed.class, SchedulingException.class })
	public void setJobGroupSchedule(long jobGrouId, WorkTime workTime) throws JobScheduleFailed, SchedulingException, InvalidDataException {
		JobGroup jobGroup = jobGroupRepository.findOne(jobGrouId);
		workTime.enrich();
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		hierarchyScheduler.schedule(jobGroup, workTime.getPlannedStartTime(), workTime.getPlannedFinishTime(), changes);
		statusUpdationService.computeUpwardStatus(jobGroup);
		jobGroupRepository.save(jobGroup);
	}

	@Override
	@Transactional
	public void setUnApprovedHierarchybSchedule(long hierarchyId, WorkTime workTime) throws HierarchyNodeNotFoundException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyId, "HierarchyNodeNotFound");
		}
		WorkTimeWrapper workTimeWrapper = workTimeWrapperRepository.findByHierarchyNodeId(hierarchyId);
		if (workTimeWrapper == null) {
			workTimeWrapper = new WorkTimeWrapper(hierarchyNode, workTime);
		} else {
			workTimeWrapper.setWorkTime(workTime);
		}
		workTimeWrapperRepository.save(workTimeWrapper);

	}

	@Override
	@Transactional
	public void setUnApprovedJobGroupSchedule(long jobGrouId, WorkTime workTime) throws JobNotFoundException {
		JobGroup jobGroup = jobGroupRepository.findOne(jobGrouId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGrouId, JobNotFoundException.GROUP, "JobNotFound");
		}
		WorkTimeWrapper workTimeWrapper = workTimeWrapperRepository.findByJobGroupId(jobGrouId);
		if (workTimeWrapper == null) {
			workTimeWrapper = new WorkTimeWrapper();
		} else {
			workTimeWrapper.setWorkTime(workTime);
		}
		workTimeWrapper.setJobGroup(jobGroup);
		workTimeWrapper.setWorkTime(workTime);
		workTimeWrapperRepository.save(workTimeWrapper);
	}

	@Override
	@Transactional
	public void setUnApprovedJobSchedule(long jobId, WorkSchedule workSchedule, int numberOfDays) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}

		WorkScheduleWrapper workScheduleWrapper = workScheduleWrapperRepository.findByJobIdAndApprovalStatus(jobId, ApprovalStatus.forApproval);
		if (workScheduleWrapper == null) {
			workScheduleWrapper = new WorkScheduleWrapper();
		}
		workScheduleWrapper.setJob(job);
		workScheduleWrapper.setPersonDays(new PersonDays(numberOfDays));
		for (ScheduledStage scheduledStage2 : workSchedule.getScheduledStages()) {
			if (scheduledStage2.getId() != 0) {
				scheduledStage2.setId(0);
			}
		}
		workScheduleWrapper.setWorkSchedule(workSchedule);
		workScheduleWrapperRepository.save(workScheduleWrapper);

	}

	@Override
	@Transactional
	public WorkSchedule getPendingForApprovelJobSchedules(long jobId) throws JobNotFoundException {
		Job job = jobRepository.findOne(jobId);
		if (job == null) {
			throw new JobNotFoundException(jobId, JobNotFoundException.JOB, "JobNotFound");
		}

		WorkScheduleWrapper scheduleWrapper = workScheduleWrapperRepository.findByJobIdAndApprovalStatus(jobId, ApprovalStatus.forApproval);
		WorkSchedule workSchedule = scheduleWrapper.getWorkSchedule();
		// job.setWorkSchedule(workSchedule);
		return workSchedule;

	}

	@Override
	@Transactional
	public PageItr<HierarchyNode> getPendingForApprovelSchedules(String level, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<WorkTimeWrapper> page = workTimeWrapperRepository.findByHierarchyNodeHierarchyLevelLevelNameAndApprovelStatus(level, ApprovalStatus.forApproval, pageable);
		List<HierarchyNode> content = new ArrayList<HierarchyNode>();
		for (WorkTimeWrapper scheduleWrapper : page) {
			HierarchyNode hierarchyNode = scheduleWrapper.getHierarchyNode();
			hierarchyNode.setWorkTime(scheduleWrapper.getWorkTime());
			content.add(hierarchyNode);
		}
		PageItr<HierarchyNode> itr = new PageItr<>(content, page.getNumber(), page.getTotalPages());
		return itr;

	}

	@Override
	@Transactional
	public PageItr<JobGroup> getPendingForApprovelJobGroupSchedules(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<WorkTimeWrapper> page = workTimeWrapperRepository.findByJobGroupAndApprovelStatus(ApprovalStatus.forApproval, pageable);
		List<JobGroup> content = new ArrayList<JobGroup>();
		for (WorkTimeWrapper scheduleWrapper : page) {
			JobGroup<Job> jobGroup = scheduleWrapper.getJobGroup();
			jobGroup.setWorkTime(scheduleWrapper.getWorkTime());
			content.add(jobGroup);
		}
		PageItr<JobGroup> itr = new PageItr<>(content, page.getNumber(), page.getTotalPages());
		return itr;

	}

	@Override
	@Transactional
	public PageItr<Job> getPendingForApprovelJobSchedules(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);

		Page<WorkScheduleWrapper> page = workScheduleWrapperRepository.findByApprovalStatus(ApprovalStatus.forApproval, pageable);
		ObjectMapper mapper = new ObjectMapper();
		List<Job> content = new ArrayList<Job>();
		for (WorkScheduleWrapper scheduleWrapper : page) {
			try {
				Job job = scheduleWrapper.getJob();
				String jobString = mapper.writeValueAsString(job);
				job = mapper.readValue(jobString, Job.class);

				WorkSchedule workScheduleW = scheduleWrapper.getWorkSchedule();

				String workScheduleString = mapper.writeValueAsString(workScheduleW);
				WorkSchedule workScheduleN = mapper.readValue(workScheduleString, WorkSchedule.class);
				job.setDuration(scheduleWrapper.getPersonDays());
				job.setWorkSchedule(workScheduleN);
				content.add(job);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		PageItr<Job> itr = new PageItr<Job>(content, page.getNumber(), page.getTotalPages());
		return itr;

	}

	@Override
	@Transactional
	public void removeUnApprovedJobSchedule(long jobId) {
		List<WorkScheduleWrapper> scheduleWrappers = workScheduleWrapperRepository.findByJobId(jobId);
		for (WorkScheduleWrapper scheduleWrapper : scheduleWrappers) {
			workScheduleWrapperRepository.delete(scheduleWrapper);
		}
	}

	@Override
	@Transactional
	public void removeUnApprovedHierarchybSchedule(long hierarchyId) {
		WorkTimeWrapper workTimeWrapper = workTimeWrapperRepository.findByHierarchyNodeId(hierarchyId);
		if (workTimeWrapper != null) {
			workTimeWrapperRepository.delete(workTimeWrapper);
		}
	}

	@Override
	@Transactional
	public void removeUnApprovedJobGroupSchedule(long jobGrouId) {
		WorkScheduleWrapper workScheduleWrapper = workScheduleWrapperRepository.findByJobGroupId(jobGrouId);
		if (workScheduleWrapper != null) {
			workScheduleWrapperRepository.delete(workScheduleWrapper);
		}

	}

	@Override
	@Transactional
	public void rejectUnApprovedJobSchedule(long jobId) {
		WorkScheduleWrapper workScheduleWrapper = workScheduleWrapperRepository.findByJobIdAndApprovalStatus(jobId, ApprovalStatus.forApproval);
		if (workScheduleWrapper != null) {
			workScheduleWrapper.setApprovalStatus(ApprovalStatus.rejected);
			workScheduleWrapperRepository.save(workScheduleWrapper);
		}

	}

	@Override
	@Transactional
	public void rejectUnApprovedHierarchybSchedule(long hierarchyId) {
		WorkTimeWrapper workTimeWrapper = workTimeWrapperRepository.findByHierarchyNodeId(hierarchyId);
		if (workTimeWrapper != null) {
			workTimeWrapper.setApprovelStatus(ApprovalStatus.rejected);
			workTimeWrapperRepository.save(workTimeWrapper);
		}

	}

	@Override
	@Transactional
	public void rejectUnApprovedJobGroupSchedule(long jobGrouId) {
		WorkTimeWrapper workTimeWrapper = workTimeWrapperRepository.findByJobGroupId(jobGrouId);
		if (workTimeWrapper != null) {
			workTimeWrapper.setApprovelStatus(ApprovalStatus.rejected);
			workTimeWrapperRepository.save(workTimeWrapper);
		}

	}

	@Override
	@Transactional(rollbackOn = { SchedulingException.class, JobNotFoundException.class, JobScheduleFailed.class, JobScheduleFailed.class, InvalidDataException.class,
			JobsBarrierException.class })
	public Set<ScheduleLog> setJobGroupSchedule(long jobGroupId, WorkSchedule workSchedule, int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed,
			JobScheduleFailed, InvalidDataException, JobsBarrierException {

		JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGroupId, JobNotFoundException.GROUP, "JobGroup not found");
		}
		Set<ScheduleLog> chnagesjobs = new TreeSet<ScheduleLog>();
		hierarchyScheduler.schedule(jobGroup, workSchedule.getPlannedStartTime(), workSchedule.getPlannedFinishTime(), chnagesjobs);
		Set<Job> jobs = jobGroup.getJobs();
		for (Job job : jobs) {
//			SortedSet<ScheduledStage> jobGroupScheduledStages = jobGroup.getWorkSchedule().getScheduledStages();
			SortedSet<ScheduledStage> jobScheduledStages = job.getWorkSchedule().getScheduledStages();
			SortedSet<ScheduledStage> jobGroupScheduledStages =workSchedule.getScheduledStages();
			for (ScheduledStage jobGroupScheduledStage : jobGroupScheduledStages ) {
				for (ScheduledStage jobScheduledStage : jobScheduledStages) {
					if (jobScheduledStage.compareTo(jobGroupScheduledStage) == 0) {
						jobScheduledStage.merge(jobGroupScheduledStage);
						break;
					}
				}
			}
			jobRepository.save(job);
			statusUpdationService.computeUpwardStatus(job);
		}
		// for (Job job : chnagesjobs) {
		// JobGroup<Job> finishForStart =
		// job.getPredecessorDependency().getFinishForStart();
		// if (finishForStart.getJobs().size() > 0) {
		// throw new
		// SchedulingException(MessageKeys.JOBGROUPSCHEDULE_CONTAIN_DEPENDENCY,
		// "JobGroup contain dependency, unable to Schedule");
		// }
		// Set<Job> chagnes = setJobSchedule(job.getId(), workSchedule.copy(),
		// job.getTotalEffert().getNumberofDays());
		// chnagesjobs.addAll(chagnes);
		// }
		workSchedule = workSchedule.copy();
		jobGroup.setDuration(new PersonDays(personDays));
		jobGroup.getWorkTime().setStartTime(workSchedule.getScheduledStages().first().getActionTime());
		jobGroup.getWorkTime().setFinishTime(workSchedule.getScheduledStages().last().getActionTime());
		jobGroup.setWorkSchedule(workSchedule);
		return chnagesjobs;
	}

	@Override
	@Transactional(rollbackOn = { SchedulingException.class, JobNotFoundException.class, JobScheduleFailed.class, JobScheduleFailed.class, InvalidDataException.class,
			JobsBarrierException.class, RollBackException.class })
	public void validateJobGroupWorkSchedule(long jobGrouId, WorkSchedule workSchedule, int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed,
			InvalidDataException, JobsBarrierException, RollBackException {
		Set<ScheduleLog> jobs = setJobGroupSchedule(jobGrouId, workSchedule, personDays);
		throw new RollBackException(jobs);
	}

	@Override
	@Transactional(rollbackOn = { SchedulingException.class, JobNotFoundException.class, JobScheduleFailed.class, JobScheduleFailed.class, InvalidDataException.class,
			JobsBarrierException.class, RollBackException.class })
	public void validateJobSchedule(long jobId, WorkSchedule workSchedule, int personDays) throws JobNotFoundException, JobScheduleFailed, SchedulingException,
			InvalidDataException, JobsBarrierException, RollBackException {
		Set<ScheduleLog> jobs = setJobSchedule(jobId, workSchedule, personDays);
		throw new RollBackException(jobs);
	}

	@Override
	public void setUnApprovedJobGroupSchedule(long jobGrouId, WorkSchedule workSchedule, int personDays) throws JobNotFoundException {
		JobGroup<Job> jobGroup = jobGroupRepository.findOne(jobGrouId);
		if (jobGroup == null) {
			throw new JobNotFoundException(jobGrouId, JobNotFoundException.GROUP, "JobGroup NotFound");
		}

		WorkScheduleWrapper workScheduleWrapper = workScheduleWrapperRepository.findByJobIdAndApprovalStatus(jobGrouId, ApprovalStatus.forApproval);
		if (workScheduleWrapper == null) {
			workScheduleWrapper = new WorkScheduleWrapper();
		}
		workScheduleWrapper.setJob(jobGroup);
		workScheduleWrapper.setPersonDays(new PersonDays(personDays));
		for (ScheduledStage scheduledStage2 : workSchedule.getScheduledStages()) {
			if (scheduledStage2.getId() != 0) {
				scheduledStage2.setId(0);
			}
		}
		workScheduleWrapper.setWorkSchedule(workSchedule);
		workScheduleWrapperRepository.save(workScheduleWrapper);

	}

	@Override
	public void rejectUnApprovedJobGroupWorkSchedule(long jobGrouId) {
		WorkScheduleWrapper workScheduleWrapper = workScheduleWrapperRepository.findByJobGroupIdAndApprovalStatus(jobGrouId, ApprovalStatus.forApproval);
		if (workScheduleWrapper != null) {
			workScheduleWrapper.setApprovalStatus(ApprovalStatus.rejected);
			workScheduleWrapperRepository.save(workScheduleWrapper);
		}
	}

	@Override
	public PageItr<JobGroup<Job>> getPendingForApprovelJobGroupWorkSchedules(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);

		Page<WorkScheduleWrapper> page = workScheduleWrapperRepository.findByJobGroup(ApprovalStatus.forApproval, pageable);
		ObjectMapper mapper = new ObjectMapper();
		List<JobGroup<Job>> content = new ArrayList<JobGroup<Job>>();
		for (WorkScheduleWrapper scheduleWrapper : page) {
			try {
				JobGroup<Job> jobGroup = scheduleWrapper.getJobGroup();
				String jobString = mapper.writeValueAsString(jobGroup);
				jobGroup = mapper.readValue(jobString, JobGroup.class);

				WorkSchedule workScheduleW = scheduleWrapper.getWorkSchedule();

				String workScheduleString = mapper.writeValueAsString(workScheduleW);
				WorkSchedule workScheduleN = mapper.readValue(workScheduleString, WorkSchedule.class);
				jobGroup.setDuration(scheduleWrapper.getPersonDays());
				jobGroup.setWorkSchedule(workScheduleN);
				content.add(jobGroup);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		PageItr<JobGroup<Job>> itr = new PageItr<JobGroup<Job>>(content, page.getNumber(), page.getTotalPages());
		return itr;
	}

	@Override
	@Transactional(rollbackOn = { SchedulingException.class, JobNotFoundException.class, JobScheduleFailed.class, JobScheduleFailed.class, InvalidDataException.class,
			JobsBarrierException.class, RollBackException.class })
	public Set<ScheduleLog> validateHierarchybSchedule(long hierarchyId, WorkTime workTime) throws JobScheduleFailed, HierarchyNodeNotFoundException, SchedulingException,
			InvalidDataException, RollBackException {
		Set<ScheduleLog> jobs = setHierarchybSchedule(hierarchyId, workTime);
		throw new RollBackException(jobs);
	}

}
