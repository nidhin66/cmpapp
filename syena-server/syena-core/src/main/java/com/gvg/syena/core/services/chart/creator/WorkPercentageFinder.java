package com.gvg.syena.core.services.chart.creator;

import java.util.Date;
import java.util.Set;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkPercentageType;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.PersonDays;

public class WorkPercentageFinder {

	private Duration totalDuration;

	private Duration completedDuration;

	public WorkPercentageFinder() {
		this.totalDuration = new PersonDays(0);
		this.completedDuration = new PersonDays(0);
	}

	public double findWorkPercentage(HierarchyNode hierarchyNode, Date ondate, WorkPercentageType workPercentageType) {
		calculateCompletedDurationAndTotalDuration(hierarchyNode, workPercentageType, ondate);
		return completedDuration.divide(totalDuration);
	}

	private void calculateCompletedDurationAndTotalDuration(HierarchyNode hierarchyNode, WorkPercentageType workPercentageType, Date date) {
		if (hierarchyNode != null) {
			calculateCompletedDurationAndTotalDurationJob(hierarchyNode.getJobs(), workPercentageType, date);
			calculateCompletedDurationAndTotalDurationJobGroup(hierarchyNode.getJobGroups(), workPercentageType, date);
			Set<HierarchyNode> children = hierarchyNode.getChildren();
			if (children != null && children.size() > 0) {
				for (HierarchyNode node : children) {
					calculateCompletedDurationAndTotalDuration(node, workPercentageType, date);
				}
			}
		}

	}

	private void calculateCompletedDurationAndTotalDurationJobGroup(Set<JobGroup<Job>> jobGroups, WorkPercentageType workPercentageType, Date date) {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				calculateCompletedDurationAndTotalDurationJob(jobGroup.getJobs(), workPercentageType, date);
				calculateCompletedDurationAndTotalDurationJobGroup(jobGroup.getJobGroups(), workPercentageType, date);
			}
		}
	}

	private void calculateCompletedDurationAndTotalDurationJob(Set<Job> jobs, WorkPercentageType workPercentageType, Date date) {
		if (jobs != null) {
			for (Job job : jobs) {

				Duration duration = job.getDuration().copy();

				Float percentageComplete = workPercentageType.getPercentageComplete(job, date);

				duration.multiple(percentageComplete);

				duration.multiple(job.getWeightage());

				// double b = workPercentageType.getPercentageComplete(job,
				// date);
				// Duration a =
				// job.getEstimatedTimeSpent().multiple(b).multiple(job.getWeightage());
				completedDuration.add(duration);

				totalDuration.add(job.getDuration());
			}
		}
	}

}
