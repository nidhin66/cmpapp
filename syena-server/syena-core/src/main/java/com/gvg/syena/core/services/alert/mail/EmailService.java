package com.gvg.syena.core.services.alert.mail;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.gvg.syena.core.api.entity.alert.mail.MailServerConfig;
import com.gvg.syena.core.api.services.alert.mail.Email;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;

 
public class EmailService {
	
	
	

	private static JavaMailSenderImpl mailSender = null;
	
	
	private static volatile EmailService INSTANCE = null;
	    

	private String fromAdress;
	
	private EmailService() {
		
	}
	
	public static EmailService getInstance(){
	    if(INSTANCE == null){
	        synchronized(EmailService.class){
	            if(INSTANCE == null){
	                INSTANCE = new EmailService();
	            }
	        }
	    }
	    return INSTANCE;
	}
	
	
	
	
	public static void sendEmail(Email email){
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		// use the true flag to indicate you need a multipart message
		boolean hasAttachments = (email.getAttachments() != null && email.getAttachments().size() > 0);
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mimeMessage, hasAttachments);
			helper.setTo(email.getTo());		
			helper.setFrom(email.getFrom());
			helper.setSubject(email.getSubject());
			helper.setText(email.getText(), true);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

//		List<Attachment> attachments = email.getAttachments();
//		if (attachments != null && attachments.size() > 0) {
//			for (Attachment attachment : attachments) {
//				String filename = attachment.getFilename();
//				DataSource dataSource = new ByteArrayDataSource(attachment.getData(), attachment.getMimeType());
//				if (attachment.isInline()) {
//					helper.addInline(filename, dataSource);
//				} else {
//					helper.addAttachment(filename, dataSource);
//				}
//			}
//		}

		mailSender.send(mimeMessage);
	}

	public static void sendEmail(long alertTemplateID,List<PlaceHolderData> placeHolders)  {
		
		AlertComposer composer = new AlertComposer(alertTemplateID, placeHolders);
		
		Email email = composer.getEmail();
		
		sendEmail(email);
	}
	
	public void initialize(MailServerConfig mailServerConfig){
		mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailServerConfig.getHost());
		mailSender.setPort(mailServerConfig.getPort());
		mailSender.setDefaultEncoding(mailServerConfig.getDefaultEncoding());
		mailSender.setProtocol(mailServerConfig.getProtocol());
		mailSender.setUsername(mailServerConfig.getUsername());
		mailSender.setPassword(mailServerConfig.getPassword());
		mailServerConfig.setJavaMailProperties(mailServerConfig.getJavaMailProperties());
		fromAdress = mailServerConfig.getUsername();
	}

	public String getFromAdress() {
		return fromAdress;
	}

	
}