package com.gvg.syena.core.services.criticalpath;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;

@Service("criticalPathFinder")
public class CriticalPathFinderImpl implements CriticalPathFinder {

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Override
	@Transactional
	public CriticalAndNonCriticalPath findCriticalAndNonCriticalPath(long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Hierarchy Node Not Found");
		}
		List<JobData> criticalPathJobData = new ArrayList<JobData>();
		List<JobData> otherJobs = new ArrayList<JobData>();
		sperateLastDateAndOtherJobsHierarchyNode(hierarchyNode, criticalPathJobData, otherJobs);

		for (JobData jobData : criticalPathJobData) {
			Job job = jobRepository.findOne(jobData.getJobId());
			JobGroup<Job> finishForStartJobs = job.getPredecessorDependency().getFinishForStart();
			addDependecyJobstoCriticalPath(finishForStartJobs, criticalPathJobData);
		}
		otherJobs.removeAll(criticalPathJobData);

		CriticalAndNonCriticalPath criticalAndNonCriticalPath = new CriticalAndNonCriticalPath(criticalPathJobData, otherJobs);
		return criticalAndNonCriticalPath;
	}

	private void sperateLastDateAndOtherJobsHierarchyNode(HierarchyNode hierarchyNode, List<JobData> criticalPathJobData, List<JobData> otherJobs) {
		if (hierarchyNode != null) {
			Set<HierarchyNode> children = hierarchyNode.getChildren();
			if (children != null)
				for (HierarchyNode child : children) {
					sperateLastDateAndOtherJobsHierarchyNode(child, criticalPathJobData, otherJobs);
				}
			findLastDateJobGroups(hierarchyNode.getJobGroups(), criticalPathJobData, otherJobs);
			findLastDateJobs(hierarchyNode.getJobs(), criticalPathJobData, otherJobs);
		}
	}

	private void findLastDateJobGroups(Set<JobGroup<Job>> jobGroups, List<JobData> criticalPathJobData, List<JobData> otherJobs) {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				findLastDateJobGroups(jobGroup.getJobGroups(), criticalPathJobData, otherJobs);
				findLastDateJobs(jobGroup.getJobs(), criticalPathJobData, otherJobs);
			}
		}

	}

	private void findLastDateJobs(Set<Job> jobs, List<JobData> criticalPathJobData, List<JobData> otherJobs) {

		for (Job job : jobs) {
			if(!job.isPlanned()){
				continue;
			}
			JobData jobData = new JobData(job.getId(), job.getWorkTime(), job.getWorkStatus());
			if (criticalPathJobData.size() <= 0) {
				criticalPathJobData.add(jobData);
			} else {
				Date palnnedFinishTime = job.getPalnnedFinishTime();
				Date cpalnnedFinishTime = criticalPathJobData.get(0).getWorkTime().getPlannedFinishTime();
				if (palnnedFinishTime != null) {
					if (palnnedFinishTime.equals(cpalnnedFinishTime)) {
						criticalPathJobData.add(jobData);
					} else if (palnnedFinishTime.after(cpalnnedFinishTime)) {
						otherJobs.addAll(criticalPathJobData);
						criticalPathJobData.clear();
						criticalPathJobData.add(jobData);
					} else {
						otherJobs.add(jobData);
					}
				} else {
					otherJobs.add(jobData);
				}
			}

		}
	}

	private void addDependecyJobstoCriticalPath(JobGroup<Job> finishForStartJobs, List<JobData> criticalPathJobDatas) {
		if (finishForStartJobs != null) {
			Set<Job> finishForStartJobsList = finishForStartJobs.getJobs();
			if (finishForStartJobsList != null) {
				for (Job job : finishForStartJobsList) {
					JobData criticalPathJobData = new JobData(job.getId(), job.getWorkTime(), job.getWorkStatus());
					criticalPathJobDatas.add(criticalPathJobData);
				}
			}
			Set<JobGroup<Job>> jobGroupsList = finishForStartJobs.getJobGroups();
			if (jobGroupsList != null) {
				for (JobGroup<Job> jobGroup : jobGroupsList) {
					addDependecyJobstoCriticalPath(jobGroup.getPredecessorDependency().getFinishForStart(), criticalPathJobDatas);
				}
			}
		}

	}

	// @Override
	// public List<JobData> findNonCriticalPath(long hierarchyNodeId) throws
	// HierarchyNodeNotFoundException, NotScheduledException {
	// HierarchyNode hierarchyNode =
	// hierarchyNodeRepository.findOne(hierarchyNodeId);
	// if (hierarchyNode == null) {
	// throw new HierarchyNodeNotFoundException(hierarchyNodeId,
	// "HierarchyNodeNotFound", "Parent Node NotFound");
	// }
	// List<Long> ids = new ArrayList<Long>();
	// UtilServices.getChildNodeIds(hierarchyNode, ids);
	// int pageSize = 1;
	// int pageNumber = 0;
	//
	// Pageable pageable = new PageRequest(pageNumber, pageSize);
	// Page<Job> page = jobRepository.findByconnectedNodeIdIn(ids, pageable);
	// pageSize = 30;
	// SortedSet<JobData> nonCriticalPathJobDatas = new TreeSet<JobData>(new
	// CriticalPathJobDataComparitor());
	// while (pageNumber++ < page.getTotalPages()) {
	// pageable = new PageRequest(pageNumber, pageSize);
	// page = jobRepository.findByconnectedNodeIdIn(ids, pageable);
	// for (Job job : page) {
	// JobData criticalPathJobData = new JobData(job.getId(), job.getWorkTime(),
	// job.getWorkStatus());
	// nonCriticalPathJobDatas.add(criticalPathJobData);
	// // JobGroup<Job> finishForStartJobs =
	// // job.getPredecessorDependency().getFinishForStart();
	// // addDependecyJobstoCriticalPath(finishForStartJobs,
	// // criticalPathJobDatas);
	// }
	//
	// }
	//
	// Iterator<JobData> iterator = nonCriticalPathJobDatas.iterator();
	// Date t1 = null;
	// List<JobData> criticalPathJobDataList = new ArrayList<JobData>();
	// while (iterator.hasNext()) {
	// JobData data = iterator.next();
	// Date t = data.getWorkTime().getPlannedFinishTime();
	// if (t1 == null) {
	// t1 = t;
	// } else {
	// if (t != null && t1.after(t)) {
	// criticalPathJobDataList.add(data);
	// }
	// }
	// }
	//
	// return criticalPathJobDataList;
	// }

	// @Transactional
	// private List<CriticalPathJobData>
	// findCriticalPathJobAndGroup(HierarchyNode hierarchyNode) throws
	// NotScheduledException {
	//
	// // Date fromTime = UtilServices.getMinFromTime(hierarchyNode);
	// // Date toTime = UtilServices.getMaxToTime(hierarchyNode);
	//
	// // CriticalPathPeriodSet criticalPathPeriodSet = new
	// CriticalPathPeriodSet(fromTime, toTime);
	// // criticalPathPeriodSet.add(findCriticalPathJob(hierarchyNode));
	// return findCriticalPathJob(hierarchyNode);
	// }

	// @Override
	// @Transactional
	// public List<JobData> findCriticalPath(long hierarchyNodeId) throws
	// HierarchyNodeNotFoundException, NotScheduledException {
	// HierarchyNode hierarchyNode =
	// hierarchyNodeRepository.findOne(hierarchyNodeId);
	// if (hierarchyNode == null) {
	// throw new HierarchyNodeNotFoundException(hierarchyNodeId,
	// "HierarchyNodeNotFound", "Parent Node NotFound");
	// }
	// return findCriticalPathJob(hierarchyNode);
	// }
	//
	// @Transactional
	// private List<JobData> findCriticalPathJob(HierarchyNode hierarchyNode) {
	// // List<Long> hierarchyNodeIds = new ArrayList<Long>();
	// // addHierarchyNodeIds(hierarchyNode, hierarchyNodeIds);
	//
	// List<Job> jobs = new ArrayList<Job>();
	// sperateLastDateAndOtherJobsHierarchyNode(hierarchyNode, jobs);
	//
	// List<JobData> criticalPathJobDatas = new ArrayList<JobData>();
	// for (Job job : jobs) {
	// JobData criticalPathJobData = new JobData(job.getId(), job.getWorkTime(),
	// job.getWorkStatus());
	// criticalPathJobDatas.add(criticalPathJobData);
	// JobGroup<Job> finishForStartJobs =
	// job.getPredecessorDependency().getFinishForStart();
	// addDependecyJobstoCriticalPath(finishForStartJobs, criticalPathJobDatas);
	// }
	// return criticalPathJobDatas;
	// }

}
