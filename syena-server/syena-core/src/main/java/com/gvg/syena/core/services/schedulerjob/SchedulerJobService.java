package com.gvg.syena.core.services.schedulerjob;

import java.util.List;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.gvg.syena.core.api.entity.scheduler.SchedulerJob;

public class SchedulerJobService {

	private static volatile SchedulerJobService INSTANCE = null;

	private SchedulerJobService() {

	}

	public static SchedulerJobService getInstance() {
		if (INSTANCE == null) {
			synchronized (SchedulerJobService.class) {
				if (INSTANCE == null) {
					INSTANCE = new SchedulerJobService();
				}
			}
		}
		return INSTANCE;
	}

	public void initialize(List<SchedulerJob> scheduleJobs) {
		SchedulerFactory schFactory = new StdSchedulerFactory();

		for (SchedulerJob schedulerJob : scheduleJobs) {
			CronTrigger cronTrigger = TriggerBuilder.newTrigger()
					.withSchedule(CronScheduleBuilder.cronSchedule(schedulerJob.getCronExpression())).build();

			JobDetail job = null;
			try {
				job = JobBuilder.newJob((Class<? extends Job>) Class.forName(schedulerJob.getJobBeanClass()))
						.withIdentity(schedulerJob.getName()).build();

				Scheduler sch;

				sch = schFactory.getScheduler();
				sch.start();
				sch.scheduleJob(job, cronTrigger);
			} catch (SchedulerException e) {
			
			} catch (ClassNotFoundException e1) {
				
			}
		}

	}

}
