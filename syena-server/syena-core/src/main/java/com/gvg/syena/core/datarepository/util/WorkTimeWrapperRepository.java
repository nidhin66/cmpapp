package com.gvg.syena.core.datarepository.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.wrapperentity.ApprovalStatus;
import com.gvg.syena.core.wrapperentity.WorkTimeWrapper;

public interface WorkTimeWrapperRepository extends PagingAndSortingRepository<WorkTimeWrapper, Long> {

	// Page<WorkTimeWrapper> findByHierarchyNodeHierarchyLevelLevelName(String
	// level, Pageable pageable);

	@Query("select w from WorkTimeWrapper w  where w.jobGroup  != null")
	Page<WorkTimeWrapper> findByJobGroup(Pageable pageable);

	WorkTimeWrapper findByHierarchyNodeId(long hierarchyId);

	WorkTimeWrapper findByJobGroupId(long jobGrouId);

	Page<WorkTimeWrapper> findByHierarchyNodeHierarchyLevelLevelNameAndApprovelStatus(String level, ApprovalStatus approvelStatus, Pageable pageable);

	@Query("select w from WorkTimeWrapper w  where w.jobGroup  != null and approvelStatus = :approvelStatus")
	Page<WorkTimeWrapper> findByJobGroupAndApprovelStatus(@Param("approvelStatus") ApprovalStatus approvelStatus, Pageable pageable);

}
