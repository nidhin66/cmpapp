package com.gvg.syena.core.services.schedule;

import java.util.Date;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.services.StatusUpdationService;
import com.gvg.syena.core.config.AppilicationConfig;
import com.gvg.syena.core.services.schedule.exception.JobCompleted;
import com.gvg.syena.core.services.schedule.exception.JobDateBeforeToday;
import com.gvg.syena.core.services.schedule.exception.JobScheduleTimeLimit;

@Service("jobScheduler")
public class JobScheduler {

	@Autowired
	private StatusUpdationService statusUpdationService;

	@Transactional(rollbackOn = JobScheduleFailed.class)
	public void changeStartDate(Job job, Date date, Set<ScheduleLog> changes) throws JobScheduleFailed {

		changeStartAndCorrespondingEndDate(job, date, changes);

		JobGroup<Job> startToStart = job.getPredecessorDependency().getStartToStart();
		ensureStartDateBefore(date, startToStart, changes);
		JobGroup<Job> finishToStart = job.getPredecessorDependency().getFinishToStart();
		ensureFinishDateBefore(date, finishToStart, changes);

		JobGroup<Job> startToStartSuccessor = job.getSuccessorDependency().getStartForStart();
		ensureStartDateAfter(date, startToStartSuccessor, changes);
		JobGroup<Job> finishToStartSuccessor = job.getSuccessorDependency().getStartForFinish();
		ensureFinishDateAfter(date, finishToStartSuccessor, changes);

	}

	private void changeStartAndCorrespondingEndDate(Job job, Date date, Set<ScheduleLog> changes) throws JobScheduleFailed {
		checkPosibilityOfDate(job, date);
		ScheduleLog scheduleLog = new ScheduleLog(job.getId(), job.getPalnnedStartTime(), job.getPalnnedFinishTime());
		job.changePlannedStartDate(date);
		scheduleLog.setCurrentStartDate(job.getPalnnedStartTime());
		scheduleLog.setCurrentEndDate(job.getPalnnedFinishTime());
		Duration duration = job.getDuration();
		Date plannedStartTime = job.getPalnnedStartTime();
		Date plannedFinishTime = job.getPalnnedFinishTime();
		if (plannedStartTime == null || plannedFinishTime == null || duration.isLess(plannedStartTime, plannedFinishTime)) {
			changeFinishDate(job, duration.calculateEndDate(date), changes);
		}
		statusUpdationService.computeUpwardStatus(job);
	}

	private void ensureStartDateBefore(Date date, JobGroup<Job> startToStart, Set<ScheduleLog> changes) throws JobScheduleFailed {
		Set<Job> jobs = startToStart.getJobs();
		for (Job job : jobs) {
			if (job.getPalnnedStartTime().after(date)) {
				changeStartDate(job, date, changes);
			}
		}

	}

	private void ensureFinishDateBefore(Date date, JobGroup<Job> finishToStart, Set<ScheduleLog> changes) throws JobScheduleFailed {
		Set<Job> jobs = finishToStart.getJobs();
		for (Job job : jobs) {
			if (job.getPalnnedFinishTime() == null || DateUtil.after(job.getPalnnedFinishTime(), date)) {
				changeFinishDate(job, date, changes);
			}
		}

	}

	private void ensureStartDateAfter(Date date, JobGroup<Job> startToStartSuccessor, Set<ScheduleLog> changes) throws JobScheduleFailed {
		Set<Job> jobs = startToStartSuccessor.getJobs();
		for (Job job : jobs) {
			if (job.getPalnnedStartTime() == null || DateUtil.before(job.getPalnnedStartTime(), date)) {
				changeStartDate(job, date, changes);
			}
		}

	}

	private void ensureFinishDateAfter(Date date, JobGroup<Job> finishToStartSuccessor, Set<ScheduleLog> changes) throws JobScheduleFailed {
		Set<Job> jobs = finishToStartSuccessor.getJobs();
		for (Job job : jobs) {
			if (job.getPalnnedFinishTime() == null || DateUtil.before(job.getPalnnedFinishTime(), date)) {
				changeFinishDate(job, date, changes);
			}
		}

	}

	@Transactional(rollbackOn = JobScheduleFailed.class)
	public void changeFinishDate(Job job, Date date, Set<ScheduleLog> changes) throws JobScheduleFailed {

		changeEndAndCorrespondingStartDate(job, date, changes);

		JobGroup<Job> startToFinish = job.getPredecessorDependency().getStartToFinish();
		ensureStartDateBefore(date, startToFinish, changes);
		JobGroup<Job> finishToFinish = job.getPredecessorDependency().getFinishToFinish();
		ensureFinishDateBefore(date, finishToFinish, changes);

		JobGroup<Job> startToStartSuccessor = job.getSuccessorDependency().getFinishForStart();
		ensureStartDateAfter(date, startToStartSuccessor, changes);
		JobGroup<Job> finishToStartSuccessor = job.getSuccessorDependency().getFinishForFinish();
		ensureFinishDateAfter(date, finishToStartSuccessor, changes);
	}

	private void changeEndAndCorrespondingStartDate(Job job, Date date, Set<ScheduleLog> changes) throws JobScheduleFailed {
		checkPosibilityOfDate(job, date);
		ScheduleLog scheduleLog = new ScheduleLog(job.getId(), job.getPalnnedStartTime(), job.getPalnnedFinishTime());
		changes.add(scheduleLog);
		job.changePlannedFinishDate(date);
		scheduleLog.setCurrentStartDate(job.getPalnnedStartTime());
		scheduleLog.setCurrentEndDate(job.getPalnnedFinishTime());
		Duration duration = job.getDuration();
		Date plannedStartTime = job.getPalnnedStartTime();
		Date plannedFinishTime = job.getPalnnedFinishTime();
		if (plannedStartTime == null || plannedFinishTime == null || duration.isLess(plannedStartTime, plannedFinishTime)) {
			// if (duration.isLess(job.getEstimatedStartTime(),
			// job.getEstimatedFinishTime())) {
			changeStartDate(job, duration.calculateStatDate(date), changes);
		}
		statusUpdationService.computeUpwardStatus(job);
	}

	private void checkPosibilityOfDate(Job job, Date date) throws JobScheduleFailed {
		if (AppilicationConfig.jobSheduleOnlyFromToday && job.getDuration().beforeToday(date)) {
			throw new JobDateBeforeToday(job, "", "");
		}
		if (RunningStatus.completed == job.getWorkStatus().getRunningStatus()) {
			throw new JobCompleted(job, date, MessageKeys.JOB_COMPLETED, "JobCompleted");
		}
		HierarchyNode connectedNode = job.getConnectedNode();
		if (connectedNode != null) {
			if(null != connectedNode.getWorkTime()) {
				if (!connectedNode.getWorkTime().isInBetweenStartDateAndEndDate(date)) {
					throw new JobScheduleTimeLimit(connectedNode, job, date, MessageKeys.SCHEDULE_TIME_NOT_IN_BETWEEN, date + " not in between of start date and end date of "
							+ connectedNode.getName());
				}
			}
			
		}

	}

}
