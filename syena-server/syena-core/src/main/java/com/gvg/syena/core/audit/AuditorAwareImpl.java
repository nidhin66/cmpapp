package com.gvg.syena.core.audit;

import org.springframework.data.domain.AuditorAware;

import com.gvg.syena.core.api.entity.organization.Person;

public class AuditorAwareImpl implements AuditorAware<Person> {

	@Override
	public Person getCurrentAuditor() {

		// http://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/#jpa.auditing
		// TODO Auto-generated method stub
		return new Person("TEST");
	}

}
