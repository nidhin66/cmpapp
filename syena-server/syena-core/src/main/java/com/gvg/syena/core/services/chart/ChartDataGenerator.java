package com.gvg.syena.core.services.chart;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import javax.transaction.Transactional;

import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.api.util.DateIterator;
import com.gvg.syena.core.services.chart.creator.ChartCreator;
import com.gvg.syena.core.services.chart.creator.ChartCreatorFactory;

public class ChartDataGenerator {

	@Transactional
	public static ChartData<Date> generateChartData(HierarchyNode hierarchyNode, Date fromTime, Date toTime, ChartType[] chartTypes) {

		 chartTypes = new ChartType[] { ChartType.Actual};

		Set<ChartCreator> chartCreators = ChartCreatorFactory.getInstance().getChartCreators(chartTypes);
		ChartData<Date> chartData = new ChartData<Date>(toNameList(chartCreators));

		int numLines = 0;
		for (ChartCreator chartCreator : chartCreators) {
			numLines = numLines + chartCreator.getChartTypes().size();
		}

		TreeMap<Date, Float[]> val = new TreeMap<Date, Float[]>();
		int currentPos = 0;
		int valuesSublength = 0;
		for (ChartCreator chartCreator : chartCreators) {
			chartCreator.prepareData(hierarchyNode, toTime);
			DateIterator graphPoints = new DateIterator(fromTime, toTime, ApplicationUtil.getChartPeriod(fromTime, toTime));
			while (graphPoints.hasNext()) {
				Date graphPoint = graphPoints.next();
				Float[] valuesSub = chartCreator.getChartPointsOn(hierarchyNode, graphPoint);
				Float[] vals = val.get(graphPoint);
				if (vals == null) {
					vals = new Float[numLines];
					val.put(graphPoint, vals);
				}
				System.arraycopy(valuesSub, 0, vals, currentPos, valuesSub.length);
				valuesSublength = valuesSub.length;
			}
			currentPos = currentPos + valuesSublength;
		}

		for (Date date : val.keySet()) {
			chartData.addChartPoint(date, val.get(date));
		}
		chartData.removeAfter100();
		chartData.removeAfter0();
		List<ChartType> chartTypess = getChartType(chartCreators);
		for (int i = 0; i < chartTypess.size(); i++) {
			ChartType chartTypesx = chartTypess.get(i);
			if (chartTypesx == ChartType.Actual) {
				chartData.removeAfter(DateUtil.today(), i);
			}
		}

		return chartData;
	}

	private static String[] toNameList(Set<ChartCreator> chartCreators) {

		List<ChartType> chartTypes = getChartType(chartCreators);
		// return chartTypes.
		String[] predictionChartTypeName = new String[chartTypes.size()];
		int i = 0;
		for (ChartType chartType : chartTypes) {
			predictionChartTypeName[i++] = chartType.getName();
		}
		return predictionChartTypeName;
	}

	private static List<ChartType> getChartType(Set<ChartCreator> chartCreators) {
		List<ChartType> chartTypes = new ArrayList<>();
		for (ChartCreator chartCreator : chartCreators) {
			List<ChartType> types = chartCreator.getChartTypes();
			chartTypes.addAll(types);
		}
		return chartTypes;
	}

}
