package com.gvg.syena.core.services.dependency.analysis;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.DependencyMetrix;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.services.dependency.api.DependencyAnalyzer;
import com.gvg.syena.core.utilities.AppUtil;

@Component("dependencyAnalyzer")
public class DependencyAnalyzerImpl implements DependencyAnalyzer {

	@Override
	@Transactional
	public void analyzeDependency(DependencyAttachable dependentTo, List<Job> jobs, DependencyType dependencyType) throws DependencyFailureException {
		for (Job dependent : jobs) {
			checkDependency((DependencyAttachableHierarchy) dependentTo, (DependencyAttachableHierarchy) dependent, dependencyType);
		}

	}

	@Override
	@Transactional
	public void analyzeDependency(DependencyAttachableHierarchy dependentTo, List<DependencyAttachableHierarchy> dependents, DependencyType dependencyType)
			throws DependencyFailureException {

		for (DependencyAttachableHierarchy dependent : dependents) {
			checkDependency(dependentTo, dependent, dependencyType);
		}

	}

	private void checkDependency(DependencyAttachableHierarchy dependentTo, DependencyAttachableHierarchy dependent, DependencyType dependencyType)
			throws DependencyFailureException {
		if (dependentTo.getId() == dependent.getId()) {
			throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "dependentTo and dependents are same");
		} else {
			DependencyMetrix dependencyMetrixDirect = new DependencyMetrix(dependencyType);
			// DependencyMetrix dependencyMetrixInverse = new
			// DependencyMetrix(dependencyType);
			checkDependencyInDependency(dependent, dependentTo, dependencyMetrixDirect, true);
			checkDependencyInDependency(dependentTo, dependent, dependencyMetrixDirect, false);

			// checkDependencyInDependency(dependent, dependentTo,
			// dependencyMetrixInverse);
			// checkSide(dependencyMetrixDirect, dependencyMetrixInverse);

			checkDependencyInParent(dependentTo, dependent, dependencyMetrixDirect, false);
			checkDependencyInChild(dependentTo, dependent, dependencyMetrixDirect, false);
		}

	}

	private void checkSide(DependencyMetrix dependencyMetrixDirect, DependencyMetrix dependencyMetrixInverse) throws DependencyFailureException {
		boolean result = DependencyMetrix.checkSide(dependencyMetrixDirect, dependencyMetrixInverse);
		if (result) {
			throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "dependentTo and dependents are same");
		}

	}

	private void checkDependencyInParent(DependencyAttachableHierarchy node, DependencyAttachableHierarchy dependent, DependencyMetrix dependencyMetrix, boolean inverse)
			throws DependencyFailureException {
		DependencyAttachableHierarchy parent = node.getParent();
		if (parent != null) {
			checkDependencyInDependency(parent, dependent, dependencyMetrix, inverse);
			checkDependencyInParent(parent, dependent, dependencyMetrix, inverse);
		}
	}

	private void checkDependencyInChild(DependencyAttachableHierarchy node, DependencyAttachableHierarchy dependent, DependencyMetrix dependencyMetrix, boolean inverse)
			throws DependencyFailureException {
		if (node != null) {
			Set<DependencyAttachableHierarchy> children = node.getChildren();
			if (children != null) {
				for (DependencyAttachableHierarchy child : children) {
					checkDependencyInDependency(child, dependent, dependencyMetrix, inverse);
					checkDependencyInChild(child, dependent, dependencyMetrix, inverse);
				}
			}
		}

	}

	private void checkDependencyInDependency(DependencyAttachableHierarchy dependentTo, DependencyAttachableHierarchy dependent, DependencyMetrix dependencyMetrix, boolean inverse)
			throws DependencyFailureException {
		DependencyJobs predecessorDependency = dependentTo.getPredecessorDependency();

		dependencyMetrixJob(predecessorDependency.getStartToStart().getJobs(), dependent, dependencyMetrix, DependencyType.startToStart, inverse);
		dependencyMetrixJob(predecessorDependency.getStartToFinish().getJobs(), dependent, dependencyMetrix, DependencyType.startToFinish, inverse);
		dependencyMetrixJob(predecessorDependency.getFinishToStart().getJobs(), dependent, dependencyMetrix, DependencyType.FinishToStart, inverse);
		dependencyMetrixJob(predecessorDependency.getFinishToFinish().getJobs(), dependent, dependencyMetrix, DependencyType.FinishToFinish, inverse);

		checkDependencyInJobGroup(predecessorDependency.getStartToStart().getJobGroups(), dependent, dependencyMetrix, DependencyType.startToStart, inverse);
		checkDependencyInJobGroup(predecessorDependency.getStartToFinish().getJobGroups(), dependent, dependencyMetrix, DependencyType.startToFinish, inverse);
		checkDependencyInJobGroup(predecessorDependency.getFinishToStart().getJobGroups(), dependent, dependencyMetrix, DependencyType.FinishToStart, inverse);
		checkDependencyInJobGroup(predecessorDependency.getFinishToFinish().getJobGroups(), dependent, dependencyMetrix, DependencyType.FinishToFinish, inverse);

	}

	private void checkDependencyInJobGroup(Set<JobGroup<Job>> jobGroups, DependencyAttachableHierarchy dependent, DependencyMetrix dependencyMetrix, DependencyType dependencyType,
			boolean inverse) throws DependencyFailureException {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				dependencyMetrixJob(jobGroup.getJobs(), dependent, dependencyMetrix, dependencyType, inverse);
				checkDependencyInJobGroup(jobGroup.getJobGroups(), dependent, dependencyMetrix, dependencyType, inverse);
			}
		}

	}

	private void dependencyMetrixJob(Set<Job> jobs, DependencyAttachableHierarchy dependent, DependencyMetrix dependencyMetrix, DependencyType dependencyType, boolean inverse)
			throws DependencyFailureException {
		if (jobs.contains(dependent)) {
			if (dependencyMetrix.hasCross(dependencyType, inverse)) {
				throw new DependencyFailureException(dependencyMetrix, MessageKeys.CYCLIC_REFERENCE, "Cyclic reference DependencyFailure");
			}
			dependencyMetrix.add(dependencyType);
		}
		for (Job job : jobs) {
			checkDependencyInDependency(job, dependent, dependencyMetrix, inverse);
		}

	}

	@Override
	public void analyzeDependency(DependencyAttachable parentJob, JobGroup jobGroup, DependencyType dependencyType) throws DependencyFailureException {
		Set<Job> set = jobGroup.getJobs();
		List<Job> jobs = AppUtil.convertToList(set);
		analyzeDependency(parentJob, jobs, dependencyType);
		Set<JobGroup> jobGroups = jobGroup.getJobGroups();
		for (JobGroup group : jobGroups) {
			analyzeDependency(parentJob, group, dependencyType);
		}

	}

}
