package com.gvg.syena.core.datarepository.diary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.mysema.query.types.Predicate;



public interface DiaryThreadRepository extends PagingAndSortingRepository<DiaryThread, Long>,QueryDslPredicateExecutor<DiaryThread> {
	
	@Query("SELECT dt FROM DiaryThread dt ORDER BY dt.lastUpdatedDate DESC")
	Page<DiaryThread> getLatestThreads(Pageable pageable);
	
	Page<DiaryThread> findAll(Predicate predicate, Pageable pageable);

}
