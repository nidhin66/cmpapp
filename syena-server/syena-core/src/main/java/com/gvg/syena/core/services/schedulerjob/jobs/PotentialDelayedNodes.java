package com.gvg.syena.core.services.schedulerjob.jobs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.scheduler.ScheduleVarianceLog;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.util.ApplicationDateUtil;
import com.gvg.syena.core.datarepository.ApplicationConfigurationRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.datarepository.scheduler.ScheduleVarianceLogRepository;
import com.gvg.syena.core.services.criticalpath.CriticalAndNonCriticalPath;
import com.gvg.syena.core.services.criticalpath.CriticalPathFinder;
import com.gvg.syena.core.services.criticalpath.JobData;
import com.gvg.syena.core.utilities.UtilServices;

public class PotentialDelayedNodes extends QuartzJobBean implements ApplicationContextAware {

	private static ApplicationContext applicationContext = null;
	private JobRepository jobRepository = null;
	private ScheduleVarianceLogRepository scheduleVarianceLogRepository;
	private CriticalPathFinder criticalPathFinder = null;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		PotentialDelayedNodes.applicationContext = applicationContext;

	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		criticalPathFinder = applicationContext.getBean(CriticalPathFinder.class);
		HierarchyManagement hierarchyManagement = applicationContext.getBean(HierarchyManagement.class);
		jobRepository = applicationContext.getBean(JobRepository.class);
		scheduleVarianceLogRepository = applicationContext.getBean(ScheduleVarianceLogRepository.class);
		ApplicationConfigurationRepository applicationConfigurationRepository = applicationContext
				.getBean(ApplicationConfigurationRepository.class);
		ApplicationConfiguration config = applicationConfigurationRepository
				.findOne(ApplicationDateUtil.APPLICATION_DATE_HOUR_DEFERENCE);
		int deference = 0;
		if (null != config) {
			deference = Integer.valueOf(config.getParamValue());
		}
		Date applicationDate = ApplicationDateUtil.getApplicationDate(deference);
		List<HierarchyNode> plants = null;
		try {
			PageItr<HierarchyNode> plantPage = hierarchyManagement.getAllHierarchyNodes("Plant", 0, 100);
			plants = plantPage.getContent();
		} catch (HierarchyLevelNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null != plants) {
			for (HierarchyNode plant : plants) {
				findPotentialDelays(plant);
			}
		}
		
		List<HierarchyNode> units = null;
		try {
			PageItr<HierarchyNode> unitPAge = hierarchyManagement.getAllHierarchyNodes("Unit", 0, 100);
			units = unitPAge.getContent();
		} catch (HierarchyLevelNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (null != units) {
			for (HierarchyNode unit : units) {
				findPotentialDelays(unit);
			}
		}

	}

	private void findPotentialDelays(HierarchyNode node) {
		int plantDelayDays = 0;
		int plantPlannedDays = 0;
		WorkTime plantWorkTime = node.getWorkTime();
		if (null != plantWorkTime) {
			Date plantStartDate = plantWorkTime.getPlannedStartTime();
			Date plantEndDate = plantWorkTime.getPlannedFinishTime();
			plantPlannedDays = UtilServices.dateDifferenceInDays(plantStartDate, plantEndDate);
		}
		try {
			CriticalAndNonCriticalPath criticalAndNonCriticalPath = criticalPathFinder
					.findCriticalAndNonCriticalPath(node.getId());
			List<JobData> jobDatas = criticalAndNonCriticalPath.getCriticalPath();
			List<Long> criticalJobIds = new ArrayList<>();			
			if (null != jobDatas) {
				for (JobData jobData : jobDatas) {
					criticalJobIds.add(jobData.getJobId());
				}
			}
			Iterable<Job> iterableJobs = jobRepository.findAll(criticalJobIds);
			if (null != iterableJobs) {
				for (Job job : iterableJobs) {
					WorkSchedule workSchedule = job.getWorkSchedule();
					if (null != workSchedule) {
						Date plannedStart = workSchedule.getPlannedStartTime();
						Date plannedEnd = workSchedule.getPlannedFinishTime();
						Date actualtStart = workSchedule.getActualStartTime();
						Date actualFinish = workSchedule.getActualFinishTime();
						int plannedDays = 0;
						int actualDays = 0;
						int daysinDelay = 0;
						if (null != plannedStart && null != plannedEnd) {
							plannedDays = UtilServices.dateDifferenceInDays(plannedStart, plannedEnd);
						}
						boolean isStarted = false;
						boolean isCompleted = false;

						if (null != actualtStart) {
							isStarted = true;
						}
						if (null != actualFinish) {
							isCompleted = true;
						}
						if (isCompleted) {
							actualDays = UtilServices.dateDifferenceInDays(actualtStart, actualFinish);
							daysinDelay = actualDays - plannedDays;
						} else if (isStarted && !isCompleted) {
							daysinDelay = UtilServices.dateDifferenceInDays(actualtStart, plannedStart);
						}
						if (daysinDelay > 0) {
							plantDelayDays = plantDelayDays + daysinDelay;
						}

					}
				}
			}
		} catch (HierarchyNodeNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(plantDelayDays !=0 && plantPlannedDays != 0){
			if (plantDelayDays > 0) {
				ScheduleVarianceLog log = scheduleVarianceLogRepository.findOne(node.getId());
				if(null == log){
					log = new ScheduleVarianceLog();
					log.setHierarchyId(node.getId());
				}
				log.setCurrentVariance(plantDelayDays/plantPlannedDays);
				scheduleVarianceLogRepository.save(log);
			}else{
				scheduleVarianceLogRepository.delete(node.getId());
			}
		}
		
	}

}
