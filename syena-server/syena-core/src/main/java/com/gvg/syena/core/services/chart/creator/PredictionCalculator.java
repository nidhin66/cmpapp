package com.gvg.syena.core.services.chart.creator;

import java.util.Date;
import java.util.Set;
import java.util.SortedSet;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.time.ActionTime;
import com.gvg.syena.core.api.util.Configuration;

public class PredictionCalculator {

	public void predict(HierarchyNode hierarchyNode, double efficiencyFactor) {
		predictHierarchyNode(hierarchyNode, efficiencyFactor);
	}

	public void predictHierarchyNode(HierarchyNode hierarchyNode, double efficiencyFactor) {
		if (hierarchyNode != null) {
			predictInJobs(hierarchyNode.getJobs(), efficiencyFactor);
			predictInJobGroup(hierarchyNode.getJobGroups(), efficiencyFactor);
			Set<HierarchyNode> children = hierarchyNode.getChildren();
			if (children != null && children.size() > 0) {
				for (HierarchyNode node : children) {
					predictHierarchyNode(node, efficiencyFactor);
				}
			}
		}

	}

	public void predictInJobGroup(Set<JobGroup<Job>> jobGroups, double efficiencyFactor) {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				predictInJobs(jobGroup.getJobs(), efficiencyFactor);
				predictInJobGroup(jobGroup.getJobGroups(), efficiencyFactor);
			}
		}

	}

	public void predictInJobs(Set<Job> jobs, double efficiencyFactor) {
		for (Job job : jobs) {
			predictInJob(job, efficiencyFactor);
		}

	}

	private void predictInJob(Job job, double efficiencyFactor) {
		SortedSet<ScheduledStage> scheduledstages = job.getWorkSchedule().getScheduledStages();
		ScheduledStage lastActualEnterd = scheduledstages.first();
		for (ScheduledStage scheduledStage : scheduledstages) {
			if (scheduledStage.isActualEnterd()) {
				lastActualEnterd = scheduledStage;
				scheduledStage.setPredictedTime(scheduledStage.getActualTime());
			}
		}
		SortedSet<ScheduledStage> tailSet = scheduledstages.tailSet(lastActualEnterd);
		if (lastActualEnterd.getActionTime() != null) {
			Date startTimez = lastActualEnterd.getActionTime().getPlannedTime();
			for (ScheduledStage scheduledStage : tailSet) {
				ActionTime actionTime = scheduledStage.getActionTime();
				if (actionTime != null) {
					Date startTimey = actionTime.getPlannedTime();
					int kk = Configuration.personDaysCPMCalendar.getPersonDays(startTimey, startTimez);
					int kku = (int) (kk * efficiencyFactor);
					Date ad = Configuration.personDaysCPMCalendar.getDateAfter(startTimez, kku);
					scheduledStage.setPredictedTime(ad);
					startTimez = startTimey;
				}
			}
		}

	}
}
