package com.gvg.syena.core.datarepository.job;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;

public interface JobRepository extends PagingAndSortingRepository<Job, Long> {

	String advancedSearchOptionQuery = "SELECT c FROM Job c where c.jobName LIKE :nameStr and c.description LIKE :descStr and c.owner.id LIKE :ownerId and c.initiator.id LIKE :initiatorId and c.workStatus.runningStatus in :runningStatus";

	List<Job> findByIdIn(Collection<Long> ids);

	List<Job> findByConnectedNodeIdAndStandardJobIdIn(long hierarchyNodeId, long[] standardJobIds);

	Page<Job> findByIdInAndJobNameLike(List<Long> ids, String jobName, Pageable pageable);

	Page<Job> findByNodeType(NodeType nodeType, Pageable pageable);

	Page<Job> findByIdAndConnectedNodeIdOrJobNameLikeIgnoreCaseOrNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseAndOwnerIdLikeIgnoreCaseAndInitiatorIdLikeIgnoreCaseAndNodeType(
			long id, long connectedNodeId, String name, String name2, String description, String ownerId, String initiatorId, NodeType instance, Pageable pageable);

	List<Job> findByWorkScheduleIsNotNull();

	Page<Job> findByIdAndConnectedNodeIdOrJobNameLikeIgnoreCaseOrNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrOwnerIdLikeIgnoreCaseOrInitiatorIdLikeIgnoreCaseAndNodeType(
			long id, long connectedNodeId, String name, String name2, String description, String ownerId, String initiatorId, NodeType nodeType, Pageable pageable);

	Page<Job> findByParentIdIn(Collection<Long> parentIds, Pageable pageable);

	Page<Job> findByWorkStatusRunningStatusInAndWorkStatusDelayToEndAndWorkStatusDelayToStart(RunningStatus[] runningStatus, boolean b, boolean c, Pageable pageable);

	Page<Job> findByInitiatorIdAndJobNameLike(String personId, String jobName, Pageable pageable);

	Page<Job> findByOwnerIdAndJobNameLike(String personId, String jobName, Pageable pageable);

	Page<Job> findByconnectedNodeIdIn(List<Long> ids, Pageable pageable);

	@Query("SELECT c FROM Job c where c.id in (:ids)")
	Page<Job> findByIds(@Param("ids") Collection<Long> ids, Pageable pageable);

	@Query("SELECT c FROM Job c where c.id in (:ids) and c.nodeType = '2'")
	Page<Job> findInstanceJobByIds(@Param("ids") Collection<Long> ids, Pageable pageable);

	@Query("SELECT c FROM Job c where ((:date   >= c.workTime.startTime.actualTime and c.workTime.finishTime.actualTime  >=  :date) or(:date   >= c.workTime.startTime.actualTime and c.workTime.finishTime.actualTime  is null)) and c.description like :jobName")
	Page<Job> getOnGoingJobs(@Param("date") Date date, @Param("jobName") String jobName, Pageable pageable);

	@Query("SELECT c FROM Job c where  :date  >= c.workTime.finishTime.actualTime  and c.workStatus.runningStatus = '3' and c.description like :jobName")
	Page<Job> completedJobs(@Param("date") Date date, @Param("jobName") String jobName, Pageable pageable);

	@Query("select c from Job c JOIN c.workSchedule.scheduledStages s where s.percentageOfWork = 0.0 and COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) >= :date and  s.actionTime.actualTime IS NULL and c.description like :jobName")
	Page<Job> plannedJobs(@Param("date") Date date, @Param("jobName") String jobName, Pageable pageable);

	@Query(value = "SELECT * FROM job j where connectedNode_id in (:hierarchyNodeIds) and COALESCE(finish_revisedTime,finish_estimatedTime) = (select max(COALESCE(finish_revisedTime,finish_estimatedTime)) from job where id in(SELECT id FROM job where connectedNode_id in (:hierarchyNodeIds)))", nativeQuery = true)
	Set<Job> jobsInCriticalPath(@Param("hierarchyNodeIds") List<Long> hierarchyNodeIds);

	@Query("select distinct j.connectedNode from Job j where j.id in (:ids)) ")
	Page<HierarchyNode> findParentHierarchyNodes(@Param("ids") List<Long> ids, Pageable pageable);

	@Query("select c from Job c JOIN c.workSchedule.scheduledStages s where s.percentageOfWork = 0.0 and COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) between :startDate and :endDate and s.actionTime.actualTime IS NULL and c.description like :jobName")
	Page<Job> jobsStartInNDays(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("jobName") String jobName, Pageable pageable);

	@Query("select c from Job c JOIN c.workSchedule.scheduledStages s where s.percentageOfWork = 100.0 and COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) between :startDate and :endDate and s.actionTime.actualTime IS NULL and c.description like :jobName")
	Page<Job> jobsCompleteInNDays(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("jobName") String jobName, Pageable pageable);

}

// Page<Job>
// findByIdOrJobNameLikeOrNameLikeOrDescriptionLikeOrOwnerIdLikeOrInitiatorIdLike(long
// id, String jobName, String name, String description, String ownerId,
// String initiatorId, Pageable pageable);

//
// Page<Job>
// findByIdOrJobNameLikeOrNameLikeOrDescriptionLikeOrOwnerIdLikeOrInitiatorIdLikeAndConnectedNodeId(long
// id, String jobName, String name, String description,
// String ownerId, String initiatorId, long connectedNodeId, Pageable
// pageable);
//

// Page<Job> findByWorkStatusRunningStatus(RunningStatus runningStatus, Date
// date, Pageable pageable);

// @Query("SELECT c FROM Job c where  :date  BETWEEN c.workTime.startTime.actualTime and c.workTime.finishTime.actualTime  and workStatus.delayInCurrentStage = '1'")
// @Query("select distinct c from Job c JOIN c.workSchedule.scheduledStages s where COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) < :date and  ( s.actionTime.actualTime IS NULL or s.actionTime.actualTime != COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime))")
// Page<Job> delayedJobs(@Param("date") Date date, Pageable pageable);

// @Query(advancedSearchOptionQuery)
// Page<Job> findByAdvancedSearchOptions(@Param("nameStr") String nameStr,
// @Param("descStr") String descStr, @Param("ownerId") String ownerId,
// @Param("initiatorId") String initiatorId, @Param("runningStatus")
// List<RunningStatus> runningStatus, Pageable pagable);

// @Param("startTime") Date startTime, @Param("finishTime") Date finishTime,
//
// @Param("initiatorId") String initiatorId, ,

// String advancedSearchOptionQuery =
// "SELECT c FROM Job c where ( c.jobName LIKE :nameStr or c.jobName is null )and ( c.description LIKE :descStr or c.description is null) and (c.owner.id LIKE :ownerId or c.owner.id is null)  and  ( c.initiator.id LIKE :initiatorId or  c.initiator.id is null) and ( (workTime.startTime.actualTime between :startTime and :finishTime) or workTime.startTime.actualTime is null) and ( (workTime.finishTime.actualTime between :startTime and :finishTime) or workTime.finishTime.actualTime is null)  and c.workStatus.runningStatus in :runningStatus ";
// and c.description LIKE :descStr and c.owner.id LIKE :ownerId and
// c.initiator.id LIKE :initiatorId
//

// Page<Job> findByParentId(long jobGroupId, Pageable pageable);

// @Query(value =
// "SELECT  jobs_id FROM JOBGROUP_STANDARDJOB where JobGroup_id = ?",
// nativeQuery = true)
// int[] findParentId(long JobGroup_id);

// Page<Job> findByIdIn(long[] ids, Pageable pageable);
