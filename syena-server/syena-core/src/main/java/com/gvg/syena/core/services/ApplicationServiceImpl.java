package com.gvg.syena.core.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.services.ApplicationService;
import com.gvg.syena.core.datarepository.ApplicationConfigurationRepository;

@Service("applicationService")
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private ApplicationConfigurationRepository applicationConfigurationRepository;

	@Override
	public String getPropertyValue(String key) {
		ApplicationConfiguration config = applicationConfigurationRepository.findOne(key);
		if (config != null) {
			return config.getParamValue();
		}
		return null;
	}

	@Override
	public List<ApplicationConfiguration> getApplicationConfigs() {
		Iterable<ApplicationConfiguration> applicationConfigs = applicationConfigurationRepository.findAll();
		List<ApplicationConfiguration> configList = new ArrayList<>();
		if (null != applicationConfigs) {
			for (ApplicationConfiguration applicationConfiguration : applicationConfigs) {
				configList.add(applicationConfiguration);
			}
		}
		return configList;
	}

}
