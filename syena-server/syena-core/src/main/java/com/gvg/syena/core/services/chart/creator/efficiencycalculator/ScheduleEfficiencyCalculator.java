package com.gvg.syena.core.services.chart.creator.efficiencycalculator;

import java.util.Date;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.schedule.WorkPercentageType;
import com.gvg.syena.core.services.chart.creator.WorkPercentageFinder;

public class ScheduleEfficiencyCalculator implements EfficiencyCalculator {

	private WorkPercentageFinder workPercentageFinder;

	public ScheduleEfficiencyCalculator(WorkPercentageFinder workPercentageFinder) {
		this.workPercentageFinder = workPercentageFinder;
	}

	@Override
	public double calculateEfficiencyFactor(HierarchyNode hierarchyNode, Date ondate) {
		double actualWorkPercentage = workPercentageFinder.findWorkPercentage(hierarchyNode, ondate, WorkPercentageType.Actual);
		double estimatedWorkPercentage = workPercentageFinder.findWorkPercentage(hierarchyNode, ondate, WorkPercentageType.Planned);
		if (estimatedWorkPercentage == 0 || actualWorkPercentage == 0) {
			return 0;
		}
		return actualWorkPercentage / estimatedWorkPercentage;
	}

}
