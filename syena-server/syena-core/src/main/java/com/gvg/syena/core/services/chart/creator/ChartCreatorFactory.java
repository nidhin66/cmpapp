package com.gvg.syena.core.services.chart.creator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.job.schedule.ChartCreatorType;

public class ChartCreatorFactory {

	private static ChartCreatorFactory me;

	private ChartCreatorFactory() {
	}

	public static ChartCreatorFactory getInstance() {
		if (me == null) {
			me = new ChartCreatorFactory();
		}
		return me;
	}

	public Set<ChartCreator> getChartCreators(ChartType[] chartTypes) {
		Map<ChartCreatorType, ChartCreator> chartCreators = new HashMap<ChartCreatorType, ChartCreator>(5);
		Set<ChartCreator> calculators = new TreeSet<ChartCreator>();
		for (ChartType chartType : chartTypes) {
			ChartCreatorType calculatorType = chartType.getWorkPercentageType().getChartCreatorType();
			ChartCreator chartCreator = chartCreators.get(calculatorType);
			if (chartCreator == null) {
				chartCreator = getPredictionCalculator(calculatorType);
				chartCreators.put(calculatorType, chartCreator);
				calculators.add(chartCreator);
			}
			chartCreator.addChartType(chartType);

		}
		return calculators;
	}

	private ChartCreator getPredictionCalculator(ChartCreatorType calculatorType) {
		ChartCreator chartCreator = null;
		if (calculatorType == calculatorType.Base) {
			chartCreator = new BaseChartCreator();
		} else if (calculatorType == calculatorType.Projected) {
			chartCreator = new ProjectedChartCreator();
		} else if (calculatorType == calculatorType.workefficiency) {
			chartCreator = new WorkefficiencyChartCreator();
		} else if (calculatorType == calculatorType.planningefficiency) {
			chartCreator = new PlanningefficiencyChartCreator();
		} else if (calculatorType == calculatorType.scheduleefficiency) {
			chartCreator = new ScheduleEfficiencyChartCreator();
		}
		return chartCreator;
	}
}
