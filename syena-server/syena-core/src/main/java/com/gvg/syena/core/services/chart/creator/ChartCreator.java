package com.gvg.syena.core.services.chart.creator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.services.chart.ChartDataAggregator;

public abstract class ChartCreator implements Comparable<ChartCreator> {

	private int id;

	private List<ChartType> chartTypes;

	private ChartDataAggregator chartDataAggregator;

	public ChartCreator(int id) {
		this.id = id;
		this.chartTypes = new ArrayList<ChartType>(3);
		this.chartDataAggregator = new ChartDataAggregator();
	}

	public void addChartType(ChartType chartType) {
		this.chartTypes.add(chartType);
	}

	public List<ChartType> getChartTypes() {
		return chartTypes;
	}

	public abstract void prepareData(HierarchyNode hierarchyNode, Date toTime);

	public Float[] getChartPointsOn(HierarchyNode hierarchyNode, Date onDate) {
		chartDataAggregator.reset(chartTypes);
		return chartDataAggregator.getGraphPoint(hierarchyNode, onDate);
	}

	public int getId() {
		return id;
	}

	@Override
	public int compareTo(ChartCreator o) {
		return this.getId() - o.getId();
	}
}
