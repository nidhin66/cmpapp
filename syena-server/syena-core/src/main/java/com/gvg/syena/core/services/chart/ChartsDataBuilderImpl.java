package com.gvg.syena.core.services.chart;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.NotScheduledException;
import com.gvg.syena.core.api.services.chart.ChartsDataBuilder;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.utilities.UtilServices;

@Service("chartsDataBuilder")
public class ChartsDataBuilderImpl implements ChartsDataBuilder {

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Override
	@Transactional
	public ChartData<Date> getChartsDataOfHierarchyNode(long hierarchyNodeId, ChartType[] chartTypes) throws HierarchyNodeNotFoundException, NotScheduledException {

		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Hierarchy Node Not Found");
		}

		Date fromTime = UtilServices.getMinFromTime(hierarchyNode);
		Date toTime = UtilServices.getMaxToTime(hierarchyNode);

		return ChartDataGenerator.generateChartData(hierarchyNode, fromTime, toTime, chartTypes);

	}

	@Override
	public ChartData<Date> getChartsDataOfJobGroup(long jobGroupId, ChartType[] chartTypes) throws HierarchyNodeNotFoundException, NotScheduledException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChartData<Date> getChartsDataOfJob(long jobId, ChartType[] chartTypes) throws HierarchyNodeNotFoundException, NotScheduledException {
		// TODO Auto-generated method stub
		return null;
	}

}
