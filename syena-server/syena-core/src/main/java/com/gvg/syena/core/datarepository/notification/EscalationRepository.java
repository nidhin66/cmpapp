package com.gvg.syena.core.datarepository.notification;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.notification.Escalation;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;

public interface EscalationRepository extends CrudRepository<Escalation, Long>{
	public List<Escalation> findByToPersons(Person toPerson);
	public List<Escalation> findByToRolesIn(List<Role> toRoles);
}
