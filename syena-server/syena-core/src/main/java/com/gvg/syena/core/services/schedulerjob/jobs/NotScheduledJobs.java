package com.gvg.syena.core.services.schedulerjob.jobs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.services.NotificationService;
import com.gvg.syena.core.api.services.notification.NotificationRecord;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;
import com.gvg.syena.core.api.services.notification.PlaceHolderTableData;
import com.gvg.syena.core.api.services.notification.PlaceHolderType;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.datarepository.notification.NotificationConfigRepository;
import com.gvg.syena.core.services.notification.NotificationServiceImpl;

@Service("notScheduledJobs")
public class NotScheduledJobs extends QuartzJobBean implements ApplicationContextAware{
	
	private static final String jobGroupsToBeScheduledQury = "select * from jobgroup where nodeType = 2 and id in (select distinct JobGroup_id from jobgroup_scheduledstage where scheduledstages_id in (select id from scheduledstage where percentageofwork = 0 and estimatedtime is null)) ";
	private static final String jobsToBeScheduledQury = "select * from job where id in (select distinct job_id from job_scheduledstage where scheduledstages_id in (select id from scheduledstage where percentageofwork = 0 and estimatedtime is null))";
	
	private static ApplicationContext applicationContext = null;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		NotScheduledJobs.applicationContext = applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		JobRepository jobRepository = applicationContext.getBean(JobRepository.class);
		EntityManager em = applicationContext.getBean(EntityManager.class);
		NotificationConfigRepository notificationConfigRepository = applicationContext.getBean(NotificationConfigRepository.class);	
		NotificationConfig notificationConfig = notificationConfigRepository.findOne("NOT_SCHEDULED");
		List<JobGroup> jobGroups = em.createNativeQuery(jobGroupsToBeScheduledQury, JobGroup.class).getResultList();
		
		List<Job> jobs = em.createNativeQuery(jobsToBeScheduledQury, Job.class).getResultList();
		
		boolean dataFound = false;
		List<PlaceHolderData> placeHolders = new ArrayList<>();
		PlaceHolderData jobGroupsData = null;
		PlaceHolderData jobData = null;
		if(null != jobs && jobs.size() > 0){
			dataFound = true;
			jobData =  new PlaceHolderData("JOB_DATA", PlaceHolderType.TABLE);
			PlaceHolderTableData tableData = new PlaceHolderTableData(2);
			String[] headers = new String[2];
			headers[0] = "Name";
			headers[1] = "Description";		
			tableData.setHeader(headers);
			for(Job job : jobs){
				if(null == job.getConnectedNode()){
					String[] row = new String[2];
					row[0] = job.getName();
					row[1] = job.getDescription();
					tableData.addRow(row);	
				}							
			}
			jobData.setValue(tableData);
			placeHolders.add(jobData);
		}
		
		if(null != jobGroups && jobGroups.size() > 0){
			dataFound = true;
			jobGroupsData =  new PlaceHolderData("JOB_GROUP_DATA", PlaceHolderType.TABLE);
			PlaceHolderTableData tableData = new PlaceHolderTableData(2);
			String[] headers = new String[2];
			headers[0] = "Name";
			headers[1] = "Description";		
			tableData.setHeader(headers);
			for(JobGroup jobGroup : jobGroups){
				String[] row = new String[2];
				row[0] = jobGroup.getName();
				row[1] = jobGroup.getDescription();
				tableData.addRow(row);
			}
			jobGroupsData.setValue(tableData);
			placeHolders.add(jobGroupsData);
		}
		if(dataFound){
			List<NotificationRecord> records = new ArrayList<>();
			NotificationRecord record = new NotificationRecord();
			record.setRecordId(notificationConfig.getConfigId());
			record.setSubject(notificationConfig.getName());
			record.setPlaceHolderDatas(placeHolders);
			records.add(record);
			NotificationService notificationService  = applicationContext.getBean(NotificationServiceImpl.class);
			notificationService.notify(notificationConfig,records);
		}
		

	}

}
