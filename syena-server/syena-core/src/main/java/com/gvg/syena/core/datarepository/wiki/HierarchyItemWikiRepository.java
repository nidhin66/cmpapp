package com.gvg.syena.core.datarepository.wiki;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.wiki.HierarchyItemWiki;

public interface HierarchyItemWikiRepository extends CrudRepository<HierarchyItemWiki,Long>{
	public HierarchyItemWiki findByHierarchyNodeId(long hierarchyNodeId);
}
