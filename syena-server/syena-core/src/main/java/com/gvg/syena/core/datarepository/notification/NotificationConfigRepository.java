package com.gvg.syena.core.datarepository.notification;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.services.notification.NotificationType;

public interface NotificationConfigRepository extends CrudRepository<NotificationConfig, String> {
	List<NotificationConfig> findByType(NotificationType type);
}
