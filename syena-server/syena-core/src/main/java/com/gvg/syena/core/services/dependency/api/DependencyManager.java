package com.gvg.syena.core.services.dependency.api;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;

public interface DependencyManager<T extends CrudRepository> {

	PageItr<Job> getSuccessorJobs(long id, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<Job> getPredecessorJobs(long id, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<JobGroup> getSuccessorJobGroups(long id, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<JobGroup> getPredecessorJobGroups(long id, int pageNumber, int pageSize) throws JobNotFoundException;

	List<Job> addSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType) throws JobNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<Job> addPredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType) throws JobNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<JobGroup<Job>> addPredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<JobGroup<Job>> addSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	void removeSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType) throws JobNotFoundException;

	void removePredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType) throws JobNotFoundException;

	void removePredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException;

	void removeSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException;

	void setDependencyAnalyzer(DependencyAnalyzer dependencyAnalyzer);

	List<Job> addAsPredecessorJobs(DependencyAttachable parentJob, List<Job> predecessorJobs, DependencyType dependencyType) throws DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<JobGroup<Job>> addAsPredecessorJobGroups(DependencyAttachable parentJob, List<JobGroup<Job>> jobGroups, DependencyType dependencyType) throws DependencyFailureException,
			JobScheduleFailed, InvalidDataException;

	void removeAsPredecessorJobs(DependencyAttachable parentJob, List<Job> predecessorJobsList, DependencyType dependencyType);

	void removeAsPredecessorJobGroups(DependencyAttachable parentJob, List<JobGroup<Job>> predecessorJobGroupList, DependencyType dependencyType);

	PageItr<JobGroup> searchSuccessorJobGroups(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<JobGroup> searchPredecessorJobGroups(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<Job> searchSuccessorJobs(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<Job> searchPredecessorJobs(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException;

	List<JobGroup<Job>> addAsSuccessorJobGroups(DependencyAttachable parent, List<JobGroup<Job>> successorJobGroups, DependencyType dependencyType) throws DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<Job> addAsSuccessorJobs(DependencyAttachable parent, List<Job> successorJobs, DependencyType dependencyType) throws DependencyFailureException, JobScheduleFailed, InvalidDataException;

	void removeAsSuccessorJobs(DependencyAttachable parent, List<Job> successorJobs, DependencyType dependencyType);

	void removeAsSuccessorJobGroups(DependencyAttachable parent, List<JobGroup<Job>> predecessorJobGroups, DependencyType dependencyType);

}
