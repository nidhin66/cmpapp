package com.gvg.syena.core.services.schedulerjob.jobs;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.util.ApplicationDateUtil;
import com.gvg.syena.core.datarepository.ApplicationConfigurationRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;

@Service("dailyPlannedActivities")
public class DailyPlannedActivities extends QuartzJobBean implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext = null;
	
	private static final String plannedJobsQuery = "SELECT t1.Job_id,COALESCE(t2.revisedTime,t2.estimatedTime) as JobDate,t2.percentageOfWork FROM job_scheduledstage t1, scheduledstage t2 where t2.id = t1.ScheduledStages_id and  COALESCE(t2.revisedTime,t2.estimatedTime) = :date ";
	private static final String plannedJobGroupQuery = "SELECT t1.JobGroup_id,COALESCE(t2.revisedTime,t2.estimatedTime),t2.percentageOfWork as JobDate FROM jobgroup_scheduledstage t1, scheduledstage t2 where t2.id = t1.ScheduledStages_id and  COALESCE(t2.revisedTime,t2.estimatedTime) = :date";

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		DailyPlannedActivities.applicationContext = applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		ApplicationConfigurationRepository applicationConfigurationRepository = applicationContext.getBean(ApplicationConfigurationRepository.class);
		ApplicationConfiguration config = applicationConfigurationRepository.findOne(ApplicationDateUtil.APPLICATION_DATE_HOUR_DEFERENCE);
		int deference = 0;
		JobRepository jobRepository = applicationContext.getBean(JobRepository.class);
		JobGroupRepository jobGroupRepository = applicationContext.getBean(JobGroupRepository.class);
		
		if(null != config){
			deference = Integer.valueOf(config.getParamValue());
		}
		Date applicationDate = ApplicationDateUtil.getApplicationDate(deference);
		EntityManager em = applicationContext.getBean(EntityManager.class);
		
		Query todaysJobGroupQuery = em.createNativeQuery(plannedJobGroupQuery);
		todaysJobGroupQuery.setParameter("date", applicationDate,TemporalType.DATE);		
		List<Object[]> todaysJobGroupResultList = todaysJobGroupQuery.getResultList();
		if(null != todaysJobGroupResultList){
			for(Object[] row : todaysJobGroupResultList){
				long jobGroupId = ((BigInteger) row[0]).longValue();
				JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
				if (jobGroup.getNodeType() == NodeType.instance) {
					
				}
			}
		}
		
		
		
		Query todayJobQuery = em.createNativeQuery(plannedJobsQuery);
		todayJobQuery.setParameter("date", applicationDate,TemporalType.DATE);		
		List<Object[]> todaysJobResultList = todayJobQuery.getResultList();
		if(null != todaysJobResultList){
			for(Object[] row : todaysJobResultList){
				long jobId = ((BigInteger) row[0]).longValue();
				Job job = jobRepository.findOne(jobId);
				if (job.getNodeType() == NodeType.instance) {
					
				}
			}
		}
		
		
		Query nextDayJobGroupQuery = em.createNativeQuery(plannedJobGroupQuery);
		nextDayJobGroupQuery.setParameter("date", applicationDate,TemporalType.DATE);		
		List<Object[]> nextDayJobGroupResultList = nextDayJobGroupQuery.getResultList();
		if(null != nextDayJobGroupResultList){
			for(Object[] row : nextDayJobGroupResultList){
				long jobGroupId = ((BigInteger) row[0]).longValue();
				JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
				if (jobGroup.getNodeType() == NodeType.instance) {
					
				}
			}
		}
		
		
		
		Query nextDayJobQuery = em.createNativeQuery(plannedJobsQuery);
		nextDayJobQuery.setParameter("date", applicationDate,TemporalType.DATE);		
		List<Object[]> nextDayJobResultList = nextDayJobQuery.getResultList();
		if(null != nextDayJobResultList){
			for(Object[] row : nextDayJobResultList){
				long jobId = ((BigInteger) row[0]).longValue();
				Job job = jobRepository.findOne(jobId);
				if (job.getNodeType() == NodeType.instance) {
					
				}
			}
		}

		
	}

}
