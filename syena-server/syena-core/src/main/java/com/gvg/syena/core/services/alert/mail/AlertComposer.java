package com.gvg.syena.core.services.alert.mail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;

import com.gvg.syena.core.api.entity.alert.mail.MailTemplate;
import com.gvg.syena.core.api.entity.alert.mail.MailToAddress;
import com.gvg.syena.core.api.services.alert.mail.Email;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;
import com.gvg.syena.core.api.services.notification.PlaceHolderTableData;
import com.gvg.syena.core.api.services.notification.PlaceHolderType;
import com.gvg.syena.core.datarepository.alert.mail.MailTemplateRepository;



public class AlertComposer {
	
	@Autowired
	MailTemplateRepository mailTemplateRepository;
	
	private long alertTemplateId;
	private List<PlaceHolderData> placeHolders;
	
	public AlertComposer(long alertTemplateID,List<PlaceHolderData> placeHolders){
		this.alertTemplateId = alertTemplateID;
		this.placeHolders = placeHolders;
	}	
	
	public Email getEmail(){
		Email email = new Email();
		try{
			
			MailTemplate mailTemplate = mailTemplateRepository.findOne(alertTemplateId);
			String templateMessage = mailTemplate.getMessage();
			Map<String, String> data = new HashMap<String, String>();
			for(PlaceHolderData placeHolder : placeHolders){
				if(placeHolder.getType() == PlaceHolderType.TABLE){
					PlaceHolderTableData tableContent = (PlaceHolderTableData) placeHolder.getValue();
					data.put(placeHolder.getPlaceHolderName(),createHtmlTable(tableContent));
				}else{
					data.put(placeHolder.getPlaceHolderName(),String.valueOf(placeHolder.getValue()));
				}
			}
			 String varPrefix = "#{";
			    String varSuffix = "}";
			String message = StrSubstitutor.replace(templateMessage, data,varPrefix,varSuffix);
			
			
			email.setFrom(mailTemplate.getFromAddress());
			email.setSubject(mailTemplate.getSubject());
			
			email.setText(message);

			String[] toAddresses = new String[2];
			toAddresses[0] = "sudeep.mohandas@gamillusvaluegen.com";
			Set<MailToAddress> addresses = mailTemplate.getAdrresses();
			toAddresses[1] = "sudeepcm@gmail.com";
			email.setTo(toAddresses);

		}catch(Exception e){
			e.printStackTrace();
		}
				return email;
	}
	
	private String createHtmlTable(PlaceHolderTableData tableContent){
		StringBuilder sb = new StringBuilder();
		sb.append("<table border=\"1\" style=\"width:100%\">");
		
		//Add Header 
		sb.append('<').append("th").append('>');
		
		for(String header : tableContent.getHeader()){
			sb.append('<').append("td").append('>');
		    sb.append(header);
		    sb.append("</").append("td").append('>');
		}
		sb.append("</").append("th").append('>');
		
		for(String[] row : tableContent.getRows()){
			sb.append('<').append("tr").append('>');
			for(String column : row){
				sb.append('<').append("td").append('>');
			    sb.append(column);
			    sb.append("</").append("td").append('>');
			}
			sb.append("</").append("tr").append('>');

		}		
		sb.append("</table>");
		return sb.toString();
	}
	
}
