package com.gvg.syena.core.services.chart;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkPercentageType;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.util.ApplicationUtil;

public class ChartDataAggregator {

	private Float[] values;

	private WorkPercentageType[] workPercentageTypes;

	private Duration totalDuration;

	public void reset(List<ChartType> chartTypes) {
		ChartType[] chartTypesArray = new ChartType[chartTypes.size()];
		chartTypes.toArray(chartTypesArray);
		reset(chartTypesArray);
	}

	public void reset(final ChartType[] chartTypes) {
		WorkPercentageType[] workPercentageTypes = new WorkPercentageType[chartTypes.length];
		int i = 0;
		for (ChartType chartType : chartTypes) {
			workPercentageTypes[i++] = chartType.getWorkPercentageType();
		}
		reset(workPercentageTypes);
	}

	public void reset(final WorkPercentageType[] workPercentageTypes) {
		values = new Float[workPercentageTypes.length];
		this.workPercentageTypes = workPercentageTypes;
		this.totalDuration = ApplicationUtil.getDurationZero();

	}

	public Float[] getGraphPoint(final HierarchyNode hierarchyNode, final Date graphPoint) {
		getGraphPointOnHierarchyNode(hierarchyNode, graphPoint);
		Float totValue = totalDuration.getDurationValue();
		int i = 0;
		for (Float value : values) {
			if (value != null) {
				if (value == 0) {
					values[i++] = 0f;
				} else {
					values[i++] = value / totValue;
				}
			}
		}
		return values;
	}

	private void getGraphPointOnHierarchyNode(final HierarchyNode hierarchyNode, Date date) {
		if (hierarchyNode != null) {
			Set<Job> jobs = hierarchyNode.getJobs();
			getGraphPointOnJobs(jobs, date);
			Set<JobGroup<Job>> jobGroup = hierarchyNode.getJobGroups();
			getGraphPointOnJobGroups(jobGroup, date);
			Set<HierarchyNode> children = hierarchyNode.getChildren();
			for (HierarchyNode child : children) {
				getGraphPointOnHierarchyNode(child, date);
			}
		}

	}

	private void getGraphPointOnJobGroups(Set<JobGroup<Job>> jobGroups, Date date) {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				Set<Job> jobs = jobGroup.getJobs();
				getGraphPointOnJobs(jobs, date);
				Set<JobGroup<Job>> children = jobGroup.getJobGroups();
				getGraphPointOnJobGroups(children, date);
			}
		}
	}

	private void getGraphPointOnJobs(final Set<Job> jobs, Date date) {
		if (jobs != null) {
			for (Job job : jobs) {
				if(!job.isPlanned()){
					continue;
				}
				this.totalDuration.add(job.getDuration());

				int i = 0;
				for (WorkPercentageType workPercentageTypes : workPercentageTypes) {

					Duration duration = job.getDuration().copy();

					Float percentageComplete = workPercentageTypes.getPercentageComplete(job, date);

					duration.multiple(percentageComplete);

					duration.multiple(job.getWeightage());

					Float value = duration.getDurationValue();

					Float currentValue = values[i];

					if (currentValue == null) {
						values[i] = value;
					} else {
						values[i] = values[i] + value;
					}

					i++;
				}

			}
		}
	}

}
