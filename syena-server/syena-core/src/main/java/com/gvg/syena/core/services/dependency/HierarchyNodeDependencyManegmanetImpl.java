package com.gvg.syena.core.services.dependency;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.dependency.HierarchyNodeDependency;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeDependencySchedulingException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.HierarchyNodeDependencyManegmanet;
import com.gvg.syena.core.datarepository.dependency.HierarchyNodeDependencyRepository;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;

@Service("hierarchyNodeDependencyManegmanet")
public class HierarchyNodeDependencyManegmanetImpl implements HierarchyNodeDependencyManegmanet {

	@Autowired
	private HierarchyNodeDependencyRepository hierarchyNodeDependencyRepository;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	private HierarchyNodeDependencyConnector hierarchyNodeDependencyConnector;

	@Autowired
	private JobRepository jobRepository;

	private DependencyType dependencyType = DependencyType.FinishToStart;

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, SchedulingException.class, HierarchyNodeDependencySchedulingException.class,
			InvalidDataException.class })
	public void addPredecessorHierarchyNodeDependency(long hierarchyNodeId, long[] predecessorNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "HierarchyNodeNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (Long dependencyNodeId : predecessorNodeIds) {
			if (dependencyNodeId == hierarchyNodeId) {
				throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "Same node");
			}
			ids.add(dependencyNodeId);
		}
		List<HierarchyNodeDependency> exConn = hierarchyNodeDependencyRepository.findByDependencyConnectionSuccessorNodeIdAndDependencyConnectionPredecessorNodeIn(hierarchyNodeId,
				ids);
		if (!exConn.isEmpty()) {
			throw new DependencyFailureException(MessageKeys.ALREADY_CONNECTED, "Connection already exist");
		}
		exConn = hierarchyNodeDependencyRepository.findByDependencyConnectionPredecessorNodeIdAndDependencyConnectionSuccessorNodeIn(hierarchyNodeId, ids);
		if (!exConn.isEmpty()) {
			throw new DependencyFailureException(MessageKeys.ALREADY_CONNECTED, "Connection already exist");
		}
		Iterable<HierarchyNode> dependencyNodes = hierarchyNodeRepository.findAll(ids);
		List<HierarchyNodeDependency> hierarchyNodeDependencies = new ArrayList<HierarchyNodeDependency>();
		for (HierarchyNode dependencyNode : dependencyNodes) {
			HierarchyNodeDependency hierarchyNodeDependency = new HierarchyNodeDependency(dependencyNode, hierarchyNode);

			Job connectionJob = new Job(dependencyNode.getId() + ":" + hierarchyNode.getId(), "");
			connectionJob.setDuration(new PersonDays(0));
			SortedSet<ScheduledStage> scheduledStages = new TreeSet<ScheduledStage>();
			scheduledStages.add(new ScheduledStage(0));
			scheduledStages.add(new ScheduledStage(100));
			connectionJob.getWorkSchedule().setScheduledStages(scheduledStages);
			connectionJob.setNodeType(NodeType.internal);
			jobRepository.save(connectionJob);

			hierarchyNodeDependency.setConnectionJob(connectionJob);
			addJobtoJobdependency(hierarchyNodeDependency);
			hierarchyNodeDependencies.add(hierarchyNodeDependency);
		}
		hierarchyNodeDependencyRepository.save(hierarchyNodeDependencies);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, SchedulingException.class, HierarchyNodeDependencySchedulingException.class,
			InvalidDataException.class })
	public void addSuccessorHierarchyNodeDependency(long hierarchyNodeId, long[] successorNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException {
		for (long dependencyNodeId : successorNodeIds) {
			addPredecessorHierarchyNodeDependency(dependencyNodeId, new long[] { hierarchyNodeId });
		}

	}

	@Override
	@Transactional(rollbackOn = { SchedulingException.class })
	public void removePredecessorHierarchyNodeDependency(long hierarchyNodeId, long[] predecessorNodeIds) throws SchedulingException, ApplicationException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "HierarchyNodeNotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (Long dependencyNodeId : predecessorNodeIds) {
			ids.add(dependencyNodeId);
		}
		List<HierarchyNodeDependency> hierarchyNodeDependencies = hierarchyNodeDependencyRepository
				.findByDependencyConnectionSuccessorNodeIdAndDependencyConnectionPredecessorNodeIn(hierarchyNodeId, ids);

		for (HierarchyNodeDependency hierarchyNodeDependency : hierarchyNodeDependencies) {

			removeJobtoJobdependency(hierarchyNodeDependency);
		}
		hierarchyNodeDependencyRepository.delete(hierarchyNodeDependencies);
	}

	@Override
	@Transactional(rollbackOn = { SchedulingException.class })
	public void removeSuccessorHierarchyNodeDependency(long hierarchyNodeId, long[] successorNodeIds) throws SchedulingException, ApplicationException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "HierarchyNodeNotFound");
		}
		for (long dependencyNodeId : successorNodeIds) {
			removePredecessorHierarchyNodeDependency(dependencyNodeId, new long[] { hierarchyNodeId });
		}
	}

	@Override
	@Transactional
	public PageItr<HierarchyNode> getPredecessorHierarchyNodeDependency(long hierarchyNodeId, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "HierarchyNodeNotFound");
		}
		PageRequest pagable = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNodeDependency> page = hierarchyNodeDependencyRepository.findByDependencyConnectionSuccessorNodeId(hierarchyNodeId, pagable);
		List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();
		for (HierarchyNodeDependency hierarchyNodeDependency : page) {
			hierarchyNodes.add(hierarchyNodeDependency.getPredecessorNode());
		}
		return new PageItr(hierarchyNodes, page.getNumber(), page.getTotalPages());
	}

	@Override
	@Transactional
	public PageItr<HierarchyNode> getSuccessorHierarchyNodeDependency(long hierarchyNodeId, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "HierarchyNodeNotFound");
		}
		PageRequest pagable = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNodeDependency> page = hierarchyNodeDependencyRepository.findByDependencyConnectionPredecessorNodeId(hierarchyNodeId, pagable);
		List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();
		for (HierarchyNodeDependency hierarchyNodeDependency : page) {
			hierarchyNodes.add(hierarchyNodeDependency.getSuccessorNode());
		}
		return new PageItr(hierarchyNodes, page.getNumber(), page.getTotalPages());
	}

	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, SchedulingException.class, HierarchyNodeDependencySchedulingException.class,
			InvalidDataException.class })
	public void addJobtoJobdependency(HierarchyNodeDependency hierarchyNodeDependency) throws DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed,
			SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException {
		hierarchyNodeDependencyConnector.addjobtoJobDependencyHierarchyNode(hierarchyNodeDependency, dependencyType);

	}

	private void removeJobtoJobdependency(HierarchyNodeDependency hierarchyNodeDependency) throws SchedulingException, ApplicationException {
		hierarchyNodeDependencyConnector.removeJobtoJobdependency(hierarchyNodeDependency, dependencyType);

	}

	@Override
	public PageItr<HierarchyNode> searchPredecessorHierarchyNodeDependency(long hierarchyNodeId, String name, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException {
		PageRequest pagable = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNodeDependency> page = hierarchyNodeDependencyRepository.findByDependencyConnectionSuccessorNodeIdAndSuccessorNodeNameLike(hierarchyNodeId, name, pagable);
		List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();
		for (HierarchyNodeDependency hierarchyNodeDependency : page) {
			hierarchyNodes.add(hierarchyNodeDependency.getSuccessorNode());
		}
		return new PageItr(hierarchyNodes, page.getNumber(), page.getTotalPages());
	}

	@Override
	public PageItr<HierarchyNode> searchSuccessorHierarchyNodeDependency(long hierarchyNodeId, String name, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException {
		PageRequest pagable = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNodeDependency> page = hierarchyNodeDependencyRepository.findByDependencyConnectionPredecessorNodeIdAndSuccessorNodeNameLike(hierarchyNodeId, name, pagable);
		List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();
		for (HierarchyNodeDependency hierarchyNodeDependency : page) {
			hierarchyNodes.add(hierarchyNodeDependency.getSuccessorNode());
		}
		return new PageItr(hierarchyNodes, page.getNumber(), page.getTotalPages());
	}

}
