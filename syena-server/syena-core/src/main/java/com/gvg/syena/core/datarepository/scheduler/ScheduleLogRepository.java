package com.gvg.syena.core.datarepository.scheduler;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;

public interface ScheduleLogRepository extends PagingAndSortingRepository<ScheduleLog, Long>{

}
