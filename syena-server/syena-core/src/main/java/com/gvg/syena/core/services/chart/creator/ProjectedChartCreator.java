package com.gvg.syena.core.services.chart.creator;

import java.util.Date;
import java.util.Set;
import java.util.SortedSet;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.api.util.Configuration;

public class ProjectedChartCreator extends ChartCreator {

	public ProjectedChartCreator() {
		super(3);
	}

	@Override
	public void prepareData(HierarchyNode hierarchyNode, Date toTime) {
		projectHierarchyNode(hierarchyNode);
	}

	public void projectHierarchyNode(HierarchyNode hierarchyNode) {
		if (hierarchyNode != null) {
			projectInJobs(hierarchyNode.getJobs());
			projectInJobGroup(hierarchyNode.getJobGroups());
			Set<HierarchyNode> children = hierarchyNode.getChildren();
			if (children != null && children.size() > 0) {
				for (HierarchyNode node : children) {
					projectHierarchyNode(node);
				}
			}
		}

	}

	public void projectInJobGroup(Set<JobGroup<Job>> jobGroups) {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				projectInJobs(jobGroup.getJobs());
				projectInJobGroup(jobGroup.getJobGroups());
			}
		}

	}

	public void projectInJobs(Set<Job> jobs) {
		for (Job job : jobs) {
			projectInJob(job);
		}

	}

	private void projectInJob(Job job) {
		if (!job.isPlanned()) {
			return;
		}

		SortedSet<ScheduledStage> scheduledstages = job.getWorkSchedule().getScheduledStages();
		ScheduledStage lastActualEnterd = scheduledstages.first();
		for (ScheduledStage scheduledStage : scheduledstages) {
			if (scheduledStage.isActualEnterd()) {
				lastActualEnterd = scheduledStage;
			}
		}

		SortedSet<ScheduledStage> headSet = scheduledstages.headSet(lastActualEnterd);
		for (ScheduledStage scheduledStage : headSet) {
			scheduledStage.setPredictedTime(scheduledStage.getActualTime());
		}

		lastActualEnterd.setPredictedTime(lastActualEnterd.getActualTime());

		SortedSet<ScheduledStage> tailSet = scheduledstages.tailSet(lastActualEnterd);

		Date planned = lastActualEnterd.getPlannedTime();
		Date actual = lastActualEnterd.getActualTime();
		if (actual == null) {
			actual = DateUtil.today();
			lastActualEnterd.setPredictedTime(actual);
		}
		// int ad = 0;
		// if (actual != null && planned != null) {
		int ad = Configuration.personDaysCPMCalendar.getPersonDays(actual, planned);
		// }

		boolean first = true;
		for (ScheduledStage scheduledStage : tailSet) {
			if (!first && scheduledStage.getPlannedTime() != null) {
				Date newDate = Configuration.personDaysCPMCalendar.getDateAfter(scheduledStage.getPlannedTime(), ad);
				scheduledStage.setPredictedTime(newDate);
			}
			first = false;
		}

	}

}

// // Date edEndDate = null;
// //
// // Duration duration =
// // job.getWorkTime().getDuration().multiple(efficiencyFactor);
// //
// // job.setDuration(duration);
// //
// // job.getPalnnedFinishTime();
// //
// SortedSet<ScheduledStage> scheduledstages =
// job.getWorkSchedule().getScheduledStages();
//
// // ScheduledStage firstScheduledStage = scheduledstages.first();
//
// // Date actualTime1 =
// // firstScheduledStage.getActionTime().getActualTime();
// // Date estimatedTime1 =
// // firstScheduledStage.getActionTime().getEstimatedTime();
// // Date actualTime2 = null;
// // Date estimatedTime2 = null;
//
// ScheduledStage lastActualEnterd = scheduledstages.first();
// for (ScheduledStage scheduledStage : scheduledstages) {
// // ActionTime actionTime = scheduledStage.getActionTime();
// if (scheduledStage.isActualEnterd()) {
// // estimatedTime2 = actionTime.getEstimatedTime();
// // actualTime2 = actionTime.getActualTime();
// lastActualEnterd = scheduledStage;
// scheduledStage.setPredictedTime(scheduledStage.getActualTime());
// }
// }
// SortedSet<ScheduledStage> tailSet =
// scheduledstages.tailSet(lastActualEnterd);
//
// Date startTimez = lastActualEnterd.getActionTime().getPlannedTime();
// for (ScheduledStage scheduledStage : tailSet) {
// ActionTime actionTime = scheduledStage.getActionTime();
// if (actionTime != null) {
// Date startTimey = actionTime.getPlannedTime();
// int kk = Configuration.personDaysCPMCalendar.getPersonDays(startTimey,
// startTimez);
// int kku = (int) (kk * efficiencyFactor);
// Date ad = Configuration.personDaysCPMCalendar.getDateAfter(startTimez, kku);
// scheduledStage.setPredictedTime(ad);
// startTimez = startTimey;
// }
// // Date predictedTime = null;
// // TODO
//
// }
//
// // Duration actualTimeDuration = job.getDuration().copy();
// // actualTimeDuration.setDayStartTime(actualTime1);
// // actualTimeDuration.setDayStartTime(actualTime2);
// //
// // Duration estimatedDuration = job.getDuration().copy();
// // estimatedDuration.setDayStartTime(estimatedTime1);
// // estimatedDuration.setDayStartTime(estimatedTime2);

