package com.gvg.syena.core.services.checklist;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.exception.CheckListNotFound;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ValidationException;
import com.gvg.syena.core.api.services.CheckListManagement;
import com.gvg.syena.core.datarepository.checklist.CheckListItemRepository;
import com.gvg.syena.core.datarepository.checklist.CheckListRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.AppUtil;

@Service("checkListManagement")
public class CheckListManagementImpl implements CheckListManagement {

	@Autowired
	private CheckListItemRepository checkListItemRepository;

	@Autowired
	private CheckListRepository checkListRepository;

	@Autowired
	private PersonRepository personRepository;

	// public void createCheckListItem(CheckListItem checkListItem) {
	// checkListItemRepository.save(checkListItem);
	//
	// }
	//
	// public void deleteCheckList(long checkListItemId) throws
	// CheckListNotFoundExeption {
	// CheckListItem checkListItem =
	// checkListItemRepository.findOne(checkListItemId);
	// if (checkListItem == null) {
	// throw new CheckListNotFoundExeption(checkListItemId,
	// "CHECKLISTITEMNOTFOUND", "checkListItem not found");
	// }
	// checkListItemRepository.delete(checkListItemId);
	//
	// }
	//
	// public void upadteCheckListItem(CheckListItem checkListItem) {
	// checkListItemRepository.save(checkListItem);
	//
	// }
	//
	// public void updateStatus(long checkListItemId, boolean isCompleted)
	// throws CheckListNotFoundExeption {
	// CheckListItem checkListItem =
	// checkListItemRepository.findOne(checkListItemId);
	// if (checkListItem == null) {
	// throw new CheckListNotFoundExeption(checkListItemId,
	// "CHECKLISTITEMNOTFOUND", "checkListItem not found");
	// }
	// checkListItem.setCompleted(isCompleted);
	// checkListItemRepository.save(checkListItem);
	// }

	@Transactional
	public void createCheckLists(List<CheckList> checkLists) throws ValidationException {
		for(CheckList checkList: checkLists){
			checkList.validate();
		}
		checkListRepository.save(checkLists);

	}

	@Transactional
	public void addCheckListItemsInCheckList(long checkListId, List<CheckListItem> checkListItems) throws CheckListNotFound {

		CheckList checkList = checkListRepository.findOne(checkListId);
		if (checkList == null) {
			throw new CheckListNotFound(checkListId, "CheckList Not Found");
		}
		checkList.addCheckListItems(checkListItems);
		checkListRepository.save(checkList);

	}

	@Transactional(rollbackOn = { CheckListNotFound.class, ValidationException.class, InvalidDataException.class })
	public CheckListItem updateCheckList(long checkListId, long checkListItemId, boolean status, Date completedDate, String completedPersonId) throws CheckListNotFound,
			PersonNotFoundException, ValidationException, InvalidDataException {
		CheckList checkList = checkListRepository.findOne(checkListId);

		if (checkList == null) {
			throw new CheckListNotFound(checkListId, "CheckList Not Found");
		}
		if (checkList.getCheckListItems() == null) {
			throw new CheckListNotFound(checkListId, "CheckList Item Not Found");
		}
		Person completedPerson = personRepository.findOne(completedPersonId);
		if (completedPerson == null) {
			throw new PersonNotFoundException(completedPersonId, "Person Not Found");
		}
		CheckListItem checkListItemSel = null;
		for (CheckListItem checkListItem : checkList.getCheckListItems()) {
			if (checkListItem.getId() == checkListItemId) {
				checkListItem.setCompletedDate(completedDate);
				checkListItem.setCompleted(status);
				checkListItem.setCompletedPerson(completedPerson);
				checkListItemSel = checkListItem;
			}
		}
		if (checkListItemSel == null) {
			throw new CheckListNotFound(checkListId, "CheckList Item Not Found");
		}

		checkListRepository.save(checkList);
		// JobItem parent = checkList.getParent();
		// if (parent instanceof Job) {
		// Job job = (Job) parent;
		// job.validateCheckList(CheckListType.initiationCheckList);
		// job.validateCheckList(CheckListType.completionCheckList);
		// }
		return checkListItemSel;
	}

	@Transactional
	public PageItr<CheckList> getAllStandardCheckList(int pageNumber, int pageSize) {
		Pageable pagable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(checkListRepository.findAllByIsStandard(true, pagable));
	}

	@Transactional
	public PageItr<CheckList> getAllCheckList(int pageNumber, int pageSize) {
		Pageable pagable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(checkListRepository.findAllByIsStandard(false, pagable));
	}

	@Transactional
	public PageItr<CheckList> findInStandardCheckList(String name, int pageNumber, int pageSize) {
		Pageable pagable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(checkListRepository.findAllByIsStandardAndNameLikeIgnoreCase(true, name, pagable));
	}

	@Transactional
	public PageItr<CheckList> findInCheckList(String name, int pageNumber, int pageSize) {
		Pageable pagable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(checkListRepository.findAllByIsStandardAndNameLikeIgnoreCase(false, name, pagable));
	}

	@Override
	public PageItr<CheckList> getCheckListByParent(long parentId, int pageNumber, int pageSize) {
		Pageable pagable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(checkListRepository.findAllByIsStandardAndParentId(false, parentId, pagable));
	}

	@Override
	public PageItr<CheckList> findInCheckListWithParent(long parentId, String name, int pageNumber, int pageSize) {
		Pageable pagable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(checkListRepository.findAllByIsStandardAndParentIdAndNameLike(false, parentId, name, pagable));
	}

}
