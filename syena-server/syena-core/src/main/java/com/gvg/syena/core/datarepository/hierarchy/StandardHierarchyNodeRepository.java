package com.gvg.syena.core.datarepository.hierarchy;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;

@SuppressWarnings("rawtypes")
public interface StandardHierarchyNodeRepository extends PagingAndSortingRepository<StandardHierarchyNode<StandardHierarchyNode>, Long> {

	@Query("SELECT s FROM StandardHierarchyNode s WHERE TYPE(s) = StandardHierarchyNode and s.hierarchyLevel = :hierarchyLevel")
	Page<StandardHierarchyNode<StandardHierarchyNode>> findByHierarchyLevel(@Param("hierarchyLevel") HierarchyLevel hierarchyLevel, Pageable pageable);

	@Query("SELECT s FROM StandardHierarchyNode s WHERE TYPE(s) = StandardHierarchyNode and s.parent.id in :parentIds")
	Page<StandardHierarchyNode<StandardHierarchyNode>> findByParentIdIn(@Param("parentIds") long[] parentIds, Pageable pageable);

	@Query("SELECT s FROM StandardHierarchyNode s WHERE TYPE(s) = StandardHierarchyNode and s.id in :ids")
	List<StandardHierarchyNode<StandardHierarchyNode>> findByIdIn(@Param("ids") Collection<Long> ids);

	@Query("SELECT s FROM StandardHierarchyNode s WHERE TYPE(s) = StandardHierarchyNode and s.parent.id = :parentId")
	Page<StandardHierarchyNode> findByParentId(@Param("parentId") long parentId, Pageable pageble);

	@Query("SELECT s FROM StandardHierarchyNode s WHERE TYPE(s) = StandardHierarchyNode and UPPER(s.name) Like UPPER(:name) and s.hierarchyLevel = :hierarchyLevel")
	Page<StandardHierarchyNode> findByNameLikeAndHierarchyLevel(@Param("name") String name, @Param("hierarchyLevel")HierarchyLevel hierarchyLevel, Pageable pageable);

	
	
}
