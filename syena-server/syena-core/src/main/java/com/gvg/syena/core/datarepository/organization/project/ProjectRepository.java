package com.gvg.syena.core.datarepository.organization.project;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.project.Project;

public interface ProjectRepository extends CrudRepository<Project, Integer> {

}
