package com.gvg.syena.core.datarepository.notification;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.notification.EscalationLog;
import com.gvg.syena.core.api.services.notification.NotificationType;

public interface EscalationLogRepository extends CrudRepository<EscalationLog, Long> {
	public Page<EscalationLog> findByEscalationIdInAndNotificationType(List<Long> escalationId,NotificationType notificationType, Pageable pageable);
	public List<EscalationLog> findByAlertSendFalse();
	@Query("SELECT count(id) from EscalationLog e where e.escalationId in :escalationIds")
	public long findCountbyEscalationIdIn(@Param("escalationIds") List<Long> escalationIds);
}
