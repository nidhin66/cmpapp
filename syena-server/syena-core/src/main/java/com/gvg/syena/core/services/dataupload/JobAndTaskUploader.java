package com.gvg.syena.core.services.dataupload;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;

public class JobAndTaskUploader {

	private XSSFSheet jobTaskSheet;

	public JobAndTaskUploader(InputStream inputStream) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		this.jobTaskSheet = wb.getSheetAt(2);
	}

	@Test
	public ArrayList<JobGroup<StandardJob>> read() {
		ArrayList<JobGroup<StandardJob>> parents = new ArrayList<JobGroup<StandardJob>>();
		int numRow = jobTaskSheet.getPhysicalNumberOfRows();
		for (int i = 0; i < numRow; i++) {
			XSSFRow row0 = jobTaskSheet.getRow(i);
			XSSFCell cell1 = row0.getCell(0);
			if (cell1 != null) {
				String value = cell1.getStringCellValue();
				if (!(value == null || "".equals(value))) {
					JobGroup<StandardJob> jobgroup = new JobGroup<StandardJob>(value);
					parents.add(jobgroup);
				}
			}
			XSSFCell cell2 = row0.getCell(1);
			if (cell2 != null) {
				String value2 = cell2.getStringCellValue();
				if (!(value2 == null || "".equals(value2))) {
					StandardJob job = new StandardJob(value2, value2);
					parents.get(parents.size() - 1).addJob(job);
				}
			}

		}
		return parents;
	}

	public static void main(String[] args) throws IOException {
		InputStream in = HierarchyNodeReaderExcel.class.getResourceAsStream("/MappedData_DataUpLoad1.xlsx");
		JobAndTaskUploader dataExcel = new JobAndTaskUploader(in);
		ArrayList<JobGroup<StandardJob>> datas = dataExcel.read();
		for (JobGroup<StandardJob> jobGroup : datas) {
			jobGroup.printAll();
		}

	}

}
