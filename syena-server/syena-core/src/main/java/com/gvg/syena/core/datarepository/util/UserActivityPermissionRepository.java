package com.gvg.syena.core.datarepository.util;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.util.UserActivityPermission;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;

public interface UserActivityPermissionRepository extends PagingAndSortingRepository<UserActivityPermission, Long> {

	List<UserActivityPermission> findByPermittedRolesOrPermittedPersonsIdAndActivityAreaAndActivityOperations(String roleId, String personId, ActivityArea activityArea,
			ActivityOperation activityOperation);

}
