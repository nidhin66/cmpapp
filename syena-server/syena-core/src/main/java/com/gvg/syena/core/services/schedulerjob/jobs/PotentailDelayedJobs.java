package com.gvg.syena.core.services.schedulerjob.jobs;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.util.ApplicationDateUtil;
import com.gvg.syena.core.datarepository.ApplicationConfigurationRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.utilities.UtilServices;
@Service("PotentailDelayedJobs")
public class PotentailDelayedJobs extends QuartzJobBean implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext = null;
	
	private static final String notcompletedJobsQuery = "SELECT t1.Job_id FROM job_scheduledstage t1, scheduledstage t2 where t2.id = t1.ScheduledStages_id and  COALESCE(t2.revisedTime,t2.estimatedTime) < :date and t2.actualTime is null group by t1.Job_id";
	private static final String notcompletedJobGroupQuery = "SELECT t1.JobGroup_id FROM jobgroup_scheduledstage t1, scheduledstage t2 where t2.id = t1.ScheduledStages_id and  COALESCE(t2.revisedTime,t2.estimatedTime) < :date and t2.actualTime is null group by t1.JobGroup_id";


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		PotentailDelayedJobs.applicationContext = applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		ApplicationConfigurationRepository applicationConfigurationRepository = applicationContext.getBean(ApplicationConfigurationRepository.class);
		ApplicationConfiguration config = applicationConfigurationRepository.findOne(ApplicationDateUtil.APPLICATION_DATE_HOUR_DEFERENCE);
		int deference = 0;
		JobRepository jobRepository = applicationContext.getBean(JobRepository.class);
		JobGroupRepository jobGroupRepository = applicationContext.getBean(JobGroupRepository.class);
		
		if(null != config){
			deference = Integer.valueOf(config.getParamValue());
		}
		Date applicationDate = ApplicationDateUtil.getApplicationDate(deference);
		EntityManager em = applicationContext.getBean(EntityManager.class);
		
		Query jobGroupQuery = em.createNativeQuery(notcompletedJobsQuery);
		jobGroupQuery.setParameter("firstDate", applicationDate,TemporalType.DATE);
		jobGroupQuery.setParameter("secondDate", UtilServices.nextDate(applicationDate,7), TemporalType.DATE);
		List<Object[]> jobGroupResultList = jobGroupQuery.getResultList();
		List<Long> jobGroupIds = new ArrayList<Long>();
		List<JobGroup> successorJobGroups = new ArrayList<>();
		if(null != jobGroupResultList){
			for(Object[] row : jobGroupResultList){
				long jobGroupId = ((BigInteger) row[0]).longValue();
				if(!jobGroupIds.contains(jobGroupId)){
					jobGroupIds.add(jobGroupId);
					JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
					getSuccessorJobGroups(successorJobGroups, jobGroup);
				}
				
			}
		}
		
		Query jobQuery = em.createNativeQuery(notcompletedJobGroupQuery);
		jobQuery.setParameter("firstDate", applicationDate,TemporalType.DATE);
		jobQuery.setParameter("secondDate", UtilServices.nextDate(applicationDate,7), TemporalType.DATE);
		List<Object[]> jobResultList = jobQuery.getResultList();
		List<Long> jobIds = new ArrayList<Long>();
		List<Job> successorJobs = new ArrayList<>();
		if(null != jobResultList){
			for(Object[] row : jobResultList){
				long jobId = ((BigInteger) row[0]).longValue();
				if(!jobIds.contains(jobId)){
					jobIds.add(jobId);
					Job job = jobRepository.findOne(jobId);
					getSuccessorJobs(successorJobs, job);
				}
				
			}
		}
		
	}
	
	private void getSuccessorJobGroups(List<JobGroup> successorJobGroups, JobGroup jobGroup){
		DependencyJobs successorDependency = jobGroup.getSuccessorDependency();
		if(null != successorDependency){
			if(null != successorDependency.getFinishToStart()){
				Set<JobGroup<Job>> jobs = successorDependency.getFinishToStart().getJobGroups();
				if(null != jobs){
					for (JobGroup job : jobs) {
						if (job.getNodeType() == NodeType.instance) {
							successorJobGroups.add(job);
							getSuccessorJobGroups(successorJobGroups, jobGroup);
						}
					}
				}
			}
		}
	}
	
	
	
	private void getSuccessorJobs(List<Job> successorJobs, Job job){
		DependencyJobs successorDependency = job.getPredecessorDependency();
		if(null != successorDependency){
			if(null != successorDependency.getFinishToStart()){
				Set<Job> jobs = successorDependency.getFinishToStart().getJobs();
				if(null != jobs){
					for (Job successorJob : jobs) {
						if (successorJob.getNodeType() == NodeType.instance) {
							successorJobs.add(successorJob);
							getSuccessorJobs(successorJobs, successorJob);
						}
					}
				}
			}
		}
	}

}
