package com.gvg.syena.core.services.criticalpath;

import java.util.Comparator;
import java.util.Date;

public class CriticalPathJobDataComparitor implements Comparator<JobData> {

	@Override
	public int compare(JobData one, JobData two) {
		Date palnnedFinishTimeOne = one.getWorkTime().getPlannedFinishTime();
		Date palnnedFinishTimeTwo = one.getWorkTime().getPlannedFinishTime();
		return palnnedFinishTimeTwo.compareTo(palnnedFinishTimeOne);
	}
}
