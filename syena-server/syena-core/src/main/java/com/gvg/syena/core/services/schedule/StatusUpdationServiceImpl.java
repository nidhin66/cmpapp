package com.gvg.syena.core.services.schedule;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.job.status.ComputationFactor;
import com.gvg.syena.core.api.services.StatusUpdationService;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;

@Service("statusUpdationService")
// @Service("checkListManagement")
public class StatusUpdationServiceImpl implements StatusUpdationService {

	private ComputationFactor statusComputationFactor = ComputationFactor.Least;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	public StatusUpdationServiceImpl() {
	}

	@Transactional
	public void computeUpwardStatus(HierarchyNode node, WorkStatus workStatus, WorkTime workTime) {

		Set<HierarchyNode> children = node.getChildren();
		node.getTotalEffert().setNumberOfDays(0);
		node.getWorkTime().getDuration().setNumberOfDays(0);
		if (children != null) {
			for (HierarchyNode child : children) {
				workStatus = statusComputationFactor.compute(workStatus, child.getWorkStatus());
				node.getTotalEffert().setNumberOfDays(node.getTotalEffert().getNumberofDays() + child.getTotalEffert().getNumberofDays());
				node.getWorkTime().upwardMerge(node.getWorkTime());
			}
		}

		JobGroup<Job> jobGroup = node.getDefaultJobGroup();
		if (jobGroup != null && (jobGroup.getJobs().size() > 0 || jobGroup.getJobGroups().size() > 0)) {
			workStatus = statusComputationFactor.compute(workStatus, jobGroup.getWorkStatus());
			node.getTotalEffert().setNumberOfDays(jobGroup.getTotalEffert().getNumberofDays() + jobGroup.getTotalEffert().getNumberofDays());
			node.getWorkTime().upwardMerge(jobGroup.getWorkTime());
		}

		node.setWorkStatus(workStatus);
		// node = hierarchyNodeRepository.save(node);

		StandardHierarchyNode parent = node.getParent();
		if (parent != null) {
			HierarchyNode parentx = hierarchyNodeRepository.findOne(parent.getId());
			computeUpwardStatus(parentx, workStatus, workTime);
		}
	}

	@Transactional
	public void computeUpwardStatus(JobGroup<Job> jobGroup, WorkStatus workStatus, WorkTime workTime) {

		jobGroup.getTotalEffert().setNumberOfDays(0);
		jobGroup.getWorkTime().getDuration().setNumberOfDays(0);

		Set<Job> jobs = jobGroup.getJobs();
		if (jobs != null) {
			for (Job job : jobs) {
				if (jobs.size() == 1 || job.isApplicableForComplete()) {
					workStatus = statusComputationFactor.compute(workStatus, job.getWorkStatus());
				}
			}
		}

		Set<JobGroup<Job>> jobGroups = jobGroup.getJobGroups();

		if (jobGroups != null) {
			for (JobGroup<Job> child : jobGroups) {
				if (jobGroups.size() == 1 || child.isApplicableForComplete()) {
					workStatus = statusComputationFactor.compute(workStatus, child.getWorkStatus());
					jobGroup.getTotalEffert().setNumberOfDays(child.getTotalEffert().getNumberofDays() + child.getTotalEffert().getNumberofDays());
					jobGroup.getWorkTime().upwardMerge(child.getWorkTime());
				}
			}
		}

		jobGroup.setWorkStatus(workStatus);
		// jobGroupRepository.save(jobGroup);

		JobGroup<Job> parent = jobGroup.getParent();
		if (parent != null) {
			computeUpwardStatus(parent, workStatus, workTime);
		} else {
			HierarchyNode node = jobGroup.getConnectedNode();
			if (node != null) {
				computeUpwardStatus(node, workStatus, workTime);
			}
		}

	}

	@Override
	@Transactional
	public void computeUpwardStatus(Job job) {
		WorkStatus workStatus = job.computeWorkStatus();
		WorkTime workTime = job.getWorkTime();
		// jobRepository.save(job);
		JobGroup parent = job.getParentJobGroup();
		if (parent != null) {
			if (parent != null) {
				computeUpwardStatus(parent, workStatus, workTime);
			}
		}

	}

	@Override
	public void computeUpwardStatus(JobGroup<Job> jobGroup) {
//		computeUpwardStatus(jobGroup, jobGroup.getWorkStatus(), jobGroup.getWorkTime());

	}

	@Override
	public void computeUpwardStatus(HierarchyNode node) {
//		computeUpwardStatus(node, node.getWorkStatus(), node.getWorkTime());

	}
}
