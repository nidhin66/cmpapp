package com.gvg.syena.core.services.schedule;

import java.util.Date;
import java.util.Set;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.PersonDays;

public class WorkTimeComputer {

	public void computeWorkTime(HierarchyNode hierarchyNode) {
		WorkTime workTime = new WorkTime(new Date(), new PersonDays(0));
		computeWorkTime(hierarchyNode, workTime);
		hierarchyNode.setWorkTime(workTime);
	}

	public void computeWorkTime(HierarchyNode hierarchyNode, WorkTime workTime) {
		computeWorkTime(hierarchyNode.getDefaultJobGroup(), workTime);
		Set<HierarchyNode> children = hierarchyNode.getChildren();
		for (HierarchyNode child : children) {
			computeWorkTime(child, workTime);
		}
	}

	private void computeWorkTime(JobGroup<Job> jobGroup, WorkTime workTime) {
		Set<Job> jobs = jobGroup.getJobs();
		computeWorkTime(jobs, workTime);
		Set<JobGroup<Job>> jobGroups = jobGroup.getJobGroups();
		for (JobGroup<Job> group : jobGroups) {
			computeWorkTime(group, workTime);
		}
	}

	private void computeWorkTime(Set<Job> jobs, WorkTime workTime) {
		for (Job job : jobs) {
			workTime.add(job.getWorkTime());
		}
	}

}
