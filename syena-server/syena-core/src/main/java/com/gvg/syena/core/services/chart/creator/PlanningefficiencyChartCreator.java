package com.gvg.syena.core.services.chart.creator;

import com.gvg.syena.core.services.chart.creator.efficiencycalculator.EfficiencyCalculator;
import com.gvg.syena.core.services.chart.creator.efficiencycalculator.PlanningEfficiencyCalculator;

public class PlanningefficiencyChartCreator extends PredictedChartCreator {

	@Override
	EfficiencyCalculator getEfficiencyCalculator() {
		return new PlanningEfficiencyCalculator(getWorkPercentageFinder());
	}

}
