package com.gvg.syena.core.datarepository.hierarchy;

import java.util.Collection;
import java.util.List;

import org.hibernate.annotations.BatchSize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.UsageStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

public interface HierarchyNodeRepository extends PagingAndSortingRepository<HierarchyNode, Long> {

	@BatchSize(size = 100)
	Page<HierarchyNode> findByParentIdAndUseStatus(@Param("parentId") long parentId, @Param("useStatus") UsageStatus useStatus, Pageable pageable);

	Page<HierarchyNode> findByUseStatus(UsageStatus useStatus, Pageable pageable);

	List<HierarchyNode> findByHierarchyLevel(HierarchyLevel hierarchyLevel);

	Page<HierarchyNode> findByIdIn(Collection<Long> ids, Pageable pageable);

	Page<HierarchyNode> findByHierarchyNodeNameLikeIgnoreCaseAndUseStatus(String hierarchyNodeName, UsageStatus useStatus, Pageable pageable);

	// @BatchSize(size = 100)
	// @Query("select h from HierarchyNode h where parent in (select hn from HierarchyNode hn where id in (:parentIds)) and useStatus=:useStatus")
	// @Query(nativeQuery= true, value =
	// "SELECT * FROM hierarchynode where parent_id in (:parentIds) and useStatus = :useStatus")
	Page<HierarchyNode> findByParentIdInAndUseStatus(@Param("parentIds") long[] parentIds, @Param("useStatus") UsageStatus useStatus, Pageable pageable);

	Page<HierarchyNode> findByHierarchyLevel(HierarchyLevel hierarchyLevel, Pageable pageable);

	Page<HierarchyNode> findByHierarchyNodeNameLikeIgnoreCaseAndUseStatusAndHierarchyLevel(String hierarchyNodename, UsageStatus useStatus, HierarchyLevel hierarchyLevel,
			Pageable pageable);

	Page<HierarchyNode> findByHierarchyNodeNameLikeIgnoreCaseAndUseStatusAndParentId(String hierarchyNodename, UsageStatus useStatus, long parentId, Pageable pageable);

	List<HierarchyNode> findByIdIn(Collection<Long> ids);

	Page<HierarchyNode> findByHierarchyNodeNameLike(String hierarchyNodename, Pageable pageable);

	Page<HierarchyNode> findByHierarchyNodeNameOrDescriptionAndUseStatus(String hierarchyNodeName, String description, UsageStatus useStatus, Pageable pageable);

	List<HierarchyNode> findByParentIdAndStandardHierarchyNodeIdIn(long parentId, long[] standardHierarchyNodeIds);

	List<HierarchyNode> findByParentIdAndHierarchyNodeName(long parentNodeId, String hierarchyNodeName);

	@Query("select j.connectedNode from Job j")
	List<HierarchyNode> findJobConnectedHierarchyNodes();

	@Query("select jg.connectedNode from JobGroup jg where jg.nodeType = :nodeType")
	List<HierarchyNode> findJobGroupConnectedHierarchyNodes(@Param("nodeType") NodeType nodeType);

	List<HierarchyNode> findByIdNotIn(Collection<Long> ids);

	// SELECT * FROM hierarchynode where id in (select parent_id from
	// hierarchynode where id in (213, 214, 215));
	@Query("select h from HierarchyNode h where h.id in(select h.parent.id from h  where h.id in (:ids)) ")
	Page<HierarchyNode> findParentByIdIn(@Param("ids") List<Long> ids, Pageable pageble);

}
