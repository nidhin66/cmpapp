package com.gvg.syena.core.services.chart.creator.efficiencycalculator;

import java.util.Date;
import java.util.Set;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkPercentageType;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.services.chart.creator.WorkPercentageFinder;

public class WorkEfficiencyCalcuator implements EfficiencyCalculator {

	private WorkPercentageFinder workPercentageFinder;

	private Duration actualTimeSpent;

	private Duration estimatedTimeSpent;

	public WorkEfficiencyCalcuator(WorkPercentageFinder workPercentageFinder) {
		this.workPercentageFinder = workPercentageFinder;
	}

	@Override
	public double calculateEfficiencyFactor(HierarchyNode hierarchyNode, Date ondate) {
		double workPercentage = workPercentageFinder.findWorkPercentage(hierarchyNode, ondate, WorkPercentageType.Actual);
		calculateActualTimeSpentAndEstimatedTimeSpent(hierarchyNode, workPercentage);
		double workEfficiency = actualTimeSpent.divide(estimatedTimeSpent);
		return workEfficiency;
	}

	private void calculateActualTimeSpentAndEstimatedTimeSpent(HierarchyNode hierarchyNode, double workPercentage) {
		if (hierarchyNode != null) {
			calculateActualTimeSpentAndEstimatedTimeSpentJob(hierarchyNode.getJobs(), workPercentage);
			calculateActualTimeSpentAndEstimatedTimeSpentJobGroup(hierarchyNode.getJobGroups(), workPercentage);
			Set<HierarchyNode> children = hierarchyNode.getChildren();
			if (children != null && children.size() > 0) {
				for (HierarchyNode node : children) {
					calculateActualTimeSpentAndEstimatedTimeSpent(node, workPercentage);
				}
			}
		}

	}

	private void calculateActualTimeSpentAndEstimatedTimeSpentJobGroup(Set<JobGroup<Job>> jobGroups, double workPercentage) {
		if (jobGroups != null) {
			for (JobGroup<Job> jobGroup : jobGroups) {
				calculateActualTimeSpentAndEstimatedTimeSpentJob(jobGroup.getJobs(), workPercentage);
				calculateActualTimeSpentAndEstimatedTimeSpentJobGroup(jobGroup.getJobGroups(), workPercentage);
			}
		}
	}

	private void calculateActualTimeSpentAndEstimatedTimeSpentJob(Set<Job> jobs, double workPercentage) {
		if (jobs != null) {
			for (Job job : jobs) {
				Duration actualTime = job.getActualTimeSpent(workPercentage);
				if (actualTimeSpent == null) {
					actualTimeSpent = actualTime;
				} else {
					actualTimeSpent.add(actualTime);
				}
				Duration estimatedTime = job.getEstimatedTimeSpent(workPercentage);
				if (estimatedTimeSpent == null) {
					estimatedTimeSpent = estimatedTime;
				} else {
					estimatedTimeSpent.add(estimatedTime);
				}
			}
		}
	}

	// @Override
	// public Float[] getGraphPoint(HierarchyNode hierarchyNode) {
	// Calendar calendar = Calendar.getInstance();
	// Date now = calendar.getTime();
	// double efficiency = calculateEfficiencyFactor(hierarchyNode, now);
	// applyEfficiency(hierarchyNode);
	// Date graphPoint = null;
	// return super.getGraphPoint(hierarchyNode, graphPoint);
	// }
	//
	// @Override
	// public List<ChartPoint<Date>> getGraphPoint(HierarchyNode hierarchyNode)
	// throws NotScheduledException {
	// WorkPercentageType[] workPercentageTypes = { WorkPercentageType.Predicted
	// };
	// List<ChartPoint<Date>> chartPoints = new ArrayList<ChartPoint<Date>>();
	// Date fromTime = UtilServices.getMinFromTime(hierarchyNode);
	// Date toTime = UtilServices.getMaxToTime(hierarchyNode);
	// GraphDateIterator graphPoints = new GraphDateIterator(fromTime, toTime,
	// 7);
	// while (graphPoints.hasNext()) {
	// Date graphPoint = graphPoints.next();
	// super.reset(workPercentageTypes);
	// Float[] values = super.getGraphPoint(hierarchyNode, graphPoint);
	// ChartPoint<Date> chartPoint = new ChartPoint<Date>(graphPoint, values);
	// chartPoints.add(chartPoint);
	// }
	// return chartPoints;
	// }
	//
	// @Override
	// public void calculatePerdicatedDate(HierarchyNode hierarchyNode, double
	// efficiencyFactor) {
	// PredictionCalculator predictionCalculator = new PredictionCalculator();
	// predictionCalculator.predict(hierarchyNode, efficiencyFactor);
	// }

}
