package com.gvg.syena.core.services.schedulerjob.jobs;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.services.NotificationService;
import com.gvg.syena.core.api.services.notification.NotificationRecord;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;
import com.gvg.syena.core.api.services.notification.PlaceHolderTableData;
import com.gvg.syena.core.api.services.notification.PlaceHolderType;
import com.gvg.syena.core.api.util.ApplicationDateUtil;
import com.gvg.syena.core.datarepository.ApplicationConfigurationRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.datarepository.notification.NotificationConfigRepository;
import com.gvg.syena.core.services.notification.NotificationServiceImpl;
import com.gvg.syena.core.utilities.UtilServices;

@Service("weeklyPlannedActivities")
public class WeeklyPlannedActivities extends QuartzJobBean implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext = null;
	
	private static final String weeklyPlannedJobs = "SELECT t1.Job_id,COALESCE(t2.revisedTime,t2.estimatedTime) as JobDate,t2.percentageOfWork FROM job_scheduledstage t1, scheduledstage t2 where t2.id = t1.ScheduledStages_id and  COALESCE(t2.revisedTime,t2.estimatedTime) between :firstDate and :secondDate";
	private static final String weeklyPlannedJobGroups = "SELECT t1.JobGroup_id,COALESCE(t2.revisedTime,t2.estimatedTime) as JobDate,t2.percentageOfWork FROM jobgroup_scheduledstage t1, scheduledstage t2 where t2.id = t1.ScheduledStages_id and  COALESCE(t2.revisedTime,t2.estimatedTime) between :firstDate and :secondDate";

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		WeeklyPlannedActivities.applicationContext = applicationContext;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		NotificationConfigRepository notificationConfigRepository = applicationContext.getBean(NotificationConfigRepository.class);	
		NotificationConfig notificationConfig = notificationConfigRepository.findOne("WEEKLY_JOBS");
		ApplicationConfigurationRepository applicationConfigurationRepository = applicationContext.getBean(ApplicationConfigurationRepository.class);
		ApplicationConfiguration config = applicationConfigurationRepository.findOne(ApplicationDateUtil.APPLICATION_DATE_HOUR_DEFERENCE);
		int deference = 0;
		JobRepository jobRepository = applicationContext.getBean(JobRepository.class);
		JobGroupRepository jobGroupRepository = applicationContext.getBean(JobGroupRepository.class);
		
		if(null != config){
			deference = Integer.valueOf(config.getParamValue());
		}
		Date applicationDate = ApplicationDateUtil.getApplicationDate(deference);
		EntityManager em = applicationContext.getBean(EntityManager.class);
		List<JobGroup> jobGroups = new ArrayList<>();
		Query jobGroupQuery = em.createNativeQuery(weeklyPlannedJobGroups);
		jobGroupQuery.setParameter("firstDate", applicationDate,TemporalType.DATE);
		jobGroupQuery.setParameter("secondDate", UtilServices.nextDate(applicationDate,7), TemporalType.DATE);
		List<Object[]> jobGroupResultList = jobGroupQuery.getResultList();
		if(null != jobGroupResultList){
			for(Object[] row : jobGroupResultList){
				long jobGroupId = ((BigInteger) row[0]).longValue();
				JobGroup jobGroup = jobGroupRepository.findOne(jobGroupId);
				if (jobGroup.getNodeType() == NodeType.instance) {
					jobGroups.add(jobGroup);
				}				
			}
		}
		
		
		List<Job> jobs = new ArrayList<>();
		Query jobQuery = em.createNativeQuery(weeklyPlannedJobs);
		jobQuery.setParameter("firstDate", applicationDate,TemporalType.DATE);
		jobQuery.setParameter("secondDate", UtilServices.nextDate(applicationDate,7), TemporalType.DATE);
		List<Object[]> jobResultList = jobQuery.getResultList();
		if(null != jobResultList){
			for(Object[] row : jobResultList){
				long jobId = ((BigInteger) row[0]).longValue();
				Job job = jobRepository.findOne(jobId);
				if (job.getNodeType() == NodeType.instance) {
					jobs.add(job);
				}
				
			}
		}
		
		boolean dataFound = false;
		List<PlaceHolderData> placeHolders = new ArrayList<>();
		PlaceHolderData jobGroupsData = null;
		PlaceHolderData jobData = null;
		if(null != jobs && jobs.size() > 0){
			dataFound = true;
			jobData =  new PlaceHolderData("JOB_DATA", PlaceHolderType.TABLE);
			PlaceHolderTableData tableData = new PlaceHolderTableData(2);
			String[] headers = new String[2];
			headers[0] = "Name";
			headers[1] = "Description";		
			tableData.setHeader(headers);
			for(Job job : jobs){
				if(null == job.getConnectedNode()){
					String[] row = new String[2];
					row[0] = job.getName();
					row[1] = job.getDescription();
					tableData.addRow(row);	
				}							
			}
			jobData.setValue(tableData);
			placeHolders.add(jobData);
		}
		
		if(null != jobGroups && jobGroups.size() > 0){
			dataFound = true;
			jobGroupsData =  new PlaceHolderData("JOB_GROUP_DATA", PlaceHolderType.TABLE);
			PlaceHolderTableData tableData = new PlaceHolderTableData(2);
			String[] headers = new String[2];
			headers[0] = "Name";
			headers[1] = "Description";		
			tableData.setHeader(headers);
			for(JobGroup jobGroup : jobGroups){
				String[] row = new String[2];
				row[0] = jobGroup.getName();
				row[1] = jobGroup.getDescription();
				tableData.addRow(row);
			}
			jobGroupsData.setValue(tableData);
			placeHolders.add(jobGroupsData);
		}
		
		if(dataFound){
			List<NotificationRecord> records = new ArrayList<>();
			NotificationRecord record = new NotificationRecord();
			record.setRecordId(notificationConfig.getConfigId());
			record.setSubject(notificationConfig.getName());
			record.setPlaceHolderDatas(placeHolders);
			records.add(record);
			NotificationService notificationService  = applicationContext.getBean(NotificationServiceImpl.class);
			notificationService.notify(notificationConfig,records);
		}
		
	}

}
