package com.gvg.syena.core.services.chart.creator;

import java.util.Date;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.services.chart.creator.efficiencycalculator.EfficiencyCalculator;

public abstract class PredictedChartCreator extends ChartCreator {

	private PredictionCalculator predictionCalculator;

	public PredictedChartCreator() {
		super(2);
		this.predictionCalculator = new PredictionCalculator();
	}

	@Override
	public void prepareData(HierarchyNode hierarchyNode, Date date) {
		double efficiencyFactor = getEfficiencyCalculator().calculateEfficiencyFactor(hierarchyNode, date);
		this.predictionCalculator.predict(hierarchyNode, efficiencyFactor);

	}

	abstract EfficiencyCalculator getEfficiencyCalculator();

	protected WorkPercentageFinder getWorkPercentageFinder() {
		return new WorkPercentageFinder();
	}

}
