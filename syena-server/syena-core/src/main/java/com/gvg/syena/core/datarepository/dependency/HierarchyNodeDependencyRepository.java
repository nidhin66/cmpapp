package com.gvg.syena.core.datarepository.dependency;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.dependency.DependencyConnection;
import com.gvg.syena.core.api.entity.dependency.HierarchyNodeDependency;

public interface HierarchyNodeDependencyRepository extends PagingAndSortingRepository<HierarchyNodeDependency, DependencyConnection> {

	@Query("SELECT s FROM HierarchyNodeDependency s WHERE s.dependencyConnection.predecessorNode.id = :hierarchyNodeId and s.dependencyConnection.successorNode.id in ( :dependencyNodeIds) ")
	List<HierarchyNodeDependency> findByDependencyConnectionPredecessorNodeIdAndDependencyConnectionSuccessorNodeIn(@Param("hierarchyNodeId") long hierarchyNodeId,
			@Param("dependencyNodeIds") List<Long> dependencyNodeIds);

	@Query("SELECT s FROM HierarchyNodeDependency s WHERE s.dependencyConnection.predecessorNode.id = :hierarchyNodeId")
	Page<HierarchyNodeDependency> findByDependencyConnectionPredecessorNodeId(@Param("hierarchyNodeId") long hierarchyNodeId, Pageable pagable);

	@Query("SELECT s FROM HierarchyNodeDependency s WHERE s.dependencyConnection.successorNode.id = :hierarchyNodeId")
	Page<HierarchyNodeDependency> findByDependencyConnectionSuccessorNodeId(@Param("hierarchyNodeId") long hierarchyNodeId, Pageable pagable);

	@Query("SELECT s FROM HierarchyNodeDependency s WHERE s.dependencyConnection.successorNode.id = :hierarchyNodeId and s.dependencyConnection.predecessorNode.id in ( :dependencyNodeIds) ")
	List<HierarchyNodeDependency> findByDependencyConnectionSuccessorNodeIdAndDependencyConnectionPredecessorNodeIn(@Param("hierarchyNodeId") long hierarchyNodeId,
			@Param("dependencyNodeIds") List<Long> ids);

	@Query("SELECT s FROM HierarchyNodeDependency s WHERE s.dependencyConnection.successorNode.id = :hierarchyNodeId and s.dependencyConnection.predecessorNode.name like :name ")
	Page<HierarchyNodeDependency> findByDependencyConnectionSuccessorNodeIdAndSuccessorNodeNameLike(@Param("hierarchyNodeId") long hierarchyNodeId, @Param("name") String name,
			Pageable pagable);

	@Query("SELECT s FROM HierarchyNodeDependency s WHERE s.dependencyConnection.predecessorNode.id = :hierarchyNodeId and s.dependencyConnection.successorNode.name like :name ")
	Page<HierarchyNodeDependency> findByDependencyConnectionPredecessorNodeIdAndSuccessorNodeNameLike(@Param("hierarchyNodeId") long hierarchyNodeId, @Param("name") String name,
			Pageable pagable);
}
