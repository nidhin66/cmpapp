package com.gvg.syena.core.datarepository.scheduler;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.scheduler.SchedulerJob;

public interface SchedulerJobRepository extends CrudRepository<SchedulerJob, Long>{
	
	public List<SchedulerJob> findByEnabledTrue();

}
