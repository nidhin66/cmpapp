package com.gvg.syena.core.services.organization;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;
import com.gvg.syena.core.api.exception.LoginFailedException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.services.UserManagemnet;
import com.gvg.syena.core.datarepository.organization.RoleRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.AppUtil;
import com.gvg.syena.core.utilities.UtilServices;

@Service("organizationManagemnet")
public class OrganizationManagemnetImpl implements UserManagemnet {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private RoleRepository roleRepository;

//	@Autowired
//	private ApplicationService applicationService;

	public void createPerson(Person person) {
		personRepository.save(person);

	}

	@Override
	public PageItr<Person> searchPerson(String personName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(personRepository.findByIdLike(personName, pageable));

	}

	@Override
	public void loginUser(String userId, String inputPassword) throws PersonNotFoundException, LoginFailedException {
		if (null != inputPassword && !"".equalsIgnoreCase(inputPassword) && inputPassword.length() > 0) {
			inputPassword = inputPassword.substring(1, inputPassword.length() - 1);
		}
		Person person = personRepository.findOne(userId);
		if (person == null) {
			throw new PersonNotFoundException(person.getId(), "PersonNotFound");
		} else {
			String encoded = UtilServices.getHashPassword(inputPassword);
			if (!UtilServices.verifyHashPassword(inputPassword, person.getPassword())) {
				throw new LoginFailedException(userId, "Login Failed");
			}
		}
	

	}

	@Override
	@Transactional
	public List<Person> getPersonRoleIds(List<String> ids) {
		List<Role> roles = roleRepository.findByRoleIdIn(ids);
		List<Person> persons = new ArrayList<>();
		for (Role role : roles) {
			List<Person> users = role.getUsers();
			if (null != users) {
				persons.addAll(users);
			}
		}
		return persons;
	}

	@Override
	public Person getPerson(String userId) throws PersonNotFoundException {
		Person person = personRepository.findOne(userId);
		if (person == null) {
			throw new PersonNotFoundException(userId, "PersonNotFound");
		} else {
			return person;
		}

	}
}
