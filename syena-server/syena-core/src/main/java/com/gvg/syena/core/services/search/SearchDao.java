package com.gvg.syena.core.services.search;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.SearchException;
import com.gvg.syena.core.api.services.serach.SearchCriteria;

@Component("searchDao")
public class SearchDao {

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	public List search(SearchCriteria searchCriteria, int startPosition, int pageSize) throws SearchException {
		EntityManager em = null;
		try {
			em = entityManagerFactory.createEntityManager();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			Class clasz = searchCriteria.getEntityClass();
			ParameterExpression<Integer> param1 = criteriaBuilder.parameter(Integer.class);
			CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(clasz);
			Root c = criteriaQuery.from(clasz);
			Expression restriction = criteriaBuilder.equal(c.get("id"), 0);
			criteriaQuery.select(c).where(restriction);
			TypedQuery typedQuery = em.createQuery(criteriaQuery);
			// Parameter param =
			// criteriaBuilder.parameter(StandardHierarchyNode.class, "id");
			// typedQuery.setParameter(param, 0);
			typedQuery.setFirstResult(startPosition);
			typedQuery.setMaxResults(pageSize);
			return typedQuery.getResultList();
		} catch (Exception e) {
			throw new SearchException(MessageKeys.SEARCH_FAILED, "search_failed", e);
		} finally {
			if (em != null) {
				em.close();
			}
		}

	}
}
