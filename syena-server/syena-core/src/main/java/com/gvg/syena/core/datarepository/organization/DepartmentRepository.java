package com.gvg.syena.core.datarepository.organization;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.organization.Department;

public interface DepartmentRepository extends PagingAndSortingRepository<Department, Integer>{

}
