package com.gvg.syena.core.datarepository.organization.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.organization.Person;

public interface PersonRepository extends PagingAndSortingRepository<Person, String> {

	Page<Person> findByIdLike(String id, Pageable pageable);

}
