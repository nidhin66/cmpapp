package com.gvg.syena.core.datarepository.util;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.util.Message;

public interface MessageRepository extends CrudRepository<Message, Serializable> {

}
