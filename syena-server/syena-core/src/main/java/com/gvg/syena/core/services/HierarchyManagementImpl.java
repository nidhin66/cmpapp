package com.gvg.syena.core.services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.UsageStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.project.Project;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.DocIOException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.ImageIOException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.RemovalBarrierException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.services.HierarchyDoc;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.HierarchyNodeDependencyManegmanet;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.api.services.serach.HierarchyNodeProperties;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyLevelRepository;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.hierarchy.StandardHierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.datarepository.organization.project.ProjectRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.utilities.AppUtil;
import com.gvg.syena.core.utilities.UtilServices;

@Service("hierarchyManagement")
public class HierarchyManagementImpl implements HierarchyManagement {

	private static final String FilePath = ".";

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private HierarchyNodeRepository hierarchyRepository;

	@Autowired
	private HierarchyLevelRepository hierarchyLevelRepository;

	@Autowired
	private StandardHierarchyNodeRepository standardHierarchyNodeRepository;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private JobManagement jobManagement;

	@Autowired
	private HierarchyNodeDependencyManegmanet hierarchyNodeDependencyManegmanet;

	@Transactional
	public void createHierarchyLevel(HierarchyLevel hierarchyLevel) {
		hierarchyLevelRepository.save(hierarchyLevel);
	}

	@Transactional
	public HierarchyLevel getHierarchyLevels(String levelName)
			throws HierarchyLevelNotFoundException, ServiceLevelValidationException {
		if (levelName == null) {
			throw new ServiceLevelValidationException(MessageKeys.LEVEL_NAME_NOT_NULL, "level Name can not be null.");
		}
		HierarchyLevel hierarchyLevel = hierarchyLevelRepository.findByLevelName(levelName);

		if (hierarchyLevel == null) {
			throw new HierarchyLevelNotFoundException(levelName, "HierarchyLevelNotFound");
		} else {
			return hierarchyLevel;
		}
	}

	public PageItr<StandardHierarchyNode<StandardHierarchyNode>> getAllStandardHierarchyNodes(String hierarchyLevelName,
			int pageNumber, int pageSize) throws HierarchyLevelNotFoundException {
		PageRequest pageable = new PageRequest(pageNumber, pageSize);
		HierarchyLevel level = hierarchyLevelRepository.findOne(hierarchyLevelName);
		if (level == null) {
			throw new HierarchyLevelNotFoundException(hierarchyLevelName, "Hierarchy Level Not Found");
		}
		return AppUtil.convert(standardHierarchyNodeRepository.findByHierarchyLevel(level, pageable));

	}

	public PageItr<HierarchyNode> getAllHierarchyNodes(String hierarchyLevelName, int pageNumber, int pageSize)
			throws HierarchyLevelNotFoundException {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		HierarchyLevel hierarchyLevel = hierarchyLevelRepository.findOne(hierarchyLevelName);
		if (hierarchyLevel == null) {
			throw new HierarchyLevelNotFoundException(hierarchyLevelName, "Hierarchy Level Not Found");
		}
		return AppUtil.convert(hierarchyRepository.findByHierarchyLevel(hierarchyLevel, pageable));
	}

	@Transactional
	public PageItr<HierarchyNode> getLinkedHierarchyNodes(long parentId, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		UsageStatus useStatus = UsageStatus.ACTIVE;
		return AppUtil.convert(hierarchyRepository.findByParentIdAndUseStatus(parentId, useStatus, pageable));
	}

	@Transactional(rollbackOn = { HierarchyNodeNotFoundException.class })
	public void updateHierarchyNode(List<HierarchyNode> hierarchyNodes) throws HierarchyNodeNotFoundException {
		for (HierarchyNode hierarchyNode : hierarchyNodes) {
			HierarchyNode exHierarchyNode = hierarchyRepository.findOne(hierarchyNode.getId());
			if (exHierarchyNode == null) {
				throw new HierarchyNodeNotFoundException(hierarchyNode.getId(), "HierarchyNodeNotFound");
			}
			updateHierarchyNode(hierarchyNode, exHierarchyNode);
			hierarchyRepository.save(exHierarchyNode);
		}
	}

	private void updateHierarchyNode(HierarchyNode hierarchyNode, HierarchyNode exHierarchyNode) {
		exHierarchyNode.setDescription(hierarchyNode.getDescription());
		exHierarchyNode.setOwner(hierarchyNode.getOwner());
		exHierarchyNode.setWeightage(hierarchyNode.getWeightage());

	}

	@Transactional
	public void obsoleteHierarchyNode(long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		HierarchyNode hierarchyNode = hierarchyRepository.findOne(hierarchyNodeId);
		if (hierarchyNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "HierarchyNodeNotFound");
		}
		hierarchyNode.setUseStatus(UsageStatus.INACTIVE);
		hierarchyRepository.save(hierarchyNode);
	}

	@Transactional
	public HierarchyNode createHierarchyNode(HierarchyNode hierarchyNode, String owner)
			throws PersonNotFoundException, ServiceLevelValidationException {
		if (null != owner) {
			Person person = personRepository.findOne(owner);
			if (person == null) {
				throw new PersonNotFoundException(owner, "ownerNotFound");
			}
			hierarchyNode.setOwner(person);
			return hierarchyRepository.save(hierarchyNode);
		} else {
			throw new PersonNotFoundException(owner, "ownerNotFound");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gvg.syena.core.api.services.HierarchyManagement#
	 * createStandardHierarchyNodes
	 * (com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode[],
	 * java.lang.String)
	 */
	@Transactional
	@Override
	public StandardHierarchyNode[] createStandardHierarchyNodes(StandardHierarchyNode[] hierarchyNodes, String owner)
			throws ServiceLevelValidationException, PersonNotFoundException {
		Person person;

		if (owner == null) {
			throw new PersonNotFoundException(owner, "ownerNotFound");
		} else {
			person = personRepository.findOne(owner);
			if (person == null) {
				throw new PersonNotFoundException(owner, "ownerNotFound");
			}
		}

		List<StandardHierarchyNode<StandardHierarchyNode>> standardHierarchyNodes = new ArrayList<StandardHierarchyNode<StandardHierarchyNode>>();
		for (StandardHierarchyNode standardHierarchyNode : hierarchyNodes) {
			setOwner(standardHierarchyNode, person);
			standardHierarchyNodes.add(standardHierarchyNode);
			standardHierarchyNode.resetParent();
		}

		Iterable<StandardHierarchyNode<StandardHierarchyNode>> nodes = standardHierarchyNodeRepository
				.save(standardHierarchyNodes);
		ArrayList<StandardHierarchyNode> arrayList = new ArrayList<StandardHierarchyNode>();
		for (StandardHierarchyNode node : nodes) {
			arrayList.add(node);
		}

		StandardHierarchyNode[] array = new StandardHierarchyNode[arrayList.size()];
		arrayList.toArray(array);
		return array;
	}

	private void setOwner(StandardHierarchyNode standardHierarchyNode, Person person) {

		standardHierarchyNode.setOwner(person);
		Set<StandardHierarchyNode> children = standardHierarchyNode.getChildren();
		if (children != null) {
			for (StandardHierarchyNode standardHierarchyNode2 : children) {
				setOwner(standardHierarchyNode2, person);
			}
		}

	}

	@Transactional
	public void createProject(Project project) {
		projectRepository.save(project);
	}

	@Transactional
	public void linkHierarchyNodes(long parentNodeId, long nodeId)
			throws HierarchyNodeNotFoundException, AlreadyLinkedException {
		HierarchyNode parentNode = hierarchyRepository.findOne(parentNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(parentNodeId, "Parent Node NotFound");
		}
		HierarchyNode childNode = hierarchyRepository.findOne(nodeId);
		if (childNode == null) {
			throw new HierarchyNodeNotFoundException(parentNodeId, "Child Node NotFound");
		}

		@SuppressWarnings("unchecked")
		boolean newOne = parentNode.addChild(childNode);
		if (!newOne) {
			throw new AlreadyLinkedException(parentNode, "Already Linked", childNode);
		}

		hierarchyRepository.save(parentNode);
		// childNode.setParentId(parentNode.getId());
		childNode.setParent(parentNode);
		hierarchyRepository.save(childNode);
	}

	@Transactional
	public void unlinkHierarchyNodes(long parentNodeId, long nodeId) throws HierarchyNodeNotFoundException {
		HierarchyNode parentNode = hierarchyRepository.findOne(parentNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(parentNodeId, "Parent Node NotFound");
		}
		HierarchyNode childNode = hierarchyRepository.findOne(nodeId);
		if (childNode == null) {
			throw new HierarchyNodeNotFoundException(parentNodeId, "Child Node NotFound");
		}
		parentNode.removeChild(childNode);
		// childNode.setParentId(0);
		childNode.removeParent();
		hierarchyRepository.save(childNode);
		hierarchyRepository.save(parentNode);

	}

	@Transactional
	public PageItr<HierarchyNode> getObsoleteHierarchyNodes(int pageNumber, int pageSize) {
		PageRequest pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(hierarchyRepository.findByUseStatus(UsageStatus.INACTIVE, pageable));

	}

	@Transactional
	public long[] getHierarchyNodeIds(String hierarchyLevelName)
			throws ServiceLevelValidationException, HierarchyLevelNotFoundException {
		HierarchyLevel hierarchyLevel = hierarchyLevelRepository.findOne(hierarchyLevelName);
		if (hierarchyLevel == null) {
			throw new HierarchyLevelNotFoundException(hierarchyLevelName, "Hierarchy Level Not Found");
		}
		List<HierarchyNode> datas = hierarchyRepository.findByHierarchyLevel(hierarchyLevel);
		long[] nodeIds = new long[datas.size()];
		int i = 0;
		for (HierarchyNode data : datas) {
			nodeIds[i++] = data.getId();
		}
		return nodeIds;
	}

	@Transactional
	public PageItr<HierarchyNode> getChildHierarchyNodes(long[] parentIds, int pageNumber, int pageSize)
			throws ServiceLevelValidationException {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		UsageStatus useStatus = UsageStatus.ACTIVE;
		return AppUtil.convert(hierarchyRepository.findByParentIdInAndUseStatus(parentIds, useStatus, pageable));
	}

	@Transactional
	public PageItr<HierarchyNode> getHierarchyNodes(long[] nodeIds, int pageNumber, int pageSize) {
		List<Long> ids = new ArrayList<Long>();
		for (long value : nodeIds) {
			ids.add(value);
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(hierarchyRepository.findByIdIn(ids, pageable));

	}

	@Transactional
	public PageItr<HierarchyNode> searchHierarchyNodesInLevel(String hierarchyNodename, String hierarchyLevelName,
			int pageNumber, int pageSize) throws HierarchyLevelNotFoundException {
		HierarchyLevel hierarchyLevel = hierarchyLevelRepository.findOne(hierarchyLevelName);
		if (hierarchyLevel == null) {
			throw new HierarchyLevelNotFoundException(hierarchyLevelName, "Hierarchy Level Not Found");
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		UsageStatus useStatus = UsageStatus.ACTIVE;
		return AppUtil.convert(hierarchyRepository.findByHierarchyNodeNameLikeIgnoreCaseAndUseStatusAndHierarchyLevel(
				hierarchyNodename, useStatus, hierarchyLevel, pageable));
	}

	@Override
	public PageItr<StandardHierarchyNode> searchStandardHierarchyNodesInLevel(String hierarchyNodename,
			String hierarchyLevelName, int pageNumber, int pageSize) throws HierarchyLevelNotFoundException {
		HierarchyLevel hierarchyLevel = hierarchyLevelRepository.findOne(hierarchyLevelName);
		if (hierarchyLevel == null) {
			throw new HierarchyLevelNotFoundException(hierarchyLevelName, "Hierarchy Level Not Found");
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(standardHierarchyNodeRepository.findByNameLikeAndHierarchyLevel(hierarchyNodename,
				hierarchyLevel, pageable));

	}

	@Transactional
	public PageItr<HierarchyNode> searchHierarchyNodesInChild(String hierarchyNodename, long parentId, int pageNumber,
			int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		UsageStatus useStatus = UsageStatus.ACTIVE;
		return AppUtil.convert(hierarchyRepository.findByHierarchyNodeNameLikeIgnoreCaseAndUseStatusAndParentId(
				hierarchyNodename, useStatus, parentId, pageable));
	}

	@Transactional(rollbackOn = { AlreadyLinkedException.class })
	public void linkHierarchyNodes(long parentNodeId, long... nodeIds)
			throws HierarchyNodeNotFoundException, AlreadyLinkedException {
		HierarchyNode parentNode = hierarchyRepository.findOne(parentNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(parentNodeId, "Parent Node NotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long nodeId : nodeIds) {
			ids.add(nodeId);
		}
		List<HierarchyNode> linkNodes = hierarchyRepository.findByIdIn(ids);
		parentNode.addChildren(linkNodes);
		for (HierarchyNode linkNode : linkNodes) {
			// linkNode.setParentId(parentNode.getId());
			linkNode.setParent(parentNode);
		}
		hierarchyRepository.save(parentNode);
		hierarchyRepository.save(linkNodes);
	}

	@Transactional(rollbackOn = { AlreadyLinkedException.class })
	public void createAndLinkHierarchyNodes(long parentNodeId, long... standardHierarchyNodeIds)
			throws HierarchyNodeNotFoundException, AlreadyLinkedException {
		HierarchyNode parentNode = hierarchyRepository.findOne(parentNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(parentNodeId, "Parent Node NotFound");
		}

		List<HierarchyNode> exNode = hierarchyRepository.findByParentIdAndStandardHierarchyNodeIdIn(parentNodeId,
				standardHierarchyNodeIds);
		if (!exNode.isEmpty()) {
			throw new AlreadyLinkedException(parentNode, "AlreadyLinked", exNode.get(0));
		}
		List<HierarchyNode> newHierarchyNodes = createHierarchyNodesFromStandardHierarchyNodes(
				standardHierarchyNodeIds);
		List<Long> containIds = new ArrayList<Long>();
		for (HierarchyNode newHierarchyNode : newHierarchyNodes) {
			boolean added = parentNode.addChild(newHierarchyNode);
			if (!added) {
				containIds.add(newHierarchyNode.getId());
			}
		}
		if (containIds.size() > 0) {
			throw new AlreadyLinkedException(parentNode, "AlreadyLinked", exNode.get(0));
		}

		hierarchyRepository.save(parentNode);
	}

	private List<HierarchyNode> createHierarchyNodesFromStandardHierarchyNodes(long... nodeIds) {
		List<Long> ids = new ArrayList<Long>();
		for (long nodeId : nodeIds) {
			ids.add(nodeId);
		}
		List<StandardHierarchyNode<StandardHierarchyNode>> linkNodes = standardHierarchyNodeRepository.findByIdIn(ids);
		List<HierarchyNode> newHierarchyNodes = new ArrayList<HierarchyNode>();
		for (StandardHierarchyNode<StandardHierarchyNode> linkNode : linkNodes) {
			newHierarchyNodes.add(new HierarchyNode(linkNode, 1, true));
		}
		return newHierarchyNodes;
	}

	@Transactional
	public void linkJobs(long hierarchyNodeId, long... jobIds) throws HierarchyNodeNotFoundException {
		HierarchyNode parentNode = hierarchyRepository.findOne(hierarchyNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Parent Node NotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		for (long nodeId : jobIds) {
			ids.add(nodeId);
		}
		List<Job> jobs = jobRepository.findByIdIn(ids);
		parentNode.addJobs(jobs);
		jobRepository.save(jobs);
	}

	@Transactional
	public PageItr<Job> getLinkedJob(long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		HierarchyNode hierarchyNode = hierarchyRepository.findOne(hierarchyNodeId);
		ArrayList<Job> data = new ArrayList<Job>();
		// data.addAll(hierarchyNode.getJobs());
		// TODO clarification
		List<Job> datSet = new ArrayList<Job>();
		AppUtil.getJobFromJobGroup(datSet, hierarchyNode.getDefaultJobGroup());
		data.addAll(datSet);
		PageItr<Job> pageItr = new PageItr<Job>(data, 0, 1);
		return pageItr;
	}

	@Transactional
	public StandardHierarchyNode<StandardHierarchyNode> getStandardHierarchyNode(long nodeId) {
		return standardHierarchyNodeRepository.findOne(nodeId);
	}

	@Transactional
	public PageItr<StandardHierarchyNode<StandardHierarchyNode>> getChildStandardHierarchyNode(long[] parentIds,
			int pageNumber, int pageSize) {
		UsageStatus useStatus = UsageStatus.ACTIVE;
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(standardHierarchyNodeRepository.findByParentIdIn(parentIds, pageable));
	}

	private void saveAsHierarchyNode(Set<StandardHierarchyNode<StandardHierarchyNode>> children, long parentId,
			String ownerId) throws PersonNotFoundException, ServiceLevelValidationException,
					HierarchyNodeNotFoundException, AlreadyLinkedException {
		for (StandardHierarchyNode standardHierarchyNode : children) {
			System.out.println("Save Me" + standardHierarchyNode.getName());
			HierarchyNode hierarchyNode = new HierarchyNode(standardHierarchyNode, 1, false);
			hierarchyNode = createHierarchyNode(hierarchyNode, ownerId);
			if (parentId != 0) {
				linkHierarchyNodes(parentId, hierarchyNode.getId());
			}

			Set<StandardHierarchyNode<StandardHierarchyNode>> grandChildren = standardHierarchyNode.getChildren();
			if (grandChildren != null) {
				saveAsHierarchyNode(grandChildren, hierarchyNode.getId(), ownerId);
			}

		}

	}

	@Transactional
	public String[] getImageNames(long hierarchyNodeId) throws HierarchyNodeNotFoundException {
		HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
		if (node == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Node NotFound");
		}
		Map<String, String> imageMap = node.getImages();
		if (imageMap != null) {
			Set<String> keys = imageMap.keySet();
			return (String[]) keys.toArray();
		}
		return new String[] {};
	}

	// @Transactional
	// public BufferedImage getImage(long hierarchyNodeId, String imageName)
	// throws HierarchyNodeNotFoundException, ApplicationIOException {
	// HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
	// if (node == null) {
	// throw new HierarchyNodeNotFoundException(hierarchyNodeId,
	// "HierarchyNodeNotFound", "Node NotFound");
	// }
	// Map<String, String> imageMap = node.getImages();
	// if (imageMap != null) {
	// String imagePath = imageMap.get(imageName);
	// InputStream inputStream = this.getClass().getResourceAsStream(imagePath);
	// try {
	// return ImageIO.read(inputStream);
	// } catch (IOException e) {
	// throw new ApplicationIOException("", "", e);
	// }
	// }
	// return null;
	// }

	@Transactional
	public PageItr<HierarchyNode> searchWithProperties(HierarchyNodeProperties hierarchyNodeProperties, int pageNumber,
			int pageSize) {
		String name = hierarchyNodeProperties.getName();
		String description = hierarchyNodeProperties.getDescription();
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		UsageStatus useStatus = UsageStatus.ACTIVE;
		return AppUtil.convert(hierarchyRepository.findByHierarchyNodeNameOrDescriptionAndUseStatus(name, description,
				useStatus, pageable));
	}

	@Transactional
	public List<JobGroup<Job>> createAndlinkJobGroups(long hierarchyNodeId, long... standardJobGroupIds)
			throws HierarchyNodeNotFoundException, JobNotFoundException {
		HierarchyNode parentNode = hierarchyRepository.findOne(hierarchyNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Parent Node NotFound");
		}
		List<JobGroup<Job>> newJobGroups = jobManagement.onlyCreateJobGroupFromStandardJobGroup(standardJobGroupIds);
		for (JobGroup<Job> jobGroup : newJobGroups) {
			jobGroup.setConnectedNode(parentNode);
			// TODO
			for (Job job : jobGroup.getJobs()) {
				job.setConnectedNode(parentNode);
				job.setDescription(parentNode.getName() +"-" + job.getDescription());
			}
			jobGroup.setDescription(parentNode.getName() + "-" + jobGroup.getName());
		}
		parentNode.addJobGropus(newJobGroups);
		jobGroupRepository.save(newJobGroups);
		hierarchyRepository.save(parentNode);
		return newJobGroups;
	}

	@Transactional
	public PageItr<JobGroup<Job>> getLinkedJobGroups(long hierarchyNodeId) {
		HierarchyNode hierarchyNode = hierarchyRepository.findOne(hierarchyNodeId);
		Collection<JobGroup<Job>> data = hierarchyNode.getJobGroups();
		ArrayList<JobGroup<Job>> list = new ArrayList<JobGroup<Job>>();
		list.addAll(data);
		PageItr<JobGroup<Job>> pageItr = new PageItr<JobGroup<Job>>(list, 0, 1);
		return pageItr;
	}

	@Transactional
	public void saveAsHierarchyNode(long nodeId, String ownerId) throws PersonNotFoundException,
			ServiceLevelValidationException, HierarchyNodeNotFoundException, AlreadyLinkedException {
		StandardHierarchyNode<StandardHierarchyNode> standardHierarchyNode = getStandardHierarchyNode(nodeId);
		HierarchyNode hierarchyNode = new HierarchyNode(standardHierarchyNode, 1, true);
		createHierarchyNode(hierarchyNode, ownerId);
		// PageItr<StandardHierarchyNode<StandardHierarchyNode>> child =
		// getChildStandardHierarchyNode(parentIds);
		// for (StandardHierarchyNode standardHierarchyNode : nodes) {
		// HierarchyNode hierarchyNode = new
		// HierarchyNode(standardHierarchyNode, 1);
		// createHierarchy(hierarchyNode, ownerId);
		// }
		// Set<StandardHierarchyNode<StandardHierarchyNode>> set = new
		// TreeSet<StandardHierarchyNode<StandardHierarchyNode>>();
		// set.addAll(child.getContent());
		// saveAsHierarchyNode(set, parentId, ownerId);

	}

	@Transactional
	public List<Job> createAndlinkJob(long hierarchyNodeId, long[] standardJobIds)
			throws HierarchyNodeNotFoundException, InvalidDataException {
		HierarchyNode parentNode = hierarchyRepository.findOne(hierarchyNodeId);
		if (parentNode == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Parent Node NotFound");
		}
		List<Job> exJobs = jobRepository.findByConnectedNodeIdAndStandardJobIdIn(hierarchyNodeId, standardJobIds);
		if (!exJobs.isEmpty()) {
			throw new InvalidDataException(MessageKeys.JOB_ALREADY_CONNECTED, "Already linked");
		}
		List<Job> jobs = jobManagement.onlyCreateJobFromStandardJob(standardJobIds);
		for (Job job : jobs) {
			parentNode.addJob(job);
		}
		// parentNode.addJobs(jobs);
		jobRepository.save(jobs);
		hierarchyRepository.save(parentNode);
		return jobs;
	}

	@Transactional
	@Override
	public StandardHierarchyNode createStandardHierarchyNode(StandardHierarchyNode standardHierarchyNode, String owner)
			throws ServiceLevelValidationException, PersonNotFoundException {

		Person person;

		if (owner == null) {
			throw new ServiceLevelValidationException(MessageKeys.OWNER_CAN_NOT_BE_NULL, " owner can not be null.");
		} else {
			person = personRepository.findOne(owner);
			if (person == null) {
				throw new PersonNotFoundException(owner, "ownerNotFound");
			}
		}
		setOwner(standardHierarchyNode, person);
		standardHierarchyNode.resetParent();
		standardHierarchyNode = standardHierarchyNodeRepository.save(standardHierarchyNode);
		return standardHierarchyNode;

	}

	@Override
	@Transactional
	public PageItr<StandardHierarchyNode> getLinkedStandardHierarchyNodes(long parentId, int pageNumber, int pageSize) {
		Pageable pageble = new PageRequest(pageNumber, pageSize);
		return AppUtil.convert(standardHierarchyNodeRepository.findByParentId(parentId, pageble));
	}

	@Override
	@Transactional
	public HierarchyDoc getHierarchyNodeImage(long hierarchyNodeId, String imageName, DocumentType documentType)
			throws HierarchyNodeNotFoundException, ImageIOException {

		HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
		if (node == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Node NotFound");
		}
		try {
			String path = node.getDocumntPath(documentType, imageName);

			if (path == null) {
				throw new ImageIOException(hierarchyNodeId, imageName, "Image not found", "ImageIOException");
			}
			File file = new File(path);
			byte[] bFile = new byte[(int) file.length()];
			FileInputStream fileInputStream = null;
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();
			HierarchyDoc image = new HierarchyDoc();
			image.setDocumentName(imageName);
			image.setContent(bFile);
			image.setDocumentType(documentType);
			return image;
		} catch (IOException e) {
			throw new ImageIOException(hierarchyNodeId, imageName, "ImageIOException", "ImageIOException");
		}
	}

	private String getDirPath(HierarchyNode node, DocumentType documentType) {
		return FilePath + File.separator + "documents" + File.separator + node.getId() + File.separator + documentType;
	}

	@Override
	@Transactional
	public void uploadHierarchyNodeImage(long hierarchyNodeId, String imageName, byte[] bytes,
			DocumentType documentType) throws ImageIOException {

		try {
			HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
			if (node == null) {
				throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Node NotFound");
			}
			File dir = new File(getDirPath(node, documentType));
			dir.mkdirs();

			File file = new File(dir, imageName + ".jpg");

			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
			stream.write(bytes);
			stream.close();
			node.putDocumntPath(documentType, imageName, file.getPath());
		} catch (Exception e) {
			throw new ImageIOException(hierarchyNodeId, imageName, e);
		}

	}

	@Override
	@Transactional
	public void uploadHierarchyNodeDoc(long hierarchyNodeId, String docName, byte[] bytes, DocumentType documentType)
			throws DocIOException {

		try {
			HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
			if (node == null) {
				throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Node NotFound");
			}
			File dir = new File(getDirPath(node, documentType));
			dir.mkdirs();

			File file = new File(dir, docName);

			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
			stream.write(bytes);
			stream.close();
			node.putDocumntPath(documentType, docName, file.getPath());
		} catch (Exception e) {
			throw new DocIOException(hierarchyNodeId, docName, e);
		}

	}

	@Override
	@Transactional
	public List<HierarchyDoc> getHierarchyNodeDocs(long hierarchyNodeId, DocumentType documentType)
			throws HierarchyNodeNotFoundException {
		HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
		if (node == null) {
			throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Node NotFound");
		}
		List<HierarchyDoc> docs = new ArrayList<>();
		if (documentType == DocumentType.Documents) {
			if (null != node.getDocuments()) {
				for (Iterator<String> it = node.getDocuments().keySet().iterator(); it.hasNext();) {
					String docName = it.next();
					HierarchyDoc doc = new HierarchyDoc();
					doc.setDocumentName(docName);
					doc.setDocumentType(DocumentType.Documents);
					docs.add(doc);
				}
			}
		} else if (documentType == DocumentType.Drawings) {
			if (null != node.getDrawings()) {
				for (Iterator<String> it = node.getDrawings().keySet().iterator(); it.hasNext();) {
					String docName = it.next();
					HierarchyDoc doc = new HierarchyDoc();
					doc.setDocumentName(docName);
					doc.setDocumentType(DocumentType.Drawings);
					docs.add(doc);
				}
			}
		}
		return docs;

	}

	@Override
	@Transactional
	public void createHierarchyNode(List<HierarchyNode> hierarchyNodes) {
		hierarchyRepository.save(hierarchyNodes);

	}

	@Override
	@Transactional
	public PageItr<HierarchyNode> getParentHierarchyNodes(long[] childIds, int pageNumber, int pageSize) {

		List<Long> ids = new ArrayList<Long>();
		for (long value : childIds) {
			ids.add(value);
		}

		Pageable pageble = new PageRequest(pageNumber, pageSize);
		Page<HierarchyNode> page = hierarchyRepository.findParentByIdIn(ids, pageble);
		// List<HierarchyNode> hierarchyNodes = page.getContent();
		// List<StandardHierarchyNode> parents = new
		// ArrayList<StandardHierarchyNode>();
		// if (hierarchyNodes != null) {
		// for (HierarchyNode hierarchyNode : hierarchyNodes) {
		// StandardHierarchyNode parent = hierarchyNode.getParent();
		// if (parent != null) {
		// parents.add(parent);
		// }
		// }
		// }
		// new PageItr<>(parents, page.getNumber(), page.getTotalPages())
		return AppUtil.convert(page);
	}

	@Override
	@Transactional
	public PageItr<Job> getJobsByConnectedInchildNodeIdsOtherThan(long parentIds, int pageNumber, int pageSize)
			throws HierarchyNodeNotFoundException {
		HierarchyNode node = hierarchyRepository.findOne(parentIds);
		if (node == null) {
			throw new HierarchyNodeNotFoundException(parentIds, "Node NotFound");
		}
		List<Long> ids = new ArrayList<Long>();
		UtilServices.getChildNodeIds(node, ids);
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> jobs = jobRepository.findByconnectedNodeIdIn(ids, pageable);
		return AppUtil.convert(jobs);
	}

	@Override
	@Transactional(rollbackOn = { Exception.class })
	public Job createPuchListJob(long parentIds, Job job) throws HierarchyNodeNotFoundException, InvalidDataException {
		HierarchyNode node = hierarchyRepository.findOne(parentIds);
		if (node == null) {
			throw new HierarchyNodeNotFoundException(parentIds, "Node NotFound");
		}
//		Job newJob = new Job(job.getJobName(), job.getDescription());
		job = jobManagement.createJob(job);
		job.setNodeType(NodeType.instance);
		job.setInPunchList(true);
		node.addJob(job);
		hierarchyRepository.save(node);
		return job;
	}

	@Transactional
	private void update(Job fromJob, Job toJob) {
		toJob.setDescription(fromJob.getDescription());
		toJob.setWeightage(fromJob.getWeightage());
		toJob.setOwner(fromJob.getOwner());
		toJob.setApplicableForComplete(fromJob.isApplicableForComplete());

		toJob.setExecutor(fromJob.getExecutor());
		toJob.setInitiator(fromJob.getInitiator());
	}

	@Override
	@Transactional(rollbackOn = { Exception.class })
	public JobGroup<Job> createPuchListJobGroup(long parentIds, JobGroup<Job> jobGroup)
			throws HierarchyNodeNotFoundException, InvalidDataException {
		HierarchyNode node = hierarchyRepository.findOne(parentIds);
		if (node == null) {
			throw new HierarchyNodeNotFoundException(parentIds, "Node NotFound");
		}
		List<Job> jobs = new ArrayList<>();
		for (Job job : jobGroup.getJobs()) {
			Job newJob = createPuchListJob(node.getId(), job);
			jobs.add(newJob);

		}
		jobGroup.getJobs().clear();
		jobGroup.getJobs().addAll(jobs);
//		jobRepository.save(jobGroup.getJobs());
		jobGroup = jobManagement.createJobGroup(jobGroup);
		jobGroup.setNodeType(NodeType.instance);
		jobGroup.setInPunchList(true);
		node.addJobGroup(jobGroup);

		hierarchyRepository.save(node);
		return jobGroup;
	}

	// private void copyJobGroup(JobGroup<Job> fromJobGroup, JobGroup<Job>
	// toJobGroup, NodeType instance) throws InvalidDataException {
	//
	// toJobGroup.setNodeType(instance);
	// JobManagementImpl.update(fromJobGroup, toJobGroup);
	// jobManagement.createJobGroup(toJobGroup);
	//
	// for (Job job : fromJobGroup.getJobs()) {
	// Job newJob = new Job(job.getJobName(), job.getDescription());
	// newJob.setNodeType(NodeType.instance);
	// newJob.setInPunchList(true);
	// JobManagementImpl.update(job, newJob);
	// jobManagement.createJob(newJob);
	// toJobGroup.addJob(newJob);
	// }
	//
	// Set<JobGroup<Job>> jobGroups = fromJobGroup.getJobGroups();
	// if (jobGroups != null) {
	// for (JobGroup<Job> jobgGroup1 : jobGroups) {
	// JobGroup<Job> newjobGroup = new JobGroup<Job>(jobgGroup1.getName());
	// toJobGroup.addJobGroup(newjobGroup);
	// copyJobGroup(newjobGroup, jobgGroup1, instance);
	// }
	// }
	//
	// }

	@Override
	@Transactional(rollbackOn = { HierarchyNodeNotFoundException.class, RemovalBarrierException.class })
	public void removeHierarchyNodeLink(long[] hierarchyNodeIds)
			throws HierarchyNodeNotFoundException, RemovalBarrierException {

		for (long hierarchyNodeId : hierarchyNodeIds) {
			HierarchyNode node = hierarchyRepository.findOne(hierarchyNodeId);
			if (node == null) {
				throw new HierarchyNodeNotFoundException(hierarchyNodeId, "Node NotFound");
			}
			checkForJob(node);
			checkHn(node);
			jobCheck(node);
			hierarchyRepository.delete(node);
		}
	}

	private void checkForJob(DependencyAttachableHierarchy node)
			throws HierarchyNodeNotFoundException, RemovalBarrierException {

		boolean a = node.getPredecessorDependency().getStartForStart().getJobs().size() > 0;
		a = a || node.getPredecessorDependency().getStartForFinish().getJobs().size() > 0;
		a = a || node.getPredecessorDependency().getFinishForStart().getJobs().size() > 0;
		a = a || node.getPredecessorDependency().getFinishToFinish().getJobs().size() > 0;
		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.PredecessorDependency);
		}

		a = a || node.getPredecessorDependency().getStartForStart().getJobGroups().size() > 0;
		a = a || node.getPredecessorDependency().getStartForFinish().getJobGroups().size() > 0;
		a = a || node.getPredecessorDependency().getFinishForStart().getJobGroups().size() > 0;
		a = a || node.getPredecessorDependency().getFinishToFinish().getJobGroups().size() > 0;

		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.PredecessorDependency);
		}

		a = a || node.getSuccessorDependency().getStartForStart().getJobs().size() > 0;
		a = a || node.getSuccessorDependency().getStartForFinish().getJobs().size() > 0;
		a = a || node.getSuccessorDependency().getFinishForStart().getJobs().size() > 0;
		a = a || node.getSuccessorDependency().getFinishToFinish().getJobs().size() > 0;

		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.SuccessorDependency);
		}

		a = a || node.getSuccessorDependency().getStartForStart().getJobGroups().size() > 0;
		a = a || node.getSuccessorDependency().getStartForFinish().getJobGroups().size() > 0;
		a = a || node.getSuccessorDependency().getFinishForStart().getJobGroups().size() > 0;
		a = a || node.getSuccessorDependency().getFinishToFinish().getJobGroups().size() > 0;
		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.SuccessorDependency);
		}

	}

	private void checkHn(HierarchyNode node) throws HierarchyNodeNotFoundException, RemovalBarrierException {
		boolean a = false;
		a = a || hierarchyNodeDependencyManegmanet.getPredecessorHierarchyNodeDependency(node.getId(), 0, 1)
				.getContent().size() > 0;
		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.PredecessorHierarchyNode);
		}
		a = a || hierarchyNodeDependencyManegmanet.getSuccessorHierarchyNodeDependency(node.getId(), 0, 1).getContent()
				.size() > 0;
		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.SuccessorHierarchyNode);
		}

	}

	private void jobCheck(HierarchyNode node) throws RemovalBarrierException {
		boolean a = false;
		a = a || node.getJobs().size() > 0;
		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.job);
		}

		a = a || node.getJobGroups().size() > 0;
		if (a) {
			throw new RemovalBarrierException(node, RemovalBarrierException.jobGroup);
		}

	}

	@Override
	@Transactional
	public void removeLinkedJobs(long... jobIds) throws HierarchyNodeNotFoundException {
		List<Long> ids = new ArrayList<>();
		for (long jobId : jobIds) {
			ids.add(jobId);
		}
		List<Job> jobs = (List<Job>) jobRepository.findByIdIn(ids);
		for (Job job : jobs) {
			HierarchyNode connectedNode = job.getConnectedNode();
			connectedNode.getJobs().remove(job);
			hierarchyRepository.save(connectedNode);

		}
		jobRepository.delete(jobs);
	}

	@Override
	@Transactional(rollbackOn = { HierarchyNodeNotFoundException.class, RemovalBarrierException.class })
	public void removeLinkedJobGroups(long[] jobGroupIds)
			throws HierarchyNodeNotFoundException, RemovalBarrierException {

		List<Long> ids = new ArrayList<>();
		for (long jobGroupId : jobGroupIds) {
			ids.add(jobGroupId);
		}
		List<JobGroup<Job>> jobGroups = jobGroupRepository.findByIdIn(ids);

		for (JobGroup<Job> jobGroup : jobGroups) {
			checkForJob(jobGroup);
			HierarchyNode connectedNode = jobGroup.getConnectedNode();
			connectedNode.getJobGroups().remove(jobGroup);
			hierarchyRepository.save(connectedNode);
		}
		jobGroupRepository.delete(jobGroups);

	}

	private void getParentIds(StandardHierarchyNode node, List<Long> ids) {
		if (null != node.getParent()) {
			if (ids.contains(node.getParent().getId())) {
				ids.add(node.getParent().getId());
				getParentIds(node.getParent(), ids);
			}
		}
	}

	@Override
	@Transactional
	public List<Long> getParentIds(long hierarchyId, List<Long> ids) {
		HierarchyNode node = hierarchyRepository.findOne(hierarchyId);
		getParentIds(node, ids);
		return ids;
	}

}
