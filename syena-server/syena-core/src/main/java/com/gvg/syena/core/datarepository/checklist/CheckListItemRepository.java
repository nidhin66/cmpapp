package com.gvg.syena.core.datarepository.checklist;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;

public interface CheckListItemRepository extends PagingAndSortingRepository<CheckListItem, Long>{

}
