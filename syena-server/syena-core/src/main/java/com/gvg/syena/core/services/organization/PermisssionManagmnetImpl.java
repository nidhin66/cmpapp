package com.gvg.syena.core.services.organization;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;
import com.gvg.syena.core.api.entity.util.UserActivityPermission;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;
import com.gvg.syena.core.api.services.PermisssionManagmnet;
import com.gvg.syena.core.datarepository.organization.RoleRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.datarepository.util.UserActivityPermissionRepository;

@Service("permisssionManagmnet")
public class PermisssionManagmnetImpl implements PermisssionManagmnet {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private UserActivityPermissionRepository userActivityPermissionRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	@Transactional
	public boolean checkPermission(String userId, String departmentId, ActivityArea activityArea, ActivityOperation activityOperation) throws PersonNotFoundException {
		Person user = personRepository.findOne(userId);
		if (user == null) {
			throw new PersonNotFoundException(userId, "Person Not Found");
		}

		Role roleInDepartment = user.getRolesInDepartment().get(departmentId);
		String roleId;
		if (roleInDepartment == null) {
			roleId = null;
		} else {
			roleId = roleInDepartment.getRoleId();
		}

		List<UserActivityPermission> userActivityPermissions = userActivityPermissionRepository.findByPermittedRolesOrPermittedPersonsIdAndActivityAreaAndActivityOperations(
				roleId, user.getId(), activityArea, activityOperation);

		for (UserActivityPermission userActivityPermission : userActivityPermissions) {
			List<Person> permittedPersons = userActivityPermission.getPermittedPersons();
			if (permittedPersons.contains(user)) {
				return true;
			}
		}
		return false;

	}

	@Override
	@Transactional
	public void setPermissionUser(String userId, ActivityArea activityArea, List<ActivityOperation> activityOperations) throws PersonNotFoundException {
		Person user = personRepository.findOne(userId);
		if (user == null) {
			throw new PersonNotFoundException(userId, "Person Not Found");
		}
		UserActivityPermission activityPermission = new UserActivityPermission();
		List<Person> persons = new ArrayList<Person>();
		persons.add(user);
		activityPermission.setPermittedPersons(persons);
		activityPermission.setActivityArea(activityArea);
		activityPermission.setActivityOperations(activityOperations);
		userActivityPermissionRepository.save(activityPermission);
	}

	@Override
	@Transactional
	public void setPermissionRole(String roleId, ActivityArea activityArea, List<ActivityOperation> activityOperations) {
		Role role = roleRepository.findOne(roleId);
		UserActivityPermission activityPermission = new UserActivityPermission();
		List<Role> permittedRoles = new ArrayList<Role>();
		permittedRoles.add(role);
		activityPermission.setPermittedRoles(permittedRoles);
		activityPermission.setActivityArea(activityArea);
		activityPermission.setActivityOperations(activityOperations);
		userActivityPermissionRepository.save(activityPermission);

	}

}
