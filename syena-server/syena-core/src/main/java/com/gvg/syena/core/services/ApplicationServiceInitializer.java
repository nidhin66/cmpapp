package com.gvg.syena.core.services;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.gvg.syena.core.api.entity.alert.mail.MailServerConfig;
import com.gvg.syena.core.api.entity.scheduler.SchedulerJob;
import com.gvg.syena.core.api.services.ApplicationService;
import com.gvg.syena.core.api.util.ApplicationDateUtil;
import com.gvg.syena.core.api.util.Configuration;
import com.gvg.syena.core.datarepository.alert.mail.MailServerConfigRepository;
import com.gvg.syena.core.datarepository.scheduler.SchedulerJobRepository;
import com.gvg.syena.core.services.alert.mail.EmailService;
import com.gvg.syena.core.services.schedulerjob.SchedulerJobService;

@Component
public class ApplicationServiceInitializer implements ApplicationListener<ContextRefreshedEvent> {
	
	private SchedulerJobRepository schedulerJobRepository;

	private MailServerConfigRepository mailServerConfigRepository;
	
	@Autowired
	private ApplicationService applicationService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		initializeMailServie();
		initializeSchedulerJobService();
		String dateHrDiffStr = applicationService.getPropertyValue(ApplicationDateUtil.APPLICATION_DATE_HOUR_DEFERENCE);
		int dateDeference = 0;
		if (dateHrDiffStr != null && dateHrDiffStr != "") {
			dateDeference = Integer.valueOf(dateHrDiffStr);
		}
		Configuration.dateDeference = dateDeference;
	}
	
	private void initializeSchedulerJobService(){
		List<SchedulerJob> scheduleJobs = schedulerJobRepository.findByEnabledTrue();
		SchedulerJobService.getInstance().initialize(scheduleJobs);
	}
	
	private void initializeMailServie(){
		Iterable<MailServerConfig> mailServers = mailServerConfigRepository.findAll();
		if(null !=mailServers){
			for(Iterator<MailServerConfig> it = mailServers.iterator(); it.hasNext();){
				MailServerConfig mailServerConfig = it.next();
				EmailService.getInstance().initialize(mailServerConfig);
			}
			
		}
	}
	
	@Autowired
	 public void setSchedulerJobRepository(SchedulerJobRepository schedulerJobRepository) {
		this.schedulerJobRepository = schedulerJobRepository;
	}
	
	@Autowired
	public void setMailServerConfigRepository(MailServerConfigRepository mailServerConfigRepository) {
		this.mailServerConfigRepository = mailServerConfigRepository;
	}

}
