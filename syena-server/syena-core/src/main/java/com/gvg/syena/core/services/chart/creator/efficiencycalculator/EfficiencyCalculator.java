package com.gvg.syena.core.services.chart.creator.efficiencycalculator;

import java.util.Date;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

public interface EfficiencyCalculator {

	double calculateEfficiencyFactor(HierarchyNode hierarchyNode, Date ondate);

}
