package com.gvg.syena.core.services.search;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.util.ApplicationUtil;

@Component("advancedJobGroupSearchDao")
public class AdvancedJobGroupSearchDao {

	@PersistenceContext
	private EntityManager em;

	private String dealy = "select * from Jobgroup where description like :name and nodeType = '2' and id in (select o.Jobgroup_id from (select *  from Jobgroup_scheduledstage js  join scheduledstage ss  on js.scheduledStages_id = ss.id) o join (select Jobgroup_id, max(percentageOfWork)  as topPercentageOfWork from Jobgroup_scheduledstage js  join scheduledstage ss  on js.scheduledStages_id = ss.id where  COALESCE(revisedTime, estimatedTime) <= :date group by Jobgroup_id)  r on o.Jobgroup_id = r.Jobgroup_id and o.percentageOfWork = r.topPercentageOfWork where ( o.actualTime is null or o.actualTime > COALESCE(o.revisedTime, o.estimatedTime) ))";

	private String jobsToBeScheduledQury = "select * from Jobgroup  where description like :name and nodeType = '2' and id in (select distinct Jobgroup_id from Jobgroup_scheduledstage where scheduledstages_id in (select id from scheduledstage where percentageofwork = 0 and estimatedtime is null))";

	private String jobsWithRevisedDates = "select * from Jobgroup where description like :name and nodeType = '2' and id in (select distinct Jobgroup_id from Jobgroup_scheduledstage where scheduledstages_id in (select id from scheduledstage where  revisedtime is not null))";

	public PageItr<JobGroup<Job>> delayedJobGroups(Date date, String name, int pageNumber, int pageSize) {
		Query typedQuery = em.createNativeQuery(dealy, JobGroup.class);
		typedQuery.setParameter("date", date, TemporalType.DATE);
		typedQuery.setParameter("name", name);
		int max = typedQuery.getResultList().size();
		List<JobGroup<Job>> res = typedQuery.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize).getResultList();
		PageItr<JobGroup<Job>> page = new PageItr<>(res, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));
		return page;
	}

	public PageItr<JobGroup<Job>> jobsToBeScheduled(String name, int pageNumber, int pageSize) {
		Query typedQuery = em.createNativeQuery(jobsToBeScheduledQury, JobGroup.class);
		typedQuery.setParameter("name", name);
		int max = typedQuery.getResultList().size();
		List<JobGroup<Job>> res = typedQuery.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize).getResultList();
		PageItr<JobGroup<Job>> page = new PageItr<JobGroup<Job>>(res, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));
		return page;
	}

	public PageItr<JobGroup<Job>> jobsWithRevisedDates(String name, int pageNumber, int pageSize) {
		Query typedQuery = em.createNativeQuery(jobsWithRevisedDates, JobGroup.class);
		typedQuery.setParameter("name", name);
		int max = typedQuery.getResultList().size();
		List<JobGroup<Job>> res = typedQuery.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize).getResultList();
		PageItr<JobGroup<Job>> page = new PageItr<JobGroup<Job>>(res, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));
		return page;
	}

}
