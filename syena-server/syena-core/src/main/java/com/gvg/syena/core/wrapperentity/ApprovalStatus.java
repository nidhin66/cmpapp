package com.gvg.syena.core.wrapperentity;

public enum ApprovalStatus {
	
	forApproval, approved, rejected;

}
