package com.gvg.syena.core.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

public class AppUtil {

	public static <TY> PageItr<TY> convert(Page<TY> page) {
		List<TY> content = page.getContent();
		int currentPage = page.getNumber();
		int totalPages = page.getTotalPages();
		PageItr<TY> itr = new PageItr<TY>(content, currentPage, totalPages);
		return itr;
	}

	public static <T> List<T> arrayToList(T[] ts) {
		List<T> tsList = new ArrayList<T>();
		for (T t : ts) {
			tsList.add(t);
		}
		return tsList;
	}

	public static <T> List<T> convertToList(Iterable<T> items) {
		List<T> list = new ArrayList<T>();
		for (T t : items) {
			list.add(t);
		}
		return list;
	}

	public static List<Job> getJobFromJobGroup(List<Job> jobset, final JobGroup jobGroup) {
		jobset.addAll(jobGroup.getJobs());
		Set<JobGroup> jobGroups = jobGroup.getJobGroups();
		for (JobGroup jobGroup2 : jobGroups) {
			getJobFromJobGroup(jobset, jobGroup2);
		}
		return jobset;
	}
}
