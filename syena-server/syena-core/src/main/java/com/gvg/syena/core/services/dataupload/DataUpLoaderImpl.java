package com.gvg.syena.core.services.dataupload;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.UserActivityPermission;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyLevelRepository;
import com.gvg.syena.core.datarepository.hierarchy.StandardHierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.StandardJobRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;
import com.gvg.syena.core.datarepository.util.UserActivityPermissionRepository;

@Service("dataUpLoader")
public class DataUpLoaderImpl implements DataUpLoader {

	@Autowired
	private HierarchyLevelRepository hierarchyLevelRepository;

	@Autowired
	private StandardHierarchyNodeRepository standardHierarchyNodeRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private StandardJobRepository standardJobRepository;
	
	@Autowired
	private UserActivityPermissionRepository useractivityPermissionRepository;
	
	
	@Autowired
	private PersonRepository personRepository;
	
	

	@Transactional
	public void uploadHierarchyLevel(InputStream inputStream) throws IOException {
		HierarchyNodeReaderExcel dataExcel = new HierarchyNodeReaderExcel(inputStream);
		ArrayList<HierarchyLevel> hierarchyLevels = dataExcel.readHierarchyLevels();
		for (int i = hierarchyLevels.size(); i > 0; i--) {
			hierarchyLevelRepository.save(hierarchyLevels.get(i - 1));
		}

	}
	
	@Transactional
	public void uploadUserActivityPermission(UserActivityPermission userActivityPermission)  {
		useractivityPermissionRepository.save(userActivityPermission);
	}

	@Transactional
	public void uploadStandardHierarchyNodes(InputStream inputStream) throws IOException {
		HierarchyNodeReaderExcel dataExcel = new HierarchyNodeReaderExcel(inputStream);
		StandardHierarchyNode<StandardHierarchyNode> data = dataExcel.read();
		saveChild(data);
		saveMe(data);

	}

	private void saveMe(StandardHierarchyNode data) {
		System.out.println("saveMe : " + data.getName());
		data = standardHierarchyNodeRepository.save(data);
		Set<StandardHierarchyNode> childeren = data.getChildren();
		if (childeren != null) {
			for (StandardHierarchyNode child : childeren) {
				child.setParent(data);
				// child.setParentId(data.getId());
			}
			standardHierarchyNodeRepository.save(childeren);

		}
	}

	private void saveChild(StandardHierarchyNode data) {
		Set<StandardHierarchyNode> childern = data.getChildren();
		if (childern != null) {
			for (StandardHierarchyNode<StandardHierarchyNode> child : childern) {
				saveChild(child);
				saveMe(child);
			}
		}

	}

	@Transactional
	public void uploadJobAndTask(InputStream inputStream) throws IOException {
		JobAndTaskUploader taskUploader = new JobAndTaskUploader(inputStream);
		ArrayList<JobGroup<StandardJob>> jobGroups = taskUploader.read();
		for (JobGroup<StandardJob> jobGroup : jobGroups) {
			saveJobGroup(jobGroup);
			jobGroupRepository.save(jobGroup);
		}

	}

	private void saveJobGroup(JobGroup<StandardJob> jobGroup) {
		Set<StandardJob> jobList = jobGroup.getJobs();
		if (jobList != null) {
			for (StandardJob standardJob : jobList) {
				StandardJob job = standardJobRepository.findByName(standardJob.getName());
				if (job == null) {
					// job = standardJobRepository.save(standardJob);
				} else {
					jobList.remove(standardJob);
					jobList.add(job);
				}

			}
		}
		Set<JobGroup<StandardJob>> jobGroups = jobGroup.getJobGroups();
		if (jobGroups != null) {
			for (JobGroup<StandardJob> jobGrou : jobGroups) {
				saveJobGroup(jobGrou);
			}
		}

	}

	@Override
	public void uploadPerson(Person person) {
		personRepository.save(person);
		
	}

}
