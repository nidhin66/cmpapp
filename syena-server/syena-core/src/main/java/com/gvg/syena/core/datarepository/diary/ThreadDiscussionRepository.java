package com.gvg.syena.core.datarepository.diary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussionId;

public interface ThreadDiscussionRepository extends  PagingAndSortingRepository<ThreadDiscussion,ThreadDiscussionId>{
	
	@Query("select count(*) from ThreadDiscussion th where th.threadDiscussionId.threadId= :threadId ")
	public int getDiscussionCount(@Param("threadId") long threadId);
	
	@Query("select th from ThreadDiscussion th where th.threadDiscussionId.threadId= :threadId ORDER BY th.threadDiscussionId.sequence ASC ")
	public Page<ThreadDiscussion> findDiscussions(@Param("threadId") long threadId,Pageable pageable);
}
