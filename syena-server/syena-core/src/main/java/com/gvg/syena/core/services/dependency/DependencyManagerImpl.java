package com.gvg.syena.core.services.dependency;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.services.dependency.api.DependencyAnalyzer;
import com.gvg.syena.core.services.dependency.api.DependencyManager;
import com.gvg.syena.core.services.schedule.HierarchyScheduler;
import com.gvg.syena.core.utilities.AppUtil;

public class DependencyManagerImpl<R extends PagingAndSortingRepository<T, Serializable>, T extends DependencyAttachable> implements DependencyManager<R> {

	private R repository;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private DependencyAnalyzer dependencyAnalyzer;

	@Autowired
	private HierarchyScheduler hierarchyScheduler;

	public DependencyManagerImpl(R repository, JobRepository jobRepository, JobGroupRepository jobGroupRepository, DependencyAnalyzer dependencyAnalyzer,
			HierarchyScheduler hierarchyScheduler) {
		this.repository = repository;
		this.jobRepository = jobRepository;
		this.jobGroupRepository = jobGroupRepository;
		this.dependencyAnalyzer = dependencyAnalyzer;
		this.hierarchyScheduler = hierarchyScheduler;
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addAsPredecessorJobs(DependencyAttachable parent, List<Job> predecessorJobs, DependencyType dependencyType) throws DependencyFailureException,
			JobScheduleFailed, InvalidDataException {

		if (this.dependencyAnalyzer != null) {
			dependencyAnalyzer.analyzeDependency(parent, predecessorJobs, dependencyType);
		}

		parent.getPredecessorDependency().addJobs(predecessorJobs, dependencyType);
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		for (Job job : predecessorJobs) {
			hierarchyScheduler.scheduleAsPredecessor(job, parent, changes);
			job.getSuccessorDependency().add(parent, dependencyType);
		}
		repository.save((T) parent);

		return predecessorJobs;
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addAsPredecessorJobGroups(DependencyAttachable parent, List<JobGroup<Job>> jobGroups, DependencyType dependencyType)
			throws DependencyFailureException, JobScheduleFailed, InvalidDataException {

		for (JobGroup jobGroup : jobGroups) {
			if (this.dependencyAnalyzer != null) {
				dependencyAnalyzer.analyzeDependency(parent, jobGroup, dependencyType);
			}
		}

		parent.getPredecessorDependency().addJobGroups(jobGroups, dependencyType);
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		for (JobGroup jobGroup : jobGroups) {
			hierarchyScheduler.scheduleAsPredecessor(jobGroup, parent, changes);
			jobGroup.getSuccessorDependency().add(parent, dependencyType);
		}
		repository.save((T) parent);
		return jobGroups;
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addAsSuccessorJobs(DependencyAttachable parent, List<Job> successorJobs, DependencyType dependencyType) throws DependencyFailureException, JobScheduleFailed,
			InvalidDataException {

		if (this.dependencyAnalyzer != null) {
			dependencyAnalyzer.analyzeDependency(parent, successorJobs, dependencyType.inverse(true));
		}

//		if (this.dependencyAnalyzer != null) {
//			for (Job successorJob : successorJobs) {
//				List<DependencyAttachableHierarchy> jobz = new ArrayList<>();
//				jobz.add((DependencyAttachableHierarchy) parent);
//				dependencyAnalyzer.analyzeDependency(successorJob, jobz, dependencyType.inverse(true));
//			}
//
//		}
		parent.getSuccessorDependency().addJobs(successorJobs, dependencyType);
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		for (Job job : successorJobs) {
			hierarchyScheduler.scheduleAsSuccessorr(job, parent, changes);
			job.getPredecessorDependency().add(parent, dependencyType);
		}
		repository.save((T) parent);

		return successorJobs;
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addAsSuccessorJobGroups(DependencyAttachable parent, List<JobGroup<Job>> successorJobGroups, DependencyType dependencyType)
			throws DependencyFailureException, JobScheduleFailed, InvalidDataException {

		for (JobGroup jobGroup : successorJobGroups) {
			if (this.dependencyAnalyzer != null) {
				dependencyAnalyzer.analyzeDependency(parent, jobGroup, dependencyType.inverse(true));
			}
		}

		parent.getSuccessorDependency().addJobGroups(successorJobGroups, dependencyType);
		Set<ScheduleLog> changes = new TreeSet<ScheduleLog>();
		for (JobGroup jobGroup : successorJobGroups) {

			hierarchyScheduler.scheduleAsSuccessorr(jobGroup, parent, changes);
			jobGroup.getPredecessorDependency().add(parent, dependencyType);
		}
		repository.save((T) parent);
		return successorJobGroups;
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addPredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType) throws JobNotFoundException, DependencyFailureException,
			JobScheduleFailed, InvalidDataException {

		T parent = repository.findOne(parentid);
		if (parent == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : predecessorJobIds) {
			ids.add(id);
		}
		List<Job> predecessorJobs = jobRepository.findByIdIn(ids);
		return addAsPredecessorJobs(parent, predecessorJobs, dependencyType);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addPredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException,
			DependencyFailureException, JobScheduleFailed, InvalidDataException {

		T parent = repository.findOne(parentid);
		if (parent == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : predecessorJobGroupIds) {
			ids.add(id);
		}
		List<JobGroup<Job>> jobGroups = jobGroupRepository.findByIdIn(ids);

		return addAsPredecessorJobGroups(parent, jobGroups, dependencyType);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType) throws JobNotFoundException, DependencyFailureException,
			JobScheduleFailed, InvalidDataException {

		T parent = repository.findOne(parentid);
		if (parent == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : successoJobIds) {
			ids.add(id);
		}
		List<Job> successorJobs = jobRepository.findByIdIn(ids);

		return addAsSuccessorJobs(parent, successorJobs, dependencyType);

	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException,
			DependencyFailureException, JobScheduleFailed, InvalidDataException {

		T parent = repository.findOne(parentid);
		if (parent == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : successorJobGroupIds) {
			ids.add(id);
		}
		List<JobGroup<Job>> successorJobGroups = jobGroupRepository.findByIdIn(ids);

		return addAsSuccessorJobGroups(parent, successorJobGroups, dependencyType);
	}

	@Override
	@Transactional
	public void removeAsPredecessorJobs(DependencyAttachable parent, List<Job> successorJobs, DependencyType dependencyType) {

		parent.getPredecessorDependency().removeJobs(successorJobs, dependencyType);

		for (Job job : successorJobs) {
			// TODO remove type casting
			job.getSuccessorDependency().remove(parent, dependencyType);
		}
		repository.save((T) parent);

	}

	@Override
	@Transactional
	public void removeAsPredecessorJobGroups(DependencyAttachable parent, List<JobGroup<Job>> predecessorJobGroups, DependencyType dependencyType) {

		parent.getSuccessorDependency().removeJobGroups(predecessorJobGroups, dependencyType);

		for (JobGroup jobGroup : predecessorJobGroups) {
			// TODO remove type casting
			jobGroup.getPredecessorDependency().remove(parent, dependencyType);
		}
		repository.save((T) parent);
	}

	@Override
	@Transactional
	public void removeAsSuccessorJobs(DependencyAttachable parent, List<Job> successorJobs, DependencyType dependencyType) {

		parent.getSuccessorDependency().removeJobs(successorJobs, dependencyType);

		for (Job job : successorJobs) {
			// TODO remove type casting
			job.getPredecessorDependency().remove(parent, dependencyType);
		}
		repository.save((T) parent);

	}

	@Override
	@Transactional
	public void removeAsSuccessorJobGroups(DependencyAttachable parent, List<JobGroup<Job>> predecessorJobGroups, DependencyType dependencyType) {

		parent.getSuccessorDependency().removeJobGroups(predecessorJobGroups, dependencyType);

		for (JobGroup jobGroup : predecessorJobGroups) {
			// TODO remove type casting
			jobGroup.getPredecessorDependency().remove(parent, dependencyType);
		}
		repository.save((T) parent);

	}

	@Override
	@Transactional
	public void removePredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType) throws JobNotFoundException {

		T parentJob = repository.findOne(parentid);
		if (parentJob == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : predecessorJobIds) {
			ids.add(id);
		}
		List<Job> jobs = jobRepository.findByIdIn(ids);

		removeAsPredecessorJobs(parentJob, jobs, dependencyType);

	}

	@Override
	@Transactional
	public void removePredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException {
		T parentJob = repository.findOne(parentid);
		if (parentJob == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : predecessorJobGroupIds) {
			ids.add(id);
		}
		List<JobGroup<Job>> jobGroups = jobGroupRepository.findByIdIn(ids);

		removeAsPredecessorJobGroups(parentJob, jobGroups, dependencyType);
	}

	@Override
	@Transactional
	public void removeSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType) throws JobNotFoundException {

		T parent = repository.findOne(parentid);

		if (parent == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : successoJobIds) {
			ids.add(id);
		}

		List<Job> successorJobs = jobRepository.findByIdIn(ids);

		removeAsSuccessorJobs(parent, successorJobs, dependencyType);

	}

	@Override
	@Transactional
	public void removeSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType) throws JobNotFoundException {
		T parent = repository.findOne(parentid);
		if (parent == null) {
			throw new JobNotFoundException(parentid, JobNotFoundException.JOB, "Job Not Found");
		}

		List<Long> ids = new ArrayList<Long>();
		for (long id : successorJobGroupIds) {
			ids.add(id);
		}

		List<JobGroup<Job>> predecessorJobGroups = jobGroupRepository.findByIdIn(ids);

		removeAsSuccessorJobGroups(parent, predecessorJobGroups, dependencyType);
	}

	@Override
	@Transactional
	public PageItr<Job> getSuccessorJobs(long id, int pageNumber, int pageSize) throws JobNotFoundException {

		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}
		// TODO implementation limited to FinishForStart, pagination not
		// Implemented
		Set<Job> jobs = jobAttachable.getSuccessorDependency().getFinishToStart().getJobs();
		List<Long> ids = new ArrayList<Long>();
		for (Job job : jobs) {
			if (job.getNodeType() == NodeType.instance) {
				ids.add(job.getId());
			}
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		if (ids.size() > 0) {
			Page<Job> page = jobRepository.findByIds(ids, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<Job>(new ArrayList<>(0), 0, 0);
		}

	}

	@Override
	@Transactional
	public PageItr<Job> getPredecessorJobs(long id, int pageNumber, int pageSize) throws JobNotFoundException {
		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}
		Set<Job> jobs = jobAttachable.getPredecessorDependency().getFinishToStart().getJobs();
		List<Long> ids = new ArrayList<>();
		for (Job job : jobs) {
			if (job.getNodeType() == NodeType.instance) {
				ids.add(job.getId());
			}
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		// return jobAttachable.getSuccessorDependency();
		if (ids.size() > 0) {
			Page<Job> page = jobRepository.findByIds(ids, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<Job>(new ArrayList<>(0), 0, 0);
		}
	}

	@Override
	@Transactional
	public PageItr<JobGroup> getSuccessorJobGroups(long id, int pageNumber, int pageSize) throws JobNotFoundException {

		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}

		// TODO implementation limited to FinishForStart, pagination not
		// Implemented
		Set<JobGroup<Job>> jobs = jobAttachable.getSuccessorDependency().getFinishToStart().getJobGroups();
		List<Long> ids = new ArrayList<Long>();
		for (JobGroup job : jobs) {
			if (job.getNodeType() == NodeType.instance) {
				ids.add(job.getId());
			}
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		if (ids.size() > 0) {
			Page<JobGroup> page = jobGroupRepository.findByIdIn(ids, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<JobGroup>(new ArrayList<>(0), 0, 0);
		}

	}

	@Override
	@Transactional
	public PageItr<JobGroup> getPredecessorJobGroups(long id, int pageNumber, int pageSize) throws JobNotFoundException {

		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}
		// TODO implementation limited to FinishForStart, pagination not
		// Implemented
		Set<JobGroup<Job>> jobs = jobAttachable.getPredecessorDependency().getFinishToStart().getJobGroups();
		List<Long> ids = new ArrayList<Long>();
		for (JobGroup job : jobs) {
			ids.add(job.getId());
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		if (ids.size() > 0) {
			Page<JobGroup> page = jobGroupRepository.findByIdIn(ids, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<JobGroup>(new ArrayList<>(0), 0, 0);
		}

	}

	@Override
	public PageItr<Job> searchSuccessorJobs(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException {
		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}

		Set<Job> jobs = jobAttachable.getSuccessorDependency().getFinishToStart().getJobs();
		List<Long> ids = new ArrayList<Long>();
		for (Job job : jobs) {
			ids.add(job.getId());
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		// return jobAttachable.getSuccessorDependency();
		if (ids.size() > 0) {
			Page<Job> page = jobRepository.findByIdInAndJobNameLike(ids, name, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<Job>(new ArrayList<>(0), 0, 0);
		}

	}

	@Override
	@Transactional
	public PageItr<JobGroup> searchSuccessorJobGroups(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException {

		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}
		// TODO implementation limited to FinishForStart, pagination not
		// Implemented
		Set<JobGroup<Job>> jobs = jobAttachable.getSuccessorDependency().getFinishToStart().getJobGroups();
		List<Long> ids = new ArrayList<Long>();
		for (JobGroup job : jobs) {
			ids.add(job.getId());
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		if (ids.size() > 0) {
			Page<JobGroup> page = jobGroupRepository.findByIdInAndNameLike(ids, name, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<JobGroup>(new ArrayList<>(0), 0, 0);
		}

	}

	@Override
	@Transactional
	public PageItr<Job> searchPredecessorJobs(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException {
		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}
		Set<Job> jobs = jobAttachable.getPredecessorDependency().getFinishToStart().getJobs();
		List<Long> ids = new ArrayList<>();
		for (Job job : jobs) {
			ids.add(job.getId());
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		// return jobAttachable.getSuccessorDependency();
		if (ids.size() > 0) {
			Page<Job> page = jobRepository.findByIdInAndJobNameLike(ids, name, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<Job>(new ArrayList<>(0), 0, 0);
		}
	}

	@Override
	@Transactional
	public PageItr<JobGroup> searchPredecessorJobGroups(long id, String name, int pageNumber, int pageSize) throws JobNotFoundException {

		DependencyAttachable jobAttachable = this.repository.findOne(id);
		if (jobAttachable == null) {
			throw new JobNotFoundException(id, JobNotFoundException.JOB, "Job Not Found");
		}
		// TODO implementation limited to FinishForStart, pagination not
		// Implemented
		Set<JobGroup<Job>> jobs = jobAttachable.getPredecessorDependency().getFinishToStart().getJobGroups();
		List<Long> ids = new ArrayList<Long>();
		for (JobGroup job : jobs) {
			ids.add(job.getId());
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		if (ids.size() > 0) {
			Page<JobGroup> page = jobGroupRepository.findByIdInAndNameLike(ids, name, pageable);
			return AppUtil.convert(page);
		} else {
			return new PageItr<JobGroup>(new ArrayList<>(0), 0, 0);
		}

	}

	@Override
	@Transactional
	public void setDependencyAnalyzer(DependencyAnalyzer dependencyAnalyzer) {
		this.dependencyAnalyzer = dependencyAnalyzer;
	}

}
