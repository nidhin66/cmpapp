package com.gvg.syena.core.services.schedule.exception;

import java.util.Date;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.JobScheduleFailed;

public class JobScheduleTimeLimit extends JobScheduleFailed {

	private HierarchyNode connectedNode;
	private Job job;
	private Date date;
	private JobGroup<Job> jobGroup;

	public JobScheduleTimeLimit(HierarchyNode connectedNode, Job job, Date date, String messageKey, String messageString) {
		super(job, messageKey, messageString);
		this.connectedNode = connectedNode;
		this.job = job;
		this.date = date;
	}

	public JobScheduleTimeLimit(HierarchyNode connectedNode, JobGroup<Job> jobGroup, Date date, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.connectedNode = connectedNode;
		this.jobGroup = jobGroup;
		this.date = date;
	}

}
