package com.gvg.syena.core.services.dataupload;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;

public class HierarchyNodeReaderExcel {

	private XSSFSheet sheetH;
	private String[] propertyHeaders;
	private XSSFSheet sheetD;

	HierarchyNodeReaderExcel(InputStream inputStream) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		this.sheetH = wb.getSheetAt(0);
		this.sheetD = wb.getSheetAt(1);
	}

	public StandardHierarchyNode read() {

		ArrayList<HierarchyLevel> hierarchyLevels = readHierarchyLevels();
		propertyHeaders = getPropertyHeaders(hierarchyLevels.size() + 1);
		StandardHierarchyNode<StandardHierarchyNode> hierarchyNode = readHierarchyNodes(hierarchyLevels, hierarchyLevels.size());
		hierarchyNode.asString();
		return hierarchyNode;
	}

	private String[] getPropertyHeaders(int initC) {
		XSSFRow row0 = sheetD.getRow(0);
		int numCell = row0.getPhysicalNumberOfCells();
		String[] propertyHeaderList = new String[numCell - initC + 1];
		for (int j = initC; j <= numCell; j++) {
			XSSFCell cell1 = row0.getCell(j);
			if (cell1 != null) {
				String value = cell1.getStringCellValue();
				propertyHeaderList[j - initC] = value;
			}
		}
		return propertyHeaderList;
	}

	public ArrayList<HierarchyLevel> readHierarchyLevels() {

		ArrayList<HierarchyLevel> hierarchyLevels = new ArrayList<HierarchyLevel>();
		ArrayList<HierarchyLevel> parents = new ArrayList<HierarchyLevel>();
		int numRow = sheetH.getPhysicalNumberOfRows();
		for (int i = 0; i < numRow; i++) {
			XSSFRow row0 = sheetH.getRow(i);
			int numCell = row0.getPhysicalNumberOfCells();
			for (int j = 0; j < numCell; j++) {
				XSSFCell cell1 = row0.getCell(j);
				String value = cell1.getStringCellValue();
				if (!(value == null || "".equals(value))) {
					HierarchyLevel level1 = new HierarchyLevel(value);
					hierarchyLevels.add(level1);
					if (j > 0) {
						HierarchyLevel parent = parents.get(j - 1);
						parent.addChild(level1);
					}
					parents.add(j, level1);
					break;
				}
			}
		}
		return hierarchyLevels;
	}

	private StandardHierarchyNode<StandardHierarchyNode> readHierarchyNodes(ArrayList<HierarchyLevel> hierarchyLevels, int colNum) {
		int numRow = sheetD.getPhysicalNumberOfRows();
		Map<HierarchyLevel, StandardHierarchyNode> parents = new HashMap<HierarchyLevel, StandardHierarchyNode>();
		StandardHierarchyNode topParent = null;
		for (int i = 1; i < numRow; i++) {
			XSSFRow row0 = sheetD.getRow(i);
			for (int j = 0; j < colNum; j++) {
				XSSFCell cell1 = row0.getCell(j);
				if (cell1 != null) {
					String value = cell1.getStringCellValue();
					if (value != null && !"".equals(value)) {
						Map<String, Object> properties = getProperties(i, colNum + 1, colNum + propertyHeaders.length);
						String description = (String) properties.get("description");
						HierarchyLevel hierarchyLevel = hierarchyLevels.get(j);
						StandardHierarchyNode hierarchyNode = new StandardHierarchyNode(value, description, hierarchyLevel);
						if (topParent == null) {
							topParent = hierarchyNode;
						}
						parents.put(hierarchyLevel, hierarchyNode);
						// System.out.print(hierarchyNode.getName());
						int parentCellId = cell1.getColumnIndex() - 1;
						if (parentCellId >= 0) {
							StandardHierarchyNode parent = parents.get(hierarchyLevels.get(parentCellId));
							parent.addChild(hierarchyNode);
							// System.out.println("->"+parent.getName());
						}

						break;
					}
				}
			}
		}
		return topParent;

	}

	private Map<String, Object> getProperties(int i, int colStart, int colEnd) {
		XSSFRow row0 = sheetD.getRow(i);
		Map<String, Object> properties = new HashMap<String, Object>();
		for (int j = colStart; j <= colEnd; j++) {
			String propertyHeader = propertyHeaders[j - colStart];
			XSSFCell cell = row0.getCell(j);
			String value = null;
			if (cell != null) {
				value = cell.getStringCellValue();
			}
			properties.put(propertyHeader, value);
		}
		return properties;
	}

	public static void main(String[] args) throws IOException {
		InputStream in = HierarchyNodeReaderExcel.class.getResourceAsStream("/MappedData_DataUpLoad1.xlsx");
		HierarchyNodeReaderExcel dataExcel = new HierarchyNodeReaderExcel(in);
		dataExcel.read();

	}
}
