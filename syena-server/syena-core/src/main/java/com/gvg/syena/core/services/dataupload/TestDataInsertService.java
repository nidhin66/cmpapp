package com.gvg.syena.core.services.dataupload;

public interface TestDataInsertService {

	void insertData(int plant, int unit, int system, int substem, int loopEqu);

	void addJob(String levelName, int pageNumber, int num, String id);

}
