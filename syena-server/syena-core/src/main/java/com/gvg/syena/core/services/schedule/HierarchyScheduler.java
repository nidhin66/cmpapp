package com.gvg.syena.core.services.schedule;

import java.util.Date;
import java.util.Set;
import java.util.SortedSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.calendar.WorkCalendar;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.services.StatusUpdationService;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.services.schedule.exception.JobScheduleTimeLimit;

@Service("hierarchyScheduler")
public class HierarchyScheduler {

	@Autowired
	private JobScheduler jobScheduler;
	

	@Autowired
	private StatusUpdationService statusUpdationService;

	@Transactional(rollbackOn = JobScheduleFailed.class)
	public void schedule(Job job, Date startTime, Date finishTime,
			Set<ScheduleLog> changes) throws JobScheduleFailed,
			InvalidDataException {
		if (startTime == null || finishTime == null) {
			throw new InvalidDataException(MessageKeys.SCHEDULE_TIME_NOT_NULL,
					"startTime and finishTime should not be null");
		}
		scheduleJob(job, startTime, finishTime, true, changes);
		scheduleTimeUpdateUp(job.getConnectedNode(), startTime, finishTime);
	}

	@Transactional(rollbackOn = JobScheduleFailed.class)
	public void schedule(JobGroup<Job> jobGroup, Date startTime,
			Date finishTime, Set<ScheduleLog> changes)
			throws JobScheduleFailed, InvalidDataException {
		if (startTime == null || finishTime == null) {
			throw new InvalidDataException(MessageKeys.SCHEDULE_TIME_NOT_NULL,
					"startTime and finishTime should not be null");
		}
		jobGroup.getWorkTime().setPlanedStartTime(startTime);
		jobGroup.getWorkTime().setPlanedFinishTime(finishTime);
		scheduleTimeUpdateDown(jobGroup.getJobGroups(), startTime, finishTime);
		scheduleTimeUpdateUp(jobGroup.getConnectedNode(), startTime, finishTime);
		scheduleJobGroup(jobGroup, startTime, finishTime, false, changes);
	}

	@Transactional(rollbackOn = JobScheduleFailed.class)
	public void schedule(HierarchyNode hierarchyNode, Date startTime,
			Date finishTime, Set<ScheduleLog> changes)
			throws JobScheduleFailed, InvalidDataException {
		if (startTime == null || finishTime == null) {
			throw new InvalidDataException(MessageKeys.SCHEDULE_TIME_NOT_NULL,
					"startTime and finishTime should not be null");
		}
		hierarchyNode.getWorkTime().setPlanedStartTime(startTime);
		hierarchyNode.getWorkTime().setPlanedFinishTime(finishTime);
		scheduleJobs(hierarchyNode, startTime, finishTime, false, changes);
		scheduleTimeUpdateDown(hierarchyNode, startTime, finishTime);
		scheduleTimeUpdateUp(hierarchyNode, startTime, finishTime);

	}

	private void scheduleTimeUpdateDown(HierarchyNode hierarchyNode,
			Date startTime, Date endTime) throws JobScheduleTimeLimit {

		hierarchyNode.getWorkTime().setPlanedStartTime(startTime);
		hierarchyNode.getWorkTime().setPlanedFinishTime(endTime);
		for (HierarchyNode child : hierarchyNode.getChildren()) {
			if (child != null) {
				scheduleTimeUpdateDown(child, startTime, endTime);
			}
		}
		scheduleTimeUpdateDown(hierarchyNode.getJobGroups(), startTime, endTime);
	}

	private void scheduleTimeUpdateDown(Set<JobGroup<Job>> jobGroups,
			Date startTime, Date finishTime) {
		for (JobGroup jobGroup : jobGroups) {
			jobGroup.getWorkTime().setPlanedStartTime(startTime);
			jobGroup.getWorkTime().setPlanedFinishTime(finishTime);
			scheduleTimeUpdateDown(jobGroup.getJobGroups(), startTime,
					finishTime);
		}
	}

	private void scheduleTimeUpdateUp(StandardHierarchyNode hierarchyNode,
			Date startTime, Date endTime) {
		if (hierarchyNode != null) {
			hierarchyNode.getWorkTime().outer(startTime, endTime);
			StandardHierarchyNode parent = hierarchyNode.getParent();
			scheduleTimeUpdateUp(parent, startTime, endTime);
		}
	}

	private void scheduleJobs(HierarchyNode hierarchyNode, Date startTime,
			Date endTime, boolean isExact, Set<ScheduleLog> changes)
			throws JobScheduleFailed {

		scheduleJobGroup(hierarchyNode.getDefaultJobGroup(), startTime,
				endTime, isExact, changes);
		scheduleDependency(hierarchyNode.getPredecessorDependency(), startTime,
				endTime, isExact, changes);
		scheduleDependency(hierarchyNode.getSuccessorDependency(), startTime,
				endTime, isExact, changes);

		for (HierarchyNode child : hierarchyNode.getChildren()) {
			scheduleJobs(child, startTime, endTime, isExact, changes);
		}

	}

	private void scheduleDependency(DependencyJobs dependencyJobs,
			Date startTime, Date endTime, boolean isExact,
			Set<ScheduleLog> changes) throws JobScheduleFailed {
		// scheduleJobGroup(dependencyJobs.getStartToStart(), startTime,
		// endTime, isExact, changes);
		// scheduleJobGroup(dependencyJobs.getStartToFinish(), startTime,
		// endTime, isExact, changes);
		scheduleJobGroup(dependencyJobs.getFinishToStart(), startTime, endTime,
				isExact, changes);
		// scheduleJobGroup(dependencyJobs.getFinishToFinish(), startTime,
		// endTime, isExact, changes);
	}

	private Date scheduleJobGroup(JobGroup<Job> jobGroups, Date startTime,
			Date endTime, boolean isExact, Set<ScheduleLog> changes)
			throws JobScheduleFailed {

		Date lastDate = schedulePredecessorJobGroup(jobGroups, startTime,
				isExact, changes);

		SortedSet<ScheduledStage> scheduledStages = jobGroups.getWorkSchedule()
				.getScheduledStages();
		if (scheduledStages.size() <= 0) {
			WorkSchedule workSchedule = new WorkSchedule(
					jobGroups.getWorkTime());
			jobGroups.setWorkSchedule(workSchedule);
		}

		jobGroups.getWorkSchedule().getScheduledStages().first()
				.setPlannedTime(lastDate);

		lastDate = scheduleJobs(jobGroups.getJobs(), lastDate, endTime,
				isExact, changes);

		for (JobGroup<Job> inJobGroup : jobGroups.getJobGroups()) {
			Date date = scheduleJobGroup(inJobGroup, jobGroups
					.getWorkSchedule().getPlannedStartTime(), endTime, isExact,
					changes);
			lastDate = DateUtil.after(date, lastDate) ? date : lastDate;
		}

		jobGroups.getWorkSchedule().getScheduledStages().last()
				.setPlannedTime(lastDate);

		scheduleSuccessorJobGroup(jobGroups, lastDate, isExact, changes);

		return lastDate;

		// DependencyJobs predecessorDependency =
		// jobGroups.getPredecessorDependency();
		// Set<JobGroup<Job>> prefinishToStart =
		// predecessorDependency.getFinishToStart().getJobGroups();
		// schedulePreJobGroup(prefinishToStart, startTime, false, changes);
		//
		// /*---------------*/
		//
		// /*---------------*/
		//
		// DependencyJobs successorDependency =
		// jobGroups.getSuccessorDependency();
		// Set<JobGroup<Job>> susinishToStart =
		// successorDependency.getFinishToStart().getJobGroups();
		// scheduleSuccJobGroup(susinishToStart, endTime, isExact, changes);

	}

	private Date schedulePredecessorJobGroup(JobGroup<Job> jobGroups,
			Date date, boolean isExact, Set<ScheduleLog> changes)
			throws JobScheduleFailed {
		DependencyJobs predecessorDependency = jobGroups
				.getPredecessorDependency();
		JobGroup<Job> finishToStart = predecessorDependency.getFinishToStart();
		Set<JobGroup<Job>> fsjobGroups = finishToStart.getJobGroups();
		Date lastDate = date;
		for (JobGroup<Job> fsjobGroup : fsjobGroups) {

			WorkCalendar calender = fsjobGroup.getTotalEffert().getCalender();
			if (jobGroups.getWorkSchedule().getPlannedFinishTime() == null
					|| !calender.beforeOrOn(jobGroups.getWorkSchedule()
							.getPlannedFinishTime(), date)) {

				Date ndate = schedulePredecessorJobGroup(fsjobGroup, date,
						isExact, changes);

				ndate = scheduleJobs(fsjobGroup.getJobs(), date, null, isExact,
						changes);

				HierarchyNode connectedNode = fsjobGroup.getConnectedNode();

				if (!connectedNode.getWorkTime()
						.isInBetweenStartDateAndEndDate(ndate)) {
					throw new JobScheduleTimeLimit(
							connectedNode,
							fsjobGroup,
							date,
							MessageKeys.SCHEDULE_TIME_NOT_IN_BETWEEN,
							date
									+ " not in between of start date and end date of "
									+ connectedNode.getName());
				}
				fsjobGroup.getWorkSchedule().getScheduledStages().first()
						.setPlannedTime(date);
				// Date endTime =
				// jobGroups.getTotalEffert().calculateEndDate(date);
				fsjobGroup.getWorkSchedule().getScheduledStages().last()
						.setPlannedTime(ndate);
				lastDate = DateUtil.after(ndate, lastDate) ? ndate : lastDate;
			}
		}
		return lastDate;
	}

	private void scheduleSuccessorJobGroup(JobGroup<Job> jobGroups,
			Date startTime, boolean isExact, Set<ScheduleLog> changes)
			throws JobScheduleFailed {
		DependencyJobs successorDependency = jobGroups.getSuccessorDependency();
		JobGroup<Job> finishToStart = successorDependency.getFinishForStart();
		Set<JobGroup<Job>> fsjobGroups = finishToStart.getJobGroups();
		for (JobGroup<Job> fsjobGroup : fsjobGroups) {
			WorkCalendar calender = fsjobGroup.getTotalEffert().getCalender();
			if (jobGroups.getWorkSchedule().getPlannedFinishTime() == null
					|| calender.beforeOrOn(jobGroups.getWorkSchedule()
							.getPlannedFinishTime(), startTime)) {

				fsjobGroup.getWorkSchedule().getScheduledStages().first()
						.setPlannedTime((Date) startTime.clone());

				Date ndate = scheduleJobs(fsjobGroup.getJobs(), fsjobGroup
						.getWorkSchedule().getPlannedStartTime(), null,
						isExact, changes);

				// Date endTime =
				// fsjobGroup.getTotalEffert().calculateEndDate(startTime);
				HierarchyNode connectedNode = fsjobGroup.getConnectedNode();
				if (!connectedNode.getWorkTime()
						.isInBetweenStartDateAndEndDate(startTime)
						|| !connectedNode.getWorkTime()
								.isInBetweenStartDateAndEndDate(ndate)) {
					throw new JobScheduleTimeLimit(
							connectedNode,
							fsjobGroup,
							startTime,
							MessageKeys.SCHEDULE_TIME_NOT_IN_BETWEEN,
							startTime
									+ " not in between of start date and end date of "
									+ connectedNode.getName());
				}

				fsjobGroup.getWorkSchedule().getScheduledStages().last()
						.setPlannedTime(ndate);

				scheduleSuccessorJobGroup(fsjobGroup, fsjobGroup
						.getWorkSchedule().getPlannedFinishTime(), isExact,
						changes);
			}
		}

	}

	private void scheduleSuccJobGroup(Set<JobGroup<Job>> susinishToStart,
			Date startTime, boolean isExact, Set<ScheduleLog> changes)
			throws JobScheduleFailed {
		if (susinishToStart != null) {
			for (JobGroup<Job> jobGroups : susinishToStart) {
				Date endTime = jobGroups.getTotalEffert().calculateEndDate(
						startTime);

				jobGroups.getWorkSchedule().getScheduledStages().first()
						.setPlannedTime(startTime);
				jobGroups.getWorkSchedule().getScheduledStages().last()
						.setPlannedTime(endTime);

				Set<Job> prejobs = jobGroups.getJobs();
				for (Job job : prejobs) {
					WorkCalendar calender = job.getTotalEffert().getCalender();
					scheduleJob(job, startTime, endTime, false, changes);
				}

			}
		}

	}

	private void schedulePreJobGroup(Set<JobGroup<Job>> prefinishToStart2,
			Date endTime, boolean isExact, Set<ScheduleLog> changes)
			throws JobScheduleFailed {
		if (prefinishToStart2 != null) {
			for (JobGroup<Job> jobGroups : prefinishToStart2) {
				Date startTime = jobGroups.getTotalEffert().calculateStatDate(
						endTime);
				jobGroups.getWorkSchedule().getScheduledStages().first()
						.setPlannedTime(startTime);
				jobGroups.getWorkSchedule().getScheduledStages().last()
						.setPlannedTime(endTime);

				Set<Job> prejobs = jobGroups.getJobs();
				for (Job job : prejobs) {
					WorkCalendar calender = job.getTotalEffert().getCalender();
					scheduleJob(job, startTime, endTime, false, changes);
				}

			}
		}

	}

	private Date scheduleJobs(Set<Job> jobs, Date startTime, Date endTime,
			boolean isExact, Set<ScheduleLog> changes) throws JobScheduleFailed {
		Date lastDate = startTime;
		for (Job job : jobs) {
			if (job.getWorkSchedule().getScheduledStages().size() <= 0) {
				job.getWorkSchedule().setScheduledStages(
						ApplicationUtil.getSystemDefaultScheduledStages());
			}
			scheduleJob(job, startTime, endTime, isExact, changes);
			statusUpdationService.computeUpwardStatus(job);
			lastDate = DateUtil.after(job.getPalnnedFinishTime(), lastDate) ? job
					.getPalnnedFinishTime() : lastDate;
		}
		return lastDate;
	}

	private void scheduleJob(Job job, Date startTime, Date endTime,
			boolean isExact, Set<ScheduleLog> changes) throws JobScheduleFailed {

		WorkCalendar calender = job.getDuration().getCalender();

		Date exStartTime = job.getPalnnedStartTime();
		boolean completed = false;
		if (exStartTime == null) {
			jobScheduler.changeStartDate(job, startTime, changes);
			completed = true;
		}
		Date exFinishTime = job.getPalnnedFinishTime();
		if (exFinishTime == null) {
			jobScheduler.changeFinishDate(job, endTime, changes);
			completed = true;
		}
		if (completed) {
			return;
		}
		if (isExact) {
			if ((!DateUtil.equals(exStartTime, startTime))) {
				jobScheduler.changeStartDate(job, startTime, changes);
			}
			if ((!DateUtil.equals(exFinishTime, endTime))) {
				jobScheduler.changeFinishDate(job, endTime, changes);
			}
		} else {

			if (calender.OnOrAfter(exStartTime, startTime)
					&& calender.beforeOrOn(exFinishTime, endTime) && !isExact) {
				return;
			} else {
				if ((!calender.OnOrAfter(exStartTime, startTime))
						|| (!calender.beforeOrOn(exStartTime, endTime))) {
					jobScheduler.changeStartDate(job, startTime, changes);
					Date newEndTime = job.getDuration().calculateEndDate(
							startTime);
					jobScheduler.changeFinishDate(job, newEndTime, changes);
					return;
				}
				if ((!calender.OnOrAfter(exFinishTime, startTime))
						|| (!calender.beforeOrOn(exFinishTime, endTime))) {
					jobScheduler.changeFinishDate(job, endTime, changes);
					Date newEndTime = job.getDuration().calculateStatDate(
							endTime);
					jobScheduler.changeStartDate(job, newEndTime, changes);
					return;
				}
			}
		}

	}

	public void scheduleAsPredecessor(Job job, DependencyAttachable parent,
			Set<ScheduleLog> changes) throws JobScheduleFailed,
			InvalidDataException {

		Date finishTime = ApplicationUtil.getStartTime(parent);
		Date startTime = ApplicationUtil.calculateStartTime(finishTime,
				job.getDuration());
		if (finishTime != null && startTime != null) {
			schedule(job, startTime, finishTime, changes);
		}
	}

	public void scheduleAsPredecessor(JobGroup jobGroup,
			DependencyAttachable parent, Set<ScheduleLog> changes)
			throws JobScheduleFailed, InvalidDataException {
		Date finishTime = ApplicationUtil.getStartTime(parent);
		Date startTime = ApplicationUtil.calculateStartTime(finishTime,
				jobGroup.getDuration());
		if (finishTime != null && startTime != null) {
			schedule(jobGroup, startTime, finishTime, changes);
		}

	}

	public void scheduleAsSuccessorr(Job job, DependencyAttachable parent,
			Set<ScheduleLog> changes) throws JobScheduleFailed,
			InvalidDataException {

		Date startTime = ApplicationUtil.getFinishTime(parent);
		Date finishTime = ApplicationUtil.calculateFinishTime(startTime,
				job.getDuration());
		if (finishTime != null && startTime != null) {
			schedule(job, startTime, finishTime, changes);
		}

	}

	public void scheduleAsSuccessorr(JobGroup jobGroup,
			DependencyAttachable parent, Set<ScheduleLog> changes)
			throws JobScheduleFailed, InvalidDataException {
		Date startTime = ApplicationUtil.getFinishTime(parent);
		Date finishTime = ApplicationUtil.calculateFinishTime(startTime,
				jobGroup.getDuration());
		if (finishTime != null && startTime != null) {
			schedule(jobGroup, startTime, finishTime, changes);
		}

	}
}
