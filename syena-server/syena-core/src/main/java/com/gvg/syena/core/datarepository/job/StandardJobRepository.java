package com.gvg.syena.core.datarepository.job;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.StandardJob;

public interface StandardJobRepository extends PagingAndSortingRepository<StandardJob, Long> {

	@Query("SELECT s FROM StandardJob s WHERE TYPE(s) = StandardJob and s.name = :name")
	StandardJob findByName(@Param("name") String name);

	List<StandardJob> findByIdIn(long[] ids);

	@Query("SELECT s FROM StandardJob s WHERE TYPE(s) = StandardJob and s.id like :id or upper(s.name) like upper(:name) or upper(s.description) like upper(:description)")
	Page<Job> findByIdOrNameLikeOrDescriptionLike(@Param("id")long id, @Param("name")String name, @Param("description")String description, Pageable pageable);

	@Override
	@Query("SELECT s FROM StandardJob s WHERE TYPE(s) = StandardJob")
	public Page<StandardJob> findAll(Pageable pageable);
}
