package com.gvg.syena.core.services.dependency;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.DependencyManagmnet;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.services.dependency.api.DependencyAnalyzer;
import com.gvg.syena.core.services.dependency.api.DependencyManager;
import com.gvg.syena.core.services.schedule.HierarchyScheduler;

@Service("dependencyManagmnet")
public class DependencyManagmnetImpl implements DependencyManagmnet {

	@Autowired(required = true)
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired(required = true)
	private JobGroupRepository jobGroupRepository;

	@Autowired(required = true)
	private JobRepository jobRepository;

	@Autowired(required = true)
	private DependencyAnalyzer dependencyAnalyzer;

	@Autowired(required = true)
	private HierarchyScheduler hierarchyScheduler;

	private DependencyManager<HierarchyNodeRepository> hierarchyNodeDependencyManager;

	private DependencyManager<JobGroupRepository> jobGroupDependencyManager;

	private DependencyManager<JobRepository> jobDependencyManager;

	public DependencyManagmnetImpl() {
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addPredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		if (NodeCategory.job == nodeType) {
			if (ApplicationUtil.contain(parentid, predecessorJobIds)) {
				throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "dependentTo and dependents are same");
			}
		}

		return getDependencyManagmner(nodeType).addPredecessorJobs(parentid, predecessorJobIds, dependencyType);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addPredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType)
			throws JobNotFoundException, DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		if (NodeCategory.jobgroup == nodeType) {
			if (ApplicationUtil.contain(parentid, predecessorJobGroupIds)) {
				throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "dependentTo and dependents are same");
			}
		}

		return getDependencyManagmner(nodeType).addPredecessorJobGroups(parentid, predecessorJobGroupIds, dependencyType);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {

		if (NodeCategory.jobgroup == nodeType) {
			if (ApplicationUtil.contain(parentid, successorJobGroupIds)) {
				throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "dependentTo and dependents are same");
			}
		}

		return getDependencyManagmner(nodeType).addSuccessorJobGroups(parentid, successorJobGroupIds, dependencyType);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addPredecessorJobs(Job job, Set<Job> predecessorJobs, DependencyType dependencyType, NodeCategory nodeCategory) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {

		List<Job> predecessorJobsList = ApplicationUtil.setToList(predecessorJobs);
		return getDependencyManagmner(nodeCategory).addAsPredecessorJobs(job, predecessorJobsList, dependencyType);

		// new ArrayList<Job>();
		// predecessorJobsList.addAll(predecessorJobs);
		// List<Job> jobs =
		// getDependencyManagmner(nodeCategory).addAsPredecessorJobs(job,
		// predecessorJobsList, dependencyType);
		// for (Job pJob : jobs) {
		// job.getWorkTime().setPlanedStartTime(job.getPalnnedFinishTime());
		// job.getWorkTime().enrich();
		//
		// Date startTime = job.getWorkTime().getPlanedStartTime();
		// Date finishTime = job.getWorkTime().getPlannedFinishTime();
		// if (startTime == null || finishTime == null) {
		// try {
		// hierarchyScheduler.schedule(pJob, startTime, finishTime);
		// } catch (InvalidDataException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// }
		// return jobs;

	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<JobGroup<Job>> addPredecessorJobGroups(Job parentJob, Set<JobGroup<Job>> predecessorJobGroups, DependencyType dependencyType, NodeCategory nodeCategory)
			throws DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {

		List<JobGroup<Job>> predecessorJobGroupsList = ApplicationUtil.setToList(predecessorJobGroups);
		return getDependencyManagmner(nodeCategory).addAsPredecessorJobGroups(parentJob, predecessorJobGroupsList, dependencyType);

		// List<JobGroup> predecessorJobGroupList = new ArrayList<JobGroup>();
		// predecessorJobGroupList.addAll(predecessorJobGroups);
		// List<JobGroup> jobGroups =
		// getDependencyManagmner(nodeCategory).addAsPredecessorJobGroups(job,
		// predecessorJobGroupList, dependencyType);
		// for (JobGroup pJobG : jobGroups) {
		// job.getWorkTime().setPlanedStartTime(job.getPalnnedFinishTime());
		// job.getWorkTime().enrich();
		// Date startTime = job.getWorkTime().getPlanedStartTime();
		// Date finishTime = job.getWorkTime().getPlannedFinishTime();
		// if (startTime == null || finishTime == null) {
		// try {
		// hierarchyScheduler.schedule(pJobG, startTime, finishTime);
		// } catch (InvalidDataException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// }
		// return jobGroups;
	}

	private DependencyManager<? extends CrudRepository> getDependencyManagmner(NodeCategory nodeType) throws DependencyManagerNotFoundException {

		this.hierarchyNodeDependencyManager = new DependencyManagerImpl(hierarchyNodeRepository, jobRepository, jobGroupRepository, dependencyAnalyzer, hierarchyScheduler);
		this.jobGroupDependencyManager = new DependencyManagerImpl(jobGroupRepository, jobRepository, jobGroupRepository, dependencyAnalyzer, hierarchyScheduler);
		this.jobDependencyManager = new DependencyManagerImpl(jobRepository, jobRepository, jobGroupRepository, dependencyAnalyzer, hierarchyScheduler);
		// this.jobDependencyManager.setDependencyAnalyzer(new
		// DependencyAnalyzerImpl());
		DependencyManager<? extends CrudRepository> dependencyManager = null;
		if (NodeCategory.hierarchy == nodeType) {
			dependencyManager = hierarchyNodeDependencyManager;
		} else if (NodeCategory.jobgroup == nodeType) {
			dependencyManager = jobGroupDependencyManager;
		} else if (nodeType.job == nodeType) {
			dependencyManager = jobDependencyManager;
		} else {
			throw new DependencyManagerNotFoundException(nodeType, " DependencyManagerNotFound", " DependencyManagerNotFound");
		}
		return dependencyManager;

	}

	@Override
	@Transactional
	public PageItr<Job> getSuccessorJobs(long id, NodeCategory nodeType, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeType).getSuccessorJobs(id, pageNumber, pageSize);
	}

	@Override
	@Transactional
	public PageItr<Job> getPredecessorJobs(long id, NodeCategory nodeType, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeType).getPredecessorJobs(id, pageNumber, pageSize);
	}

	@Override
	@Transactional(rollbackOn = { DependencyFailureException.class, JobScheduleFailed.class, InvalidDataException.class })
	public List<Job> addSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException {
		if (NodeCategory.job == nodeType) {
			if (ApplicationUtil.contain(parentid, successoJobIds)) {
				throw new DependencyFailureException(MessageKeys.DEPENDENCY_SAME_NODE, "dependentTo and dependents are same");
			}
		}
		return getDependencyManagmner(nodeType).addSuccessorJobs(parentid, successoJobIds, dependencyType);
	}

	@Override
	@Transactional
	public void removeSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		getDependencyManagmner(nodeType).removeSuccessorJobs(parentid, successoJobIds, dependencyType);

	}

	@Override
	@Transactional
	public void removePredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		getDependencyManagmner(nodeType).removePredecessorJobs(parentid, predecessorJobIds, dependencyType);

	}

	@Override
	@Transactional
	public void removePredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		getDependencyManagmner(nodeType).removePredecessorJobGroups(parentid, predecessorJobGroupIds, dependencyType);

	}

	@Override
	@Transactional
	public void removeSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		getDependencyManagmner(nodeType).removeSuccessorJobGroups(parentid, successorJobGroupIds, dependencyType);

	}

	@Override
	public void removePredecessorJobs(Job parentJob, Set<Job> predecessorJobs, DependencyType dependencyType, NodeCategory nodeCategory) throws DependencyManagerNotFoundException {
		List<Job> predecessorJobList = ApplicationUtil.setToList(predecessorJobs);
		getDependencyManagmner(nodeCategory).removeAsPredecessorJobs(parentJob, predecessorJobList, dependencyType);

	}

	@Override
	public void removePredecessorJobGroups(Job parentJob, Set<JobGroup<Job>> predecessorJobGroups, DependencyType dependencyType, NodeCategory nodeCategory)
			throws DependencyFailureException, DependencyManagerNotFoundException {
		List<JobGroup<Job>> predecessorJobGroupsList = ApplicationUtil.setToList(predecessorJobGroups);
		getDependencyManagmner(nodeCategory).removeAsPredecessorJobGroups(parentJob, predecessorJobGroupsList, dependencyType);

	}

	@Override
	public PageItr<JobGroup> getSuccessorJobGroups(long jobId, NodeCategory nodeCategory, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeCategory).getSuccessorJobGroups(jobId, pageNumber, pageSize);
	}

	@Override
	public PageItr<JobGroup> getPredecessorJobGroups(long jobId, NodeCategory nodeCategory, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeCategory).getPredecessorJobGroups(jobId, pageNumber, pageSize);
	}

	@Override
	public PageItr<JobGroup> searchSuccessorJobGroups(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeCategory).getPredecessorJobGroups(id, pageNumber, pageSize);
	}

	@Override
	public PageItr<JobGroup> searchPredecessorJobGroups(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeCategory).searchSuccessorJobGroups(id, name, pageNumber, pageSize);
	}

	@Override
	public PageItr<Job> searchSuccessorJobs(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws DependencyManagerNotFoundException,
			JobNotFoundException {
		return getDependencyManagmner(nodeCategory).searchSuccessorJobs(id, name, pageNumber, pageSize);
	}

	@Override
	public PageItr<Job> searchPredecessorJobs(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException {
		return getDependencyManagmner(nodeCategory).searchPredecessorJobs(id, name, pageNumber, pageSize);
	}

}
