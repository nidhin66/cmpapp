package com.gvg.syena.core.services.dependency.api;

import java.util.List;

import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.DependencyFailureException;

public interface DependencyAnalyzer {

	void analyzeDependency(DependencyAttachableHierarchy dependentTo, List<DependencyAttachableHierarchy> dependents, DependencyType dependencyType) throws DependencyFailureException;

	void analyzeDependency(DependencyAttachable parentJob, List<Job> jobs, DependencyType dependencyType) throws DependencyFailureException;

	void analyzeDependency(DependencyAttachable parentJob, JobGroup jobGroup, DependencyType dependencyType) throws DependencyFailureException;

}
