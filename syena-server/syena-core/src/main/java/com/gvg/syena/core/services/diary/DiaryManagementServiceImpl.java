
package com.gvg.syena.core.services.diary;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.diary.QDiaryThread;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussionId;
import com.gvg.syena.core.api.services.DiaryManagementService;
import com.gvg.syena.core.api.services.diary.DiaryFilterProperties;
import com.gvg.syena.core.datarepository.diary.DiaryThreadRepository;
import com.gvg.syena.core.datarepository.diary.ThreadDiscussionRepository;
import com.gvg.syena.core.utilities.AppUtil;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;


@Service("diaryManagementService")
public class DiaryManagementServiceImpl implements DiaryManagementService {

	@Autowired
	DiaryThreadRepository diaryThreadRepository;

	@Autowired
	ThreadDiscussionRepository threadDiscussionRepository;

	@PersistenceContext
	EntityManager em;

	@Override
	public DiaryThread createNewThread(DiaryThread diaryThread) {
		diaryThread = diaryThreadRepository.save(diaryThread);
		return diaryThread;
	}

	@Override
	public void addDiscussion(long threadId, ThreadDiscussion threadDiscussion) {
		int count = threadDiscussionRepository.getDiscussionCount(threadId);
		ThreadDiscussionId discussionId = new ThreadDiscussionId();
		discussionId.setSequence(count);
		discussionId.setThreadId(threadId);
		threadDiscussion.setThreadDiscussionId(discussionId);
		threadDiscussionRepository.save(threadDiscussion);
		DiaryThread diaryThread = diaryThreadRepository.findOne(threadId);
		diaryThread.setLastUpdatedDate(threadDiscussion.getDate());
		diaryThreadRepository.save(diaryThread);
	}

	public PageItr<DiaryThread> getLatestThreads(int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<DiaryThread> threadPage = diaryThreadRepository.getLatestThreads(pageable);
		return AppUtil.convert(threadPage);
	}
	
	public PageItr<DiaryThread> searchDiscussionThreads(DiaryFilterProperties filterProperties, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<DiaryThread> threadPage = diaryThreadRepository.findAll(createPredicate(filterProperties), pageable);
		return AppUtil.convert(threadPage);
	}

	public PageItr<ThreadDiscussion> getDiscussions(long threadId, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<ThreadDiscussion> threadDiscussionPAge = threadDiscussionRepository.findDiscussions(threadId, pageable);
		return AppUtil.convert(threadDiscussionPAge);
	}
	
	private Predicate createPredicate(DiaryFilterProperties filterProperties) {
	    QDiaryThread qDiaryThread = QDiaryThread.diaryThread;
	 
//	    qDiaryThread.createdBy.
//	    qDiaryThread.lastUpdatedDate.b
//	    qDiaryThread.createdBy.equals(o)
	    
	    BooleanBuilder booleanBuilder = new BooleanBuilder();
	    
	    if(filterProperties.getHierarchyItemId() > 0){
	    	booleanBuilder.and(qDiaryThread.hierarchyItemId.eq(filterProperties.getHierarchyItemId()));
	    	booleanBuilder.and(qDiaryThread.category.eq(filterProperties.getCategory()));
	    }
	    
	    if(null != filterProperties.getUser() && "".equalsIgnoreCase(filterProperties.getUser())){
	    	booleanBuilder.and(qDiaryThread.createdBy.eq(filterProperties.getUser()));
	    }
	    
	    if(null != filterProperties.getStartDate() && null != filterProperties.getEndDate()){
	    	booleanBuilder.and(qDiaryThread.lastUpdatedDate.between(filterProperties.getStartDate(), filterProperties.getEndDate()));
	    }else if(null != filterProperties.getStartDate()){
	    	booleanBuilder.and(qDiaryThread.lastUpdatedDate.after(filterProperties.getStartDate()));
	    }else if(null != filterProperties.getEndDate()){
	    	booleanBuilder.and(qDiaryThread.lastUpdatedDate.before(filterProperties.getEndDate()));
	    }
	   
//	      booleanBuilder
//	        .and(qDiaryThread.createdBy.contains(filterProperties.getUser()));
	    
//	      booleanBuilder.and(qDiaryThread.hierarchyItemId.eq(filterProperties.getHierarchyItemId()));

	    return booleanBuilder.getValue();
	  }

}

