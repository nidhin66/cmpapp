package com.gvg.syena.core.services.chart.creator.efficiencycalculator;

import java.util.Date;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.schedule.WorkPercentageType;
import com.gvg.syena.core.services.chart.creator.WorkPercentageFinder;

public class PlanningEfficiencyCalculator implements EfficiencyCalculator {

	private WorkPercentageFinder workPercentageFinder;

	public PlanningEfficiencyCalculator(WorkPercentageFinder workPercentageFinder) {
		this.workPercentageFinder = workPercentageFinder;
	}

	@Override
	public double calculateEfficiencyFactor(HierarchyNode hierarchyNode, Date ondate) {
		double estimatedWorkPercentage = workPercentageFinder.findWorkPercentage(hierarchyNode, ondate, WorkPercentageType.Planned);
		double revisedWorkPercentage = workPercentageFinder.findWorkPercentage(hierarchyNode, ondate, WorkPercentageType.Revised);
		if (estimatedWorkPercentage == 0 || revisedWorkPercentage == 0) {
			return 0;
		}
		return estimatedWorkPercentage / revisedWorkPercentage;
	}

}
