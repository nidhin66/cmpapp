package com.gvg.syena.core.datarepository.organization;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.organization.Role;

public interface RoleRepository extends PagingAndSortingRepository<Role, String> {
	
	public List<Role> findByRoleIdIn(List<String> roleIds);

}
