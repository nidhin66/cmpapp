package com.gvg.syena.core.services.dependency;

import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.dependency.HierarchyNodeDependency;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeDependencySchedulingException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.services.DependencyManagmnet;

@Service("hierarchyNodeDependencyAnalyzer")
public class HierarchyNodeDependencyConnector {

	private static final short REMOVE = 2;

	private static final short CREATE = 1;

	@Autowired
	private DependencyManagmnet dependencyManagmnet;

	@Transactional(rollbackOn = { DependencyFailureException.class, ApplicationException.class, SchedulingException.class, HierarchyNodeDependencySchedulingException.class,
			InvalidDataException.class })
	public void addjobtoJobDependencyHierarchyNode(HierarchyNodeDependency hierarchyNodeDependency, DependencyType dependencyType) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException {

		Set<Job> jobset = new TreeSet<Job>();
		jobset.add(hierarchyNodeDependency.getConnectionJob());
		try {
			addAsSuccessorNodeToJob(jobset, hierarchyNodeDependency.getSuccessorNode(), dependencyType, CREATE);
			addAsPredecessorNodeToJob(jobset, hierarchyNodeDependency.getPredecessorNode(), dependencyType, CREATE);
		} catch (SchedulingException schedulingException) {
			throw new HierarchyNodeDependencySchedulingException(MessageKeys.HIERARCHY_NODE_DEPENDENCY_SCHEDULING_EXCEPTION, schedulingException.getMessage() + "in Job scheduling",
					schedulingException);
		}

	}

	@Transactional(rollbackOn = { DependencyFailureException.class, ApplicationException.class, SchedulingException.class })
	public void removeJobtoJobdependency(HierarchyNodeDependency hierarchyNodeDependency, DependencyType dependencyType) throws DependencyManagerNotFoundException,
			DependencyFailureException, ApplicationException, SchedulingException {
		// jobtoJobDependencyHierarchyNode(dependentTo, dependent,
		// dependencyType, REMOVE);
		Set<Job> jobset = new TreeSet<Job>();
		jobset.add(hierarchyNodeDependency.getConnectionJob());
		addAsSuccessorNodeToJob(jobset, hierarchyNodeDependency.getSuccessorNode(), dependencyType, REMOVE);

	}

	@SuppressWarnings("unused")
	private void jobtoJobDependencyHierarchyNode(HierarchyNode dependentTo, HierarchyNode dependent, DependencyType dependencyType, short action)
			throws DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		addAsSuccessorNodeToJob(dependentTo.getJobs(), dependent, dependencyType, action);
		Set<JobGroup<Job>> jobGroups = dependentTo.getJobGroups();
		for (JobGroup jobGroup : jobGroups) {
			addJobtoJobDependency(jobGroup, dependent, dependencyType, action);
		}

		Set<HierarchyNode> children = dependentTo.getChildren();
		for (HierarchyNode child : children) {
			jobtoJobDependencyHierarchyNode(child, dependent, dependencyType, action);
		}

	}

	private void addJobtoJobDependency(JobGroup jobGroup, HierarchyNode dependent, DependencyType dependencyType, short action) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		addAsSuccessorNodeToJob(jobGroup.getJobs(), dependent, dependencyType, action);
		Set<JobGroup> jobGroups = jobGroup.getJobGroups();
		for (JobGroup jobGroup2 : jobGroups) {
			addJobtoJobDependency(jobGroup2, dependent, dependencyType, action);
		}

	}

	private void addAsSuccessorNodeToJob(Set<Job> jobs, HierarchyNode dependent, DependencyType dependencyType, short action) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		addAsPredecessorJobs(dependent.getJobs(), jobs, dependencyType, action);
		Set<JobGroup<Job>> jobGroups = dependent.getJobGroups();
		addJobtoJobDependencySUS_JobGroup(jobGroups, jobs, dependencyType, action);
		Set<HierarchyNode> children = dependent.getChildren();
		for (HierarchyNode child : children) {
			addAsSuccessorNodeToJob(jobs, child, dependencyType, action);
		}
	}

	private void addJobtoJobDependencySUS_JobGroup(Set<JobGroup<Job>> jobGroups, Set<Job> jobs, DependencyType dependencyType, short action)
			throws DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		for (JobGroup jobGroup : jobGroups) {
			addAsPredecessorJobs(jobs, jobGroup.getJobs(), dependencyType, action);
			addJobtoJobDependencySUS_JobGroup(jobGroup.getJobGroups(), jobs, dependencyType, action);
		}

	}

	private void addAsPredecessorNodeToJob(Set<Job> jobs, HierarchyNode dependent, DependencyType dependencyType, short action) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		addAsPredecessorJobs(jobs, dependent.getJobs(), dependencyType, action);
		Set<JobGroup<Job>> jobGroups = dependent.getJobGroups();
		addAsPredecessorJobGroup(jobs, jobGroups, dependencyType, action);
		Set<HierarchyNode> children = dependent.getChildren();
		for (HierarchyNode child : children) {
			addAsPredecessorNodeToJob(jobs, child, dependencyType, action);
		}
	}

	private void addAsPredecessorJobGroup(Set<Job> jobs, Set<JobGroup<Job>> jobGroups, DependencyType dependencyType, short action) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		for (Job job : jobs) {
			if (action == CREATE) {
				dependencyManagmnet.addPredecessorJobGroups(job, jobGroups, dependencyType, NodeCategory.jobgroup);
			} else if (action == REMOVE) {
				dependencyManagmnet.removePredecessorJobGroups(job, jobGroups, dependencyType, NodeCategory.jobgroup);
			}
		}

	}

	private void addAsPredecessorJobs(Set<Job> jobs, Set<Job> predecessorJobs, DependencyType dependencyType, short action) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException {
		if (predecessorJobs.size() > 0) {
			for (Job job : jobs) {
				if (action == CREATE) {
					dependencyManagmnet.addPredecessorJobs(job, predecessorJobs, dependencyType, NodeCategory.job);
				} else if (action == REMOVE) {
					dependencyManagmnet.removePredecessorJobs(job, predecessorJobs, dependencyType, NodeCategory.job);
				}
			}
		}

	}

}
