package com.gvg.syena.core.services.schedulerjob.jobs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.NotificationService;
import com.gvg.syena.core.api.services.notification.NotificationRecord;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;
import com.gvg.syena.core.api.services.notification.PlaceHolderTableData;
import com.gvg.syena.core.api.services.notification.PlaceHolderType;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.notification.NotificationConfigRepository;
import com.gvg.syena.core.services.notification.NotificationServiceImpl;

@Service("commissioningActivitiesJob")
public class CommissioningActivitiesJob extends QuartzJobBean implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext = null;
	
	private EntityManager em = null;
	
	private static final String parentQuery =  "select from HierarchyNode h where children = :child"; 

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		CommissioningActivitiesJob.applicationContext = applicationContext;
		
	}

	@Override
	@Transactional
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		
		NotificationConfigRepository notificationConfigRepository = applicationContext.getBean(NotificationConfigRepository.class);	
		NotificationConfig notificationConfig = notificationConfigRepository.findOne("NO_COMMISSIONING_ACTIVITY");
		HierarchyManagement hierarchyManagement = applicationContext.getBean(HierarchyManagement.class);
		
		
		
		
	
		HierarchyNodeRepository hierarchyNodeRepository = applicationContext.getBean(HierarchyNodeRepository.class);
		List<HierarchyNode> linkedNodes = new ArrayList<>();
		List<HierarchyNode> jobLinkedNodes = hierarchyNodeRepository.findJobConnectedHierarchyNodes();
		List<HierarchyNode> jobGroupLinkedNodes = hierarchyNodeRepository.findJobGroupConnectedHierarchyNodes(NodeType.instance);
		if(null != jobLinkedNodes && jobLinkedNodes.size() > 0){
			linkedNodes.addAll(jobLinkedNodes);
		}
		if(null != jobGroupLinkedNodes && jobGroupLinkedNodes.size() > 0){
			linkedNodes.addAll(jobGroupLinkedNodes);
		}
		List<Long> linkedNodeids = new ArrayList<>();
		for(HierarchyNode linkedNode : linkedNodes){
			if(!linkedNodeids.contains(linkedNode.getId())){
				hierarchyManagement.getParentIds(linkedNode.getId(), linkedNodeids);
				linkedNodeids.add(linkedNode.getId());
			}			
		}
		List<HierarchyNode> nonLinkedNodes = hierarchyNodeRepository.findByIdNotIn(linkedNodeids);
		if(null != nonLinkedNodes && nonLinkedNodes.size() > 0){
			List<NotificationRecord> records = new ArrayList<>();
			NotificationRecord record = new NotificationRecord();
			record.setRecordId(notificationConfig.getConfigId());
			record.setSubject(notificationConfig.getName());			
			
			List<PlaceHolderData> placeHolders = new ArrayList<>();
			PlaceHolderData tablePlaceHolder = new PlaceHolderData("TABLE_DATA", PlaceHolderType.TABLE);
			PlaceHolderTableData tableData = new PlaceHolderTableData(3);
			String[] headers = new String[3];
			headers[0] = "Name";
			headers[1] = "Description";
			headers[2] = "Level";			
			tableData.setHeader(headers);
			for(HierarchyNode hierarchyNode : nonLinkedNodes){
				String[] row = new String[3];
				row[0] = hierarchyNode.getName();
				row[1] = hierarchyNode.getDescription();
				row[2] = hierarchyNode.getHierarchyLevel().getLevelName();	
				tableData.addRow(row);
			}
			tablePlaceHolder.setValue(tableData);
			placeHolders.add(tablePlaceHolder);
			record.setPlaceHolderDatas(placeHolders);
			records.add(record);			
			NotificationService notificationService  = applicationContext.getBean(NotificationServiceImpl.class);
			notificationService.notify(notificationConfig,records);
		}
	}
	
//	private void getParent(StandardHierarchyNode node,List<Long> linkedNodeids){
//		Query query = em.createNativeQuery(parentQuery, StandardHierarchyNode.class);		
//		query.setParameter("child", node.getId());
//		List<StandardHierarchyNode> parents = query.getResultList();
//		if(null != parents && parents.size()>0){
//			StandardHierarchyNode parent = parents.get(0);
//			if(null != parent){
//				if(!linkedNodeids.contains(parent.getId())){					
//					linkedNodeids.add(parent.getId());
//					getParent(parent, linkedNodeids);
//				}			
//			}
//		}
//	}

}
