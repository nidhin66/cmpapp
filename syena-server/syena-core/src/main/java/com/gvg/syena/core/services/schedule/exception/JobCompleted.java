package com.gvg.syena.core.services.schedule.exception;

import java.util.Date;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.exception.JobScheduleFailed;

public class JobCompleted extends JobScheduleFailed {

	private Job job;
	private Date date;

	public JobCompleted(Job job, Date date, String messageKey, String messageString) {
		super(job, messageKey, messageString);
		this.job = job;
		this.date = date;
	}

}
