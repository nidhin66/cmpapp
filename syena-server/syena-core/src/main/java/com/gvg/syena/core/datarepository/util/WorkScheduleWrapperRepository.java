package com.gvg.syena.core.datarepository.util;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.wrapperentity.ApprovalStatus;
import com.gvg.syena.core.wrapperentity.WorkScheduleWrapper;
import com.gvg.syena.core.wrapperentity.WorkTimeWrapper;

public interface WorkScheduleWrapperRepository extends PagingAndSortingRepository<WorkScheduleWrapper, Long> {

	WorkScheduleWrapper findByJobIdAndApprovalStatus(long jobId, ApprovalStatus approvalStatus);

	@Query("select w from WorkScheduleWrapper w  where w.job  != null and approvalStatus =  :approvalStatus")
	Page<WorkScheduleWrapper> findByApprovalStatus(@Param("approvalStatus") ApprovalStatus approvalStatus, Pageable pageable);

	List<WorkScheduleWrapper> findByJobId(long jobId);

	WorkScheduleWrapper findByJobGroupIdAndApprovalStatus(long jobGrouId, ApprovalStatus forapproval);

	@Query("select w from WorkScheduleWrapper w  where w.jobGroup  != null and approvalStatus =  :approvalStatus")
	Page<WorkScheduleWrapper> findByJobGroup(@Param("approvalStatus") ApprovalStatus approvelStatus, Pageable pageable);

	WorkScheduleWrapper findByJobGroupId(long jobGrouId);

}
