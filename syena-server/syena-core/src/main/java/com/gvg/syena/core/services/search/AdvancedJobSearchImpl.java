package com.gvg.syena.core.services.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.job.JobManagement;
import com.gvg.syena.core.api.services.serach.AdvancedJobGroupSearch;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.datarepository.job.JobRepository;
import com.gvg.syena.core.services.chart.creator.BaseChartCreator;
import com.gvg.syena.core.services.chart.creator.ChartCreator;
import com.gvg.syena.core.utilities.AppUtil;
import com.gvg.syena.core.utilities.UtilServices;

@Service("AdvancedJobSearch")
public class AdvancedJobSearchImpl implements AdvancedJobSearch {

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private AdvancedJobSearchDao advancedJobSearchDao;

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private JobManagement jobmanagement;

	@Autowired
	private AdvancedJobGroupSearch advancedJobGroupSearchService;

	@Override
	@Transactional
	public PageItr<Job> getOnGoingJobs(Date date, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		RunningStatus[] runningStatus = { RunningStatus.onprogress };
		Page<Job> jobPage = jobRepository.getOnGoingJobs(date, jobName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	@Transactional
	public PageItr<Job> getCompletedJobs(Date date, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> jobPage = jobRepository.completedJobs(date, jobName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	@Transactional
	public PageItr<Job> getPlannedJobs(Date date, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> jobPage = jobRepository.plannedJobs(date, jobName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	@Transactional
	public PageItr<Job> getJobsInitiatedBy(String personId, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> jobPage = jobRepository.findByInitiatorIdAndJobNameLike(personId, jobName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	@Transactional
	public PageItr<Job> getJobsOwnedBy(String personId, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> jobPage = jobRepository.findByOwnerIdAndJobNameLike(personId, jobName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	@Transactional
	public PageItr<Job> getDelayedJobs(Date date, String jobName, int pageNumber, int pageSize) {
		// Pageable pageable = new PageRequest(pageNumber, pageSize);
		// Page<Job> jobPage = jobRepository.delayedJobs(date, pageable);
		// return AppUtil.convert(jobPage);
		return advancedJobSearchDao.getDelayedJobs(date, jobName, pageNumber, pageSize);
	}

	@Override
	@Transactional
	public PageItr<Job> searchJobs(JobProperties jobSearchOptions, int pageNumber, int pageSize) {
		return advancedJobSearchDao.searchJobs(jobSearchOptions, pageNumber, pageSize, Job.class);

		//
		// Pageable pagable = new PageRequest(pageNumber, pageSize);
		// String namePart = "%" + jobSearchOptions.getName() + "%";
		// String descStr = "%" + jobSearchOptions.getDescription() + "%";
		// String ownerId = "%" + jobSearchOptions.getOwnerId() + "%";
		// String initiatorId = "%" + jobSearchOptions.getInitiatorId() + "%";
		// Date startTime = jobSearchOptions.getStartDate();
		// Date endTime = jobSearchOptions.getEndDate();
		//
		// List<RunningStatus> runningStatus =
		// jobSearchOptions.getRunningStatus();
		// if (runningStatus.isEmpty()) {
		// for (RunningStatus runningStatu : RunningStatus.values()) {
		// runningStatus.add(runningStatu);
		// }
		// }
		//
		// String sdec = "%" + jobSearchOptions.getMilestoneDescription() + "%";
		// // , descStr, ownerId, initiatorId, runningStatus,
		// // startTime, endTime,
		// return
		// AppUtil.convert(jobRepository.findByAdvancedSearchOptions(namePart,
		// descStr, ownerId, initiatorId, runningStatus, pagable));
		// // return jobmanagement.searchJobWithPropertiesOR(jobSearchOptions,
		// // pageNumber, pageSize);
	}

	@Override
	@Transactional
	public PageItr<Job> jobsToBeScheduled(String jobName, int pageNumber, int pageSize) {
		return advancedJobSearchDao.jobsToBeScheduled(jobName, pageNumber, pageSize);

	}

	@Override
	@Transactional
	public PageItr<Job> jobsWithRevisedDates(String jobName, int pageNumber, int pageSize) {
		return advancedJobSearchDao.jobsWithRevisedDates(jobName, pageNumber, pageSize);

	}

	@Override
	@Transactional
	public PageItr<HierarchyNode> nodeWithDelayed(float delayedPercentage, Date when, ValueComparitor valueComparitor, String hierarchyLevel, int pageNumber, int pageSize)
			throws HierarchyLevelNotFoundException {
		PageItr<HierarchyNode> page = null;
		List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();
		do {
			page = hierarchyManagement.getAllHierarchyNodes(hierarchyLevel, pageNumber, pageSize);
			List<HierarchyNode> content = page.getContent();
			for (HierarchyNode hierarchyNode : content) {
				float percentage = getPercentageOfComplete(hierarchyNode, when);
				if (valueComparitor.check(delayedPercentage, percentage)) {
					hierarchyNodes.add(hierarchyNode);
				}
			}
			pageNumber++;
		} while (page.getTotalPages() > pageNumber);
		PageItr<HierarchyNode> pageItr = new PageItr<>(hierarchyNodes, hierarchyNodes.size(), 1);
		return pageItr;
	}

	private float getPercentageOfComplete(HierarchyNode hierarchyNode, Date date) {
		ChartCreator chartCreator = new BaseChartCreator();
		chartCreator.addChartType(ChartType.Actual);
		Float[] valuesSub = chartCreator.getChartPointsOn(hierarchyNode, date);
		return valuesSub[0];
	}

	@Override
	@Transactional
	public PageItr<Job> jobsInCriticalPath(long hierarchyId, String jobName, int pageNumber, int pageSize) {
		SortedSet<Long> preJobIds = getJobIdsInCriticalPath(hierarchyId);
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> page = jobRepository.findInstanceJobByIds(preJobIds, pageable);
		return AppUtil.convert(page);
	}

	@Override
	@Transactional
	public SortedSet<Long> getJobIdsInCriticalPath(long hierarchyId) {

		List<Long> hierarchyNodeIds = new ArrayList<Long>();
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyId);
		UtilServices.getChildNodeIds(hierarchyNode, hierarchyNodeIds);
		hierarchyNodeIds.add(hierarchyId);
		Set<Job> jobs = jobRepository.jobsInCriticalPath(hierarchyNodeIds);

		SortedSet<Long> preJobIds = new TreeSet<Long>();
		ApplicationUtil.addPredecessorJobIds(jobs, preJobIds);

		return preJobIds;
	}

	@Override
	public PageItr<Job> jobsStartInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> page = jobRepository.jobsStartInNDays(fromDate, toDate, jobName, pageable);
		return AppUtil.convert(page);
	}

	@Override
	public PageItr<Job> jobsCompleteInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> page = jobRepository.jobsCompleteInNDays(fromDate, toDate, jobName, pageable);
		return AppUtil.convert(page);
	}

	@Override
	public PageItr<Job> jobJobsInDelyedStartJobGroups(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobName, int pageNumber, int pageSize) {
		SortedSet<Long> parentIds = advancedJobGroupSearchService.jobGroupwithDelyedStartJobGroupId(hierarchyNodeId, valueComparitor, percentage);
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> page = jobRepository.findByParentIdIn(parentIds, pageable);
		return AppUtil.convert(page);
	}

	@Override
	public PageItr<Job> JobsInIncreasedDurationJobGroups(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobName, int pageNumber, int pageSize) {
		List<Long> parentIds = advancedJobGroupSearchService.jobGroupIdwithIncreasedDurationJobs(hierarchyNodeId, valueComparitor, percentage);
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<Job> page = jobRepository.findByParentIdIn(parentIds, pageable);
		return AppUtil.convert(page);
	}

}
