package com.gvg.syena.core.services.chart.creator;

import com.gvg.syena.core.services.chart.creator.efficiencycalculator.EfficiencyCalculator;
import com.gvg.syena.core.services.chart.creator.efficiencycalculator.ScheduleEfficiencyCalculator;

public class ScheduleEfficiencyChartCreator extends PredictedChartCreator {

	@Override
	EfficiencyCalculator getEfficiencyCalculator() {
		return new ScheduleEfficiencyCalculator(getWorkPercentageFinder());
	}

}
