package com.gvg.syena.core.services.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.ManagedType;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.services.serach.DateRange;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.util.ApplicationUtil;

@Component("advancedJobSearchDao")
public class AdvancedJobSearchDao {

	private static final String jobsToBeScheduledQury = "select * from job where description like :jobName and id in (select distinct job_id from job_scheduledstage where scheduledstages_id in (select id from scheduledstage where percentageofwork = 0 and estimatedtime is null))";

	private static final String jobsWithRevisedDates = "select * from job where description like :jobName and  id in (select distinct job_id from job_scheduledstage where scheduledstages_id in (select id from scheduledstage where  revisedtime is not null))";

	private static final String dealy = "select * from job where description like :jobName and  id in (select o.Job_id from (select *  from job_scheduledstage js  join scheduledstage ss  on js.scheduledStages_id = ss.id) o join (select Job_id, max(percentageOfWork)  as topPercentageOfWork from job_scheduledstage js  join scheduledstage ss  on js.scheduledStages_id = ss.id where  COALESCE(revisedTime, estimatedTime) <= :date group by Job_id)  r on o.Job_id = r.Job_id and o.percentageOfWork = r.topPercentageOfWork where ( o.actualTime is null or o.actualTime > COALESCE(o.revisedTime, o.estimatedTime) ))";

	@PersistenceContext
	private EntityManager em;

	public PageItr<Job> getDelayedJobs(Date date, String jobName, int pageNumber, int pageSize) {
		Query typedQuery = em.createNativeQuery(dealy, Job.class);
		typedQuery.setParameter("date", date, TemporalType.DATE);
		typedQuery.setParameter("jobName", jobName);
		int max = typedQuery.getResultList().size();
		List<Job> res = typedQuery.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize).getResultList();
		PageItr<Job> page = new PageItr<>(res, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));
		return page;
	}

	public PageItr<Job> jobsToBeScheduled(String jobName, int pageNumber, int pageSize) {
		Query typedQuery = em.createNativeQuery(jobsToBeScheduledQury, Job.class);
		typedQuery.setParameter("jobName", jobName);
		int max = typedQuery.getResultList().size();
		List<Job> res = typedQuery.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize).getResultList();
		PageItr<Job> page = new PageItr<>(res, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));
		return page;
	}

	public PageItr<Job> jobsWithRevisedDates(String jobName, int pageNumber, int pageSize) {
		Query typedQuery = em.createNativeQuery(jobsWithRevisedDates, Job.class);
		typedQuery.setParameter("jobName", jobName);
		int max = typedQuery.getResultList().size();
		List<Job> res = typedQuery.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize).getResultList();
		PageItr<Job> page = new PageItr<>(res, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));
		return page;
	}

	@Transactional
	public <T> PageItr<T> searchJobs(JobProperties jobSearchOptions, int pageNumber, int pageSize, Class<T> claz) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(claz);
		Root<T> root = criteriaQuery.from(claz);

		Predicate[] predicates = new Predicate[20];

		int index = 0;

		Predicate nodeTypepredicate = criteriaBuilder.equal(root.get("nodeType"), NodeType.instance);
		predicates[index++] = nodeTypepredicate;

		String jobName = jobSearchOptions.getName();
		if (jobName != null && jobName.trim() != "") {
			jobName = append(jobName);
			if (claz == JobGroup.class) {
				Predicate predicate = criteriaBuilder.like(root.get("name"), jobName);
				predicates[index++] = predicate;
			} else {
				Predicate predicate = criteriaBuilder.like(root.get("jobName"), jobName);
				predicates[index++] = predicate;
			}
		}

		String description = jobSearchOptions.getDescription();
		if (description != null && description.trim() != "") {
			description = append(description);
			Predicate predicate = criteriaBuilder.like(root.get("description"), description);
			predicates[index++] = predicate;
		}

		String ownerId = jobSearchOptions.getOwnerId();
		if (ownerId != null && ownerId.trim() != "") {
			ownerId = append(ownerId);
			Predicate predicate = criteriaBuilder.like(root.get("owner").get("id"), ownerId);
			predicates[index++] = predicate;
		}

		String initiatorId = jobSearchOptions.getInitiatorId();
		if (initiatorId != null && initiatorId.trim() != "") {
			initiatorId = append(initiatorId);
			Predicate predicate = criteriaBuilder.like(root.get("initiator").get("id"), initiatorId);
			predicates[index++] = predicate;
		}

		WorkStatus workStatus = jobSearchOptions.getWorkStatus();
		if (workStatus != null) {
			Predicate predicate1 = criteriaBuilder.equal(root.get("workStatus").get("runningStatus"), workStatus.getRunningStatus());
			Predicate predicate2 = criteriaBuilder.equal(root.get("workStatus").get("delayInCurrentStage"), workStatus.isDelayInCurrentStage());
			Predicate predicate = criteriaBuilder.and(predicate1, predicate2);
			predicates[index++] = predicate;
		}
		String milestoneDescription = jobSearchOptions.getMilestoneDescription();
		if (milestoneDescription != null) {
			// ManagedType<WorkSchedule> mt =
			// em.getMetamodel().embeddable(WorkSchedule.class);
			// Root<ScheduledStage> scheRoot =
			// criteriaQuery.from(ScheduledStage.class);
			//
			// Predicate schPredicate = criteriaBuilder.like(scheRoot.<String>
			// get("description"), milestoneDescription);
			//
			// Predicate predicate = criteriaBuilder.equal(schPredicate,
			// root.<WorkSchedule> get("workSchedule"));
			//
			// mt.getSingularAttributes();
			//
			// predicates[index++] = predicate;
			// System.out.println("Done");
		}

		DateRange plannedDateRange = jobSearchOptions.getPlannedDateRange();
		if (plannedDateRange != null) {

			if (plannedDateRange.getFromDate() != null) {
				Expression c1 = criteriaBuilder.coalesce(root.get("workTime").get("startTime").get("revisedTime"), root.get("workTime").get("startTime").get("estimatedTime"));
				Predicate predicate1 = criteriaBuilder.greaterThanOrEqualTo(c1, plannedDateRange.getFromDate());
				predicates[index++] = predicate1;
				Predicate predicate3 = criteriaBuilder.isNotNull(c1);
				predicates[index++] = predicate3;
			}
			if (plannedDateRange.getToDate() != null) {
				Expression c1 = criteriaBuilder.coalesce(root.get("workTime").get("finishTime").get("revisedTime"), root.get("workTime").get("finishTime").get("estimatedTime"));
				Predicate predicate1 = criteriaBuilder.lessThanOrEqualTo(c1, plannedDateRange.getToDate());
				predicates[index++] = predicate1;
				Predicate predicate3 = criteriaBuilder.isNotNull(c1);
				predicates[index++] = predicate3;
			}

		}

		DateRange actualDateRange = jobSearchOptions.getActualDateRange();
		if (actualDateRange != null) {
			if (actualDateRange.getFromDate() != null) {
				Predicate predicate1 = criteriaBuilder.greaterThanOrEqualTo(root.get("workTime").get("startTime").get("actualTime"), actualDateRange.getFromDate());
				predicates[index++] = predicate1;
				Predicate predicate3 = criteriaBuilder.isNotNull(root.get("workTime").get("startTime").get("actualTime"));
				predicates[index++] = predicate3;
			}
			if (actualDateRange.getToDate() != null) {
				Predicate predicate2 = criteriaBuilder.lessThanOrEqualTo(root.get("workTime").get("finishTime").get("actualTime"), actualDateRange.getToDate());
				Predicate predicate3 = criteriaBuilder.isNotNull(root.get("workTime").get("finishTime").get("actualTime"));
				predicates[index++] = predicate2;
				predicates[index++] = predicate3;
			}
		}

		String executor = jobSearchOptions.getExecutor();
		if (executor != null && executor.trim() != "") {
			executor = append(executor);
			Predicate predicate = criteriaBuilder.like(root.get("executor"), executor);
			predicates[index++] = predicate;
		}

		Predicate[] newPredicates = new Predicate[index];
		if (index == 0) {
			return new PageItr<T>(new ArrayList<T>(), 0, 0);
		}
		System.arraycopy(predicates, 0, newPredicates, 0, index);

		criteriaQuery.where(criteriaBuilder.and(newPredicates));

		TypedQuery<T> typedQuery = em.createQuery(criteriaQuery);
		// typedQuery.
		int max = typedQuery.getResultList().size();
		typedQuery.setFirstResult(pageNumber * pageSize);
		typedQuery.setMaxResults(pageSize);

		List<T> resultList = typedQuery.getResultList();

		PageItr<T> pageItr = new PageItr<>(resultList, pageNumber, ApplicationUtil.getTotalPageCount(max, pageSize));

		return pageItr;
	}

	private String append(String name) {
		return "%" + name + "%";
	}

}

// private static final String scheduledstagedescription =
// "SELECT * FROM job  where jobName like :jobName and  id in(select Job_id from job_scheduledstage where scheduledStages_id in(select id from scheduledstage where description = ?))";

// private static final String dealy =
// "select * from (select * from job j join (select Job_id, max(percentageOfWork) as topPercentageOfWork from job_scheduledstage js  join scheduledstage ss  on js.scheduledStages_id = ss.id where  COALESCE(revisedTime, estimatedTime) <= :date group by Job_id) r on j.id = r.Job_id  ) s join (select  * from job_scheduledstage js  join scheduledstage ss  on js.scheduledStages_id = ss.id ) j on s.Job_id=j.Job_id and s.topPercentageOfWork = j.percentageOfWork where ( actualTime is null or actualTime > COALESCE(revisedTime, estimatedTime) ) ";

// Predicate predicate =
// criteriaBuilder.like(job.<WorkSchedule>get("workSchedule").<Set<ScheduledStage>>get("scheduledStages").<String>get("description"),milestoneDescription);
// for(PluralAttribute pluralAttribute : mt.getPluralAttributes()){
// job.get("workSchedule").get(pluralAttribute);
// }
//
// job.get("workSchedule").get("scheduledStages.description");
// for(PluralAttribute pluralAttribute : attribute){
// pluralAttribute.getName();
// job.get("workSchedule").get(pluralAttribute);
// }
// job.get("workSchedule").get(collection)
// job.get("workSchedule").in(mt.getCollection("scheduledStages",ScheduledStage.class));
// job.join
// job.get(mt.getCollection("scheduledStages"))

// Root<ScheduledStage> schedRoot =
// criteriaQuery.from(ScheduledStage.class);
// Root<WorkSchedule> workRoot =
// criteriaQuery.from(WorkSchedule.class);

// Predicate schedPredicate =
// criteriaBuilder.like(schedRoot.get("description"),
// milestoneDescription);
// Predicate workPredicate =
// criteriaBuilder.equal(workRoot.get("scheduledStages"),schedPredicate);
// Predicate predicate =
// criteriaBuilder.exists(job.get("workSchedule").get("scheduledStages"),schedPredicate);
// schedRoot.get("description");
// Metamodel m = em.getMetamodel();
// EntityType<WorkSchedule> WorkSchedule_ =
// m.entity(WorkSchedule.class);
// EntityType<Job> Job_ = m.entity(Job.class);

// Path<WorkSchedule> workSchedulePath = job.get("workSchedule");
// Path<ScheduledStage> schedulPath =
// workSchedulePath.get("scheduledStages");
// schedulPath.get
// Join<Job, WorkSchedule> jobworks =
// job.<Job,WorkSchedule>join("workSchedule");
// jobworks.joinSet("scheduledStages", );
// Predicate predicate =
// criteriaBuilder.equal(jobworks.join("scheduledStages").get("description"),
// milestoneDescription);
// Predicate predicate =
// criteriaBuilder.like(job.get("workSchedule").get("scheduledStages").get("description"),milestoneDescription);
// job.join(Job_.getCollection("workSchedule"));
// Join<WorkSchedule, ScheduledStage> scJoin =
// jobworks.join("scheduledStages");
// Predicate predicate =
// criteriaBuilder.equal(scJoin.get("description"),
// milestoneDescription);
// // em.createQuery("").
// criteriaBuilder.
// Expression<List<String>> scheduledStages =
// job.get("workSchedule").get("scheduledStages").get("description");
// Predicate predicate =
// criteriaBuilder.isMember(milestoneDescription, scheduledStages);
