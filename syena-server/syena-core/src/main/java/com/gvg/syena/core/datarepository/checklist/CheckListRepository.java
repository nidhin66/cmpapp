package com.gvg.syena.core.datarepository.checklist;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.job.checklist.CheckList;

public interface CheckListRepository extends PagingAndSortingRepository<CheckList, Long> {

	Page<CheckList> findAllByIsStandard(boolean isStandard, Pageable pagable);

	Page<CheckList> findAllByIsStandardAndNameLikeIgnoreCase(boolean isStandard, String name, Pageable pagable);

	Page<CheckList> findAllByParentId(long parentId, Pageable pageable);

	List<CheckList> findAllByIdIn(long[] ids);

	Page<CheckList> findAllByIsStandardAndParentId(boolean b, long parentId, Pageable pagable);

	Page<CheckList> findAllByIsStandardAndParentIdAndNameLike(boolean b, long parentId, String name, Pageable pagable);

}
