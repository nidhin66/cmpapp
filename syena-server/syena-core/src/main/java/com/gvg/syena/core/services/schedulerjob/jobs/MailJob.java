package com.gvg.syena.core.services.schedulerjob.jobs;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.notification.Escalation;
import com.gvg.syena.core.api.entity.notification.EscalationLog;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;
import com.gvg.syena.core.api.services.UserManagemnet;
import com.gvg.syena.core.api.services.alert.mail.Email;
import com.gvg.syena.core.datarepository.notification.EscalationLogRepository;
import com.gvg.syena.core.datarepository.notification.EscalationRepository;
import com.gvg.syena.core.services.alert.mail.EmailService;

@Service("mailJob")
public class MailJob extends QuartzJobBean implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext = null;
	
	

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		
			EscalationLogRepository escalationLogRepository  = applicationContext.getBean(EscalationLogRepository.class);
			EscalationRepository escalationRepository = applicationContext.getBean(EscalationRepository.class);
			UserManagemnet userManagemnet = applicationContext.getBean(UserManagemnet.class);
			List<EscalationLog> escalationLogs = escalationLogRepository.findByAlertSendFalse();
			if(null != escalationLogs){
				for(EscalationLog escalationLog : escalationLogs){
					if(escalationLog.isAlertRequired()){
						Escalation escalation = escalationRepository.findOne(escalationLog.getEscalationId());	
						Email email = new Email();
						email.setFrom(EmailService.getInstance().getFromAdress());
						email.setSubject(escalationLog.getNotificationLog().getSubject());
						email.setText(escalationLog.getNotificationLog().getMessage());
						List<Role> roles = escalation.getToRoles();
						List<Person> persons = escalation.getToPersons();
						List<Person> toPersons = new ArrayList<>();
						if(null != persons){
							toPersons.addAll(persons);
						}
						if(null != roles){
							List<String> roleIds = new ArrayList<>();
							for(Role role : roles){
								roleIds.add(role.getRoleId());
							}
							List<Person> users = userManagemnet.getPersonRoleIds(roleIds);					
								
								if(null != users){
									toPersons.addAll(users);
								}
							
						}
						String[] toAddresses = new String[toPersons.size()];
						int i = 0;
						for(Person toPerson : toPersons){
							toAddresses[i] = toPerson.getEmailAddress();
							i++;
						}
						email.setTo(toAddresses);
						try{
							EmailService.sendEmail(email);
						}catch (Exception e){
							e.printStackTrace();
						}
						
						escalationLog.setAlertSend(true);
						escalationLogRepository.save(escalationLog);
					}
					
					
				}
			}
	
//			Email email = new Email();
//			email.setFrom("sudeep.mohandas@gamillusvaluegen.com");
//			email.setSubject("Test Mail");
//			
//			email.setText("Test Mail");
//
//			String[] toAddresses = new String[1];
//			toAddresses[0] = "sudeep.mohandas@gamillusvaluegen.com";
////			Set<MailToAddress> addresses = mailTemplate.getAdrresses();
////			toAddresses[1] = "sudeepcm@gmail.com";
//			email.setTo(toAddresses);
//			EmailService.sendEmail(email);
		
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		MailJob.applicationContext = applicationContext;
		
	}

}
