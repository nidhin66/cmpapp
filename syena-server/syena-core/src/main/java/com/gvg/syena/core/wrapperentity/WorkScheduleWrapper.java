package com.gvg.syena.core.wrapperentity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.time.PersonDays;

@Entity
public class WorkScheduleWrapper {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;

	@Embedded
	private WorkSchedule workSchedule;

	@OneToOne
	private Job job;

	@Embedded
	private PersonDays personDays;

	private ApprovalStatus approvalStatus = ApprovalStatus.forApproval;

	@OneToOne
	private JobGroup<Job> jobGroup;

	public WorkScheduleWrapper() {
	}

	public WorkScheduleWrapper(Job job, WorkSchedule workSchedule) {
		this.job = job;
		this.workSchedule = workSchedule;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public WorkSchedule getWorkSchedule() {
		return workSchedule;
	}

	public void setWorkSchedule(WorkSchedule workSchedule) {
		this.workSchedule = workSchedule;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public PersonDays getPersonDays() {
		return personDays;
	}

	public void setPersonDays(PersonDays personDays) {
		this.personDays = personDays;
	}

	public void setJob(JobGroup<Job> jobGroup) {
		this.jobGroup = jobGroup;

	}
	
	public JobGroup<Job> getJobGroup() {
		return jobGroup;
	}

}
