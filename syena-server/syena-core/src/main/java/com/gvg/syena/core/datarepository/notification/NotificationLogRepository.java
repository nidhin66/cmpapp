package com.gvg.syena.core.datarepository.notification;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.notification.NotificationLog;

public interface NotificationLogRepository extends CrudRepository<NotificationLog, Long>{
	public List<NotificationLog> findByConfigId(String configId);
	public NotificationLog findByConfigIdAndRecordId(String configId,String recordId);
	public void deleteByConfigIdAndRecordIdNotIn(String configId,List<String> recordIdList);
}
