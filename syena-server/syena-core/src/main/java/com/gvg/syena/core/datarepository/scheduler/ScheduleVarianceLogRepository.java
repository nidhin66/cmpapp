package com.gvg.syena.core.datarepository.scheduler;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.scheduler.ScheduleVarianceLog;

public interface ScheduleVarianceLogRepository extends CrudRepository<ScheduleVarianceLog, Long>{
	
	public List<ScheduleVarianceLog> findByCurrentVarianceLessThanEqual(double currentVariance);
	public List<ScheduleVarianceLog> findByCurrentVarianceGreaterThan(double currentVariance);


}
