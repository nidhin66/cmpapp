package com.gvg.syena.core.services.chart.creator;

import java.util.Date;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

public class BaseChartCreator extends ChartCreator {

	public BaseChartCreator() {
		super(1);
	}

	@Override
	public void prepareData(HierarchyNode hierarchyNode, Date toTime) {

	}

}
