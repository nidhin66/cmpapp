package com.gvg.syena.core.datarepository.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;

public interface HierarchyLevelRepository extends PagingAndSortingRepository<HierarchyLevel, String> {

	HierarchyLevel findByLevelName(String levelName);

}
