package com.gvg.syena.core.services.dataupload;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyLevelRepository;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;

@Service("testDataInsertService")
public class TestDataInsertServiceImpl implements TestDataInsertService {

	private int projectId = 1;

	@Autowired
	private HierarchyLevelRepository hierarchyLevelRepository;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	private int[] count;

	@Transactional
	public void insertData(int plant, int unit, int system, int substem, int loopEqu) {
		count = new int[] { plant, unit, system, substem, loopEqu };
		HierarchyLevel plantLevel = hierarchyLevelRepository.findByLevelName("Plant");
		List<HierarchyNode> hierarchyNodes = hierarchyNodeRepository.findByHierarchyLevel(plantLevel);
		if (hierarchyNodes != null && hierarchyNodes.size() > 0) {
			createUnit(hierarchyNodes.get(0).getId(), plantLevel.getChildren().get(0));
		}

	}

	private void createUnit(long id, HierarchyLevel level) {
		HierarchyNode node = hierarchyNodeRepository.findOne(id);
		int num = count[level.getLevel()];

		List<HierarchyNode> hierarchyNodes = new ArrayList<>();
		for (int i = 0; i < num; i++) {
			HierarchyNode unit = new HierarchyNode(level.getLevelName() + i, level.getLevelName(), level, projectId);
			hierarchyNodes.add(unit);
			node.addChild(unit);
		}
		hierarchyNodeRepository.save(hierarchyNodes);
		long[] ids = new long[hierarchyNodes.size()];
		int i = 0;
		for (HierarchyNode hierarchyNode : hierarchyNodes) {
			ids[i++] = hierarchyNode.getId();
		}

		System.out.println(level.getLevelName());
		for (long id1 : ids) {
			List<HierarchyLevel> children = level.getChildren();
			if (children != null) {
				for (HierarchyLevel child : children) {
					createUnit(id1, child);
				}
			}
		}

	}

	@Override
	@Transactional
	public void addJob(String levelName, int pageNumber, int num, String id) {

		HierarchyLevel plantLevel = hierarchyLevelRepository.findByLevelName(levelName);
		// Pageable pageable = new PageRequest(0, 10);
		// Page<HierarchyNode> page =
		// hierarchyNodeRepository.findByHierarchyLevel(plantLevel, pageable);
		// int i = 0;
		// while (i < page.getTotalPages()) {
		PageRequest pageable = new PageRequest(pageNumber, 10);
		Page<HierarchyNode> page = hierarchyNodeRepository.findByHierarchyLevel(plantLevel, pageable);
		List<HierarchyNode> hierarchyNodes = page.getContent();
		for (HierarchyNode hierarchyNode : hierarchyNodes) {
			for (int j = 0; j < num; j++) {
				Job job = new Job("Job" + hierarchyNode.getId() + pageNumber + "" + j + "" + id, "job");
				hierarchyNode.addJob(job);
				job.setConnectedNode(hierarchyNode);
			}
			saveJobData(hierarchyNode);

		}

		// }

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void saveJobData(HierarchyNode hierarchyNode) {
		hierarchyNodeRepository.save(hierarchyNode);

	}

	// private void createSystem() {
	// for (int s = 0; s < 10; s++) {
	// HierarchyNode system = new HierarchyNode("system" + s, "system",
	// plantLevel, projectId);
	// for (int b = 0; b < 100; b++) {
	// HierarchyNode subSystem = new HierarchyNode("subSystem" + b, "subSystem",
	// plantLevel, projectId);
	// for (int l = 0; l < 100; l++) {
	// HierarchyNode loopEqu = new HierarchyNode("loopEqu" + l, "loopEqu",
	// plantLevel, projectId);
	// for (int j = 0; j < 10; j++) {
	// Job job = new Job("job" + 1, "job");
	// loopEqu.addJob(job);
	// System.out.println("Job");
	// }
	// subSystem.addChild(loopEqu);
	// System.out.println("loopEqu");
	// }
	// system.addChild(subSystem);
	// System.out.println("subSystem");
	// }
	// unit.addChild(system);
	// System.out.println("system");
	// }
	//
	// }

}
