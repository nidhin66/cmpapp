package com.gvg.syena.core.datarepository;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;

public interface ApplicationConfigurationRepository extends CrudRepository<ApplicationConfiguration, String>  {

}
