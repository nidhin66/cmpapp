package com.gvg.syena.core.services.notification;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.notification.Escalation;
import com.gvg.syena.core.api.entity.notification.EscalationLog;
import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.entity.notification.NotificationLog;
import com.gvg.syena.core.api.services.NotificationService;
import com.gvg.syena.core.api.services.notification.NotificationDeleteType;
import com.gvg.syena.core.api.services.notification.NotificationRecord;
import com.gvg.syena.core.datarepository.notification.NotificationLogRepository;
import com.gvg.syena.core.utilities.UtilServices;

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService{
	
	
	@Autowired
	NotificationLogRepository notificationLogRepository;
	
	
	

	public void notify(NotificationConfig config, List<NotificationRecord> records){
		if(config.getDeleteType() == NotificationDeleteType.AFTER_ALERT){
			deleteAfterAlertLogs(config);
		}
		if(null != records){
			List<NotificationLog> notificationLogs = new ArrayList<>();
			List<String> recordIdList = new ArrayList<>();
			for(NotificationRecord record : records){
				NotificationLog notificationLog = notificationLogRepository.findByConfigIdAndRecordId(config.getConfigId(), record.getRecordId());
				if(null == notificationLog){
					notificationLog = new NotificationLog();
					notificationLog.setConfigId(config.getConfigId());
					notificationLog.setRecordId(record.getRecordId());
					notificationLog.setFirstNotifyDate(new Date());
					notificationLog.setName(config.getName());
					notificationLog.setSubject(record.getSubject());					
				}
				notificationLog.setMessage(MessageFormatter.formatMessage(config.getMessageTemplate(), record.getPlaceHolderDatas()));
				checkEscalationPoint(config, notificationLog);
				notificationLog.setLastLastNotifyDate(new Date());
				notificationLogs.add(notificationLog);
				recordIdList.add(record.getRecordId());
			}	
			try{
				notificationLogRepository.save(notificationLogs);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(config.getDeleteType() == NotificationDeleteType.NO_RECORD){
				notificationLogRepository.deleteByConfigIdAndRecordIdNotIn(config.getConfigId(), recordIdList);
			}
		}
	}
	
	
	private void checkEscalationPoint(NotificationConfig config,NotificationLog notificationLog){
		List<Escalation> escalations = config.getEscalations();
		if(null != escalations && escalations.size() > 0){
			List<EscalationLog> escalationLogs = notificationLog.getEscalationLog();			
			if(null == escalationLogs){
				escalationLogs = new ArrayList<>();	
			}		
			int notified  = escalationLogs.size();
			if(notified < escalations.size()){
				Escalation escalation = escalations.get(notified);					
				Date secondDate = new Date();
				Date firstDate = null;
				if(notified == 0){
					firstDate = notificationLog.getLastLastNotifyDate();						
				}else{
					EscalationLog escalationLog = escalationLogs.get(notified-1);
					firstDate = escalationLog.getLastNotifyDate();
				}
				int dayDiff = UtilServices.dateDifferenceInDays(firstDate, secondDate);
				if(dayDiff >= escalation.getIntervalInDays()){
					EscalationLog escalationLog = new EscalationLog(escalation);
					escalationLog.setNotificationLog(notificationLog);
					escalationLog.setNotificationType(config.getType());
					escalationLogs.add(escalationLog);
				}
			}
			for(EscalationLog escalationLog : escalationLogs){
				escalationLog.setLastNotifyDate(new Date());
				if(escalationLog.isAlertRequired()){
					escalationLog.setAlertSend(false);
				}
			}
			notificationLog.setEscalationLog(escalationLogs);
		}
	}
	
	
	
	
	
	
	/**
	 * Delete the records which is already alert send
	 * @param config
	 */
	private void deleteAfterAlertLogs(NotificationConfig config){
		 List<NotificationLog> notificationLogs = notificationLogRepository.findByConfigId(config.getConfigId());
		 if(null != notificationLogs){
			 List<NotificationLog> deleteRecords = new ArrayList<>();
			 for(NotificationLog notificationLog : notificationLogs){
				 List<EscalationLog> escalationLogs =   notificationLog.getEscalationLog();
				 int escalationCount = 0;
				 int notifiedEscalations = 0;
				 if(null != escalationLogs){
					 escalationCount = escalationLogs.size();
					 for(EscalationLog escalationLog : escalationLogs){
						 if (escalationLog.isAlertSend()){
							 notifiedEscalations++;
						 }
					 }
				 }
				 if(notifiedEscalations == escalationCount){
					 deleteRecords.add(notificationLog);
				 }
			 }
			 notificationLogRepository.delete(deleteRecords);
		 }
	}
	
	

}
