package com.gvg.syena.core.services.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.exception.MessageNotFoundException;
import com.gvg.syena.core.api.services.MessageService;
import com.gvg.syena.core.datarepository.util.MessageRepository;

@Service("messageService")
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageRepository messageRepository;

	@Override
	public Message getMessage(String messageId) throws MessageNotFoundException {
		Message message = messageRepository.findOne(messageId);
		if (message == null) {
			throw new MessageNotFoundException(messageId);
		}
		return message;
	}

}
