package com.gvg.syena.core.services.schedule.exception;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.exception.JobScheduleFailed;

public class JobDateBeforeToday extends JobScheduleFailed {

	public JobDateBeforeToday(Job job, String messageKey, String messageString) {

		super(job, messageKey, messageString);
	}

}
