package com.gvg.syena.core.services.notification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.notification.Escalation;
import com.gvg.syena.core.api.entity.notification.EscalationLog;
import com.gvg.syena.core.api.entity.organization.Department;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;
import com.gvg.syena.core.api.services.NotificationManagementService;
import com.gvg.syena.core.api.services.notification.Notification;
import com.gvg.syena.core.api.services.notification.NotificationType;
import com.gvg.syena.core.datarepository.notification.EscalationLogRepository;
import com.gvg.syena.core.datarepository.notification.EscalationRepository;
import com.gvg.syena.core.datarepository.notification.NotificationConfigRepository;
import com.gvg.syena.core.datarepository.organization.user.PersonRepository;

@Service("notificationManagementService")
public class NotificationManagementServiceImpl implements NotificationManagementService{
	
	@Autowired
	EscalationLogRepository escalationLogRepository;
	
	@Autowired
	EscalationRepository escalationRepository;
	
	@Autowired
	NotificationConfigRepository notificationConfigRepository;
	
	@Autowired
	PersonRepository personRepository;
	
	
	public long getNotificationCount(String userId){
		Person person = personRepository.findOne(userId);
		List<Escalation> escalations = new ArrayList<>();
		List<Escalation> personEscalations =	escalationRepository.findByToPersons(person);
		if(null != personEscalations && personEscalations.size() > 0){
			escalations.addAll(personEscalations);
		}
		Map<Department, Role> depertMentRoles = person.getRolesInDepartment();
		List<Role> roles = new ArrayList<>();
		for(Iterator<Role> it = depertMentRoles.values().iterator(); it.hasNext();){
			Role role = it.next();
			roles.add(role);
		}
		if(null != roles && roles.size() > 0){
			List<Escalation> roleEscalations = escalationRepository.findByToRolesIn(roles);
			if(null != roleEscalations && roleEscalations.size()>0){
				escalations.addAll(roleEscalations);
			}
		}
		List<Long> escalationIds = new ArrayList<>();
		if(null != escalations && escalations.size() > 0){
			for(Escalation escalation : escalations){
				escalationIds.add(escalation.getId());
			}
		}
	   long count = 0;
	   if(escalationIds.size() > 0) {
		   count = escalationLogRepository.findCountbyEscalationIdIn(escalationIds);
	   }
	   return count;
	}
	
	
	public PageItr<Notification> getNotifications(String userId,NotificationType notificationType,int pageNumber, int pageSize){
		Person person = personRepository.findOne(userId);
		List<Escalation> escalations = new ArrayList<>();
		List<Escalation> personEscalations =	escalationRepository.findByToPersons(person);
		if(null != personEscalations && personEscalations.size() > 0){
			escalations.addAll(personEscalations);
		}
		Map<Department, Role> depertMentRoles = person.getRolesInDepartment();
		List<Role> roles = new ArrayList<>();
		for(Iterator<Role> it = depertMentRoles.values().iterator(); it.hasNext();){
			Role role = it.next();
			roles.add(role);
		}
		if(null != roles && roles.size() > 0){
			List<Escalation> roleEscalations = escalationRepository.findByToRolesIn(roles);
			if(null != roleEscalations && roleEscalations.size()>0){
				escalations.addAll(roleEscalations);
			}
		}
		List<Long> escalationIds = new ArrayList<>();
		if(null != escalations && escalations.size() > 0){
			for(Escalation escalation : escalations){
				escalationIds.add(escalation.getId());
			}
		}
		Pageable pageable = new PageRequest(pageNumber, pageSize);
//		List<NotificationConfig> notificationConfigs = notificationConfigRepository.findByType(type);
		Page<EscalationLog> escalationLogPage = escalationLogRepository.findByEscalationIdInAndNotificationType(escalationIds,notificationType,  pageable);
		
		if(null != escalationLogPage){
			List<Notification> notifications = new ArrayList<>();
			if(null != escalationLogPage.getContent()){
				for(EscalationLog escalationLog : escalationLogPage.getContent()){
					Notification notification = new Notification();
					notification.setAlertRequired(escalationLog.isAlertRequired());
					notification.setDate(escalationLog.getLastNotifyDate());
					notification.setMessage(escalationLog.getNotificationLog().getMessage());
					notification.setName(escalationLog.getNotificationLog().getName());
					notification.setSubject(escalationLog.getNotificationLog().getSubject());
					notification.setEscalationLogId(escalationLog.getId());
					notifications.add(notification);
				}
			}
			PageItr<Notification> notificationPage = new PageItr<>(notifications,escalationLogPage.getNumber(), escalationLogPage.getTotalPages());
			return notificationPage;
		}else{
			return null;
		}
	}
}
