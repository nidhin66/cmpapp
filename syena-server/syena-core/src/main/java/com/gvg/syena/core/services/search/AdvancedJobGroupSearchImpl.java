package com.gvg.syena.core.services.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.services.HierarchyManagement;
import com.gvg.syena.core.api.services.serach.AdvancedJobGroupSearch;
import com.gvg.syena.core.api.services.serach.AdvancedJobSearch;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;
import com.gvg.syena.core.datarepository.hierarchy.HierarchyNodeRepository;
import com.gvg.syena.core.datarepository.job.JobGroupRepository;
import com.gvg.syena.core.utilities.AppUtil;
import com.gvg.syena.core.utilities.UtilServices;

@Service("advancedJobGroupSearchService")
public class AdvancedJobGroupSearchImpl implements AdvancedJobGroupSearch {

	@Autowired
	private JobGroupRepository jobGroupRepository;

	@Autowired
	private AdvancedJobGroupSearchDao advancedJobGroupSearchDao;

	@Autowired
	private AdvancedJobSearchDao advancedJobSearchDao;

	@Autowired
	private HierarchyManagement hierarchyManagement;

	@Autowired
	private HierarchyNodeRepository hierarchyNodeRepository;

	@Autowired
	private AdvancedJobSearch advancedJobSearch;

	@Override
	public PageItr<JobGroup<Job>> getOnGoingJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		RunningStatus[] runningStatus = { RunningStatus.onprogress };
		Page<JobGroup<Job>> jobPage = jobGroupRepository.onGoingJobGroups(date, jobGroupName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	public PageItr<JobGroup<Job>> completedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup<Job>> jobPage = jobGroupRepository.completedJobGroups(date, jobGroupName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	public PageItr<JobGroup<Job>> plannedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup<Job>> jobPage = jobGroupRepository.plannedJobs(date, jobGroupName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	public PageItr<JobGroup<Job>> getJobsInitiatedBy(String personId, String jobGroupName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup<Job>> jobPage = jobGroupRepository.findByInitiatorIdAndNameLike(personId, jobGroupName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	public PageItr<JobGroup<Job>> getJobsOwnedBy(String personId, String jobGroupName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup<Job>> jobPage = jobGroupRepository.findByOwnerIdAndNameLike(personId, jobGroupName, pageable);
		return AppUtil.convert(jobPage);
	}

	@Override
	public PageItr<JobGroup<Job>> delayedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize) {
		return advancedJobGroupSearchDao.delayedJobGroups(date, jobGroupName, pageNumber, pageSize);
	}

	@Override
	public PageItr<JobGroup> searchJobs(JobProperties jobSearchOptions, int pageNumber, int pageSize) {
		return advancedJobSearchDao.searchJobs(jobSearchOptions, pageNumber, pageSize, JobGroup.class);
	}

	@Override
	public PageItr<JobGroup<Job>> jobsToBeScheduled(String jobGroupName, int pageNumber, int pageSize) {
		return advancedJobGroupSearchDao.jobsToBeScheduled(jobGroupName, pageNumber, pageSize);
	}

	@Override
	public PageItr<JobGroup<Job>> jobsWithRevisedDates(String jobGroupName, int pageNumber, int pageSize) {
		return advancedJobGroupSearchDao.jobsWithRevisedDates(jobGroupName, pageNumber, pageSize);
	}

	@Override
	@Transactional
	public PageItr<JobGroup> jobGroupwithDelyedStartJobs(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobName, int pageNumber, int pageSize) {

		SortedSet<Long> resultId = jobGroupwithDelyedStartJobGroupId(hierarchyNodeId, valueComparitor, percentage);

		Pageable pageable = new PageRequest(pageNumber, pageSize);
		List<Long> list = new ArrayList<>();
		list.addAll(resultId);
		if (resultId.size() > 0) {
			Page<JobGroup> page = jobGroupRepository.findByIdInAndNodeType(list, NodeType.instance, pageable);
			return AppUtil.convert(page);
		} else {
			PageItr<JobGroup> pageItr = new PageItr<>(new ArrayList<>(), 0, 0);
			return pageItr;
		}
	}

	@Override
	@Transactional
	public SortedSet<Long> jobGroupwithDelyedStartJobGroupId(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage) {
		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		List<Long> hierarchyNodeIds = new ArrayList<>();
		UtilServices.getChildNodeIds(hierarchyNode, hierarchyNodeIds);
		// Pageable pageable = new PageRequest(pageNumber, pageSize);
		// Page<JobGroup<Job>> page =
		// jobGroupRepository.findByConnectedNodeIdInAndNodeType(hierarchyNodeIds,
		// NodeType.instance);
		List<JobGroup<Job>> jobGroups = jobGroupRepository.findByConnectedNodeIdInAndNodeType(hierarchyNodeIds, NodeType.instance);

		// List<JobGroup<Job>> jobGroups = page.getContent();
		SortedSet<Long> resultIds = new TreeSet<Long>();

		for (JobGroup<Job> jobGroup : jobGroups) {
			Set<Job> jobs = jobGroup.getJobs();
			int count = 0;
			for (Job job : jobs) {
				if (job.getWorkSchedule().delayInCurrentStage()) {
					count++;
				}
			}
			float act = 0;
			if (jobs.size() != 0 && count != 0) {
				act = jobs.size() / count;
			}

			if (valueComparitor.check(act * 100, percentage)) {
				resultIds.add(jobGroup.getId());
			}
		}
		return resultIds;

	}

	@Override
	@Transactional
	public PageItr<JobGroup> jobGroupwithIncreasedDurationJobs(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobName, int pageNumber, int pageSize) {

		List<Long> resultId = jobGroupIdwithIncreasedDurationJobs(hierarchyNodeId, valueComparitor, percentage);
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		if (resultId.size() > 0) {
			Page<JobGroup> page = jobGroupRepository.findByIdInAndNodeType(resultId, NodeType.instance, pageable);
			return AppUtil.convert(page);
		} else {
			PageItr<JobGroup> pageItr = new PageItr<>(new ArrayList<>(), 0, 0);
			return pageItr;
		}

	}
	@Override
	@Transactional
	public List<Long> jobGroupIdwithIncreasedDurationJobs(Long hierarchyNodeId, ValueComparitor valueComparitor, float percentage) {

		HierarchyNode hierarchyNode = hierarchyNodeRepository.findOne(hierarchyNodeId);
		List<Long> hierarchyNodeIds = new ArrayList<>();
		UtilServices.getChildNodeIds(hierarchyNode, hierarchyNodeIds);
		// Pageable pageable = new PageRequest(pageNumber, pageSize);
		// Page<JobGroup<Job>> page =
		// jobGroupRepository.findByConnectedNodeIdInAndNodeType(hierarchyNodeIds,
		// NodeType.instance);
		// List<JobGroup<Job>> jobGroups = page.getContent();
		List<JobGroup<Job>> jobGroups = jobGroupRepository.findByConnectedNodeIdInAndNodeType(hierarchyNodeIds, NodeType.instance);

		List<Long> resultId = new ArrayList<Long>();
		for (JobGroup<Job> jobGroup : jobGroups) {
			Set<Job> jobs = jobGroup.getJobs();
			int count = 0;
			for (Job job : jobs) {
				if (job.isDurationIncreased()) {
					count++;
				}
			}
			float act = 0;
			if (jobs.size() != 0 && count != 0) {
				act = jobs.size() / count;
			}

			if (valueComparitor.check(act * 100, percentage)) {
				resultId.add(jobGroup.getId());
			}
		}
		return resultId;
	}

	@Override
	public PageItr<JobGroup> jobGroupsInCriticalPath(long hierarchyNodeId, String jobGroupName, int pageNumber, int pageSize) {
		SortedSet<Long> jobIds = advancedJobSearch.getJobIdsInCriticalPath(hierarchyNodeId);
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup> jobGroups = jobGroupRepository.findJobGroupWithJobIds(jobIds, pageable);
		return AppUtil.convert(jobGroups);
	}

	@Override
	public PageItr<JobGroup> jobsGroupsStartInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup> jobGroups = jobGroupRepository.jobsStartInNDays(fromDate, toDate, jobName, pageable);
		return AppUtil.convert(jobGroups);
	}

	@Override
	public PageItr<JobGroup> jobsGroupsCompleteInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize) {
		Pageable pageable = new PageRequest(pageNumber, pageSize);
		Page<JobGroup> jobGroups = jobGroupRepository.jobsCompleteInNDays(fromDate, toDate, jobName, pageable);
		return AppUtil.convert(jobGroups);
	}

}
