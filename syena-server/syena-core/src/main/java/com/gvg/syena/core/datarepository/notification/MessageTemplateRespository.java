package com.gvg.syena.core.datarepository.notification;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.notification.MessageTemplate;

public interface MessageTemplateRespository extends CrudRepository<MessageTemplate, Long>{

}
