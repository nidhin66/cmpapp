package com.gvg.syena.core.wrapperentity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;

@Entity
public class WorkTimeWrapper {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;

	@Embedded
	private WorkTime workTime;

	@OneToOne
	private HierarchyNode hierarchyNode;

	@OneToOne
	private JobGroup jobGroup;

	private ApprovalStatus approvelStatus = ApprovalStatus.forApproval;

	public WorkTimeWrapper() {
	}

	public WorkTimeWrapper(JobGroup jobGroup, WorkTime workTime) {
		this.jobGroup = jobGroup;
		this.workTime = workTime;
	}

	public WorkTimeWrapper(HierarchyNode hierarchyNode, WorkTime workTime) {
		this.hierarchyNode = hierarchyNode;
		this.workTime = workTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public WorkTime getWorkTime() {
		return workTime;
	}

	public void setWorkTime(WorkTime workTime) {
		this.workTime = workTime;
	}

	public HierarchyNode getHierarchyNode() {
		return hierarchyNode;
	}

	public void setHierarchyNode(HierarchyNode hierarchyNode) {
		this.hierarchyNode = hierarchyNode;
	}

	public JobGroup getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(JobGroup jobGroup) {
		this.jobGroup = jobGroup;
	}

	public ApprovalStatus getApprovelStatus() {
		return approvelStatus;
	}

	public void setApprovelStatus(ApprovalStatus approvelStatus) {
		this.approvelStatus = approvelStatus;
	}

}
