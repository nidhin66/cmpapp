package com.gvg.syena.core.datarepository.job;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

public interface JobGroupRepository extends PagingAndSortingRepository<JobGroup, Long> {

	List<JobGroup<Job>> findByIdIn(List<Long> ids);

	Page<JobGroup> findAllByNodeType(NodeType nodeType, Pageable pageable);

	Page<JobGroup<Job>> findByInitiatorIdAndNameLike(String personId, String name, Pageable pageable);

	Page<JobGroup<Job>> findByOwnerIdAndNameLike(String personId, String name, Pageable pageable);

	Page<JobGroup> findByIdIn(List<Long> ids, Pageable pageable);

	Page<JobGroup> findByIdInAndNameLike(List<Long> ids, String name, Pageable pageable);

	Page<JobGroup> findByParentIdIn(List<Long> jobParentId, Pageable pageable);

	Page<JobGroup<Job>> findByConnectedNodeIdInAndNodeType(List<Long> ids, NodeType nodeType, Pageable pageable);

	List<JobGroup<Job>> findByConnectedNodeIdInAndNodeType(List<Long> hierarchyNodeIds, NodeType instance);

	@Query("SELECT s FROM JobGroup s WHERE  (upper(s.name) like upper(:name) or  upper(description) like upper(:description) ) and nodeType = :nodeType and s.connectedNode.id = :connectedNodeId ")
	Page<JobGroup> findByNameLikeOrDescriptionLikeOrNodeTypeAndConnectedNodeId(@Param("name") String name, @Param("description") String description,
			@Param("nodeType") NodeType nodeType, @Param("connectedNodeId") long connectedNodeId, Pageable pageable);

	@Query("SELECT s FROM JobGroup s WHERE  (upper(s.name) like upper(:name) or  upper(description) like upper(:description))  and nodeType = :nodeType")
	Page<JobGroup> findByNameLikeOrDescriptionLikeOrNodeType(@Param("name") String name, @Param("description") String description, @Param("nodeType") NodeType nodeType,
			Pageable pageable);

	@Query("SELECT s FROM JobGroup s WHERE s.id in (:ids) and nodeType= :nodeType")
	Page<JobGroup> findByIdInAndNodeType(@Param("ids") List<Long> ids, @Param("nodeType") NodeType nodeType, Pageable pageable);

	@Query("select distinct j.connectedNode from JobGroup j where j.id in (:ids)) and j.nodeType = '2' ")
	Page<HierarchyNode> findParentHierarchyNodes(@Param("ids") List<Long> ids, Pageable pageable);

	@Query("SELECT c FROM JobGroup c where ((:date   >= c.workTime.startTime.actualTime and c.workTime.finishTime.actualTime  >=  :date) or(:date   >= c.workTime.startTime.actualTime and c.workTime.finishTime.actualTime  is null)) and c.nodeType = '2' and c.description like :name")
	Page<JobGroup<Job>> onGoingJobGroups(@Param("date") Date date, @Param("name") String name, Pageable pageable);

	@Query("SELECT c FROM JobGroup c where  :date  >= c.workTime.finishTime.actualTime  and c.workStatus.runningStatus = '3' and c.nodeType = '2' and c.description like :name")
	Page<JobGroup<Job>> completedJobGroups(@Param("date") Date date, @Param("name") String name, Pageable pageable);

	@Query("select c from JobGroup c JOIN c.workSchedule.scheduledStages s where s.percentageOfWork = 0.0 and COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) >= :date and  s.actionTime.actualTime IS NULL and c.nodeType = '2' and c.description like :name")
	Page<JobGroup<Job>> plannedJobs(@Param("date") Date date, @Param("name") String name, Pageable pageable);

	@Query("select c from JobGroup c JOIN c.jobs j where j.id in (:jobIds) and c.nodeType = '2' ")
	Page<JobGroup> findJobGroupWithJobIds(@Param("jobIds") Collection<Long> jobIds, Pageable pageable);

	@Query("select c from JobGroup c JOIN c.workSchedule.scheduledStages s where s.percentageOfWork = 0.0 and COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) between :startDate and :endDate and  s.actionTime.actualTime IS NULL and c.nodeType = '2' and c.description like :name ")
	Page<JobGroup> jobsStartInNDays(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("name") String jobName, Pageable pageable);

	@Query("select c from JobGroup c JOIN c.workSchedule.scheduledStages s where s.percentageOfWork = 100.0 and COALESCE(s.actionTime.revisedTime,s.actionTime.estimatedTime) between :startDate and :endDate and  s.actionTime.actualTime IS NULL and c.nodeType = '2' and c.description like :name")
	Page<JobGroup> jobsCompleteInNDays(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("name") String jobName, Pageable pageable);

}

// Page<JobGroup> findByNameLikeOrDescriptionLikeOrNodeType(String name,
// String description, NodeType nodeType, Pageable pageable);

// @Query("SELECT s FROM JobGroup s WHERE (upper(s.name) like upper(:name)
// or upper(description) like upper(:description)) and nodeType = :nodeType
// ")
// Page<JobGroup> findByNameLikeOrDescriptionLikeOrNodeType(@Param("name")
// String name, @Param("description") String description, @Param("nodeType")
// NodeType nodeType,
// Pageable pageable);

// @Query("SELECT s FROM JobGroup s WHERE s.connectedNode.id in (:ids) and (
// s.parent.id = :ParentId or s.parent.id = null)")
// Page findByconnectedNodeIdInAndParentId(@Param("ids") List<Long> ids,
// @Param("ParentId") long ParentId, Pageable pageable);

// @Query("SELECT s FROM JobGroup s WHERE s.connectedNode.id in (:ids) and
// s.isStandard = false ")

