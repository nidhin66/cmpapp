
package com.gvg.syena.core.services.schedulerjob.jobs;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.services.NotificationService;
import com.gvg.syena.core.api.services.notification.NotificationRecord;
import com.gvg.syena.core.datarepository.notification.NotificationConfigRepository;
import com.gvg.syena.core.services.notification.NotificationServiceImpl;

@Service("notifyJob")
public class NotiFyJob extends QuartzJobBean implements ApplicationContextAware{
	

NotificationConfigRepository notificationConfigRepository;


	private NotificationService notificationService;

	private static ApplicationContext applicationContext = null;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {			
		notificationConfigRepository = applicationContext.getBean(NotificationConfigRepository.class);	
		NotificationConfig notificationConfig = notificationConfigRepository.findOne("TEST_CONFIG");
		notificationService  = applicationContext.getBean(NotificationServiceImpl.class);	
		List<NotificationRecord> records = new ArrayList<>();
		NotificationRecord record = new NotificationRecord();
		record.setRecordId("TEST_NOTIFY");
		
		records.add(record);
		notificationService.notify(notificationConfig, records);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		NotiFyJob.applicationContext = applicationContext;
		
	}
	

}

