package com.gvg.syena.core.datarepository.alert.mail;

import org.springframework.data.repository.CrudRepository;

import com.gvg.syena.core.api.entity.alert.mail.MailTemplate;

public interface MailTemplateRepository extends CrudRepository<MailTemplate, Long>{

}
