
package com.gvg.syena.core.application;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ImportResource("classpath*:/dao-config.xml")
@EnableJpaRepositories(basePackages = { "com.gvg.syena.core.datarepository" })
public class JPAConfig {

}