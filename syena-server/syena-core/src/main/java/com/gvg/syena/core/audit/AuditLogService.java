package com.gvg.syena.core.audit;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.organization.Person;

public class AuditLogService {

	final static Logger logger = Logger.getLogger(AuditLogService.class);

	private ObjectMapper objectMapper = new ObjectMapper();

	public void log(Object oldValue, Object newValue, Person person) {
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Old Value : ");
			stringBuilder.append(objectMapper.writeValueAsString(oldValue));
			stringBuilder.append("New Value : ");
			stringBuilder.append(objectMapper.writeValueAsString(newValue));
			String message = null;
			logger.trace(message);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

	}

	public void log(Object oldValue, Object newValue) {
		log(oldValue, newValue, null);
	}

}
