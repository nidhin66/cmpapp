package com.gvg.syena.core.services.wiki;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.entity.wiki.HierarchyItemWiki;
import com.gvg.syena.core.api.services.WikiManagementService;
import com.gvg.syena.core.datarepository.wiki.HierarchyItemWikiRepository;

@Service("wikiManagementService") 
public class WikiManagementServiceImpl implements WikiManagementService{
	
	
	@Autowired
	HierarchyItemWikiRepository hierarchyItemWikiRepository;

	@Override
	public void createOrUpdateWiki(HierarchyItemWiki wiki) {
		hierarchyItemWikiRepository.save(wiki);
		
	}
	
	public HierarchyItemWiki getProjectWiki(long hierarchyNodeId){
		HierarchyItemWiki wiki = hierarchyItemWikiRepository.findByHierarchyNodeId(hierarchyNodeId);
		return wiki;
	}

}
