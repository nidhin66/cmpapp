package com.gvg.syena.core.services.notification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;

import com.gvg.syena.core.api.entity.notification.MessageTemplate;
import com.gvg.syena.core.api.services.notification.PlaceHolderData;
import com.gvg.syena.core.api.services.notification.PlaceHolderTableData;
import com.gvg.syena.core.api.services.notification.PlaceHolderType;

public class MessageFormatter {
	public static String formatMessage(MessageTemplate template, List<PlaceHolderData> placeHolders){
		String templateMessage = template.getMessage();
		Map<String, String> data = new HashMap<String, String>();
		if(null != placeHolders){
			for(PlaceHolderData placeHolder : placeHolders){
				if(placeHolder.getType() == PlaceHolderType.TABLE){
					PlaceHolderTableData tableContent = (PlaceHolderTableData) placeHolder.getValue();
					data.put(placeHolder.getPlaceHolderName(),createHtmlTable(tableContent));
				}else{
					data.put(placeHolder.getPlaceHolderName(),String.valueOf(placeHolder.getValue()));
				}
			}
		}
		
		 String varPrefix = "#{";
		    String varSuffix = "}";
		String message = StrSubstitutor.replace(templateMessage, data,varPrefix,varSuffix);
		return message;
	}
	
	
	private static String createHtmlTable(PlaceHolderTableData tableContent){
		StringBuilder sb = new StringBuilder();
		sb.append("<table border=\"1\" style=\"width:100%\">");
		
		//Add Header 
//		sb.append('<').append("th").append('>');
		
		for(String header : tableContent.getHeader()){
			sb.append('<').append("th").append('>');
		    sb.append(header);
		    sb.append("</").append("th").append('>');
		}
//		sb.append("</").append("th").append('>');
		
		for(String[] row : tableContent.getRows()){
			sb.append('<').append("tr").append('>');
			for(String column : row){
				sb.append('<').append("td").append('>');
			    sb.append(column);
			    sb.append("</").append("td").append('>');
			}
			sb.append("</").append("tr").append('>');

		}		
		sb.append("</table>");
		return sb.toString();
	}
}
