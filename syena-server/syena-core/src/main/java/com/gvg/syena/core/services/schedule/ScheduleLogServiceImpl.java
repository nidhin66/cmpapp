package com.gvg.syena.core.services.schedule;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.services.ScheduleLogService;
import com.gvg.syena.core.datarepository.scheduler.ScheduleLogRepository;

@Service("scheduleLogService")
public class ScheduleLogServiceImpl implements ScheduleLogService {

	@Autowired
	private ScheduleLogRepository scheduleLogRepository;

	@Override
	public void logSchedule(long schedulingNode, NodeCategory nodeCategory, Set<ScheduleLog> scheduleLogs) {
//		for (ScheduleLog scheduleLog : scheduleLogs) {
//			scheduleLog.setSchedulingNode(schedulingNode);
//			scheduleLog.setNodeCategory(nodeCategory);
//		}
//		scheduleLogRepository.save(scheduleLogs);
	}

}
