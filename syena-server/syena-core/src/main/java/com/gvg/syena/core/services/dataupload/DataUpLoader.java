package com.gvg.syena.core.services.dataupload;

import java.io.IOException;
import java.io.InputStream;

import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.UserActivityPermission;

public interface DataUpLoader {

	void uploadHierarchyLevel(InputStream inputStream) throws IOException;

	void uploadJobAndTask(InputStream inputStream) throws IOException;

	void uploadStandardHierarchyNodes(InputStream in) throws IOException;
	
	void uploadUserActivityPermission(UserActivityPermission userActivityPermission);
	
	void uploadPerson(Person person);

}
