package org.cpm.core.api.entity.hierarchy;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;

public class HierarchyLevelTest {

	@Test
	public void test() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		HierarchyLevel value = new HierarchyLevel("System");
		String str = objectMapper.writeValueAsString(value);
		System.out.println(str);
	}
}
