package org.cpm.core.api.entity.job.schedule;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;

public class ScheduledStageTest {

	@Test
	public void test() throws IOException {

		ObjectMapper mapper = new ObjectMapper();

		ScheduledStage scheduledStage = new ScheduledStage(new Date(), 25);
		System.out.println(scheduledStage);
		String str = mapper.writeValueAsString(scheduledStage);
		System.out.println(str);
		scheduledStage = mapper.readValue(str, ScheduledStage.class);
		System.out.println(scheduledStage);
	}

}
