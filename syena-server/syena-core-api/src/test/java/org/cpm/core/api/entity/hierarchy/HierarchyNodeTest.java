package org.cpm.core.api.entity.hierarchy;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

public class HierarchyNodeTest {

	@Test
	public void test() throws IOException {
		HierarchyLevel hierarchyLevel = new HierarchyLevel("");
		HierarchyNode hierarchyNode = new HierarchyNode("", "", hierarchyLevel, 1);
		HierarchyNode childNode = new HierarchyNode("", "", hierarchyLevel, 1);
		hierarchyNode.addChild(childNode);
		ObjectMapper mapper = new ObjectMapper();
		
		String str = mapper.writeValueAsString(hierarchyNode);
		System.out.println(str);

		mapper.readValue(str, HierarchyNode.class);
	}
}

class HierarchyNodeSerializer extends JsonSerializer<HierarchyNode> {

	@Override
	public void serialize(HierarchyNode arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException, JsonProcessingException {
//		arg0.asJson();
	}

}