package org.cpm.core.api.entity.job;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

public class JobGroupTest {

	@Test
	public void test() throws IOException {
		JobGroup<Job> jobGroup = new JobGroup<Job>("");
		Job job1 = new Job();
		jobGroup.addJob(job1);
		ObjectMapper objectMapper = new ObjectMapper();
		String str = objectMapper.writeValueAsString(jobGroup);
		System.out.println(str);
		objectMapper.readValue(str, JobGroup.class);

	}

}
