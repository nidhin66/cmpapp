package org.cpm.core.api.entity.job;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;

public class DependencyJobsTest {

	@Test
	public void test() throws JsonProcessingException {
		DependencyJobs dependencyJobs = new DependencyJobs();
		Job job = new Job();
		dependencyJobs.addJob(job , DependencyType.FinishToStart);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.getSerializationConfig();
		String str = objectMapper.writeValueAsString(dependencyJobs);
		System.out.println(str);
	}

}
