package org.cpm.core.api.entity.job;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;

public class CheckListTest {

	@Test
	public void test() throws JsonProcessingException {

		ObjectMapper objectMapper = new ObjectMapper();
		List<CheckList> listCheckList = new ArrayList<CheckList>();
		CheckList c1 = new CheckList("one", true);
		CheckListItem checkListItem1 = new CheckListItem("one.one");
		c1.addCheckListItem(checkListItem1);
		listCheckList.add(c1);
		String str = objectMapper.writeValueAsString(listCheckList);
		System.out.println(str);
	}

}
