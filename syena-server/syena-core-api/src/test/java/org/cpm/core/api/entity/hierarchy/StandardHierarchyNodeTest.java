package org.cpm.core.api.entity.hierarchy;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.util.View;

public class StandardHierarchyNodeTest {

	@Test
	public void test() throws JsonProcessingException {

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
		HierarchyLevel hierarchyLevel = new HierarchyLevel("1");
		StandardHierarchyNode<StandardHierarchyNode> stn1 = new StandardHierarchyNode<>("name", "description", hierarchyLevel);
		StandardHierarchyNode<StandardHierarchyNode> stn2 = new StandardHierarchyNode<>("name", "description", hierarchyLevel);
		stn1.addChild(stn2);
		// Set<StandardHierarchyNode> children = new
		// TreeSet<StandardHierarchyNode>();
		// StandardHierarchyNode e = new
		// StandardHierarchyNode<StandardHierarchyNode>();
		// children.add(e);
		// stn1.setChildren(children);
		String str = objectMapper.writerWithView(View.ClientSerilize.class).writeValueAsString(stn1);
		System.out.println(str);
	}

}
