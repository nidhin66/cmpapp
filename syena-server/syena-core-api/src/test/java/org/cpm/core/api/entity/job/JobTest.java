package org.cpm.core.api.entity.job;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.PersonDays;

public class JobTest {
//	@Test
	public void json() throws IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		Job job1 = new Job("", "");

		// String str = objectMapper.writeValueAsString(job1);
		// System.out.println(str);
		// objectMapper.readValue(str, Job.class);

		java.util.List<Job> content = new ArrayList<Job>();
		content.add(job1);
		PageItr<Job> pageItr = new PageItr<Job>(content, 0, 1);
		String str1 = objectMapper.writeValueAsString(pageItr);
		System.out.println(str1);
		TypeReference<PageItr<Job>> typer = new TypeReference<PageItr<Job>>() {
		};
		objectMapper.readValue(str1, typer);

	}

	// @Test
	public void testAnalyseDependency() {
		Job job1 = new Job();
		job1.setId(1);
		Job job2 = new Job();
		job2.setId(2);
		// job2.addDependentJob(job1);

		// job2.getPathtInDependentJobs(1);

		Job job3 = new Job();
		job3.setId(3);
		// job3.addDependentJob(job2);

		// job3.getPathtInDependentJobs(1);

		Job job4 = new Job();
		job4.setId(4);
		// job2.addDependentJob(job4);

		// job3.getPathtInDependentJobs(4);

	}

	@Test
	public void workTimeInJob() throws JsonProcessingException {
		Job job1 = new Job("", "");
		Duration duration = new PersonDays(10);
//		Configuration.EnableWorkTimeInJob = true;
		job1.setDuration(duration);
		ObjectMapper mapper = new ObjectMapper();
		String str = mapper.writeValueAsString(job1);
		System.out.println(str);

	}

}
