package com.gvg.syena.core.api.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStageComapritor;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.PersonDays;

public class ApplicationUtil {

	private final static ScheduledStageComapritor scheduledStageComapritor = new ScheduledStageComapritor();

	static Calendar calendar = Calendar.getInstance();

	public static ScheduledStageComapritor getScheduledStageComapritor() {
		return scheduledStageComapritor;
	}

	public static SortedSet<ScheduledStage> getSystemDefaultScheduledStages() {

		TreeSet<ScheduledStage> scheduledstages = new TreeSet<ScheduledStage>(ApplicationUtil.getScheduledStageComapritor());
		// scheduledstages.add(new ScheduledStage(new Date(), new Date(), 0));
		// scheduledstages.add(new ScheduledStage(new Date(), new Date(), 25));
		// scheduledstages.add(new ScheduledStage(new Date(), new Date(), 50));
		// scheduledstages.add(new ScheduledStage(new Date(), new Date(), 75));
		// scheduledstages.add(new ScheduledStage(new Date(), new Date(), 100));
		scheduledstages.add(new ScheduledStage(0));
		scheduledstages.add(new ScheduledStage(25));
		scheduledstages.add(new ScheduledStage(50));
		scheduledstages.add(new ScheduledStage(75));
		scheduledstages.add(new ScheduledStage(100));
		return scheduledstages;
	}

	public static Date getDate() {
		return new Date();
	}

	public static WorkSchedule getSystemDefaultWorkSchedule(WorkTime worktime) {

		// scheduledStages.first().setEstimatedDate(today());
		// scheduledStages.last().setEstimatedDate(today());
		return new WorkSchedule(worktime);
	}

	public static Duration getDuration(Date first, Date last) {
		return new PersonDays(0);
	}

	public static Duration getDurationZero() {
		return new PersonDays(0);
	}

	public static boolean isDateBetween(Date date, Date startTime, Date endTime) {
		if (date.equals(startTime) || date.equals(endTime)) {
			return true;
		} else if (date.after(startTime) && date.before(endTime)) {
			return true;
		}
		return false;

	}

	public static WorkSchedule deepCopy(WorkSchedule workSchedule) {

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(workSchedule);
			oos.flush();
			oos.close();
			bos.close();
			byte[] byteData = bos.toByteArray();

			ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
			WorkSchedule object = (WorkSchedule) new ObjectInputStream(bais).readObject();
			return object;
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static <T> List<T> setToList(Set<T> set) {
		List<T> list = new ArrayList<T>();
		list.addAll(set);
		return list;
	}

	public static boolean contain(long id, long[] ids) {
		for (long idn : ids) {
			if (idn == id) {
				return true;
			}
		}
		return false;
	}

	public static Date getStartTime(DependencyAttachable parent) {
		if (parent instanceof Job) {
			Job item = (Job) parent;
			return item.getWorkSchedule().getPlannedStartTime();
		} else if (parent instanceof JobGroup) {
			JobGroup item = (JobGroup) parent;
			return item.getWorkTime().getPlannedStartTime();
		}
		return null;
	}

	public static Date getFinishTime(DependencyAttachable parent) {
		if (parent instanceof Job) {
			Job item = (Job) parent;
			return item.getWorkSchedule().getPlannedFinishTime();
		} else if (parent instanceof JobGroup) {
			JobGroup item = (JobGroup) parent;
			return item.getWorkTime().getPlannedFinishTime();
		}
		return null;
	}

	public static Date calculateStartTime(Date finishTime, Duration duration) {
		if (finishTime != null) {
			return duration.calculateStatDate(finishTime);
		}
		return null;
	}

	public static Date calculateFinishTime(Date startTime, Duration duration) {
		if (startTime != null) {
			return duration.calculateEndDate(startTime);
		}
		return null;
	}

	public static int getTotalPageCount(int max, int pageSize) {
		int count = max / pageSize;
		if ((max % pageSize) > 0) {
			count++;
		}
		return count;
	}

	public static void addPredecessorJobIds(Set<Job> jobs, SortedSet<Long> preJobIds) {

		for (Job job : jobs) {
			preJobIds.add(job.getId());
			JobGroup<Job> finishForStart = job.getPredecessorDependency().getFinishForStart();
			addPredecessorJobIds(finishForStart.getJobs(), preJobIds);
			addPredecessorJobIdsJG(finishForStart.getJobGroups(), preJobIds);
		}

	}

	public static void addPredecessorJobIdsJG(Set<JobGroup<Job>> jobGroups, SortedSet<Long> preJobIds) {
		for (JobGroup<Job> jobGroup : jobGroups) {
			addPredecessorJobIds(jobGroup.getJobs(), preJobIds);
		}

	}
	public static int getChartPeriod(Date fromTime, Date toTime) {
		int days = DateUtil.getNumberOfDays(fromTime, toTime);
		if (days > 12 * 7) {
			return 10;
		} else if (days > 30 * 12) {
			return 365;
		}
		if (days < 7) {
			return 1;
		}
		return 7;
	}

}
