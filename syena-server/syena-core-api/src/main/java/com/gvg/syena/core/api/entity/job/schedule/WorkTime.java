package com.gvg.syena.core.api.entity.job.schedule;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gvg.syena.core.api.entity.time.ActionTime;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.SchedulingException;

//@Entity
@Embeddable
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class WorkTime {

	// @Id
	// @GeneratedValue(strategy = GenerationType.TABLE)
	// private long id;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "estimatedTime", column = @Column(name = "start_estimatedTime")),
			@AttributeOverride(name = "actualTime", column = @Column(name = "start_actualTime")),
			@AttributeOverride(name = "revisedTime", column = @Column(name = "start_revisedTime")),
			@AttributeOverride(name = "projectedTime", column = @Column(name = "start_projectedTime")) })
	private ActionTime startTime;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "estimatedTime", column = @Column(name = "finish_estimatedTime")),
			@AttributeOverride(name = "actualTime", column = @Column(name = "finish_actualTime")),
			@AttributeOverride(name = "revisedTime", column = @Column(name = "finish_revisedTime")),
			@AttributeOverride(name = "projectedTime", column = @Column(name = "finish_projectedTime")) })
	private ActionTime finishTime;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "numberofDays", column = @Column(name = "duration_numberofDays")) })
	private PersonDays duration;

	public WorkTime() {
		this(new PersonDays(1));

	}

	public WorkTime(Duration duration) {
		this.duration = to(duration);
		init();
	}

	public WorkTime(Date startTime, Duration duration) {
		this.startTime = new ActionTime(startTime);
		this.duration = to(duration);
		this.finishTime = new ActionTime(duration.calculateEndDate(startTime));
	}

	public WorkTime(Duration duration, Date endTime) {
		this.finishTime = new ActionTime(endTime);
		this.duration = to(duration);
		this.startTime = new ActionTime(duration.calculateStatDate(endTime));
	}

	public void init() {
		this.startTime = new ActionTime();
		this.finishTime = new ActionTime();
	}

	public ActionTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ActionTime startTime) {
		this.startTime = startTime;
	}

	public ActionTime getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(ActionTime finishTime) {
		this.finishTime = finishTime;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = to(duration);
	}

	@JsonIgnore
	public Date getPlannedStartTime() {
		if (startTime == null) {
			return null;
		}
		return startTime.getPlannedTime();
	}

	@JsonIgnore
	public void setPlanedStartTime(Date startTime) {
		if (this.startTime == null) {
			this.startTime = new ActionTime(startTime);
		} else {
			this.startTime.setPlannedTime(startTime);
		}
	}

	@JsonIgnore
	public Date getPlannedFinishTime() {
		if (this.finishTime == null) {
			return null;
		}
		return this.finishTime.getPlannedTime();
	}

	@JsonIgnore
	public void setPlanedFinishTime(Date finishTime) {
		if (this.finishTime == null) {
			this.finishTime = new ActionTime(finishTime);
		} else {
			this.finishTime.setPlannedTime(finishTime);
		}
	}

	@JsonIgnore
	public Date getActualStartTime() {
		if (this.startTime == null) {
			return null;
		}
		return this.startTime.getActualTime();
	}

	@JsonIgnore
	public void setActualStartTime(Date actualStartTime) {
		if (this.startTime == null) {
			this.startTime = new ActionTime(actualStartTime);
		}
		this.startTime.setActualTime(actualStartTime);
	}

	@JsonIgnore
	public Date getActualFinishTime() {
		if (this.finishTime == null) {
			return null;
		}
		return this.finishTime.getActualTime();
	}

	@JsonIgnore
	public void setActualFinishTime(Date actualFinishTime) {
		if (this.finishTime == null) {
			this.finishTime = new ActionTime(actualFinishTime);
		}
		this.finishTime.setActualTime(actualFinishTime);
	}

	@JsonIgnore
	public void add(WorkTime workTime) {
		if (this.getPlannedStartTime().before(workTime.getPlannedStartTime())) {
			workTime.setPlanedStartTime(this.getPlannedStartTime());
		}
		if (this.getPlannedFinishTime().after(workTime.getPlannedFinishTime())) {
			workTime.setPlanedFinishTime(this.getPlannedFinishTime());
		}
		workTime.getDuration().add(workTime.getDuration());
	}

	@JsonIgnore
	public void changeStartDate(Date startTime) {
		setPlanedStartTime(startTime);
		if (duration.isLess(startTime, getPlannedFinishTime())) {
			setPlanedFinishTime(duration.calculateEndDate(startTime));
		}

	}

	@JsonIgnore
	public void changeFinishDate(Date finishTime) {
		setPlanedFinishTime(finishTime);
		if (duration.isLess(getPlannedStartTime(), finishTime)) {
			setPlanedFinishTime(duration.calculateStatDate(finishTime));
		}

	}

	@JsonIgnore
	public boolean isInBetweenStartDateAndEndDate(Date date) {
		if (getPlannedStartTime() == null || getPlannedFinishTime() == null) {
			return true;
		}
		return (date.equals(getPlannedStartTime()) || date.equals(getPlannedFinishTime())) || (date.after(getPlannedStartTime()) && date.before(getPlannedFinishTime()));
	}

	@JsonIgnore
	public void outer(Date startTime, Date finishTime) {

		if (this.getPlannedStartTime() == null || startTime.before(this.getPlannedStartTime())) {
			setPlanedStartTime(startTime);
		}
		if (this.getPlannedFinishTime() == null || finishTime.after(this.getPlannedFinishTime())) {
			setPlanedFinishTime(finishTime);
		}

	}

	@Override
	@JsonIgnore
	public String toString() {
		return "" + getPlannedStartTime() + ", " + getPlannedFinishTime() + ", " + duration;
	}

	@JsonIgnore
	public boolean isScheduled() {
		return getPlannedStartTime() != null && getPlannedFinishTime() != null;
	}

	public void enrich() throws SchedulingException {
		if (duration == null) {
			throw new SchedulingException(MessageKeys.DURATION_NULL, "duration is null");
		} else if (getPlannedStartTime() == null && getPlannedFinishTime() != null) {
			if (getPlannedFinishTime() != null && duration != null) {
				setPlanedStartTime(duration.calculateStatDate(getPlannedFinishTime()));
			} else {
				throw new SchedulingException(MessageKeys.ESTIMATED_NULL, "estimatedStartTime  and ( estmatedFinishTime or duration) are null");
			}
		} else if (getPlannedFinishTime() == null && getPlannedStartTime() != null) {
			if (getPlannedStartTime() != null && duration != null) {
				setPlanedFinishTime(duration.calculateEndDate(getPlannedStartTime()));
			} else {
				throw new SchedulingException(MessageKeys.ESTIMATED_NULL, "estmatedFinishTime and ( estimatedStartTime or duration) are null");
			}
		}
	}

	@JsonIgnore
	private PersonDays to(Duration duration2) {
		return (PersonDays) duration2;
	}

	@JsonIgnore
	public boolean isSame(WorkTime workTime) {
		// TODO Auto-generated method stub
		return false;
	}

	@JsonIgnore
	public void validate() throws SchedulingException {
		Date startPlanedTime = startTime.getPlannedTime();
		Date finishPlanedTime = finishTime.getPlannedTime();
		if (startPlanedTime == null || finishPlanedTime == null) {
			throw new SchedulingException(MessageKeys.PLANNED_NULL, "Planed start time and finish time should not be null");
		}
		boolean b = !finishPlanedTime.before(startPlanedTime);
		if (!b) {
			throw new SchedulingException(MessageKeys.PLANNED_FINISH_TIME_NOT_AFTER_PLANED_START_TIME, "Planned finish time should be after Planed start Time");
		}
	}

	public void upwardMerge(WorkTime workTime) {
		if (this.getActualStartTime() != null && workTime.getActualStartTime() != null) {
			Date actualStartTime = DateUtil.first(this.getActualStartTime(), workTime.getActualStartTime());
			this.setActualStartTime(actualStartTime);
		}
		if (this.getActualFinishTime() != null && workTime.getActualFinishTime() != null) {
			Date actualFinishTime = DateUtil.last(this.getActualFinishTime(), workTime.getActualFinishTime());
			this.setActualFinishTime(actualFinishTime);
		}
		this.duration.add(workTime.getDuration());
	}

}
