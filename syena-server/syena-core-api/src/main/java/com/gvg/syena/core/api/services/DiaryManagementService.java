package com.gvg.syena.core.api.services;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.services.diary.DiaryFilterProperties;

public interface DiaryManagementService {
	
	public DiaryThread createNewThread(DiaryThread diaryThread);
	
	public void addDiscussion(long threadId, ThreadDiscussion threadDiscussion);
	
	public PageItr<DiaryThread> getLatestThreads(int pageNumber,int pageSize);
	
	public PageItr<ThreadDiscussion> getDiscussions(long threadId,int pageNumber, int pageSize);
	
	public PageItr<DiaryThread> searchDiscussionThreads(DiaryFilterProperties filterProperties, int pageNumber, int pageSize);

}
