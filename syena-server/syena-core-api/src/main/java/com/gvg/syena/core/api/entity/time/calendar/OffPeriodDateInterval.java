package com.gvg.syena.core.api.entity.time.calendar;

import java.util.Date;

public class OffPeriodDateInterval extends DateInterval {

	private Week gweek;
	private Day gday;

	public OffPeriodDateInterval(Date statTime, Date endTime) {
		super(statTime, endTime);
	}

	public void setWeek(Week gweek) {
		this.gweek = gweek;
	}

	public void setDay(Day gday) {
		this.gday = gday;
	}

}
