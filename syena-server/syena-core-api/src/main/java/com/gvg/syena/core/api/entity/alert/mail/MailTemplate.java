package com.gvg.syena.core.api.entity.alert.mail;

import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MailTemplate {
	
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private long id; 
	
	private String name;
	
	private String message;
	
	private String fromAddress;
	
	private String subject;
	
	@ElementCollection
	private Set<MailToAddress> adrresses;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Set<MailToAddress> getAdrresses() {
		return adrresses;
	}

	public void setAdrresses(Set<MailToAddress> adrresses) {
		this.adrresses = adrresses;
	}

	
	
	

}
