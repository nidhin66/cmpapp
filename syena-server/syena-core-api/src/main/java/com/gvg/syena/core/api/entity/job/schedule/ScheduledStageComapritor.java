package com.gvg.syena.core.api.entity.job.schedule;

import java.util.Comparator;

public class ScheduledStageComapritor implements Comparator<ScheduledStage> {

	public int compare(ScheduledStage o1, ScheduledStage o2) {
		if (o1.getPercentageOfWork() > o2.getPercentageOfWork()) {
			return 1;
		} else if (o1.getPercentageOfWork() < o2.getPercentageOfWork()) {
			return -1;
		} else {
			return 0;
		}
	}

}
