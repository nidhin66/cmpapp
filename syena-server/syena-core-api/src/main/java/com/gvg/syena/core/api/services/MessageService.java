package com.gvg.syena.core.api.services;

import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.exception.MessageNotFoundException;

public interface MessageService {

	public Message getMessage(String messageId) throws MessageNotFoundException;

}
