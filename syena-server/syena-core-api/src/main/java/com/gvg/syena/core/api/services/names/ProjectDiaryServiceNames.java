package com.gvg.syena.core.api.services.names;

public interface ProjectDiaryServiceNames {
	
	String createNewThread = "createNewThread";
	String addDiscussion = "addDiscussion";
	String getLatestThreads = "getLatestThreads";
	String getDiscussions = "getDiscussions";
	String searchDiscussionThreads =  "searchDiscussionThreads";

}
