package com.gvg.syena.core.api.entity.job.checklist;

public enum CheckListType {
	initiationCheckList, completionCheckList, infoCheckList;
}
