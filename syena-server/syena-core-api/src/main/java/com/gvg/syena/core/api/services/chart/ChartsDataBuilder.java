package com.gvg.syena.core.api.services.chart;

import java.util.Date;

import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.NotScheduledException;

public interface ChartsDataBuilder {

	ChartData<Date> getChartsDataOfHierarchyNode(long hierarchyNodeId, ChartType[] chartTypes) throws HierarchyNodeNotFoundException, NotScheduledException;

	ChartData<Date> getChartsDataOfJobGroup(long jobGroupId, ChartType[] chartTypes) throws HierarchyNodeNotFoundException, NotScheduledException;

	ChartData<Date> getChartsDataOfJob(long jobId, ChartType[] chartTypes) throws HierarchyNodeNotFoundException, NotScheduledException;

}
