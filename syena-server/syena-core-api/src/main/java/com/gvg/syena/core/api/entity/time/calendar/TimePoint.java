package com.gvg.syena.core.api.entity.time.calendar;

import java.util.Date;

import javax.persistence.Embeddable;

import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.exception.CalanderException;

@Embeddable
public class TimePoint {

	private int hr;

	private int min;

	private int sec;
	
	
	@OnlyForJPA
	public TimePoint() {
	}

	public TimePoint(int hr, int min) throws CalanderException {
		this(hr, min, 0);
	}

	public TimePoint(int hr, int min, int sec) throws CalanderException {
		if (hr < 0 || hr > 23) {
			throw new CalanderException("hr should be  0 to 23");
		}
		if (min < 0 || min > 59) {
			throw new CalanderException("min should be 0 to 59");
		}
		if (sec < 0 || sec > 59) {
			throw new CalanderException("sec should be 0 to  59");
		}

		this.hr = hr;
		this.min = min;
		this.sec = sec;
	}

	public int getHr() {
		return hr;
	}

	public void setHr(int hr) {
		this.hr = hr;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getSec() {
		return sec;
	}

	public void setSec(int sec) {
		this.sec = sec;
	}

	public static TimePoint getTimePoint(Date statTime) {
		// TODO Auto-generated method stub
		return null;
	}

}