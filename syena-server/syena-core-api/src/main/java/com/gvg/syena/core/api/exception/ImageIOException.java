package com.gvg.syena.core.api.exception;


public class ImageIOException extends ApplicationException {

	private long hierarchyNodeId;
	private String imageName;

	public ImageIOException(long hierarchyNodeId, String imageName, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.hierarchyNodeId = hierarchyNodeId;
		this.imageName = imageName;

	}

	public ImageIOException(long hierarchyNodeId, String imageName, Exception e) {
		super(e);
	}

}
