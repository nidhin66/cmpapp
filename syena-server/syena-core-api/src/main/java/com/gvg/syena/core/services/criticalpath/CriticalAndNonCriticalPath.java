package com.gvg.syena.core.services.criticalpath;

import java.util.List;

public class CriticalAndNonCriticalPath {

	private List<JobData> criticalPath;

	private List<JobData> nonCriticalPath;

	public CriticalAndNonCriticalPath() {
	}

	public CriticalAndNonCriticalPath(List<JobData> criticalPath, List<JobData> nonCriticalPath) {
		this.criticalPath = criticalPath;
		this.nonCriticalPath = nonCriticalPath;
	}

	public List<JobData> getCriticalPath() {
		return criticalPath;
	}

	public void setCriticalPath(List<JobData> criticalPath) {
		this.criticalPath = criticalPath;
	}

	public List<JobData> getNonCriticalPath() {
		return nonCriticalPath;
	}

	public void setNonCriticalPath(List<JobData> nonCriticalPath) {
		this.nonCriticalPath = nonCriticalPath;
	}

}
