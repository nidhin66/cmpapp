package com.gvg.syena.core.api.services.serach;


public class SearchCriteria<T> {

	Class<T> entityClass = null;

	public SearchCriteria(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public Class<T> getEntityClass() {
		return entityClass;
	}

}
