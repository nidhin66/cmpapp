package com.gvg.syena.core.api.services.serach;

import java.util.Date;

import com.gvg.syena.core.api.entity.common.WorkStatus;

public class JobProperties {

	private long jobId;
	private String description;
	private String name;
	private String ownerId;
	private String initiatorId;
	private WorkStatus workStatus;
	private Date startDate;
	private Date endDate;
	private String executor;

	private int milestoneCompletedPercentage;
	private String milestoneDescription;
	private long connectedNodeId;
	private boolean isStandard;

	private DateRange plannedDateRange;
	private DateRange actualDateRange;

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getInitiatorId() {
		return initiatorId;
	}

	public void setInitiatorId(String initiatorId) {
		this.initiatorId = initiatorId;
	}

	public int getMilestoneCompletedPercentage() {
		return milestoneCompletedPercentage;
	}

	public void setMilestoneCompletedPercentage(int milestoneCompletedPercentage) {
		this.milestoneCompletedPercentage = milestoneCompletedPercentage;
	}

	public WorkStatus getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(WorkStatus workStatus) {
		this.workStatus = workStatus;
	}

	public String getMilestoneDescription() {
		return milestoneDescription;
	}

	public void setMilestoneDescription(String milestoneDescription) {
		this.milestoneDescription = milestoneDescription;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getConnectedNodeId() {
		return this.connectedNodeId;
	}

	public void setConnectedNodeId(long connectedNodeId) {
		this.connectedNodeId = connectedNodeId;
	}

	public boolean isStandard() {
		return isStandard;
	}

	public void setStandard(boolean isStandard) {
		this.isStandard = isStandard;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	public String getExecutor() {
		return executor;
	}

	public DateRange getPlannedDateRange() {
		return plannedDateRange;
	}

	public void setPlannedDateRange(DateRange plannedDateRange) {
		this.plannedDateRange = plannedDateRange;
	}

	public DateRange getActualDateRange() {
		return actualDateRange;
	}

	public void setActualDateRange(DateRange actualDateRange) {
		this.actualDateRange = actualDateRange;
	}

}

