package com.gvg.syena.core.api.services;


import com.gvg.syena.core.api.entity.wiki.HierarchyItemWiki;

public interface WikiManagementService {
	
	public void createOrUpdateWiki(HierarchyItemWiki wiki);
	
	public HierarchyItemWiki getProjectWiki(long hierarchyNodeId);


}
