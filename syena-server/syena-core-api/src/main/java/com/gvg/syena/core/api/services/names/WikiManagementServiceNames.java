package com.gvg.syena.core.api.services.names;

public interface WikiManagementServiceNames {
	
	String createOrUpdateWiki = "createOrUpdateWiki";
	String getProjectWiki = "getProjectWiki";

}
