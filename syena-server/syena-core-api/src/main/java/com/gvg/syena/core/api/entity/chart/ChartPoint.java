package com.gvg.syena.core.api.entity.chart;

/**
 * @author geevarughesejohn
 *
 * @param <T>
 */
public class ChartPoint<T> {

	private T point;

	private Float[] values;

	public ChartPoint() {

	}

	public ChartPoint(T point, Float[] values) {
		this.point = point;
		this.values = values;
	}

	public T getPoint() {
		return point;
	}

	public void setPoint(T point) {
		this.point = point;
	}

	public Float[] getValues() {
		return values;
	}

	public void setValues(Float[] values) {
		this.values = values;
	}

	public void setValues(int i, Float t) {
		values[i] = t;

	}

}
