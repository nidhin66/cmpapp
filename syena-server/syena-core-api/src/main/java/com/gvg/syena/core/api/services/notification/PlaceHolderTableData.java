package com.gvg.syena.core.api.services.notification;

import java.util.ArrayList;
import java.util.List;

public class PlaceHolderTableData {
	
	private int columnCount;
	
	private String[] header;
	
	private List<String[]> rows;
	
	public PlaceHolderTableData(int columnCount){
		this.columnCount = columnCount;
	}

	public int getColumnCount() {
		return columnCount;
	}

	

	public String[] getHeader() {
		return header;
	}

	public void setHeader(String[] header) {
		this.header = header;
	}

	public List<String[]> getRows() {
		return rows;
	}
	
	
	public void addRow(String[] row){
		if(null == rows){
			rows = new ArrayList<String[]>();
		}
		rows.add(row);
	}
	

}
