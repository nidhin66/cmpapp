package com.gvg.syena.core.api.entity.time.calendar;

import java.util.Date;
import java.util.Iterator;

import com.gvg.syena.core.api.exception.CalanderException;

public class DateInterval {

	private Date statTime;

	private Date endTime;

	public DateInterval(Date statTime, Date endTime) {
		if (!endTime.after(statTime)) {
			throw new CalanderException("endTime should be after than statTime ");
		}
		this.statTime = statTime;
		this.endTime = endTime;
	}

	public Date getStatTime() {
		return statTime;
	}

	public void setStatTime(Date statTime) {
		this.statTime = statTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Iterator<Date> dayIterator() {
		// TODO Auto-generated method stub
		return null;
	}

}
