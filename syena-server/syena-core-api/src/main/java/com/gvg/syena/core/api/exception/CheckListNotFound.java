package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.MessageKeys;

public class CheckListNotFound extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long checkListItemId;

	public CheckListNotFound(long checkListItemId, String messageString) {
		super(MessageKeys.CHECKLIST_ITEM_NOT_FOUND, messageString);
		this.checkListItemId = checkListItemId;
	}

	public long getCheckListItemId() {
		return checkListItemId;
	}

	public void setCheckListItemId(long checkListItemId) {
		this.checkListItemId = checkListItemId;
	}

}
