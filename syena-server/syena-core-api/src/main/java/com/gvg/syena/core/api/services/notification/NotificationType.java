package com.gvg.syena.core.api.services.notification;

public enum NotificationType {
	ALERT("A","Alert"),WARN("W","Warning"),INFO("I","Info");
	
	private String code;
	private String description;
	
	NotificationType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
	
	
}
