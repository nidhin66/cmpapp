package com.gvg.syena.core.api.entity.common;

import java.util.Date;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class WorkStatus {

	private boolean delayToStart = false;

	private boolean delayToEnd = false;

	private boolean delayInCurrentStage = false;

	private RunningStatus runningStatus = RunningStatus.NotScheduled;

	public WorkStatus() {
	}

	public WorkStatus(boolean delayToStart, boolean delayToEnd, RunningStatus runningStatus) {
		this.delayToStart = delayToStart;
		this.delayToEnd = delayToEnd;
		this.runningStatus = runningStatus;
	}

	public boolean isDelayToStart() {
		return delayToStart;
	}

	public void setDelayToStart(boolean delayToStart) {
		this.delayToStart = delayToStart;
	}

	public boolean isDelayToEnd() {
		return delayToEnd;
	}

	public void setDelayToEnd(boolean delayToEnd) {
		this.delayToEnd = delayToEnd;
	}

	public RunningStatus getRunningStatus() {
		return runningStatus;
	}

	public void setRunningStatus(RunningStatus runningStatus) {
		this.runningStatus = runningStatus;
	}

	public void setDelayInCurrentStage(boolean delayInCurrentStage) {
		this.delayInCurrentStage = delayInCurrentStage;
	}

	public boolean isDelayInCurrentStage() {
		return delayInCurrentStage;
	}

	public static WorkStatus computeStatus(Date estimatedStartDate, Date estimatedEndDate, Date actualStartDate, Date actualEndDate) {
		WorkStatus workStatus = new WorkStatus();

		workStatus.setRunningStatus(RunningStatus.notStarted);

		if (actualEndDate != null) {
			workStatus.setRunningStatus(RunningStatus.completed);
			if (estimatedEndDate.after(actualEndDate)) {
				workStatus.setDelayToEnd(true);
			}
			return workStatus;
		}

		if (actualStartDate != null) {
			workStatus.setRunningStatus(RunningStatus.onprogress);
			if (estimatedStartDate.after(actualStartDate)) {
				workStatus.setDelayToStart(true);
			}
		}
		return workStatus;

	}

	public boolean isEqual(WorkStatus workStatus) {
		boolean isEqualdelayToStart = !(this.isDelayToStart() ^ workStatus.isDelayToStart());
		boolean isEqualdelayToEnd = !(this.isDelayToEnd() ^ workStatus.isDelayToEnd());
		boolean isEqualRunningStatus = this.getRunningStatus() == workStatus.getRunningStatus();
		return isEqualdelayToStart && isEqualdelayToEnd && isEqualRunningStatus;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(runningStatus);
		if (delayToStart) {
			stringBuilder.append("delay to start");
		} else {
			stringBuilder.append("no delay to start");
		}

		if (delayToEnd) {
			stringBuilder.append("delay to end");
		} else {
			stringBuilder.append("no delay to end");
		}

		return stringBuilder.toString();
	}

	@JsonIgnore
	public boolean isStarted() {
		return runningStatus.isStarted();
	}

	@JsonIgnore
	public boolean isCompleted() {
		return runningStatus.isCompleted();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof WorkStatus)) {
			return false;
		}
		WorkStatus workStatus = (WorkStatus) obj;
		return this.delayToStart == workStatus.delayToStart && this.delayToEnd == workStatus.delayToEnd && this.getRunningStatus().equals(workStatus.getRunningStatus());
	}

}
