package com.gvg.syena.core.api.services;

import java.util.Set;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobExecutionException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.JobsBarrierException;
import com.gvg.syena.core.api.exception.RollBackException;
import com.gvg.syena.core.api.exception.SchedulingException;

public interface ScheduleManagment {

	// WorkStatus startValidationJob(long jobId) throws JobNotFoundException,
	// JobExecutionException, JobsBarrierException;

	WorkStatus finishValidationJob(long jobId) throws JobNotFoundException, JobExecutionException, JobsBarrierException;

	Set<ScheduleLog> setJobSchedule(long jobId, WorkSchedule timeSchedule, int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed, JobScheduleFailed,
			InvalidDataException, JobsBarrierException;

	void setJobGroupSchedule(long jobGrouId, WorkTime workTime) throws JobScheduleFailed, SchedulingException, InvalidDataException;

	Set<ScheduleLog> setJobGroupSchedule(long jobGroupId, WorkSchedule timeSchedule, int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed, JobScheduleFailed,
			InvalidDataException, JobsBarrierException;

	Set<ScheduleLog> setHierarchybSchedule(long hierarchyId, WorkTime timeSchedule) throws JobScheduleFailed, HierarchyNodeNotFoundException, SchedulingException, InvalidDataException;

	void setUnApprovedJobSchedule(long jobId, WorkSchedule workSchedule, int personDays) throws JobNotFoundException;

	void setUnApprovedHierarchybSchedule(long hierarchyId, WorkTime workTime) throws HierarchyNodeNotFoundException;

	void setUnApprovedJobGroupSchedule(long jobGrouId, WorkTime workTime) throws JobNotFoundException;

	void setUnApprovedJobGroupSchedule(long jobGrouId, WorkSchedule workSchedule, int personDays) throws JobNotFoundException;

	WorkSchedule getPendingForApprovelJobSchedules(long jobId) throws JobNotFoundException;

	PageItr<Job> getPendingForApprovelJobSchedules(int pageNumber, int pageSize);

	PageItr<JobGroup> getPendingForApprovelJobGroupSchedules(int pageNumber, int pageSize);

	PageItr<HierarchyNode> getPendingForApprovelSchedules(String level, int pageNumber, int pageSize);

	void removeUnApprovedJobSchedule(long jobId);

	void removeUnApprovedHierarchybSchedule(long hierarchyId);

	void removeUnApprovedJobGroupSchedule(long jobGrouId);

	void rejectUnApprovedJobSchedule(long jobId);

	void rejectUnApprovedHierarchybSchedule(long hierarchyId);

	void rejectUnApprovedJobGroupSchedule(long jobGrouId);

	void validateJobGroupWorkSchedule(long jobGrouId, WorkSchedule workSchedule, int personDays) throws SchedulingException, JobNotFoundException, JobScheduleFailed,
			InvalidDataException, JobsBarrierException, RollBackException;

	void validateJobSchedule(long jobId, WorkSchedule workSchedule, int personDays) throws JobNotFoundException, JobScheduleFailed, SchedulingException, InvalidDataException,
			JobsBarrierException, RollBackException;

	void rejectUnApprovedJobGroupWorkSchedule(long jobGrouId);

	PageItr<JobGroup<Job>> getPendingForApprovelJobGroupWorkSchedules(int pageNumber, int pageSize);

	Set<ScheduleLog> validateHierarchybSchedule(long hierarchyId, WorkTime workTime) throws JobScheduleFailed, HierarchyNodeNotFoundException, SchedulingException,
			InvalidDataException, RollBackException;

}
