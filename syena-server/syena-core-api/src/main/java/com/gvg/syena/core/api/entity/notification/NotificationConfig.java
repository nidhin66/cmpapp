package com.gvg.syena.core.api.entity.notification;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.gvg.syena.core.api.services.notification.NotificationDeleteType;
import com.gvg.syena.core.api.services.notification.NotificationJobType;
import com.gvg.syena.core.api.services.notification.NotificationType;

@Entity
public class NotificationConfig {
	
	@Id
	private String configId;	
	
	private String name;
	
	@Enumerated(EnumType.STRING)
	private NotificationType type;
	
	@Enumerated(EnumType.STRING)
	private NotificationJobType jobType;
	
	private long offlineJobId;
	
	private NotificationDeleteType deleteType;
	
	private int afterAlertInterval;
	
	@ManyToOne
	private MessageTemplate messageTemplate;
	
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@OrderBy("level ASC")	
	private List<Escalation> escalations;

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public NotificationJobType getJobType() {
		return jobType;
	}

	public void setJobType(NotificationJobType jobType) {
		this.jobType = jobType;
	}

	public long getOfflineJobId() {
		return offlineJobId;
	}

	public void setOfflineJobId(long offlineJobId) {
		this.offlineJobId = offlineJobId;
	}

	public MessageTemplate getMessageTemplate() {
		return messageTemplate;
	}

	public void setMessageTemplate(MessageTemplate messageTemplate) {
		this.messageTemplate = messageTemplate;
	}

	public NotificationDeleteType getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(NotificationDeleteType deleteType) {
		this.deleteType = deleteType;
	}

	public int getAfterAlertInterval() {
		return afterAlertInterval;
	}

	public void setAfterAlertInterval(int afterAlertInterval) {
		this.afterAlertInterval = afterAlertInterval;
	}

	public List<Escalation> getEscalations() {
		return escalations;
	}

	public void setEscalations(List<Escalation> escalations) {
		this.escalations = escalations;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
