package com.gvg.syena.core.api.entity.time;

import java.util.Calendar;
import java.util.Date;

import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.util.ApplicationDateUtil;
import com.gvg.syena.core.api.util.Configuration;

public class DateUtil {

	private static Calendar c = Calendar.getInstance();

	public static Date today() {
		return ApplicationDateUtil.getApplicationDate(Configuration.dateDeference);
	}

	public static boolean equals(Date date, Date datas) {
		if (datas == null || date == null) {
			return false;
		}
		c.setTime(datas);
		Date datasc = c.getTime();
		c.setTime(date);
		Date datec = c.getTime();
		return datasc.equals(datec);
	}

	public static boolean after(Date date, Date basedate) {
		if (date == null || basedate == null) {
			return false;
		}
		c.setTime(date);
		Date d1 = c.getTime();
		c.setTime(basedate);
		Date d2 = c.getTime();
		return d1.after(d2);
	}

	public static boolean before(Date date, Date basedate) {
		if (date == null || basedate == null) {
			return false;
		}
		c.setTime(date);
		Date d1 = c.getTime();
		c.setTime(basedate);
		Date d2 = c.getTime();
		return d1.before(d2);
	}

	public static Date first(Date date1, Date date2) {
		if (date1 == null) {
			return date2;
		}
		if (date2 == null) {
			return date1;
		}
		c.setTime(date1);
		Date d1 = c.getTime();
		c.setTime(date2);
		Date d2 = c.getTime();
		if (d1.before(d2)) {
			return date1;
		} else {
			return date2;
		}

	}

	public static Date last(Date date1, Date date2) {
		c.setTime(date1);
		Date d1 = c.getTime();
		c.setTime(date2);
		Date d2 = c.getTime();
		if (d1.before(d2)) {
			return date2;
		} else {
			return date1;
		}
	}

	public static int getNumberOfDays(WorkSchedule timeSchedule) {
		Date first = timeSchedule.getScheduledStages().first().getPlannedTime();
		Date last = timeSchedule.getScheduledStages().last().getPlannedTime();
		if (first == null || last == null) {
			return 0;
		}
		int days = (int) ((last.getTime() - first.getTime()) / (1000 * 60 * 60 * 24));
		return days;
	}

	public static int getNumberOfDays(Date date1, Date date2) {
		int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24));
		return days;
	}

	public static boolean isInRange(Date startTime1, Date endTime1, Date startTime2, Date endTime2, boolean isInclude) {
		boolean a = (after(startTime1, startTime2) && before(startTime1, endTime2)) || (after(endTime1, startTime2) && before(endTime1, endTime2));
		if (a || !isInclude) {
			return a;
		}
		boolean b = (equals(startTime1, startTime2)) || (equals(startTime1, endTime2)) || (equals(endTime1, startTime2)) || (equals(endTime1, endTime2));
		return a || b;
	}

	public static boolean between(Date date, Date date1, Date data2) {
		boolean a = after(date, date1) && before(date, data2);
		return a;
	}
}
