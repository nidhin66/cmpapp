package com.gvg.syena.core.api.entity.job.schedule;

import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.time.ActionTime;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.calendar.WorkCalendar;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.SchedulingException;

/**
 * @author geevarugehsejohn
 *
 */
@Entity
public class ScheduledStage implements Comparable<ScheduledStage> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String description;

	private float percentageOfWork;

	@Embedded
	private ActionTime actionTime;

	@ElementCollection
	private List<Date> revisedDates;

	@Transient
	// Temparay use
	private Date predictedTime;

	// @ElementCollection
	// private Map<PredictionStrategy, Date> predictionDates;

	@OnlyForJPA
	public ScheduledStage() {
		this.actionTime = new ActionTime();
	}

	public ScheduledStage(float percentageOfWork) {
		this();
		this.percentageOfWork = percentageOfWork;
	}

	public ScheduledStage(Date estimatedTime, float percentageOfWork) {
		this(percentageOfWork);
		setPlannedTime(estimatedTime);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPercentageOfWork() {
		return percentageOfWork;
	}

	public void setPercentageOfWork(float percentageOfWork) {
		this.percentageOfWork = percentageOfWork;
	}

	// public Date getRevisedTime() {
	// return this.actionTime.getRevisedTime();
	// }
	//
	// public void setRevisedTime(Date revisedTime) {
	// this.actionTime.setRevisedTime(revisedTime);
	// }
	@JsonIgnore
	public Date getPlannedTime() {
		if (this.actionTime == null) {
			return null;
		}
		return this.actionTime.getPlannedTime();
	}

	@JsonIgnore
	public Date getActualTime() {
		if (this.actionTime == null) {
			return null;
		}
		return this.actionTime.getActualTime();
	}

	@JsonIgnore
	public void setPlannedTime(Date planedTime) {
		if (this.actionTime == null) {
			this.actionTime = new ActionTime();
		}
		this.actionTime.setPlannedTime(planedTime);
	}

	@JsonIgnore
	public void setActualTime(Date actualTime) {
		if (this.actionTime == null) {
			this.actionTime = new ActionTime();
		}
		this.actionTime.setActualTime(actualTime);
	}

	@JsonIgnore
	public boolean isEndBeforeStart(ScheduledStage next) {
		return getPlannedTime().before(next.getPlannedTime());

	}

	@JsonIgnore
	public void validate(WorkCalendar workCalendar) throws SchedulingException {
		if (getActualTime() != null) {
			if (getPlannedTime() != null) {
				workCalendar.beforeOrOn(getPlannedTime(), getActualTime());
			} else {
				throw new SchedulingException(MessageKeys.ESTIMATEDDATE_NOT_NULL, "should need to configure estimatedDate");
			}
		}

	}

	@JsonIgnore
	public boolean isActualEnterd() {
		return getActualTime() != null;
	}

	@JsonIgnore
	public boolean isEstimated() {
		return getPlannedTime() != null;

	}

	@Override
	public int compareTo(ScheduledStage o) {
		if (percentageOfWork > o.getPercentageOfWork()) {
			return 1;
		} else if (percentageOfWork < o.getPercentageOfWork()) {
			return -1;
		} else {
			return 0;
		}
	}

	@JsonIgnore
	public void merge(ScheduledStage s) {
		this.description = s.getDescription();
		setActualTime(s.getActualTime());
		setPlannedTime(s.getPlannedTime());
	}

	@JsonIgnore
	public Date getRevisedDate() {
		if (revisedDates == null || revisedDates.size() <= 0) {
			return getPlannedTime();
		}
		return revisedDates.get(0);
	}

	public ActionTime getActionTime() {
		return actionTime;
	}

	public void setActionTime(ActionTime actionTime) {
		this.actionTime = actionTime;
	}

	public List<Date> getRevisedDates() {
		return revisedDates;
	}

	public void setRevisedDates(List<Date> revisedDates) {
		this.revisedDates = revisedDates;
	}

	public Date getPredictedTime() {
		return predictedTime;
	}

	public void setPredictedTime(Date predictedTime) {
		this.predictedTime = predictedTime;
	}

	// @JsonIgnore
	// public Date getProjectedTime() {
	// if (actionTime == null) {
	// return null;
	// } else {
	// return actionTime.getProjectedTime();
	// }
	// }

	public boolean hasDelay() {
		Date plannedTime = getPlannedTime();
		Date actualTime = getActualTime();
		if (actualTime == null) {
			actualTime = DateUtil.today();
		}
		return DateUtil.after(actualTime, plannedTime);

	}

	@JsonIgnore
	public float getPercentageValue() {
		return percentageOfWork / 100;
	}

	public ScheduledStage copy() {
		ScheduledStage scheduledStageCopy = new ScheduledStage();
		scheduledStageCopy.setDescription(description);
		scheduledStageCopy.setPercentageOfWork(percentageOfWork);
		scheduledStageCopy.setActionTime(actionTime.copy());
		return scheduledStageCopy;
	}

	// @JsonIgnore
	// public boolean isAfter(ScheduledStage preScheduledStage) {
	// Date preActualTime = preScheduledStage.getActualTime();
	// boolean actr = true;
	// if (this.getActualTime() != null && preActualTime != null) {
	// actr = actr && !this.getActualTime().before(preActualTime);
	// }
	// if (this.getPlannedTime() != null && preScheduledStage.getPlannedTime()
	// != null) {
	// actr = actr &&
	// !this.getPlannedTime().before(preScheduledStage.getPlannedTime());
	// }
	// return actr;
	// }
	//
	// public boolean isBefore(ScheduledStage nextScheduledStage) {
	// Date nextActualTime = nextScheduledStage.getActualTime();
	// boolean actr = true;
	// if (this.getActualTime() != null && nextActualTime != null) {
	// actr = actr && !this.getActualTime().after(nextActualTime);
	// }
	// if (this.getPlannedTime() != null && nextScheduledStage.getPlannedTime()
	// != null) {
	// actr = actr &&
	// !this.getPlannedTime().after(nextScheduledStage.getPlannedTime());
	// }
	// return actr;
	// }

	// public ScheduledStage copy() {
	// ScheduledStage scheduledStage = new ScheduledStage();
	// scheduledStage.setActionTime(actionTime);
	// scheduledStage.setActualTime(actualTime);
	// scheduledStage.set
	// return scheduledStage;
	// }

}
