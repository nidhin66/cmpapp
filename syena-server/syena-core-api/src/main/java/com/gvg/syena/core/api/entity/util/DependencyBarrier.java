package com.gvg.syena.core.api.entity.util;

import java.util.List;

import com.gvg.syena.core.api.entity.job.Job;

public class DependencyBarrier {

	private List<Job> barriersNotStarted;

	private List<Job> barriersNotEnded;

	public DependencyBarrier() {
	}

	public void setBarriersNotStarted(List<Job> barriersNotStarted) {
		this.barriersNotStarted = barriersNotStarted;

	}

	public void setBarriersNotEnded(List<Job> barriersNotEnded) {
		this.barriersNotEnded = barriersNotEnded;

	}

	public List<Job> getBarriersNotEnded() {
		return barriersNotEnded;
	}

	public List<Job> getBarriersNotStarted() {
		return barriersNotStarted;
	}

	public boolean isEmpty() {
		return barriersNotStarted.isEmpty() && barriersNotEnded.isEmpty();
	}

}
