package com.gvg.syena.core.api.services.names;

public interface CriticalPathServiceNames {

	String findCriticalPath = "findCriticalPath";
	String findNonCriticalPath = "findNonCriticalPath";
	String findCriticalAndNonCriticalPath = "findCriticalAndNonCriticalPath";

}
