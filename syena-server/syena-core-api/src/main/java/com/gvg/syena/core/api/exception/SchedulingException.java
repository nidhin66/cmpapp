package com.gvg.syena.core.api.exception;

public class SchedulingException extends ApplicationException{

	public SchedulingException(String messageKey, String messageString) {
		super(messageKey, messageString);
	}

	
}
