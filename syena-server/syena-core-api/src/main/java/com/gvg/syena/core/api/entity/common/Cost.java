package com.gvg.syena.core.api.entity.common;

import java.math.BigDecimal;
import java.util.Currency;

import javax.persistence.Embeddable;

/**
 * @author geevarugehsejohn
 *
 */
@Embeddable
public class Cost {

	private BigDecimal amount;

	private Currency currency;

	@OnlyForJPA
	public Cost() {
	}

	public Cost(Cost cost) {
		this.amount = new BigDecimal(cost.getAmount().doubleValue());
		this.currency = Currency.getInstance(currency.getCurrencyCode());
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
