package com.gvg.syena.core.api.entity.time.calendar;

import java.util.List;

import com.gvg.syena.core.api.exception.CalanderException;

public class DailyWorkAnyTime  implements DailyWorkTime{

	private List<TimePointInterval> workPeriods;
	private TimePoint startTime;
	private TimePoint endTime;
	private int workingHrs;
	private int addtionalMintes;

	public DailyWorkAnyTime(TimePoint startTime, TimePoint endTime, int workingHrs, int addtionalMintes) throws CalanderException {
		if (workingHrs > 24) {
			throw new CalanderException("workingHrs should be less than 24");
		}
		if (addtionalMintes >= 60) {
			throw new CalanderException("addtionalMintes should be less than 60");
		}
		this.startTime = startTime;
		this.endTime = endTime;
		this.workingHrs = workingHrs;
		this.addtionalMintes = addtionalMintes;
	}

	public List<TimePointInterval> getWorkPeriods() {
		return workPeriods;
	}

	public void setWorkPeriods(List<TimePointInterval> workPeriods) {
		this.workPeriods = workPeriods;
	}

	public TimePoint getStartTime() {
		return startTime;
	}

	public void setStartTime(TimePoint startTime) {
		this.startTime = startTime;
	}

	public TimePoint getEndTime() {
		return endTime;
	}

	public void setEndTime(TimePoint endTime) {
		this.endTime = endTime;
	}

	public int getWorkingHrs() {
		return workingHrs;
	}

	public void setWorkingHrs(int workingHrs) {
		this.workingHrs = workingHrs;
	}

	public int getAddtionalMintes() {
		return addtionalMintes;
	}

	public void setAddtionalMintes(int addtionalMintes) {
		this.addtionalMintes = addtionalMintes;
	}

}
