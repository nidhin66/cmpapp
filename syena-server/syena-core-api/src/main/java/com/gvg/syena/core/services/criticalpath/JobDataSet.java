package com.gvg.syena.core.services.criticalpath;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.gvg.syena.core.api.util.DateIterator;

public class JobDataSet {

	private Set<JobDataPeriod> criticalPathPeriods;

	public JobDataSet(final Date fromTime, final Date toTime) {
		this.criticalPathPeriods = new TreeSet<JobDataPeriod>();
		DateIterator graphPoints = new DateIterator(fromTime, toTime, 7);
		Date previousDate = null;
		while (graphPoints.hasNext()) {
			Date date = graphPoints.next();
			if (previousDate != null) {
				JobDataPeriod criticalPathPeriod = new JobDataPeriod(previousDate, date);
				criticalPathPeriods.add(criticalPathPeriod);
			}
			previousDate = date;
		}
	}

	public void add(List<JobData> findCriticalPathJob) {
		if (findCriticalPathJob != null) {
			for (JobData criticalPathJobData : findCriticalPathJob) {
				for (JobDataPeriod criticalPathPeriod : criticalPathPeriods) {
					criticalPathPeriod.put(criticalPathJobData);
				}

			}
		}

	}

	public Set<JobDataPeriod> getCriticalPathPeriods() {
		return criticalPathPeriods;
	}

	public void setCriticalPathPeriods(Set<JobDataPeriod> criticalPathPeriods) {
		this.criticalPathPeriods = criticalPathPeriods;
	}
	
	

}
