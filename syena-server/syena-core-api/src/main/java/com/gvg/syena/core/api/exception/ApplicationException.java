package com.gvg.syena.core.api.exception;

public class ApplicationException extends Exception {

	private static final long serialVersionUID = 1L;
	private String messageId;
	private String messageString;

	public ApplicationException(String messageId, String messageString) {
		this(messageId, messageString, null);
	}

	public ApplicationException(String messageKey, String messageString, Exception e) {
		super(messageString, e);
		this.messageId = messageKey;
		this.messageString = messageString;
	}

	public ApplicationException(Exception e) {
		super(e);
	}

	public String getMessageId() {
		return messageId;
	}

	public  String getMessageString() {
		return messageString;
	}
}
