package com.gvg.syena.core.api.exception;


public class JobExecutionException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long jobId;

	public JobExecutionException(long jobId, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.jobId = jobId;
	}

}
