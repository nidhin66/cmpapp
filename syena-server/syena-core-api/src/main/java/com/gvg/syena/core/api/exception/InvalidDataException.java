package com.gvg.syena.core.api.exception;

public class InvalidDataException extends ApplicationException {

	public InvalidDataException(String messageId, String messageString) {
		super(messageId, messageString);
	}

}
