package com.gvg.syena.core.api.entity.diary;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ThreadDiscussionId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long threadId;
	private long sequence;
	public long getThreadId() {
		return threadId;
	}
	public void setThreadId(long threadId) {
		this.threadId = threadId;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	
}
