package com.gvg.syena.core.api;

import com.gvg.syena.core.api.entity.job.DependencyJobs;

/**
 * @author geevarughesejohn
 *
 */
public interface DependencyAttachable {

	DependencyJobs getPredecessorDependency();

	DependencyJobs getSuccessorDependency();

//	boolean isEqual(DependencyAttachable dependencyAttachable);

}
