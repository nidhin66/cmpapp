package com.gvg.syena.core.api.entity.util;

import java.util.Set;
import java.util.TreeSet;

import com.gvg.syena.core.api.entity.job.DependencyType;

public class DependencyMetrix {

	private Set<DependencyType> dependencyTypes;

	public DependencyMetrix(DependencyType dependencyType) {
		dependencyTypes = new TreeSet<>();
		add(dependencyType);
	}

	public boolean hasCross(DependencyType dependencyType, boolean inverse) {
		boolean a1 = dependencyType.isEqual(DependencyType.startToFinish.inverse(inverse)) && contain(DependencyType.FinishToStart);
		boolean a2 = dependencyType.isEqual(DependencyType.FinishToStart.inverse(inverse)) && contain(DependencyType.startToFinish);
		return a1 || a2;
	}

	public void add(DependencyType dependencyType) {
		dependencyTypes.add(dependencyType);
	}

	public static boolean checkSide(DependencyMetrix dependencyMetrixDirect, DependencyMetrix dependencyMetrixInverse) {
		boolean a1 = dependencyMetrixDirect.contain(DependencyType.startToStart) && dependencyMetrixInverse.contain(DependencyType.startToStart.inverse(true));
		boolean a2 = dependencyMetrixDirect.contain(DependencyType.FinishToFinish) && dependencyMetrixInverse.contain(DependencyType.FinishToFinish.inverse(true));
		return a1 || a2;

	}

	private boolean contain(DependencyType dependencyType) {
		return dependencyTypes.contains(dependencyType);

	}

}
