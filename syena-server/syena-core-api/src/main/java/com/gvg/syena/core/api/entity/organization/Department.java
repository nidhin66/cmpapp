package com.gvg.syena.core.api.entity.organization;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;

@Entity
public class Department implements Comparable<Department> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(unique = true)
	private String name;

	private String description;

	@ManyToMany
	private List<Person> users;

	@OnlyForJPA
	public Department() {
	}

	public Department(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Person> getUsers() {
		return users;
	}

	public void setUsers(List<Person> users) {
		this.users = users;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Department)) {
			return false;
		} else {
			Department department = (Department) obj;
			boolean idEqu = department.getId() == this.getId();
			boolean idName;
			if (!(this.name == null)) {
				idName = this.name.equals(department.getName());
			} else {
				idName = this.name == department.getName();
			}
			return idEqu && idName;
		}

	}

	@Override
	@JsonIgnore
	public String toString() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

	public int compareTo(Department o) {
		if (!equals(0)) {
			return 1;
		}
		return 0;
	}

}
