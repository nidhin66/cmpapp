package com.gvg.syena.core.api.services.names;

public interface PermissionManagementServiceNames {

	String setPermissionUser = "setPermissionUser";
	String setPermissionRole = "setPermissionRole";
	String checkPermission = "checkPermission";

}
