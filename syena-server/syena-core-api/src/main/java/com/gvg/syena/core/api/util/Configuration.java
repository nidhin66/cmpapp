package com.gvg.syena.core.api.util;

import com.gvg.syena.core.api.entity.time.calendar.PersonDaysWorkCalendar;
import com.gvg.syena.core.api.entity.time.calendar.WorkCalendar;

public class Configuration {

	public static WorkCalendar personDaysCPMCalendar = new PersonDaysWorkCalendar();

	public static boolean childEnable = false;

	public static boolean EnableWorkTimeInJob = false;

	public static int dateDeference = 0;

}
