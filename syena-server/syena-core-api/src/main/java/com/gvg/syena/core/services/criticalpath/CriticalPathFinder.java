package com.gvg.syena.core.services.criticalpath;

import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;

public interface CriticalPathFinder {

//	List<JobData> findCriticalPath(long hierarchyNodeId) throws HierarchyNodeNotFoundException, NotScheduledException;
//
//	List<JobData> findNonCriticalPath(long hierarchyNodeId) throws HierarchyNodeNotFoundException, NotScheduledException;

	CriticalAndNonCriticalPath findCriticalAndNonCriticalPath(long hierarchyNodeId) throws HierarchyNodeNotFoundException;

}
