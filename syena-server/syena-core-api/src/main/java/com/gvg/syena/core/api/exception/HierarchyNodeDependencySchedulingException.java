package com.gvg.syena.core.api.exception;

public class HierarchyNodeDependencySchedulingException extends ApplicationException{

	public HierarchyNodeDependencySchedulingException(String messageKey, String messageString, Exception e) {
		super(messageKey, messageString, e);
	}

}
