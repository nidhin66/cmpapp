package com.gvg.syena.core.api.exception;


public class SearchException extends ApplicationException {

	
	private static final long serialVersionUID = 1L;

	public SearchException(String messageKey, String messageString, Exception e) {
		super(messageKey, messageString, e);
	}

}
