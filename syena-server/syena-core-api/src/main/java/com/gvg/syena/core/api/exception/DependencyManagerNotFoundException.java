package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.NodeCategory;

public class DependencyManagerNotFoundException extends ApplicationException {

	private static final long serialVersionUID = 1L;
	private NodeCategory nodeType;

	public DependencyManagerNotFoundException(NodeCategory nodeType, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.nodeType = nodeType;
	}

	public NodeCategory getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeCategory nodeType) {
		this.nodeType = nodeType;
	}

}
