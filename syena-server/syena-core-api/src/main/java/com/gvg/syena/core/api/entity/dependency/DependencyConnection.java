package com.gvg.syena.core.api.entity.dependency;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;

@Embeddable
public class DependencyConnection implements Serializable {

	@OneToOne
	private HierarchyNode predecessorNode;

	@OneToOne
	private HierarchyNode successorNode;

	public DependencyConnection() {
		// TODO Auto-generated constructor stub
	}

	public DependencyConnection(HierarchyNode predecessorNode, HierarchyNode successorNode) {
		this.predecessorNode = predecessorNode;
		this.successorNode = successorNode;
	}

	public HierarchyNode getPredecessorNode() {
		return predecessorNode;
	}

	public void setPredecessorNode(HierarchyNode predecessorNode) {
		this.predecessorNode = predecessorNode;
	}

	public HierarchyNode getSuccessorNode() {
		return successorNode;
	}

	public void setSuccessorNode(HierarchyNode successorNode) {
		this.successorNode = successorNode;
	}

}