package com.gvg.syena.core.api.entity.job.schedule;

import java.util.Date;

import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.time.ActionTime;

public enum WorkPercentageType {
	Actual(new ActualWorkPercentageTypeProcess(), ChartCreatorType.Base), Planned(new PlannedWorkPercentageTypeProcess(), ChartCreatorType.Base), Revised(
			new RevisedWorkPercentageTypeProcess(), ChartCreatorType.Base), Projected(new ProjectedWorkPercentageTypeProcess(), ChartCreatorType.Projected), planningefficiency(
			new PredictedWorkPercentageTypeProcess(), ChartCreatorType.planningefficiency), scheduleefficiency(new PredictedWorkPercentageTypeProcess(),
			ChartCreatorType.scheduleefficiency), workefficiency(new PredictedWorkPercentageTypeProcess(), ChartCreatorType.workefficiency);

	private WorkPercentageTypeProcess workPercentageTypeProcess;
	private ChartCreatorType chartCreatorType;

	private WorkPercentageType(WorkPercentageTypeProcess workPercentageTypeProcess, ChartCreatorType chartCreatorType) {
		this.workPercentageTypeProcess = workPercentageTypeProcess;
		this.chartCreatorType = chartCreatorType;
	}

	public Float getPercentageComplete(Job job, Date date) {
		ScheduledStage scheduledStage = job.getWorkSchedule().getScheduledstagesWith(date, this);
		return scheduledStage.getPercentageOfWork();
	}

	public Date getDate(ScheduledStage scheduledstage) {
		return workPercentageTypeProcess.getDate(scheduledstage);
	}

	public ChartCreatorType getChartCreatorType() {
		return chartCreatorType;
	}

}

interface WorkPercentageTypeProcess {

	Date getDate(ScheduledStage scheduledstage);

}

class ActualWorkPercentageTypeProcess implements WorkPercentageTypeProcess {

	@Override
	public Date getDate(ScheduledStage scheduledstage) {
		return scheduledstage.getActualTime();
	}

}

class PlannedWorkPercentageTypeProcess implements WorkPercentageTypeProcess {

	@Override
	public Date getDate(ScheduledStage scheduledstage) {
		ActionTime actionTime = scheduledstage.getActionTime();
		if (actionTime == null) {
			return null;
		}
		return actionTime.getEstimatedTime();
	}

}

class RevisedWorkPercentageTypeProcess implements WorkPercentageTypeProcess {

	@Override
	public Date getDate(ScheduledStage scheduledstage) {
		return scheduledstage.getRevisedDate();
	}

}

class PredictedWorkPercentageTypeProcess implements WorkPercentageTypeProcess {

	@Override
	public Date getDate(ScheduledStage scheduledstage) {
		return scheduledstage.getPredictedTime();
	}

}

class ProjectedWorkPercentageTypeProcess implements WorkPercentageTypeProcess {

	@Override
	public Date getDate(ScheduledStage scheduledstage) {
		return scheduledstage.getPredictedTime();
	}

}
