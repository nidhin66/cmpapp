package com.gvg.syena.core.api;

import com.gvg.syena.core.api.entity.common.HierarchyItem;

/**
 * @author geevarughesejohn
 *
 */
public interface DependencyAttachableHierarchy extends DependencyAttachable, HierarchyItem {

	DependencyAttachableHierarchy getParent();

}
