package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.MessageKeys;

public class HierarchyNodeNotFoundException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long hierarchyNodeId;

	public HierarchyNodeNotFoundException(long hierarchyNodeId, String messageString) {
		super(MessageKeys.HIERARCHY_NODE_NOT_FOUND, messageString);
		this.hierarchyNodeId = hierarchyNodeId;
	}

	public long getHierarchyNodeId() {
		return hierarchyNodeId;
	}

	public void setHierarchyNodeId(long hierarchyNodeId) {
		this.hierarchyNodeId = hierarchyNodeId;
	}

}
