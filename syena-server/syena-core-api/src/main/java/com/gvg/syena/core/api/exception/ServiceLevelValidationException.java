package com.gvg.syena.core.api.exception;

public class ServiceLevelValidationException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public ServiceLevelValidationException(String messageKey, String messageString) {
		super(messageKey, messageString);
	}

}
