package com.gvg.syena.core.api.entity.job.checklist;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.entity.common.Item;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.ValidationException;

/**
 * @author geevarugehsejohn
 *
 */
@Entity
public class CheckListItem implements Item<CheckListItem>, Comparable<CheckListItem> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private boolean isCompleted;

	@Column(nullable = false)
	private String name;

	@Temporal(TemporalType.DATE)
	private Date completedDate;

	private boolean isStandard;

	@ManyToOne
	private Person completedPerson;

	@ManyToOne
	@JsonIgnore
	private CheckList parent;

	public CheckListItem() {
	}

	public CheckListItem(CheckListItem checkListItem) {
		this.name = checkListItem.getName();
	}

	public CheckListItem(String name) {
		this.name = name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(CheckListItem o) {
		if (this.name.equals(o.getName())) {
			return 0;
		} else {
			return 1;
		}
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public boolean isStandard() {
		return isStandard;
	}

	public void setStandard(boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Person getCompletedPerson() {
		return completedPerson;
	}

	public void setCompletedPerson(Person completedPerson) {
		this.completedPerson = completedPerson;
	}

	@Override
	@JsonIgnore
	public String getDescription() {
		return name;
	}

	// @Override
	// @JsonIgnore
	// public Set<? extends HierarchyItem> getChildren() {
	// return null;
	// }

	@Override
	@JsonIgnore
	public boolean isEquual(CheckListItem dependent) {
		return this.getId() == dependent.getId();
	}

	public void validate() throws ValidationException {
		if (this.name == null || this.name.trim() == "") {
			throw new ValidationException(MessageKeys.CHECKLIST_ITEM_NAME_NULL, "Check List item name null");
		}
	}

	// @Override
	// @JsonIgnore
	// public CheckList getParent() {
	// return this.parent;
	// }

	// @Override
	// @JsonIgnore
	// public NodeCategory getNodeCategory() {
	// return NodeCategory.checklistItem;
	// }

}
