package com.gvg.syena.core.api.entity.alert.mail;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.gvg.syena.core.api.entity.common.MailAddressType;

@Entity
public class MailToAddress {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id; 
	
	private String name;
	
	@Enumerated(EnumType.STRING)
	private MailAddressType type;
	
	
	
	private long groupEntityId;
	
	private String mailId;
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MailAddressType getType() {
		return type;
	}

	public void setType(MailAddressType type) {
		this.type = type;
	}

	

	public long getGroupEntityId() {
		return groupEntityId;
	}

	public void setGroupEntityId(long groupEntityId) {
		this.groupEntityId = groupEntityId;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

		
	

}
