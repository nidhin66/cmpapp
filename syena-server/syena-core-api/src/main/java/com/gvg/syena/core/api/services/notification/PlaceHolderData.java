package com.gvg.syena.core.api.services.notification;

public class PlaceHolderData {
	
	private String placeHolderName;
	
	private PlaceHolderType type;
	
	private Object value;
	
	
	
	public PlaceHolderData(String name,PlaceHolderType type) {
		this.placeHolderName = name;
		this.type = type;
	}



	public Object getValue() {
		return value;
	}



	public void setValue(Object value) {
		this.value = value;
	}



	public String getPlaceHolderName() {
		return placeHolderName;
	}



	public PlaceHolderType getType() {
		return type;
	}
	
	

}
