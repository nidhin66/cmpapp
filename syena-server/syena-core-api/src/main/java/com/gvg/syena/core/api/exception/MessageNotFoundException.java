package com.gvg.syena.core.api.exception;

public class MessageNotFoundException extends ApplicationException {

	private String messageKey;

	public MessageNotFoundException(String messageId) {
		super("MessageNotFound", "MessageNotFound");
		this.messageKey = messageId;
	}

	public String getMessageKey() {
		return messageKey;
	}
}
