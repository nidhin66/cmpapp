package com.gvg.syena.core.api.services.names;

public interface HierarchyManagementServiceNames {

	String getLinkedHierarchyNodes = "getLinkedHierarchyNodes";
	String updateHierarchyNode = "updateHierarchyNode";
	String obsoleteHierarchyNode = "obsoleteHierarchyNode";
	String getHierarchyLevels = "getHierarchyLevels";
	String createHierarchyNode = "createHierarchyNode";
	String createStandardHierarchyNodes = "createStandardHierarchyNodes";
	String createHierarchyLevel = "createHierarchyLevel";
	String createProject = "createProject";
	String linkHierarchyNodes = "linkHierarchyNodes";
	String unlinkHierarchyNodes = "unlinkHierarchyNodes";
	String getChildHierarchyNodeIds = "getChildHierarchyNodeIds";
	String searchInHierarchyNodes = "searchInHierarchyNodes";
	String getHierarchyNodes = "getHierarchyNodes";
	String getObsoleteHierarchyNodes = "getObsoleteHierarchyNodes";
	String getAllStandardHierarchyNodes = "getAllStandardHierarchyNodes";
	String getAllHierarchyNodes = "getAllHierarchyNodes";
	String searchHierarchyNodesInChild = "searchHierarchyNodesInChild";
	String searchHierarchyNodesInLevel = "searchHierarchyNodesInLevel";
	String getChildHierarchyNodes = "getChildHierarchyNodes";
	String getHierarchyNodeIds = "getHierarchyNodeIds";
	String linkJobs = "linkJobs";
	String getLinkedJob = "getLinkedJob";
	String getChildStandardHierarchyNode = "getChildStandardHierarchyNode";
	String saveAsHierarchyNode = "saveAsHierarchyNode";
	String createAndlinkJobGroups = "createAndlinkJobGroups";
	String getLinkedJobGroups = "getLinkedJobGroups";
	String createAndLinkHierarchyNodes = "createAndLinkHierarchyNodes";
	String createAndlinkJob = "createAndlinkJob";
	String getLinkedStandardHierarchyNodes = "getLinkedStandardHierarchyNodes";
	String getHierarchyNodeImage = "getHierarchyNodeImage";
	String uploadHierarchyNodeImage = "uploadHierarchyNodeImage";
	String uploadHierarchyNodeDoc = "uploadHierarchyNodeDoc";
	String getHierarchyNodeDocs = "getHierarchyNodeDocs";
	String createHierarchyNodes = "createHierarchyNodes";
	String getParentHierarchyNodes = "getParentHierarchyNodes";
	String createStandardHierarchyNode = "createStandardHierarchyNode";
	String getJobGroupssByConnectedNodeIdsOtherThan = "getJobGroupssByConnectedNodeIdsOtherThan";
	String searchStandardHierarchyNodesInLevel = "searchStandardHierarchyNodesInLevel";
	String createPuchListJob = "createPuchListJob";
	String createPuchListJobGroup = "createPuchListJobGroup";
	String removeHierarchyNodeLink = "removeHierarchyNodeLink";
	String removeLinkedJobs = "removeLinkedJobs";
	String removeLinkedJobGroups = "removeLinkedJobGroups";
}
