package com.gvg.syena.core.api.entity.job.schedule;

import java.util.Date;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.common.RunningStatus;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.calendar.WorkCalendar;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.SchedulingException;
import com.gvg.syena.core.api.util.ApplicationUtil;

/**
 * @version
 * @author geevarugehsejohn
 *
 */

@Embeddable
public class WorkSchedule {

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy("percentageOfWork ASC")
	private SortedSet<ScheduledStage> scheduledStages;

	@OnlyForJPA
	public WorkSchedule() {
	}

	public WorkSchedule(WorkTime workTime) {
		this.scheduledStages = ApplicationUtil.getSystemDefaultScheduledStages();
		if (workTime.getStartTime() != null) {
			this.scheduledStages.first().setPlannedTime(workTime.getStartTime().getPlannedTime());
		}
		if (workTime.getFinishTime() != null) {
			this.scheduledStages.last().setPlannedTime(workTime.getFinishTime().getPlannedTime());
		}
	}

	public SortedSet<ScheduledStage> getScheduledStages() {
		return scheduledStages;
	}

	public void setScheduledStages(SortedSet<ScheduledStage> scheduledStages) {
		this.scheduledStages = scheduledStages;
	}

	@JsonIgnore
	public Date getPlannedStartTime() {
		return this.getScheduledStages().first().getPlannedTime();

	}

	@JsonIgnore
	public Date getPlannedFinishTime() {
		return this.getScheduledStages().last().getPlannedTime();
	}

	@JsonIgnore
	private ScheduledStage getFirstScheduledStage() {
		return getScheduledStages().first();

	}

	// change to compare start
	@Deprecated
	@JsonIgnore
	public boolean isStartBeforeStart(WorkSchedule timeSchedule) {
		return getPlannedStartTime().before(timeSchedule.getPlannedStartTime());

	}

	// change to compare end
	@Deprecated
	@JsonIgnore
	public boolean isStartBeforeEnd(WorkSchedule timeSchedule) {
		return getPlannedStartTime().before(timeSchedule.getPlannedFinishTime());
	}

	@Deprecated
	@JsonIgnore
	public boolean isEndBeforeEnd(WorkSchedule timeSchedule) {
		return getPlannedFinishTime().before(timeSchedule.getPlannedFinishTime());
	}

	@Deprecated
	@JsonIgnore
	public boolean isEndBeforeStart(WorkSchedule timeSchedule) {
		return getPlannedFinishTime().before(timeSchedule.getPlannedStartTime());
	}

	@JsonIgnore
	public void validate(WorkCalendar workCalendar) throws SchedulingException {

		ScheduledStage first = getScheduledStages().first();
		if (first.getPercentageOfWork() != 0) {
			throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_FIRST_PERCENTAGEOFWORK_NOT_ZERO, "percentageOfWork of fisrt stage should be zero");
		}
		if (first.getPlannedTime() == null) {
			throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_ESTIMATED_START_DATE_NULL, "Estimated start Date sholud not be null");
		}
		ScheduledStage last = getScheduledStages().last();
		if (last.getPercentageOfWork() != 100) {
			throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_LAST_PERCENTAGEOFWORK_NOT_HUNDRED, "percentageOfWork of fisrt stage should be 100");
		}
		if (last.getPlannedTime() == null) {
			throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_LAST_DATE_NULL, "Estimated end Date sholud not be null");
		}
		if (last.getActualTime() != null && first.getActualTime() == null) {

			throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_FIRST_ACTUAL_TIME_NULL, "Actual time of first milestone should not be null");
		}

		Iterator<ScheduledStage> iterator = getScheduledStages().iterator();

		Date firstDate = first.getPlannedTime();

		Date preActualTime = null;

		Date prePlannedTime = null;

		while (iterator.hasNext()) {

			ScheduledStage scheduledStage = iterator.next();

			if (firstDate != null && scheduledStage.getActualTime() != null && DateUtil.before(scheduledStage.getActualTime(), firstDate)) {
				// throw new
				// SchedulingException(MessageKeys.SCHEDULEDSTAGE_ACTUAL_TIME_BEFORE_PLANNED_TIME,
				// "Actual Time should not before planned time");
			}

			scheduledStage.validate(workCalendar);

			Date actualTime = scheduledStage.getActualTime();
			if (actualTime != null) {
				if (DateUtil.after(actualTime, DateUtil.today())) {
					throw new SchedulingException(MessageKeys.ACTUALTIME_GREATER_THAN_TODAY, "actualTime should not be greater than today in " + scheduledStage.getDescription());
				}
				if (preActualTime != null) {

					if (actualTime.before(preActualTime)) {
						throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_ACTUAL_TIME_BEFORE_PRESCHEDULEDSTAGE, "preScheduledStage before scheduledStage - "
								+ scheduledStage.getPercentageValue() + "%");

					}
				}
				preActualTime = actualTime;
			}

			Date plannedTime = scheduledStage.getPlannedTime();

			if (plannedTime != null) {
				if (prePlannedTime != null) {

					if (plannedTime.before(prePlannedTime)) {
						throw new SchedulingException(MessageKeys.SCHEDULEDSTAGE_PLANNED_TIME_BEFORE_PRESCHEDULEDSTAGE, "PreScheduledStage before scheduledStage  "
								+ scheduledStage.getPercentageValue() + "%");

					}

				}

				prePlannedTime = plannedTime;
			}
			// if (preScheduledStage != null) {
			// if (!scheduledStage.isAfter(preScheduledStage)) {
			// throw new SchedulingException("",
			// "scheduledStage After preScheduledStage");
			// }
			// if (!preScheduledStage.isBefore(scheduledStage)) {
			// throw new SchedulingException("",
			// "scheduledStage After preScheduledStage");
			// }
			// }
			// preScheduledStage = scheduledStage;
			// }

		}

	}

	@JsonIgnore
	public void changePlannedStartTime(Date time) {
		ScheduledStage first = getScheduledStages().first();
		first.setPlannedTime(time);
	}

	@JsonIgnore
	public void changePlannedFinishTime(Date time) {
		ScheduledStage last = getScheduledStages().last();
		last.setPlannedTime(time);

	}

	@JsonIgnore
	public ScheduledStage getCurrentScheduledStage() {
		// ScheduledStage scheduledStageW = null;
		// for (ScheduledStage scheduledStage : getScheduledStages()) {
		// if (!scheduledStage.isActualEnterd()) {
		// scheduledStageW = scheduledStage;
		//
		// }
		// }
		// if (scheduledStageW == null) {
		// scheduledStageW = scheduledStages.last();
		// }
		// return scheduledStageW;
		return getScheduledstagesWith(DateUtil.today(), WorkPercentageType.Actual);

	}

	@JsonIgnore
	public float getPercentageOfComplete(Date date) {
		ScheduledStage scheduledstage = getScheduledstagesWith(date, WorkPercentageType.Actual);
		if (scheduledstage == null) {
			return 0;
		}
		return scheduledstage.getPercentageOfWork();
	}

	@JsonIgnore
	public Duration getActualTimeSpent(Duration totalDuration, double percentage) {
		Duration copy = totalDuration.copy();
		copy.multiple(percentage / 100);
		return copy;
		// Date first = getFirstScheduledStage().getActualTime();

		// return ApplicationUtil.getDuration(first, current);

	}

	// @JsonIgnore
	// public Duration getEstimatedTimeSpent() {
	// // double percentage = getPercentageOfComplete(new Date());
	// // ScheduledStage scheduledstage = getScheduledstagesWith(percentage,
	// WorkPercentageType.Planned);
	// // Date first = getFirstScheduledStage().getPlannedTime();
	// // if (first == null || scheduledstage == null) {
	// // return ApplicationUtil.getDurationZero();
	// // }
	// // return ApplicationUtil.getDuration(first,
	// scheduledstage.getPlannedTime());
	// }

	@JsonIgnore
	public ScheduledStage getScheduledstagesWith(Date date, WorkPercentageType workPercentageType) {

		// Date first =
		// workPercentageType.getDate(getScheduledStages().first());
		// // Date last =
		// workPercentageType.getDate(getScheduledStages().last());
		// if (first == null || date.before(first)) {
		// // return null;
		// return getScheduledStages().first();
		// }
		// if (last == null || date.after(last)) {
		// // return null;
		// return getScheduledStages().last();
		// }

		ScheduledStage[] scheduledStageArray = new ScheduledStage[getScheduledStages().size()];
		getScheduledStages().toArray(scheduledStageArray);
		int i = scheduledStageArray.length - 1;
		Date data1 = workPercentageType.getDate(scheduledStageArray[i]);
		if (DateUtil.after(date, data1) || DateUtil.equals(date, data1)) {
			return scheduledStageArray[i];
		}
		for (; i >= 1; i--) {
			Date date2 = workPercentageType.getDate(scheduledStageArray[i--]);
			if (data1 != null && date2 != null) {
				if (DateUtil.between(date, date2, data1) || DateUtil.equals(date, date2)) {
					return scheduledStageArray[i];
				}
			}
			if (date2 != null) {
				data1 = date2;
			}
		}
		return scheduledStageArray[i];
	}

	@JsonIgnore
	private ScheduledStage getScheduledstagesWith(double percentage, WorkPercentageType workPercentageType) {
		for (ScheduledStage scheduledstage : getScheduledStages()) {
			if (scheduledstage.getPercentageOfWork() == percentage) {
				return scheduledstage;
			}
		}
		return null;
	}

	// // @JsonIgnore
	// // public ScheduledStage addScheduledStage(Date estimatedDate, double
	// // percentageOfWork) throws TimeScheduleException {
	// // ScheduledStage scheduledStage = new ScheduledStage(estimatedDate,
	// // percentageOfWork);
	// // addScheduledStage(scheduledStage);
	// // return scheduledStage;
	// //
	// // }

	// @JsonIgnore
	// private void setEstimatedDate(ScheduledStage scheduledStage, Date date,
	// Duration duration) {
	// date = duration.setDayStartTime(date);
	// scheduledStage.setPlannedTime(date);
	//
	// }

	// @JsonIgnore
	// private void setActualDate(ScheduledStage scheduledStage, Date date,
	// Duration duration) {
	// date = duration.setDayEndTime(date);
	// scheduledStage.setActualTime(date);
	// }

	// @JsonIgnore
	// public boolean isStatOnOrAfter(Date startTime, WorkCalendar workCalendar)
	// {
	// ScheduledStage first = scheduledstages.first();
	// return workCalendar.OnOrAfter(first.getPlannedTime(), startTime);
	// }

	// @JsonIgnore
	// public boolean isEndOnOrBefore(Date endTime, WorkCalendar workCalendar) {
	// ScheduledStage last = scheduledstages.last();
	// return workCalendar.beforeOrOn(last.getPlannedTime(), endTime);
	// }

	// @JsonIgnore
	// private void addScheduledStage(ScheduledStage scheduledStage) throws
	// TimeScheduleException {
	// scheduledstages.add(scheduledStage);
	// SortedSet<ScheduledStage> tailSet =
	// scheduledstages.tailSet(scheduledStage);
	// tailSet.remove(scheduledStage);
	// ScheduledStage next = tailSet.first();
	// if (scheduledStage.isEndBeforeStart(next)) {
	//
	// } else {
	// scheduledstages.remove(scheduledStage);
	// throw new TimeScheduleException(null, null, null, null);
	// }
	// }

	@JsonIgnore
	public void completeSchedule(double percentage, Date actualEndDate) {
		ScheduledStage cuttrentScheduledStage = null;
		for (ScheduledStage scheduledStage : getScheduledStages()) {
			if (scheduledStage.getPercentageOfWork() <= percentage) {
				cuttrentScheduledStage = scheduledStage;
			}

		}
		if (cuttrentScheduledStage == null) {
			// TODO Exception
		}

		if (cuttrentScheduledStage.getActualTime() == null) {

		}
		cuttrentScheduledStage.setActualTime(actualEndDate);

	}

	@JsonIgnore
	public boolean isDelayToEnd() {
		ScheduledStage last = getScheduledStages().last();
		return last.hasDelay();
	}

	@JsonIgnore
	public boolean isDelayToStart() {
		ScheduledStage first = getScheduledStages().first();
		return first.hasDelay();

	}

	@JsonIgnore
	public RunningStatus getRunningStatus() {
		ScheduledStage last = getScheduledStages().last();
		if (last.isActualEnterd()) {
			return RunningStatus.completed;
		} else {
			ScheduledStage first = getScheduledStages().first();
			if (first.getPlannedTime() == null) {
				return RunningStatus.NotScheduled;
			}
			if (first.isActualEnterd()) {
				return RunningStatus.onprogress;
			} else {
				return RunningStatus.notStarted;
			}
		}

	}

	@JsonIgnore
	public boolean isStarted() {
		return scheduledStages.first().isActualEnterd();
	}

	@JsonIgnore
	public boolean isFinished() {
		return scheduledStages.last().isActualEnterd();
	}

	@JsonIgnore
	public Date getActualStartTime() {
		return scheduledStages.first().getActualTime();
	}

	@JsonIgnore
	public Date getActualFinishTime() {
		return scheduledStages.last().getActualTime();
	}

	public WorkSchedule copy() {

		WorkSchedule schedule = new WorkSchedule();
		SortedSet<ScheduledStage> scheduledStagesCopy = new TreeSet<ScheduledStage>();
		if (scheduledStages != null) {
			for (ScheduledStage scheduledStage : scheduledStages) {
				ScheduledStage scheduledStageCopy = scheduledStage.copy();
				scheduledStagesCopy.add(scheduledStageCopy);
			}
		}
		schedule.setScheduledStages(scheduledStagesCopy);
		return schedule;
	}

	// public void copyOfScheduledStages() {
	// Set<ScheduledStage> copy = new TreeSet<ScheduledStage>();
	// for (ScheduledStage scheduledStage : scheduledStages) {
	// copy.add(scheduledStage.copy());
	// }
	//
	// }

	public boolean delayInCurrentStage() {
		return getCurrentScheduledStage().hasDelay();
		// return getLastActualEntered().hasDelay();
	}

	private ScheduledStage getLastActualEntered() {
		ScheduledStage pre = scheduledStages.first();
		for (ScheduledStage scheduledStage : scheduledStages) {
			if (scheduledStage.getActualTime() != null) {
				pre = scheduledStage;
			}
		}

		return pre;
	}

	@JsonIgnore
	public int getIncreasedDuration() {
		ScheduledStage first = scheduledStages.first();
		ScheduledStage current = getLastActualEntered();
		if (first.getPercentageOfWork() == current.getPercentageOfWork()) {
			return 0;
		}
		int planned = DateUtil.getNumberOfDays(first.getPlannedTime(), current.getPlannedTime());
		int actual = DateUtil.getNumberOfDays(first.getActualTime(), current.getActualTime());

		return actual - planned;
	}
}
