package com.gvg.syena.core.api.entity.time.calendar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import com.gvg.syena.core.api.entity.time.DateUtil;

public class PersonDaysWorkCalendar implements WorkCalendar {

	private int workingHoursADay = 8;

	private TimePoint dayStartTime = new TimePoint(9, 0);

	private TimePoint dayEndTime = new TimePoint(17, 0);

	private Calendar c = new GregorianCalendar();

	public void moveForwardBusinessHours(Date date, int hrs, int min, int sec) {

		int days = hrs / workingHoursADay;
		hrs = (hrs - days * workingHoursADay) + min / 60;
		min = min - (min / 60);
		getDateAfter(date, days, hrs, min, sec);

	}

	public void moveBackwardBusinessHours(Date date, int hrs, int min, int sec) {
		int days = hrs / workingHoursADay;
		hrs = (hrs - days * workingHoursADay) + min / 60;
		min = min - (min / 60);
		getDateBefore(date, days, hrs, min, sec);

	}

	public Date getDateAfter(Date date, int days, int hrs, int min, int sec) {
		c.setTime(date);
		c.add(Calendar.DAY_OF_YEAR, days);
		c.add(Calendar.HOUR_OF_DAY, hrs);
		c.add(Calendar.MINUTE, min);
		c.add(Calendar.SECOND, sec);
		return c.getTime();
	}

	public Date getDateBefore(Date date, int days, int hrs, int min, int sec) {
		c.setTime(date);
		c.add(Calendar.DAY_OF_YEAR, -days);
		c.add(Calendar.HOUR_OF_DAY, -hrs);
		c.add(Calendar.MINUTE, -min);
		c.add(Calendar.SECOND, -sec);
		return c.getTime();
	}

	@Override
	public Date getDateAfter(Date date, int hrs, int min, int sec) {
		return getDateAfter(date, 0, hrs, min, sec);
	}

	@Override
	public Date getDateBefore(Date date, int hrs, int min, int sec) {
		return getDateBefore(date, 0, hrs, min, sec);
	}

	@Override
	public Date getDateAfter(Date date, int days) {
		return getDateAfter(date, 0, days * 24, 0, 0);
	}

	@Override
	public Date getDateBefore(Date date, int days) {
		return getDateBefore(date, 0, days * 24, 0, 0);

	}

	@Override
	public PersonHours getPersonHrs(Date startTime, Date finishTime) {
		long durattionMili = finishTime.getTime() - startTime.getTime();
		long hrs = TimeUnit.MILLISECONDS.toHours(durattionMili);
		long hrsMili = TimeUnit.HOURS.toMillis(hrs);
		long min = TimeUnit.MILLISECONDS.toMinutes(durattionMili - hrsMili);
		long minMili = TimeUnit.MINUTES.toMillis(min);
		long sec = TimeUnit.MILLISECONDS.toSeconds(durattionMili - (hrsMili + minMili));
		return new PersonHours((int) hrs, (int) min, (int) sec);
	}

	@Override
	public int getPersonDays(Date startTime, Date finishTime) {
		long diffInMillies = startTime.getTime() - finishTime.getTime();
		return (int) TimeUnit.MILLISECONDS.toDays(diffInMillies);
	}

	@Override
	public boolean beforeNow(Date date) {
		c.setTime(date);
		c.add(Calendar.HOUR_OF_DAY, -(date.getHours() - dayStartTime.getHr()));
		c.add(Calendar.MINUTE, -(date.getMinutes() - dayStartTime.getMin()));
		return date.before(c.getTime());
	}

	@Override
	public boolean beforeOrOn(Date date, Date baseDate) {
		return !DateUtil.after(date, baseDate);
//		c.setTime(date);
//		return !c.after(baseDate);
		// (date.getYear() <= baseDate.getYear()) && (date.getMonth() <=
		// baseDate.getMonth()) && (date.getDate() <= baseDate.getDate());
	}

	@Override
	public boolean OnOrAfter(Date date, Date baseDate) {
		return DateUtil.equals(date, baseDate) || DateUtil.after(date, baseDate);
		// c.setTime(date);
		// d1 =
		// return ! c.before(baseDate);
		// return (date.getYear() >= baseDate.getYear()) && (date.getMonth() >=
		// baseDate.getMonth()) && (date.getDate() >= baseDate.getDate());

	}

	@Override
	public TimePoint getDayStartTime() {
		return dayStartTime;
	}

	@Override
	public TimePoint getDayEndTime() {
		return dayEndTime;
	}
}
