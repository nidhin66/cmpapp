package com.gvg.syena.core.services.criticalpath;

import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;

public class JobData {

	private long jobId;
	private WorkTime workTime;
	private WorkStatus workStatus;

	public JobData() {
	}

	public JobData(long jobId, WorkTime workTime, WorkStatus workStatus) {
		this.jobId = jobId;
		this.workTime = workTime;
		this.workStatus = workStatus;
	}

	public long getJobId() {
		return jobId;
	}

	public WorkTime getWorkTime() {
		return workTime;
	}

	public WorkStatus getWorkStatus() {
		return workStatus;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public void setWorkTime(WorkTime workTime) {
		this.workTime = workTime;
	}

	public void setWorkStatus(WorkStatus workStatus) {
		this.workStatus = workStatus;
	}

}
