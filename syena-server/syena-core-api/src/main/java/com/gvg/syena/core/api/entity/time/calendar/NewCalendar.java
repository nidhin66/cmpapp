package com.gvg.syena.core.api.entity.time.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.gvg.syena.core.api.exception.CalanderException;

/**
 * @author geevarughesejohn
 *
 */
public class NewCalendar {

	private Map<String, OffCalander> offtimeLayers;

	private DailyWorkTime dailyWorkTime;

	public NewCalendar(DailyWorkTime dailyWorkTime, RepatedHoliday repatedHoliday) {
		this.dailyWorkTime = dailyWorkTime;
		offtimeLayers = new TreeMap<String, OffCalander>();
		offtimeLayers.put("repatedHoliday", repatedHoliday);
	}

	public void addOffTime(String key, OffCalander value) {
		offtimeLayers.put(key, value);
	}

	public List<DateInterval> calculateOffTime(DateInterval dateInterval) {

		List<DateInterval> offIntervals = new ArrayList<DateInterval>();

		for (String offdayLayerKey : offtimeLayers.keySet()) {
			OffCalander offdayLayer = offtimeLayers.get(offdayLayerKey);
			offdayLayer.outerWithMax(dateInterval, offIntervals);
		}
		return offIntervals;
	}

	public static void main(String[] args) throws CalanderException {
		TimePoint startTime = new TimePoint(9, 0);
		TimePoint endTime = new TimePoint(18, 0);
		int workingHrs = 8;
		int addtionalMintes = 30;
		DailyWorkTime dailyWorkTime = new DailyWorkAnyTime(startTime, endTime, workingHrs, addtionalMintes);
		RepatedHoliday repatedHoliday = new RepatedHoliday();
		repatedHoliday.repeatedOffTime(Week.ALL, Day.SUNDAY);
		repatedHoliday.repeatedOffTime(Week.SECOND, Day.SATURDAY, Time.AFTERNOON);
		NewCalendar newcalendar = new NewCalendar(dailyWorkTime, repatedHoliday);
		Calendar calendar = Calendar.getInstance();
		calendar.set(2015, Calendar.JULY, 10);
		Date startdate = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, 5);
		Date enddate = calendar.getTime();
		DateInterval dateInterval = new DateInterval(startdate, enddate);
		List<DateInterval> offTime = newcalendar.calculateOffTime(dateInterval);
		System.out.println(offTime);

	}

}
