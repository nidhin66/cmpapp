package com.gvg.syena.core.api.services.serach;

import java.util.Date;
import java.util.SortedSet;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;

public interface AdvancedJobSearch {

	PageItr<Job> getOnGoingJobs(Date date, String jobName, int pageNumber, int pageSize);

	PageItr<Job> getCompletedJobs(Date date, String jobName, int pageNumber, int pageSize);

	PageItr<Job> getPlannedJobs(Date date, String jobName, int pageNumber, int pageSize);

	PageItr<Job> getDelayedJobs(Date date, String jobName, int pageNumber, int pageSize);

	PageItr<Job> getJobsInitiatedBy(String personId, String jobName, int pageNumber, int pageSize);

	PageItr<Job> getJobsOwnedBy(String personId, String jobName, int pageNumber, int pageSize);

	PageItr<Job> searchJobs(JobProperties advancedSearchOptions, int pageNaumber, int pageSize);

	PageItr<Job> jobsToBeScheduled(String jobName, int pageNumber, int pageSize);

	PageItr<Job> jobsWithRevisedDates(String jobName, int pageNumber, int pageSize);

	PageItr<Job> jobsInCriticalPath(long hierarchyId, String jobName, int pageNumber, int pageSize);

	PageItr<HierarchyNode> nodeWithDelayed(float delayedPercentage, Date when, ValueComparitor valueComparitor, String nodeLevel, int pageNumber, int pageSize)
			throws HierarchyLevelNotFoundException;

	SortedSet<Long> getJobIdsInCriticalPath(long hierarchyId);

	PageItr<Job> jobsStartInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize);

	PageItr<Job> jobsCompleteInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize);

	PageItr<Job> jobJobsInDelyedStartJobGroups(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobName, int pageNumber, int pageSize);

	PageItr<Job> JobsInIncreasedDurationJobGroups(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobName, int pageNumber, int pageSize);

}
