package com.gvg.syena.core.api.entity.hierarchy;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.gvg.syena.core.api.entity.common.OnlyForJPA;

@Entity
public class HierarchyLevel {

	@Id
	private String levelName;

	private int level = 0;

	@OneToMany(cascade = { CascadeType.PERSIST })
	private List<HierarchyLevel> children;

	@OnlyForJPA
	public HierarchyLevel() {
	}

	public HierarchyLevel(String levelName) {
		this.levelName = levelName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public List<HierarchyLevel> getChildren() {
		return children;
	}

	public void setChildren(List<HierarchyLevel> children) {
		this.children = children;
	}

	public int getLevel() {
		return level;
	}

	void setLevel(int level) {
		this.level = level;
	}

	public void addChild(HierarchyLevel child) {
		if (children == null) {
			this.children = new ArrayList<HierarchyLevel>(3);
		}
		child.setLevel(level + 1);
		// child.setParent(this);
		this.children.add(child);
	}

	@Deprecated
	// TODO
	public boolean isEqual(HierarchyLevel given) {
		if (!(this.levelName.equals(given.getLevelName()))) {
			return false;
		} else {
			for (HierarchyLevel child : children) {
				List<HierarchyLevel> givenChildren = given.getChildren();
				for (HierarchyLevel givenChild : givenChildren) {
					boolean is = child.equals(givenChild);
					if (is) {
						break;
					}
				}
			}

		}
		return false;
	}
}
