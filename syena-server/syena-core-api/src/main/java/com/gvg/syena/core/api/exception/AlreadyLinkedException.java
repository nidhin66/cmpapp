package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.util.MessageKeys;

public class AlreadyLinkedException extends UserInfoException {

	private static final long serialVersionUID = 1L;
	private HierarchyNode parentNode;
	private HierarchyNode childNode;

	public AlreadyLinkedException(HierarchyNode parentNode, String messageString, HierarchyNode childNode) {
		super(MessageKeys.ALREADY_CONNECTED, messageString);
		this.parentNode = parentNode;
		this.childNode = childNode;
	}

}
