package com.gvg.syena.core.api.services.job;

import java.util.List;
import java.util.Set;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;

public interface JobManagement {

	void createStandardJob(StandardJob job) throws InvalidDataException;

	Job createJob(Job job) throws InvalidDataException;

	void createStandardJobGroup(JobGroup<StandardJob> jobGroup) throws InvalidDataException;

	JobGroup<Job> createJobGroup(JobGroup<Job> jobGroup) throws InvalidDataException;

	List<JobGroup<Job>> createJobGroupFromStandardJobGroup(long... standardJobGroupId) throws JobNotFoundException;

	Job createJobFromStandardJob(long standardJobId) throws JobNotFoundException;

	List<CheckList> createAndLinkFromStandardCheckListToJob(long jobId, long... standardCheckListItems) throws JobNotFoundException;

	List<CheckList> createAndLinkFromStandardCheckListToJob(long jobId, CheckListType checkListType, long[] standardCheckListItems) throws JobNotFoundException;

	List<CheckList> createAndLinkFromStandardCheckListToJobGroup(long jobGroupId, CheckListType checkListType, long[] standardCheckListItems) throws JobNotFoundException;

	List<CheckList> createAndLinkFromStandardCheckListToJobGroup(long jobGroupId, long[] standardCheckListItems) throws JobNotFoundException;

	StandardJob getStandardJob(long jobId);

	void updateStandardJob(StandardJob job) throws JobNotFoundException, InvalidDataException;

	void updateStandardJobGroup(JobGroup<StandardJob> jobGroup) throws JobNotFoundException, InvalidDataException;

	void addStandardJobToStandardJobGroup(long jobId, long jobGroupId) throws JobNotFoundException;

	Job getJob(long jobId);

	Job updateJob(Job job) throws JobNotFoundException, InvalidDataException;

	List<JobGroup<Job>> onlyCreateJobGroupFromStandardJobGroup(long... standardJobGroupId) throws JobNotFoundException;

	void updateJobGroup(JobGroup<Job> jobGroup) throws JobNotFoundException, InvalidDataException;

	void addJobToJobGroup(long jobId, long jobGroupId) throws JobNotFoundException;

	PageItr<StandardJob> getAllStandardJobs(int pageNumber, int pageSize);

	PageItr<Job> getAllJobs(int pageNumber, int pageSize);

	PageItr<JobGroup> getAllStandardJobGroup(int pageNumber, int pageSize);

	PageItr<JobGroup> getAllJobGroups(int pageNumber, int pageSize);

	void abandonJob(long jobId) throws JobNotFoundException;

	void abandonJobGrop(long jogGroupId) throws JobNotFoundException;

	PageItr<CheckList> getCheckListsOfJob(long jobId, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<CheckList> getCheckListsOfJobGroup(long jobId, int pageNumber, int pageSize) throws JobNotFoundException;

	void unlinkCheckList(long jobId, long... checkListItemId) throws JobNotFoundException;

	PageItr<Job> searchJobWithProperties(JobProperties jobSearchOptions, int pageNumber, int pageSize);

	List<Job> onlyCreateJobFromStandardJob(long[] standardJobIds);

	PageItr<JobGroup> searchJobGroupWithProperties(JobGroupProperties jobGroupProperties, int pageNumber, int pageSize);

	PageItr<Job> getSuccessorJobs(long[] jobIds, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<Job> getPredecessorJobs(long[] jobIds, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<JobGroup> getSuccessorJobGroups(long[] jobGroupIds, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<JobGroup> getPredecessorJobGroups(long[] jobGroupIds, int pageNumber, int pageSize) throws JobNotFoundException;

	PageItr<HierarchyNode> getConnectedHierarchyNode(long[] jobIds, int pageNumber, int pageSize);

	PageItr<Job> getJobsByConnectedNodeIds(long[] parentIds, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> getJobGroupssByConnectedNodeIds(long[] parentIds, int pageNumber, int pageSize);

	PageItr<Job> getJobs(long[] jobIds, int pageNumber, int pageSize);

	Set<Job> getJobsInJobGroup(long jobGroupId) throws JobNotFoundException;

	Set<StandardJob> getStandardJobInJobGroup(long jobGroupId) throws JobNotFoundException;

	PageItr<HierarchyNode> getConnectedHierarchyNodeJobGroup(long[] jobGroupIds, int pageNumber, int pageSize);

}
