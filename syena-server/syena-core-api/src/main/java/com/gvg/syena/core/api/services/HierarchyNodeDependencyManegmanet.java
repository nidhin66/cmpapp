package com.gvg.syena.core.api.services;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.exception.ApplicationException;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeDependencySchedulingException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;

public interface HierarchyNodeDependencyManegmanet {

	void addPredecessorHierarchyNodeDependency(long successorNodeId, long[] predecessorNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException;

	void addSuccessorHierarchyNodeDependency(long predecessorNodeId, long[] successorNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, HierarchyNodeDependencySchedulingException, InvalidDataException;

	void removePredecessorHierarchyNodeDependency(long successorNodeId, long[] predecessorNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, SchedulingException, ApplicationException;

	void removeSuccessorHierarchyNodeDependency(long predecessorNodeId, long[] successorNodeIds) throws HierarchyNodeNotFoundException, DependencyManagerNotFoundException,
			DependencyFailureException, SchedulingException, ApplicationException;

	PageItr<HierarchyNode> getPredecessorHierarchyNodeDependency(long hierarchyNodeId, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException;

	PageItr<HierarchyNode> getSuccessorHierarchyNodeDependency(long hierarchyNodeId, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException;

	PageItr<HierarchyNode> searchPredecessorHierarchyNodeDependency(long hierarchyNodeId, String name, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException;

	PageItr<HierarchyNode> searchSuccessorHierarchyNodeDependency(long hierarchyNodeId, String name, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException;

}
