package com.gvg.syena.core.api.util;

import java.util.Date;

import org.joda.time.DateTime;

public class ApplicationDateUtil {

	public static final String APPLICATION_DATE_HOUR_DEFERENCE = "APPLICATION_DATE_HOUR_DEFFERENCE";
	
	public static Date getApplicationDate(int deference){
		DateTime dateTime = new DateTime();
		if(deference > 0){
			dateTime = dateTime.plusHours(deference);
		}else if(deference < 0){
			deference = deference * -1;
			dateTime = dateTime.minusHours(deference);
		}
		return dateTime.toDate();
	}
	
}
