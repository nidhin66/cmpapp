package com.gvg.syena.core.api.services;

import java.util.List;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.exception.LoginFailedException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;

public interface UserManagemnet {

	void createPerson(Person person);

	PageItr<Person> searchPerson(String personName, int pageNumber, int pageSize);

	public void loginUser(String userId, String inputPassword) throws PersonNotFoundException, LoginFailedException ;

	Person getPerson(String userId) throws PersonNotFoundException;
	
	public List<Person> getPersonRoleIds(List<String> ids);

}
