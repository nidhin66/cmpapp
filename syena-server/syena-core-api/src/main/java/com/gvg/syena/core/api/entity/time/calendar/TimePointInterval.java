package com.gvg.syena.core.api.entity.time.calendar;

public class TimePointInterval {

	private TimePoint statTime;

	private TimePoint endTime;

	public TimePointInterval() {
	}

	public TimePointInterval(TimePoint statTime, TimePoint time2) {
		// TODO Auto-generated constructor stub
	}

	public TimePoint getStatTime() {
		return statTime;
	}

	public void setStatTime(TimePoint statTime) {
		this.statTime = statTime;
	}

	public TimePoint getEndTime() {
		return endTime;
	}

	public void setEndTime(TimePoint endTime) {
		this.endTime = endTime;
	}

}
