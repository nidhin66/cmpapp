package com.gvg.syena.core.api.services;

import java.util.List;

import com.gvg.syena.core.api.entity.notification.NotificationConfig;
import com.gvg.syena.core.api.services.notification.NotificationRecord;

public interface NotificationService {
	public void notify(NotificationConfig config, List<NotificationRecord> records);
}
