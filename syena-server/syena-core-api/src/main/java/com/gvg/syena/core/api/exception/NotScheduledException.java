package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.util.MessageKeys;

public class NotScheduledException extends ApplicationException{

//	public NotScheduledException(String messageKey, String messageString) {
//		super(messageKey, messageString);
//	}

	public NotScheduledException(HierarchyNode hierarchyNode, String messageString) {
		super(MessageKeys.NOT_SCHEDULED, messageString);
	}

}
