package com.gvg.syena.core.api.entity.application;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author geevarughesejohn
 *
 */
@Entity
public class ApplicationConfiguration {

	@Id
	private String paramKey;

	private String paramValue;

	public ApplicationConfiguration() {
	}

	public String getParamKey() {
		return paramKey;
	}

	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setparamValue(String paramValue) {
		this.paramValue = paramValue;
	}

}
