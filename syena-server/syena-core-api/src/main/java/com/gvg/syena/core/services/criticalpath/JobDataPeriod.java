package com.gvg.syena.core.services.criticalpath;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.util.ApplicationUtil;

public class JobDataPeriod implements Comparable<JobDataPeriod> {

	private final Date startTime;

	private final Date endTime;

	private Map<WorkStatusType, List<JobData>> jobdatas;

	public JobDataPeriod(Date startTime, Date endTime) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.jobdatas = new HashMap<WorkStatusType, List<JobData>>();
	}

	public void put(JobData criticalPathJobData) {
		WorkTime workTime = criticalPathJobData.getWorkTime();
		Date jobStartTime = workTime.getPlannedStartTime();
		Date jobFinishTime = workTime.getPlannedFinishTime();
		if (DateUtil.isInRange(startTime, endTime, jobStartTime, jobFinishTime, true)) {
			List<JobData> list = jobdatas.get(WorkStatusType.getWorkStatusType(criticalPathJobData.getWorkStatus()));
			if (list == null) {
				list = new ArrayList<JobData>();
				jobdatas.put(WorkStatusType.getWorkStatusType(criticalPathJobData.getWorkStatus()), list);
			}
			list.add(criticalPathJobData);
		}

	}

	public Map<WorkStatusType, List<JobData>> getCriticalPathJobDatas() {
		return jobdatas;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	@Override
	public int compareTo(JobDataPeriod o) {
		return this.startTime.compareTo(o.startTime);
	}
}