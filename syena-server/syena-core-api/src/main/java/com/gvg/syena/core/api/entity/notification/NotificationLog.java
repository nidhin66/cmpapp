package com.gvg.syena.core.api.entity.notification;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class NotificationLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String configId;
	
	private String recordId;
	
	private Date firstNotifyDate;
	
	private Date lastLastNotifyDate;
	
	private String name;
	
	private String subject;
	
	@Lob
	private String message;
	
	@OneToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@OrderBy("level ASC")
	private List<EscalationLog> escalationLog;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public Date getFirstNotifyDate() {
		return firstNotifyDate;
	}

	public void setFirstNotifyDate(Date firstNotifyDate) {
		this.firstNotifyDate = firstNotifyDate;
	}

	public Date getLastLastNotifyDate() {
		return lastLastNotifyDate;
	}

	public void setLastLastNotifyDate(Date lastLastNotifyDate) {
		this.lastLastNotifyDate = lastLastNotifyDate;
	}

	public List<EscalationLog> getEscalationLog() {
		return escalationLog;
	}

	public void setEscalationLog(List<EscalationLog> escalationLog) {
		this.escalationLog = escalationLog;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	

}
