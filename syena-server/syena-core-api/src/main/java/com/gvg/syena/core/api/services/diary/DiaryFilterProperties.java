package com.gvg.syena.core.api.services.diary;

import java.util.Date;

import com.gvg.syena.core.api.NodeCategory;

public class DiaryFilterProperties {
	private long hierarchyItemId;
	private NodeCategory category;
	private String user;
	private Date startDate;
	private Date endDate;
	public long getHierarchyItemId() {
		return hierarchyItemId;
	}
	public void setHierarchyItemId(long hierarchyItemId) {
		this.hierarchyItemId = hierarchyItemId;
	}
	public NodeCategory getCategory() {
		return category;
	}
	public void setCategory(NodeCategory category) {
		this.category = category;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
}
