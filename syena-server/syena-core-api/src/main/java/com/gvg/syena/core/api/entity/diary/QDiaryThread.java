package com.gvg.syena.core.api.entity.diary;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;


/**
 * QDiaryThread is a Querydsl query type for DiaryThread
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDiaryThread extends EntityPathBase<DiaryThread> {

    private static final long serialVersionUID = -1914074085L;

    public static final QDiaryThread diaryThread = new QDiaryThread("diaryThread");

    public final EnumPath<com.gvg.syena.core.api.NodeCategory> category = createEnum("category", com.gvg.syena.core.api.NodeCategory.class);

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.util.Date> createdDate = createDateTime("createdDate", java.util.Date.class);

    public final NumberPath<Long> hierarchyItemId = createNumber("hierarchyItemId", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> lastUpdatedDate = createDateTime("lastUpdatedDate", java.util.Date.class);

    public final StringPath topic = createString("topic");

    public QDiaryThread(String variable) {
        super(DiaryThread.class, forVariable(variable));
    }

    public QDiaryThread(Path<? extends DiaryThread> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDiaryThread(PathMetadata<?> metadata) {
        super(DiaryThread.class, metadata);
    }

}

