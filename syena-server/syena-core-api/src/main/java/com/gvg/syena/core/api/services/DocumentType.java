package com.gvg.syena.core.api.services;

public enum DocumentType {
	Images, Drawings, Documents;
}
