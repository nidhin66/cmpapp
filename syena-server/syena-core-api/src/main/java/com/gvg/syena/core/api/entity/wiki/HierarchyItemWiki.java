package com.gvg.syena.core.api.entity.wiki;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class HierarchyItemWiki {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private long hierarchyNodeId;

	@OneToOne(cascade = CascadeType.ALL)
	private WikiContent mainContent;

	@OneToMany(cascade = CascadeType.ALL)	
	private List<WikiContent> subContentList;

	public HierarchyItemWiki() {

	}

	public HierarchyItemWiki(long hierarchyItemId) {
		this.hierarchyNodeId = hierarchyItemId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getHierarchyNodeId() {
		return hierarchyNodeId;
	}

	public void setHierarchyNodeId(long hierarchyNodeId) {
		this.hierarchyNodeId = hierarchyNodeId;
	}

	public WikiContent getMainContent() {
		return mainContent;
	}

	public void setMainContent(WikiContent mainContent) {
		this.mainContent = mainContent;
	}

	public List<WikiContent> getSubContentList() {
		return subContentList;
	}

	public void setSubContentList(List<WikiContent> subContentList) {
		this.subContentList = subContentList;
	}
	
	public void removeSubContent(WikiContent subContent) {
		this.subContentList.remove(subContent);
	}

	public void addOrUpdateSubContent(WikiContent subContent) {
		if (null == this.subContentList) {
			this.subContentList = new ArrayList<>();
		}
		int index = this.subContentList.indexOf(subContent);
		if (index >= 0) {
			this.subContentList.set(index, subContent);
		} else {
			this.subContentList.add(subContent);
		}
	}

}
