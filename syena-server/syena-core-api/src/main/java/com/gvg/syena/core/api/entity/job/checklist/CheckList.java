package com.gvg.syena.core.api.entity.job.checklist;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.entity.common.Item;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.ValidationException;

@Entity
public class CheckList implements Item<CheckList>, Comparable<CheckList> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<CheckListItem> checkListItems;

	// @Column(nullable = false, unique = true)
	private String name;

	private boolean isStandard;

	private CheckListType checkListType;

	@ManyToOne
	@JsonIgnore
	private JobItem parent;

	public CheckList() {
	}

	public CheckList(String name, boolean isStandard) {
		this.name = name;
		this.isStandard = isStandard;
		this.checkListItems = new TreeSet<CheckListItem>();
	}

	public CheckList(CheckList checkList) {
		this.name = checkList.getName();
		this.checkListItems = new TreeSet<CheckListItem>();
		for (CheckListItem checkListItem : checkList.getCheckListItems()) {
			this.checkListItems.add(new CheckListItem(checkListItem));
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Set<CheckListItem> getCheckListItems() {
		return checkListItems;
	}

	public void setCheckListItems(Set<CheckListItem> checkListItems) {
		this.checkListItems = checkListItems;
	}

	public boolean isStandard() {
		return isStandard;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStandard(boolean isStandard) {
		this.isStandard = isStandard;
	}

	public void setAllStandard(boolean b) {
		this.isStandard = b;
		if (checkListItems != null) {
			for (CheckListItem checkListItem : checkListItems) {
				checkListItem.setStandard(b);
			}
		}
	}

	public int compareTo(CheckList o) {
		if (this.getName().equals(o.getName())) {
			return 0;
		}
		return 1;
	}

	public void addCheckListItems(List<CheckListItem> checkListItems) {
		if (this.checkListItems == null) {
			this.checkListItems = new TreeSet<CheckListItem>();
		}
		this.checkListItems.addAll(checkListItems);
	}

	public void addCheckListItem(CheckListItem checkListItem) {
		if (this.checkListItems == null) {
			this.checkListItems = new TreeSet<CheckListItem>();
		}
		this.checkListItems.add(checkListItem);
	}

	@Override
	@JsonIgnore
	public String getDescription() {
		return name;
	}

	@Override
	@JsonIgnore
	public boolean isEquual(CheckList dependent) {
		return this.getId() == dependent.getId();
	}

	public void setParent(JobItem parent) {
		this.parent = parent;
	}

	public JobItem getParent() {
		return parent;
	}

	// @Override
	// @JsonIgnore
	// public NodeCategory getNodeCategory() {
	// return NodeCategory.checklist;
	// }

	public CheckListType getCheckListType() {
		return checkListType;
	}

	public void setCheckListType(CheckListType checkListType) {
		this.checkListType = checkListType;
	}

	public void validate() throws ValidationException {
		if (this.name == null || this.name.trim() == "") {
			throw new ValidationException(MessageKeys.CHECKLIST_NAME_NULL, "Check List name null");
		}
		int size = this.checkListItems.size();
		if (size < 1) {
			throw new ValidationException(MessageKeys.CHECKLIST_DOESNOT_CONTAIN_ITEMS, "checkList doesnot contain any Items");
		}

		for (CheckListItem checkListItem : checkListItems) {
			checkListItem.validate();
		}
	}

	public void checkListItemCompletion(CheckListType checkListType, Date date) throws InvalidDataException {
		for (CheckListItem checkListItem : getCheckListItems()) {
			if (checkListType.equals(checkListType) && !checkListItem.isCompleted()) {
				throw new InvalidDataException(MessageKeys.CHECKLISTITEM_NOT_COMPLETED, "CheckListItem " + checkListItem.getName() + " not completed");
			}
			if (DateUtil.after(checkListItem.getCompletedDate(), date)) {
				throw new InvalidDataException(MessageKeys.CHECKLISTITEM_DATE_AFTER_JOB_START, "CheckListItem " + checkListItem.getName() + " date after start date");
			}
		}

	}
}
