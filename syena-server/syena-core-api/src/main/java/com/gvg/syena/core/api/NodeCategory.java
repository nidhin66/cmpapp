package com.gvg.syena.core.api;

/**
 * @author geevarughesejohn
 *
 */
public enum NodeCategory {

	hierarchy, jobgroup, job, checklist, checklistItem, StandardHierarchyNode;

}
