package com.gvg.syena.core.api.entity.chart;

import com.gvg.syena.core.api.entity.job.schedule.WorkPercentageType;


/**
 * @author geevarughesejohn
 *
 */
public enum ChartType {

	Actual("Actual", WorkPercentageType.Actual), Planned("Planned", WorkPercentageType.Planned), Revised("Revised", WorkPercentageType.Revised), Projected("Projected",
			WorkPercentageType.Projected), workefficiency("Prediction-workefficiency", WorkPercentageType.workefficiency), planningefficiency("Prediction-workefficiency",
			WorkPercentageType.planningefficiency), scheduleefficiency("Prediction-workefficiency", WorkPercentageType.scheduleefficiency);

	private WorkPercentageType workPercentageType;
	private String name;

	private ChartType(String name, WorkPercentageType workPercentageType) {
		this.name = name();
		this.workPercentageType = workPercentageType;
	}

	public WorkPercentageType getWorkPercentageType() {
		return workPercentageType;
	}

	public String getName() {
		return name;
	}

}
