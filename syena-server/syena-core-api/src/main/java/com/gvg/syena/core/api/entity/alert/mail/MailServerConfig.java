package com.gvg.syena.core.api.entity.alert.mail;

import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MailServerConfig {
	
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private long id; 
	
	private String host;
	
	private int port;
	
	private String protocol;
	
	private String username;
	
	private String password;
	
	private String defaultEncoding;
	
	@ElementCollection
	private Map<String,String> javaMailProperties;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDefaultEncoding() {
		return defaultEncoding;
	}

	public void setDefaultEncoding(String defaultEncoding) {
		this.defaultEncoding = defaultEncoding;
	}

	public Map<String, String> getJavaMailProperties() {
		return javaMailProperties;
	}

	public void setJavaMailProperties(Map<String, String> javaMailProperties) {
		this.javaMailProperties = javaMailProperties;
	}
	
	

}
