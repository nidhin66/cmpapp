package com.gvg.syena.core.api.entity.common;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.NodeCategory;

public interface HierarchyItem<P> {

	@JsonIgnore
	Set<? extends HierarchyItem> getChildren();

	P getParent();

	String getName();

	String getDescription();

	long getId();

	NodeCategory getNodeCategory();

	@JsonIgnore
	WorkStatus getStatus();

}
