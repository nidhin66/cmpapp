package com.gvg.syena.core.api.services.names;

public interface NotificationManagementServiceNames {
	String getNotifications = "getNotifications";
	String getNotificationCount = "getNotificationCount";
}
