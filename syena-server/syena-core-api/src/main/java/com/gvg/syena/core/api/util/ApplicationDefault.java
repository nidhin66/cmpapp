package com.gvg.syena.core.api.util;

import com.gvg.syena.core.api.entity.job.status.ComputationFactor;

/**
 * @author geevarughesejohn
 *
 */
public class ApplicationDefault {

	public static ComputationFactor getStatusComptationFactor() {
		return ComputationFactor.Least;
	}

}
