package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.MessageKeys;

public class JobNotFoundException extends ApplicationException {

	private static final long serialVersionUID = 1L;
	public static final String GROUP = "GROUPOFJOB";
	public static final String JOB = "SINGLEJOB";
	private String type;

	public JobNotFoundException(long id, String type, String messageString) {
		super(MessageKeys.JOB_NOT_FOUND, messageString);
		this.type = type;
	}

}
