package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.DependencyBarrier;
import com.gvg.syena.core.api.entity.util.MessageKeys;

public class JobsBarrierException extends ApplicationException {

	private long jobId;
	private DependencyBarrier barriers;

	public JobsBarrierException(long id, DependencyBarrier barriers, String messageString) {
		super(MessageKeys.BARRIERS_TO_START, messageString);
		this.jobId = id;
		this.barriers = barriers;
	}

	public JobsBarrierException(long jobid, DependencyBarrier barriers, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.jobId = jobid;
		this.barriers = barriers;
	}

}
