package com.gvg.syena.core.api.entity.dependency;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;

@Entity
public class HierarchyNodeDependency {

	@EmbeddedId
	private DependencyConnection dependencyConnection;

	@OneToOne(cascade = CascadeType.ALL)
	private Job connectionJob;

	@OnlyForJPA
	public HierarchyNodeDependency() {

	}

	public HierarchyNodeDependency(HierarchyNode dependencyNode, HierarchyNode hierarchyNode) {
		this.dependencyConnection = new DependencyConnection(dependencyNode, hierarchyNode);
	}

	public DependencyConnection getDependencyConnection() {
		return dependencyConnection;
	}

	public void setDependencyConnection(DependencyConnection dependencyConnection) {
		this.dependencyConnection = dependencyConnection;
	}

	public HierarchyNode getPredecessorNode() {
		return dependencyConnection.getPredecessorNode();
	}

	public HierarchyNode getSuccessorNode() {
		return dependencyConnection.getSuccessorNode();
	}

	public Job getConnectionJob() {
		return connectionJob;
	}

	
	public void setConnectionJob(Job connectionJob) {
		this.connectionJob = connectionJob;
	}
}
