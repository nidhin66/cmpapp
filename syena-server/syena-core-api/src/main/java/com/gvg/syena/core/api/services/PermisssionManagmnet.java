package com.gvg.syena.core.api.services;

import java.util.List;

import com.gvg.syena.core.api.exception.PersonNotFoundException;

public interface PermisssionManagmnet {

	void setPermissionUser(String userId, ActivityArea activityArea, List<ActivityOperation>  activityOperation) throws PersonNotFoundException;

	void setPermissionRole(String roleId, ActivityArea activityArea,  List<ActivityOperation> activityOperations);

	boolean checkPermission(String userId, String departmentId, ActivityArea activityArea,  ActivityOperation activityOperations) throws PersonNotFoundException;

}
