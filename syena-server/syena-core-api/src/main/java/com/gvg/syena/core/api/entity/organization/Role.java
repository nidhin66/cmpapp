package com.gvg.syena.core.api.entity.organization;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.gvg.syena.core.api.entity.common.OnlyForJPA;

@Entity
public class Role {

	@Id
	private String roleId;

	private String description;

	@ManyToMany
	private List<Person> users;

	@OnlyForJPA
	public Role() {
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Person> getUsers() {
		return users;
	}

	public void setUsers(List<Person> users) {
		this.users = users;
	}

}
