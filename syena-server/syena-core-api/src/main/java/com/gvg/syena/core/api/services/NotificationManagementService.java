package com.gvg.syena.core.api.services;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.services.notification.Notification;
import com.gvg.syena.core.api.services.notification.NotificationType;

public interface NotificationManagementService {
	public PageItr<Notification> getNotifications(String userId,NotificationType notificationType,int pageNumber, int pageSize);
	public long getNotificationCount(String userId);
}
