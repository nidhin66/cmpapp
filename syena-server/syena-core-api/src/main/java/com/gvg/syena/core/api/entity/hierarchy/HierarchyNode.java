package com.gvg.syena.core.api.entity.hierarchy;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.UsageStatus;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.job.DependencyJobs;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.services.DocumentType;

@Entity
// @Cacheable(key = "id")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class HierarchyNode extends StandardHierarchyNode<HierarchyNode> implements DependencyAttachableHierarchy {

	private int projectId;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private JobGroup<Job> defaultJobGroup;

	private boolean isInpunchList;

	private UsageStatus useStatus = UsageStatus.ACTIVE;

	@Embedded
	private WorkStatus workStatus = new WorkStatus();;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private DependencyJobs predecessorDependency;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private DependencyJobs successorDependency;

	@JsonIgnore
	@ElementCollection(fetch = FetchType.LAZY)
	private Map<String, String> drawings;

	@JsonIgnore
	@ElementCollection(fetch = FetchType.LAZY)
	private Map<String, String> documents;

	@Column(unique = true)
	private String hierarchyNodeName;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "numberofDays", column = @Column(name = "totalEffert_numberofDays")) })
	private PersonDays totalEffert;

	private long standardHierarchyNodeId;

	// @JsonIgnore
	// @ManyToOne
	// private StandardHierarchyNode<StandardHierarchyNode>
	// standardHierarchyNode;

	public HierarchyNode() {

	}

	public HierarchyNode(String name, String description, HierarchyLevel hierarchyLevel, int projectId) {
		super("", description, hierarchyLevel);
		init(name, projectId);

	}

	public HierarchyNode(StandardHierarchyNode<StandardHierarchyNode> standardHierarchyNode, int projectId, boolean copyChild) {

		this(standardHierarchyNode.getName(), standardHierarchyNode.getDescription(), standardHierarchyNode.getHierarchyLevel(), projectId);
		this.standardHierarchyNodeId = standardHierarchyNode.getId();
		// super.setWorkTime(new
		// WorkTime(standardHierarchyNode.getWorkTime().getDuration().copy()));
		if (copyChild) {
			Set<HierarchyNode> grandChildern = new TreeSet<HierarchyNode>();
			for (StandardHierarchyNode<StandardHierarchyNode> child : standardHierarchyNode.getChildren()) {
				HierarchyNode newHierarchyNode = new HierarchyNode(child, projectId, copyChild);
				newHierarchyNode.setParent(this);
				grandChildern.add(newHierarchyNode);
			}
			setChildren(grandChildern);
		}
		super.images.putAll(standardHierarchyNode.getImages());
		super.weightage = standardHierarchyNode.getWeightage();
		super.owner = standardHierarchyNode.getOwner();
	}

	private void init(String name, int projectId) {
		super.setName(null);
		this.hierarchyNodeName = name;
		this.projectId = projectId;
		this.isInpunchList = false;
		this.useStatus = UsageStatus.ACTIVE;
		this.workStatus = new WorkStatus();
		this.defaultJobGroup = new JobGroup<Job>(this.getName());
		this.defaultJobGroup.setConnectedNode(this);
		this.defaultJobGroup.setNodeType(NodeType.internal);
		this.predecessorDependency = new DependencyJobs();
		this.successorDependency = new DependencyJobs();
	}

	public int getProjectId() {
		return this.projectId;
	}

	@Override
	public String getName() {
		return hierarchyNodeName;
	}

	@Override
	public void setName(String hierarchyNodeName) {
		this.hierarchyNodeName = hierarchyNodeName;
	}

	public void setHierarchyNodeName(String hierarchyNodeName) {
		this.hierarchyNodeName = hierarchyNodeName;
	}

	public String getHierarchyNodeName() {
		return hierarchyNodeName;
	}

	public Map<String, String> getDrawings() {
		return drawings;
	}

	public void setDrawings(Map<String, String> drawings) {
		this.drawings = drawings;
	}

	public Map<String, String> getDocuments() {
		return documents;
	}

	public void setDocuments(Map<String, String> documents) {
		this.documents = documents;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public boolean isInpunchList() {
		return isInpunchList;
	}

	public void setInpunchList(boolean isInpunchList) {
		this.isInpunchList = isInpunchList;
	}

	public UsageStatus getUseStatus() {
		return useStatus;
	}

	public void setUseStatus(UsageStatus useStatus) {
		this.useStatus = useStatus;
	}

	@JsonProperty("workStatus")
	public WorkStatus getWorkStatus() {
		return workStatus;
	}

	@JsonProperty("workStatus")
	public void setWorkStatus(WorkStatus workStatus) {
		this.workStatus = workStatus;
	}

	@Override
	public DependencyJobs getPredecessorDependency() {
		return this.predecessorDependency;
	}

	@Override
	public DependencyJobs getSuccessorDependency() {
		return this.successorDependency;
	}

	public void setPredecessorDependency(DependencyJobs predecessorDependency) {
		this.predecessorDependency = predecessorDependency;
	}

	public void setSuccessorDependency(DependencyJobs successorDependency) {
		this.successorDependency = successorDependency;
	}

	public JobGroup<Job> getDefaultJobGroup() {
		return defaultJobGroup;
	}

	public void setDefaultJobGroup(JobGroup<Job> defaultJobGroup) {
		this.defaultJobGroup = defaultJobGroup;
	}

	public void addJobs(List<Job> jobs) {
		// defaultJobGroup.addJobs(jobs);
		for (Job job : jobs) {
			addJob(job);
		}
	}

	public void addJob(Job job) {
		job.setDescription(this.getName() + "-" + job.getName());
		defaultJobGroup.addJob(job);
		job.setParentJobGroup(defaultJobGroup);
		job.setConnectedNode(this);
	}

	@JsonIgnore
	public Set<Job> getJobs() {
		return defaultJobGroup.getJobs();
	}

	@JsonIgnore
	public Set<JobGroup<Job>> getJobGroups() {
		return defaultJobGroup.getJobGroups();
	}

	public boolean hasJob() {
		return defaultJobGroup != null && (defaultJobGroup.getJobs().size() > 0 || defaultJobGroup.getJobGroups().size() > 0);

	}

	public void addJobGropus(List<JobGroup<Job>> newJobGroups) {
		for (JobGroup<Job> jobGroup : newJobGroups) {
			// jobGroup.setDescription(this.getName() + "-" +
			// jobGroup.getName());
			// jobGroup.setParent(defaultJobGroup);
			// jobGroup.setConnectedNode(this);
			addJobGroup(jobGroup);
		}
		// defaultJobGroup.addToJobGroup(newJobGroups);
	}

	public void addJobGroup(JobGroup<Job> jobGroup) {
		jobGroup.setDescription(this.getName() + "-" + jobGroup.getName());
		jobGroup.setParent(defaultJobGroup);
		defaultJobGroup.addJobGroup(jobGroup);
		jobGroup.setConnectedNode(this);
	}

	public void putDocumntPath(DocumentType documentType, String imageName, String filepath) {
		if (documentType == DocumentType.Documents) {
			documents.put(imageName, filepath);
		} else if (documentType == DocumentType.Drawings) {
			drawings.put(imageName, filepath);
		} else {
			super.putDocumntPath(documentType, imageName, filepath);
		}

	}

	public String getDocumntPath(DocumentType documentType, String imageName) {
		String path = null;
		if (documentType == DocumentType.Documents) {
			path = documents.get(imageName);
		} else if (documentType == DocumentType.Drawings) {
			path = drawings.get(imageName);
		} else {
			path = super.getDocumntPath(documentType, imageName);
		}
		return path;
	}

	@Override
	@JsonIgnore
	public void setParent(HierarchyNode parent) {
		super.setParent(parent);
	}

	@Override
	@JsonIgnore
	public WorkStatus getStatus() {
		return getWorkStatus();
	}

	public PersonDays getTotalEffert() {
		if (totalEffert == null) {
			totalEffert = new PersonDays(1);
		}
		return totalEffert;
	}

	public void setTotalEffert(PersonDays totalEffert) {
		this.totalEffert = totalEffert;
	}

	public long getStandardHierarchyNodeId() {
		return standardHierarchyNodeId;
	}

	public void setStandardHierarchyNodeId(long standardHierarchyNodeId) {
		this.standardHierarchyNodeId = standardHierarchyNodeId;
	}

//	@JsonIgnore
//	public boolean isEquual(HierarchyNode node) {
//		return this.getId() == node.getId();
//	}

	// @Override
	// public boolean isEqual(DependencyAttachable dependencyAttachable) {
	// if (dependencyAttachable instanceof HierarchyNode) {
	// (HierarchyNode)dependencyAttachable);
	// }
	//
	// }

	// @Override
	// @JsonIgnore
	// public HierarchyNode getParent() {
	// return super.getParent();
	// }
}
