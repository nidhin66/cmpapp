package com.gvg.syena.core.api.entity.time;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.util.Configuration;

/**
 * @author geevarugehsejohn
 *
 */

// @DiscriminatorValue(value = "P")
@Embeddable
public class PersonDays extends Duration {

	@Transient
	@JsonIgnore
	private Calendar calendar = Calendar.getInstance();

	private int numberofDays;

	public PersonDays() {
		super(Configuration.personDaysCPMCalendar);
	}

	public PersonDays(int numberofDays) {
		super(Configuration.personDaysCPMCalendar);
		this.numberofDays = numberofDays;
	}

	public int getNumberofDays() {
		return numberofDays;
	}

	@JsonIgnore
	@Override
	public Date calculateEndDate(Date date) {
		return getCalender().getDateAfter(date, numberofDays);
	}

	@JsonIgnore
	@Override
	public Date calculateStatDate(Date endTime) {
		return getCalender().getDateBefore(endTime, numberofDays);
	}

	@JsonIgnore
	@Override
	public void add(Duration duration) {
		if (duration instanceof PersonDays) {
			PersonDays personDays = (PersonDays) duration;
			this.numberofDays = this.numberofDays + personDays.getNumberofDays();
		}
	}

	@JsonIgnore
	@Override
	public String toString() {
		return numberofDays + " days";
	}

	@JsonIgnore
	@Override
	public boolean isLess(Date startTime, Date finishTime) {
		int numberofDayst = getCalender().getPersonDays(finishTime, startTime);
		return numberofDayst < numberofDays;
	}

	@JsonIgnore
	@Override
	public boolean beforeToday(Date date) {
		return getCalender().beforeNow(date);
	}

	@JsonIgnore
	@Override
	public Duration copy() {
		return new PersonDays(numberofDays);
	}

	@JsonIgnore
	@Override
	public Date setDayStartTime(Date date) {
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, getDayStartTime().getHr());
		calendar.set(Calendar.MINUTE, getDayStartTime().getMin());
		calendar.set(Calendar.SECOND, getDayStartTime().getSec());
		return calendar.getTime();
	}

	@JsonIgnore
	@Override
	public Date setDayEndTime(Date date) {
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, getDayEndTime().getHr());
		calendar.set(Calendar.MINUTE, getDayEndTime().getMin());
		calendar.set(Calendar.SECOND, getDayEndTime().getSec());
		return calendar.getTime();
	}

	@JsonIgnore
	@Override
	public double divide(Duration duration) {
		PersonDays personDays = (PersonDays) duration;
		int numday = personDays.getNumberofDays();
		if (numday == 0) {
			return 0;
		}
		int num = this.numberofDays / numday;
		return num;
	}

	@JsonIgnore
	@Override
	public Duration multiple(double num) {
		float num1 = this.numberofDays;
		this.numberofDays = (int) (num1 * num);
		return this;

	}

	@JsonIgnore
	@Override
	public Float getDurationValue() {
		return (float) this.numberofDays;
	}

	@Override
	public void divide(float percentageOfWork) {
		this.numberofDays = this.numberofDays / (int) percentageOfWork;

	}

	@Override
	public void setNumberOfDays(int numberOfDays) {
		this.numberofDays = numberOfDays;
	}

}
