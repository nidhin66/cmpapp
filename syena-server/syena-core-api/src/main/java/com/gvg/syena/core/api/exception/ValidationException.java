package com.gvg.syena.core.api.exception;

public class ValidationException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public ValidationException(String messageKey, String messageString) {
		super(messageKey, messageString);
	}

}
