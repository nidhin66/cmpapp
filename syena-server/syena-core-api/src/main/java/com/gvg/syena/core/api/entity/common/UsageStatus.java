package com.gvg.syena.core.api.entity.common;

/**
 * @author geevarughesejohn
 *
 */
public enum UsageStatus {

	ACTIVE, INACTIVE;

}
