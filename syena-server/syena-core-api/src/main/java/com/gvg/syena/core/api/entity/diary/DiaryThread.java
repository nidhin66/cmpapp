package com.gvg.syena.core.api.entity.diary;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.gvg.syena.core.api.NodeCategory;

@Entity
public class DiaryThread {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private long hierarchyItemId;
	
	private String hierarchyName;
	
	@Enumerated
	private NodeCategory category;
	
	private String topic;
	
	private String createdBy;
	
	private Date createdDate;
	
	private Date lastUpdatedDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getHierarchyItemId() {
		return hierarchyItemId;
	}

	public void setHierarchyItemId(long hierarchyItemId) {
		this.hierarchyItemId = hierarchyItemId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public NodeCategory getCategory() {
		return category;
	}

	public void setCategory(NodeCategory category) {
		this.category = category;
	}

	public String getHierarchyName() {
		return hierarchyName;
	}

	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}
	
	
	

}
