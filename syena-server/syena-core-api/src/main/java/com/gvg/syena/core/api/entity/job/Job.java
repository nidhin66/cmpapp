package com.gvg.syena.core.api.entity.job;

import java.util.Date;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.Cost;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.util.DependencyBarrier;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobsBarrierException;
import com.gvg.syena.core.api.util.ApplicationUtil;

/**
 * @author geevarugehsejohn
 *
 */
@Entity
// @Cacheable(key = "id")
// @Table(uniqueConstraints = @UniqueConstraint(columnNames = { "jobName",
// "parent.id", "connectedNode.id" }))
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Job extends StandardJob implements DependencyAttachable, DependencyAttachableHierarchy {

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "amount", column = @Column(name = "actual_cost_amount")),
			@AttributeOverride(name = "currency", column = @Column(name = "actual_cost_currency")) })
	private Cost actualCost;

	// @JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Person initiator;

	@Column(unique = false, nullable = false)
	private String jobName;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private DependencyJobs predecessorDependency;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private DependencyJobs successorDependency;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private HierarchyNode connectedNode;

	// @JsonIgnore
	// @ManyToOne(fetch = FetchType.LAZY)
	// private JobGroup<Job> parent;

	@Embedded
	private WorkStatus workStatus;

	@Embedded
	private WorkSchedule workSchedule;

	private String executor;

	boolean isInPunchList;

	@JsonIgnore
	@ManyToOne
	private StandardJob standardJob;

	@OnlyForJPA
	public Job() {
	}

	public Job(String name, String description) {
		super(name, description);
		init(name);
		this.isInPunchList = true;

	}

	public Job(StandardJob standardJob, String description) {
		super(standardJob, description);
		this.standardJob = standardJob;
		init(standardJob.getName());
		this.isInPunchList = false;
		Set<CheckList> checkLists = getCheckList();
		for (CheckList checkList : checkLists) {
			checkList.setAllStandard(false);
		}

	}

	private void init(String name) {
		super.setName(null);
		setNodeType(NodeType.instance);
		this.jobName = name;
		this.workStatus = new WorkStatus();
		this.predecessorDependency = new DependencyJobs();
		this.successorDependency = new DependencyJobs();
		this.workSchedule = ApplicationUtil.getSystemDefaultWorkSchedule(getWorkTime());

	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Cost getActualCost() {
		return actualCost;
	}

	public void setActualCost(Cost actualCost) {
		this.actualCost = actualCost;
	}

	public boolean isInPunchList() {
		return isInPunchList;
	}

	public void setInPunchList(boolean isInPunchList) {
		this.isInPunchList = isInPunchList;
	}

	public void setPredecessorDependency(DependencyJobs predecessorDependency) {
		this.predecessorDependency = predecessorDependency;
	}

	public void setSuccessorDependency(DependencyJobs successorDependency) {
		this.successorDependency = successorDependency;
	}

	public WorkStatus getWorkStatus() {
		return this.workStatus;
	}

	public void setWorkStatus(WorkStatus workStatus) {
		this.workStatus = workStatus;
	}

	public Person getInitiator() {
		return initiator;
	}

	public void setInitiator(Person initiator) {
		this.initiator = initiator;
	}

	public WorkSchedule getWorkSchedule() {
		return workSchedule;
	}

	public void setWorkSchedule(WorkSchedule workSchedule) {
		this.workSchedule = workSchedule;
	}

	public String getExecutor() {
		return executor;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	@Override
	public String getName() {
		return getJobName();
	}

	@Override
	public void setName(String name) {
		setJobName(name);
	}

	@Override
	@JsonIgnore
	public DependencyJobs getPredecessorDependency() {
		return predecessorDependency;
	}

	@Override
	@JsonIgnore
	public DependencyJobs getSuccessorDependency() {
		return successorDependency;
	}

	// @JsonIgnore
	// public JobGroup<Job> getParent() {
	// return parent;
	// }
	//
	// public void setParent(JobGroup<Job> parent) {
	// this.parent = parent;
	// }

	@JsonIgnore
	public HierarchyNode getConnectedNode() {
		return connectedNode;
	}

	@JsonIgnore
	public void setConnectedNode(HierarchyNode connectedNode) {
		this.connectedNode = connectedNode;
	}

	@Override
	@Deprecated
	public Set<? extends HierarchyItem> getChildren() {
		return null;
	}

	@JsonIgnore
	public Date getPalnnedStartTime() {
		return getWorkSchedule().getPlannedStartTime();
	}

	@JsonIgnore
	public Date getPalnnedFinishTime() {
		return getWorkSchedule().getPlannedFinishTime();
	}

	@JsonIgnore
	public void changePlannedStartDate(Date date) {
		super.getWorkTime().setPlanedStartTime(date);
		if (workSchedule != null) {
			workSchedule.changePlannedStartTime(date);
		}
		computeWorkStatus();
	}

	@JsonIgnore
	public void changePlannedFinishDate(Date date) {
		super.getWorkTime().setPlanedFinishTime(date);
		if (workSchedule != null) {
			workSchedule.changePlannedFinishTime(date);
		}
		computeWorkStatus();
	}

	@JsonIgnore
	public void addPredecessorDependencyJob(Job job, DependencyType dependencyType) {
		this.getPredecessorDependency().addJob(job, dependencyType);
		job.getSuccessorDependency().addJob(this, dependencyType);
	}

	@JsonIgnore
	public void addSuccessorDependency(Job job, DependencyType dependencyType) {
		this.getSuccessorDependency().addJob(job, dependencyType);
		job.getPredecessorDependency().addJob(this, dependencyType);
	}

	@JsonIgnore
	public Duration getActualTimeSpent(double percentage) {
		return workSchedule.getActualTimeSpent(getDuration(), percentage);
	}

	@JsonIgnore
	public Duration getEstimatedTimeSpent(double percentage) {
		return workSchedule.getActualTimeSpent(getDuration(), percentage);
		// return workSchedule.getEstimatedTimeSpent();
	}

	@JsonIgnore
	public double getPercentageOfComplete(Date date) {
		return workSchedule.getPercentageOfComplete(date);
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Id " + getId() + ":");
		buffer.append("Name " + getJobName() + ":");
		buffer.append("Description " + getDescription() + ":");
		buffer.append(getDuration());
		return buffer.toString();
	}

	@Override
	@JsonIgnore
	public NodeCategory getNodeCategory() {
		return NodeCategory.job;
	}

	@JsonIgnore
	public void validateWorkSchedule(WorkSchedule timeSchedule) throws InvalidDataException, JobsBarrierException {
		if (timeSchedule.isStarted()) {
			validateDependencyJobs_start(timeSchedule.getActualStartTime());
			validateCheckList(CheckListType.initiationCheckList, timeSchedule.getActualStartTime());
		}
		if (timeSchedule.isFinished()) {
			validateCheckList(CheckListType.completionCheckList, timeSchedule.getActualFinishTime());
			validateCheckList(CheckListType.infoCheckList, timeSchedule.getActualFinishTime());
			validateCheckList(CheckListType.completionCheckList, timeSchedule.getActualFinishTime());
		}
	}

	@JsonIgnore
	public WorkStatus computeWorkStatus() {
		this.workStatus.setRunningStatus(workSchedule.getRunningStatus());
		this.workStatus.setDelayToEnd(workSchedule.isDelayToEnd());
		this.workStatus.setDelayToStart(workSchedule.isDelayToStart());
		this.workStatus.setDelayInCurrentStage(workSchedule.delayInCurrentStage());
		return this.workStatus;
	}

	@Override
	public void validateOnCreate() throws InvalidDataException {
		if (jobName == null || jobName.trim() == "") {
			throw new InvalidDataException(MessageKeys.INVALID_JOB_NAME, "Job name should not be null or empty");
		}
		if (getDescription() == null || getDescription().trim() == "") {
			throw new InvalidDataException(MessageKeys.INVALID_DESCRIPTION, "Description should not be null or empty");
		}
	}

	//
	// public void validateOnStart(Date date) throws InvalidDataException,
	// JobsBarrierException {
	// validateCheckList(CheckListType.initiationCheckList, date);
	// validateDependencyJobs_start();
	//
	// }

	public void validateCheckList(CheckListType checkListType, Date date) throws InvalidDataException {
		for (CheckList checkList : getCheckList()) {
			if (checkList.getCheckListType() == checkListType) {
				checkList.checkListItemCompletion(checkListType, date);
			}
		}

	}

	public void validateDependencyJobs_start(Date date) throws JobsBarrierException {
		DependencyBarrier barriers = predecessorDependency.analyseBarriersToStart(date);
		if (!barriers.isEmpty()) {
			throw new JobsBarrierException(getId(), barriers, MessageKeys.BARRIERS_TO_START, "Barriers to start");
		}

	}

	// public void validateOnFinish(Date date) throws InvalidDataException {
	// validateCheckList(CheckListType.infoCheckList, date);
	// validateCheckList(CheckListType.completionCheckList, date);
	// }

	@Override
	@JsonIgnore
	public WorkStatus getStatus() {
		return getWorkStatus();
	}

	@JsonIgnore
	public boolean isDelayedToStart() {
		return getWorkSchedule().isDelayToStart();
	}

	@JsonIgnore
	public boolean isDurationIncreased() {
		int inc = getWorkSchedule().getIncreasedDuration();
		return inc > 0;
	}

	public void validateCheckList(CheckListType checkListType) throws InvalidDataException {
		Date date = null;
		if (CheckListType.initiationCheckList == checkListType) {
			date = getWorkSchedule().getActualStartTime();
		} else if (CheckListType.completionCheckList == checkListType) {
			date = getWorkSchedule().getActualFinishTime();
		} else if (CheckListType.infoCheckList == checkListType) {
			date = getWorkSchedule().getActualFinishTime();
		}
		validateCheckList(checkListType, date);

	}

	@JsonIgnore
	@Override
	public int compareTo(StandardJob job) {
		if (this.getId() == 0) {
			if (this.getName().equals(job.getName())) {
				return 0;
			} else {
				return 1;
			}
		} else if (this.getId() == job.getId()) {
			return 0;
		} else {
			return 1;
		}
	}

	@JsonIgnore
	@Override
	public DependencyAttachableHierarchy getParent() {
		return getParentJobGroup();
	}

	@JsonIgnore
	public boolean isPlanned() {
		return getWorkSchedule().getPlannedStartTime() != null;
	}

	// @Override
	// @JsonIgnore
	// public void setDuration(Duration duration) {
	// super.setDuration(duration);
	// }

	// @JsonIgnore
	// public void setStartTime(Date startTime) {
	// super.getWorkTime().setPlanedStartTime(startTime);
	// }
	//
	// @JsonIgnore
	// public void setFinishTime(Date finishTime) {
	// super.getWorkTime().setPlanedFinishTime(finishTime);
	// }

	//
	// @JsonIgnore
	// public Date getEndDate() {
	// if (getCurrentSchedule() != null) {
	// return getCurrentSchedule().getEndDate();
	// } else {
	// return null;
	// }
	// }

	// public void addPercentageOfCompleted(double percentage, Date date) {
	// currentSchedule.completeSchedule(percentage, date);
	// }

	// public void addDependentJob(List<Job> endToStartJobs) {
	// predecessorJobs.addEndToStart(endToStartJobs);
	// }
	//
	// public List<Job> getPathtInDependentJobs(long dependentJobId) {
	// return
	// predecessorJobs.checkPathInEndToStartDependencyTree(dependentJobId, new
	// ArrayList<Job>());
	// }

	// @JsonIgnore
	// public void setWorkTime(Date date, Time duration) {
	// WorkTime workTimet = new WorkTime(date, duration);
	// this.setWorkTime(workTimet);
	//
	// }

}
