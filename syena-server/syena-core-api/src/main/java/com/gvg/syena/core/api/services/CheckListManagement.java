package com.gvg.syena.core.api.services;

import java.util.Date;
import java.util.List;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;
import com.gvg.syena.core.api.exception.CheckListNotFound;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.ValidationException;

public interface CheckListManagement {

	void createCheckLists(List<CheckList> checkLists) throws ValidationException;

	void addCheckListItemsInCheckList(long checkListId, List<CheckListItem> checkListItems) throws CheckListNotFound;

	public CheckListItem updateCheckList(long checkListId, long checkListItemId, boolean status, Date completedDate, String completedPersonId) throws CheckListNotFound,
			PersonNotFoundException, ValidationException, InvalidDataException;

	PageItr<CheckList> getAllStandardCheckList(int pageNumber, int pageSize);

	PageItr<CheckList> getAllCheckList(int pageNumber, int pageSize);

	PageItr<CheckList> findInStandardCheckList(String name, int pageNumber, int pageSize);

	PageItr<CheckList> findInCheckList(String name, int pageNumber, int pageSize);

	PageItr<CheckList> getCheckListByParent(long parentId, int pageNumber, int pageSize);

	PageItr<CheckList> findInCheckListWithParent(long parentId, String name, int pageNumber, int pageSize);


}
