package com.gvg.syena.core.api.entity.organization;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DepartmentJsonSerializer extends KeyDeserializer {

	@Override
	public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		Object object = mapper.readValue(key, Department.class);
		return object;
	}

}