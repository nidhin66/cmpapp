package com.gvg.syena.core.api.entity.wiki;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class WikiContent {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String heading;

	@Lob
	private String textContent;
	
	@Column(columnDefinition="mediumblob")
	private byte[] image;
	
	private String updatedBy;
	
	private Date updatedOn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getTextContent() {
		return textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Override
	public boolean equals(Object other) {
		 if (!(other instanceof WikiContent)) {
		        return false;
		    }else{
		    	 WikiContent wikiContent = (WikiContent) other;
				 if(wikiContent.getId() > 0 ){
					if(this.getId() == wikiContent.getId()){
						return true;
					}else{
						return false;
					}
				 }else{
					 return false;
				 }
		    }
		
	}

}
