package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.DependencyMetrix;

public class DependencyFailureException extends ApplicationException {

	private static final long serialVersionUID = 1L;
	private DependencyMetrix dependencyMetrix;

	public DependencyFailureException() {
		super(null);
	}

	public DependencyFailureException(String messageKey, String messageString) {
		super(messageKey, messageString);
	}

	public DependencyFailureException(DependencyMetrix dependencyMetrix, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.dependencyMetrix = dependencyMetrix;
	}
}
