package com.gvg.syena.core.api.entity.notification;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.gvg.syena.core.api.services.notification.NotificationType;

@Entity
public class EscalationLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;	
	
	private int level;
	
	private long escalationId;
	
	private Date firstNotifyDate;
	
	private Date lastNotifyDate;
	
	private boolean alertRequired;
	
	private boolean alertSend;	
	
	@Enumerated(EnumType.STRING)
	private NotificationType notificationType;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	private NotificationLog notificationLog;
	
	public EscalationLog(){
		
	}
	
	public EscalationLog(Escalation escalation){
		this.alertRequired = true;
		this.alertSend = false;
		this.escalationId = escalation.getId();
		this.firstNotifyDate = new Date();
		this.lastNotifyDate = new Date();
		this.level = escalation.getLevel();		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getEscalationId() {
		return escalationId;
	}

	public void setEscalationId(long escalationId) {
		this.escalationId = escalationId;
	}

	public Date getFirstNotifyDate() {
		return firstNotifyDate;
	}

	public void setFirstNotifyDate(Date firstNotifyDate) {
		this.firstNotifyDate = firstNotifyDate;
	}

	public Date getLastNotifyDate() {
		return lastNotifyDate;
	}

	public void setLastNotifyDate(Date lastNotifyDate) {
		this.lastNotifyDate = lastNotifyDate;
	}

	public boolean isAlertRequired() {
		return alertRequired;
	}

	public void setAlertRequired(boolean alertRequired) {
		this.alertRequired = alertRequired;
	}

	public boolean isAlertSend() {
		return alertSend;
	}

	public void setAlertSend(boolean alertSend) {
		this.alertSend = alertSend;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public NotificationLog getNotificationLog() {
		return notificationLog;
	}

	public void setNotificationLog(NotificationLog notificationLog) {
		this.notificationLog = notificationLog;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	
	

}
