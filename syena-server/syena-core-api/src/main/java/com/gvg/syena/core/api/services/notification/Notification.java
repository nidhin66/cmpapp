package com.gvg.syena.core.api.services.notification;

import java.util.Date;

public class Notification {
	
	private long id;
	
	private String name;
	
	private String subject;
	
	private Date date;
	
	private boolean alertRequired;
	
	private String message;
	
	private long escalationLogId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isAlertRequired() {
		return alertRequired;
	}

	public void setAlertRequired(boolean alertRequired) {
		this.alertRequired = alertRequired;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getEscalationLogId() {
		return escalationLogId;
	}

	public void setEscalationLogId(long escalationLogId) {
		this.escalationLogId = escalationLogId;
	}
	
	

}
