package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.job.Job;

public class JobScheduleFailed extends ApplicationException {

	private Job job;

	public JobScheduleFailed(Job job, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.job = job;
	}

	public JobScheduleFailed(String messageKey, String messageString) {
		super(messageKey, messageString);
	}

}
