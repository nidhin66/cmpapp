package com.gvg.syena.core.api.services;

import java.util.List;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.project.Project;
import com.gvg.syena.core.api.exception.AlreadyLinkedException;
import com.gvg.syena.core.api.exception.DocIOException;
import com.gvg.syena.core.api.exception.HierarchyLevelNotFoundException;
import com.gvg.syena.core.api.exception.HierarchyNodeNotFoundException;
import com.gvg.syena.core.api.exception.ImageIOException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.PersonNotFoundException;
import com.gvg.syena.core.api.exception.RemovalBarrierException;
import com.gvg.syena.core.api.exception.ServiceLevelValidationException;
import com.gvg.syena.core.api.services.serach.HierarchyNodeProperties;

public interface HierarchyManagement {

	void createProject(Project project);

	void createHierarchyLevel(HierarchyLevel hierarchyLevel);

	HierarchyLevel getHierarchyLevels(String hierarchyName) throws HierarchyLevelNotFoundException, ServiceLevelValidationException;

	PageItr<StandardHierarchyNode<StandardHierarchyNode>> getAllStandardHierarchyNodes(String hierarchyLevel, int pageNumber, int pageSize) throws HierarchyLevelNotFoundException;

	PageItr<HierarchyNode> getAllHierarchyNodes(String hierarchyLevel, int pageNumber, int pageSize) throws HierarchyLevelNotFoundException;

	PageItr<HierarchyNode> getHierarchyNodes(long[] nodeIds, int pageNumber, int pageSize);

	PageItr<HierarchyNode> getChildHierarchyNodes(long[] parentIds, int pageNumber, int pageSize) throws ServiceLevelValidationException;

	PageItr<HierarchyNode> getLinkedHierarchyNodes(long parentId, int pageNumber, int pageSize);

	HierarchyNode createHierarchyNode(HierarchyNode hierarchyNode, String owner) throws PersonNotFoundException, ServiceLevelValidationException;

	void updateHierarchyNode(List<HierarchyNode> hierarchyNodes) throws HierarchyNodeNotFoundException;

	void obsoleteHierarchyNode(long hierarchyNodeId) throws HierarchyNodeNotFoundException;

	void linkHierarchyNodes(long parentNodeId, long... nodeId) throws HierarchyNodeNotFoundException, AlreadyLinkedException;

	void unlinkHierarchyNodes(long parentNodeId, long nodeId) throws HierarchyNodeNotFoundException;

	PageItr<HierarchyNode> getObsoleteHierarchyNodes(int pageNumber, int pageSize);

	long[] getHierarchyNodeIds(String hierarchyLevel) throws ServiceLevelValidationException, HierarchyLevelNotFoundException;

	PageItr<HierarchyNode> searchHierarchyNodesInLevel(String hierarchyNodename, String hierarchyLevel, int pageNumber, int pageSize) throws HierarchyLevelNotFoundException;

	PageItr<HierarchyNode> searchHierarchyNodesInChild(String hierarchyNodename, long parentId, int pageNumber, int pageSize);

	void linkJobs(long hierarchyNodeId, long... jobIds) throws HierarchyNodeNotFoundException;

	void removeLinkedJobs( long... jobIds) throws HierarchyNodeNotFoundException;

	List<JobGroup<Job>> createAndlinkJobGroups(long hierarchyNodeId, long... standardJobGroupIds) throws HierarchyNodeNotFoundException, JobNotFoundException;

	PageItr<Job> getLinkedJob(long hierarchyNodeId) throws HierarchyNodeNotFoundException;

	PageItr<StandardHierarchyNode<StandardHierarchyNode>> getChildStandardHierarchyNode(long[] parentIds, int pageNumber, int pageSize);

	void saveAsHierarchyNode(long parentId, String ownerId) throws PersonNotFoundException, ServiceLevelValidationException, HierarchyNodeNotFoundException, AlreadyLinkedException;

	String[] getImageNames(long hierarchyNodeId) throws HierarchyNodeNotFoundException;

	// BufferedImage getImage(long hierarchyNodeId, String imageName) throws
	// HierarchyNodeNotFoundException, ApplicationIOException;

	PageItr<HierarchyNode> searchWithProperties(HierarchyNodeProperties hierarchyNodeProperties, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> getLinkedJobGroups(long hierarchyNodeId);

	void createAndLinkHierarchyNodes(long parentNodeId, long... standardHierarchyNodeIds) throws HierarchyNodeNotFoundException, AlreadyLinkedException;

	List<Job> createAndlinkJob(long hierarchyNodeId, long[] standardJobIds) throws HierarchyNodeNotFoundException, InvalidDataException;

	PageItr<StandardHierarchyNode> getLinkedStandardHierarchyNodes(long parentId, int pageNumber, int pageSize);

	void uploadHierarchyNodeImage(long hierarchyNodeId, String imageName, byte[] bytes, DocumentType documentType) throws ImageIOException;

	HierarchyDoc getHierarchyNodeImage(long hierarchyNodeId, String imageName, DocumentType documentType) throws HierarchyNodeNotFoundException, ImageIOException;

	void uploadHierarchyNodeDoc(long hierarchyNodeId, String imageName, byte[] bytes, DocumentType documentType) throws DocIOException;

	public List<HierarchyDoc> getHierarchyNodeDocs(long hierarchyNodeId, DocumentType documentType) throws HierarchyNodeNotFoundException;

	void createHierarchyNode(List<HierarchyNode> hierarchyNodes);

	PageItr<HierarchyNode> getParentHierarchyNodes(long[] childIds, int pageNumber, int pageSize);

	StandardHierarchyNode[] createStandardHierarchyNodes(StandardHierarchyNode[] hierarchyNodes, String ownerId) throws ServiceLevelValidationException, PersonNotFoundException;

	StandardHierarchyNode createStandardHierarchyNode(StandardHierarchyNode standardHierarchyNode, String owner) throws ServiceLevelValidationException, PersonNotFoundException;

	PageItr<Job> getJobsByConnectedInchildNodeIdsOtherThan(long parentIds, int pageNumber, int pageSize) throws HierarchyNodeNotFoundException;

	PageItr<StandardHierarchyNode> searchStandardHierarchyNodesInLevel(String hierarchyNodename, String hierarchyLevelName, int pageNumber, int pageSize)
			throws HierarchyLevelNotFoundException;

	Job createPuchListJob(long parentIds, Job job) throws HierarchyNodeNotFoundException, InvalidDataException;

	JobGroup<Job> createPuchListJobGroup(long parentIds, JobGroup<Job> jobGroup) throws HierarchyNodeNotFoundException, InvalidDataException;

	void removeHierarchyNodeLink(long[] hierarchyNodeIds) throws HierarchyNodeNotFoundException, RemovalBarrierException;

	void removeLinkedJobGroups( long[] jobGroupIds) throws HierarchyNodeNotFoundException, RemovalBarrierException;
	
	public List<Long> getParentIds(long hierarchyId,List<Long> nodeids);

}
