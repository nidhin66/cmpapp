package com.gvg.syena.core.api.entity.time;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author geevarughesejohn
 *
 */
@Embeddable
public class ActionTime {

	@Temporal(TemporalType.TIMESTAMP)
	private Date estimatedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date revisedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date actualTime;

	public ActionTime() {
	}

	public ActionTime(Date planedTime) {
		setPlannedTime(planedTime);
	}

	public Date getActualTime() {
		return actualTime;
	}

	public void setActualTime(Date actualTime) {
		this.actualTime = actualTime;
	}

	public Date getRevisedTime() {
		return revisedTime;
	}

	public void setRevisedTime(Date revisedTime) {
		this.revisedTime = revisedTime;
	}

	public Date getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(Date estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	@JsonIgnore
	public Date getPlannedTime() {
		if (revisedTime != null) {
			return revisedTime;
		} else {
			return estimatedTime;
		}
	}

	@JsonIgnore
	public void setPlannedTime(Date planedTime) {

		if (estimatedTime != null && ! DateUtil.equals(estimatedTime, planedTime)) {
			revisedTime = planedTime;
		} else {
			estimatedTime = planedTime;
		}
	}

	public ActionTime copy() {
		ActionTime actionTime = new ActionTime();
		if (estimatedTime != null) {
			actionTime.setEstimatedTime((Date) estimatedTime.clone());
		}
		if (revisedTime != null) {
			actionTime.setRevisedTime((Date) revisedTime.clone());
		}
		if (actualTime != null) {
			actionTime.setActualTime((Date) actualTime.clone());
		}
		return actionTime;
	}

}
