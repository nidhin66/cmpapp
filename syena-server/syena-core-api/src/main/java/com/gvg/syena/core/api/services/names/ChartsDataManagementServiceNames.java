package com.gvg.syena.core.api.services.names;

public interface ChartsDataManagementServiceNames {

	String getChartsDatas = "getChartsDatas";
	String getPredictionChartsDatas = "getPredictionChartsDatas";

}
