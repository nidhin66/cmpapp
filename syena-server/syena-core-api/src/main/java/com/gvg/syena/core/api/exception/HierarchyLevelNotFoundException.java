package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.MessageKeys;

public class HierarchyLevelNotFoundException extends ApplicationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String levelName;

	public HierarchyLevelNotFoundException(String levelName, String messageString) {
		super(MessageKeys.HIERARCHY_LEVEL_NOT_FOUND, messageString);
		this.levelName = levelName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

}
