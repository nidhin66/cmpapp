package com.gvg.syena.core.api.entity.common;

public interface Item<T> {

	String getName();

	long getId();

	String getDescription();
	
	boolean isEquual(T dependent);
}
