package com.gvg.syena.core.api.entity.chart;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import com.gvg.syena.core.api.entity.time.DateUtil;

/**
 * @author geevarughesejohn
 *
 * @param <T>
 */
public class ChartData<T> {

	private String[] dataSeriesNames;

	private List<ChartPoint<T>> chartPoints;

	public ChartData() {

	}

	public ChartData(String[] names) {
		this.dataSeriesNames = names;
		this.chartPoints = new ArrayList<ChartPoint<T>>();
	}

	public String[] getDataSeriesNames() {
		return dataSeriesNames;
	}

	public List<ChartPoint<T>> getChartPoints() {
		return chartPoints;
	}

	public void setChartPoints(List<ChartPoint<T>> chartPoints) {
		this.chartPoints = chartPoints;
	}

	public void addChartPoint(T t, Float[] values) {
		ChartPoint<T> chartPoint = new ChartPoint<T>(t, values);
		chartPoints.add(chartPoint);
	}

	public void removeAfter100() {
		if (chartPoints != null && chartPoints.size() > 0) {
			int length = chartPoints.get(0).getValues().length;

			for (int i = 0; i < length; i++) {
				Float lastValue = null;
				for (ChartPoint chartPoint : chartPoints) {
					if (lastValue != null && lastValue == 100) {
						chartPoint.setValues(i, null);
					} else {
						lastValue = chartPoint.getValues()[i];
					}

				}
			}

		}

	}

	public void removeAfter0() {
		if (chartPoints != null && chartPoints.size() > 0) {
			int length = chartPoints.get(0).getValues().length;

			for (int i = 0; i < length; i++) {
				Float lastValue = null;
				ListIterator<ChartPoint<T>> listIterator = chartPoints.listIterator(chartPoints.size());
				while (listIterator.hasPrevious()) {
					ChartPoint chartPoint = listIterator.previous();
					Float value = chartPoint.getValues()[i];
					if (value != null && value == 0) {
						chartPoint.setValues(i, null);

					} else {
						break;
					}
				}

			}

		}

	}

	public void removeAfter(Date date, int i) {
		for (ChartPoint chartPoint : chartPoints) {
			if (DateUtil.after((Date) chartPoint.getPoint(), date)) {
				chartPoint.setValues(i, null);
			}
		}
	}

}
