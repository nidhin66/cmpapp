package com.gvg.syena.core.api.entity.common;

public enum WorkStatusType {

	// Items completed on schedule, Ongoing items - on track, Ongoing items –
	// delayed, Items completed with delays, No ongoing work, but planned work
	// available, No work planned

	Not_scheduled("Not Scheduled"), Completed_on_schedule("Completed on Schedule"), Ongoing_on_track("Ongoing on Track"), Ongoing_delayed("Ongoing Delayed"), Completed_with_delays(
			"Completed with Delays"), Not_Stared("Not Started");
	private String text;

	WorkStatusType(String text) {
		this.text = text;
	}

	public static WorkStatusType getWorkStatusType(WorkStatus workStatus) {
		if (workStatus.getRunningStatus() == RunningStatus.NotScheduled) {
			return Not_scheduled;
		}
		WorkStatusType type = Not_Stared;
		// if (workStatus.getRunningStatus() == RunningStatus.completed) {
		// if (workStatus.isDelayToEnd() || workStatus.isDelayToStart()) {
		// type = Completed_with_delays;
		// } else {
		// type = Completed_on_schedule;
		// }
		// } else if (workStatus.getRunningStatus() == RunningStatus.onprogress)
		// {
		// if (workStatus.isDelayToStart()) {
		// type = Ongoing_delayed;
		// } else {
		// type = Ongoing_on_track;
		// }
		// }

		if (workStatus.getRunningStatus() == RunningStatus.completed) {
			if (workStatus.isDelayInCurrentStage()) {
				type = Completed_with_delays;
			} else {
				type = Completed_on_schedule;
			}
		} else if (workStatus.getRunningStatus() == RunningStatus.onprogress) {
			if (workStatus.isDelayInCurrentStage()) {
				type = Ongoing_delayed;
			} else {
				type = Ongoing_on_track;
			}
		}
		return type;
	}

	public String toString() {
		return text;
	}

	public WorkStatus getWorkStatus() {
		WorkStatus workStatus = new WorkStatus();
		workStatus.setRunningStatus(this.getRunningStatus());
		workStatus.setDelayToEnd(this.getDelayToEnd());
		workStatus.setDelayToStart(this.getDelayToStart());
		workStatus.setDelayInCurrentStage(this.isDelayInCurrentStage());
		return workStatus;

	}

	private boolean isDelayInCurrentStage() {
		if (this == Ongoing_delayed || this == Completed_with_delays || this == Not_Stared) {
			return true;
		} else {
			return false;
		}
	}

	private boolean getDelayToStart() {
		if (this == Ongoing_delayed) {
			return true;
		}
		return false;

	}

	private boolean getDelayToEnd() {
		if (this == Completed_with_delays) {
			return true;
		}
		return false;
	}

	private RunningStatus getRunningStatus() {

		RunningStatus runningStatus = null;
		if (this == Not_scheduled) {
			runningStatus = RunningStatus.NotScheduled;
		} else if (this == Completed_on_schedule || this == Completed_with_delays) {
			runningStatus = RunningStatus.completed;
		} else if (this == Ongoing_delayed || this == Ongoing_on_track) {
			runningStatus = RunningStatus.onprogress;
		} else if (this == Not_Stared) {
			runningStatus = RunningStatus.notStarted;
		}
		return runningStatus;
	}
}
