package com.gvg.syena.core.api.services.serach;

import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

public interface AdvancedJobGroupSearch {

	PageItr<JobGroup<Job>> getOnGoingJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> completedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> plannedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> delayedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> getJobsInitiatedBy(String personId, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> getJobsOwnedBy(String personId, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> jobsToBeScheduled(String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup<Job>> jobsWithRevisedDates(String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup> searchJobs(JobProperties advancedSearchOptions, int pageNaumber, int pageSize);

	PageItr<JobGroup> jobGroupwithDelyedStartJobs(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup> jobGroupwithIncreasedDurationJobs(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup> jobGroupsInCriticalPath(long hierarchyNodeId, String jobGroupName, int pageNumber, int pageSize);

	PageItr<JobGroup> jobsGroupsStartInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize);

	PageItr<JobGroup> jobsGroupsCompleteInNDays(Date fromDate, Date toDate, String jobName, int pageNumber, int pageSize);

	SortedSet<Long> jobGroupwithDelyedStartJobGroupId(long hierarchyNodeId, ValueComparitor valueComparitor, float percentage);

	List<Long> jobGroupIdwithIncreasedDurationJobs(Long hierarchyNodeId, ValueComparitor valueComparitor, float percentage);

}
