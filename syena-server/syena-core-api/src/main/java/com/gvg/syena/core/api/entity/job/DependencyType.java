package com.gvg.syena.core.api.entity.job;

public enum DependencyType {

	startToStart, startToFinish, FinishToStart, FinishToFinish;

	public boolean isEqual(DependencyType dependencyType) {
		return this == dependencyType;
	}

	public DependencyType inverse(boolean inverse) {
		if (inverse) {
			if (this == startToFinish) {
				return FinishToStart;
			} else if (this == FinishToStart) {
				return startToFinish;
			}
		}
		return this;
	}

}
