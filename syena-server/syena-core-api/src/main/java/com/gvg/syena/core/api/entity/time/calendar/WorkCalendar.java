package com.gvg.syena.core.api.entity.time.calendar;

import java.util.Date;

public interface WorkCalendar {

	Date getDateAfter(Date date, int hrs, int min, int min2);

	Date getDateBefore(Date date, int hrs, int min, int min2);

	PersonHours getPersonHrs(Date startTime, Date finishTime);

	int getPersonDays(Date startTime, Date finishTime);

	Date getDateAfter(Date date, int numberofDays);

	Date getDateBefore(Date date, int numberofDays);

	boolean beforeNow(Date date);

	boolean beforeOrOn(Date date, Date baseDate);

	boolean OnOrAfter(Date date, Date baseDate);

	TimePoint getDayStartTime();

	TimePoint getDayEndTime();

}
