package com.gvg.syena.core.api.entity.organization;

import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Person {

	@Id
	private String id;

	@JsonIgnore
	private String password;

	// TODO pattern checking
	@JsonIgnore
	private String emailAddress;

	@JsonIgnore
	@ManyToMany
//	@JsonDeserialize(keyUsing = DepartmentJsonSerializer.class)
	private Map<Department, Role> rolesInDepartment;

	@OnlyForJPA
	public Person() {
	}

	public Person(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Map<Department, Role> getRolesInDepartment() {
		return rolesInDepartment;
	}

	public void setRolesInDepartment(Map<Department, Role> rolesInDepartment) {
		this.rolesInDepartment = rolesInDepartment;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
