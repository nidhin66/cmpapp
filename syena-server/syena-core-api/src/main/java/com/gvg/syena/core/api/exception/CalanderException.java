package com.gvg.syena.core.api.exception;

public class CalanderException extends RuntimeException {
	public CalanderException(String message) {
		super(message);
	}
}
