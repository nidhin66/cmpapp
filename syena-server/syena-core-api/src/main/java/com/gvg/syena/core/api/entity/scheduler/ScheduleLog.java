package com.gvg.syena.core.api.entity.scheduler;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.job.Job;

@Entity
public class ScheduleLog implements Comparable<ScheduleLog> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private long schedulingNode;
	private NodeCategory nodeCategory;

	private long affectedNode;
	private NodeCategory affectedNodeCategory;

	private String createdUser;
	private String approvedUser;

	private Date previousStartDate;
	private Date previousEndDate;
	private Date currentStartDate;
	private Date currentEndDate;

	public ScheduleLog() {
		// TODO Auto-generated constructor stub
	}

	public ScheduleLog(long schedulingNode, NodeCategory nodeCategory, Job changedJob) {
		this.schedulingNode = schedulingNode;
		this.nodeCategory = nodeCategory;
		this.affectedNode = changedJob.getId();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ScheduleLog(long id2, Object clone, Object clone2) {
		// TODO Auto-generated constructor stub
	}

	public long getSchedulingNode() {
		return schedulingNode;
	}

	public void setSchedulingNode(long schedulingNode) {
		this.schedulingNode = schedulingNode;
	}

	public NodeCategory getNodeCategory() {
		return nodeCategory;
	}

	public void setNodeCategory(NodeCategory nodeCategory) {
		this.nodeCategory = nodeCategory;
	}

	public long getAffectedNode() {
		return affectedNode;
	}

	public void setAffectedNode(long affectedNode) {
		this.affectedNode = affectedNode;
	}

	public NodeCategory getAffectedNodeCategory() {
		return affectedNodeCategory;
	}

	public void setAffectedNodeCategory(NodeCategory affectedNodeCategory) {
		this.affectedNodeCategory = affectedNodeCategory;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getApprovedUser() {
		return approvedUser;
	}

	public void setApprovedUser(String approvedUser) {
		this.approvedUser = approvedUser;
	}

	public Date getPreviousStartDate() {
		return previousStartDate;
	}

	public void setPreviousStartDate(Date previousStartDate) {
		this.previousStartDate = previousStartDate;
	}

	public Date getPreviousEndDate() {
		return previousEndDate;
	}

	public void setPreviousEndDate(Date previousEndDate) {
		this.previousEndDate = previousEndDate;
	}

	public Date getCurrentStartDate() {
		return currentStartDate;
	}

	public void setCurrentStartDate(Date currentStartDate) {
		this.currentStartDate = currentStartDate;
	}

	public Date getCurrentEndDate() {
		return currentEndDate;
	}

	public void setCurrentEndDate(Date currentEndDate) {
		this.currentEndDate = currentEndDate;
	}

	@Override
	public int compareTo(ScheduleLog o) {
		return (int) (this.getId() - o.getId());
	}

}
