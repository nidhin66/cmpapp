package com.gvg.syena.core.api.entity.scheduler;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ScheduleVarianceLog {

	@Id
	private long hierarchyId;
	private double currentVariance;
	private double notifiedVariance;
	public long getHierarchyId() {
		return hierarchyId;
	}
	public void setHierarchyId(long hierarchyId) {
		this.hierarchyId = hierarchyId;
	}
	public double getCurrentVariance() {
		return currentVariance;
	}
	public void setCurrentVariance(double currentVariance) {
		this.currentVariance = currentVariance;
	}
	public double getNotifiedVariance() {
		return notifiedVariance;
	}
	public void setNotifiedVariance(double notifiedVariance) {
		this.notifiedVariance = notifiedVariance;
	}
	
	
	
}
