package com.gvg.syena.core.api.services;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

public interface StatusUpdationService {

	void computeUpwardStatus(Job job);

	void computeUpwardStatus(JobGroup<Job> jobGroup);

	void computeUpwardStatus(HierarchyNode node);

}
