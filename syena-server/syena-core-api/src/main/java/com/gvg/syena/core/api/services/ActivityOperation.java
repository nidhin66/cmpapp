package com.gvg.syena.core.api.services;

public enum ActivityOperation {

	create_new, create_approve, update_new, update_approve, delete_new, delete_approve;

}
