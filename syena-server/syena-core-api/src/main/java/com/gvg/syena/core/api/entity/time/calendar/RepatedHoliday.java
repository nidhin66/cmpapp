package com.gvg.syena.core.api.entity.time.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RepatedHoliday implements OffCalander {

	private List<OffTime> offtimes;

	public RepatedHoliday() {
		this.offtimes = new ArrayList<OffTime>(5);
	}

	@Override
	public void outerWithMax(DateInterval dateInterval, List<DateInterval> offIntervals) {
		for (OffTime offTime : offtimes) {
			offTime.getouterWithMax(dateInterval, offIntervals);
		}
	}

	public void repeatedOffTime(Week week, Day day) {
		offtimes.add(new OffTime(week, day));
	}

	public void repeatedOffTime(Week week, Day day, Time time) {
		offtimes.add(new OffTime(week, day, time));
	}

}

enum Week {
	ALL(0), FIRST(1), SECOND(2), THIRD(3), FOURTH(4), FIFTH(5);

	private int weekNumber;

	private Week(int weekNumber) {

		this.weekNumber = weekNumber;
	}

	public Week isSatisfy(Date date) {
		if (this == ALL) {
			return ALL;
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int wn = cal.get(Calendar.WEEK_OF_MONTH);
			return getWeek(wn);
		}
	}

	private Week getWeek(int wn) {
		for (Week week : values()) {
			if (week.getWeekNumber() == wn) {
				return week;
			}
		}
		return null;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

};

enum Day {
	SUNDAY(1), MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7);

	private int dayNumber;

	Day(int dayNumber) {
		this.dayNumber = dayNumber;
	}

	public Day isSatisfy(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dn = cal.get(Calendar.DAY_OF_WEEK);
		return getDay(dn);
	}

	private Day getDay(int dn) {
		for (Day day : values()) {
			if (day.getDayNumber() == dn) {
				return day;
			}
		}
		return null;
	}

	public int getDayNumber() {
		return dayNumber;
	}
}

enum Time {
	ALL(new TimePoint(0, 0), new TimePoint(23, 59)), MORNING(new TimePoint(6, 0), new TimePoint(11, 59)), AFTERNOON(new TimePoint(12, 0), new TimePoint(17, 59)), DAY(new TimePoint(6, 0),
			new TimePoint(17, 59));

	private TimePoint fromTimePoint;
	private TimePoint toTimePoint;

	private Time(TimePoint fromTimePoint, TimePoint toTimePoint) {
		this.fromTimePoint = fromTimePoint;
		this.toTimePoint = toTimePoint;
	}

	public TimePoint getFromTimePoint() {
		return fromTimePoint;
	}

	public void setFromTimePoint(TimePoint fromTimePoint) {
		this.fromTimePoint = fromTimePoint;
	}

	public TimePoint getToTimePoint() {
		return toTimePoint;
	}

	public void setToTimePoint(TimePoint toTimePoint) {
		this.toTimePoint = toTimePoint;
	}

}

class OffTime {

	private Week week;

	private Day day;

	private TimePoint fromTimePoint;

	private TimePoint toTimePoint;

	public OffTime(Week week, Day day) {
		this(week, day, Time.ALL);
	}

	public List<DateInterval> getouterWithMax(DateInterval dateInterval, List<DateInterval> offIntervals) {
		Iterator<Date> dateIterator = dateInterval.dayIterator();
		while (dateIterator.hasNext()) {
			Date date = dateIterator.next();
			Week gweek = week.isSatisfy(date);
			if (gweek == week) {
				Day gday = day.isSatisfy(date);
				if (gday == day) {
					OffPeriodDateInterval offPeriodDateInterval = getTimePeriod(date);
					offPeriodDateInterval.setWeek(gweek);
					offPeriodDateInterval.setDay(gday);
					offIntervals.add(offPeriodDateInterval);
				}
			}
		}
		return offIntervals;
	}

	private OffPeriodDateInterval getTimePeriod(Date date) {
		Date statTime = new Date(date.getTime());
		statTime.setHours(fromTimePoint.getHr());
		statTime.setMinutes(fromTimePoint.getMin());
		statTime.setSeconds(fromTimePoint.getSec());
		Date endTime = new Date(date.getTime());
		endTime.setHours(toTimePoint.getHr());
		endTime.setMinutes(toTimePoint.getMin());
		endTime.setSeconds(toTimePoint.getSec());
		OffPeriodDateInterval dateInterval = new OffPeriodDateInterval(statTime, endTime);
		return dateInterval;
	}

	public OffTime(Week week, Day day, Time time) {
		this.week = week;
		this.day = day;
		this.fromTimePoint = time.getFromTimePoint();
		this.toTimePoint = time.getToTimePoint();
	}

	public Week getWeek() {
		return week;
	}

	public void setWeek(Week week) {
		this.week = week;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public TimePoint getFromTimePoint() {
		return fromTimePoint;
	}

	public void setFromTimePoint(TimePoint fromTimePoint) {
		this.fromTimePoint = fromTimePoint;
	}

	public TimePoint getToTimePoint() {
		return toTimePoint;
	}

	public void setToTimePoint(TimePoint toTimePoint) {
		this.toTimePoint = toTimePoint;
	}

}

 class WorkTimeIterator implements Iterator<Date> {

	private Date iterationStartFrom;
	private Date iterationStartTo;
	private Date currentdate;

	public WorkTimeIterator(DailyWorkTime dailyWorkTime, Date iterationStartFrom, Date iterationStartTo) {
		this.iterationStartFrom = iterationStartTo;
		this.iterationStartTo = iterationStartTo;
		this.currentdate = new Date(iterationStartFrom.getTime());
	}

	@Override
	public boolean hasNext() {
		return currentdate.before(iterationStartTo);
	}

	@Override
	public Date next() {

		if (currentdate.equals(iterationStartFrom)) {

			currentdate.setHours(23);
			currentdate.setMinutes(59);
			currentdate.setSeconds(59);
			return currentdate;
		} else {
			if (currentdate.after(iterationStartTo)) {
				currentdate.setHours(iterationStartTo.getHours());
				currentdate.setMinutes(iterationStartTo.getMinutes());
				currentdate.setSeconds(iterationStartTo.getSeconds());
				return currentdate;
			} else {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(currentdate);
				calendar.add(Calendar.DAY_OF_YEAR, 1);
				currentdate = calendar.getTime();
				return currentdate;
			}
		}

	}

}