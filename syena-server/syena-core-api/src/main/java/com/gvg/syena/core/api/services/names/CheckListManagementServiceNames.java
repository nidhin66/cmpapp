package com.gvg.syena.core.api.services.names;

public interface CheckListManagementServiceNames {

	String createCheckListItem = "createCheckListItem";
	String deleteCheckList = "deleteCheckList";
	String updateStatus = "updateStatus";
	String upadteCheckListItem = "upadteCheckListItem";
	String createCheckLists = "createCheckLists";
	String addCheckListItemsInCheckList = "addCheckListItemsInCheckList";
	String updateCheckList = "updateCheckList";
	String getAllStandardCheckList = "getAllStandardCheckList";
	String getAllCheckList = "getAllCheckList";
	String findInStandardCheckList = "findInStandardCheckList";
	String findInCheckList = "findInCheckList";
	String createCheckList = "createCheckList";
	String getCheckListByParent = "getCheckListByParent";
	String findInCheckListWithParent = "findInCheckListWithParent";

}
