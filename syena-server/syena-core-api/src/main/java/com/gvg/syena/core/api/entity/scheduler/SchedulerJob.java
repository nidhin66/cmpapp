package com.gvg.syena.core.api.entity.scheduler;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class SchedulerJob {
	
	@Id

	@GeneratedValue(strategy = GenerationType.AUTO)

	private long id;
	
	private String name;
	
	private String description;
	
	private long jobBeanId;
	
	private String jobBeanClass;
	
	private String cronExpression;
	
	private boolean enabled;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public long getJobBeanId() {
		return jobBeanId;
	}

	public void setJobBeanId(long jobBeanId) {
		this.jobBeanId = jobBeanId;
	}

	public String getJobBeanClass() {
		return jobBeanClass;
	}

	public void setJobBeanClass(String jobBeanClass) {
		this.jobBeanClass = jobBeanClass;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	

}
