package com.gvg.syena.core.api.exception;

import java.util.List;

import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.MessageKeys;

public class RemovalBarrierException extends ApplicationException {

	public static final String PredecessorDependency = "predecessor dependency";
	public static final String SuccessorDependency = "successor dependency";
	public static final String job = "jobs";
	public static final String jobGroup = "job groups";
	public static final String PredecessorHierarchyNode = "predecessor nodes";
	public static final String SuccessorHierarchyNode = "successor node";
	
	private List<Job> jobs;
	private List<JobGroup> jobGroups;
	private DependencyAttachableHierarchy node;

//	public RemovalBarrierException(HierarchyNode hierarchyNode, List<Job> jobs, List<JobGroup> jobGroups, String messageId, String messageString) {
//		super(messageId, messageString);
//		this.jobs = jobs;
//		this.jobGroups = jobGroups;
//	}

	public RemovalBarrierException(DependencyAttachableHierarchy node, String dependency) {
		super(MessageKeys.HIERARCHYNODEREMOVALBARRIER, node.getName() + " contain " + dependency);
		this.node = node;
	}

	

}
