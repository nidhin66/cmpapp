package com.gvg.syena.core.api.services;

import java.util.List;

import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;

public interface ApplicationService {

	String getPropertyValue(String key);
	
	public List<ApplicationConfiguration> getApplicationConfigs();

}
