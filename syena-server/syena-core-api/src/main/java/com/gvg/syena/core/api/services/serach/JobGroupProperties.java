package com.gvg.syena.core.api.services.serach;

import com.gvg.syena.core.api.entity.common.NodeType;

public class JobGroupProperties {

	private String name;
	private String description;
	private NodeType nodeType;
	private long connectedNodeId;
	private boolean isStandard;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

	public long getConnectedNodeId() {
		return this.connectedNodeId;

	}

	public void setConnectedNodeId(long connectedNodeId) {
		this.connectedNodeId = connectedNodeId;
	}

	public boolean isStandard() {
		return isStandard;
	}
	
	public void setStandard(boolean isStandard) {
		this.isStandard = isStandard;
	}

}
