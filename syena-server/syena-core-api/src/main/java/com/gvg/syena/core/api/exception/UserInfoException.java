package com.gvg.syena.core.api.exception;

public class UserInfoException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public UserInfoException(Exception e) {
		super(e);
	}

	public UserInfoException(String messageKey, String messageString) {
		super(messageKey, messageString);
	}

	public UserInfoException(String messageKey, String messageString, Exception e) {
		super(messageKey, messageString, e);
	}
}
