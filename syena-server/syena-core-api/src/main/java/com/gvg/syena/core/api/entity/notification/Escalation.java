package com.gvg.syena.core.api.entity.notification;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;

@Entity
public class Escalation {
	
	@Id
	private long id;
	
	private int level;
	
	@ManyToMany(cascade = CascadeType.ALL ,fetch=FetchType.EAGER)
	private List<Role> toRoles;
	
	@ManyToMany(cascade = CascadeType.ALL , fetch=FetchType.EAGER)
	private List<Person> toPersons;
	
	private int intervalInDays;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public List<Role> getToRoles() {
		return toRoles;
	}

	public void setToRoles(List<Role> toRoles) {
		this.toRoles = toRoles;
	}

	public List<Person> getToPersons() {
		return toPersons;
	}

	public void setToPersons(List<Person> toPersons) {
		this.toPersons = toPersons;
	}

	public int getIntervalInDays() {
		return intervalInDays;
	}

	public void setIntervalInDays(int intervalInDays) {
		this.intervalInDays = intervalInDays;
	}
	
	
	 

}
