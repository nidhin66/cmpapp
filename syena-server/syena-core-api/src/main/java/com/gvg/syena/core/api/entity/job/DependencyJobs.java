package com.gvg.syena.core.api.entity.job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.cache.annotation.Cacheable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gvg.syena.core.api.DependencyAttachable;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.time.DateUtil;
import com.gvg.syena.core.api.entity.util.DependencyBarrier;

/**
 * @author geevarughesejohn
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Cacheable(key = "id")
public class DependencyJobs {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@OneToOne(cascade = CascadeType.ALL)
	private JobGroup<Job> startToStart;

	@OneToOne(cascade = CascadeType.ALL)
	private JobGroup<Job> startToFinish;

	@OneToOne(cascade = CascadeType.ALL)
	private JobGroup<Job> finishToStart;

	@OneToOne(cascade = CascadeType.ALL)
	private JobGroup<Job> finishToFinish;

	public DependencyJobs() {
		startToStart = new JobGroup<Job>("");
		startToStart.setNodeType(NodeType.internal);

		startToFinish = new JobGroup<Job>("");
		startToFinish.setNodeType(NodeType.internal);

		finishToStart = new JobGroup<Job>("");
		finishToStart.setNodeType(NodeType.internal);

		finishToFinish = new JobGroup<Job>("");
		finishToFinish.setNodeType(NodeType.internal);

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public JobGroup<Job> getStartToStart() {
		return startToStart;
	}

	public void setStartToStart(JobGroup<Job> startToStart) {
		this.startToStart = startToStart;
	}

	public JobGroup<Job> getStartToFinish() {
		return startToFinish;
	}

	public void setStartToFinish(JobGroup<Job> startToFinish) {
		this.startToFinish = startToFinish;
	}

	public JobGroup<Job> getFinishToStart() {
		return finishToStart;
	}

	public void setFinishToStart(JobGroup<Job> finishToStart) {
		this.finishToStart = finishToStart;
	}

	public JobGroup<Job> getFinishToFinish() {
		return finishToFinish;
	}

	public void setFinishToFinish(JobGroup<Job> finishToFinish) {
		this.finishToFinish = finishToFinish;
	}

	@JsonIgnore
	public JobGroup<Job> getFinishForStart() {
		return this.finishToStart;
	}

	@JsonIgnore
	public JobGroup<Job> getStartForStart() {
		return this.startToStart;
	}

	@JsonIgnore
	public JobGroup<Job> getStartForFinish() {
		return this.startToFinish;
	}

	@JsonIgnore
	public JobGroup<Job> getFinishForFinish() {
		return this.finishToFinish;
	}

	@JsonIgnore
	public void addJob(Job job, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobs().add(job);
	}

	@JsonIgnore
	public void removeJob(Job job, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobs().remove(job);

	}

	@JsonIgnore
	public void addJobs(List<Job> jobs, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobs().addAll(jobs);
	}

	@JsonIgnore
	public void removeJobs(List<Job> jobs, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobs().removeAll(jobs);
	}

	@JsonIgnore
	public void addJobGroup(JobGroup jobGroup, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobGroups().add(jobGroup);

	}

	@JsonIgnore
	public void addJobGroups(Collection jobGroups, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobGroups().addAll(jobGroups);
	}

	@JsonIgnore
	public void removeJobGroups(List<JobGroup<Job>> jobGroups, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobGroups().removeAll(jobGroups);

	}

	@JsonIgnore
	public void removeJobGroup(JobGroup jobGroup, DependencyType dependencyType) {
		getDefaultJobGroup(dependencyType).getJobGroups().remove(jobGroup);
	}

	@JsonIgnore
	private JobGroup<Job> getDefaultJobGroup(DependencyType dependencyType) {
		if (DependencyType.startToStart == dependencyType) {
			return getStartToStart();
		} else if (DependencyType.startToFinish == dependencyType) {
			return getStartToFinish();
		} else if (DependencyType.FinishToStart == dependencyType) {
			return getFinishToStart();
		} else if (DependencyType.FinishToFinish == dependencyType) {
			return getFinishToFinish();
		}
		return null;

	}

	/**
	 * 
	 * @param date
	 * @return
	 * @throws DependencyBarrier
	 */
	@JsonIgnore
	public DependencyBarrier analyseBarriersToStart(Date date) {

		List<Job> barriersNotStarted = new ArrayList<Job>();
		List<Job> barriersNotEnded = new ArrayList<Job>();

		for (Job job : startToStart.getJobs()) {
			if (job.isApplicableForComplete()) {
				if (job.getWorkStatus().isStarted()) {
					Date actualStartTime = job.getWorkSchedule().getActualStartTime();
					if (actualStartTime == null || DateUtil.after(actualStartTime, date)) {
						barriersNotStarted.add(job);
					}
				} else {
					barriersNotStarted.add(job);
				}
			}
		}

		for (Job job : finishToStart.getJobs()) {
			if (job.isApplicableForComplete()) {
				if (job.getWorkStatus().isCompleted()) {
					Date actualStartTime = job.getWorkSchedule().getActualFinishTime();
					if (actualStartTime == null || DateUtil.after(actualStartTime, date)) {
						barriersNotStarted.add(job);
					}
				} else {
					barriersNotEnded.add(job);
				}
			}
		}

		DependencyBarrier barrierException = new DependencyBarrier();
		barrierException.setBarriersNotStarted(barriersNotStarted);
		barrierException.setBarriersNotEnded(barriersNotEnded);
		return barrierException;
	}

	@JsonIgnore
	public DependencyBarrier analyseBarriersToComplete() {
		return null;
		// List<Job> barriersNotStarted = new ArrayList<Job>();
		// List<Job> barriersNotEnded = new ArrayList<Job>();
		//
		// for (Job jobItem : startToEnd.getJobs()) {
		// if (!jobItem.getWorkStatus().isStarted()) {
		// barriersNotStarted.add(jobItem);
		// }
		// }
		//
		// for (Job jobItem : endToEnd.getJobs()) {
		// if (!jobItem.getWorkStatus().isCompleted()) {
		// barriersNotEnded.add(jobItem);
		// }
		// }
		//
		// DependencyBarrier barrierException = new DependencyBarrier();
		// barrierException.setBarriersNotStarted(barriersNotStarted);
		// barrierException.setBarriersNotEnded(barriersNotEnded);
		// return barrierException;
	}

	@JsonIgnore
	public void validateTimeScheduleStartToStart(WorkSchedule timeSchedule) {
		if (startToStart == null) {
			return;
		}
		for (Job startToStartItem : startToStart.getJobs()) {
			WorkSchedule timeSchedule2 = startToStartItem.getWorkSchedule();
			if (!timeSchedule2.isStartBeforeStart(timeSchedule)) {
				// throw new TimeScheduleException(timeSchedule2,
				// TimeScheduleException.startToStart, "",
				// "Start to Start Depended job not before this job");
			}
		}

	}

	@JsonIgnore
	public void validateTimeScheduleStartToEnd(WorkSchedule timeSchedule) {
		if (startToFinish == null) {
			return;
		}
		for (Job startToStartItem : startToFinish.getJobs()) {
			WorkSchedule timeSchedule2 = startToStartItem.getWorkSchedule();
			if (!timeSchedule2.isStartBeforeEnd(timeSchedule)) {
				// throw new TimeScheduleException(timeSchedule2,
				// TimeScheduleException.startToEnd, "",
				// "Start to Start Depended job not before this job");
			}
		}

	}

	@JsonIgnore
	public void validateTimeScheduleEndToEnd(WorkSchedule timeSchedule) {
		if (finishToFinish == null) {
			return;
		}
		for (Job startToStartItem : finishToFinish.getJobs()) {
			WorkSchedule timeSchedule2 = startToStartItem.getWorkSchedule();
			if (!timeSchedule2.isEndBeforeEnd(timeSchedule)) {
				// throw new TimeScheduleException(timeSchedule2,
				// TimeScheduleException.EndToEnd, "",
				// "Start to Start Depended job not before this job");
			}
		}

	}

	@JsonIgnore
	public void validateTimeScheduleEndToStart(WorkSchedule timeSchedule) {
		if (finishToStart == null) {
			return;
		}
		for (Job startToStartItem : finishToStart.getJobs()) {
			WorkSchedule timeSchedule2 = startToStartItem.getWorkSchedule();
			if (!timeSchedule2.isEndBeforeStart(timeSchedule)) {
				// throw new TimeScheduleException(timeSchedule2,
				// TimeScheduleException.EndToStart, "",
				// "Start to Start Depended job not before this job");
			}
		}

	}

	public void add(DependencyAttachable jobItem, DependencyType dependencyType) {
		if (jobItem instanceof Job) {
			Job job = (Job) jobItem;
			addJob(job, dependencyType);
		} else if (jobItem instanceof JobGroup) {
			JobGroup jobGroup = (JobGroup) jobItem;
			addJobGroup(jobGroup, dependencyType);
		}

	}

	public void remove(DependencyAttachable jobItem, DependencyType dependencyType) {
		if (jobItem instanceof Job) {
			Job job = (Job) jobItem;
			removeJob(job, dependencyType);
		} else if (jobItem instanceof JobGroup) {
			JobGroup jobGroup = (JobGroup) jobItem;
			removeJobGroup(jobGroup, dependencyType);
		}

	}

	// public List<Job> checkPathInEndToStartDependencyTree(long dependentJobId,
	// List<Job> jobPath) {
	// if (finishToStart == null) {
	// return jobPath;
	// }
	// Set<Job> jobs = finishToStart.getJobs();
	// if (jobs == null) {
	// return jobPath;
	// }
	// Iterator<Job> jobsIter = jobs.iterator();
	// while (jobsIter.hasNext()) {
	// Job job = jobsIter.next();
	// jobPath.add(job);
	// if (job.getId() == dependentJobId) {
	// break;
	// } else {
	// job.getDependencyJobs().checkPathInEndToStartDependencyTree(dependentJobId,
	// jobPath);
	// }
	// jobPath.remove(jobPath.size() - 1);
	// }
	// return jobPath;
	// }

}
