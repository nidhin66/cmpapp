package com.gvg.syena.core.api.entity.hierarchy;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.util.Configuration;

/**
 * @author geevarughesejohn
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StandardHierarchyNode<C extends StandardHierarchyNode> implements Comparable<StandardHierarchyNode>, HierarchyItem {

	@Id
	@OrderBy
	@GeneratedValue(strategy = GenerationType.TABLE)
	private long id;

	@Column(unique = true)
	private String name;

	private String description;

	private String note;

	// @JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	protected Person owner;

	@ManyToOne
	private HierarchyLevel hierarchyLevel;

	@JsonIgnore
	@OneToMany(targetEntity = StandardHierarchyNode.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<C> children;

	@JsonIgnore
	@ManyToOne(targetEntity = StandardHierarchyNode.class, fetch = FetchType.LAZY)
	@JoinColumn
	protected C parent;

	// @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Embedded
	private WorkTime workTime;

	protected double weightage;

	@JsonIgnore
	@ElementCollection
	protected Map<String, String> images;

	@OnlyForJPA
	public StandardHierarchyNode() {
	}

	public StandardHierarchyNode(String name, String description, HierarchyLevel hierarchyLevel) {
		this.name = name;
		this.description = description;
		this.hierarchyLevel = hierarchyLevel;
		this.children = new TreeSet<C>();
		this.images = new TreeMap<String, String>();
		this.workTime = new WorkTime(new PersonDays(0));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public HierarchyLevel getHierarchyLevel() {
		return hierarchyLevel;
	}

	public void setHierarchyLevel(HierarchyLevel hierarchyLevel) {
		this.hierarchyLevel = hierarchyLevel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Map<String, String> getImages() {
		return images;
	}

	public void setImages(Map<String, String> images) {
		this.images = images;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	@JsonIgnore
	public Set<C> getChildren() {
		return children;
	}

	@JsonProperty("children")
	public Set<C> getChildrenJson() {
		if (Configuration.childEnable) {
			return children;
		} else {
			return null;
		}
	}

	@JsonProperty("children")
	public void setChildren(Set<C> children) {
		this.children = children;
	}

	public C getParent() {
		return parent;
	}

	public void setParent(C parent) {
		this.parent = parent;
	}

	public void removeParent() {
		this.parent = null;
	}

	public WorkTime getWorkTime() {
		return workTime;
	}

	public void setWorkTime(WorkTime workTime) {
		this.workTime = workTime;
	}

	public boolean addChild(C childNode) {
		childNode.setParent(this);
		return children.add(childNode);
	}

	public boolean addChildren(List<C> childNodes) {
		for (C childNode : childNodes) {
			childNode.setParent(this);
		}
		if (null == children) {
			this.children = new TreeSet<C>();
		}
		return children.addAll(childNodes);
	}

	public void removeChild(C childNode) {
		children.remove(childNode);
	}

	public void setWeightage(double weightage) {
		this.weightage = weightage;
	}

	public double getWeightage() {
		return weightage;
	}

	public int compareTo(StandardHierarchyNode hierarchyNode) {
		if (this.getName() == hierarchyNode.getName()) {
			return 0;
		} else {
			return 1;
		}
	}

	public boolean isEquual(StandardHierarchyNode<StandardHierarchyNode> dependent) {
		return this.getId() == dependent.getId();
	}

	public void asString() {
		for (C child : getChildren()) {
			child.asString();
		}
	}

	@Override
	@JsonIgnore
	public NodeCategory getNodeCategory() {
		return NodeCategory.StandardHierarchyNode;
	}

	public void putDocumntPath(DocumentType documentType, String imageName, String filepath) {
		if (documentType == DocumentType.Images) {
			images.put(imageName, filepath);
		} else {

		}

	}

	public String getDocumntPath(DocumentType documentType, String imageName) {
		String path = null;
		if (documentType == DocumentType.Images) {
			path = images.get(imageName);
		} else {

		}
		return path;
	}

	public void resetParent() {
		for (StandardHierarchyNode node : getChildren()) {
			node.setParent(this);
			node.resetParent();
		}
	}

	@Override
	@JsonIgnore
	public WorkStatus getStatus() {
		return null;
	}

	
}
