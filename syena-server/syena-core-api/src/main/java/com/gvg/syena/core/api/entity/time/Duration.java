package com.gvg.syena.core.api.entity.time;

import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.time.calendar.PersonHours;
import com.gvg.syena.core.api.entity.time.calendar.TimePoint;
import com.gvg.syena.core.api.entity.time.calendar.WorkCalendar;
import com.gvg.syena.core.api.util.Configuration;

/**
 * @author geevarugehsejohn
 *
 */
@Embeddable
@DiscriminatorColumn(name = "D")
@MappedSuperclass
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = PersonDays.class, name = "PersonDays"), @Type(value = PersonHours.class, name = "PersonHours") })
public abstract class Duration {

	@JsonIgnore
	@Transient
	private WorkCalendar calender;

	@OnlyForJPA
	public Duration() {
		this(Configuration.personDaysCPMCalendar);
	}

	public Duration(WorkCalendar calender) {
		this.calender = calender;
	}

	@JsonIgnore
	public WorkCalendar getCalender() {
		return calender;
	}

	@JsonIgnore
	public void setCalender(WorkCalendar calender) {
		this.calender = calender;
	}

	@JsonIgnore
	public TimePoint getDayStartTime() {
		return calender.getDayStartTime();
	}

	@JsonIgnore
	public TimePoint getDayEndTime() {
		return calender.getDayEndTime();
	}

	@JsonIgnore
	public abstract Duration copy();

	@JsonIgnore
	public abstract Date calculateEndDate(Date date);

	@JsonIgnore
	public abstract Date calculateStatDate(Date endTime);

	@JsonIgnore
	@Deprecated
	// change to compare
	public abstract boolean isLess(Date startTime, Date finishTime);

	@Deprecated
	// change to compare
	public abstract boolean beforeToday(Date date);

	@JsonIgnore
	public abstract Date setDayStartTime(Date date);

	@JsonIgnore
	public abstract Date setDayEndTime(Date date);

	@JsonIgnore
	public abstract void add(Duration duration);

	@JsonIgnore
	public abstract double divide(Duration duration);

	@JsonIgnore
	public abstract Duration multiple(double percentageOfComplete);

	@JsonIgnore
	@Deprecated
	public abstract Float getDurationValue();
	
	@JsonIgnore
	public abstract void divide(float percentageOfWork);

	public abstract void setNumberOfDays(int numberOfDays) ;
}
