package com.gvg.syena.core.api.services.names;

public interface ScheduleManagmentServiceNames {

	String setJobSchedule = "setJobSchedule";
	String setHierarchybSchedule = "setHierarchybSchedule";
	String setJobGroupSchedule = "setJobGroupSchedule";
	String setUnApprovedJobSchedule = "setUnApprovedJobSchedule";
	String setUnApprovedHierarchybSchedule = "setUnApprovedHierarchybSchedule";
	String setUnApprovedJobGroupSchedule = "setUnApprovedJobGroupSchedule";
	String getUnApprovedHierarchySchedules = "getUnApprovedHierarchySchedules";
	String getUnApprovedJobGroupSchedules = "getUnApprovedJobGroupSchedules";
	String getUnApprovedJobSchedules = "getUnApprovedJobSchedules";
	String removeUnApprovedJobSchedule = "removeUnApprovedJobSchedule";
	String removeUnApprovedHierarchybSchedule = "removeUnApprovedHierarchybSchedule";
	String removeUnApprovedJobGroupSchedule = "removeUnApprovedJobGroupSchedule";
	String rejectUnApprovedJobSchedule = "rejectUnApprovedJobSchedule";
	String rejectUnApprovedHierarchybSchedule = "rejectUnApprovedHierarchybSchedule";
	String rejectUnApprovedJobGroupSchedule = "rejectUnApprovedJobGroupSchedule";
	String validateJobGroupWorkSchedule = "validateJobGroupWorkSchedule";
	String validateJobSchedule = "validateJobSchedule";
	String validateHierarchybSchedule = "validateHierarchybSchedule";

}
