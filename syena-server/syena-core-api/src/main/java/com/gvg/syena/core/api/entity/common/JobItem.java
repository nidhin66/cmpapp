package com.gvg.syena.core.api.entity.common;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class JobItem {

	@Id
	@OrderBy
	@GeneratedValue(strategy = GenerationType.TABLE)
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public abstract Set<CheckList> getCheckList();

	public abstract WorkTime getWorkTime();

	public abstract String getDescription();

	@JsonIgnore
	public abstract boolean isStarted();

}
