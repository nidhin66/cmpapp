package com.gvg.syena.core.api.entity.job;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gvg.syena.core.api.entity.common.Cost;
import com.gvg.syena.core.api.entity.common.Item;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.UsageStatus;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.util.Configuration;

/**
 * @author geevarugehsejohn
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StandardJob extends JobItem implements Item<StandardJob>, Comparable<StandardJob> {

	@Column(unique = true)
	private String name;

	private String description;

	// @JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private Person owner;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "amount", column = @Column(name = "estimatedCost_amount")),
			@AttributeOverride(name = "currency", column = @Column(name = "estimatedCost_currency")) })
	private Cost estimatedCost;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(nullable = true)
	private Set<CheckList> checkLists;

	@ElementCollection
	@Column(nullable = true)
	private Map<String, String> photoPaths;

	private UsageStatus usageStatus;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private JobGroup<? extends StandardJob> parentJobGroup;

	@JsonIgnore
	@Embedded
	private WorkTime workTime;

	private double weightage = 1;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "numberofDays", column = @Column(name = "totalEffert_numberofDays")) })
	private PersonDays totalEffert;

	private NodeType nodeType;

	private boolean applicableForComplete = true;

	public StandardJob() {
	}

	public StandardJob(String name, String description) {
		this.nodeType = NodeType.standard;
		this.name = name;
		this.description = description;
		this.checkLists = new TreeSet<CheckList>();

		// this.completionCheckList = new TreeSet<CheckList>();
		// this.infoCheckList = new TreeSet<CheckList>();
		this.workTime = new WorkTime(new PersonDays(1));
		this.totalEffert = new PersonDays(1);
		// this.photoPaths = new LinkedHashMap<String, String>();
	}

	public StandardJob(StandardJob standardJob, String description) {
		this("", description);
		this.owner = standardJob.getOwner();
		Cost currentEstimatedCost = standardJob.getEstimatedCost();
		if (currentEstimatedCost != null) {
			this.estimatedCost = new Cost(currentEstimatedCost);
		}
		for (CheckList checkListItem : standardJob.getCheckList()) {
			this.checkLists.add(new CheckList(checkListItem));
		}
		this.workTime = new WorkTime(standardJob.getWorkTime().getDuration().copy());
		this.nodeType = NodeType.standard;
		this.applicableForComplete = standardJob.isApplicableForComplete();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			// throw new
		}
		this.name = name;
	}

	public Set<CheckList> getCheckList() {
		return checkLists;
	}

	public void setCheckList(Set<CheckList> checkList) {
		this.checkLists = checkList;
	}

	public UsageStatus getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(UsageStatus usageStatus) {
		this.usageStatus = usageStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Cost getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(Cost estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public Map<String, String> getPhotoPaths() {
		return photoPaths;
	}

	public void setPhotoPaths(Map<String, String> photoPaths) {
		this.photoPaths = photoPaths;
	}

	public JobGroup<? extends StandardJob> getParentJobGroup() {
		return parentJobGroup;
	}

	public void setParentJobGroup(JobGroup<? extends StandardJob> parentJobGroup) {
		this.parentJobGroup = parentJobGroup;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

	@JsonIgnore
	public WorkTime getWorkTime() {
		return workTime;
	}

	@JsonProperty("workTime")
	public WorkTime getWorkTimeAsString() {
		if (Configuration.EnableWorkTimeInJob) {
			return workTime;
		} else {
			return null;
		}
	}

	@JsonProperty("workTime")
	public void setWorkTime(WorkTime workTime) {
		this.workTime = workTime;
	}

	public double getWeightage() {
		return weightage;
	}

	public void setWeightage(double weightage) {
		this.weightage = weightage;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	// @JsonIgnore
	@JsonProperty("duration")
	public Duration getDuration() {
		if (this.workTime == null) {
			return null;
		}
		return this.getWorkTime().getDuration();
	}

	// @JsonIgnore
	@JsonProperty("duration")
	public void setDuration(Duration duration) {
		if (this.workTime == null) {
			this.workTime = new WorkTime();
		}
		this.getWorkTime().setDuration(duration);
	}

	public PersonDays getTotalEffert() {
		if (totalEffert == null) {
			totalEffert = new PersonDays(1);
		}
		return totalEffert;
	}

	public void setTotalEffert(PersonDays totalEffert) {
		this.totalEffert = totalEffert;
	}

	@JsonIgnore
	public void addCheckList(CheckList checkList) {
		checkLists.add(checkList);
	}

	@JsonIgnore
	public void removeCheckList(CheckList checkList) {
		if (checkList != null) {
			checkLists.remove(checkList);
		}
	}

	@JsonIgnore
	public int compareTo(StandardJob standardJob) {
		if (this.getName().equals(standardJob.getName())) {
			return 0;
		} else {
			return 1;
		}
	}

	@Override
	@JsonIgnore
	public boolean isEquual(StandardJob dependent) {
		return this.getId() == dependent.getId();
	}

	public void validateOnCreate() throws InvalidDataException {
		if (name == null || name.trim() == "") {
			throw new InvalidDataException(MessageKeys.INVALID_JOB_NAME, "Job name should not be null or empty");
		}
	}

	@Override
	public boolean isStarted() {
		return false;
	}

	public boolean isApplicableForComplete() {
		return applicableForComplete;
	}

	public void setApplicableForComplete(boolean applicableForComplete) {
		this.applicableForComplete = applicableForComplete;
	}

}
