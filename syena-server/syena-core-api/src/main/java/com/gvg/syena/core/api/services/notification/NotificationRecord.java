package com.gvg.syena.core.api.services.notification;

import java.util.List;

public class NotificationRecord {
	
	private String recordId;
	
	private String subject;
	
	private List<PlaceHolderData> placeHolderDatas;

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public List<PlaceHolderData> getPlaceHolderDatas() {
		return placeHolderDatas;
	}

	public void setPlaceHolderDatas(List<PlaceHolderData> placeHolderDatas) {
		this.placeHolderDatas = placeHolderDatas;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	

}
