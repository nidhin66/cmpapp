package com.gvg.syena.core.api.entity.time.calendar;

import java.util.List;

public interface OffCalander {

	public void outerWithMax(DateInterval dateInterval, List<DateInterval> offIntervals);

}
