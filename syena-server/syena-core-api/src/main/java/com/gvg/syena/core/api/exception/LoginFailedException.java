package com.gvg.syena.core.api.exception;

import com.gvg.syena.core.api.entity.util.MessageKeys;

public class LoginFailedException extends ApplicationException {
	private static final long serialVersionUID = 1L;
	private String person;

	public LoginFailedException(String person, String messageString) {
		super(MessageKeys.LOGIN_FAILED, messageString);
		this.person = person;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}
}
