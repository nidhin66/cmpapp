package com.gvg.syena.core.api.entity.job.status;

import com.gvg.syena.core.api.entity.common.WorkStatus;

public enum ComputationFactor {

	Least(new LeastStatusComputation()), Highest(new HighestStatusComputation());

	private StatusComputation statusComputation;

	ComputationFactor(StatusComputation statusComputation) {
		this.statusComputation = statusComputation;

	}

	public WorkStatus compute(WorkStatus workStatus1, WorkStatus workStatus2) {
		return statusComputation.compute(workStatus1, workStatus2);
	}

}

interface StatusComputation {

	WorkStatus compute(WorkStatus workStatus1, WorkStatus workStatus2);

}

class LeastStatusComputation implements StatusComputation {

	public WorkStatus compute(WorkStatus workStatus1, WorkStatus workStatus2) {

		WorkStatus workStatus = new WorkStatus();
		workStatus.setDelayToStart(workStatus1.isDelayToStart() & workStatus2.isDelayToStart());
		workStatus.setDelayToEnd(workStatus1.isDelayToEnd() & workStatus2.isDelayToEnd());
		workStatus.setDelayInCurrentStage(workStatus1.isDelayInCurrentStage() & workStatus1.isDelayInCurrentStage());
		workStatus.setRunningStatus(workStatus1.getRunningStatus().least(workStatus2.getRunningStatus()));
		return workStatus;
	}

}

class HighestStatusComputation implements StatusComputation {

	public WorkStatus compute(WorkStatus workStatus1, WorkStatus workStatus2) {
		WorkStatus workStatus = new WorkStatus();
		workStatus.setDelayToStart(workStatus1.isDelayToStart() | workStatus2.isDelayToStart());
		workStatus.setDelayToEnd(workStatus1.isDelayToEnd() | workStatus2.isDelayToEnd());
		workStatus.setDelayInCurrentStage(workStatus1.isDelayInCurrentStage() | workStatus1.isDelayInCurrentStage());
		workStatus.setRunningStatus(workStatus1.getRunningStatus().highest(workStatus2.getRunningStatus()));
		return workStatus;
	}

}