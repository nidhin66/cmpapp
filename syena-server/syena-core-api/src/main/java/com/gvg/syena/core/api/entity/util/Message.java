package com.gvg.syena.core.api.entity.util;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Message {

	@Id
	private String errorCode;

	private MessageType messageType;

	private String description;

	private String message;

	@Transient
	private String strackTrace;

	public Message() {
	}

	public Message(String message, String strackTrace) {
		this.message = message;
		this.strackTrace = strackTrace;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStrackTrace() {
		return strackTrace;
	}

	public void setStrackTrace(String strackTrace) {
		this.strackTrace = strackTrace;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

enum MessageType {
	Error, Warining;
}