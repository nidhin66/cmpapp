package com.gvg.syena.core.api.entity.util;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.organization.Role;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;

@Entity
public class UserActivityPermission {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Enumerated(EnumType.STRING)
	private ActivityArea activityArea;

	@ElementCollection
	@Enumerated(EnumType.STRING)
	private List<ActivityOperation> activityOperations;

	@ManyToMany
	private List<Role> permittedRoles;

	@ManyToMany
	private List<Person> permittedPersons;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ActivityArea getActivityArea() {
		return activityArea;
	}

	public void setActivityArea(ActivityArea activityArea) {
		this.activityArea = activityArea;
	}

	public List<ActivityOperation> getActivityOperations() {
		return activityOperations;
	}

	public void setActivityOperations(List<ActivityOperation> activityOperations) {
		this.activityOperations = activityOperations;
	}

	public List<Role> getPermittedRoles() {
		return permittedRoles;
	}

	public void setPermittedRoles(List<Role> permittedRoles) {
		this.permittedRoles = permittedRoles;
	}

	public List<Person> getPermittedPersons() {
		return permittedPersons;
	}

	public void setPermittedPersons(List<Person> permittedPersons) {
		this.permittedPersons = permittedPersons;
	}

}
