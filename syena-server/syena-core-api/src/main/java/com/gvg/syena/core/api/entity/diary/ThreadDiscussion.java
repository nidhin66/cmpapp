package com.gvg.syena.core.api.entity.diary;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
public class ThreadDiscussion {
	@EmbeddedId
    private ThreadDiscussionId threadDiscussionId;
	
	private Date date;
	
	private String userId;
	
	@Lob
	private String comment;

	public ThreadDiscussionId getThreadDiscussionId() {
		return threadDiscussionId;
	}

	public void setThreadDiscussionId(ThreadDiscussionId threadDiscussionId) {
		this.threadDiscussionId = threadDiscussionId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
