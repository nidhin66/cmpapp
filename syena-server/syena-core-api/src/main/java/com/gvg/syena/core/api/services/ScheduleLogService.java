package com.gvg.syena.core.api.services;

import java.util.Set;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.scheduler.ScheduleLog;

public interface ScheduleLogService {

//	public void logSchedule(long jobId, NodeCategory job, Set<Job> changes);

	public void logSchedule(long jobId, NodeCategory job, Set<ScheduleLog> changes);

}
