package com.gvg.syena.core.api.services;

import java.util.List;
import java.util.Set;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.DependencyType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.exception.DependencyFailureException;
import com.gvg.syena.core.api.exception.DependencyManagerNotFoundException;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.exception.JobNotFoundException;
import com.gvg.syena.core.api.exception.JobScheduleFailed;
import com.gvg.syena.core.api.exception.SchedulingException;

public interface DependencyManagmnet {

	PageItr<Job> getSuccessorJobs(long parentid, NodeCategory nodeType, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException;

	PageItr<Job> getPredecessorJobs(long parentid, NodeCategory nodeType, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException;

	List<Job> addSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<Job> addPredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<JobGroup<Job>> addPredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	List<JobGroup<Job>> addSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, InvalidDataException;

	void removeSuccessorJobs(long parentid, long[] successoJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException;

	void removePredecessorJobs(long parentid, long[] predecessorJobIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException;

	void removePredecessorJobGroups(long parentid, long[] predecessorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException;

	void removeSuccessorJobGroups(long parentid, long[] successorJobGroupIds, DependencyType dependencyType, NodeCategory nodeType) throws JobNotFoundException,
			DependencyManagerNotFoundException;

	List<Job> addPredecessorJobs(Job job, Set<Job> predecessorJobs, DependencyType dependencyType, NodeCategory nodeCategory) throws DependencyManagerNotFoundException,
			DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException;

	List<JobGroup<Job>> addPredecessorJobGroups(Job job, Set<JobGroup<Job>> jobGroups, DependencyType dependencyType, NodeCategory nodeCategory)
			throws DependencyManagerNotFoundException, DependencyFailureException, JobScheduleFailed, SchedulingException, InvalidDataException;

	void removePredecessorJobs(Job job, Set<Job> predecessorJobs, DependencyType dependencyType, NodeCategory job2) throws DependencyManagerNotFoundException;

	void removePredecessorJobGroups(Job job, Set<JobGroup<Job>> jobGroups, DependencyType dependencyType, NodeCategory jobgroup) throws DependencyFailureException,
			DependencyManagerNotFoundException;

	PageItr<JobGroup> getSuccessorJobGroups(long jobId, NodeCategory nodeCategory, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException;

	PageItr<JobGroup> getPredecessorJobGroups(long jobId, NodeCategory nodeCategory, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException;

	PageItr<JobGroup> searchPredecessorJobGroups(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException;

	PageItr<JobGroup> searchSuccessorJobGroups(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws JobNotFoundException,
			DependencyManagerNotFoundException;

	PageItr<Job> searchSuccessorJobs(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws DependencyManagerNotFoundException, JobNotFoundException;

	PageItr<Job> searchPredecessorJobs(long id, NodeCategory nodeCategory, String name, int pageNumber, int pageSize) throws JobNotFoundException, DependencyManagerNotFoundException;

}
