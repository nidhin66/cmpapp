package com.gvg.syena.core.api.exception;

public class DocIOException extends ApplicationException {
	private long hierarchyNodeId;
	private String docName;

	public DocIOException(long hierarchyNodeId, String docName, String messageKey, String messageString) {
		super(messageKey, messageString);
		this.hierarchyNodeId = hierarchyNodeId;
		this.docName = docName;
	}

	public DocIOException(long hierarchyNodeId, String docName, Exception e) {
		super(e);
	}
}
