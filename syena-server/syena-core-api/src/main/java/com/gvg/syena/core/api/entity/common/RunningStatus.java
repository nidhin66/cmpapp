package com.gvg.syena.core.api.entity.common;

public enum RunningStatus {

	NotScheduled((short) 0), notStarted((short) 1), onprogress((short) 2), completed((short) 3);

	private short priority;

	private RunningStatus(short priority) {
		this.priority = priority;
	}

	public boolean isStarted() {
		return this == onprogress || this == completed;
	}

	public boolean isCompleted() {
		return this == completed;
	}

	public boolean isEqual(RunningStatus workStatus) {
		return this == workStatus;
	}

	public int getPriority() {
		return this.priority;
	}

	public RunningStatus least(RunningStatus runningStatus) {
		if (this.getPriority() < runningStatus.getPriority()) {
			return this;
		}
		return runningStatus;
	}

	public RunningStatus highest(RunningStatus runningStatus) {
		if (this.getPriority() > runningStatus.getPriority()) {
			return this;
		}
		return runningStatus;
	}
}
