package com.gvg.syena.core.api.services.names;

public class SearchServiceNames {

	public static final String onGoingJobs = "onGoingJobs";
	public static final String jobsOwnedBy = "jobsOwnedBy";
	public static final String completedJobs = "completedJobs";
	public static final String plannedJobs = "plannedJobs";
	public static final String delayedJobs = "delayedJobs";
	public static final String jobsInitiatedBy = "jobsInitiatedBy";
	public static final String searchJobs = "searchJobs";
	public static final String jobsToBeScheduled = "jobsToBeScheduled";
	public static final String jobsWithRevisedDates = "jobsWithRevisedDates";
	public static final String nodeWithDelayed = "nodeWithDelayed";
	public static final String jobGroupwithDelyedStartJobs = "jobGroupwithDelyedStartJobs";
	public static final String jobGroupwithIncreasedDurationJobs = "jobGroupwithIncreasedDurationJobs";
	public static final String jobswithreviseddates = "jobswithreviseddates";
	public static final String jobsInCriticalPath = "jobsInCriticalPath";
	public static final String getOnGoingJobGroups = "getOnGoingJobGroups";
	public static final String completedJobGroups = "completedJobGroups";
	public static final String plannedJobGroups = "plannedJobGroups";
	public static final String delayedJobGroups = "delayedJobGroups";
	public static final String jobGroupsInCriticalPath = "jobGroupsInCriticalPath";
	public static final String jobsGroupsStartInNDays = "jobsGroupsStartInNDays";
	public static final String jobsGroupsCompleteInNDays = "jobsGroupsCompleteInNDays";
	public static final String jobsStartInNDays = "jobsStartInNDays";
	public static final String jobsCompleteInNDays = "jobsCompleteInNDays";
	public static final String JobsInDelyedStartJobGroups = "jobJobsInDelyedStartJobGroups";
	public static final String JobsInIncreasedDurationJobGroups = "JobsInIncreasedDurationJobGroups";

}
