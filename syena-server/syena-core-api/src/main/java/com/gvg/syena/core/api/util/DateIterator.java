package com.gvg.syena.core.api.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class DateIterator implements Iterator<Date> {

	private final Date toTime;

	private Calendar calendar;

	private int intervalfield = Calendar.DAY_OF_YEAR;

	private int intervalamount = 1;

	private boolean first = true;

	public DateIterator(final Date fromTime, final Date toTime) {
		this.calendar = Calendar.getInstance();
		this.calendar.setTime(fromTime);
		this.toTime = toTime;
	}

	public DateIterator(Date fromTime, Date toTime, int intervalamount) {
		this(fromTime, toTime);
		this.intervalamount = intervalamount;
	}

	public void setIntervalfieldIntervalamount(int Intervalfield, int Intervalamount) {
		this.intervalfield = Intervalfield;
		this.intervalamount = Intervalamount;
	}

	@Override
	public boolean hasNext() {

		return calendar.getTime().before(toTime);
	}

	@Override
	public Date next() {
		if (first) {
			first = false;
		} else {
			calendar.add(intervalfield, intervalamount);
		}
		return calendar.getTime();
	}
}
