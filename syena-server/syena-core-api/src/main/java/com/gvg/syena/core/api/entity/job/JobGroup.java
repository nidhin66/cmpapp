package com.gvg.syena.core.api.entity.job;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gvg.syena.core.api.DependencyAttachableHierarchy;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.Cost;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.Item;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.OnlyForJPA;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.entity.util.MessageKeys;
import com.gvg.syena.core.api.exception.InvalidDataException;
import com.gvg.syena.core.api.util.ApplicationUtil;
import com.gvg.syena.core.api.util.Configuration;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class JobGroup<T extends StandardJob> extends JobItem implements DependencyAttachableHierarchy, Item<JobGroup>, Comparable<JobGroup> {

	private String name;

	private String description;

	private NodeType nodeType;

	@Embedded
	private WorkStatus workStatus;

	@Embedded
	private WorkTime workTime;

	@ManyToMany(targetEntity = StandardJob.class, cascade = CascadeType.ALL)
	private Set<T> jobs;

	@JsonIgnore
	@ManyToMany(targetEntity = JobGroup.class, cascade = CascadeType.ALL)
	private Set<JobGroup<T>> jobGroups;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<CheckList> checkLists;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private HierarchyNode connectedNode;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private DependencyJobs predecessorDependency;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private DependencyJobs successorDependency;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private JobGroup<Job> parent;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "numberofDays", column = @Column(name = "totalEffert_numberofDays")) })
	private PersonDays totalEffert;

	@ManyToOne(fetch = FetchType.LAZY)
	private Person owner;

	@ManyToOne(fetch = FetchType.LAZY)
	private Person initiator;

	private String executor;

	private boolean isInPunchList;

	@Embedded
	private WorkSchedule workSchedule;

	private boolean applicableForComplete = true;

	private double weightage = 1;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "amount", column = @Column(name = "estimatedCost_amount")),
			@AttributeOverride(name = "currency", column = @Column(name = "estimatedCost_currency")) })
	private Cost estimatedCost;

	public boolean isInPunchList() {
		return isInPunchList;
	}

	@OnlyForJPA
	public JobGroup() {

	}

	@OnlyForJPA
	public JobGroup(String name) {
		this.name = name;
		this.description = name;
		this.jobs = new TreeSet<T>();
		this.workStatus = new WorkStatus();
		this.checkLists = new TreeSet<CheckList>();
		this.jobGroups = new TreeSet<JobGroup<T>>();
		this.nodeType = NodeType.standard;
		this.workTime = new WorkTime();
		this.workSchedule = ApplicationUtil.getSystemDefaultWorkSchedule(getWorkTime());
		// this.isStandard = true;
	}

	@SuppressWarnings("unchecked")
	public JobGroup(JobGroup<StandardJob> jobGroup) {
		this(jobGroup.getName());
		Set<StandardJob> exiJobs = jobGroup.getJobs();
		if (exiJobs != null) {
			for (StandardJob job : exiJobs) {
				// TODO
				StandardJob newJob = new Job(job, job.getDescription());
				newJob.setParentJobGroup(this);
				this.jobs.add((T) newJob);
			}
		}
		Set<JobGroup<StandardJob>> newJobGroup = jobGroup.getJobGroups();
		if (newJobGroup != null) {
			for (JobGroup<StandardJob> jobg : newJobGroup) {
				this.jobGroups.add((JobGroup<T>) new JobGroup<StandardJob>(jobg));
			}
		}
		this.nodeType = NodeType.instance;
		// this.isStandard = true;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<T> getJobs() {
		return jobs;
	}

	@JsonProperty("jobs")
	public Set<T> getJobsJson() {
		if (Configuration.childEnable) {
			return jobs;
		} else {
			return null;
		}
	}

	public void setJobs(Set<T> jobs) {
		this.jobs = jobs;
	}

	public Set<JobGroup<T>> getJobGroups() {
		return jobGroups;
	}

	public void setJobGroups(Set<JobGroup<T>> jobGroups) {
		this.jobGroups = jobGroups;
	}

	public void addJob(T job) {
		job.setParentJobGroup(this);
		jobs.add(job);
	}

	public WorkTime getWorkTime() {
		return workTime;
	}

	public void setWorkTime(WorkTime workTime) {
		this.workTime = workTime;
		// if (workSchedule != null) {
		// workSchedule.getScheduledStages().first().setActionTime(workTime.getStartTime());
		// workSchedule.getScheduledStages().last().setActionTime(workTime.getFinishTime());
		// }
	}

	public void addJobs(List<T> jobs) {
		for (T t : jobs) {
			t.setParentJobGroup(this);
		}
		this.jobs.addAll(jobs);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void addToJobGroup(Collection<? extends JobGroup> newJobGroups) {
		jobGroups.addAll((Collection<? extends JobGroup<T>>) newJobGroups);
	}

	@JsonIgnore
	public Set<CheckList> getCheckList() {
		return this.checkLists;
	}

	public void setCheckLists(Set<CheckList> checkLists) {
		this.checkLists = checkLists;
	}

	public WorkStatus getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(WorkStatus workStatus) {
		this.workStatus = workStatus;
	}

	public void addCheckList(CheckList checkList) {
		this.checkLists.add(checkList);
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;

	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public Person getInitiator() {
		return initiator;
	}

	public void setInitiator(Person initiator) {
		this.initiator = initiator;
	}

	public String getExecutor() {
		return executor;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	public int compareTo(JobGroup o) {
		if (this.getName() == o.getName()) {
			return 0;
		} else {
			return 1;
		}

	}

	public void printAll() {
		System.out.println("Job Group: " + getName());
		if (jobs != null) {
			for (T job : jobs) {
				System.out.println("Job : " + job.getName());
			}
		}
		if (jobGroups != null) {
			for (JobGroup<T> jobGroup : jobGroups) {
				jobGroup.printAll();
			}
		}

	}

	public void addJobGroup(JobGroup<T> jobGroup) {
		jobGroups.add(jobGroup);

	}

	@Override
	@JsonIgnore
	public DependencyJobs getPredecessorDependency() {
		if (this.predecessorDependency == null) {
			this.predecessorDependency = new DependencyJobs();
		}
		return this.predecessorDependency;
	}

	@Override
	@JsonIgnore
	public DependencyJobs getSuccessorDependency() {
		if (this.successorDependency == null) {
			this.successorDependency = new DependencyJobs();
		}
		return this.successorDependency;
	}

	@JsonIgnore
	public HierarchyNode getConnectedNode() {
		return connectedNode;
	}

	public void setConnectedNode(HierarchyNode connectedNode) {
		this.connectedNode = connectedNode;
	}

	public JobGroup<Job> getParent() {
		return parent;
	}

	public void setParent(JobGroup<Job> parent) {
		this.parent = parent;
	}

	public void setPredecessorDependency(DependencyJobs predecessorDependency) {
		this.predecessorDependency = predecessorDependency;
	}

	public void setSuccessorDependency(DependencyJobs successorDependency) {
		this.successorDependency = successorDependency;
	}

	@Override
	public Set<? extends HierarchyItem> getChildren() {
		return getJobGroups();
	}

	@Override
	@JsonIgnore
	public boolean isEquual(JobGroup dependent) {
		return this.getId() == dependent.getId();
	}

	@Override
	@JsonIgnore
	public NodeCategory getNodeCategory() {
		return NodeCategory.jobgroup;
	}

	@Override
	@JsonIgnore
	public WorkStatus getStatus() {
		if (getNodeType() != NodeType.instance) {
			return null;
		}
		return getWorkStatus();
	}

	public PersonDays getTotalEffert() {
		if (totalEffert == null) {
			totalEffert = new PersonDays(1);
		}
		return totalEffert;
	}

	public void setTotalEffert(PersonDays totalEffert) {
		this.totalEffert = totalEffert;
	}

	public void setInPunchList(boolean b) {
		this.isInPunchList = b;

	}

	public void validateOnCreate() throws InvalidDataException {
		if (name == null || name.trim() == "") {
			throw new InvalidDataException(MessageKeys.INVALID_JOB_NAME, "Job name should not be null or empty");
		}
		if (description == null || description.trim() == "") {
			throw new InvalidDataException(MessageKeys.INVALID_DESCRIPTION, "Description should not be null or empty");
		}

	}

	@Override
	public boolean isStarted() {
		return false;
	}

	public void setWorkSchedule(WorkSchedule workSchedule) {
		this.workSchedule = workSchedule;
		// if (workTime != null && workSchedule != null) {
		// this.workTime.setStartTime(workSchedule.getScheduledStages().first().getActionTime());
		// this.workTime.setFinishTime(workSchedule.getScheduledStages().last().getActionTime());
		// }
	}

	public WorkSchedule getWorkSchedule() {
		return workSchedule;
	}

	@JsonIgnore
	public Duration getDuration() {
		return this.getWorkTime().getDuration();
	}

	@JsonIgnore
	public void setDuration(Duration duration) {
		this.getWorkTime().setDuration(duration);

	}

	public void setApplicableForComplete(boolean applicableForComplete) {
		this.applicableForComplete = applicableForComplete;
	}

	public boolean isApplicableForComplete() {
		return applicableForComplete;
	}

	public void setWeightage(double weightage) {
		this.weightage = weightage;
	}

	public double getWeightage() {
		return weightage;
	}

	public Cost getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(Cost estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

}

// @Embedded
// @AttributeOverrides({ @AttributeOverride(name = "startToStart", column =
// @Column(name = "preStartToStart")),
// @AttributeOverride(name = "startToFinish", column = @Column(name =
// "preStartToFinish")), @AttributeOverride(name = "finishToStart", column =
// @Column(name = "preFinishToStart")),
// @AttributeOverride(name = "finishToFinish", column = @Column(name =
// "preFinishToFinish")) })

// @Embedded
// @AttributeOverrides({ @AttributeOverride(name = "startToStart", column =
// @Column(name = "successorStartToStart")),
// @AttributeOverride(name = "startToFinish", column = @Column(name =
// "successorStartToFinish")),
// @AttributeOverride(name = "finishToStart", column = @Column(name =
// "successorFinishToStart")),
// @AttributeOverride(name = "finishToFinish", column = @Column(name =
// "successorFinishToFinish")) })
