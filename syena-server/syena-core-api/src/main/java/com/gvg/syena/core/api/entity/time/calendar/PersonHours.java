package com.gvg.syena.core.api.entity.time.calendar;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embeddable;

import com.gvg.syena.core.api.entity.time.Duration;
import com.gvg.syena.core.api.util.Configuration;

@Embeddable
@DiscriminatorValue(value = "H")
public class PersonHours extends Duration {

	private int hrs;

	private int min;

	private int sec;

	public PersonHours() {
		this(0, 0, 0);
	}

	public PersonHours(int hrs, int min, int sec) {
		super(Configuration.personDaysCPMCalendar);
		this.hrs = hrs;
		this.min = min;
		this.sec = sec;

	}

	public int getHrs() {
		return hrs;
	}

	public void setHrs(int hrs) {
		this.hrs = hrs;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getSec() {
		return sec;
	}

	public void setSec(int sec) {
		this.sec = sec;
	}

	@Override
	public Date calculateEndDate(Date date) {
		return getCalender().getDateAfter(date, hrs, min, min);

	}

	@Override
	public Date calculateStatDate(Date endTime) {
		return getCalender().getDateBefore(endTime, hrs, min, min);
	}

	@Override
	public void add(Duration duration) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLess(Date startTime, Date finishTime) {
		PersonHours personHrs = getCalender().getPersonHrs(startTime, finishTime);
		return this.isLess(personHrs);
	}

	private boolean isLess(PersonHours personHrs) {
		return false;
	}

	@Override
	public boolean beforeToday(Date date) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Duration copy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date setDayStartTime(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date setDayEndTime(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double divide(Duration duration) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Duration multiple(double percentageOfComplete) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Float getDurationValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void divide(float percentageOfWork) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNumberOfDays(int numberOfDays) {
		// TODO Auto-generated method stub

	}

}
