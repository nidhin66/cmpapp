package com.gvg.syena.core.api.services.serach;

public enum ValueComparitor {
	equal(new EqualValueComparitorProcessor()), lessthan(new LessthanValueComparitorProcessor()), lessthanOrEqual(new LessthanOrEqualValueComparitorProcessor()), greater(
			new GreaterValueComparitorProcessor()), greaterOrEqual(new GreaterOrEqualValueComparitorProcessor());

	private ValueComparitorProcessor processor;

	ValueComparitor(ValueComparitorProcessor processor) {
		this.processor = processor;
	}

	public boolean check(float num1, float num2) {
		return processor.check(num1, num2);
	}
}

interface ValueComparitorProcessor {

	boolean check(float num1, float num2);

}

class EqualValueComparitorProcessor implements ValueComparitorProcessor {

	@Override
	public boolean check(float num1, float num2) {
		return num1 == num2;
	}

}

class LessthanValueComparitorProcessor implements ValueComparitorProcessor {

	@Override
	public boolean check(float num1, float num2) {
		return num1 < num2;
	}

}

class LessthanOrEqualValueComparitorProcessor implements ValueComparitorProcessor {

	@Override
	public boolean check(float num1, float num2) {
		return num1 <= num2;
	}

}

class GreaterValueComparitorProcessor implements ValueComparitorProcessor {

	@Override
	public boolean check(float num1, float num2) {
		return num1 > num2;
	}

}

class GreaterOrEqualValueComparitorProcessor implements ValueComparitorProcessor {

	@Override
	public boolean check(float num1, float num2) {
		return num1 >= num2;
	}
}