/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import javafx.scene.control.CheckBox;

import org.controlsfx.control.spreadsheet.SpreadsheetCellBase;

import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;

/**
 *
 * @author hp
 */
public class CheckListItemCell  extends SpreadsheetCellBase{

    public CheckListItemCell(int row, int column, int rowSpan, int columnSpan,CheckListItem value) {
        super(row, column, rowSpan, columnSpan, new CheckListItemCellType());
        this.setItem(value);          
        CheckBox checkBox = new CheckBox();
        checkBox.setText(value.getName());
        checkBox.getStyleClass().add("checklist-item-checkbox");
        this.setGraphic(checkBox);
        if(null != value){
             this.setTooltip(value.getName());
              checkBox.setText(value.getName());
             checkBox.setSelected(value.isCompleted());
        }
        this.getStyleClass().add("checklist-item-cell");
    }
    
}
