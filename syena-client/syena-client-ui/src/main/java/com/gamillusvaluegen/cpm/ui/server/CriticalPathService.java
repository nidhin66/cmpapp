/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.CriticalPathServiceNames;
import com.gvg.syena.core.services.criticalpath.CriticalAndNonCriticalPath;
import com.gvg.syena.core.services.criticalpath.JobData;

/**
 *
 * @author hp
 */
public class CriticalPathService {

    public static final String url = ScreensConfiguration.getUrl();

    private static RestCaller createRestCallser() {
        RestCaller criticalPathController = new RestCaller(url);
        criticalPathController.path("criticalPath");
        return criticalPathController;
    }

    public static List<JobData> findCriticalPath(long hierarchyNodeId) throws ServiceFailedException {
        RestCaller criticalPathController = createRestCallser();
        criticalPathController.path(CriticalPathServiceNames.findCriticalPath);
        criticalPathController.param("hierarchyNodeId", hierarchyNodeId);
        List<JobData> jobDataList = null;
        try {
            TypeReference<List<JobData>> type = new TypeReference<List<JobData>>() {
            };
            jobDataList = criticalPathController.post(null, type);

        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CRITICALPATH_SERVICE_001, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobDataList;
    }

    public static List<JobData> findNonCriticalPath(long hierarchyNodeId) throws ServiceFailedException {

        RestCaller criticalPathController = createRestCallser();
        criticalPathController.path(CriticalPathServiceNames.findNonCriticalPath);
        criticalPathController.param("hierarchyNodeId", hierarchyNodeId);

        List<JobData> jobDataList = null;

        try {
            TypeReference<List<JobData>> type = new TypeReference<List<JobData>>() {
            };
            jobDataList = criticalPathController.post(null, type);

        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CRITICALPATH_SERVICE_002, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobDataList;
    }

    public static CriticalAndNonCriticalPath findCriticalAndNonCriticalPath(long hierarchyNodeId) throws ServiceFailedException {
        RestCaller criticalPathController = createRestCallser();
        criticalPathController.path(CriticalPathServiceNames.findCriticalAndNonCriticalPath);
        criticalPathController.param("hierarchyNodeId", hierarchyNodeId);
        CriticalAndNonCriticalPath criticalAndNonCriticalPath = null;
        try {
            criticalAndNonCriticalPath = criticalPathController.post(CriticalAndNonCriticalPath.class);
        } catch (RestCallException ex) {
           ex.printStackTrace();
            Message erroMessage = new Message(ExceptionMessages.CRITICALPATH_SERVICE_002, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return criticalAndNonCriticalPath;
    }

}
