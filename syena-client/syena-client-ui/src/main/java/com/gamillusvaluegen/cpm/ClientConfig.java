/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.animation.FadeTransition;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import org.controlsfx.control.BreadCrumbBar;

import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.PlantHierarchyPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.dependency.DependencyPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.statusview.StatusViewPane;
import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;

/**
 *
 * @author hp
 */
public class ClientConfig {

	private HierarchyNode plant = null;

	private Map<String, HierarchyItemPanel> hierarchyPanels = new HashMap<>();

	private HierarchyJobPanel hierarchyJobPanel = null;

	private DependencyPane dependencyPane = null;

	private HierarchyLevel hierarchyLevel = null;

	private static ClientConfig me = new ClientConfig();

	private Map<String, Parent> screens = new HashMap<String, Parent>();

	private BreadCrumbBar<String> breadCrumbBar;

	private String currentScreenId;

	private Map<String, String> nodeIcons = new HashMap<>();

	private Person loginUser = null;

	private Map<String, String> applicationConfigs = null;

	private ClientConfig() {
		nodeIcons.put("Unit", "unit-panel-label");
		nodeIcons.put("System", "system-panel-label");
		nodeIcons.put("Sub system", "subsystem-panel-label");
		nodeIcons.put("Loop/Equipments", "loop-panel-label");
		nodeIcons.put("Job", "job-panel-label");
	}

	public static ClientConfig getInstance() {
		return me;
	}

	public HierarchyNode getPlant() {
		return plant;
	}

	public void setPlant(HierarchyNode plant) {
		this.plant = plant;
	}

	public HierarchyItemPanel getHierarchyItemPanel(String levelName) {
		return hierarchyPanels.get(levelName);
	}

	public void setHierarchyItemPanel(String levelName, HierarchyItemPanel hierarchyItemPanel) {
		hierarchyPanels.put(levelName, hierarchyItemPanel);
	}
	
	public List<HierarchyItemPanel> getAllHierarchyPanels(){
		List<HierarchyItemPanel> panels = new ArrayList<>();
		for(Iterator<HierarchyItemPanel> it = hierarchyPanels.values().iterator(); it.hasNext();){
			HierarchyItemPanel panel = it.next();
			panels.add(panel);
		}
		return panels;
	}

	public HierarchyJobPanel getHierarchyJobPanel() {
		return hierarchyJobPanel;
	}

	public void setHierarchyJobPanel(HierarchyJobPanel hierarchyJobPanel) {
		this.hierarchyJobPanel = hierarchyJobPanel;
	}

	public HierarchyLevel getHierarchyLevel() {
		return hierarchyLevel;
	}

	public void setHierarchyLevel(HierarchyLevel hierarchyLevel) {
		this.hierarchyLevel = hierarchyLevel;
	}

	public void setBreadCrumbBar(BreadCrumbBar<String> breadCrumbBar) {
		this.breadCrumbBar = breadCrumbBar;
	}

	public BreadCrumbBar<String> getBreadCrumbBar() {
		return breadCrumbBar;
	}

	public DependencyPane getDependencyPane() {
		return dependencyPane;
	}

	public void setDependencyPane(DependencyPane dependencyPane) {
		this.dependencyPane = dependencyPane;
	}

	public Parent getScreenById(String painId) {
		Parent screen = screens.get(painId);
		if (null == screen) {
			screen = createScreen(painId);
			if (!CPMClientConstants.MENU_ID_UNAPPROVED_DATA.equalsIgnoreCase(painId)) {
				screens.put(painId, screen);
			}
		}
		currentScreenId = painId;
		setTransition(screen);
		return screen;
	}

	private void setTransition(Parent screen) {
		FadeTransition ft = new FadeTransition(Duration.millis(3000), screen);
		// // ft.setFromValue(0.0);
		// // ft.setToValue(1.0);
		// // ft.play();
		//
		ft.setFromValue(0.0);
		ft.setToValue(1.0);
		ft.setCycleCount(1);
		ft.setAutoReverse(true);
		ft.play();

	}

	private Parent createScreen(String id) {
		if (CPMClientConstants.MENU_ID_PLANT_HIERARCHY.equalsIgnoreCase(id)) {
			PlantHierarchyPane plantHierarchyPane = new PlantHierarchyPane(true);
			// plantHierarchyPane.loadPlantHierarchy();
			return plantHierarchyPane;
		} else if (CPMClientConstants.MENU_ID_STATUS_VIEW.equalsIgnoreCase(id)) {
			StatusViewPane statusViewPane = new StatusViewPane();
			return statusViewPane;
		} else if (CPMClientConstants.MENU_ID_UNAPPROVED_DATA.equalsIgnoreCase(id)) {
			PlantHierarchyPane plantHierarchyPane = new PlantHierarchyPane(false);
			plantHierarchyPane.loadUnApprovdedData();
			return plantHierarchyPane;
		}
		return null;
	}

	public String getCurrentScreenId() {
		return currentScreenId;
	}

	public String getLevelNodeIconStyle(String levelName) {
		String styleClass = nodeIcons.get(levelName);
		return styleClass;
	}

	public Person getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(Person loginUser) {
		this.loginUser = loginUser;
	}

	public static Rectangle getStatusGraphics(WorkStatusType statusType) {
		Rectangle rect = new Rectangle(0, 0, 10, 10);
		rect.setTranslateX(1);
		rect.setTranslateY(1);

		rect.setFill(getStatusColor(statusType));

		return rect;
	}

	private static Paint getStatusColor(WorkStatusType statusType) {
		Paint paint = null;
		if (statusType == WorkStatusType.Completed_on_schedule) {
			paint = Color.web("#8bed6a");
		} else if (statusType == WorkStatusType.Completed_with_delays) {
			paint = Color.web("#ffaf35");
		} else if (statusType == WorkStatusType.Not_scheduled) {
			paint = Color.web("#51cdd1");
		} else if (statusType == WorkStatusType.Ongoing_delayed) {
			paint = Color.web("#f97961");
		} else if (statusType == WorkStatusType.Ongoing_on_track) {
			paint = Color.web("#ffee72");
		} else if (statusType == WorkStatusType.Not_Stared) {
			paint = Color.web("#a8601b");
		}
		return paint;
	}

	public String getApplicationConfig(String property) {
		return applicationConfigs.get(property);
	}

	public void setApplicationConfigs(List<ApplicationConfiguration> applicationConfigs) {
		this.applicationConfigs = new HashMap<>();
		if (null != applicationConfigs) {
			for (ApplicationConfiguration applicationConfiguration : applicationConfigs) {
				this.applicationConfigs.put(applicationConfiguration.getParamKey(), applicationConfiguration.getParamValue());
			}
		}

	}

}
