package com.gamillusvaluegen.cpm.ui.diary;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.ClientMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.HierarchySearchController;
import com.gamillusvaluegen.cpm.ui.custom.SearchTextField;
import com.gamillusvaluegen.cpm.ui.server.DiaryClientService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.services.serach.JobProperties;

public class NewThreadPane extends BorderPane implements DialogController,HierarchySearchController{
	
	private FXMLDialog dialog;
	
	@FXML
	private ToggleButton unitButton;
	
	@FXML
	private ToggleButton systemButton;
	
	@FXML
	private ToggleButton subSystemButton;
	
	@FXML
	private ToggleButton loopButton;
	
	@FXML
	private ToggleButton jobButton;
	
	@FXML
	private SearchTextField searchBox;
	
	@FXML
	private TextField topicField;
	
	@FXML
	private TextArea contentField;
	
	private ToggleGroup searchBtnGroup;
	
	private NodeCategory category;
	
	private DiaryPane diaryPane;
	
	public NewThreadPane(DiaryPane diaryPane){
		this.diaryPane = diaryPane;
		searchBtnGroup = new ToggleGroup();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/new_diary_thread_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		    unitButton.setToggleGroup(searchBtnGroup);
	        unitButton.setUserData("Unit");
	        unitButton.setTooltip(new Tooltip("Unit"));
	       

	        systemButton.setToggleGroup(searchBtnGroup);
	        systemButton.setUserData("System");
	        systemButton.setTooltip(new Tooltip("System"));

	        subSystemButton.setToggleGroup(searchBtnGroup);
	        subSystemButton.setUserData("Sub system");
	        subSystemButton.setTooltip(new Tooltip("Sub system"));

	        loopButton.setToggleGroup(searchBtnGroup);
	        loopButton.setUserData("Loop/Equipments");
	        loopButton.setTooltip(new Tooltip("Loop/Equipments"));

	        jobButton.setToggleGroup(searchBtnGroup);
	        jobButton.setUserData("Job");
	        jobButton.setTooltip(new Tooltip("Job"));
	        
	        searchBtnGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
	            public void changed(ObservableValue<? extends Toggle> ov,
	                    Toggle toggle, Toggle new_toggle) {
	                if (null != new_toggle) {
	                	searchBox.clear();
	                    searchBox.setPromptText("Search " + new_toggle.getUserData());
	                    if("Job".equalsIgnoreCase((String) new_toggle.getUserData())){
	                    	category = NodeCategory.job;
	                    }else {
	                    	category = NodeCategory.hierarchy;
	                    }
	                }
	            }
	        });
	        searchBox.setController(this);
	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;		
	}
	
	@FXML
	private void onCancel(){
		this.dialog.close();
	}
	
	@FXML
	private void onSubmit(){
		if(null == searchBox.getHierarchyItem()){
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, ClientMessages.INVALID_NODE_DAIRY);
			dialog.show();
		}else if(Utilities.isNullOrEmpty(topicField.getText())){
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, ClientMessages.INVALID_TOPIC_DAIRY);
			dialog.show();
		}else{
			DiaryThread diaryThread = new DiaryThread();
			diaryThread.setCategory(category);
			diaryThread.setCreatedBy(ClientConfig.getInstance().getLoginUser().getId());
			diaryThread.setCreatedDate(Utilities.getDate());
			diaryThread.setHierarchyItemId(searchBox.getHierarchyItem().getId());
			diaryThread.setLastUpdatedDate(Utilities.getDate());
			diaryThread.setTopic(topicField.getText());
			diaryThread.setHierarchyName(searchBox.getHierarchyItem().getName());
			try {
				diaryThread = DiaryClientService.createNewThread(diaryThread);
				ThreadDiscussion discussion = new ThreadDiscussion();
				discussion.setComment(contentField.getText());
				discussion.setDate(Utilities.getDate());
				discussion.setUserId(ClientConfig.getInstance().getLoginUser().getId());
				DiaryClientService.addDiscussion(diaryThread.getId(), discussion);
				onCancel();
				diaryPane.onClearFilter();
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	 @Override
	    public List<? extends HierarchyItem> searchData(String text) {
	        List<? extends HierarchyItem> hierarchyNodes = new ArrayList<>();
	        Toggle selectedToggle = searchBtnGroup.getSelectedToggle();
	        if (null != selectedToggle) {
	            try {
	                String level = (String) selectedToggle.getUserData();
	                PageItr<? extends HierarchyItem> hierarchyNodesPage = null;
	                if("Job".equalsIgnoreCase(level)){
	                	JobProperties jobSearchOption = new JobProperties();	
	          			jobSearchOption.setStandard(false);
	          			jobSearchOption.setName(text+"%");
	          			try {
	          				hierarchyNodesPage = HierarchyJobService.searchJobWithProperties(jobSearchOption, 0, 25);
	          			} catch (ServiceFailedException e) {
	          				// TODO Auto-generated catch block
	          				e.printStackTrace();
	          			}
	                }else{
	                	hierarchyNodesPage = HierarchyNodeService.searchHierarchyNodesInLevel(text + "%", level, 0, 25);
	                }              
	    			
	    			
	                if (null != hierarchyNodesPage) {
	                    hierarchyNodes = hierarchyNodesPage.getContent();
	                }
	            } catch (ServiceFailedException ex) {
	                ex.printStackTrace();
	            }
	        }
	        return hierarchyNodes;
	    }

}
