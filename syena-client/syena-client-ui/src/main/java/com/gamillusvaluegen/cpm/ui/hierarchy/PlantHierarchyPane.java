/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.scene.layout.BorderPane;

/**
 *
 * @author hp
 */
public class PlantHierarchyPane extends BorderPane {
	
	private PlantHierarchyDetailsPane plantHierarchyDetailsPane;

    public PlantHierarchyPane(boolean approved) {

        plantHierarchyDetailsPane = new PlantHierarchyDetailsPane(approved);

//      this.setTop(new PlantHierarchySearchPane());
        this.setCenter(plantHierarchyDetailsPane);
//        this.setPrefWidth(1400);

    }
    
    public void loadPlantHierarchy(){
    	plantHierarchyDetailsPane.loadPlantHierarchy();
    }
    
    public void loadUnApprovdedData(){
    	plantHierarchyDetailsPane.loadUnApprovedData();
    }

}
