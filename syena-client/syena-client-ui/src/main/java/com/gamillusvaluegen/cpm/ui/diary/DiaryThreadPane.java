package com.gamillusvaluegen.cpm.ui.diary;

import javafx.scene.control.TitledPane;

import com.gvg.syena.core.api.entity.diary.DiaryThread;

public class DiaryThreadPane extends TitledPane{
	
	private DiaryThread diaryThread;
	
	public DiaryThreadPane(DiaryThread diaryThread){
		this.diaryThread = diaryThread;
		DiaryThreadHeader header = new DiaryThreadHeader(diaryThread);
		this.setGraphic(header);
		setAnimated(true);
	}
	
	public void loadContent(){
		setContent(new ThreadDiscussionList(diaryThread));
	}

}
