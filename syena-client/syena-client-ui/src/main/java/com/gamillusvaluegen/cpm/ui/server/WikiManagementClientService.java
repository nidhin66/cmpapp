package com.gamillusvaluegen.cpm.ui.server;

import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.entity.wiki.HierarchyItemWiki;
import com.gvg.syena.core.api.services.names.WikiManagementServiceNames;

public class WikiManagementClientService { 
	

    public static final String url = ScreensConfiguration.getUrl();

    private static RestCaller createRestCallser() {
        RestCaller wikiManagementController = new RestCaller(url);
        wikiManagementController.path("wiki");
        return wikiManagementController;
    }
    
    public static void createOrUpdateWiki(HierarchyItemWiki wiki) throws ServiceFailedException {
    	 RestCaller wikiManagementController = createRestCallser();
    	 wikiManagementController.path(WikiManagementServiceNames.createOrUpdateWiki);       
         try {
        	 wikiManagementController.post(wiki,HierarchyItemWiki.class);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
    }
    
    
    public static HierarchyItemWiki getHierarchyWiki(long hierarchyNodeId ) throws ServiceFailedException{
    	 RestCaller wikiManagementController = createRestCallser();
    	 wikiManagementController.path(WikiManagementServiceNames.getProjectWiki).param("hierarchyNodeId", hierarchyNodeId);       
    	 HierarchyItemWiki wiki = null;
         try {
        	wiki = wikiManagementController.get(HierarchyItemWiki.class);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
         return wiki;
    }


}
