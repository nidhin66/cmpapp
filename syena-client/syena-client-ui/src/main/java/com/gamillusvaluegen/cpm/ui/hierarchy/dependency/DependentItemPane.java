/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.dependency;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.DependencySearchField;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.server.DependencyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public abstract class DependentItemPane extends BorderPane {

	@FXML
	protected TableView<HierarchyItem> predecessorJobsTable;

	@FXML
	protected TableView<HierarchyItem> successorJobsTable;

	@FXML
	protected TableColumn<HierarchyItem, String> predecessorNameColumn;

	@FXML
	protected TableColumn<HierarchyItem, HierarchyItem> predecessorStatusColumn;

	@FXML
	protected TableColumn<HierarchyItem, String> successorNameColumn;

	@FXML
	protected TableColumn<HierarchyItem, HierarchyItem> successorStatusColumn;

	@FXML
	protected ToggleButton dependentButton;

	@FXML
	protected ToggleButton allItemsButton;

	@FXML
	protected Label predPaginationLabel;

	@FXML
	protected Label succPaginationLabel;
	
	@FXML
	protected DependencySearchField successorSearch;
	
	@FXML
	protected DependencySearchField predecessorSearch;

	protected DependentJobContextMenu predecessorDependentJobContextMenu;

	protected DependentJobContextMenu successorDependentJobContextMenu;

	protected int contextID;

	protected static final int CONTEXT_DEPENDENT = 0;
	protected static final int CONTEXT_ALL = 1;

	protected static final int pageSize = 10;

	protected ToggleGroup group;

	protected HierarchyLevel level;

	protected PageItr<? extends HierarchyItem> currentPredecessorPage = null;

	protected PageItr<? extends HierarchyItem> currentSuccessorPage = null;

	@FXML
	private void initialize() {

		contextID = CONTEXT_DEPENDENT;
		group = new ToggleGroup();

		dependentButton.setToggleGroup(group);
		allItemsButton.setToggleGroup(group);

		dependentButton.setTooltip(new Tooltip("Show Dependent"));
		allItemsButton.setTooltip(new Tooltip("Show All Items"));

		dependentButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				loadDependentData();
			}
		});

		allItemsButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				clearTables();
				contextID = CONTEXT_ALL;				
				loadPredecessorFirstPage();
				loadSuccessorFirstPage();
			}
		});

	
		predecessorJobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		successorJobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		predecessorJobsTable.getStyleClass().add("dependent-item-table");
		successorJobsTable.getStyleClass().add("dependent-item-table");
		predecessorNameColumn.setCellValueFactory(new PropertyValueFactory<HierarchyItem, String>("description"));
		successorNameColumn.setCellValueFactory(new PropertyValueFactory<HierarchyItem, String>("description"));

		predecessorNameColumn
				.setCellFactory(new Callback<TableColumn<HierarchyItem, String>, TableCell<HierarchyItem, String>>() {
					@Override
					public TableCell<HierarchyItem, String> call(TableColumn<HierarchyItem, String> param) {
						TableCell<HierarchyItem, String> cell = new TableCell<HierarchyItem, String>() {
							@Override
							public void updateItem(final String item, boolean empty) {
								super.updateItem(item, empty);
								if (null != getTableView().getItems() && getTableView().getItems().size() > 0) {
									if (getIndex() >= 0 && getIndex() < getTableView().getItems().size()) {
										HierarchyItem hierarchyItem = getTableView().getItems().get(getIndex());
										if (null != hierarchyItem) {
											if (Utilities.isNullOrEmpty(hierarchyItem.getDescription())) {
												setText(hierarchyItem.getName());
											} else {
												setText(hierarchyItem.getDescription());
											}
										} else {

											setText(null);
											setGraphic(null);
										}
									} else {

										setText(null);
										setGraphic(null);
									}
								} else {

									setText(null);
									setGraphic(null);
								}

							}
						};
						// cell.getStyleClass().add("dependent-item-cell");
						return cell;
					}
				});

		successorNameColumn
				.setCellFactory(new Callback<TableColumn<HierarchyItem, String>, TableCell<HierarchyItem, String>>() {
					@Override
					public TableCell<HierarchyItem, String> call(TableColumn<HierarchyItem, String> param) {
						TableCell<HierarchyItem, String> cell = new TableCell<HierarchyItem, String>() {
							@Override
							public void updateItem(final String item, boolean empty) {
								super.updateItem(item, empty);
								if (null != getTableView().getItems() && getTableView().getItems().size() > 0) {
									if (getIndex() >= 0 && getIndex() < getTableView().getItems().size()) {
										HierarchyItem hierarchyItem = getTableView().getItems().get(getIndex());
										if (null != hierarchyItem) {
											if (Utilities.isNullOrEmpty(hierarchyItem.getDescription())) {
												setText(hierarchyItem.getName());
											} else {
												setText(hierarchyItem.getDescription());
											}
										} else {

											setText(null);
											setGraphic(null);
										}
									} else {

										setText(null);
										setGraphic(null);
									}
								} else {

									setText(null);
									setGraphic(null);
								}
							}
						};
						// cell.getStyleClass().add("dependent-item-cell");
						return cell;
					}
				});

		predecessorStatusColumn.setCellFactory(
				new Callback<TableColumn<HierarchyItem, HierarchyItem>, TableCell<HierarchyItem, HierarchyItem>>() {
					@Override
					public TableCell<HierarchyItem, HierarchyItem> call(
							TableColumn<HierarchyItem, HierarchyItem> param) {
						TableCell<HierarchyItem, HierarchyItem> cell = new TableCell<HierarchyItem, HierarchyItem>() {
							@Override
							public void updateItem(final HierarchyItem item, boolean empty) {
								super.updateItem(item, empty);
								if (null != getTableView().getItems() && getTableView().getItems().size() > 0) {
									if (getIndex() >= 0 && getIndex() < getTableView().getItems().size()) {
										HierarchyItem hierarchyItem = getTableView().getItems().get(getIndex());
										if (null != hierarchyItem) {
											if (null != hierarchyItem.getStatus()) {

												WorkStatusType statusType = WorkStatusType
														.getWorkStatusType(hierarchyItem.getStatus());

												Rectangle rect = ClientConfig.getStatusGraphics(statusType);
												setGraphic(rect);
											}else{
												setText(null);
												setGraphic(null);
											}
										} else {

											setText(null);
											setGraphic(null);
										}
									} else {

										setText(null);
										setGraphic(null);
									}
								} else {

									setText(null);
									setGraphic(null);
								}
								
							}
						};
						// cell.getStyleClass().add("dependent-item-cell");
						return cell;
					}
				});

		successorStatusColumn.setCellFactory(
				new Callback<TableColumn<HierarchyItem, HierarchyItem>, TableCell<HierarchyItem, HierarchyItem>>() {
					@Override
					public TableCell<HierarchyItem, HierarchyItem> call(
							TableColumn<HierarchyItem, HierarchyItem> param) {
						TableCell<HierarchyItem, HierarchyItem> cell = new TableCell<HierarchyItem, HierarchyItem>() {
							@Override
							public void updateItem(final HierarchyItem item, boolean empty) {
								super.updateItem(item, empty);
								if (null != getTableView().getItems() && getTableView().getItems().size() > 0) {
									if (getIndex() >= 0 && getIndex() < getTableView().getItems().size()) {
										HierarchyItem hierarchyItem = getTableView().getItems().get(getIndex());
										if (null != hierarchyItem) {
											if (null != hierarchyItem.getStatus()) {

												WorkStatusType statusType = WorkStatusType
														.getWorkStatusType(hierarchyItem.getStatus());

												Rectangle rect = ClientConfig.getStatusGraphics(statusType);
												setGraphic(rect);
											}else{
												setText(null);
												setGraphic(null);
											}
										} else {

											setText(null);
											setGraphic(null);
										}
									} else {

										setText(null);
										setGraphic(null);
									}
								} else {

									setText(null);
									setGraphic(null);
								}
							}
						};
						// cell.getStyleClass().add("dependent-item-cell");
						return cell;
					}
				});
		
		predecessorSearch.setDepndencyItemPane("P",this);
		successorSearch.setDepndencyItemPane("S",this);

	}

	public void loadDependentData() {
		clearTables();
		contextID = CONTEXT_DEPENDENT;		
		group.selectToggle(dependentButton);
		loadPredecessorFirstPage();
		loadSuccessorFirstPage();
	}

	public abstract void addAsSuccessor();

	public abstract void addAsPredecessor();

	public abstract void removePredecessor();

	public abstract void removeSuccessor();

	public List<? extends HierarchyItem> getSelectedPredecessorItems() {
		ObservableList<? extends HierarchyItem> selectedItems = predecessorJobsTable.getSelectionModel()
				.getSelectedItems();
		return selectedItems;
	}

	public List<? extends HierarchyItem> getSelectedSuccessorItems() {
		ObservableList<? extends HierarchyItem> selectedItems = successorJobsTable.getSelectionModel()
				.getSelectedItems();
		return selectedItems;
	}

	public int getContextID() {
		return contextID;
	}

	public void showAllItems() {
		clearTables();
		getAllItems(0);
	}

	public void clearTables() {
		if (null != predecessorJobsTable.getItems()) {
			predecessorJobsTable.getItems().clear();
		}
		if (null != successorJobsTable.getItems()) {
			successorJobsTable.getItems().clear();
		}
	}

	public abstract List<? extends HierarchyItem> searchData(String context,String searchText);

	@FXML
	protected void loadPredecessorFirstPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (contextID == CONTEXT_ALL) {
			pageHierarchyNodes = getAllItems(0);			
		} else {
			pageHierarchyNodes = getPredecessorItems(0);			
		}
		loadPredecessorTable(pageHierarchyNodes);
	}

	@FXML
	protected void loadPredecessorPreviousPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentPredecessorPage) {
			if ((currentPredecessorPage.getCurrentPage()) > 0) {
				int pageNumber = currentPredecessorPage.getCurrentPage() - 1;
				if (contextID == CONTEXT_ALL) {
					pageHierarchyNodes = getAllItems(pageNumber);			
				} else {
					pageHierarchyNodes = getPredecessorItems(pageNumber);					
				}
				loadPredecessorTable(pageHierarchyNodes);
			}
		}		
		
	}

	@FXML
	protected void loadPredecessorNextPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentPredecessorPage) {
			if ((currentPredecessorPage.getCurrentPage() + 1) < currentPredecessorPage.getTotalPages()) {
				int pageNumber = currentPredecessorPage.getCurrentPage() + 1;
				if (contextID == CONTEXT_ALL) {
					pageHierarchyNodes = getAllItems(pageNumber);			
				} else {
					pageHierarchyNodes = getPredecessorItems(pageNumber);					
				}
				loadPredecessorTable(pageHierarchyNodes);
			}
		}		
	}

	@FXML
	protected void loadPredecessorLastPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentPredecessorPage) {			
			int pageNumber = currentPredecessorPage.getTotalPages() - 1;
				if (contextID == CONTEXT_ALL) {
					pageHierarchyNodes = getAllItems(pageNumber);			
				} else {
					pageHierarchyNodes = getPredecessorItems(pageNumber);					
				}
				loadPredecessorTable(pageHierarchyNodes);
			
		}		
	}

	@FXML
	protected void loadSuccessorFirstPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (contextID == CONTEXT_ALL) {
			pageHierarchyNodes = getAllItems(0);			
		} else {
			pageHierarchyNodes = getSuccessorItems(0);			
		}
		loadSuccessorTable(pageHierarchyNodes);
	}

	@FXML
	protected void loadSuccessorPreviousPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentSuccessorPage) {
			if ((currentSuccessorPage.getCurrentPage()) > 0) {
				int pageNumber = currentSuccessorPage.getCurrentPage() - 1;
				if (contextID == CONTEXT_ALL) {
					pageHierarchyNodes = getAllItems(pageNumber);			
				} else {
					pageHierarchyNodes = getSuccessorItems(pageNumber);					
				}
				loadSuccessorTable(pageHierarchyNodes);
			}
		}		
		
	}

	@FXML
	protected void loadSuccessorNextPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentSuccessorPage) {
			if ((currentSuccessorPage.getCurrentPage() + 1) < currentSuccessorPage.getTotalPages()) {
				int pageNumber = currentSuccessorPage.getCurrentPage() + 1;
				if (contextID == CONTEXT_ALL) {
					pageHierarchyNodes = getAllItems(pageNumber);			
				} else {
					pageHierarchyNodes = getSuccessorItems(pageNumber);					
				}
				loadSuccessorTable(pageHierarchyNodes);
			}
		}		
	}

	@FXML
	protected void loadSuccessorLastPage(){
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentSuccessorPage) {			
			int pageNumber = currentSuccessorPage.getTotalPages() - 1;
				if (contextID == CONTEXT_ALL) {
					pageHierarchyNodes = getAllItems(pageNumber);			
				} else {
					pageHierarchyNodes = getSuccessorItems(pageNumber);					
				}
				loadSuccessorTable(pageHierarchyNodes);
			
		}		
	}

	private PageItr<? extends HierarchyItem> getPredecessorItems(int pageNumber) {
		HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
		HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
		PageItr<? extends HierarchyItem> predecessorNodesPage = null;
		if (null != hierarchyItem) {
			try {
				predecessorNodesPage = DependencyJobService.getPredecessorHierarchyNodeDependency(hierarchyItem.getId(), pageNumber, pageSize);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return predecessorNodesPage;
	}

	public void loadPredecessorTable(PageItr<? extends HierarchyItem> pageHierarchyNodes) {
		if (null != predecessorJobsTable.getItems()) {
			predecessorJobsTable.getItems().clear();
		}
		currentPredecessorPage = pageHierarchyNodes;
		if(null != pageHierarchyNodes){
			ObservableList<HierarchyItem> predecessorItems = FXCollections.observableArrayList(pageHierarchyNodes.getContent());
			predecessorJobsTable.setItems(predecessorItems);
			if(pageHierarchyNodes.getTotalPages() > 0){
				predPaginationLabel.setText((pageHierarchyNodes.getCurrentPage() +1) + " of " + pageHierarchyNodes.getTotalPages());
			}
		
		}
		
	}

	private PageItr<? extends HierarchyItem> getSuccessorItems(int pageNumber) {
		HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
		HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
		PageItr<? extends HierarchyItem> successorNodesPages = null;
		if (null != hierarchyItem) {

			try {
				successorNodesPages = DependencyJobService.getSuccessorHierarchyNodeDependency(hierarchyItem.getId(),pageNumber,pageSize);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return successorNodesPages;
	}

	public void loadSuccessorTable(PageItr<? extends HierarchyItem> pageHierarchyNodes) {
		if (null != successorJobsTable.getItems()) {
			successorJobsTable.getItems().clear();
		}
		currentSuccessorPage = pageHierarchyNodes;
		if(null != pageHierarchyNodes){
			ObservableList<HierarchyItem> successorItems = FXCollections.observableArrayList(pageHierarchyNodes.getContent());
			successorJobsTable.setItems(successorItems);
			if(pageHierarchyNodes.getTotalPages() > 0){
				succPaginationLabel.setText( (pageHierarchyNodes.getCurrentPage() +1) + " of " + pageHierarchyNodes.getTotalPages());
			}			
		}
		
	}

	

	private PageItr<? extends HierarchyItem> getAllItems(int pageNumber) {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;		
		try {
			pageHierarchyNodes = HierarchyNodeService.getAllHierarchyNodes(level.getLevelName(), pageNumber, pageSize);
//			items = FXCollections.observableArrayList(pageHierarchyNodes.getContent());
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		return pageHierarchyNodes;
	}

}
