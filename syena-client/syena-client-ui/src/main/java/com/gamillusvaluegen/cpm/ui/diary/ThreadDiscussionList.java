package com.gamillusvaluegen.cpm.ui.diary;

import java.io.IOException;
import java.util.Iterator;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.DiaryClientService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.entity.util.Message;

public class ThreadDiscussionList extends BorderPane{
	
	@FXML
	private VBox discussionList;
	
	@FXML
	private Button moreButton;
	
	@FXML
	private Button replyButton;
	
	private DiaryThread diaryThread;
	
	PageItr<ThreadDiscussion> currentPage; 
	
	
	public ThreadDiscussionList(DiaryThread diaryThread){
		this.diaryThread = diaryThread;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/thread_discussion_list.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		moreButton.setVisible(true);
		replyButton.setVisible(false);
		loadData(0);
	}
	
	private void loadData(int pageNumber){
		try {
			PageItr<ThreadDiscussion> threadDiscussionPage =  DiaryClientService.getDiscussions(diaryThread.getId(), pageNumber, 5);
			if(null != threadDiscussionPage){
				if(null != threadDiscussionPage.getContent()){
					for(ThreadDiscussion threadDiscussion : threadDiscussionPage.getContent()){
						ThreadDiscussionPane discussionPane = new ThreadDiscussionPane(threadDiscussion);
						discussionList.getChildren().add(discussionPane);
					}
				}
				currentPage = threadDiscussionPage;
				if(currentPage.getTotalPages() == currentPage.getCurrentPage() + 1){
					moreButton.setVisible(false);
					replyButton.setVisible(true);
				}
			}
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@FXML
	private void onReply(){
		ThreadDiscussion threadDiscussion = new ThreadDiscussion();
		threadDiscussion.setDate(Utilities.getDate());
		threadDiscussion.setUserId(ClientConfig.getInstance().getLoginUser().getId());
		ThreadDiscussionPane discussionPane = new ThreadDiscussionPane(threadDiscussion);
		discussionPane.setEditable(true);
		discussionPane.setParentPane(this);
		discussionList.getChildren().add(discussionPane);
		replyButton.setVisible(false);
	}
	
	@FXML
	private void onMore(){
		if(null != currentPage){
			loadData(currentPage.getCurrentPage() + 1);
		}
		
	}
	
	public void submitDiscussion(ThreadDiscussion discussion){
		try {
			DiaryClientService.addDiscussion(diaryThread.getId(), discussion);	
			replyButton.setVisible(true);
		} catch (ServiceFailedException e) {
			Message message = e.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, message.getMessage());
			alertDialog.show();
		}
	}
	
	public void cancelReply(){
		Node removeNode = null;
		for(Iterator<Node> it = discussionList.getChildren().iterator(); it.hasNext();){
			ThreadDiscussionPane discussionPane = (ThreadDiscussionPane) it.next();
			if(discussionPane.isEditMode()){
				removeNode = discussionPane;
				break;
			}
		}		
		discussionList.getChildren().remove(removeNode);
		replyButton.setVisible(true);
	}

}
