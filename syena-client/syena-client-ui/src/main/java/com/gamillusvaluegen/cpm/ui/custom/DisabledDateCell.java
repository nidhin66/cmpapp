package com.gamillusvaluegen.cpm.ui.custom;

import java.time.LocalDate;

import com.gamillusvaluegen.cpm.ui.util.Utilities;

import javafx.scene.control.DateCell;

public class DisabledDateCell extends DateCell {

	@Override
	public void updateItem(LocalDate item, boolean empty) {
		super.updateItem(item, empty);
		if(Utilities.dateIsAfter(Utilities.LocalDateToDate(item), Utilities.getDate())){
			setDisable(true);
		}
	}

}
