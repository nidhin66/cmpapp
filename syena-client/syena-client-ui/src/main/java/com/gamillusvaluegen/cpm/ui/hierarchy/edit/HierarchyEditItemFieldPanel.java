/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;

/**
 *
 * @author hp
 */
public class HierarchyEditItemFieldPanel extends GridPane {
    
    private Button addButton;
    private HierarchyLevel level;
    private int levelIndex;
    private List<HierarchyEditItemField> items = new ArrayList<>();
    
    public HierarchyEditItemFieldPanel(HierarchyLevel level, int levelIndex) {
        this.level = level;
        this.levelIndex = levelIndex;
        this.setHgap(25);
        this.setVgap(10);
        this.setPadding(new Insets(5, 5, 5, 15));
        this.setAlignment(Pos.CENTER);
        
        addButton = new Button("");
        addButton.setMinSize(25, 25);
        addButton.setMaxSize(25, 25);
        addButton.getStyleClass().add("hierarchy-edit-addItem-button");
        
        addRow();
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                addRow();
            }
        });
        
    }
    
    public HierarchyEditItemField addRow() {
        int numRows = this.getRowConstraints().size();
        
        this.getChildren().remove(addButton);
        
        HierarchyEditItemField field = new HierarchyEditItemField(level);
        items.add(field);
        this.add(field, 0, numRows);
        this.add(addButton, 1, numRows);
        
        RowConstraints rc = new RowConstraints();
        rc.setVgrow(Priority.ALWAYS);
        this.getRowConstraints().add(rc);
        this.setHeight(this.getHeight() + 25);
        if (null != level.getChildren()) {
            for (HierarchyLevel childLevel : level.getChildren()) {
                HierarchyEditItemFieldPanel childPanel = new HierarchyEditItemFieldPanel(childLevel, levelIndex - 1);
                this.add(childPanel, 3, numRows, levelIndex - 1, 1);
                field.addChildPanel(childPanel);
            }
        }
        return field;
    }
    
    public List<? extends StandardHierarchyNode> getContent() {
        List<StandardHierarchyNode> hierarchyNodes = new ArrayList<>();
        if (null != items) {
            for (HierarchyEditItemField field : items) {
                StandardHierarchyNode hierarchyNode = (StandardHierarchyNode) field.getHierarchyNode();
                List<HierarchyEditItemFieldPanel> childPanels = field.getChildPanels();
                if (null != childPanels) {
                    List<StandardHierarchyNode> childNodes = new ArrayList<>();
                    for (HierarchyEditItemFieldPanel childPanel : childPanels) {
                        List<? extends StandardHierarchyNode> childContents = childPanel.getContent();
                        if (null != childContents) {
                            childNodes.addAll(childContents);
                        }                        
                    }   
                    if(hierarchyNode != null){
                        hierarchyNode.addChildren(childNodes);        
                       
                    }                    
                }
                if(null != hierarchyNode){
                      hierarchyNodes.add(hierarchyNode);
                }
              
            }
        }
        return hierarchyNodes;
    }
    
    public void loadData(List<StandardHierarchyNode> hierarchyNodes) {
        int i = 0;
        if (hierarchyNodes != null) {
            if (null != items) {
                for (HierarchyEditItemField field : items) {
                    if (i < hierarchyNodes.size()) {
                        StandardHierarchyNode hierarchyNode = hierarchyNodes.get(i);
                        field.setHierarchyNode(hierarchyNode);
//                        field.setViewMode();
//                        loadChildData(field);
                        i++;
                    }                    
                }
            }
            if (i < hierarchyNodes.size()) {
                for (int j = i; j < hierarchyNodes.size(); j++) {
                    HierarchyEditItemField field = addRow();
                    field.setViewMode();
                    field.setHierarchyNode(hierarchyNodes.get(j));
//                    loadChildData(field);
                }
            }
        }
        
    }
    
    private void loadChildData(HierarchyEditItemField field) {
        List<HierarchyEditItemFieldPanel> childPanels = field.getChildPanels();
        if (null != childPanels) {
            try {
                HierarchyEditItemFieldPanel childpanel = childPanels.get(0);
                PageItr<HierarchyNode> pagedData = HierarchyNodeService.getLinkedHierarchyNodes(field.getHierarchyNode().getId(), 0, 200);
//            childpanel.loadData(pagedData.getContent());
            } catch (ServiceFailedException ex) {
                Logger.getLogger(HierarchyEditItemFieldPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    public List<HierarchyEditItemField> getItems() {
        return items;
    }
    
}
