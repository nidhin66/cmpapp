package com.gamillusvaluegen.cpm.ui.login;

import java.util.List;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.main.MainScreen;
import com.gamillusvaluegen.cpm.ui.server.ApplicationClientService;
import com.gamillusvaluegen.cpm.ui.server.UserManagementService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.util.Configuration;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class LoginController implements DialogController {
	
	private FXMLDialog splashDialog;
	private Pane splashLayout;
	private ProgressBar loadProgress;
	private Label progressText;
	private static final int SPLASH_WIDTH = 700;
	private static final int SPLASH_HEIGHT = 227;


	private FXMLDialog dialog;
	private ScreensConfiguration screens;

	@FXML
	private Button loginButton;

	@FXML
	private TextField userNameField;

	@FXML
	private PasswordField passwordField;

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;
	}

	public LoginController(ScreensConfiguration screens) {
		this.screens = screens;
	}
	
	@FXML
	private void onCancel(){
		dialog.close();
	}

	@FXML
	public void authoticateUser() {
		initSplash();
		String userId = userNameField.getText();
		if (!Utilities.isNullOrEmpty(userId)) {
			String password = passwordField.getText();
			try {
				UserManagementService.loginUser(userId, password);
				Person loginPerson = UserManagementService.getPerson(userId);
				ClientConfig.getInstance().setLoginUser(loginPerson);
				List<ApplicationConfiguration> applicationconfigs = ApplicationClientService.getApplicationConfigs();
				ClientConfig.getInstance().setApplicationConfigs(applicationconfigs);
				Configuration.dateDeference = Utilities.getApplicationDateDeference();
				this.dialog.close();		
//				showSplash(ScreensConfiguration.getInstance().getCurrentParentWindow());
				MainScreen mainScreen = screens.getMainScreen();
				// PlantHierarchyPane plantHierarchyPane = new PlantHierarchyPane();
				Parent screen = ClientConfig.getInstance().getScreenById(CPMClientConstants.MENU_ID_PLANT_HIERARCHY);
				mainScreen.setMainDataPane(screen);
//				screens.setPrimaryStage(new Stage());
				screens.showScreen(mainScreen);
			} catch (ServiceFailedException e) {
				Message errorMessage = e.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}
		
		}

	}
	
	private void showSplash(final Stage initStage) {
//		splashDialog.show();
//		final Rectangle2D bounds = Screen.getPrimary().getBounds();
		
//		screens.showScreen(splashLayout);
//		initStage.initOwner(ScreensConfiguration.getInstance().getCurrentParentWindow());
		Scene splashScene = new Scene(splashLayout);
		initStage.initStyle(StageStyle.UNDECORATED);
		final Rectangle2D bounds = Screen.getPrimary().getBounds();
		initStage.setScene(splashScene);
		initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
		initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
		initStage.setWidth(SPLASH_WIDTH);
		initStage.setHeight(SPLASH_HEIGHT);
		initStage.toFront();
		FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2),
				splashLayout);
				 fadeSplash.setFromValue(1.0);
				 fadeSplash.setToValue(0.0);
				 fadeSplash.setOnFinished(new EventHandler<ActionEvent>() {
					
					@Override
					public void handle(ActionEvent event) {
						 initStage.hide();
//						splashDialog.close();
						
					}
				});
				 fadeSplash.play();
	}

	
	private void initSplash(){
		splashLayout = new Splash();
//		splashDialog = new FXMLDialog(splashLayout, ScreensConfiguration.getInstance().getCurrentParentWindow());
//		ImageView splash = new ImageView(new Image("images/SplashScreen.png"));
//		loadProgress = new ProgressBar();
//		loadProgress.setPrefWidth(SPLASH_WIDTH + 20);
//		progressText = new Label("Loading plant hierachy and project details . . .");
//		splashLayout = new VBox();
//		splashLayout.getChildren().addAll(splash, loadProgress, progressText);
//		progressText.setAlignment(Pos.CENTER);
//		splashLayout.setStyle("-fx-padding: 5; -fx-background-color: transparent;");
//		splashLayout.setEffect(new DropShadow());
	}

}
