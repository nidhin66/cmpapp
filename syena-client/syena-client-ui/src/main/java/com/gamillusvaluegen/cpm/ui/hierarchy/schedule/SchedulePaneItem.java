/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.schedule;

import javafx.scene.layout.BorderPane;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.exception.SchedulingException;

/**
 *
 * @author hp
 */
public class SchedulePaneItem extends BorderPane {

	private NodeCategory nodeType;

	private Job job;

	private JobGroup jobGroup;

	private HierarchyNode node;

	private HierarchyItemMileStonePane mileStonePane;

	private HierarchyItemEstimatePane estimatePane;

	// public SchedulePaneItem(HierarchyItem hierarchyItem){
	// HierarchyItemDescriptionPane descriptionPane = new
	// HierarchyItemDescriptionPane(hierarchyItem);
	// this.setLeft(descriptionPane);
	//
	// HierarchyItemMileStonePane mileStonePane = new
	// HierarchyItemMileStonePane();
	// this.setCenter(mileStonePane);
	//
	// HierarchyItemEstimatePane estimatePane = new HierarchyItemEstimatePane();
	// this.setRight(estimatePane);
	//
	// this.setStyle("-fx-border-width: 0 0 1 0;-fx-border-color: #828A95;");
	// }

	public SchedulePaneItem(HierarchyNode node, boolean approve, ScheduleMode mode) {
		this.nodeType = NodeCategory.hierarchy;
		this.node = node;
		HierarchyItemDescriptionPane descriptionPane = new HierarchyItemDescriptionPane(node);
		this.setLeft(descriptionPane);

		mileStonePane = new HierarchyItemMileStonePane(node.getWorkTime(), mode);
		this.setCenter(mileStonePane);

		estimatePane = new HierarchyItemEstimatePane(node.getWorkTime(),mode, approve);
		this.setRight(estimatePane);

		this.setStyle("-fx-border-width:  0 0 1 0;-fx-border-color: #828A95;");

	}

	public SchedulePaneItem(Job job, boolean approve, ScheduleMode mode) {
		this.nodeType = NodeCategory.job;
		this.job = job;
		HierarchyItemDescriptionPane descriptionPane = new HierarchyItemDescriptionPane(job);
		this.setLeft(descriptionPane);

		mileStonePane = new HierarchyItemMileStonePane(job.getWorkSchedule(), mode);
		this.setCenter(mileStonePane);

		estimatePane = new HierarchyItemEstimatePane(job,mode, approve);
		this.setRight(estimatePane);

		this.setStyle("-fx-border-width:  0 0 1 0;-fx-border-color: #828A95;");
	}

	public SchedulePaneItem(JobGroup jobGroup, boolean approve, ScheduleMode mode) {
		this.nodeType = NodeCategory.jobgroup;
		this.jobGroup = jobGroup;
		HierarchyItemDescriptionPane descriptionPane = new HierarchyItemDescriptionPane(jobGroup);
		this.setLeft(descriptionPane);

		mileStonePane = new HierarchyItemMileStonePane(jobGroup.getWorkSchedule(), mode);
		this.setCenter(mileStonePane);

		estimatePane = new HierarchyItemEstimatePane(jobGroup,mode, approve);
		this.setRight(estimatePane);

		this.setStyle("-fx-border-width:  0 0 1 0;-fx-border-color: #828A95;");
	}

	public Job getJob() {
		mileStonePane.getJobSchedule();
		// WorkTime workTime = estimatePane.getWorkTime();
		// workTime = new WorkTime(mileStonePane.getEstimatedStartDate(),
		// workTime.getDuration());
		// job.setWorkTime(workTime);
		estimatePane.getJob();
		return job;
	}

	public JobGroup getJobGroup() {
		mileStonePane.getJobSchedule();

		estimatePane.getJobGroup();

		return jobGroup;
	}

	public HierarchyNode getHierarchyNode() {
		if (null == node.getWorkTime()) {
			node.setWorkTime(estimatePane.getWorkTime());
			WorkTime mileStoneWorkItem = mileStonePane.getWorkTime();
			node.getWorkTime().setPlanedStartTime(mileStoneWorkItem.getPlannedStartTime());
			node.getWorkTime().setPlanedFinishTime(mileStoneWorkItem.getPlannedFinishTime());
			node.getWorkTime().setActualFinishTime(mileStoneWorkItem.getActualFinishTime());
			node.getWorkTime().setActualStartTime(mileStoneWorkItem.getActualStartTime());
		} else {
			estimatePane.getWorkTime();
			mileStonePane.getWorkTime();
		}
		return node;
	}

	// public JobGroup getJobGroup(){
	// if(null == jobGroup.getWorkTime()){
	// jobGroup.setWorkTime(estimatePane.getWorkTime());
	// WorkTime mileStoneWorkItem = mileStonePane.getWorkTime();
	// jobGroup.getWorkTime().setPlanedStartTime(mileStoneWorkItem.getPlannedStartTime());
	// jobGroup.getWorkTime().setPlanedFinishTime(mileStoneWorkItem.getPlannedFinishTime());
	// jobGroup.getWorkTime().setActualFinishTime(mileStoneWorkItem.getActualFinishTime());
	// jobGroup.getWorkTime().setActualStartTime(mileStoneWorkItem.getActualStartTime());
	// }else{
	// estimatePane.getWorkTime();
	// mileStonePane.getWorkTime();
	// }
	// return jobGroup;
	// }

	public boolean isApproved() {
		try {
			return estimatePane.isApproved();
		} catch (SchedulingException e) {
			return false;
		}
	}

}
