/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.dependency;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.server.DependencyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class DependentNodePane extends DependentItemPane {

	public DependentNodePane(HierarchyItemPanel hierarchyItemPanel, HierarchyLevel level) {
		this.level = level;

		contextID = CONTEXT_DEPENDENT;
		group = new ToggleGroup();

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dependency_item_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		this.getStyleClass().add("dependency-tab");

		predecessorJobsTable.getStyleClass().add("dependency-table");
		successorJobsTable.getStyleClass().add("dependency-table");

//		successorJobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//		predecessorJobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		predecessorDependentJobContextMenu = new DependentJobContextMenu(this, "P");
		successorDependentJobContextMenu = new DependentJobContextMenu(this, "S");

		successorJobsTable.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {
					if (successorDependentJobContextMenu.isShowing()) {
						successorDependentJobContextMenu.hide();
					}
					List<? extends HierarchyItem> jobs = getSelectedSuccessorItems();
					if (null != jobs && jobs.size() > 0) {
						successorDependentJobContextMenu.show(successorJobsTable, e.getScreenX(), e.getScreenY());
					}

				}
			}
		});

		predecessorJobsTable.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {
					if (predecessorDependentJobContextMenu.isShowing()) {
						predecessorDependentJobContextMenu.hide();
					}
					List<? extends HierarchyItem> jobs = getSelectedPredecessorItems();
					if (null != jobs && jobs.size() > 0) {
						predecessorDependentJobContextMenu.show(predecessorJobsTable, e.getScreenX(), e.getScreenY());
					}

				}
			}
		});

	}

	//
	// @FXML
	// public void showAllJobGroups() {
	// PageItr<? extends JobItem> jobGroups =
	// HierarchyJobService.getAllJobGroups(0, 1000);
	// ObservableList<JobItem> items =
	// FXCollections.observableArrayList(jobGroups.getContent());
	// predecessorJobsTable.setItems(items);
	// successorJobsTable.setItems(items);
	// }
	//
	//
	// @FXML
	// public void showDependentJobs() {
	// predecessorJobsTable.setItems(null);
	// successorJobsTable.setItems(null);
	// }

	@Override
	public void addAsSuccessor() {
		try {
			List<? extends HierarchyItem> successorItems = getSelectedSuccessorItems();
			HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
			HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
			if (null != successorItems && null != hierarchyItem) {
				DependencyJobService.addSuccessorHierarchyNodeDependency(hierarchyItem.getId(), successorItems);
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Successor added successfully");
				alertDialog.show();
				group.selectToggle(dependentButton);
				loadDependentData();
			}else{
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, "Please select any item from " + level.getLevelName() + " to add dependency");
				alertDialog.show();
			}
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}

	}

	@Override
	public void addAsPredecessor() {
		try {
			List<? extends HierarchyItem> predecessorItems = getSelectedPredecessorItems();
			HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
			HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
			if (null != predecessorItems && null != hierarchyItem) {
				DependencyJobService.addPredecessorHierarchyNodeDependency(hierarchyItem.getId(), predecessorItems);
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Predecessor added successfully");
				alertDialog.show();
				group.selectToggle(dependentButton);
				loadDependentData();
			}else{
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, "Please select any item from " + level.getLevelName() + " to add dependency");
				alertDialog.show();
			}
			
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@Override
	public void removePredecessor() {
		try {
			List<? extends HierarchyItem> selectedPredecessorItems = getSelectedPredecessorItems();
			HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
			HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
			if (null != selectedPredecessorItems && null != hierarchyItem) {
				DependencyJobService.removePredecessorHierarchyNodeDependency(hierarchyItem.getId(), selectedPredecessorItems);
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Predecessor dependency removed successfully");
				alertDialog.show();
				group.selectToggle(dependentButton);
			}
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@Override
	public void removeSuccessor() {
		try {
		List<? extends HierarchyItem> selectedSuccessorItems = getSelectedSuccessorItems();
		HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
		HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
		if (null != selectedSuccessorItems && null != hierarchyItem) {
			DependencyJobService.removeSuccessorHierarchyNodeDependency(hierarchyItem.getId(), selectedSuccessorItems);
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Successor dependency removed successfully");
			alertDialog.show();
			group.selectToggle(dependentButton);
		}
	} catch (ServiceFailedException ex) {
		Message errorMessage = ex.getErroMessage();
		AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
		alertDialog.show();
	}
	}

	@Override
	public List<? extends HierarchyItem> searchData(String context, String searchText) {
		PageItr<HierarchyNode> pageHierarchyNodes = null;
		List<? extends HierarchyItem> hierarchyNodes = new ArrayList<>();
		if(!Utilities.isNullOrEmpty(searchText)) {
			if (contextID == CONTEXT_ALL) {
				try {
					pageHierarchyNodes = HierarchyNodeService.searchHierarchyNodesInLevel(searchText + "%", level.getLevelName(), 0, pageSize) ;
				} catch (ServiceFailedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if ("P".equalsIgnoreCase(context)) {
					loadPredecessorTable(pageHierarchyNodes);
				}else{
					loadSuccessorTable(pageHierarchyNodes);
				}
			}else{
				HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
				HierarchyItem hierarchyItem = itemPanel.getSelectedItem();
				if (null != hierarchyItem) {
					if ("P".equalsIgnoreCase(context)) {

						try {
							pageHierarchyNodes = DependencyJobService.searchPredecessorHierarchyNodeDependency(
									hierarchyItem.getId(), searchText + "%", 0, pageSize);
						} catch (ServiceFailedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						loadPredecessorTable(pageHierarchyNodes);
					} else {
						try {
							pageHierarchyNodes = DependencyJobService.searchSuccessorHierarchyNodeDependency(
									hierarchyItem.getId(), searchText + "%", 0, pageSize);
						} catch (ServiceFailedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						loadSuccessorTable(pageHierarchyNodes);
					}
				}

			}
		}else{
			if ("P".equalsIgnoreCase(context)) {
				loadPredecessorFirstPage();
			}else{
				loadSuccessorFirstPage();
			}
		}

		
		if (null != pageHierarchyNodes) {
			hierarchyNodes = pageHierarchyNodes.getContent();
		}
		return hierarchyNodes;
	}

}
