/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.statusview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.HierarchySearchController;
import com.gamillusvaluegen.cpm.ui.custom.SearchTextField;
import com.gamillusvaluegen.cpm.ui.hierarchy.search.QuickSearchPane;
import com.gamillusvaluegen.cpm.ui.server.CriticalPathService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.server.StatusViewService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartPoint;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.services.criticalpath.CriticalAndNonCriticalPath;
import com.gvg.syena.core.services.criticalpath.JobData;
import com.gvg.syena.core.services.criticalpath.JobDataPeriod;
import com.gvg.syena.core.services.criticalpath.JobDataSet;

/**
 *
 * @author hp
 */
public class StatusViewPane extends BorderPane implements HierarchySearchController {

	@FXML
	private GridPane criticalJobGrid;

	@FXML
	private GridPane nonCriticalJobGrid;

	@FXML
	private BorderPane chartPane;

	@FXML
	private ToggleButton unitButton;

	@FXML
	private ToggleButton systemButton;

	@FXML
	private ToggleButton subSystemButton;

	@FXML
	private ToggleButton loopButton;

	@FXML
	// private ToggleButton jobButton;

	private ToggleGroup searchBtnGroup;

	@FXML
	private BorderPane quickSearchPaneHolder;

	@FXML
	private RadioButton workEfficiencyBtn;

	@FXML
	private RadioButton scheduleEfficiencyBtn;

	@FXML
	private RadioButton planningEfficiencyBtn;

	@FXML
	private SearchTextField searchBox;

	private ToggleGroup radioGroup;

	private LineChart<String, Number> lineChart = null;

	private HierarchyNode currentHierarchyNode = null;

	@FXML
	private void initialize() {
		radioGroup = new ToggleGroup();
		workEfficiencyBtn.setToggleGroup(radioGroup);
		scheduleEfficiencyBtn.setToggleGroup(radioGroup);
		planningEfficiencyBtn.setToggleGroup(radioGroup);

		searchBtnGroup = new ToggleGroup();

		unitButton.setToggleGroup(searchBtnGroup);
		unitButton.setUserData("Unit");
		unitButton.setTooltip(new Tooltip("Unit"));
		searchBox.setPromptText("Search Unit");

		systemButton.setToggleGroup(searchBtnGroup);
		systemButton.setUserData("System");
		systemButton.setTooltip(new Tooltip("System"));

		subSystemButton.setToggleGroup(searchBtnGroup);
		subSystemButton.setUserData("Sub system");
		subSystemButton.setTooltip(new Tooltip("Sub system"));

		loopButton.setToggleGroup(searchBtnGroup);
		loopButton.setUserData("Loop/Equipments");
		loopButton.setTooltip(new Tooltip("Loop/Equipments"));

		// jobButton.setToggleGroup(searchBtnGroup);
		// jobButton.setUserData("Job");
		// jobButton.setTooltip(new Tooltip("Job"));

		searchBtnGroup.selectToggle(unitButton);

		searchBtnGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle) {
				if (null != new_toggle) {
					searchBox.setPromptText("Search " + new_toggle.getUserData());
				}
			}
		});

		searchBox.setController(this);
		// ClientConfig.getInstance().getPlant().get
	}

	public StatusViewPane() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/status_view.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis(0.0, 120, 25);
		xAxis.setLabel("Weeks");
		// xAxis.setMinorTickVisible(false);
		yAxis.setMinorTickVisible(false);
		yAxis.setLabel("Status");

		lineChart = new LineChart<>(xAxis, yAxis);
		lineChart.setTitle("Syena View");

		lineChart.getData().addListener(new ListChangeListener<XYChart.Series<String, Number>>() {
			@Override
			public void onChanged(ListChangeListener.Change<? extends XYChart.Series<String, Number>> c) {
				lineChart.setAnimated(c.getList().size() > 1);
			}
		});

		chartPane.setCenter(lineChart);
		QuickSearchPane quickSearchPane = new QuickSearchPane();
		quickSearchPaneHolder.setLeft(quickSearchPane);
		quickSearchPane.disableCancel();
		
		HierarchyNode plant = ClientConfig.getInstance().getPlant();
		loadChartData(plant);

	}

	@FXML
	private void showWorkEfficiencyChart() {
		if (workEfficiencyBtn.isSelected()) {
			ChartType[] charTypes = new ChartType[1];
			charTypes[0] = ChartType.workefficiency;
			if (null != currentHierarchyNode) {
				try {
					ChartData<Date> chartData = StatusViewService.getChartsData(currentHierarchyNode.getId(),
							charTypes);
					loadPredictionChart(chartData);
				} catch (ServiceFailedException ex) {
					Message errorMessage = ex.getErroMessage();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
					alertDialog.show();
				}
			}

		}
	}

	@FXML
	private void showScheduleEfficiencyChart() {
		if (scheduleEfficiencyBtn.isSelected()) {
			ChartType[] charTypes = new ChartType[1];
			charTypes[0] = ChartType.scheduleefficiency;
			if (null != currentHierarchyNode) {
				try {
					ChartData<Date> chartData = StatusViewService.getChartsData(currentHierarchyNode.getId(),
							charTypes);
					loadPredictionChart(chartData);
				} catch (ServiceFailedException ex) {
					Message errorMessage = ex.getErroMessage();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
					alertDialog.show();
				}
			}

		}
	}

	@FXML
	private void showPlanningEfficiencyChart() {
		if (planningEfficiencyBtn.isSelected()) {
			ChartType[] charTypes = new ChartType[1];
			charTypes[0] = ChartType.planningefficiency;
			if (null != currentHierarchyNode) {
				try {
					ChartData<Date> chartData = StatusViewService.getChartsData(currentHierarchyNode.getId(),
							charTypes);
					loadPredictionChart(chartData);
				} catch (ServiceFailedException ex) {
					Message errorMessage = ex.getErroMessage();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
					alertDialog.show();
				}
			}

		}
	}

	@Override
	public List<? extends HierarchyItem> searchData(String text) {
		List<? extends HierarchyItem> hierarchyNodes = new ArrayList<>();
		Toggle selectedToggle = searchBtnGroup.getSelectedToggle();
		if (null != selectedToggle) {
			try {
				String level = (String) selectedToggle.getUserData();
				PageItr<? extends HierarchyItem> hierarchyNodesPage = null;
				if ("Job".equalsIgnoreCase(level)) {
					JobProperties jobSearchOption = new JobProperties();
					jobSearchOption.setStandard(false);
					jobSearchOption.setName(text + "%");
					try {
						hierarchyNodesPage = HierarchyJobService.searchJobWithProperties(jobSearchOption, 0, 25);
					} catch (ServiceFailedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					hierarchyNodesPage = HierarchyNodeService.searchHierarchyNodesInLevel(text + "%", level, 0, 25);
				}

				if (null != hierarchyNodesPage) {
					hierarchyNodes = hierarchyNodesPage.getContent();
				}
			} catch (ServiceFailedException ex) {
				ex.printStackTrace();
			}
		}
		return hierarchyNodes;
	}

	@FXML
	private void showChart() {
		Toggle selectedToggle = searchBtnGroup.getSelectedToggle();
		if (null != selectedToggle) {
			List<? extends HierarchyItem> hierarchyNodes = searchData(searchBox.getText() + "%");
			if (null != hierarchyNodes) {
				if (null != hierarchyNodes && hierarchyNodes.size() > 0) {
					HierarchyNode hierarchyNode = (HierarchyNode) hierarchyNodes.get(0);
					loadChartData(hierarchyNode);
				} else {
					currentHierarchyNode = null;
				}
			}

		}
	}

	private void loadChartData(HierarchyNode hierarchyNode) {
		try {
			currentHierarchyNode = hierarchyNode;
			ChartType[] charTypes = new ChartType[4];
			charTypes[0] = ChartType.Planned;
			charTypes[1] = ChartType.Revised;
			charTypes[2] = ChartType.Projected;
			charTypes[3] = ChartType.Actual;
			resetUI();
			ChartData<Date> chartData = StatusViewService.getChartsData(hierarchyNode.getId(), charTypes);
			loadDefaultChart(chartData);
			CriticalAndNonCriticalPath criticalAndNonCriticalPath = CriticalPathService
					.findCriticalAndNonCriticalPath(hierarchyNode.getId());
			if (null != criticalAndNonCriticalPath) {
				loadCriticalJobs(hierarchyNode, criticalAndNonCriticalPath.getCriticalPath());
				loadNonCriticalJobs(hierarchyNode, criticalAndNonCriticalPath.getNonCriticalPath());
			}
		} catch (ServiceFailedException ex) {
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, ex.getErroMessage().getMessage());
			alertDialog.show();
		}
	}

	private void loadPredictionChart(ChartData<Date> chartData) {
		if (lineChart.getData().size() > 4) {
			lineChart.getData().remove(4);
		}
		if (null != chartData) {
			if (null != chartData.getDataSeriesNames() && chartData.getDataSeriesNames().length > 0) {
				int i = 0;
				for (String seriesName : chartData.getDataSeriesNames()) {
					if (null != chartData.getChartPoints()) {
						XYChart.Series series = new XYChart.Series();
						series.setName(seriesName);
						lineChart.getData().add(series);
						int j = 1;
						for (ChartPoint<Date> chartPoint : chartData.getChartPoints()) {
							Float pointValue = chartPoint.getValues()[i];
							if (null != pointValue) {
								series.getData().add(new XYChart.Data("Week " + j, pointValue.doubleValue()));
							}
							j++;
						}

					}
					i++;
				}
			}
		}
	}

	private void resetUI() {
		if (null != lineChart.getData()) {
			lineChart.getData().clear();
		}

		if (null != criticalJobGrid.getChildren()) {
			criticalJobGrid.getChildren().clear();
			criticalJobGrid.getColumnConstraints().clear();
		}

		if (null != nonCriticalJobGrid.getChildren()) {
			nonCriticalJobGrid.getChildren().clear();
			nonCriticalJobGrid.getColumnConstraints().clear();
		}
		workEfficiencyBtn.setSelected(false);
		planningEfficiencyBtn.setSelected(false);
		scheduleEfficiencyBtn.setSelected(false);
	}

	private void loadDefaultChart(ChartData<Date> chartData) {

		if (null != chartData) {
			if (null != chartData.getDataSeriesNames() && chartData.getDataSeriesNames().length > 0) {
				int i = 0;
				for (String seriesName : chartData.getDataSeriesNames()) {
					if (null != chartData.getChartPoints()) {
						XYChart.Series series = new XYChart.Series();
						series.setName(seriesName);
						lineChart.getData().add(series);
						int j = 1;
						for (ChartPoint<Date> chartPoint : chartData.getChartPoints()) {
							Float pointValue = chartPoint.getValues()[i];
							if (null != pointValue) {
								XYChart.Data<String, Number> data = new XYChart.Data("Week " + j,
										pointValue.doubleValue());
								data.setNode(new HoveredThresholdNode(pointValue.doubleValue()));
								series.getData().add(data);
							}
							j++;
						}

					}
					i++;
				}
			}
		}
		// lineChart.re
	}

	private void loadNonCriticalJobs(HierarchyNode hierarchyNode, List<JobData> jobDataList) {

		Date startDate = hierarchyNode.getWorkTime().getPlannedStartTime();
		Date endDate = hierarchyNode.getWorkTime().getPlannedFinishTime();

		// List<JobData> jobDataList =
		// CriticalPathService.findNonCriticalPath(hierarchyNode.getId());
		if (null != jobDataList) {
			JobDataSet jobDataSet = new JobDataSet(startDate, endDate);
			jobDataSet.add(jobDataList);
			Set<JobDataPeriod> criticalPathPeriods = jobDataSet.getCriticalPathPeriods();
			if (null != criticalPathPeriods) {
				int i = 0;
				for (Iterator<JobDataPeriod> it = criticalPathPeriods.iterator(); it.hasNext();) {
					JobDataPeriod criticalPathPeriod = it.next();
					Map<WorkStatusType, List<JobData>> criticalPathJobDatas = criticalPathPeriod
							.getCriticalPathJobDatas();
					List<CriticalJobList> criticaljobs = new ArrayList<>();
					if (null != criticalPathJobDatas) {
						for (WorkStatusType workStatus : criticalPathJobDatas.keySet()) {
							List<JobData> criticalPathJobs = criticalPathJobDatas.get(workStatus);
							if (null != criticalPathJobs) {
								long[] jobids = new long[criticalPathJobs.size()];
								int j = 0;
								CriticalJobList criticalJobList = new CriticalJobList(workStatus, jobids);
								for (JobData criticalPathJobData : criticalPathJobs) {
									jobids[j] = criticalPathJobData.getJobId();									
									j++;
								}
								criticaljobs.add(criticalJobList);
							}
						}
					}
					JobsStatusList jobsStatusList = new JobsStatusList(criticaljobs);
					jobsStatusList.setHeaderLabel("Week " + (i + 1));
					nonCriticalJobGrid.add(jobsStatusList, i, 0);
					nonCriticalJobGrid.getColumnConstraints().add(i, new ColumnConstraints(250));
					i++;
				}
			}
		}

	}

	private void loadCriticalJobs(HierarchyNode hierarchyNode, List<JobData> jobDataList) {
		Date startDate = hierarchyNode.getWorkTime().getPlannedStartTime();
		Date endDate = hierarchyNode.getWorkTime().getPlannedFinishTime();

		// List<JobData> jobDataList =
		// CriticalPathService.findCriticalPath(hierarchyNode.getId());
		if (null != jobDataList) {
			JobDataSet jobDataSet = new JobDataSet(startDate, endDate);
			jobDataSet.add(jobDataList);
			Set<JobDataPeriod> criticalPathPeriods = jobDataSet.getCriticalPathPeriods();
			if (null != criticalPathPeriods) {
				int i = 0;
				for (Iterator<JobDataPeriod> it = criticalPathPeriods.iterator(); it.hasNext();) {
					JobDataPeriod criticalPathPeriod = it.next();
					Map<WorkStatusType, List<JobData>> criticalPathJobDatas = criticalPathPeriod
							.getCriticalPathJobDatas();
					List<CriticalJobList> criticaljobs = new ArrayList<>();
					if (null != criticalPathJobDatas) {
						for (WorkStatusType workStatus : criticalPathJobDatas.keySet()) {
							List<JobData> criticalPathJobs = criticalPathJobDatas.get(workStatus);
							criticalPathJobs.size();
							if (null != criticalPathJobs) {
								long[] jobids = new long[criticalPathJobs.size()];
								int j = 0;
								CriticalJobList criticalJobList = new CriticalJobList(workStatus, jobids);
								for (JobData criticalPathJobData : criticalPathJobs) {
									jobids[j] = criticalPathJobData.getJobId();								
									j++;
								}
								criticaljobs.add(criticalJobList);
							}
						}
					}
					JobsStatusList jobsStatusList = new JobsStatusList(criticaljobs);
					jobsStatusList.setHeaderLabel("Week " + (i + 1));
					criticalJobGrid.add(jobsStatusList, i, 0);
					criticalJobGrid.getColumnConstraints().add(i, new ColumnConstraints(250));
					i++;
				}
			}
		}
	}

	class HoveredThresholdNode extends StackPane {

		HoveredThresholdNode(double value) {
			setPrefSize(15, 15);

			final Label label = createDataThresholdLabel(value);

			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					getChildren().setAll(label);
					setCursor(Cursor.NONE);
					toFront();
				}
			});
			setOnMouseExited(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					getChildren().clear();
					setCursor(Cursor.CROSSHAIR);
				}
			});
		}

		private Label createDataThresholdLabel(double value) {
			final Label label = new Label(Utilities.round(value, 2) + "");
			label.getStyleClass().addAll("default-color0", "chart-line-symbol", "chart-series-line");
			label.setStyle("-fx-font-size: 20; -fx-font-weight: bold;");

			// if (priorValue == 0) {
			// label.setTextFill(Color.DARKGRAY);
			// } else if (value > priorValue) {
			// label.setTextFill(Color.FORESTGREEN);
			// } else {
			label.setTextFill(Color.FIREBRICK);
			// }

			label.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
			return label;
		}
	}

}
