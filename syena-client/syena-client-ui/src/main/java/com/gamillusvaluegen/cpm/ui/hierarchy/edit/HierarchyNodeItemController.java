/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.custom.PersonSearchField;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.organization.Person;

/**
 *
 * @author hp
 */
public class HierarchyNodeItemController implements DialogController {

    private FXMLDialog dialog;

    private HierarchyEditItemField itemField;

    @FXML
    private TextField nameField;

    @FXML
    private TextArea descriptionField;

    @FXML
    private Label levelLabel;

    @FXML
    private TextField weightageField;

    @FXML
    private PersonSearchField ownerField;

    @FXML
    private TextField statusField;

    @FXML
    private Button closeBtn;

    public HierarchyNodeItemController(HierarchyEditItemField itemField) {
        this.itemField = itemField;
    }

    @FXML
    private void initialize() {
        levelLabel.setText(itemField.getLevel().getLevelName());
        HierarchyItem hierarchyItem = itemField.getHierarchyNode();
        StandardHierarchyNode standardHierarchyNode = null;
        HierarchyNode hierarchyNode = null;
        standardHierarchyNode = (StandardHierarchyNode) hierarchyItem;
        if (hierarchyItem instanceof HierarchyNode) {
            hierarchyNode = (HierarchyNode) hierarchyItem;
        }
        
        if (null != standardHierarchyNode) {
            if (this.itemField.getMode().equalsIgnoreCase("V")) {
                nameField.setEditable(false);
                descriptionField.setEditable(false);
                ownerField.setEditable(false);
                weightageField.setEditable(false);
            }
            if (null != standardHierarchyNode.getOwner()) {
                ownerField.setText(standardHierarchyNode.getOwner().getId());
                ownerField.setPerson(standardHierarchyNode.getOwner());
                ownerField.setEditable(false);
            }
            if (null != hierarchyNode && null != hierarchyNode.getWorkStatus() && null != hierarchyNode.getWorkStatus().getRunningStatus()) {
                statusField.setText(hierarchyNode.getWorkStatus().getRunningStatus().toString());
            }
            nameField.setText(standardHierarchyNode.getName());
            descriptionField.setText(standardHierarchyNode.getDescription());
            weightageField.setText(String.valueOf(standardHierarchyNode.getWeightage()));
            if(null != standardHierarchyNode.getOwner()){
            	ownerField.setText(standardHierarchyNode.getOwner().getId());
            }
        }
    }

    @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    @FXML
    private void closeDialog() {
        dialog.close();
    }

    @FXML
    private void submitData() {
        if (!this.itemField.getMode().equalsIgnoreCase("V")) {
            if (!Utilities.isNullOrEmpty(nameField.getText())) {
                StandardHierarchyNode hierarchyNode = (StandardHierarchyNode) itemField.getHierarchyNode();
                if(null == hierarchyNode){
                	hierarchyNode = new StandardHierarchyNode(nameField.getText(), descriptionField.getText(), itemField.getLevel());
                }else{
                	hierarchyNode.setName(nameField.getText());
                	hierarchyNode.setDescription(nameField.getText());
                }
               
                if (Utilities.isNullOrEmpty(weightageField.getText())) {
                    hierarchyNode.setWeightage(0);
                } else {
                    hierarchyNode.setWeightage(Double.valueOf(weightageField.getText()));
                }
                String ownerString = ownerField.getText();
                Person owner = null;
                if(!Utilities.isNullOrEmpty(ownerString)){
    				owner = ownerField.getPerson();
    			}
                hierarchyNode.setOwner(owner);
                this.itemField.setHierarchyNode(hierarchyNode);
                dialog.close();
            }
        }

    }

    @FXML
    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.showOpenDialog(dialog);
    }
}
