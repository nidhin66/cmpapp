package com.gamillusvaluegen.cpm.ui.util;

import com.gvg.syena.core.api.exception.ApplicationException;

public class RestCallException extends ApplicationException {

	public RestCallException(Exception e) {
		super(e);
	}

}
