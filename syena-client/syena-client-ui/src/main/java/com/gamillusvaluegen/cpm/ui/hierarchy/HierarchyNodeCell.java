/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

import org.controlsfx.control.spreadsheet.SpreadsheetCellBase;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.WorkStatusType;

/**
 *
 * @author hp
 */
public class HierarchyNodeCell extends SpreadsheetCellBase {

    public HierarchyNodeCell(int row, int column, int rowSpan, int columnSpan, HierarchyItem value) {
        super(row, column, rowSpan, columnSpan, new HierarchyNodeCellType());
        this.setItem(value);
        
     
        
         HBox hbox = new HBox();
         hbox.setPadding(new Insets(5,5,2,0));
        hbox.setSpacing(5);
     
 
                

        if (null != value) {
            
            String name = value.getName();
            String desc = value.getDescription();
//            if(!Utilities.isNullOrEmpty(name)){
//                name = String.format("%-15s", name);
//            }
//            if(!Utilities.isNullOrEmpty(desc)){
//                if(Utilities.isNullOrEmpty(name)){
//                     if(desc.length() > 60){                        
//                        desc = desc.substring(0, 60); 
//                     }
//                }else{
//                    if(desc.length() > 30){
//                        desc = desc.substring(0, 30);
//                    }    
//                }
//                            
//            }             
            
                    
            
//            Text t = new Text(name); 
//            t.setTranslateX(1);//            
//            Text t1 = new Text(desc);             
//            t1.setTranslateX(1);
//            if(name.length() > 30){   
//                
//            }
            Label nameLabel = new Label(name);
            if(null != value.getNodeCategory()){
            	if(value.getNodeCategory() == NodeCategory.job || value.getNodeCategory() == NodeCategory.jobgroup){
                	nameLabel.setText(value.getDescription());
                }
            }
            nameLabel.getStyleClass().add("spreadsheet-cell-name-text");
          
            
            
            Label descLabel = new Label(desc);
            descLabel.getStyleClass().add("spreadsheet-cell-desc-text");
            nameLabel.setPrefWidth(225);
            
//            if(Utilities.isNullOrEmpty(desc)){
//            	   nameLabel.setPrefWidth(225);
//                   descLabel.setPrefWidth(0);
//            }else{
//            	   nameLabel.setPrefWidth(100);
//                   descLabel.setPrefWidth(125);
//            }
//         

           
           
            if(Utilities.isNullOrEmpty(desc)){
            	 this.setTooltip(value.getName());
            }else{
            	this.setTooltip(value.getDescription());
            }
           
            
//            pane.setLeft(nameLabel);
//            pane.setCenter(descLabel);               
//            pane.setRight(rect);
          
            hbox.getChildren().addAll(nameLabel);  
            if(null != value.getStatus()){
            	
            	
            	
            	WorkStatusType statusType = WorkStatusType.getWorkStatusType(value.getStatus());
            	
            	Rectangle rect = ClientConfig.getStatusGraphics(statusType);
            	
                 hbox.getChildren().add(rect);
//                 WorkStatusType
            }       
            
            this.setGraphic(hbox);            
        }     
          
    }
    
    
  


}
