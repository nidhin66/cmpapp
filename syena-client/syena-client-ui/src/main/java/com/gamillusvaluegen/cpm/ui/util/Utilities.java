/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.joda.time.DateTime;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gvg.syena.core.api.util.ApplicationDateUtil;

/**
 *
 * @author hp
 */
public class Utilities {
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	public static Date nextDate(Date date, int dateDeference){
		DateTime dateTime = new DateTime(date);
		return dateTime.plusDays(dateDeference).toDate();
	}

    public static boolean isNullOrEmpty(String str) {
        boolean flag = false;
        if (null == str || "".equalsIgnoreCase(str) || str.equalsIgnoreCase("null")) {
            flag = true;
        }
        return flag;
    }

    public static String getUser() {

        return System.getProperty("user.name");

    }
    
	public static Date getDate() {
		int dateDeference = getApplicationDateDeference();
		return ApplicationDateUtil.getApplicationDate(dateDeference);
	}
	
	public static int getApplicationDateDeference(){
		String valueString = ClientConfig.getInstance().getApplicationConfig(ApplicationDateUtil.APPLICATION_DATE_HOUR_DEFERENCE);
		int dateDeference = 0;
		if (!Utilities.isNullOrEmpty(valueString)) {
			dateDeference = Integer.valueOf(valueString);
		}
		return dateDeference;
	}
    
   
    
    public static String dateToStringdd_MMM_yyyy(Date date){
    	 SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
         String dateStr = format.format(date);
         return dateStr;
    }
    
     public static String dateToString_yyyy_MM_dd(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = format.format(date);
        return dateStr;
    }
     
     public static Date stringToDate_yyyy_MM_dd(String dateString) throws ParseException{
         
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(dateString);
        return date;
    
     }
     
     public static LocalDate dateToLocalDate(Date input){
         LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
         return date;
     }
     
     public static Date LocalDateToDate(LocalDate localDate){
         Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
         return date;
     }
     
     
 	/**
 	 * Is the first date is after the second date
 	 * @param firstDate
 	 * @param secondDate
 	 * @return
 	 */
 	
 	public static boolean dateIsAfter(Date firstDate,Date secondDate){
 		DateTime firstDateTime = new DateTime(firstDate);
 		DateTime secondDatetime = new DateTime(secondDate);
 		return firstDateTime.isAfter(secondDatetime);
 	}
 	
 	/**
 	 * Is the First date is before second date
 	 * @param firstDate
 	 * @param secondDate
 	 * @return
 	 */
 	public static boolean dateIsbefore(Date firstDate,Date secondDate){
 		DateTime firstDateTime = new DateTime(firstDate);
 		DateTime secondDatetime = new DateTime(secondDate);
 		return firstDateTime.isBefore(secondDatetime);
 	}
   
}
