package com.gamillusvaluegen.cpm.ui.notification;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import com.gvg.syena.core.api.services.notification.NotificationType;

public class NotificationWindowHeader extends BorderPane{
	
	private NotificationWindow parentWindow;
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Label headerLabel;
	
	@FXML
	private Button alertButton;
	
	@FXML
	private Button warningButton;
	
	@FXML
	private Button informationButton;
	
	
	public NotificationWindowHeader(NotificationWindow parentWindow){
		this.parentWindow = parentWindow;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/notification_window_header.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		headerLabel.setText("Alerts");
		
		alertButton.getStyleClass().remove("notification-alert-button");
		alertButton.getStyleClass().add("notification-alert-button-selected");
		
		warningButton.getStyleClass().remove("notification-warning--button-selected");
		warningButton.getStyleClass().add("notification-warning--button");	
		
		informationButton.getStyleClass().remove("notification-info--button-selected");
		informationButton.getStyleClass().add("notification-info--button");

	}
	
	@FXML
	private void closeDialog(){
		parentWindow.closeWindow();
	}
	
	@FXML
	private void showAlerts(){
		alertButton.getStyleClass().remove("notification-alert-button");
		alertButton.getStyleClass().add("notification-alert-button-selected");
		
		warningButton.getStyleClass().remove("notification-warning--button-selected");
		warningButton.getStyleClass().add("notification-warning--button");	
		
		informationButton.getStyleClass().remove("notification-info--button-selected");
		informationButton.getStyleClass().add("notification-info--button");

		headerLabel.setText("Alerts");
		
		parentWindow.loadNotificationData(NotificationType.ALERT);
	}
	
	@FXML
	private void showWarnings(){
		
		warningButton.getStyleClass().remove("notification-warning--button");
		warningButton.getStyleClass().add("notification-warning--button-selected");	
		
		
		alertButton.getStyleClass().remove("notification-alert-button-selected");
		alertButton.getStyleClass().add("notification-alert-button");
		
		informationButton.getStyleClass().remove("notification-info--button-selected");
		informationButton.getStyleClass().add("notification-info--button");
		
		
		headerLabel.setText("Warnings");
		parentWindow.loadNotificationData(NotificationType.WARN);
	}
	
	@FXML
	private void showInformations(){
		informationButton.getStyleClass().remove("notification-info--button");
		informationButton.getStyleClass().add("notification-info--button-selected");
		
		warningButton.getStyleClass().remove("notification-warning--button-selected");
		warningButton.getStyleClass().add("notification-warning--button");		
		
		alertButton.getStyleClass().remove("notification-alert-button-selected");
		alertButton.getStyleClass().add("notification-alert-button");
		
		headerLabel.setText("Informations");
		parentWindow.loadNotificationData(NotificationType.INFO);
	}
}
