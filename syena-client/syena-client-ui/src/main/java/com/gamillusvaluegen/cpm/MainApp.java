package com.gamillusvaluegen.cpm;

import javafx.animation.PathTransition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MainApp extends Application {

	private Pane splashLayout;
	private ProgressBar loadProgress;
	private Label progressText;

	private Stage mainStage;
	private static final int SPLASH_WIDTH = 700;
	private static final int SPLASH_HEIGHT = 227;

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void init() {
		ImageView splash = new ImageView(new Image("images/SplashScreen.png"));
		loadProgress = new ProgressBar();
		loadProgress.setPrefWidth(SPLASH_WIDTH + 20);
		progressText = new Label("Loading plant hierachy and project details . . .");
		splashLayout = new VBox();
		splashLayout.getChildren().addAll(splash, loadProgress, progressText);
		progressText.setAlignment(Pos.CENTER);
		splashLayout.setStyle("-fx-padding: 5; -fx-background-color: transparent;");
		splashLayout.setEffect(new DropShadow());
	}

	@Override
	public void start(final Stage initStage) throws Exception {

//		 showSplash(new Stage());
		showMainStage(initStage);

//		 webView.getEngine().documentProperty().addListener(new
//		 ChangeListener<Document>() {
//		 @Override
//		 public void changed(ObservableValue<? extends Document>
//		 observableValue, Document document, Document document1) {
//		 if (initStage.isShowing()) {
//		 loadProgress.progressProperty().unbind();
//		 loadProgress.setProgress(1);
//		 progressText.setText("Configurations loaded successfully.");
//		 mainStage.setIconified(false);
//		 initStage.toFront();
//		 FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2),
//		 splashLayout);
//		 fadeSplash.setFromValue(1.0);
//		 fadeSplash.setToValue(0.0);
//		 fadeSplash.setOnFinished(new EventHandler<ActionEvent>() {
//		 @Override
//		 public void handle(ActionEvent actionEvent) {
//		 initStage.hide();
//		 }
//		 });
//		 fadeSplash.play();
//		 }
//		 }
//		 });
	}

	private void showMainStage(Stage stage) {
		ScreensConfiguration.getInstance().setPrimaryStage(stage);
		ScreensConfiguration.getInstance().loginDialog().display(createLoginTransition());
	}

	private void showSplash(Stage initStage) {
		Scene splashScene = new Scene(splashLayout);
		initStage.initStyle(StageStyle.UNDECORATED);
		final Rectangle2D bounds = Screen.getPrimary().getBounds();
		initStage.setScene(splashScene);
		initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
		initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
		initStage.show();
	}

	private PathTransition createLoginTransition() {
		Path path = new Path();
		path.getElements().add(new MoveTo(600 / 2, 600));
		path.getElements().add(new LineTo(600 / 2, 400 / 2));
		PathTransition pathTransition = new PathTransition();
		pathTransition.setDuration(Duration.millis(1500));

		pathTransition.setPath(path);
		pathTransition.setOrientation(PathTransition.OrientationType.NONE);
		pathTransition.setCycleCount(1);
		return pathTransition;
	}

}
