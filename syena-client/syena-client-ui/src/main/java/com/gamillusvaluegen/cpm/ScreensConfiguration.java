/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm;

import javafx.animation.FadeTransition;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import com.gamillusvaluegen.cpm.ui.login.LoginController;
import com.gamillusvaluegen.cpm.ui.main.MainScreen;
import com.gamillusvaluegen.cpm.ui.main.MainScreenController;

/**
 *
 * @author hp
 */
public class ScreensConfiguration {

    private Stage primaryStage;

    private Stage currentParentWindow;
    
    private MainScreenController mainScreenController;

    private static FXMLDialog waitCursor;

//    private static String url = "http://52.74.44.39:8080/cpm/";

//    private static String url = "http://localhost:8080/cpm/";

    private static String url = "http://demo.gbs-plus.com/cpm/";

    private static ScreensConfiguration me = new ScreensConfiguration();

    private ScreensConfiguration(){

    }

    public static ScreensConfiguration getInstance(){
        if (null == me){
            me = new ScreensConfiguration();
        }
        return me;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.currentParentWindow = primaryStage;
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        this.primaryStage.setX(bounds.getMinX());
        this.primaryStage.setY(bounds.getMinY());
        this.primaryStage.setWidth(bounds.getWidth() - 140);
        this.primaryStage.setHeight(bounds.getHeight()- 75);
        this.primaryStage.setMaximized(true);
    }

    public void showScreen(Parent screen) {
        Scene scene = new Scene(screen);
        setTransition(screen);
        scene.getStylesheets().add("/styles/Styles.css");
        scene.getStylesheets().add("/styles/schedule.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    private void setTransition( Parent screen){
    	FadeTransition ft = new FadeTransition(Duration.millis(2500), screen);    	
    	ft.setFromValue(0.0);
    	ft.setToValue(1.0);
    	ft.setCycleCount(1);
    	ft.setAutoReverse(true);
    	ft.play();   	
    }

    FXMLDialog loginDialog() {
        return new FXMLDialog(loginController(), getClass().getResource("/fxml/login.fxml"), primaryStage, StageStyle.UNDECORATED,0,0);
    }

    LoginController loginController() {
        return new LoginController(this);
    }

    public MainScreen getMainScreen(){
        MainScreen mainScreen = new MainScreen();
        return mainScreen;
    }

    public double getWidth() {
        return this.primaryStage.getWidth();
    }

    public double getHeight() {
        return this.primaryStage.getHeight();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static String getUrl() {
        return url;
    }

    public Stage getCurrentParentWindow() {
        return currentParentWindow;
    }

    public void setCurrentParentWindow(Stage currentParentWindow) {
        this.currentParentWindow = currentParentWindow;
    }

    public static ScreensConfiguration getMe() {
        return me;
    }

    public static void setMe(ScreensConfiguration me) {
        ScreensConfiguration.me = me;
    }


    public void showCursor(){
//        WaitCursor cursor = new WaitCursor();
//         waitCursor = new FXMLDialog(cursor, getInstance().primaryStage);
//         waitCursor.show();
//          getInstance().primaryStage.getScene().setCursor(Cursor.WAIT);
//        Image image = new Image(getClass().getResource("images/loader.gif").toString());  //pass in the image path
//        getInstance().primaryStage.getScene().setCursor(new ImageCursor(image));
//        FXMLDialog dialog = new FXMLDialog(null, getInstance().primaryStage);
//        waitCursor = new CPMWaitCursor();
//        waitCursor = dialog;
//        waitCursor.show();
//        dialog.show();
    }

    public void hideCursor() {
//         waitCursor.close();
//        getInstance().primaryStage.getScene().setCursor(Cursor.DEFAULT);
    }

    public MainScreenController getMainScreenController() {
        return mainScreenController;
    }

    public void setMainScreenController(MainScreenController mainScreenController) {
        this.mainScreenController = mainScreenController;
    }


}
