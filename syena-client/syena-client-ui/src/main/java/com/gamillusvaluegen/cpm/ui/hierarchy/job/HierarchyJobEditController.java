/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.job;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.PersonSearchField;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.UserManagementService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class HierarchyJobEditController<T extends JobItem> implements DialogController {

	private HierarchyItemPanel jobPanel;

	FXMLDialog dialog;

	private T data;

	@FXML
	private Label levelLabel;
	@FXML
	private TextField nameField;

	@FXML
	private TextArea descriptionField;
	
	@FXML
	private TextField weightageField;
	
	@FXML
	private PersonSearchField ownerField;
	
	@FXML
	private PersonSearchField initiatorField;
	
	@FXML
	private TextField executorField;

	public HierarchyJobEditController(HierarchyItemPanel jobPanel) {
		this.jobPanel = jobPanel;
	}

	public HierarchyJobEditController(HierarchyItemPanel jobPanel, T jobItem) {
		this.jobPanel = jobPanel;
		this.data = jobItem;
	}

	@FXML
	private void initialize() {
		if (null != data) {
			if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
				levelLabel.setText("Job Group");
				JobGroup jobGroup = (JobGroup) data;
				nameField.setText(jobGroup.getName());
				descriptionField.setText(jobGroup.getDescription());
				
			} else {
				levelLabel.setText("Job");
				if (jobPanel.getContextID() == 2){
					StandardJob job = (StandardJob) data;
					nameField.setText(job.getName());
					descriptionField.setText(job.getDescription());
					weightageField.setText(String.valueOf(job.getWeightage()));	
				}else{
					Job job = (Job) data;
					nameField.setText(job.getName());
					descriptionField.setText(job.getDescription());
					weightageField.setText(String.valueOf(job.getWeightage()));	
					if(null != job.getOwner()){
						ownerField.setText(job.getOwner().getId());
					}
					if(null != job.getInitiator()){
						initiatorField.setText(job.getInitiator().getId());
					}
				}
					
//				initiatorField.setText(job.get);
			}

		} else {

			if (jobPanel.getContextID() == 1) {
				levelLabel.setText("Standard Job Group");
			} else if (jobPanel.getContextID() == 3) {
				levelLabel.setText("Job Group - Punch List");
			} else if (jobPanel.getContextID() == 2) {
				levelLabel.setText("Standard Job");
			} else {
				levelLabel.setText("Job - Punch List");
			}

		}

	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;
	}

	@FXML
	public void closeDialog() {
		this.dialog.close();
	}

	@FXML
	public void submitData() {
		try {
			String name = nameField.getText();
			String description = descriptionField.getText();
			double weightage = 0.0;
			if(!Utilities.isNullOrEmpty(weightageField.getText())){
				weightage = Double.valueOf(weightageField.getText());
			}
			String executor = executorField.getText();
			
			String ownerString = ownerField.getText();
			String initiatorStrin = initiatorField.getText();
			Person owner = null;
			Person initiator = null;
			if(!Utilities.isNullOrEmpty(ownerString)){
				owner = UserManagementService.getPerson(ownerString);
			}
			if(!Utilities.isNullOrEmpty(initiatorStrin)){
				initiator = UserManagementService.getPerson(initiatorStrin);
			}
					
			if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
				if (null != data) {
					if (jobPanel.getContextID() == 1) {
						JobGroup<StandardJob> jobGroup = null;
						jobGroup = (JobGroup<StandardJob>) data;
						jobGroup.setName(name);
						jobGroup.setDescription(description);					
						HierarchyJobService.updateStandardJobGroup(jobGroup);
					}else{
						JobGroup<Job> jobGroup = null;
						jobGroup = (JobGroup<Job>) data;
						jobGroup.setName(name);
						jobGroup.setDescription(description);					
						HierarchyJobService.updateJobGroup(jobGroup);
					}
					
				} else {
					if (jobPanel.getContextID() == 1) {
						JobGroup<StandardJob> jobGroup = null;
						jobGroup = new JobGroup<>(name);
						jobGroup.setDescription(description);
						HierarchyJobService.createStandardJobGroup(jobGroup);
					} else {
						JobGroup<Job> jobGroup = null;
						jobGroup = new JobGroup<>(name);
						jobGroup.setDescription(description);
						HierarchyJobService.createJobGroup(jobGroup);
					}
				}

			} else {

				if (null != data) {
					if (jobPanel.getContextID() == 2) {
						StandardJob job = null;
						job = (StandardJob) data;
						job.setName(name);
						job.setDescription(description);
						job.setWeightage(weightage);
						job.validateOnCreate();
						HierarchyJobService.updateStandardJob(job);
					}else{
						Job job = null;
						job = (Job) data;
						job.setName(name);
						job.setDescription(description);
						job.setWeightage(weightage);
						job.setExecutor(executor);
						job.setOwner(owner);
						job.setInitiator(initiator);
						job.validateOnCreate();
						HierarchyJobService.updateJob(job);
					}
					
				} else {

					if (jobPanel.getContextID() == 2) {
						StandardJob job = null;
						job = new StandardJob(name, description);
						job.setWeightage(weightage);
						job.validateOnCreate();
						HierarchyJobService.createStandardJob(job);
					} else {
						Job job = null;
						job = new Job(name, description);
						job.setWeightage(weightage);
						job.setExecutor(executor);
						job.setOwner(owner);
						job.setInitiator(initiator);
						job.validateOnCreate();
						HierarchyJobService.createJob(job);
					}

				}

			}
			this.dialog.close();
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}catch (Exception ex) {
			ex.printStackTrace();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, "Failed to Save the data");
			alertDialog.show();

		}

	}

	@FXML
	private void openFileChooser() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.showOpenDialog(dialog);
	}

}
