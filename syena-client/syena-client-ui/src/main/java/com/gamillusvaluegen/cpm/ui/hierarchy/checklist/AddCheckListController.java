/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.CheckListClientService;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class AddCheckListController implements DialogController {

    private CheckListPane parentPane;

    private FXMLDialog dialog;

    @FXML
    private BorderPane dataPane;

    @FXML
    private GridPane gridPane;

    public AddCheckListController(CheckListPane parentPane) {
        this.parentPane = parentPane;
    }

    @FXML
    private void initialize() {
        addRow(0);
    }

    @FXML
    public void addCheckList() {
        int numRows = gridPane.getRowConstraints().size();
        addRow(numRows);
    }

    private void addRow(int numRows) {
        AddCheckListItemPane editCheckListItemPane = new AddCheckListItemPane(dataPane);

        gridPane.add(editCheckListItemPane, 0, numRows, 3, 1);
        RowConstraints rc = new RowConstraints();
        rc.setVgrow(Priority.ALWAYS);
        gridPane.getRowConstraints().add(rc);
        dataPane.setPrefHeight(dataPane.getPrefHeight() + 25);
    }

    @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    @FXML
    public void SubmitAndclose() {
        try {
            List<CheckList> checkLists = getContent();
            CheckListClientService.createCheckLists(checkLists);
            this.dialog.close();
            parentPane.ShowAllStandardCheckList();
        } catch (ServiceFailedException ex) {
            Message errorMessage = ex.getErroMessage();
            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
            alertDialog.show();
        }
    }

    private List<CheckList> getContent() {
        List<CheckList> checkLists = new ArrayList<>();
        ObservableList<Node> childrens = gridPane.getChildren();
        for (Node node : childrens) {
            if (node instanceof AddCheckListItemPane) {
                AddCheckListItemPane addCheckListItemPane = (AddCheckListItemPane) node;
                checkLists.add(addCheckListItemPane.getCheckList());
            }

        }
        return checkLists;
    }

    @FXML
    public void close() {
        this.dialog.close();
    }

}
