/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.alert.exception;

/**
 *
 * @author hp
 */
public class ExceptionMessages {
    
   
	public static String CHECKLIST_SERVICE_001 = "Failed to Create CheckList";
    public static String CHECKLIST_SERVICE_002 = "Standard checklist retrieval failed";
    public static String CHECKLIST_SERVICE_003 = "Failed to update checklist status";
    public static String CHECKLIST_SERVICE_004 = "Linked checklist retrieval failed";
    
    
    public static String CRITICALPATH_SERVICE_001 = "Failed to retrive CriticalPath";
    public static String CRITICALPATH_SERVICE_002 = "Failed to retrive Non-CriticalPath";
    
    
    public static String DEPENDENCY_SERVICE_001 = "Failed to Add Predecessor Jobs";
    public static String DEPENDENCY_SERVICE_002 = "Failed to Add Predecessor JobGroups";
    public static String DEPENDENCY_SERVICE_003 = "Failed to Add Successor Jobs";
    public static String DEPENDENCY_SERVICE_004 = "Failed to Add Successot JobGroups";
    public static String DEPENDENCY_SERVICE_005 = "Failed to Remove Predecessor Jobs";
    public static String DEPENDENCY_SERVICE_006 = "Failed to Remove Predecessor JobGroups";
    public static String DEPENDENCY_SERVICE_007 = "Failed to Remove Successor Jobs";
    public static String DEPENDENCY_SERVICE_008 = "Failed to Remove Successor Jobs";
    public static String DEPENDENCY_SERVICE_009 = "Failed to Retrive Successor Jobs";
    public static String DEPENDENCY_SERVICE_010 = "Failed to Retrive Predecessor Jobs";
    public static String DEPENDENCY_SERVICE_011 = "Failed to Add Predecessor";
    public static String DEPENDENCY_SERVICE_012 = "Failed to Add Successor";
    public static String DEPENDENCY_SERVICE_013 = "Failed to Retrive Successor Nodes";
    public static String DEPENDENCY_SERVICE_014 = "Failed to Retrive Predecessor Nodes";
    public static String DEPENDENCY_SERVICE_015 = "Failed to Remove Predecessor Nodes";
    public static String DEPENDENCY_SERVICE_016 = "Failed to Remove Successor Nodes";
    
    
    public static String JOB_SERVICE_001 = "Failed to retrive all standard JobGroups";
    public static String JOB_SERVICE_002 = "Failed to retrive all standard Jobs";
    public static String JOB_SERVICE_003 = "Failed to retrive all JobGroups";
    public static String JOB_SERVICE_004 = "Failed to retrive all Jobs";
    public static String JOB_SERVICE_005 = "Failed to retrive checkList Items";
    public static String JOB_SERVICE_006 = "Failed to link Checklists with Job";
    public static String JOB_SERVICE_007 = "Failed to link Checklists with JobGroups";
    public static String JOB_SERVICE_008 = "Failed to remove the Checkists from Job";
    public static String JOB_SERVICE_009 = "Failed to create Standard Job";
    public static String JOB_SERVICE_010 = "Failed to update Standard Job";
    public static String JOB_SERVICE_011 = "Failed to Create Standard JobGroup";
    public static String JOB_SERVICE_012 = "Failed to Update Standard JobGroups";
    public static String JOB_SERVICE_013 = "Failed to retrived the nodes from Jobs";
    public static String JOB_SERVICE_014 = "Failed to retrive the jobs from the nodes";
    public static String JOB_SERVICE_015 = "Failed to retrive the JobGroups from the nodes";
    public static String JOB_SERVICE_016 = "Failed to retrive the Jobs";
    public static String JOB_SERVICE_017 = "Failed to Create Punch List Job";
    public static String JOB_SERVICE_018 = "Failed to Create Punch List JobGroups";
    public static String JOB_SERVICE_019 = "Failed to Update Punch List JobGroups";
    public static String JOB_SERVICE_020 = "Failed to update Punch List Job";
   
    
    public static String NODE_SERVICE_001 = "Failed to retrive linked nodes";
    public static String NODE_SERVICE_002 = "Failed to retrive the linked Standard Nodes";
    public static String NODE_SERVICE_003 = "Failed to retrive the hierarchy level";
    public static String NODE_SERVICE_004 = "Failed to retrive all Standard nodes";
    public static String NODE_SERVICE_005 = "Node search failed in level";
    public static String NODE_SERVICE_006 = "Linked node search failed";
    public static String NODE_SERVICE_007 = "Failed to retrive child nodes";
    public static String NODE_SERVICE_008 = "Failed to retrive child Standard nodes";
    public static String NODE_SERVICE_009 = "Failed to link the JobGroups with node";
    public static String NODE_SERVICE_010 = "Failed to link the Jobs with node";
    public static String NODE_SERVICE_011 = "Failed to add child nodes";
    public static String NODE_SERVICE_012 = "Failed to retrive the linked Jobs";
    public static String NODE_SERVICE_013 = "Failed to retrive the linked JobGroups";
    public static String NODE_SERVICE_014 = "Failed to create standard nodes";
    public static String NODE_SERVICE_015 = "Failed to update standard nodes";
    public static String NODE_SERVICE_016 = "Failed to load the all nodes in a level";
    public static String NODE_SERVICE_017 = "Failed to retrive parent nodes";
    public static String NODE_SERVICE_018 = "Failed to remove the hierarchy link";
    public static String NODE_SERVICE_019 = "Failed to remove the Job link";
    public static String NODE_SERVICE_020 = "Failed to remove the JobGroup link";

    
    
    public static String SCHEDULE_SERVICE_001 = "Failed to schedule Job";
    public static String SCHEDULE_SERVICE_002 = "Failed to Schedule node";
    public static String SCHEDULE_SERVICE_003 = "Failed to Schedule JobGroup";
    public static String SCHEDULE_SERVICE_004 = "Failed to retrive the unapproved hierarchy nodes";
	public static final String SCHEDULE_SERVICE_005 = "Failed to retrive the unapproved JobGroups";
	public static final String SCHEDULE_SERVICE_006 = "Failed to retrive the unapproved Jobs";
	public static final String SCHEDULE_SERVICE_007 = "Failed to reject the schedule";
	
    
    public static String SEARCH_SERVICE_001 = "Failed to retrive on going Jobs";
    public static String SEARCH_SERVICE_002 = "Failed to retrive completed Jobs";
    public static String SEARCH_SERVICE_003 = "Failed to retrive planned Jobs";
    public static String SEARCH_SERVICE_004 = "Failed to retrive delayed Jobs";
    public static String SEARCH_SERVICE_005 = "Failed to retirve the Jobs initiated by the user";
    public static String SEARCH_SERVICE_006 = "Failed to retrive the Jobs owned by the user";
    public static String SEARCH_SERVICE_007 = "Job Search failed";
    
    public static String STATUS_VIEW_SERVICE_001 = "Failed to load the default status charts";
    public static String STATUS_VIEW_SERVICE_002 = "Failed to load the prediction charts";
    
    
    
    public static final String USER_MANAGEMENT_SERVICE_001 = "Login Failed";
	public static final String USER_MANAGEMENT_SERVICE_002 = "Unable to retrive user details";
	
	

	
    
        
    
    
    
    
    
    
}
