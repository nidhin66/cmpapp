/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;


import org.controlsfx.control.spreadsheet.SpreadsheetCellEditor;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;

/**
 *
 * @author hp
 */
public class HierarchyNodeCellType extends SpreadsheetCellType<HierarchyItem>{

    @Override
    public SpreadsheetCellEditor createEditor(SpreadsheetView sv) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString(HierarchyItem t) {
        if(null != t){
            String name = t.getName();
             String desc = t.getDescription();
            if(!Utilities.isNullOrEmpty(name)){
                name = String.format("%-15s", t.getName());
                name = name + ":";
            }
            if(!Utilities.isNullOrEmpty(desc)){
                if(Utilities.isNullOrEmpty(name)){
                     if(desc.length() > 60){                        
                        desc = desc.substring(0, 60); 
                     }
                }else{
                    if(desc.length() > 30){
                        desc = desc.substring(0, 30);
                    }    
                }
                            
            }             
//            return name+desc;
            return "";
        }else{
            return "";
        }
    }

    @Override
    public boolean match(Object o) {
        if(null != o){
            return o instanceof HierarchyItem;
        }
        return true;
      
    }

    @Override
    public StandardHierarchyNode convertValue(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
