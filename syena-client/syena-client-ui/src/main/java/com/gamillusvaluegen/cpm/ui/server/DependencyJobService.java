/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.JobManagementServiceNames;

/**
 *
 * @author hp
 */
public class DependencyJobService {

    public static final String url = ScreensConfiguration.getUrl();
//	private static RestCaller dependencyManagementClient = new RestCaller(url + "/dependencyManagmnet");

    private static RestCaller createRestCallser() {
        RestCaller dependencyManagementClient = new RestCaller(url);
        dependencyManagementClient.path("dependencyManagmnet");
        return dependencyManagementClient;
    }

    public static void addPredecessorJobs(long jobId, List<? extends HierarchyItem> jobs, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.addPredecessorJobs);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != jobs) {
            for (HierarchyItem job : jobs) {
                dependencyManagementClient.param("predecessorJobIds", job.getId());
            }
        } else {
            dependencyManagementClient.param("predecessorJobId", new long[0]);
        }

        try {
        	TypeReference<List<Job>> type = new TypeReference<List<Job>>() {
			};
            dependencyManagementClient.post(null,type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }

    }

    public static void addPredecessorJobGroups(long jobId, List<? extends HierarchyItem> jobGroups, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.addPredecessorJobGroups);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != jobGroups) {
            for (HierarchyItem jobGroup : jobGroups) {
                dependencyManagementClient.param("predecessorJobGroupIds", jobGroup.getId());
            }
        } else {
            dependencyManagementClient.param("predecessorJobGroupIds", new long[0]);
        }

        try {
        	TypeReference<List<JobGroup>> type = new TypeReference<List<JobGroup>>() {
			};
            dependencyManagementClient.post(null,type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_002, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void addSuccessorJobs(long jobId, List<? extends HierarchyItem> jobs, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.addSuccessorJobs);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != jobs) {
            for (HierarchyItem job : jobs) {
                dependencyManagementClient.param("successorJobIds", job.getId());
            }
        } else {
            dependencyManagementClient.param("successorJobIds", new long[0]);
        }
        try {
        	
        	TypeReference<List<Job>> type = new TypeReference<List<Job>>() {
			};
            dependencyManagementClient.post(null,type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_003, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void addSuccessorJobGroups(long jobId, List<? extends HierarchyItem> jobGroups, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.addSuccessorJobGroups);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != jobGroups) {
            for (HierarchyItem jobGroup : jobGroups) {
                dependencyManagementClient.param("successorJobIds", jobGroup.getId());
            }
        } else {
            dependencyManagementClient.param("successorJobIds", new long[0]);
        }
        try {
        	TypeReference<List<JobGroup>> type = new TypeReference<List<JobGroup>>() {
			};
            dependencyManagementClient.post(null,type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_004, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void removePredecessorJobs(long jobId, List<? extends HierarchyItem> predecessorJobs, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.removePredecessorJobs);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != predecessorJobs) {
            for (HierarchyItem predecessorJob : predecessorJobs) {
                dependencyManagementClient.param("predecessorJobIds", predecessorJob.getId());
            }
        } else {
            dependencyManagementClient.param("predecessorJobIds", new long[0]);
        }
        try {
            dependencyManagementClient.post(null);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_005, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void removePredecessorJobGroups(long jobId, List<? extends HierarchyItem> predecessorJobGroups, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.removePredecessorJobGroups);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != predecessorJobGroups) {
            for (HierarchyItem predecessorJobGroup : predecessorJobGroups) {
                dependencyManagementClient.param("predecessorJobGroupIds", predecessorJobGroup.getId());
            }
        } else {
            dependencyManagementClient.param("predecessorJobGroupIds", new long[0]);
        }

        try {
            dependencyManagementClient.post(null);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_006, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void removeSuccessorJobs(long jobId, List<? extends HierarchyItem> jobs, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.removeSuccessorJobs);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != jobs) {
            for (HierarchyItem successorJob : jobs) {
                dependencyManagementClient.param("successorJobIds", successorJob.getId());
            }
        } else {
            dependencyManagementClient.param("successorJobIds", new long[0]);
        }
        try {
            dependencyManagementClient.post(null);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_007, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void removeSuccessorJobGroups(long jobId, List<? extends HierarchyItem> jobs, NodeCategory nodeCategory) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.removeSuccessorJobGroups);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory);
        if (null != jobs) {
            for (HierarchyItem successorJob : jobs) {
                dependencyManagementClient.param("successorJobIds", successorJob.getId());
            }
        } else {
            dependencyManagementClient.param("successorJobIds", new long[0]);
        }

        try {
            dependencyManagementClient.post(null);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_008, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static PageItr<Job> getSuccessorJobs(long jobId, NodeCategory nodeCategory, int pageNumber,int pageSize) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.getSuccessorJobs);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<Job> dependencyJobs = null;
        try {
        	TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
			};
            dependencyJobs = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_009, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return dependencyJobs;
    }

    public static PageItr<Job> getPredecessorJobs(long jobId, NodeCategory nodeCategory ,int pageNumber,  int pageSize) throws ServiceFailedException {
        RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.getPredecessorJobs);
        dependencyManagementClient.param("jobId", jobId).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<Job> dependencyJobs = null;
        try {
        	TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
			};
            dependencyJobs = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_010, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return dependencyJobs;
    }
    
    
    public static PageItr<JobGroup> getSuccessorJobGroups(long id, NodeCategory nodeCategory,int pageNumber,int pageSize) throws ServiceFailedException  {
    	RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.getSuccessorJobGroups);
        dependencyManagementClient.param("id", id).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<JobGroup> dependencyJobGroups = null;
        try {
        	TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			dependencyJobGroups = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_010, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return dependencyJobGroups;
	}


	public static PageItr<JobGroup> getPredecessorJobGroups(long id,NodeCategory nodeCategory,int pageNumber, int pageSize) throws ServiceFailedException  {
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.getPredecessorJobGroups);
        dependencyManagementClient.param("id", id).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<JobGroup> dependencyJobGroups = null;
        try {
        	TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			dependencyJobGroups = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_010, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return dependencyJobGroups;
	}
      
    

	
	public static void addPredecessorHierarchyNodeDependency(long hierarchyNodeId,  List<? extends HierarchyItem>  dependencyNodes) throws ServiceFailedException  {
		 RestCaller dependencyManagementClient = createRestCallser();
	        dependencyManagementClient.path(JobManagementServiceNames.addPredecessorHierarchyNodeDependency);
	        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
	        if (null != dependencyNodes) {
	            for (HierarchyItem dependencyNode : dependencyNodes) {
	                dependencyManagementClient.param("dependencyNodeIds", dependencyNode.getId());
	            }
	        } else {
	            dependencyManagementClient.param("dependencyNodeIds", new long[0]);
	        }

	        try {
	            dependencyManagementClient.post(null);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_011, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }


	}

	
	public static void addSuccessorHierarchyNodeDependency( long hierarchyNodeId, List<? extends HierarchyItem>  dependencyNodes) throws ServiceFailedException{
		 RestCaller dependencyManagementClient = createRestCallser();
	        dependencyManagementClient.path(JobManagementServiceNames.addSuccessorHierarchyNodeDependency);
	        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
	        if (null != dependencyNodes) {
	            for (HierarchyItem dependencyNode : dependencyNodes) {
	                dependencyManagementClient.param("dependencyNodeIds", dependencyNode.getId());
	            }
	        } else {
	            dependencyManagementClient.param("dependencyNodeIds", new long[0]);
	        }

	        try {
	            dependencyManagementClient.post(null);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_012, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }


	}

	
	public static void removePredecessorHierarchyNodeDependency(long hierarchyNodeId,List<? extends HierarchyItem>  dependencyNodes) throws ServiceFailedException{
		 RestCaller dependencyManagementClient = createRestCallser();
	        dependencyManagementClient.path(JobManagementServiceNames.removePredecessorHierarchyNodeDependency);
	        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
	        if (null != dependencyNodes) {
	            for (HierarchyItem dependencyNode : dependencyNodes) {
	                dependencyManagementClient.param("dependencyNodeIds", dependencyNode.getId());
	            }
	        } else {
	            dependencyManagementClient.param("dependencyNodeIds", new long[0]);
	        }

	        try {
	            dependencyManagementClient.post(null);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_015, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }

	}

	
	public static void removeSuccessorHierarchyNodeDependency(long hierarchyNodeId,List<? extends HierarchyItem>  dependencyNodes) throws ServiceFailedException
			{
		 RestCaller dependencyManagementClient = createRestCallser();
	        dependencyManagementClient.path(JobManagementServiceNames.removeSuccessorHierarchyNodeDependency);
	        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
	        if (null != dependencyNodes) {
	            for (HierarchyItem dependencyNode : dependencyNodes) {
	                dependencyManagementClient.param("dependencyNodeIds", dependencyNode.getId());
	            }
	        } else {
	            dependencyManagementClient.param("dependencyNodeIds", new long[0]);
	        }

	        try {
	            dependencyManagementClient.post(null);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_016, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }


	}

	
	public static PageItr<HierarchyNode> getPredecessorHierarchyNodeDependency(long hierarchyNodeId, int pageNumber,int pageSize) throws ServiceFailedException{
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.getPredecessorHierarchyNodeDependency);
        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<HierarchyNode> hierarchyNodes = null;
        try {
        	TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
        	hierarchyNodes = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_014, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return hierarchyNodes;
	}

	
	public static PageItr<HierarchyNode> getSuccessorHierarchyNodeDependency(long hierarchyNodeId, int pageNumber,int pageSize) throws ServiceFailedException
			 {
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.getSuccessorHierarchyNodeDependency);
        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<HierarchyNode> hierarchyNodes = null;
        try {
        	TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
        	hierarchyNodes = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_013, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return hierarchyNodes;
	}
	
	public static PageItr<HierarchyNode> searchPredecessorHierarchyNodeDependency(long hierarchyNodeId,String name,int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.searchPredecessorHierarchyNodeDependency);
        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId).param("name",name).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<HierarchyNode> hierarchyNodes = null;
        try {
        	TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
        	hierarchyNodes = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_013, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return hierarchyNodes;
	}
	
	
	public static PageItr<HierarchyNode> searchSuccessorHierarchyNodeDependency(long hierarchyNodeId, String name,int pageNumber,  int pageSize) throws ServiceFailedException {
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.searchSuccessorHierarchyNodeDependency);
        dependencyManagementClient.param("hierarchyNodeId", hierarchyNodeId).param("name",name).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<HierarchyNode> hierarchyNodes = null;
        try {
        	TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
        	hierarchyNodes = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_013, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return hierarchyNodes;
	}
	

	
	public static PageItr<Job> searchSuccessorJobs(long id,  NodeCategory nodeCategory,String name, int pageNumber,int pageSize) throws ServiceFailedException {
		 RestCaller dependencyManagementClient = createRestCallser();
	        dependencyManagementClient.path(JobManagementServiceNames.searchSuccessorJobs);
	        dependencyManagementClient.param("id", id).param("name", name).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

	        PageItr<Job> dependencyJobs = null;
	        try {
	        	TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
				};
	            dependencyJobs = dependencyManagementClient.get(type);
	        } catch (RestCallException ex) {
	             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_009, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	        return dependencyJobs;
	}

	
	public static PageItr<Job> searchPredecessorJobs(long id,NodeCategory nodeCategory,String name, int pageNumber,int pageSize) throws ServiceFailedException {
		 RestCaller dependencyManagementClient = createRestCallser();
	        dependencyManagementClient.path(JobManagementServiceNames.searchPredecessorJobs);
	        dependencyManagementClient.param("id", id).param("name", name).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

	        PageItr<Job> dependencyJobs = null;
	        try {
	        	TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
				};
	            dependencyJobs = dependencyManagementClient.get(type);
	        } catch (RestCallException ex) {
	             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_009, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	        return dependencyJobs;
	}

	
	public static PageItr<JobGroup> searchSuccessorJobGroups(long id,NodeCategory nodeCategory,String name,int pageNumber, int pageSize) throws ServiceFailedException  {
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.searchSuccessorJobGroups);
        dependencyManagementClient.param("id", id).param("name", name).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<JobGroup> dependencyJobGroups = null;
        try {
        	TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			dependencyJobGroups = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_010, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return dependencyJobGroups;
	}

	
	public static PageItr<JobGroup> searchPredecessorJobGroups( long id, NodeCategory nodeCategory, String name, int pageNumber,int pageSize) throws ServiceFailedException {
		RestCaller dependencyManagementClient = createRestCallser();
        dependencyManagementClient.path(JobManagementServiceNames.searchPredecessorJobGroups);
        dependencyManagementClient.param("id", id).param("name", name).param("nodeCategory", nodeCategory).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<JobGroup> dependencyJobGroups = null;
        try {
        	TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			dependencyJobGroups = dependencyManagementClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_010, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return dependencyJobGroups;
	}


}
