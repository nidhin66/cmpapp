package com.gamillusvaluegen.cpm.ui.server;

//import org.springframework.web.bind.annotation.RequestParam;

import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;
import com.gvg.syena.core.api.services.names.PermissionManagementServiceNames;

public class PermissionClientService {
	public static final String url = ScreensConfiguration.getUrl();

	private static RestCaller createRestCallser() {
		RestCaller permissionManagementClient = new RestCaller(url);
		permissionManagementClient.path("permissionManagement");
		return permissionManagementClient;
	}
	
	public static boolean checkPermission(String userId, String departmentId,ActivityArea activityArea, ActivityOperation activityOperations) throws ServiceFailedException  {
		RestCaller permissionManagementClient = createRestCallser();
		permissionManagementClient.path(PermissionManagementServiceNames.checkPermission);
		permissionManagementClient.param("userId", userId).param("departmentId", departmentId);
		permissionManagementClient.param("activityArea", activityArea).param("activityOperations", activityOperations);
		boolean isPermited;
	        try {
	        	isPermited = permissionManagementClient.post(null, boolean.class);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_001, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
			return isPermited;
	}

}
