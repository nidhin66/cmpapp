package com.gamillusvaluegen.cpm.ui.hierarchy.job;

import java.io.IOException;

import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.custom.PersonSearchField;
import com.gamillusvaluegen.cpm.ui.server.UserManagementService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.exception.InvalidDataException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class JobAttributePane<T extends JobItem> extends BorderPane {
	private NodeCategory category;

	@FXML
	TextField nameField;

	@FXML
	TextField weightageField;

	@FXML
	TextArea descriptionField;

	@FXML
	TextField executorField;

	@FXML
	private PersonSearchField ownerField;

	@FXML
	private PersonSearchField initiatorField;
	
	@FXML
	private CheckBox validateCheckBox;

	private T data;
	
	private String mode;
	
	public JobAttributePane(T data, NodeCategory category, String mode) {
		this.data = data;
		this.category = category;
		this.mode = mode;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/job/job_attribute_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		setItem(data);
	}


	public Object getItem() {
		Object jobItem = null;
		String name = nameField.getText();
		String description = descriptionField.getText();
		double weightage = 0.0;
		if (!Utilities.isNullOrEmpty(weightageField.getText())) {
			weightage = Double.valueOf(weightageField.getText());
		}
		String executor = executorField.getText();

		String ownerString = ownerField.getText();
		String initiatorStrin = initiatorField.getText();
		Person owner = null;
		Person initiator = null;
		if (!Utilities.isNullOrEmpty(ownerString)) {
			owner = ownerField.getPerson();
		}
		if (!Utilities.isNullOrEmpty(initiatorStrin)) {
			initiator = initiatorField.getPerson();
		}

		if (category == NodeCategory.job) {
			Job job = null;
			if (null == data) {
				job = new Job(name, description);
			} else {
				job = (Job) data;
				job.setName(name);
				job.setDescription(description);
			}
			job.setWeightage(weightage);
			job.setExecutor(executor);
			job.setOwner(owner);
			job.setInitiator(initiator);
			job.setApplicableForComplete(validateCheckBox.isSelected());
			try {
				job.validateOnCreate();
			} catch (InvalidDataException e) {
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, e.getMessage());
				alertDialog.show();
			}
			jobItem = job;
		} else {
			JobGroup jobGroup = null;
			if (null == data) {
				jobGroup = new JobGroup<>(name);
			} else {
				jobGroup = (JobGroup) data;
				jobGroup.setName(name);
			}
			jobGroup.setDescription(description);
			jobGroup.setOwner(owner);
			jobGroup.setInitiator(initiator);
			jobGroup.setExecutor(executor);			
			jobGroup.setWeightage(weightage);
			jobGroup.setApplicableForComplete(validateCheckBox.isSelected());
			try {
				jobGroup.validateOnCreate();
			} catch (InvalidDataException e) {
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, e.getMessage());
				alertDialog.show();
			}
			jobItem = jobGroup;
		}

		return jobItem;
	}

	public void clear() {
		nameField.setText("");
		descriptionField.setText("");
		weightageField.setText("");

		ownerField.setText("");

		initiatorField.setText("");

		executorField.setText("");
		validateCheckBox.setSelected(false);
		data = null;
	}

	public void setItem(T jobItem) {
		data = jobItem;
		if (null != data) {
			nameField.setDisable(true);
			descriptionField.setDisable(true);
			if (category == NodeCategory.job) {
				Job job = (Job) data;
				nameField.setText(job.getName());
				descriptionField.setText(job.getDescription());
				weightageField.setText(String.valueOf(job.getWeightage()));
				if (null != job.getOwner()) {
					ownerField.setPerson(job.getOwner());
				}
				if (null != job.getInitiator()) {
					initiatorField.setPerson(job.getInitiator());
				}
				executorField.setText(job.getExecutor());				
				validateCheckBox.setSelected(job.isApplicableForComplete());
			} else {
				JobGroup jobGroup = (JobGroup) data;
				nameField.setText(jobGroup.getName());
				descriptionField.setText(jobGroup.getDescription());
				weightageField.setText(String.valueOf(jobGroup.getWeightage()));
				if (null != jobGroup.getOwner()) {
					ownerField.setPerson(jobGroup.getOwner());
				}
				if (null != jobGroup.getInitiator()) {
					initiatorField.setPerson(jobGroup.getInitiator());
				}
				executorField.setText(jobGroup.getExecutor());
				validateCheckBox.setSelected(jobGroup.isApplicableForComplete());
			}
		} else {
			nameField.setDisable(false);
			descriptionField.setDisable(false);
		}
		if("V".equalsIgnoreCase(mode)){
			nameField.setDisable(true);
			descriptionField.setDisable(true);
			weightageField.setDisable(true);
			ownerField.setDisable(true);
			initiatorField.setDisable(true);
			executorField.setDisable(true);
			validateCheckBox.setDisable(true);
		}
	}

}
