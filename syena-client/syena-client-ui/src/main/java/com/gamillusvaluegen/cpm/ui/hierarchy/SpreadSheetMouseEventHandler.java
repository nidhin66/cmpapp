/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import org.controlsfx.control.spreadsheet.SpreadsheetView;

/**
 *
 * @author hp
 */
public class SpreadSheetMouseEventHandler implements EventHandler<MouseEvent> {

    
     private SpreadsheetView spreadSheetView;
    private HierarchyNodePanel panel;
    
    
    
    public SpreadSheetMouseEventHandler ( HierarchyNodePanel panel,  SpreadsheetView spreadSheetView){
        this.spreadSheetView = spreadSheetView;
        this.panel = panel;
    }
    
    @Override
    public void handle(MouseEvent event) {
        if(event.isPrimaryButtonDown()){
              panel.getChildPanel().showLinkedNodes();
        }
    }
    
}
