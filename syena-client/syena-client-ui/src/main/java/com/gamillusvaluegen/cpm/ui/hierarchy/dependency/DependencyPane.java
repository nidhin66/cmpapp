/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.dependency;

import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;

/**
 *
 * @author hp
 */
public class DependencyPane extends TabPane {

    public DependencyPane() {
        this.setSide(Side.LEFT);
        HierarchyLevel level = ClientConfig.getInstance().getHierarchyLevel();
        
        createItemPanes(level);
        
        DependentJobPane jobDependencyPane = new DependentJobPane(ClientConfig.getInstance().getHierarchyJobPanel(), null);       
        Tab jobTab = new Tab();
        jobTab.setTooltip(new Tooltip("Job"));
        jobTab.getStyleClass().add(ClientConfig.getInstance().getLevelNodeIconStyle("Job"));
        jobTab.setContent(jobDependencyPane);
        jobTab.setClosable(false);
        this.getTabs().add(jobTab);
        this.getSelectionModel().select(jobTab);
        
        this.getStyleClass().add("dependency-item-tab");
       
    }

    private void createItemPanes(HierarchyLevel level) {
        Tab itemTab = new Tab();        
        itemTab.setTooltip(new Tooltip(level.getLevelName()));
        itemTab.getStyleClass().add(ClientConfig.getInstance().getLevelNodeIconStyle(level.getLevelName()));
        DependentNodePane itemPane = new DependentNodePane(ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName()), level);
        itemTab.setContent(itemPane);
        itemTab.setClosable(false);
        
        this.getTabs().add(itemTab);
        for (HierarchyLevel childLevels : level.getChildren()) {
            createItemPanes(childLevels);
        }
    }
    
    public void loadData(){
        for(Tab tab : this.getTabs()){
            DependentItemPane itemPane = (DependentItemPane) tab.getContent();
            itemPane.loadDependentData();
        }
    }

}
