/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.custom;

import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;

/**
 * This class is a TextField which implements an "autocomplete" functionality,
 * based on a supplied list of entries.
 *
 * @author Caleb Brinkman
 */
public class SearchTextField extends TextField {

    /**
     * The existing autocomplete entries.
     */
    private final SortedSet<String> entries;
    /**
     * The popup used to select an entry.
     */
    private ContextMenu entriesPopup;

    private HierarchySearchController controller;
    
    private HierarchyItem hierarchyItem;
    
    private ChangeListener<String> searchListner = null;

    /**
     * Construct
     *
     * @param controller
     */
    public SearchTextField() {
        super();
        
        entries = new TreeSet<>();
        entriesPopup = new ContextMenu();
        searchListner = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                if (getText().length() == 0) {
                    entriesPopup.hide();
                    controller.searchData("");
                } else {
                    List<? extends HierarchyItem> searchResult = controller.searchData(getText());
//          searchResult.addAll(entries.subSet(getText(), getText() + Character.MAX_VALUE));
                    if (null != searchResult && searchResult.size() > 0) {
                        populatePopup(searchResult);
                        if (!entriesPopup.isShowing()) {
                            entriesPopup.show(SearchTextField.this, Side.BOTTOM, 0, 0);
                        }
                    } else {
                        entriesPopup.hide();
                    }
                }
            }
        };
        
        addListner();

        focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean aBoolean2) {
                entriesPopup.hide();
            }
        });

    }
    
    private void addListner(){
    	 textProperty().addListener(searchListner);
    }
    
    private void removeListner(){
    	textProperty().removeListener(searchListner);
    }

    /**
     * Get the existing set of autocomplete entries.
     *
     * @return The existing autocomplete entries.
     */
    public SortedSet<String> getEntries() {
        return entries;
    }

    /**
     * Populate the entry set with the given search results. Display is limited
     * to 10 entries, for performance.
     *
     * @param searchResult The set of matching strings.
     */
    private void populatePopup(List<? extends HierarchyItem> searchResult) {
        List<CustomMenuItem> menuItems = new LinkedList<>();
        // If you'd like more entries, modify this line.
        int maxEntries = 10;
        int count = Math.min(searchResult.size(), maxEntries);
        for (int i = 0; i < count; i++) {
            final HierarchyItem node = searchResult.get(i);
                        String entryLabelString = "";
            if(null != node){
            	if(node.getNodeCategory() == NodeCategory.job || node.getNodeCategory() == NodeCategory.jobgroup){
            		entryLabelString = node.getDescription();
            	}else{
            		entryLabelString = node.getName();
            	}            	
            }
            final String result = entryLabelString;

            Label entryLabel = new Label(result);
            CustomMenuItem item = new CustomMenuItem(entryLabel, true);
            item.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    setText(result);
                    controller.searchData(result);
                    entriesPopup.hide();
                    hierarchyItem = node;
                }
            });
            menuItems.add(item);
        }
        entriesPopup.getItems().clear();
        entriesPopup.getItems().addAll(menuItems);

    }

    public void setController(HierarchySearchController controller) {
        this.controller = controller;
    }

	public HierarchyItem getHierarchyItem() {
		return hierarchyItem;
	}
    
    public void clear(){
    	removeListner();
    	hierarchyItem = null;
    	setText("");
    	addListner();
    }

}
