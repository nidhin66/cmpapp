/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

import com.gvg.syena.core.api.entity.job.checklist.CheckListType;

/**
 *
 * @author hp
 */
public class CheckListContextMenu extends ContextMenu{
    
    CheckListPane checkListPane;
    
       
    public CheckListContextMenu(final CheckListPane checkListPane){
         this.checkListPane = checkListPane;
         this.setAutoHide(true);
         MenuItem cmItem1 = new MenuItem("Add as Initiation CheckList ");
         cmItem1.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                 checkListPane.linkToJob(CheckListType.initiationCheckList);
             }
         });
         
         MenuItem cmItem2 = new MenuItem("Add as Completion CheckList ");
         cmItem2.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                 checkListPane.linkToJob(CheckListType.completionCheckList);
             }
         });
         
         
         MenuItem cmItem3 = new MenuItem("Add as Info CheckList ");
         cmItem3.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                 checkListPane.linkToJob(CheckListType.infoCheckList);
             }
         });        
         
         

        MenuItem cmItem4 = new MenuItem("Remove CheckList");
       
        
        cmItem4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                checkListPane.removeJobLink();
            }
        });
        
        this.getItems().addAll(cmItem1,cmItem2,cmItem3,cmItem4);
        this.getStyleClass().add("checkList-context-menu");
    }
}
