package com.gamillusvaluegen.cpm.ui.diary;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.diary.DiaryThread;

public class DiaryThreadHeader extends GridPane{
	
	@FXML
	private Label topicLabel;
	
	@FXML
	private Label nodeNameLabel;
	
	@FXML
	private Label userNameLabel;
	
	@FXML
	private Label dateLabel;
	
	
	private DiaryThread diaryThread;
	
	public DiaryThreadHeader(DiaryThread diaryThread){
		this.diaryThread = diaryThread;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/diary_thread_header.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		
		topicLabel.setText(diaryThread.getTopic());
		nodeNameLabel.setText("");
		userNameLabel.setText(diaryThread.getCreatedBy());
		nodeNameLabel.setText(diaryThread.getHierarchyName());
		dateLabel.setText(Utilities.dateToString_yyyy_MM_dd(diaryThread.getCreatedDate()));
	}

}
