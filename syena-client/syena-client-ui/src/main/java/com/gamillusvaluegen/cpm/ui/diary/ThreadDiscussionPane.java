package com.gamillusvaluegen.cpm.ui.diary;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;

public class ThreadDiscussionPane extends BorderPane{
	
	private ThreadDiscussion discussion;
	
	@FXML
	private Label nameLabel;
	
	@FXML
	private Label dateLabel;
	
	@FXML
	private TextArea commentField;
	
	@FXML
	private HBox editBox;
	
	private ThreadDiscussionList parentPane;
	

	
	private boolean editMode;
	
	public ThreadDiscussionPane(ThreadDiscussion discussion){		
		this.discussion = discussion;
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/thread_discussion_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		nameLabel.setText(discussion.getUserId());
		dateLabel.setText(Utilities.dateToString_yyyy_MM_dd(discussion.getDate()));
		commentField.setText(discussion.getComment());
		commentField.setEditable(false);
		editBox.setVisible(false);		
	}
	
	public void setEditable(boolean editable){
		commentField.setEditable(editable);
		editBox.setVisible(editable);
		editMode = editable;
	}
	
	@FXML
	private void onSubmit(){
		discussion.setComment(commentField.getText());
		setEditable(false);
		parentPane.submitDiscussion(discussion);
	}
	
	public void setParentPane(ThreadDiscussionList parentPane){
		this.parentPane = parentPane;
	}
	
	@FXML
	private void onCancel(){
		parentPane.cancelReply();
	}

	public boolean isEditMode() {
		return editMode;
	}

}
