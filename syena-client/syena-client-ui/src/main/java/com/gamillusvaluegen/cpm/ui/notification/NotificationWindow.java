package com.gamillusvaluegen.cpm.ui.notification;

import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gvg.syena.core.api.services.notification.NotificationType;

public class NotificationWindow extends BorderPane implements DialogController{
	
	private FXMLDialog dialog;
	NotificationDataPane dataPane = null; 
	
	public NotificationWindow(){
		NotificationWindowHeader header = new NotificationWindowHeader(this);		
		dataPane = new NotificationDataPane();
		this.setTop(header);
		this.setCenter(dataPane);
		this.setMaxHeight(500);
		this.setMaxWidth(1000);
		loadNotificationData(NotificationType.ALERT);
	}

	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;
		
	}
	
	public void closeWindow(){
		this.dialog.close();
	}
	
	public void loadNotificationData(NotificationType type){
		dataPane.loadData(type);
	}

}
