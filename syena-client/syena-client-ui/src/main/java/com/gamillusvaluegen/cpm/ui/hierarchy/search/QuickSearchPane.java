/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.search;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Popup;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.DateFormatConverter;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.ClientMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.DisabledDateCell;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilterUtility;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.JobSearchClientService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.serach.ValueComparitor;

/**
 *
 * @author hp
 */
public class QuickSearchPane extends BorderPane {

	@FXML
	private Button onGoingJobBtn;

	@FXML
	private DatePicker onGoingJobDatePicker;

	@FXML
	private Button completedJobButton;

	@FXML
	private DatePicker completedJobDatePicker;

	@FXML
	private Button plannedJobBtn;

	@FXML
	private DatePicker plannedJobDatePicker;

	@FXML
	private Button delayedJobBtn;

	@FXML
	private DatePicker delayedJobDatePicker;

	@FXML
	private Button initiatedJobBtn;

	@FXML
	private Button ownedJobsBtn;

	@FXML
	private Button cancelButton;
	
	@FXML
	private Button jobsToStartInDaysButton;
	
	@FXML
	private Button jobsToCompleteInDaysButton;


	private Popup popUp;

	public QuickSearchPane() {

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/quick_search.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
//		addTextLimiter(jobsToStartDays, 2);
//		addTextLimiter(jobsToCompleteDays, 2);
	}

	@FXML
	private void initialize() {

		onGoingJobBtn.setTooltip(new Tooltip("Ongoing Jobs"));
		completedJobButton.setTooltip(new Tooltip("Completed Jobs"));
		plannedJobBtn.setTooltip(new Tooltip("Planned Job"));
		delayedJobBtn.setTooltip(new Tooltip("Delayed Jobs"));
		initiatedJobBtn.setTooltip(new Tooltip("Jobs initiated by me"));
		ownedJobsBtn.setTooltip(new Tooltip("Jobs owned by me"));

		onGoingJobDatePicker.setValue(Utilities.dateToLocalDate(Utilities.getDate()));
		onGoingJobDatePicker.setConverter(new DateFormatConverter());
		onGoingJobDatePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DisabledDateCell();
			}
		});

		completedJobDatePicker.setValue(Utilities.dateToLocalDate(Utilities.getDate()));
		completedJobDatePicker.setConverter(new DateFormatConverter());
		completedJobDatePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DisabledDateCell();
			}
		});

		plannedJobDatePicker.setValue(Utilities.dateToLocalDate(Utilities.getDate()));
		plannedJobDatePicker.setConverter(new DateFormatConverter());

		delayedJobDatePicker.setValue(Utilities.dateToLocalDate(Utilities.getDate()));
		delayedJobDatePicker.setConverter(new DateFormatConverter());
		delayedJobDatePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(DatePicker param) {
				return new DisabledDateCell();
			}
		});
		
		jobsToStartInDaysButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_JOBS_TO_START_IN_N_DAYS);
				Popup inputPopup = new Popup();
				FilterInputWindow window = new FilterInputWindow(filter, inputPopup, popUp);
				inputPopup.show(jobsToStartInDaysButton,event.getScreenX() + 10,event.getScreenY()); 
			}
		});
		
		jobsToCompleteInDaysButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_JOBS_TO_COMPLETE_IN_N_DAYS);
				Popup inputPopup = new Popup();
				FilterInputWindow window = new FilterInputWindow(filter, inputPopup, popUp);
				inputPopup.show(jobsToCompleteInDaysButton,event.getScreenX() + 10,event.getScreenY()); 
			}
		});
	}

	public void setPopUp(Popup popUp) {
		this.popUp = popUp;
	}

	@FXML
	private void cancelAction() {
		this.popUp.hide();
	}

	@FXML
	private void onGoingJobs() {

		if (null != onGoingJobDatePicker.getValue()) {
			try {

				HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_ON_GOING_JOBS);
				Date date = Utilities.LocalDateToDate(onGoingJobDatePicker.getValue());
				filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE, date);
				PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0, 30,
						filter);
				HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
				if (null != popUp) {
					this.popUp.hide();
				}

			} catch (ServiceFailedException ex) {
				Message errorMessage = ex.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}
		}

	}

	@FXML
	private void completedJobs() {
		if (null != completedJobDatePicker.getValue()) {
			try {

				HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_COMPLETED_JOBS);
				Date date = Utilities.LocalDateToDate(completedJobDatePicker.getValue());
				filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE, date);
				PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
						CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
				HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
				if (null != popUp) {
					this.popUp.hide();
				}

			} catch (ServiceFailedException ex) {
				Message errorMessage = ex.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}
		}

	}

	@FXML
	private void plannedJobs() {
		if (null != plannedJobDatePicker.getValue()) {
			try {
				HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_PLANNED_JOBS);
				Date date = Utilities.LocalDateToDate(plannedJobDatePicker.getValue());
				filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE, date);
				PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
						CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
				HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
				if (null != popUp) {
					this.popUp.hide();
				}

			} catch (ServiceFailedException ex) {
				Message errorMessage = ex.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}
		}

	}

	@FXML
	private void delayedJobs() {
		if (null != delayedJobDatePicker.getValue()) {
			try {

				HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_DELAYED_JOBS);
				Date date = Utilities.LocalDateToDate(delayedJobDatePicker.getValue());
				filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE, date);
				PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
						CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
				HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
				if (null != popUp) {
					this.popUp.hide();
				}

			} catch (ServiceFailedException ex) {
				Message errorMessage = ex.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}
		}

	}

	@FXML
	private void jobsInitiated() {
		try {
			Person person = ClientConfig.getInstance().getLoginUser();

			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_INITIATED_JOBS);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERSON_ID, person.getId());

			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@FXML
	private void jobsOwned() {
		try {
			Person person = ClientConfig.getInstance().getLoginUser();

			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_OWNED_JOBS);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERSON_ID, person.getId());

			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@FXML
	private void jobsConsistentlyDelayed() {
		try {
			HierarchyItem plant = ClientConfig.getInstance().getPlant();

			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_CONSISTANTLY_DELAYED_ACTIVITY);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID, plant.getId());
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR, ValueComparitor.greater);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE, 25);

			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@FXML
	private void jobsConsistentlyOverrun() {
		try {
			HierarchyItem plant = ClientConfig.getInstance().getPlant();

			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_CONSISTANTLY_OVERRUN_ACTIVITY);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID, plant.getId());
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR, ValueComparitor.greater);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE, 25);

			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@FXML
	private void jobsInCriticalPath() {
		try {
			HierarchyItem plant = ClientConfig.getInstance().getPlant();

			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_CRITICAL_PATH_JOBS);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID, plant.getId());

			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}

	}

	@FXML
	private void jobsWithRevisedDate() {
		try {

			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_REVISED_DATE_JOBS);

			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}

	}

//	@FXML
//	private void jobsWithRevisedEstimate() {
//		HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_REVISED_ESTIMATE);
//	}

	@FXML
	private void jobsToSchedule() {
		try {
			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_JOBS_TO_SCHEDULE);
			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}

	}

	@FXML
	private void systemConsistentlyDelays() {
		PageItr<? extends HierarchyItem> hierarchyNodes = null;
		try {
			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_CONSISTENTLY_DELAYED_SYSTEM);
			String levelName = "System";
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_LEVEL_NAME, levelName);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR, ValueComparitor.greater);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE, 25);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE, Utilities.getDate());

			hierarchyNodes = HierarchyFilterUtility.getPage(NodeCategory.hierarchy, 0,
					CPMClientConstants.SYSTEM_PANEL_COLUMN_COUNT * CPMClientConstants.SYSTEM_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadHierarchyPanel(CPMClientConstants.CONTEXT_LINKED, filter, levelName, hierarchyNodes);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}

	}

	@FXML
	private void unitsConsistentlyDelays() {
		PageItr<? extends HierarchyItem> hierarchyNodes = null;
		try {
			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_CONSISTENTLY_DELAYED_UNIT);
			String levelName = "Unit";

			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_LEVEL_NAME, levelName);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR, ValueComparitor.greater);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE, 25);
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE, Utilities.getDate());

			hierarchyNodes = HierarchyFilterUtility.getPage(NodeCategory.hierarchy, 0,
					CPMClientConstants.UNIT_PANEL_COLUMN_COUNT * CPMClientConstants.UNIT_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadHierarchyPanel(CPMClientConstants.CONTEXT_LINKED, filter, levelName, hierarchyNodes);
			if (null != popUp) {
				this.popUp.hide();
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}
	
//	@FXML
//	private void jobsToStartInNDays(){		
//		PageItr<? extends HierarchyItem> hierarchyNodes = null;
//		try {
//			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_JOBS_TO_START_IN_N_DAYS);
//			
//			if (null != popUp) {
//				this.popUp.hide();
//			}
//		}catch (ServiceFailedException ex) {
//			Message errorMessage = ex.getErroMessage();
//			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
//			alertDialog.show();
//		}
//	}
//	
//	@FXML
//	private void jobsToCompleteInNDays(){		
//		PageItr<? extends HierarchyItem> hierarchyNodes = null;
//		try {
//			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.QUICK_JOBS_TO_COMPLETE_IN_N_DAYS);			
//			
//			if (null != popUp) {
//				this.popUp.hide();
//			}
//		}catch (ServiceFailedException ex) {
//			Message errorMessage = ex.getErroMessage();
//			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
//			alertDialog.show();
//		}
//	}
	
	

	

	public void disableCancel() {
		cancelButton.setVisible(false);
	}
	
	

}
