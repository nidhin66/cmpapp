/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.scene.control.TreeItem;

import org.controlsfx.control.BreadCrumbBar;

import com.gamillusvaluegen.cpm.ui.hierarchy.schedule.ScheduleMode;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.ScheduleManagementService;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

/**
 *
 * @author hp
 */
public abstract class HierarchyItemPanel {
    
	protected int rowCount;
	protected int columnCount;

    protected NodeCategory nodeType;
    protected HierarchyItemPanel parentPanel;
    protected HierarchyItemPanel childPanel;
    protected HierarchyLevel level;
    protected int contextID;
    
    protected boolean approved;

    protected boolean standardHierarchy = false;
    
    protected HierarchyFilter currentFilter;
    
    protected HierarchyNodePanelController controller;
//    public abstract void showData(int contextID);

    public abstract void showAllStandardHierarchyNodes();

    public abstract void showLinkedNodes();

    public abstract void loadFirstPage();

    public abstract void loadLastPage();

    public abstract void loadNextPage();

    public abstract void loadPreviousPage();

    public abstract List<? extends HierarchyItem> searchData(String text);

    public abstract HierarchyItem getSelectedItem();

    public abstract long[] getItemIds();

    public abstract void linkToParent(HierarchyItem parentItem);

    public abstract List<? extends HierarchyItem> getSelectedItems();
    
    public abstract void loadFilterData(int context,HierarchyFilter filter,PageItr<? extends HierarchyItem> hierachyNodesPage);
    
    public abstract void loadData(PageItr<? extends HierarchyItem> hierachyNodesPage,boolean loadChild);
     
    public abstract void loadParentPanelData();
    
   

//    public abstract void showDefaultContextData();
    public abstract void setJobPanel(HierarchyJobPanel jobPanel);
    
    public abstract void schedule(ScheduleMode mode);
    
    public abstract void approveSchedule();
    
    public abstract void refreshPage();
    
    public abstract void removeHierarchyLink();
    
    public void clearSearch(){
    	this.controller.clearSearch();
    }
    
    public void loadUnApprovedData(){    	
    	 PageItr<HierarchyNode> nodes = null;
    	try {
    		nodes = ScheduleManagementService.getUnApprovedHierarchySchedules(level.getLevelName(), 0, rowCount*columnCount);			
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	loadData(nodes, true);
    	if(null != childPanel){
			childPanel.loadUnApprovedData();
		}
    }

    public List<JobGroup> getParentJobGroups() {
        List<JobGroup> jobGroups = new ArrayList<>();
        HierarchyItem selectedItem = getSelectedItem();
        HierarchyNode selectedhierarchyNode = null;
        if (null != selectedItem) {
            if (selectedItem instanceof HierarchyNode) {
                selectedhierarchyNode = (HierarchyNode) selectedItem;
                jobGroups.addAll(selectedhierarchyNode.getJobGroups());
            }

        } else {
            HierarchyItem[] hierarchyItems = getItems();
            for (HierarchyItem hierarchyItem : hierarchyItems) {
                if (hierarchyItem instanceof HierarchyNode) {
                    selectedhierarchyNode = (HierarchyNode) hierarchyItem;
                    jobGroups.addAll(selectedhierarchyNode.getJobGroups());
                }
            }
            if (null != parentPanel) {
                jobGroups.addAll(parentPanel.getParentJobGroups());
            }

        }

        return jobGroups;
    }
    
    
    public List<Long> getParentids() {
       List<Long> parentIds = new ArrayList<>();
       HierarchyItem selectedItem = getSelectedItem();
       if (null != selectedItem) {
           parentIds.add(selectedItem.getId());
       }else{
            HierarchyItem[] hierarchyItems = getItems();
            for (HierarchyItem hierarchyItem : hierarchyItems) {
                parentIds.add(hierarchyItem.getId());
            }
            if (null != parentPanel) {
                 parentIds.addAll(parentPanel.getParentids());
            }
       }
       return parentIds;
    }


    public List<Job> getParentJobs() {
        List<Job> jobs = new ArrayList<>();
        HierarchyItem selectedItem = getSelectedItem();
        HierarchyNode selectedhierarchyNode = null;
        if (null != selectedItem) {
            if (selectedItem instanceof HierarchyNode) {
                selectedhierarchyNode = (HierarchyNode) selectedItem;
                jobs.addAll(selectedhierarchyNode.getJobs());
            }

        } else {
            HierarchyItem[] hierarchyItems = getItems();
            for (HierarchyItem hierarchyItem : hierarchyItems) {
                if (hierarchyItem instanceof HierarchyNode) {
                    selectedhierarchyNode = (HierarchyNode) hierarchyItem;
                    jobs.addAll(selectedhierarchyNode.getJobs());
                }
            }
            if (null != parentPanel) {
                jobs.addAll(parentPanel.getParentJobs());
            }

        }

        return jobs;
    }

    public HierarchyItemPanel getParentPanel() {
        return parentPanel;
    }

    public void setParentPanel(HierarchyItemPanel parentPanel) {
        this.parentPanel = parentPanel;
    }

    public HierarchyItemPanel getChildPanel() {
        return childPanel;
    }

    public void setChildPanel(HierarchyItemPanel childPanel) {
        this.childPanel = childPanel;
    }

    public abstract HierarchyItem[] getItems();

    public abstract void loadInitialData();

    public Map<String, HierarchyItem> getSelectedItemFromAllNodes() {
        Map<String, HierarchyItem> hierarchyItems = new HashMap<>();
        if (null != parentPanel) {
            hierarchyItems = parentPanel.getSelectedItemFromAllNodes();
        }
        HierarchyItem item = getSelectedItem();
        if (null != item) {
            hierarchyItems.put(level.getLevelName(), item);
        }
        return hierarchyItems;
    }
    
    public HierarchyItem getImmediateSelectedParent(){    	
    	HierarchyItem item = null ;
    	if(parentPanel != null){
			item = parentPanel.getSelectedItem();
			if(item == null){
				item = parentPanel.getImmediateSelectedParent();
			}
		}
    	return item;
    }

    public HierarchyLevel getLevel() {
        return level;
    }

    public boolean isStandardHierarchy() {
        return standardHierarchy;
    }

    public int getContextID() {
       
        return contextID;
    }

    public BreadCrumbBar<String> createHierarchyBreadCrumb() {
        BreadCrumbBar<String> sampleBreadCrumbBar = null;

        sampleBreadCrumbBar = ClientConfig.getInstance().getBreadCrumbBar();
        Set set = new LinkedHashSet();
        getSelectedItemSet(set, ClientConfig.getInstance().getHierarchyLevel());
//        HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
//        HierarchyItem jobItem = jobPanel.getSelectedItem();
//        if(null != jobItem){
//            set.add(jobItem.getName());
//        }
        
        TreeItem<String> subRoot = null;
        for (Iterator<String> it = set.iterator(); it.hasNext();) {
            TreeItem<String> currentNode = new TreeItem<>(it.next());
            if (subRoot == null) {
                subRoot = currentNode;
            } else {
                subRoot.getChildren().add(currentNode);
                subRoot = currentNode;
            }
        }
        if (null != sampleBreadCrumbBar) {

            sampleBreadCrumbBar.setSelectedCrumb(subRoot);

        } else {
            sampleBreadCrumbBar = new BreadCrumbBar<>(subRoot);
            sampleBreadCrumbBar.setAutoNavigationEnabled(false);

        }

        ClientConfig.getInstance().setBreadCrumbBar(sampleBreadCrumbBar);
        return sampleBreadCrumbBar;
    }

    private void getSelectedItemSet(Set<String> set, HierarchyLevel level) {
        HierarchyItemPanel itemPanel = ClientConfig.getInstance().getHierarchyItemPanel(level.getLevelName());
        HierarchyItem selectedItem = itemPanel.getSelectedItem();
      
        if (selectedItem != null) {
           set.add(selectedItem.getName());
        }
        for (HierarchyLevel childLevel : level.getChildren()) {
            getSelectedItemSet(set, childLevel);
        }

    }

	public boolean isApproved() {
		return approved;
	}

	public int getRowCount() {
		return rowCount;
	}

	public int getColumnCount() {
		return columnCount;
	}

	public NodeCategory getNodeType() {
		return nodeType;
	}
    
    

}
