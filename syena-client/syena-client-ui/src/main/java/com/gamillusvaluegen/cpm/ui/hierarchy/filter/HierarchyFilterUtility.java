package com.gamillusvaluegen.cpm.ui.hierarchy.filter;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.JobGroupSearchClientService;
import com.gamillusvaluegen.cpm.ui.server.JobSearchClientService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;

public class HierarchyFilterUtility {
	
	public static String QUICK_ON_GOING_JOBS = "QUICK_ON_GOING_JOBS";
	public static String QUICK_COMPLETED_JOBS = "QUICK_COMPLETED_JOBS";
	public static String QUICK_PLANNED_JOBS = "QUICK_PLANNED_JOBS";
	public static String QUICK_DELAYED_JOBS = "QUICK_DELAYED_JOBS";
	public static String QUICK_INITIATED_JOBS = "QUICK_INITIATED_JOBS";
	public static String QUICK_OWNED_JOBS = "QUICK_OWNED_JOBS";
	public static String QUICK_CONSISTANTLY_DELAYED_ACTIVITY = "QUICK_CONSISTANTLY_DELAYED_ACTIVITY";
	public static String QUICK_CRITICAL_PATH_JOBS = "QUICK_CRITICAL_PATH_JOBS";
	public static String QUICK_CONSISTANTLY_OVERRUN_ACTIVITY = "QUICK_CONSISTANTLY_OVERRUN_ACTIVITY";
	public static String QUICK_REVISED_DATE_JOBS = "QUICK_REVISED_DATE_JOBS";
	public static String QUICK_REVISED_ESTIMATE = "QUICK_REVISED_ESTIMATE";
	public static String QUICK_JOBS_TO_SCHEDULE = "QUICK_JOBS_TO_SCHEDULE";
	public static String QUICK_CONSISTENTLY_DELAYED_SYSTEM = "QUICK_CONSISTENTLY_DELAYED_SYSTEM";
	public static String QUICK_CONSISTENTLY_DELAYED_UNIT = "QUICK_CONSISTENTLY_DELAYED_UNIT";
	public static String QUICK_JOBS_TO_START_IN_N_DAYS = "QUICK_JOBS_TO_START_IN_N_DAYS";
	public static String QUICK_JOBS_TO_COMPLETE_IN_N_DAYS = "QUICK_JOBS_TO_COMPLETE_IN_N_DAYS";
	public static String ADVANCED_SEARCH_JOB = "ADVANCED_SEARCH_JOB";
	
	public static String SEARCH_ATTRIBUTE_DATE = "SEARCH_ATTRIBUTE_DATE";
	public static String SEARCH_ATTRIBUTE_PERSON_ID = "SEARCH_ATTRIBUTE_PERSON_ID";
	public static String SEARCH_ATTRIBUTE_PLANT_ID = "SEARCH_ATTRIBUTE_PLANT_ID";
	public static String SEARCH_ATTRIBUTE_LEVEL_NAME = "SEARCH_ATTRIBUTE_LEVEL_NAME";
	public static String SEARCH_ATTRIBUTE_VALUE_COMPARATOR = "SEARCH_ATTRIBUTE_VALUE_COMPARATOR";
	public static String SEARCH_ATTRIBUTE_PERCENTAGE = "SEARCH_ATTRIBUTE_PERCENTAGE";
	public static String SEARCH_ATTRIBUTE_DAYS = "SEARCH_ATTRIBUTE_DAYS";
	
	private static Map<String,String> toolTips = new HashMap<String, String>();
	
	static{
		toolTips.put(QUICK_CONSISTENTLY_DELAYED_SYSTEM, "System consistently delayed");
		toolTips.put(QUICK_CONSISTENTLY_DELAYED_UNIT, "Unit consistently delayed");
		toolTips.put(QUICK_ON_GOING_JOBS, "Ongoing Jobs");
		toolTips.put(QUICK_COMPLETED_JOBS, "Completed Jobs");
		toolTips.put(QUICK_PLANNED_JOBS, "Planned Jobs");
		toolTips.put(QUICK_DELAYED_JOBS, "Delayed Jobs");
		toolTips.put(QUICK_INITIATED_JOBS, "Jobs Initiated By Me");
		toolTips.put(QUICK_OWNED_JOBS, "Jobs Owned By Me");
		toolTips.put(QUICK_CONSISTANTLY_DELAYED_ACTIVITY, "Activities consistently delayed");
		toolTips.put(QUICK_CONSISTANTLY_OVERRUN_ACTIVITY, "Activities consistently overrun");
		toolTips.put(QUICK_CRITICAL_PATH_JOBS, "Jobs In Critical Path");
		toolTips.put(QUICK_REVISED_DATE_JOBS, "Jobs In Revised Date");
		toolTips.put(QUICK_JOBS_TO_SCHEDULE, "Jobs to schedule");
		toolTips.put(QUICK_JOBS_TO_START_IN_N_DAYS, "Jobs to start in days");
		toolTips.put(QUICK_JOBS_TO_COMPLETE_IN_N_DAYS, "Jobs to complete in days");
	}
	
	
	public static PageItr<? extends HierarchyItem> getPage(NodeCategory nodeCategory,int pageNumber,int pageSize, HierarchyFilter filter) throws ServiceFailedException {
		PageItr<? extends HierarchyItem> pageData = null;
		if(nodeCategory == NodeCategory.job){
			pageData = executeJobSearch(pageNumber, pageSize, filter);
		}else if(nodeCategory == NodeCategory.jobgroup){
			pageData = executeJobGroupSearch(pageNumber, pageSize, filter);
		}else {
			pageData = executeHierarchyNodeSearch(pageNumber, pageSize, filter);
		}
		return pageData;
	}
	
	private static PageItr<? extends HierarchyItem> executeHierarchyNodeSearch(int pageNumber,int pageSize, HierarchyFilter filter) throws ServiceFailedException{
		PageItr<? extends HierarchyItem> pageData = null;
		if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTENTLY_DELAYED_SYSTEM)){
			
			String levelName = (String) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_LEVEL_NAME);
			ValueComparitor valueComparitor = (ValueComparitor) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR);
			int percentage = (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE);
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			
			pageData = JobSearchClientService.nodeWithDelayed(filter.getSearchString(),levelName, date, percentage, valueComparitor, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTENTLY_DELAYED_UNIT)){
			
			String levelName = (String) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_LEVEL_NAME);
			ValueComparitor valueComparitor = (ValueComparitor) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR);
			int percentage = (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE);
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			
			pageData = JobSearchClientService.nodeWithDelayed(filter.getSearchString(),levelName, date, percentage, valueComparitor, pageNumber, pageSize);
		}
		return pageData;
	}
	
	private static PageItr<? extends HierarchyItem> executeJobGroupSearch(int pageNumber,int pageSize, HierarchyFilter filter) throws ServiceFailedException{
		PageItr<? extends HierarchyItem> pageData = null;
		if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_ON_GOING_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobGroupSearchClientService.getOnGoingJobGroups(date,filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_COMPLETED_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobGroupSearchClientService.completedJobGroups(date,filter.getSearchString(),  pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_PLANNED_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobGroupSearchClientService.plannedJobGroups(date,filter.getSearchString(),  pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_DELAYED_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobGroupSearchClientService.delayedJobGroups(date,filter.getSearchString(),  pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_INITIATED_JOBS)){
			String personId = (String) filter.getParameterValue(SEARCH_ATTRIBUTE_PERSON_ID);
			pageData = JobGroupSearchClientService.jobsInitiatedBy(personId,filter.getSearchString(),  pageNumber, pageSize);			
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_OWNED_JOBS)){
			String personId = (String) filter.getParameterValue(SEARCH_ATTRIBUTE_PERSON_ID);
			pageData = JobGroupSearchClientService.jobsOwnedBy(personId, filter.getSearchString(), pageNumber, pageSize);	
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTANTLY_DELAYED_ACTIVITY)){
			
			long hierarchyNodeId = (long) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID);
			ValueComparitor valueComparitor = (ValueComparitor) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR);
			int percentage = (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE);
			
    		pageData = JobGroupSearchClientService.jobGroupwithDelyedStartJobs(filter.getSearchString(), hierarchyNodeId, valueComparitor, percentage, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTANTLY_OVERRUN_ACTIVITY)){
			
			long hierarchyNodeId = (long) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID);
			ValueComparitor valueComparitor = (ValueComparitor) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR);
			int percentage = (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE);
    		
			pageData = JobGroupSearchClientService.jobGroupwithIncreasedDurationJobs(filter.getSearchString(), hierarchyNodeId, valueComparitor, percentage, pageNumber, pageSize);
			
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CRITICAL_PATH_JOBS)){
			
			long hierarchyNodeId = (long) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID);			
			pageData = JobGroupSearchClientService.jobGroupsInCriticalPath(filter.getSearchString(), hierarchyNodeId, pageNumber, pageSize);
			
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_REVISED_DATE_JOBS)){
			pageData = JobGroupSearchClientService.jobsWithRevisedDates(filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_SCHEDULE)){
			pageData = JobGroupSearchClientService.jobsToBeScheduled(filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_START_IN_N_DAYS)){
			int days =  (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS);
			Date fromDate = Utilities.getDate();
			Date toDate = Utilities.nextDate(fromDate, days);
			pageData = JobGroupSearchClientService.jobsGroupsStartInNDays(filter.getSearchString(), fromDate, toDate, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_COMPLETE_IN_N_DAYS)){
			int days =  (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS);
			Date fromDate = Utilities.getDate();
			Date toDate = Utilities.nextDate(fromDate, days);
			pageData = JobGroupSearchClientService.jobsGroupsCompleteInNDays(filter.getSearchString(), fromDate, toDate, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(ADVANCED_SEARCH_JOB)){
			if(!Utilities.isNullOrEmpty(filter.getSearchString())){
				filter.getJobProperties().setDescription(filter.getSearchString());
			}
			pageData = JobGroupSearchClientService.searchJobs(filter.getJobProperties(), pageNumber,pageSize);
		}
		return pageData;
	}
	
	
	
	
	private static PageItr<? extends HierarchyItem> executeJobSearch(int pageNumber,int pageSize, HierarchyFilter filter) throws ServiceFailedException{
		PageItr<? extends HierarchyItem> pageData = null;
		if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_ON_GOING_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobSearchClientService.onGoingJobs(date,filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_COMPLETED_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobSearchClientService.completedJobs(date,filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_PLANNED_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobSearchClientService.plannedJobs(date,filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_DELAYED_JOBS)){
			Date date = (Date) filter.getParameterValue(SEARCH_ATTRIBUTE_DATE);
			pageData = JobSearchClientService.delayedJobs(date,filter.getSearchString(), pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_INITIATED_JOBS)){
			String personId = (String) filter.getParameterValue(SEARCH_ATTRIBUTE_PERSON_ID);
			pageData = JobSearchClientService.jobsInitiatedBy(personId,filter.getSearchString(), pageNumber, pageSize);			
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_OWNED_JOBS)){
			String personId = (String) filter.getParameterValue(SEARCH_ATTRIBUTE_PERSON_ID);
			pageData = JobSearchClientService.jobsOwnedBy(personId,filter.getSearchString(), pageNumber, pageSize);	
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTANTLY_DELAYED_ACTIVITY)){
			
			long hierarchyNodeId = (long) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID);
			ValueComparitor valueComparitor = (ValueComparitor) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR);
			int percentage = (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE);
			
    		pageData = JobSearchClientService.JobsInDelyedStartJobGroups(filter.getSearchString(),hierarchyNodeId, valueComparitor, percentage, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTANTLY_OVERRUN_ACTIVITY)){
			
			long hierarchyNodeId = (long) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID);
			ValueComparitor valueComparitor = (ValueComparitor) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_VALUE_COMPARATOR);
			int percentage = (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PERCENTAGE);
    		
			pageData = JobSearchClientService.JobsInIncreasedDurationJobGroups(filter.getSearchString(),hierarchyNodeId, valueComparitor, percentage, pageNumber, pageSize);
			
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CRITICAL_PATH_JOBS)){
			
			long hierarchyNodeId = (long) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_PLANT_ID);			
			pageData = JobSearchClientService.jobsInCriticalPath(filter.getSearchString(),hierarchyNodeId, pageNumber, pageSize);
			
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_REVISED_DATE_JOBS)){
			pageData = JobSearchClientService.jobsWithRevisedDates(filter.getSearchString(),pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_SCHEDULE)){
			pageData = JobSearchClientService.jobsToBeScheduled(filter.getSearchString(),pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_START_IN_N_DAYS)){
			int days =  (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS);
			Date fromDate = Utilities.getDate();
			Date toDate = Utilities.nextDate(fromDate, days);
			pageData = JobSearchClientService.jobsStartInNDays(filter.getSearchString(), fromDate, toDate, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_COMPLETE_IN_N_DAYS)){
			int days =  (int) filter.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS);
			Date fromDate = Utilities.getDate();
			Date toDate = Utilities.nextDate(fromDate, days);
			pageData = JobSearchClientService.jobsCompleteInNDays(filter.getSearchString(), fromDate, toDate, pageNumber, pageSize);
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(ADVANCED_SEARCH_JOB)){
			if(!Utilities.isNullOrEmpty(filter.getSearchString())){
				filter.getJobProperties().setDescription(filter.getSearchString());
			}
			pageData = JobSearchClientService.searchJobs(filter.getJobProperties(), pageNumber,pageSize);
		}
		return pageData;
	}
	
	public static String getStyleClass(HierarchyFilter filter){
		String style = "";
		if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTANTLY_DELAYED_ACTIVITY)){
			style = "filter-jobs-consistant-delay";
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTANTLY_OVERRUN_ACTIVITY)){
			style = "filter-jobs-consistant-overrun";
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CRITICAL_PATH_JOBS)){
			style = "filter-jobs-critical-path";
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_REVISED_DATE_JOBS)){
			style = "filter-jobs-revised-date";
		}else  if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_REVISED_ESTIMATE)){
			style = "filter-jobs-revised-estimate";
		}else  if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_SCHEDULE)){
			style = "filter-jobs-to-schedule";
		}else  if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTENTLY_DELAYED_SYSTEM)){
			style = "filter-system-consistant-delay";
		}else  if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_CONSISTENTLY_DELAYED_UNIT)){
			style = "filter-unit-consistant-delay";
		}else if (filter.getFilterIdentifier().equalsIgnoreCase(QUICK_OWNED_JOBS)){
			style = "filter-ownedjobs";
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_INITIATED_JOBS)){
			style = "filter-initiatedjobs";
		}else if(filter.getFilterIdentifier().equalsIgnoreCase(QUICK_DELAYED_JOBS)){
			style = "filter-delayed";
		}else if (filter.getFilterIdentifier().equalsIgnoreCase(QUICK_PLANNED_JOBS)){
			style = "filter-planned";
		}else if (filter.getFilterIdentifier().equalsIgnoreCase(QUICK_COMPLETED_JOBS)){
			style = "filter-completed";
		}else if (filter.getFilterIdentifier().equalsIgnoreCase(QUICK_ON_GOING_JOBS)){
			style = "filter-ongoing";
		}else if (filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_START_IN_N_DAYS)){
			style = "filter-job-start-in-days";
		}else if (filter.getFilterIdentifier().equalsIgnoreCase(QUICK_JOBS_TO_COMPLETE_IN_N_DAYS)){
			style = "filter-job-complete-in-days";
		}
		return style;
	}
	
    public static void clearPanelFilter(){
    	List<HierarchyItemPanel> panels = ClientConfig.getInstance().getAllHierarchyPanels();
    	if(null != panels){
    		for(HierarchyItemPanel itemPanel : panels){
    			itemPanel.clearSearch();
    		}
    	}
    	HierarchyItemPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
    	jobPanel.clearSearch();
    }
    
    public static Popup getPopup(HierarchyFilter filter){
    	Popup popup = null;
    	if(null != filter.getJobProperties()){
    		popup = new Popup();  
    		popup.setAutoHide(true);
    		VBox vbBox = getPropertyList(filter.getJobProperties());
//    		popup.setHeight(vbBox.getHeight() + 100);
    		BorderPane pane = new BorderPane();
    		pane.setStyle("-fx-background-color:#624D5F;-fx-border-radius:10 10 10 10;");
    		pane.setCenter(vbBox);
    		if(null != vbBox.getChildren()){
    			pane.setPrefHeight(vbBox.getChildren().size() * 25);
        		pane.setPrefWidth(250);
    		}       		
    		popup.getContent().add(pane);
    	}else{
    		popup = new Popup();  
    		popup.setAutoHide(true);
    		Label toolTipLabel = getSearchToolTip(filter);
    		BorderPane pane = new BorderPane();
    		pane.setStyle("-fx-background-color:#624D5F;-fx-border-radius:10 10 10 10;");
    		pane.setCenter(toolTipLabel);
    		pane.setPrefWidth(200);
    		popup.getContent().add(pane);
    	}
    	return popup;
    }
    
    private static Label getSearchToolTip(HierarchyFilter filter){
    	Label toolTipLabel = new Label();
    	toolTipLabel.setStyle("-fx-font-size: 12px;-fx-text-fill:#C4C4C4;");
    	String toolTipString = toolTips.get(filter.getFilterIdentifier());
    	toolTipLabel.setText(toolTipString);
    	return toolTipLabel;
    }
    
    private static VBox getPropertyList(JobProperties jobProperties){
    	VBox vbBox = new VBox();
		vbBox.setStyle("-fx-background-color:#624D5F;");
		vbBox.setSpacing(10);
		vbBox.setMaxWidth(225);
//		VBox.setMargin(vbBox, new Insets(10,100,10,25));	
		if(!Utilities.isNullOrEmpty(jobProperties.getName())){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_NAME, jobProperties.getName());
			vbBox.getChildren().add(hBox);
		}		
		if(!Utilities.isNullOrEmpty(jobProperties.getDescription())){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_DESCRIPTION, jobProperties.getDescription());
			vbBox.getChildren().add(hBox);
		}
		
		if(null != jobProperties.getWorkStatus()){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_STATUS,WorkStatusType.getWorkStatusType(jobProperties.getWorkStatus()).toString());
			vbBox.getChildren().add(hBox);
		}
		
		if(!Utilities.isNullOrEmpty(jobProperties.getOwnerId())){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_OWNER,jobProperties.getOwnerId());
			vbBox.getChildren().add(hBox);
			
		}
		
		if(!Utilities.isNullOrEmpty(jobProperties.getInitiatorId())){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_INITIATOR,jobProperties.getInitiatorId());
			vbBox.getChildren().add(hBox);
			
		}
		
		if(!Utilities.isNullOrEmpty(jobProperties.getExecutor())){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_EXECUTOR,jobProperties.getExecutor());
			vbBox.getChildren().add(hBox);
		}
		if(!Utilities.isNullOrEmpty(jobProperties.getMilestoneDescription())){
			HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_MILESTONE,jobProperties.getMilestoneDescription());
			vbBox.getChildren().add(hBox);
		}
		
		if(null != jobProperties.getPlannedDateRange()){
			if(null != jobProperties.getPlannedDateRange().getFromDate()){
				HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_PLANNED_START,Utilities.dateToStringdd_MMM_yyyy(jobProperties.getPlannedDateRange().getFromDate()));
				vbBox.getChildren().add(hBox);
			}
			if(null != jobProperties.getPlannedDateRange().getToDate()){
				HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_PLANNED_END,Utilities.dateToStringdd_MMM_yyyy(jobProperties.getPlannedDateRange().getToDate()));
				vbBox.getChildren().add(hBox);
			}
		}
		
		if(null != jobProperties.getActualDateRange()){
			if(null != jobProperties.getActualDateRange().getFromDate()){
				HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_ACTUAL_START,Utilities.dateToStringdd_MMM_yyyy(jobProperties.getActualDateRange().getFromDate()));
				vbBox.getChildren().add(hBox);
			}
			if(null != jobProperties.getActualDateRange().getToDate()){
				HBox hBox = getPropertyBox(CPMClientConstants.LABEL_JOB_ACTUAL_END,Utilities.dateToStringdd_MMM_yyyy(jobProperties.getActualDateRange().getToDate()));
				vbBox.getChildren().add(hBox);
			}
		}
		
		return vbBox;
    }
    
    private static HBox getPropertyBox(String property,Object value){
    	HBox hbBox = new HBox();
		hbBox.setStyle("-fx-background-color:#624D5F;-fx-border-width:0 0 1 0;-fx-border-color:#C4C4C4;");
		hbBox.setSpacing(2);
		Label propertyLabel  = new Label(property + " : ");
		propertyLabel.setStyle("-fx-font-size: 12px;-fx-text-fill:#C4C4C4;");
		
		Label valueLabel  = new Label(String.valueOf(value));
		valueLabel.setStyle("-fx-font-size: 12px;-fx-text-fill:#C4C4C4;");
		
		hbBox.getChildren().addAll(propertyLabel,valueLabel);
		HBox.setMargin(hbBox, new Insets(2,5,10,5));
		hbBox.setMaxWidth(225);
		return hbBox;
    }
    
    
    public static void loadHierarchyPanel(int context, HierarchyFilter filter, String levelName,
			PageItr<? extends HierarchyItem> hierarchyNodes) {
		HierarchyFilterUtility.clearPanelFilter();
		String screenId = CPMClientConstants.MENU_ID_PLANT_HIERARCHY;
		Parent screen = ClientConfig.getInstance().getScreenById(screenId);
		ScreensConfiguration.getInstance().getMainScreenController().setmainDataPane(screen);
		HierarchyItemPanel hierarchyItemPanel = ClientConfig.getInstance().getHierarchyItemPanel(levelName);

		hierarchyItemPanel.loadFilterData(context, filter, hierarchyNodes);
	}

    public static void loadJobPanel(int context, HierarchyFilter filter, PageItr<? extends HierarchyItem> hierachyNodesPage) {
		HierarchyFilterUtility.clearPanelFilter();
		String screenId = CPMClientConstants.MENU_ID_PLANT_HIERARCHY;
		Parent screen = ClientConfig.getInstance().getScreenById(screenId);
		ScreensConfiguration.getInstance().getMainScreenController().setmainDataPane(screen);

		HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();

		jobPanel.loadFilterData(context, filter, hierachyNodesPage);

	}
    
    public static String getToolTipString( HierarchyFilter filter){
    	return toolTips.get(filter.getFilterIdentifier());
    }

}
