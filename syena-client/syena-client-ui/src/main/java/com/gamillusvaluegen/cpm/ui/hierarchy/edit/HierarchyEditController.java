/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class HierarchyEditController implements DialogController {

    private FXMLDialog dialog;

    private HierarchyLevel level;

    private HierarchyEditDataPanel editPanel;

    private List<StandardHierarchyNode> hierarchyData;

    @FXML
    private ScrollPane dataPane;
    
    private boolean standard;

    public HierarchyEditController(HierarchyLevel level, List<StandardHierarchyNode> hierarchyData, boolean standard) {
    	this.standard = standard;
        this.hierarchyData = hierarchyData;
        this.level = level;
    }

    @FXML
    private void initialize() {
        editPanel = new HierarchyEditDataPanel(level);
        if (null != hierarchyData) {
            editPanel.loadData(hierarchyData);
        }
        dataPane.setPrefWidth(editPanel.getPrefWidth());
        dataPane.setPrefHeight(editPanel.getPrefHeight() + 100);
        dataPane.setContent(editPanel);
    }

    @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    @FXML
    public void submitData() {
        try {
            List<? extends StandardHierarchyNode> standardHierarchyNodes = editPanel.getContent();
            if (null != standardHierarchyNodes) {
                if (null != hierarchyData) {
                	if(standard){
                      HierarchyNodeService.updateStandardHierarchyNodes(standardHierarchyNodes);
                	}else{

                    	HierarchyNodeService.updateHierarchyNode(standardHierarchyNodes);
                	}

                } else {
                	if(standard){
                		  HierarchyNodeService.createStandardHierarchyNodes(standardHierarchyNodes);
                	}else{
                		HierarchyNodeService.createHierarchyNodes(standardHierarchyNodes);
                	}
//                  
                }
            }
            this.dialog.close();
            ScreensConfiguration.getInstance().setCurrentParentWindow(ScreensConfiguration.getInstance().getPrimaryStage());
        } catch (ServiceFailedException ex) {
            Message errorMessage = ex.getErroMessage();
            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
            alertDialog.show();
        }
    }

    @FXML
    public void closeDialog() {
        this.dialog.close();
        ScreensConfiguration.getInstance().setCurrentParentWindow(ScreensConfiguration.getInstance().getPrimaryStage());
    }

}
