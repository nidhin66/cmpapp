/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.search;

import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.stage.Popup;

import com.gamillusvaluegen.cpm.ScreensConfiguration;

/**
 *
 * @author hp
 */
public class QuickSearchPopup extends Popup {
    
    
    

    public QuickSearchPopup(Parent parentNode) {
        QuickSearchPane quickSearchPane = new QuickSearchPane();
        
        this.getContent().addAll(quickSearchPane);

        Point2D point = parentNode.localToScene(0.0, 0.0);

        this.setAutoHide(true);
        double x = ScreensConfiguration.getInstance().getPrimaryStage().getX() + point.getX() - 460;
        double y = ScreensConfiguration.getInstance().getPrimaryStage().getY() + point.getY() + 60;
        this.setX(x);
        this.setY(y);
   
        quickSearchPane.setPopUp(this);
    }

}
