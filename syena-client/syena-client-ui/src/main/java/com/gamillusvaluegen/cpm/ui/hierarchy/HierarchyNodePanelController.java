/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.util.Iterator;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;
import javafx.stage.StageStyle;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.custom.HierarchySearchController;
import com.gamillusvaluegen.cpm.ui.custom.SearchTextField;
import com.gamillusvaluegen.cpm.ui.hierarchy.edit.HierarchyEditController;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.JobGroupPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.JobPane;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;

/**
 *
 * @author hp
 */
public class HierarchyNodePanelController implements HierarchySearchController{
    
    private static double TOGGLE_BTN_HEIGHT = 10;
    private static double TOGGLE_BTN_WIDTH = 25;
    private static String CONTEXT_KEY = "context";
    
    @FXML
    private AnchorPane topPane;
    
    @FXML
    private BorderPane bottomPane;


    
    @FXML
    private Button moreOptions;
    
    @FXML
    private Button nodeIcon;
    
    @FXML
    private Button pageFirstBtn;
    
    @FXML
    private Button pageLastBtn;
    
    @FXML
    private Button pagePrevBtn;
    
    @FXML
    private Button pageNextBtn;
    
    @FXML
    private Label paginationLabel;
    
    @FXML
    private SearchTextField searchBox;
    
    @FXML
    private Button addItemBtn;
    
    @FXML
    private HBox toggleBox;
    
    @FXML
    private HBox filterLabelBox;
    
    @FXML
    private ImageView filterImage;
    
    @FXML
    private Label filterLabel;
    
    private HierarchyItemPanel nodePanel;
    
    private ToggleGroup toggelGroup;
    
    @FXML
    private void initialize() {
        
        this.searchBox.setController(this);
        filterLabelBox.setVisible(false);
        
        
        filterLabelBox.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if(null != nodePanel.currentFilter){
					Popup popup = nodePanel.currentFilter.getCondtionPopup();
		    		if(null != popup){
		    			popup.show(filterLabel,event.getScreenX() + 25,event.getScreenY()); 
		    		}
				}
				
			}
		});
        
        filterLabelBox.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if(null != nodePanel.currentFilter){
					Popup popup = nodePanel.currentFilter.getCondtionPopup();
		    		if(null != popup){
		    			popup.hide();
		    		}
				}
				
			}
		});
        
//        filterLabel.setOnMouseMoved(new EventHandler<MouseEvent>() {
//
//			@Override
//			public void handle(MouseEvent event) {
//				if(null != nodePanel.currentFilter){
//					Popup popup = nodePanel.currentFilter.getCondtionPopup();
//		    		if(null != popup){
//		    			popup.show(filterLabel,event.getScreenX() + 25,event.getScreenY()); 
//		    		}
//				}
//				
//			}
//		});
//        filterLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
//			@Override
//			public void handle(MouseEvent event) {
//				if(null != nodePanel.currentFilter){
//					Popup popup = nodePanel.currentFilter.getCondtionPopup();
//		    		if(null != popup){
//		    			popup.show(filterLabel,event.getScreenX() + 25,event.getScreenY()); 
//		    		}
//				}
//				
//				
//			}
//		});   	
    }
    
    public AnchorPane getTopPane() {
        return topPane;
    }
    
    public BorderPane getBottomPane() {
        return bottomPane;
    }
    
 
    
    public void createShowMoreMenus() {
        moreOptions.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        if (e.getButton() == MouseButton.PRIMARY) {
                            MoreOptionsContextMenu contextMenu = new MoreOptionsContextMenu(nodePanel);
                            contextMenu.show(moreOptions, e.getScreenX(), e.getScreenY());
                        }
                        
                    }
                });
    }
    
    public void setNodePanel(HierarchyItemPanel nodePanel) {
        this.nodePanel = nodePanel;
        setToggleButtons();
        HierarchyLevel level = nodePanel.getLevel();
         String levelName = null;
        if(null != level){
            levelName = level.getLevelName();           
            
        }else{
            levelName = "Job";
        }
        String iconClass = ClientConfig.getInstance().getLevelNodeIconStyle(levelName);
        nodeIcon.getStyleClass().add(iconClass);
    }
    
    @FXML
    public void getFirstPage() {
        this.nodePanel.loadFirstPage();
    }
    
    @FXML
    public void getLastPage() {
        this.nodePanel.loadLastPage();
    }
    
    @FXML
    public void getNextPage() {
        this.nodePanel.loadNextPage();
    }
    
    @FXML
    public void getPreviousPage() {
        this.nodePanel.loadPreviousPage();
    }
    
    @FXML
    private void searchAction() {
        String searchText = searchBox.getText();
        searchData(searchText);
    }
    
    public List<? extends HierarchyItem> searchData(String text) {
        List<? extends HierarchyItem> hierarchNodes = this.nodePanel.searchData(text);
        return hierarchNodes;
    }
    
    @FXML
    private void showAddItemDialog() {
        final Scene scene = addItemBtn.getScene();
               final Point2D windowCoord = new Point2D(scene.getWindow().getX(), scene.getWindow().getY());
               final Point2D sceneCoord = new Point2D(scene.getX(), scene.getY());
               final Point2D nodeCoord = addItemBtn.localToScene(0.0, 0.0);
               final double clickX = Math.round(windowCoord.getX() + sceneCoord.getX() + nodeCoord.getX());
               final double clickY = Math.round(windowCoord.getY() + sceneCoord.getY() + nodeCoord.getY());
        
        if (null != nodePanel.getLevel()) {
        	boolean standard = false;
            
        	if(nodePanel.getContextID() == 1){
        		standard = true;
        	}
            FXMLDialog addNodeItemDialog = new FXMLDialog(new HierarchyEditController(nodePanel.getLevel(), null, true), getClass().getResource("/fxml/plant_hierarchy_edit.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,clickX,clickY);
            addNodeItemDialog.display();
        } else {
        	 FXMLDialog editJobDialog = null;
        	 HierarchyItem parentItem = null;
        	 if (nodePanel.getContextID() == 1 || nodePanel.getContextID() == 3) {
        		 if(nodePanel.getContextID() == 3){
        			 parentItem = nodePanel.getImmediateSelectedParent();
        		 }
        		 editJobDialog =  new FXMLDialog(new JobGroupPane(parentItem,null),ScreensConfiguration.getInstance().getCurrentParentWindow());
        	 }else{
        		 if(nodePanel.getContextID() == 4){
        			 parentItem = nodePanel.getImmediateSelectedParent();
        		 }
        		 editJobDialog  =  new FXMLDialog(new JobPane(parentItem,null),ScreensConfiguration.getInstance().getCurrentParentWindow());
        	 }     			
        			 
//             = new FXMLDialog(new HierarchyJobEditController(nodePanel), getClass().getResource("/fxml/job_edit_pane.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,clickX,clickY);
            editJobDialog.display();
        }
        
    }
    
    public void setPaginationLabel(String label) {
        if (!Utilities.isNullOrEmpty(label)) {
            paginationLabel.setVisible(true);
            paginationLabel.setText(label);
        } else {
            paginationLabel.setVisible(false);
        }
    }
    
    private void setToggleButtons() {
        
        toggelGroup = new ToggleGroup();
        
        if (null != nodePanel.getLevel()) {
         /**
          * Toggle button for the panels other than the job Panels
          */ 
          
         
            ToggleButton showAllBtn = new ToggleButton();
            showAllBtn.setPrefSize(TOGGLE_BTN_WIDTH, TOGGLE_BTN_HEIGHT);
            showAllBtn.getStyleClass().add("show-all-button");
            showAllBtn.setTooltip(new Tooltip("Show All"));
            
            ToggleButton showLinked = new ToggleButton();            
            showLinked.setPrefSize(TOGGLE_BTN_WIDTH, TOGGLE_BTN_HEIGHT);
            showLinked.getStyleClass().add("show-dependent-button");
            showLinked.setTooltip(new Tooltip("Show Linked"));
            
            showAllBtn.setToggleGroup(toggelGroup);
            showAllBtn.getProperties().put(CONTEXT_KEY, 1);
            showLinked.setToggleGroup(toggelGroup);
            showLinked.getProperties().put(CONTEXT_KEY, 2);
            
            toggelGroup.selectToggle(showLinked);
            
            toggleBox.getChildren().addAll(showLinked, showAllBtn);
            
            showAllBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	clearSearch();
                    nodePanel.showAllStandardHierarchyNodes();
                }
            });
            showLinked.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	clearSearch();
                    nodePanel.showLinkedNodes();
                }
            });
            
//            SwitchButton switchButton = new SwitchButton();
//            toggleBox.getChildren().addAll(switchButton);
//            
        } else {
            /**
             * Toggle button for the Job Panel
             */
            ToggleButton showAllJobGroupsBtn = new ToggleButton();
            showAllJobGroupsBtn.setPrefSize(TOGGLE_BTN_WIDTH, TOGGLE_BTN_HEIGHT);
            showAllJobGroupsBtn.getStyleClass().add("show-all-jobgroups-button");
            showAllJobGroupsBtn.setTooltip(new Tooltip("Show All Job Groups"));
            
            ToggleButton showLinkedJobGroups = new ToggleButton();
            showLinkedJobGroups.setPrefSize(TOGGLE_BTN_WIDTH, TOGGLE_BTN_HEIGHT);
            showLinkedJobGroups.getStyleClass().add("show-linked-job-groups-button");
            showLinkedJobGroups.setTooltip(new Tooltip("Show Linked Job Groups"));
            
            ToggleButton showAllJobsBtn = new ToggleButton();
            showAllJobsBtn.setPrefSize(TOGGLE_BTN_WIDTH, TOGGLE_BTN_HEIGHT);
            showAllJobsBtn.getStyleClass().add("show-all-jobs-button");
            showAllJobsBtn.setTooltip(new Tooltip("Show All Jobs"));
            
            ToggleButton showLinkedJobs = new ToggleButton();
            showLinkedJobs.setPrefSize(TOGGLE_BTN_WIDTH, TOGGLE_BTN_HEIGHT);
            showLinkedJobs.getStyleClass().add("show-linked-jobs-button");
            showLinkedJobs.setTooltip(new Tooltip("Show Linked Jobs"));
            
            showAllJobGroupsBtn.setToggleGroup(toggelGroup);
            showAllJobGroupsBtn.getProperties().put(CONTEXT_KEY, 1);
            showLinkedJobGroups.setToggleGroup(toggelGroup);
            showLinkedJobGroups.getProperties().put(CONTEXT_KEY, 3);
            showAllJobsBtn.setToggleGroup(toggelGroup);
            showAllJobsBtn.getProperties().put(CONTEXT_KEY, 2);
            showLinkedJobs.setToggleGroup(toggelGroup);
            showLinkedJobs.getProperties().put(CONTEXT_KEY, 4);
            
            toggelGroup.selectToggle(showLinkedJobGroups);
            
           
            
            toggleBox.getChildren().addAll(showLinkedJobGroups, showLinkedJobs, showAllJobGroupsBtn, showAllJobsBtn);
            
            showAllJobGroupsBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	clearSearch();
//                    HierarchyJobPanel hierarchyJobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
                	  HierarchyJobPanel hierarchyJobPanel = (HierarchyJobPanel) nodePanel;
                    hierarchyJobPanel.showData(1);
                }
            });
            showLinkedJobGroups.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
//                	clearSearch();
//                    HierarchyJobPanel hierarchyJobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
                	HierarchyJobPanel hierarchyJobPanel = (HierarchyJobPanel) nodePanel;
                    hierarchyJobPanel.showData(3);
                }
            });
            showAllJobsBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                	clearSearch();
//                    HierarchyJobPanel hierarchyJobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
                	 HierarchyJobPanel hierarchyJobPanel = (HierarchyJobPanel) nodePanel;
                    hierarchyJobPanel.showData(2);
                }
            });
            showLinkedJobs.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
//                	clearSearch();
//                    HierarchyJobPanel hierarchyJobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
                	 HierarchyJobPanel hierarchyJobPanel = (HierarchyJobPanel) nodePanel;
                    hierarchyJobPanel.showData(4);
                }
            });
            
        }
        
    }
    
    @FXML
    public void showNodeDetails(){
    	HierarchyItem item = this.nodePanel.getSelectedItem();
    	if(null != item){
    		NodeDetailsPane detailPane = new NodeDetailsPane(item);
        	FXMLDialog dialog = new FXMLDialog(detailPane, ScreensConfiguration.getInstance().getPrimaryStage());
        	dialog.display();
    	}
    	
    }
    
    public void setToggle(int contextId){
//    	clearSearch();
        for(Iterator<Toggle> it = toggelGroup.getToggles().iterator(); it.hasNext();){
            Toggle toggle = it.next();
            int value = (int) toggle.getProperties().get(CONTEXT_KEY);
            if(contextId == value){
                toggelGroup.selectToggle(toggle);
                        break;
            }
        }
    }
    
    public void clearSearch(){
    	searchBox.clear();
    	this.nodePanel.currentFilter = null;
    	
    	filterLabelBox.setVisible(false);
    }
    
    public String getSearchText(){
    	return searchBox.getText();
    }
    
    public void setFilterLabel(HierarchyFilter filter){
    	if( null != filterImage.getStyleClass()){
    		filterImage.getStyleClass().clear();
    	}
    	filterImage.getStyleClass().add(filter.getStyleClass());
    	filterLabelBox.setVisible(true);
    	filterLabel.setText(filter.getLabelString());
    }
    
    @FXML
    private void onClearFilter(){    	
    	clearSearch();
    	this.nodePanel.loadFirstPage();
    }
    
    
    private void showFilterPopUp(){
    	
    }
}
