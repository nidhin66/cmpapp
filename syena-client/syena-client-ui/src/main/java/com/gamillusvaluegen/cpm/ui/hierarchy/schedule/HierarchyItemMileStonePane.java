/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javafx.scene.layout.GridPane;

import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;

/**
 *
 * @author hp
 */
public class HierarchyItemMileStonePane extends GridPane{
    
    private NodeCategory nodeType;
    
    private WorkSchedule schedule;
    
    private WorkTime workTime;
    
    private List<MileStoneItemPane> itemPanes = null;
    
    private Date estimatedStartDate;
    
    private Date estimatedEndDate;
    
    public HierarchyItemMileStonePane(WorkTime workTime, ScheduleMode mode){
         this.nodeType = NodeCategory.hierarchy;
         this.workTime = workTime;
         this.itemPanes = new ArrayList<>();
         MileStoneItemPane startMileStonePane = new MileStoneItemPane("Start",MileStoneType.START,workTime, mode);
         itemPanes.add(startMileStonePane);
         this.add(startMileStonePane, 0, 0);
         MileStoneItemPane finishMileStonePane = new MileStoneItemPane("Finish",MileStoneType.FINISH,workTime, mode);
         itemPanes.add(finishMileStonePane);
         this.add(finishMileStonePane, 0, 1);
    }
      
    public HierarchyItemMileStonePane(WorkSchedule schedule,ScheduleMode mode){  
        this.nodeType = NodeCategory.job;
        this.schedule = schedule;
        this.itemPanes = new ArrayList<>();
        int i = 0;
        for(Iterator<ScheduledStage> it = schedule.getScheduledStages().iterator();it.hasNext();){
            ScheduledStage scheduledStage = it.next();
            MileStoneType mileStoneType = MileStoneType.INTERMEDIATE;
            if(i == 0){
                mileStoneType = MileStoneType.START;
            }else if(!it.hasNext()){
                mileStoneType = MileStoneType.FINISH;
            }
            MileStoneItemPane itemPane = new MileStoneItemPane(mileStoneType,scheduledStage, mode);
            itemPanes.add(itemPane);
            this.add(itemPane, 0, i);
            i++;
        }               
        this.getStyleClass().add("default-pane-background");        
        this.setStyle("-fx-border-width:  0 1 0 1;-fx-border-color: #828A95;");
    }
    
    public WorkSchedule getJobSchedule(){
        for(MileStoneItemPane itemPane : itemPanes){            
            ScheduledStage scheduledStage = itemPane.getScheduledStage();
            if(itemPane.getMileStoneType() == MileStoneType.START){
               estimatedStartDate = scheduledStage.getPlannedTime();
            }else if(itemPane.getMileStoneType() == MileStoneType.FINISH){
               estimatedEndDate = scheduledStage.getPlannedTime();
            }
        }
        return schedule;
    }
    
    public WorkTime getWorkTime(){
        if(null == workTime){
            workTime = new WorkTime();
        }
        for(MileStoneItemPane itemPane : itemPanes){
            if (itemPane.getMileStoneType() == MileStoneType.START){
                WorkTime itemWorkTime = itemPane.getWorkTime();
                workTime.setActualStartTime(itemWorkTime.getActualStartTime());
                workTime.setPlanedStartTime(itemWorkTime.getPlannedStartTime());
                estimatedStartDate = itemPane.getWorkTime().getActualStartTime();
            }else if(itemPane.getMileStoneType() == MileStoneType.FINISH){                
                WorkTime itemWorkTime = itemPane.getWorkTime();
                workTime.setActualFinishTime(itemWorkTime.getActualFinishTime());
                workTime.setPlanedFinishTime(itemWorkTime.getPlannedFinishTime());
                estimatedEndDate = itemPane.getWorkTime().getActualFinishTime();
            }
        }
        return workTime;
    }

    public Date getEstimatedStartDate() {
        return estimatedStartDate;
    }

    public Date getEstimatedEndDate() {
        return estimatedEndDate;
    }
    
    
}
