/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.schedule;

import java.io.IOException;
import java.math.BigDecimal;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;


import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.Cost;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.exception.SchedulingException;

/**
 *
 * @author hp
 */
public class HierarchyItemEstimatePane extends BorderPane {

	@FXML
	private RadioButton approveButton;

	@FXML
	private RadioButton rejectButton;

	ToggleGroup group = new ToggleGroup();

	private NodeCategory nodeType;

	private WorkTime workTime;

	private Job job;
	private JobGroup jobGroup;

	@FXML
	private TextField costField;

	@FXML
	private TextField pdField;

	public HierarchyItemEstimatePane(WorkTime workTime,ScheduleMode mode, boolean approve) {
		loadUI(mode);
		this.nodeType = NodeCategory.hierarchy;
		this.workTime = workTime;
		if (null != workTime && null != workTime.getDuration()) {
			PersonDays personDays = (PersonDays) workTime.getDuration();
			this.pdField.setText(String.valueOf(personDays.getNumberofDays()));
		} else {
			this.pdField.setText("0");
		}
		if (!approve) {
			rejectButton.setVisible(false);
			approveButton.setSelected(true);
			approveButton.setDisable(true);
		}
		

	}
	
	public HierarchyItemEstimatePane(JobGroup jobGroup,ScheduleMode mode, boolean approve) {
		loadUI(mode);
		this.nodeType = NodeCategory.jobgroup;
		this.jobGroup = jobGroup;
		if (null != jobGroup.getWorkTime() && null != jobGroup.getDuration()) {
			if (jobGroup.getTotalEffert() instanceof PersonDays) {
				PersonDays personDays = (PersonDays) jobGroup.getTotalEffert();
				this.pdField.setText(String.valueOf(personDays.getNumberofDays()));
			}
		} else {
			this.pdField.setText("0");
		}
//		Cost cost = jobGroup.getEstimatedCost();
//		if (null != cost) {
//			this.costField.setText(String.valueOf(jobGroup.getEstimatedCost().getAmount()));
//		}
		if (!approve) {
			rejectButton.setVisible(false);
			approveButton.setSelected(true);
			approveButton.setDisable(true);
		}
	}

	public HierarchyItemEstimatePane(Job job,ScheduleMode mode, boolean approve) {
		loadUI(mode);
		this.nodeType = NodeCategory.job;
		this.job = job;
		if (null != job.getWorkTime() && null != job.getDuration()) {
			if (job.getTotalEffert() instanceof PersonDays) {
				PersonDays personDays = (PersonDays) job.getTotalEffert();
				this.pdField.setText(String.valueOf(personDays.getNumberofDays()));
			}
		} else {
			this.pdField.setText("0");
		}
		Cost cost = job.getEstimatedCost();
		if (null != cost) {
			this.costField.setText(String.valueOf(job.getEstimatedCost().getAmount()));
		}
		if (!approve) {
			rejectButton.setVisible(false);
			approveButton.setSelected(true);
			approveButton.setDisable(true);
		}

	}

	private void loadUI(ScheduleMode mode) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/hierarchy_item_estimate.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		approveButton.setToggleGroup(group);
		rejectButton.setToggleGroup(group);
		if(mode == ScheduleMode.SCHEDULING){
			pdField.setEditable(true);
			costField.setEditable(true);
		}else{
			pdField.setEditable(false);
			costField.setEditable(false);
		}
	}

	public WorkTime getWorkTime() {
		if (null == workTime) {
			workTime = new WorkTime();
		}
		// Duration duration = new Duration
		if (!Utilities.isNullOrEmpty(pdField.getText())) {

			workTime.setDuration(new PersonDays(Integer.valueOf(pdField.getText())));
		}
		return workTime;
	}

	public Job getJob() {
		if (!Utilities.isNullOrEmpty(pdField.getText())) {
			job.setTotalEffert(new PersonDays(Integer.valueOf(pdField.getText())));

		}
		if (!Utilities.isNullOrEmpty(costField.getText())) {
			Cost cost = job.getEstimatedCost();
			if (null == cost) {
				cost = new Cost();
			}
			cost.setAmount(new BigDecimal(costField.getText()));
			job.setEstimatedCost(cost);
		}
		return job;
	}
	
	public JobGroup getJobGroup() {
		if (!Utilities.isNullOrEmpty(pdField.getText())) {
			jobGroup.setTotalEffert(new PersonDays(Integer.valueOf(pdField.getText())));

		}
//		if (!Utilities.isNullOrEmpty(costField.getText())) {
//			Cost cost = jobGroup.getEstimatedCost();
//			if (null == cost) {
//				cost = new Cost();
//			}
//			cost.setAmount(new BigDecimal(costField.getText()));
//			jobGroup.setEstimatedCost(cost);
//		}
		return jobGroup;
	}


	public boolean isApproved() throws SchedulingException {
		if (approveButton.isSelected()) {
			return true;
		} else if (rejectButton.isSelected()) {
			return false;
		} else {
			throw new SchedulingException("","Approve or Reject is not selected for this job");
		}
	}

}
