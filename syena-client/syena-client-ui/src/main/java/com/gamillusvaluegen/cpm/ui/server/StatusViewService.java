/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.Date;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.chart.ChartData;
import com.gvg.syena.core.api.entity.chart.ChartType;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.ChartsDataManagementServiceNames;

/**
 *
 * @author hp
 */
public class StatusViewService {

    public static final String url = ScreensConfiguration.getUrl();

//	private static RestCaller hierarchyManagementClient = new RestCaller(url + "/hierarchyManagement");
    private static RestCaller createRestCallser() {
        RestCaller chartManagementController = new RestCaller(url);
        chartManagementController.path("chartsData");
        return chartManagementController;
    }

    public static ChartData<Date> getChartsData(long hierarchyNodeId, ChartType[] chartTypes) throws ServiceFailedException {
        RestCaller chartManagementController = createRestCallser();
        chartManagementController.path(ChartsDataManagementServiceNames.getChartsDatas);
        chartManagementController.param("hierarchyNodeId", hierarchyNodeId);
        if(null != chartTypes){
            for(ChartType chartType : chartTypes){
                chartManagementController.param("chartTypes", chartType);
             }
        }  else{
            chartManagementController.param("chartTypes", "");
        }      
        ChartData<Date> chartData = null;
        try {
            TypeReference<ChartData<Date>> type = new TypeReference<ChartData<Date>>() {
            };
            chartData = chartManagementController.post(null,type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.STATUS_VIEW_SERVICE_001, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return chartData;
    }
    
//    public static ChartData<Date> getPredictionChartsDatas(long hierarchyNodeId,Chart[] predictionChartTypes) throws ServiceFailedException {
//		 RestCaller chartManagementController = createRestCallser();
//        chartManagementController.path(ChartsDataManagementServiceNames.getPredictionChartsDatas);
//        chartManagementController.param("hierarchyNodeId", hierarchyNodeId);
//        if(null != predictionChartTypes){
//            for(PredictionChartType predictionChartType : predictionChartTypes){
//                chartManagementController.param("predictionChartTypes", predictionChartType);
//             }
//        }  else{
//            chartManagementController.param("predictionChartTypes", "");
//        }      
//        ChartData<Date> chartData = null;
//        try {
//            TypeReference<ChartData<Date>> type = new TypeReference<ChartData<Date>>() {
//            };
//            chartData = chartManagementController.post(null,type);
//        } catch (RestCallException ex) {
//             Message erroMessage = new Message(ExceptionMessages.STATUS_VIEW_SERVICE_002, ex.getMessage() );
//            throw new ServiceFailedException(erroMessage);
//        }
//        return chartData;
//	}

}
