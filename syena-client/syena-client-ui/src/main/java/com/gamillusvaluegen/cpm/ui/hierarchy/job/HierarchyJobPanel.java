/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TablePosition;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetColumn;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import com.gamillusvaluegen.cpm.ui.hierarchy.schedule.HierarchyItemSchedulePane;
import com.gamillusvaluegen.cpm.ui.hierarchy.schedule.ScheduleMode;
import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.ClientMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyContextMenu;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyNodeCell;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyNodePanelController;
import com.gamillusvaluegen.cpm.ui.hierarchy.checklist.CheckListPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.dependency.DependencyPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilterUtility;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.server.ScheduleManagementService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;

/**
 *
 * @author hp
 */
public class HierarchyJobPanel extends HierarchyItemPanel {

	private BorderPane rootPane;

	private SpreadsheetView sheet;

	private final int rowHight = 25;
	 private int ColumnWidth = CPMClientConstants.DEFAULT_COLUMN_WIDTH;

	

	private HierarchyContextMenu contextMenu;

	private PageItr<? extends HierarchyItem> currentPage;

	private CheckListPane checkListPane;

	private HierarchyJobPanel me;
	
	

	private List<? extends HierarchyItem> jobItems = null;

	public HierarchyJobPanel(String level, boolean approved, int rowCount, int columnCount) {
		this.approved = approved;
		this.nodeType = NodeCategory.jobgroup;
		contextID = 3;
		this.me = this;
		contextMenu = new HierarchyContextMenu(this);
		this.rowCount = rowCount;
		this.columnCount = columnCount;
		createPanel();
		rootPane.setCenter(sheet);
//		controller.setTitle(level);
		controller.createShowMoreMenus();
	}

	private void createPanel() {
		/**
		 * Create panel with the search box and the add button
		 */
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/plant_hierarchy_node.fxml"));
		try {
			rootPane = loader.load();
			rootPane.getStyleClass().add("hierarchyNode-pane");
			controller = loader.getController();
			controller.setNodePanel(this);
		} catch (IOException ex) {
			ex.printStackTrace();
			// Logger.getLogger(SampleController.class.getName()).log(Level.SEVERE,
			// null, ex);
		}

		/**
		 * Create SpreadSheet
		 */
		GridBase grid = new GridBase(rowCount, columnCount);

		sheet = new SpreadsheetView();
		sheet.setGrid(grid);
		sheet.setContextMenu(null);

		sheet.setShowColumnHeader(false);
		sheet.setShowRowHeader(false);
		sheet.setEditable(false);
		sheet.setPrefSize((columnCount * ColumnWidth) + 5, rowCount * rowHight);

		for (SpreadsheetColumn column : sheet.getColumns()) {
			column.setPrefWidth(ColumnWidth);

		}
		double height = (rowCount * rowHight) + 15 + 2;
		rootPane.prefHeightProperty().add(height);
		rootPane.prefWidthProperty().add(sheet.getPrefWidth());
		// rootPane.setMinHeight();
		// rootPane.setPrefWidth(sheet.getPrefWidth());
		// rootPane.setPrefHeight((rowCount * rowHight) + 35 + 15 + 2);

		// SpreadSheetChangeListner changeListner = new
		// SpreadSheetChangeListner(this, sheet);
		// sheet.getSelectionModel().selectedItemProperty().addListener(changeListner);
		// sheet.setOnKeyPressed(new EventHandler<KeyEvent>() {
		// @Override
		// public void handle(KeyEvent event) {
		// KeyCode keyCode = event.getCode();
		// if (keyCode == KeyCode.LEFT) {
		// selectFocusedCell();
		//
		// } else if (keyCode == KeyCode.UP) {
		// selectFocusedCell();
		//
		// } else if (keyCode == KeyCode.DOWN) {
		// selectFocusedCell();
		//
		// } else if (keyCode == KeyCode.RIGHT) {
		// selectFocusedCell();
		//
		// }
		//
		// }
		//
		// });
		sheet.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {
					if (null != contextMenu && contextMenu.isShowing()) {
						contextMenu.hide();
					}
					if (null != me.getSelectedItem()) {

						Map<String, HierarchyItem> selectedParentNodes = null;
						if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP
								|| contextID == CPMClientConstants.CONTEXT_ALL_JOBS) {
							if (null != parentPanel) {

								selectedParentNodes = parentPanel.getSelectedItemFromAllNodes();

							}

						}
						contextMenu.setMenuItems(selectedParentNodes);
						contextMenu.show(sheet, e.getScreenX(), e.getScreenY());
					}

				} else if (e.getButton() == MouseButton.PRIMARY) {
					// childPanel.showLinkedData();
					if (approved) {
						checkListPane.showLinkedCheckList();
						loadDependencyData();
					}
				}

			}
		});

		loadData(null, true);

	}

	private void loadDependencyData() {
		if (contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS
				|| contextID == CPMClientConstants.CONTEXT_LINKED_JOBS) {
			DependencyPane dependencyPane = ClientConfig.getInstance().getDependencyPane();
			dependencyPane.loadData();
		}

	}

	@Override
	public void loadData(PageItr<? extends HierarchyItem> hierachyNodesPage, boolean loadChild) {
		this.currentPage = hierachyNodesPage;
		sheet.getSelectionModel().clearSelection();
		List<? extends HierarchyItem> hierachyNodes = null;
		if (null != hierachyNodesPage) {
			hierachyNodes = hierachyNodesPage.getContent();
		}

		GridBase grid = new GridBase(rowCount, columnCount);
		ObservableList<ObservableList<SpreadsheetCell>> rows = FXCollections.observableArrayList();
		for (int row = 0; row < grid.getRowCount(); ++row) {
			final ObservableList<SpreadsheetCell> list = FXCollections.observableArrayList();
			for (int column = 0; column < grid.getColumnCount(); ++column) {
				int index = (row * grid.getColumnCount()) + column;
				if (null != hierachyNodes) {
					if (index < hierachyNodes.size()) {
						list.add(generateCell(hierachyNodes.get(index), row, column, 1, 1));
					} else {
						list.add(generateCell(null, row, column, 1, 1));
					}
				} else {
					list.add(generateCell(null, row, column, 1, 1));
				}

			}
			rows.add(list);
		}
		grid.setRows(rows);
		sheet.setGrid(grid);
		// sheet.getSelectionModel().select(0, sheet.getColumns().get(0));
		// getSelectedItem();
		String paginationLabel = null;
		if (null != currentPage) {
			if (currentPage.getTotalPages() > 0) {
				paginationLabel = currentPage.getCurrentPage() + 1 + " of " + currentPage.getTotalPages();
			}
		}
		controller.setPaginationLabel(paginationLabel);
		if (null != checkListPane) {
			checkListPane.showLinkedCheckList();
		}

	}

	private SpreadsheetCell generateCell(HierarchyItem hierachyNode, int row, int column, int rowSpan, int colSpan) {
		SpreadsheetCell cell = new HierarchyNodeCell(row, column, rowSpan, colSpan, hierachyNode);
		return cell;
	}

	public BorderPane getRootPane() {
		return rootPane;
	}

	private void loadPage(int pageNumber) {
		PageItr<? extends HierarchyItem> pageData = null;
		String searchText = controller.getSearchText();
		loadData(null, true);
		if(null != currentFilter){
			 try {
				 pageData = HierarchyFilterUtility.getPage(nodeType, pageNumber,  columnCount * rowCount, currentFilter);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			loadFilterData(contextID, currentFilter, pageData);
		}else if (Utilities.isNullOrEmpty(searchText)) {
			if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP) {
				pageData = getAllStandardJobGroup(pageNumber);
			} else if (contextID == CPMClientConstants.CONTEXT_ALL_JOBS) {
				pageData = getAllStandardJobs(pageNumber);

			} else if (contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS) {
				pageData = getLinkedJobGroups(pageNumber);

			} else if (contextID == CPMClientConstants.CONTEXT_LINKED_JOBS) {
				pageData = getLinkedJobs(pageNumber);
			}
			loadData(pageData, true);
		} else {
			pageData = getSearchPage(searchText, pageNumber);
			loadData(pageData, true);
		}	
		
		
//		loadData(null, loadChild);
	}

	@Override
	public void refreshPage() {
		if (null != currentPage) {
			int pageNumber = currentPage.getCurrentPage();
			loadPage(pageNumber);
		}

	}

	@Override
	public void loadFirstPage() {
		loadPage(0);
	}

	@Override
	public void loadLastPage() {

		if (null != currentPage) {
			int pageNumber = currentPage.getTotalPages() - 1;
			loadPage(pageNumber);
		}

	}

	@Override
	public void loadNextPage() {
		if (null != currentPage) {
			if ((currentPage.getCurrentPage() + 1) < currentPage.getTotalPages()) {
				int pageNumber = currentPage.getCurrentPage() + 1;
				loadPage(pageNumber);
			}
		}

	}

	@Override
	public void loadPreviousPage() {
		if (null != currentPage) {
			if ((currentPage.getCurrentPage()) > 0) {
				int pageNumber = currentPage.getCurrentPage() - 1;
				loadPage(pageNumber);
			}
		}

	}

	private PageItr<? extends HierarchyItem> getSearchPage(String text, int pageNumber) {
		text = text + "%";
		PageItr<? extends HierarchyItem> hierachyNodesPage = null;
		if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP) {

			JobGroupProperties jobGroupProperties = new JobGroupProperties();
			jobGroupProperties.setStandard(true);
			jobGroupProperties.setName(text);

			try {
				hierachyNodesPage = HierarchyJobService.searchJobGroupWithProperties(jobGroupProperties, pageNumber,
						rowCount * columnCount);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (contextID == CPMClientConstants.CONTEXT_ALL_JOBS) {
			JobProperties jobSearchOption = new JobProperties();
			jobSearchOption.setStandard(true);
			jobSearchOption.setName(text);
			try {
				hierachyNodesPage = HierarchyJobService.searchJobWithProperties(jobSearchOption, pageNumber,
						rowCount * columnCount);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (contextID == CPMClientConstants.CONTEXT_LINKED_JOBS) {
			JobProperties jobSearchOption = new JobProperties();
			jobSearchOption.setDescription(text);
			jobSearchOption.setStandard(false);
			if (null != parentPanel.getSelectedItem()) {
				jobSearchOption.setConnectedNodeId(parentPanel.getSelectedItem().getId());
			}
			try {
				hierachyNodesPage = HierarchyJobService.searchJobWithProperties(jobSearchOption, pageNumber,
						rowCount * columnCount);
			} catch (ServiceFailedException e) {
				e.printStackTrace();
			}

		} else if (contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS) {
			JobGroupProperties jobGroupProperties = new JobGroupProperties();
			jobGroupProperties.setDescription(text);
			jobGroupProperties.setStandard(false);
			if (null != parentPanel.getSelectedItem()) {
				jobGroupProperties.setConnectedNodeId(parentPanel.getSelectedItem().getId());
			}
			try {
				hierachyNodesPage = HierarchyJobService.searchJobGroupWithProperties(jobGroupProperties, pageNumber,
						rowCount * columnCount);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return hierachyNodesPage;
	}

	@Override
	public List<? extends HierarchyItem> searchData(String text) {
		List<? extends HierarchyItem> resultList = new ArrayList<>();
		PageItr<? extends HierarchyItem> hierachyNodesPage = null;
		if(null == currentFilter){
			 hierachyNodesPage = getSearchPage(text, 0);
		}else{
			currentFilter.setSearchString(text + "%");
			try {
				hierachyNodesPage = HierarchyFilterUtility.getPage(nodeType, 0,  columnCount * rowCount, currentFilter);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		loadData(hierachyNodesPage, true);
		if (null != hierachyNodesPage) {
			resultList = hierachyNodesPage.getContent();
		}

		// if (contextID == CONTEXT_LINKED_JOBS || contextID ==
		// CONTEXT_LINKED_GROUPS) {
		// loadParentPanelData();
		// }
		return resultList;
	}

	@Override
	public HierarchyItem getSelectedItem() {
		ObservableList<TablePosition> list = sheet.getSelectionModel().getSelectedCells();
		if (null != list && list.size() > 0) {
			TablePosition tablePosition = list.get(0);
			if (tablePosition != null) {
				int row = tablePosition.getRow();
				int column = tablePosition.getColumn();
				return (HierarchyItem) sheet.getGrid().getRows().get(row).get(column).getItem();
			} else {
				return null;
			}
		}
		return null;
	}

	@Override
	public List<? extends HierarchyItem> getSelectedItems() {
		ObservableList<TablePosition> list = sheet.getSelectionModel().getSelectedCells();
		List<HierarchyItem> hierarchyItems = new ArrayList<>();
		if (null != list && list.size() > 0) {
			for (TablePosition tablePosition : list) {
				if (tablePosition != null) {
					int row = tablePosition.getRow();
					int column = tablePosition.getColumn();
					HierarchyItem item = (HierarchyItem) sheet.getGrid().getRows().get(row).get(column).getItem();
					hierarchyItems.add(item);
				}
			}

		}
		return hierarchyItems;
	}

	@Override
	public long[] getItemIds() {
		long[] itemIds = null;
		if (null != currentPage) {
			List<? extends HierarchyItem> hiearchNodes = currentPage.getContent();
			if (null != hiearchNodes) {
				itemIds = new long[hiearchNodes.size()];
				int i = 0;
				for (HierarchyItem hierarchyNode : hiearchNodes) {
					itemIds[i] = hierarchyNode.getId();
					i++;
				}
			} else {
				itemIds = new long[0];
			}
		} else {
			itemIds = new long[0];
		}
		return itemIds;
	}

	@Override
	public void linkToParent(HierarchyItem parentItem) {
		try {
			long parentID = parentItem.getId();
			List<? extends HierarchyItem> hierarchyItems = getSelectedItems();
			long[] jobIds = new long[hierarchyItems.size()];
			int i = 0;
			for (HierarchyItem item : hierarchyItems) {
				jobIds[i] = item.getId();
				i++;
			}
			HierarchyNode parentHierarchyNode = (HierarchyNode) parentItem;
			if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP) {
				List<JobGroup<Job>> jobGroups = HierarchyNodeService.createAndlinkJobGroups(parentID, jobIds);
				// parentHierarchyNode.getJobGroups().addAll(jobGroups);

			} else {
				List<Job> jobs = HierarchyNodeService.createAndlinkJob(parentID, jobIds);
				// parentHierarchyNode.getJobs().addAll(jobs);
			}

			AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO,
					"Selected items linked to " + parentHierarchyNode.getName());
			alertDialog.show();
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	public void setDataContext(int i) {

		this.contextID = i;
		if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP
				|| contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS) {
			this.nodeType = NodeCategory.jobgroup;
		} else {
			this.nodeType = NodeCategory.job;
		}

	}

	public void showData(int contextID) {
		this.contextID = contextID;
		if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP
				|| contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS) {
			this.nodeType = NodeCategory.jobgroup;
		} else {
			this.nodeType = NodeCategory.job;
		}
		this.controller.setToggle(contextID);
		
		loadPage(0);
		
	}

	private PageItr<? extends HierarchyItem> getLinkedJobGroups(int pageNumber) {

		PageItr<? extends HierarchyItem> jobGroupNodes = null;
		if (approved) {
			List<Long> parentIds = parentPanel.getParentids();
			Long[] parentIdArray = parentIds.toArray(new Long[parentIds.size()]);
			if(null != parentIds && parentIds.size() > 0 ){
				try {
					jobGroupNodes = HierarchyJobService.getJobGroupssByConnectedNodeIds(parentIdArray, pageNumber,
							rowCount * columnCount);
				} catch (ServiceFailedException ex) {
					ex.printStackTrace();
				}
			}
			
		} else {
			jobGroupNodes = getUnApprovedJobGroupData(pageNumber);
		}

		// List<? extends HierarchyItem> jobs =
		// parentPanel.getParentJobGroups();
		// PageItr<? extends HierarchyItem> jobGroupNodes =
		// createPagingStructure(jobs);
		return jobGroupNodes;
	}

	private PageItr<? extends HierarchyItem> getLinkedJobs(int pageNumber) {

		PageItr<? extends HierarchyItem> jobNodes = null;
		if (approved) {
			List<Long> parentIds = parentPanel.getParentids();
			Long[] parentIdArray = parentIds.toArray(new Long[parentIds.size()]);
			if(null != parentIds && parentIds.size() > 0){
				try {
					jobNodes = HierarchyJobService.getJobsByConnectedNodeIds(parentIdArray, pageNumber,
							rowCount * columnCount);
				} catch (ServiceFailedException ex) {
					ex.printStackTrace();
				}
			}
			
		} else {
			jobNodes = getUnApprovedJobData(pageNumber);
		}

		// List<? extends HierarchyItem> jobs = parentPanel.getParentJobs();
		// PageItr<? extends HierarchyItem> jobNodes =
		// createPagingStructure(jobs);
		return jobNodes;
	}

	private PageItr<? extends HierarchyItem> getAllStandardJobGroup(int pageNumber) {
		PageItr<? extends HierarchyItem> jobGroups = null;
		try {
			jobGroups = HierarchyJobService.getAllStandardJobGroup(pageNumber, columnCount * rowCount);
		} catch (ServiceFailedException ex) {
			ex.printStackTrace();
		}
		return jobGroups;
	}

	private PageItr<? extends HierarchyItem> getAllStandardJobs(int pageNumber) {
		PageItr<? extends HierarchyItem> jobs = null;
		try {
			jobs = HierarchyJobService.getAllStandardJobs(pageNumber, columnCount * rowCount);
		} catch (ServiceFailedException ex) {
			ex.printStackTrace();
		}
		return jobs;
	}

	// @Override
	// public void showDefaultContextData() {
	// this.contextID = CONTEXT_LINKED_GROUPS;
	// showData(contextID);
	// }
	public boolean isStandardContext() {
		if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP || contextID == CPMClientConstants.CONTEXT_ALL_JOBS) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isJobGroupContext() {
		if (contextID == CPMClientConstants.CONTEXT_ALL_GROUP
				|| contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS) {
			return true;
		} else {
			return false;
		}
	}

	public void setCheckListPane(CheckListPane checkListPane) {
		this.checkListPane = checkListPane;
	}

	// private PageItr<? extends HierarchyItem> createPagingStructure(List<?
	// extends HierarchyItem> items) {
	// this.jobItems = items;
	// PageItr<? extends HierarchyItem> pageItr = new PageItr<>();
	// pageItr.setCurrentPage(0);
	// int pageCount = 0;
	// List content = new ArrayList();
	// if (null != items) {
	// int pageSize = rowCount * columnCount;
	// if (items.size() > 0) {
	// pageCount = items.size() / pageSize;
	// if (items.size() % pageSize > 0) {
	// pageCount = pageCount + 1;
	// }
	// }
	// if (items.size() > pageSize) {
	// content = items.subList(0, pageSize - 1);
	// } else {
	// content = items;
	// }
	// }
	// pageItr.setTotalPages(pageCount);
	// pageItr.setContent(content);
	// return pageItr;
	// }

	@Override
	public void setJobPanel(HierarchyJobPanel jobPanel) {
		throw new UnsupportedOperationException("Not supported yet."); // To
																		// change
																		// body
																		// of
																		// generated
																		// methods,
																		// choose
																		// Tools
																		// |
																		// Templates.
	}

	@Override
	public HierarchyItem[] getItems() {
		throw new UnsupportedOperationException("Not supported yet."); // To
																		// change
																		// body
																		// of
																		// generated
																		// methods,
																		// choose
																		// Tools
																		// |
																		// Templates.
	}

	@Override
	public void loadInitialData() {
		System.out.println();
	}

	@Override
	public void showAllStandardHierarchyNodes() {
		this.contextID = CPMClientConstants.CONTEXT_ALL_GROUP;
		showData(contextID);
	}

	@Override
	public void showLinkedNodes() {
		this.contextID = CPMClientConstants.CONTEXT_LINKED_GROUPS;
		showData(contextID);
	}

	@Override
	public void schedule(ScheduleMode mode) {
		viewschedulePane(mode);
	}

	@Override
	public void loadParentPanelData() {
		long[] itemIds = getItemIds();

		PageItr<HierarchyNode> hierachyNodes = null;
		if (null != itemIds && itemIds.length > 0) {
			try {
				if(nodeType == NodeCategory.job){
					hierachyNodes = HierarchyJobService.getConnectedHierarchyNode(itemIds, 0,
							parentPanel.getRowCount() * parentPanel.getColumnCount());
				}else if(nodeType == NodeCategory.jobgroup){
					hierachyNodes = HierarchyJobService.getConnectedHierarchyNodeJobGroup(itemIds, 0,
							parentPanel.getRowCount() * parentPanel.getColumnCount());
				}
				
			} catch (ServiceFailedException ex) {
				ex.printStackTrace();
			}
		}
		parentPanel.loadData(null, false);
		parentPanel.loadData(hierachyNodes, false);
		parentPanel.loadParentPanelData();
	}

	private PageItr<? extends HierarchyItem> getUnApprovedJobData(int pageNumber) {
		PageItr<? extends HierarchyItem> jobs = null;
		try {
			jobs = ScheduleManagementService.getUnApprovedJobSchedules(pageNumber, rowCount * columnCount);
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jobs;
	}

	private PageItr<? extends HierarchyItem> getUnApprovedJobGroupData(int pageNumber) {
		PageItr<? extends HierarchyItem> jobGroups = null;
		try {
			jobGroups = ScheduleManagementService.getUnApprovedJobGroupSchedules(pageNumber, rowCount * columnCount);
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jobGroups;
	}

	@Override
	public void approveSchedule() {
		if (contextID == CPMClientConstants.CONTEXT_LINKED_GROUPS) {
			nodeType = NodeCategory.jobgroup;
		} else {
			nodeType = NodeCategory.job;
		}

		viewschedulePane(ScheduleMode.SCHEDULING);
	}

	private void viewschedulePane(ScheduleMode mode) {
		ObservableList<TablePosition> list = sheet.getSelectionModel().getSelectedCells();
		if (nodeType == NodeCategory.job) {
			List<Job> hierarchyItems = new ArrayList<>();
			if (null != list && list.size() > 0) {
				for (TablePosition tablePosition : list) {
					if (tablePosition != null) {
						int row = tablePosition.getRow();
						int column = tablePosition.getColumn();
						Job item = (Job) sheet.getGrid().getRows().get(row).get(column).getItem();
						hierarchyItems.add(item);
					}
				}

				if (null != hierarchyItems && hierarchyItems.size() > 0) {
					HierarchyItemSchedulePane schedulePane = new HierarchyItemSchedulePane(!approved, hierarchyItems,
							mode, NodeCategory.job);
					FXMLDialog dialog = new FXMLDialog(schedulePane,
							ScreensConfiguration.getInstance().getPrimaryStage());
					dialog.displayAndWait();
					refreshPage();
				}
			}
		} else {
			List<JobGroup> hierarchyItems = new ArrayList<>();
			if (null != list && list.size() > 0) {
				for (TablePosition tablePosition : list) {
					if (tablePosition != null) {
						int row = tablePosition.getRow();
						int column = tablePosition.getColumn();
						JobGroup item = (JobGroup) sheet.getGrid().getRows().get(row).get(column).getItem();
						hierarchyItems.add(item);
					}
				}

				if (null != hierarchyItems && hierarchyItems.size() > 0) {
					HierarchyItemSchedulePane schedulePane = new HierarchyItemSchedulePane(!approved, hierarchyItems,
							mode, NodeCategory.jobgroup);
					FXMLDialog dialog = new FXMLDialog(schedulePane,
							ScreensConfiguration.getInstance().getPrimaryStage());
					dialog.displayAndWait();
					refreshPage();
				}
			}
		}

	}

	@Override
	public void removeHierarchyLink() {
		List<? extends HierarchyItem> selectedItems = getSelectedItems();
		if (null != selectedItems) {
			try {
				if (nodeType == NodeCategory.job) {
					HierarchyNodeService.removeLinkedJobs(selectedItems);
				} else {
					HierarchyNodeService.removeLinkedJobGroups(selectedItems);
				}
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO,
						ClientMessages.LINK_SUCCESSFULLY_REMOVED);
				alertDialog.show();
				refreshPage();
			} catch (ServiceFailedException e) {
				Message errorMessage = e.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}

		}

	}

	@Override
	public void loadFilterData(int context,HierarchyFilter filter, PageItr<? extends HierarchyItem> hierachyNodesPage) {
		//clearSearch();
		setDataContext(context);
		controller.setToggle(context);
		this.currentFilter = filter;
		loadData(null, false);
		loadData(hierachyNodesPage, false);
		loadParentPanelData();
		controller.setFilterLabel(filter);
//		ScreensConfiguration.getInstance().getMainScreen().scrollToBottom();
	}

}
