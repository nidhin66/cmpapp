/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.alert;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;

/**
 *
 * @author hp
 */
public class AlertDialog extends BorderPane implements DialogController {

    private FXMLDialog dialog;
    
    @FXML
    private BorderPane mainPane;
    
    @FXML
    private BorderPane alertHeader;
    
    @FXML
    private Label  headerLabel;
    
    @FXML
    private ImageView alertImage;
    
    @FXML
    private Text alertText;
    
    @FXML
    private AnchorPane alertFooter;
    
    @FXML
    private Button okButton;

    public AlertDialog(AlertDialogType type,String message) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/alert_dialog.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        setStyle(type);
        alertText.setText(message);
    }

    @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }
    
    public void closeAlert(){
        this.dialog.close();
    }
    
    private void setStyle(AlertDialogType type){
      if(type == AlertDialogType.ERROR){
          loadErroAlertStyle();
      }else if(type == AlertDialogType.WARN){
          loadWarnStyle();
      }else if(type == AlertDialogType.INFO){
          loadInfoStyle();
      }    
    }
    
    private void loadInfoStyle(){
        mainPane.getStyleClass().add("info-alert-pane");
        alertHeader.getStyleClass().add("info-alert-header");
        alertImage.getStyleClass().add("info-alert-image");        
        alertText.getStyleClass().add("info-alert-text");
        headerLabel.getStyleClass().add("info-alert-header-label");
        
        alertFooter.getStyleClass().add("alert-footer");
        okButton.getStyleClass().add("alert-ok-button");
        headerLabel.setText("Information");
    }
    
    private void loadWarnStyle(){
        mainPane.getStyleClass().add("warn-alert-pane");
        alertHeader.getStyleClass().add("warn-alert-header");
        alertImage.getStyleClass().add("warn-alert-image");        
        alertText.getStyleClass().add("warn-alert-text");
        headerLabel.getStyleClass().add("warn-alert-header-label");
        
        alertFooter.getStyleClass().add("alert-footer");
        okButton.getStyleClass().add("alert-ok-button");
        headerLabel.setText("Warning");
       
     
    }
    
    private void loadErroAlertStyle(){
        mainPane.getStyleClass().add("error-alert-pane");
        alertHeader.getStyleClass().add("error-alert-header");
        alertImage.getStyleClass().add("error-alert-image"); 
        alertText.getStyleClass().add("error-alert-text");
        headerLabel.getStyleClass().add("error-alert-header-label");
        
        alertFooter.getStyleClass().add("alert-footer");
        okButton.getStyleClass().add("alert-ok-button");
        headerLabel.setText("Error");
       
        
    }
    
    public void setAlertMessage(String message){
        alertText.setText(message);
    }
    
    public void show(){
         FXMLDialog fxmldialog = new FXMLDialog(this, ScreensConfiguration.getInstance().getCurrentParentWindow());
         fxmldialog.display();
    }
    

}
