/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TablePosition;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.StageStyle;

import org.controlsfx.control.spreadsheet.GridBase;
import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetColumn;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import com.gamillusvaluegen.cpm.ui.hierarchy.schedule.HierarchyItemSchedulePane;
import com.gamillusvaluegen.cpm.ui.hierarchy.schedule.ScheduleMode;
import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.ClientMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.edit.HierarchyEditItemField;
import com.gamillusvaluegen.cpm.ui.hierarchy.edit.HierarchyNodeItemController;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilterUtility;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class HierarchyNodePanel extends HierarchyItemPanel {

    private BorderPane rootPane;

    private SpreadsheetView sheet;

    private int rowHight = 30;
    private int ColumnWidth = CPMClientConstants.DEFAULT_COLUMN_WIDTH;

   

    private HierarchyContextMenu contextMenu;

    private PageItr<? extends HierarchyItem> currentPage;

   

    private HierarchyJobPanel jobPanel;

    public HierarchyNodePanel(HierarchyLevel level,boolean approved, int rowCount, int columnCount, int columnWidth) {
    	this.approved = approved;
        this.nodeType = NodeCategory.hierarchy;
        this.ColumnWidth = columnWidth;
        contextID = 2;
        contextMenu = new HierarchyContextMenu(this);
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.level = level;
        createPanel();
        rootPane.setCenter(sheet);
//        controller.setTitle(level.getLevelName());
        controller.createShowMoreMenus();
    }

    public HierarchyNodePanel(HierarchyLevel level,boolean approved, int rowCount, int columnCount) {
        this(level, approved , rowCount, columnCount, CPMClientConstants.DEFAULT_COLUMN_WIDTH);
    }

    public HierarchyNodePanel(String levelName,boolean approved,  int rowCount, int columnCount) {
        this(new HierarchyLevel(levelName),approved ,rowCount, columnCount);
    }

    public HierarchyNodePanel(boolean approved, int rowCount, int columnCount) {
        this(new HierarchyLevel("Job"), approved,rowCount, columnCount);
    }

    private void createPanel() {
        /**
         * Create panel with the search box and the add button
         */
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/plant_hierarchy_node.fxml"));
        try {
            rootPane = loader.load();
            rootPane.getStyleClass().add("hierarchyNode-pane");
            controller = loader.getController();
            controller.setNodePanel(this);
        } catch (IOException ex) {
            ex.printStackTrace();
//            Logger.getLogger(SampleController.class.getName()).log(Level.SEVERE, null, ex);
        }

        /**
         * Create SpreadSheet
         */
        GridBase grid = new GridBase(rowCount, columnCount);

        sheet = new SpreadsheetView();
        sheet.setGrid(grid);
        sheet.setContextMenu(null);

        sheet.setShowColumnHeader(false);
        sheet.setShowRowHeader(false);
        sheet.setEditable(false);
        sheet.setPrefSize((columnCount * ColumnWidth) + 5, (rowCount * rowHight) + 5);

        for (SpreadsheetColumn column : sheet.getColumns()) {
            column.setPrefWidth(ColumnWidth);
        }

        double height = (rowCount * rowHight) + 20 + 15 + 2;
        rootPane.setPrefHeight(height);

//        sheet.setOnKeyPressed(new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent event) {
//                KeyCode keyCode = event.getCode();
//                if (keyCode == KeyCode.LEFT) {
//                    selectFocusedCell();
//                    childPanel.showData(CONTEXT_LINKED);
//                } else if (keyCode == KeyCode.UP) {
//                    selectFocusedCell();
//                    childPanel.showData(CONTEXT_LINKED);
//                } else if (keyCode == KeyCode.DOWN) {
//                    selectFocusedCell();
//                    childPanel.showData(CONTEXT_LINKED);
//                } else if (keyCode == KeyCode.RIGHT) {
//                    selectFocusedCell();
//                    childPanel.showData(CONTEXT_LINKED);
//                }
//            }
//
//        });
        sheet.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        if (e.getButton() == MouseButton.SECONDARY) {
                            if (null != contextMenu && contextMenu.isShowing()) {
                                contextMenu.hide();
                            }
                            Map<String, HierarchyItem> selectedParentNodes = null;
                            if (contextID == CPMClientConstants.CONTEXT_ALL) {
                                if (null != parentPanel) {
                                    selectedParentNodes = new HashMap<>();
                                    HierarchyItem selectedParentNode = parentPanel.getSelectedItem();
                                    if (null != selectedParentNode) {
                                        selectedParentNodes.put(parentPanel.getLevel().getLevelName(), selectedParentNode);
                                    }
                                }
                            }

                            contextMenu.setMenuItems(selectedParentNodes);
                            contextMenu.show(sheet, e.getScreenX(), e.getScreenY());
                        } else if (e.getButton() == MouseButton.PRIMARY) {
                            if (e.getClickCount() == 2) {
                                HierarchyEditItemField itemField = new HierarchyEditItemField(level, (StandardHierarchyNode) getSelectedItem(), "V");
                                FXMLDialog addNodeItemDialog = new FXMLDialog(new HierarchyNodeItemController(itemField), getClass().getResource("/fxml/node_item.fxml"), ScreensConfiguration.getInstance().getCurrentParentWindow(), StageStyle.UNDECORATED, 0, 0);
                                addNodeItemDialog.display();
                            } else {
                            	if(approved){
                            		 try {
                                         ScreensConfiguration.getInstance().showCursor();
                                         if (null != childPanel) {
                                        	 childPanel.clearSearch();
                                             childPanel.showLinkedNodes();
                                         }
                                         createHierarchyBreadCrumb();
                                         jobPanel.clearSearch();
                                         jobPanel.showData(3);
                                     } catch (Exception ex) {
                                         ex.printStackTrace();
                                     } finally {
                                         ScreensConfiguration.getInstance().hideCursor();
                                     }
                            	}
                               
                            }

                        }

                    }
                });

        loadData(null, true);

    }

    @Override
    public void loadData(PageItr<? extends HierarchyItem> hierachyNodesPage,boolean loadChild) {
        this.currentPage = hierachyNodesPage;
        sheet.getSelectionModel().clearSelection();
        List<? extends HierarchyItem> hierachyNodes = null;
        if (null != hierachyNodesPage) {
            hierachyNodes = hierachyNodesPage.getContent();
        }

        GridBase grid = new GridBase(rowCount, columnCount);
        ObservableList<ObservableList<SpreadsheetCell>> rows = FXCollections.observableArrayList();
        for (int row = 0; row < grid.getRowCount(); ++row) {
            final ObservableList<SpreadsheetCell> list = FXCollections.observableArrayList();
            for (int column = 0; column < grid.getColumnCount(); ++column) {
                int index = (row * grid.getColumnCount()) + column;
                if (null != hierachyNodes) {
                    if (index < hierachyNodes.size()) {
                        list.add(generateCell(hierachyNodes.get(index), row, column, 1, 1));
                    } else {
                        list.add(generateCell(null, row, column, 1, 1));
                    }
                } else {
                    list.add(generateCell(null, row, column, 1, 1));
                }

            }
            rows.add(list);
        }
        grid.setRows(rows);
        sheet.setGrid(grid);

        String paginationLabel = null;
        if (null != currentPage) {
            if (currentPage.getTotalPages() > 0) {
                paginationLabel = currentPage.getCurrentPage() + 1 + " of " + currentPage.getTotalPages();
            }
        }
        controller.setPaginationLabel(paginationLabel);
//        if (null != childPanel && null != hierachyNodesPage) {
        if (null != childPanel && approved && loadChild) {
            childPanel.loadInitialData();
        }else if(null != jobPanel && loadChild){
        	jobPanel.showData(CPMClientConstants.CONTEXT_LINKED_GROUPS);
        }
    }

    private SpreadsheetCell generateCell(HierarchyItem hierachyNode, int row, int column, int rowSpan, int colSpan) {
        SpreadsheetCell cell = new HierarchyNodeCell(row, column, rowSpan, colSpan, hierachyNode);
//        cell.getStyleClass().add("row_header");

        return cell;
    }

    public BorderPane getRootPane() {
        return rootPane;
    }

    public PageItr<? extends StandardHierarchyNode> showAllItems() {

        try {
            

            PageItr<? extends StandardHierarchyNode> hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), 0, columnCount * rowCount);
            return hierachyNodesPage;
        } catch (ServiceFailedException ex) {
            Message errorMessage = ex.getErroMessage();
            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
            alertDialog.show();
        }
        return null;

    }

    public PageItr<? extends StandardHierarchyNode> showLinkedData() {

        

        StandardHierarchyNode parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
        PageItr<? extends StandardHierarchyNode> hierachyNodesPage = null;
        if (null != parent) {
            try {
                //            if(contextID == CONTEXT_ALL){
//                HierarchyNodeService.getLinkedStandardHierarchyNodes(parent.getId(), 0, columnCount * rowCount);
//            }else{
                hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), 0, columnCount * rowCount);
//            }            
            } catch (ServiceFailedException ex) {
                Message errorMessage = ex.getErroMessage();
                AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
                alertDialog.show();
            }
        }
        return hierachyNodesPage;

    }

    @Override
    public HierarchyItem getSelectedItem() {
        ObservableList<TablePosition> list = sheet.getSelectionModel().getSelectedCells();
        if (null != list && list.size() > 0) {
            TablePosition tablePosition = list.get(0);
            if (tablePosition != null) {
                int row = tablePosition.getRow();
                int column = tablePosition.getColumn();
                return (StandardHierarchyNode) sheet.getGrid().getRows().get(row).get(column).getItem();
            } else {
                return null;
            }
        }
        return null;
    }
    
    private  PageItr<? extends HierarchyItem> getPage(int pageNumber){
    	PageItr<? extends HierarchyItem> hierachyNodesPage = null;
    	 try {
    		 String searchText = controller.getSearchText();
    		 if(null != currentFilter){
    			 hierachyNodesPage =  HierarchyFilterUtility.getPage(nodeType,pageNumber,  columnCount * rowCount, currentFilter);
    		 }else if(Utilities.isNullOrEmpty(searchText)){
    			 if (contextID == CPMClientConstants.CONTEXT_LINKED) {
                     StandardHierarchyNode parent = null;
                    	if(null != parentPanel){
                    		parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
                    	}
                     if (null != parent) {
                         hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), pageNumber, columnCount * rowCount);
                     } else if (parentPanel != null) {
                         hierachyNodesPage = HierarchyNodeService.getChildHierarchyNodes(parentPanel.getItemIds(), pageNumber, columnCount * rowCount);
                     }
                 } else if (contextID == CPMClientConstants.CONTEXT_ALL) {
                     hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), pageNumber, columnCount * rowCount);
                 }
    		 }else{
    			 hierachyNodesPage = getSearchPage(searchText, pageNumber);
    		 }
            
         } catch (ServiceFailedException ex) {
             ex.printStackTrace();
         }
    	 return hierachyNodesPage;
    }

    @Override
    public void loadFirstPage() {
        PageItr<? extends HierarchyItem> hierachyNodesPage = null;
//        try {
//            if (contextID == CONTEXT_LINKED) {
//                StandardHierarchyNode parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
//                if (null != parent) {
//                    hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), 0, columnCount * rowCount);
//                } else if (parentPanel != null) {
//                    hierachyNodesPage = HierarchyNodeService.getChildHierarchyNodes(parentPanel.getItemIds(), 0, columnCount * rowCount);
//                }
//            } else if (contextID == CONTEXT_ALL) {
//                hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), 0, columnCount * rowCount);
//            }
//        } catch (ServiceFailedException ex) {
//            ex.printStackTrace();
//        }
        hierachyNodesPage = getPage(0);
        loadData(hierachyNodesPage, true);
    }

    @Override
    public void loadNextPage() {
        if (null != currentPage) {
            if ((currentPage.getCurrentPage() + 1) < currentPage.getTotalPages()) {
                PageItr<? extends HierarchyItem> hierachyNodesPage = null;
                hierachyNodesPage = getPage(currentPage.getCurrentPage() + 1);
//                try {
//                    if (contextID == CONTEXT_LINKED) {
//                        StandardHierarchyNode parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
//                        if (null != parent) {
//                            hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), currentPage.getCurrentPage() + 1, columnCount * rowCount);
//                        } else if (parentPanel != null) {
//                            hierachyNodesPage = HierarchyNodeService.getChildHierarchyNodes(parentPanel.getItemIds(), currentPage.getCurrentPage() + 1, columnCount * rowCount);
//                        }
//
//                    } else if (contextID == CONTEXT_ALL) {
//                        hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), currentPage.getCurrentPage() + 1, columnCount * rowCount);
//                    }
//                } catch (ServiceFailedException ex) {
//                    ex.printStackTrace();
//                }
                loadData(hierachyNodesPage, true);
            }
        }

    }

    @Override
    public void loadPreviousPage() {
        if (null != currentPage) {
            if ((currentPage.getCurrentPage()) > 0) {
                PageItr<? extends HierarchyItem> hierachyNodesPage = null;
                hierachyNodesPage = getPage(currentPage.getCurrentPage() - 1);
//                try {
//                    if (contextID == CONTEXT_LINKED) {
//                        StandardHierarchyNode parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
//                        if (null != parent) {
//                            hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), currentPage.getCurrentPage() - 1, columnCount * rowCount);
//                        } else if (parentPanel != null) {
//                            hierachyNodesPage = HierarchyNodeService.getChildHierarchyNodes(parentPanel.getItemIds(), currentPage.getCurrentPage() - 1, columnCount * rowCount);
//                        }
//
//                    } else if (contextID == CONTEXT_ALL) {
//                        hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), currentPage.getCurrentPage() - 1, columnCount * rowCount);
//                    }
//                } catch (ServiceFailedException ex) {
//                    ex.printStackTrace();
//                }
                loadData(hierachyNodesPage, true);
            }
        }

    }

    @Override
    public void loadLastPage() {
        PageItr<? extends HierarchyItem> hierachyNodesPage = null;
        hierachyNodesPage = getPage(currentPage.getTotalPages() - 1);
//        try {
//            if (contextID == CONTEXT_LINKED) {
//                StandardHierarchyNode parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
//                if (null != parent) {
//                    hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), currentPage.getTotalPages() - 1, columnCount * rowCount);
//                } else if (parentPanel != null) {
//                    hierachyNodesPage = HierarchyNodeService.getChildHierarchyNodes(parentPanel.getItemIds(), currentPage.getTotalPages() - 1, columnCount * rowCount);
//                }
//            } else if (contextID == CONTEXT_ALL) {
//                hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), currentPage.getTotalPages() - 1, columnCount * rowCount);
//            }
//        } catch (ServiceFailedException ex) {
//            ex.printStackTrace();
//        }
        loadData(hierachyNodesPage, true);
    }

    @Override
    public List<? extends HierarchyItem> searchData(String text) {
    	PageItr<? extends HierarchyItem> hierachyNodesPage = null;
    	if(null != currentFilter){
    		currentFilter.setSearchString(text + "%");
			try {
				hierachyNodesPage = HierarchyFilterUtility.getPage(nodeType, 0,  columnCount * rowCount, currentFilter);
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else{
    		if(Utilities.isNullOrEmpty(text)){
        		
        		if (contextID == CPMClientConstants.CONTEXT_LINKED) {
        			if (null == parentPanel) {
        				showLinkedHierarchyNodes();
        			}else {
        				if(null == parentPanel.getSelectedItem()){
        					loadInitialData();
        				}else {
        					showLinkedHierarchyNodes();
        				}
        			}
        					
        		}else if (contextID == CPMClientConstants.CONTEXT_ALL) {
        			showAllStandardHierarchyNodes();
        		}
        		
        		
        	}else{
//        		 text = text + "%";
//        	        try {
//        	            if (contextID == CONTEXT_LINKED) {
//        	                StandardHierarchyNode parent = null;
//        	                if (null != parentPanel) {
//        	                    parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
//        	                }
//        	                if (null != parent) {
//        	                    hierachyNodesPage = HierarchyNodeService.searchHierarchyNodesInChild(text, parent.getId(), 0, columnCount * rowCount);
//        	                } else{
//        	                	 hierachyNodesPage = HierarchyNodeService.searchHierarchyNodesInLevel(text, level.getLevelName(), 0, columnCount * rowCount);
//        	                }
//        	            } else if (contextID == CONTEXT_ALL) {
//        	            	hierachyNodesPage = HierarchyNodeService.searchStandardHierarchyNodesInLevel(text, level.getLevelName(), 0, columnCount * rowCount);
//        	            }
//        	        } catch (ServiceFailedException ex) {
//        	            ex.printStackTrace();
//        	        }
        		hierachyNodesPage = getSearchPage(text, 0);
        	        loadData(hierachyNodesPage, true);
        	       
        	}
    	}
    
    	
    	 List<? extends HierarchyItem> hiearchNodes = new ArrayList<>();
	        if (null != hierachyNodesPage) {
	            hiearchNodes = hierachyNodesPage.getContent();
	      }
//	        if (contextID == CONTEXT_LINKED) {	        	
//					loadParentPanelData();			
//	        }
	    	
       
        return hiearchNodes;
    }
    
    private PageItr<? extends StandardHierarchyNode> getSearchPage(String searchText,int pageNumber){
    	PageItr<? extends StandardHierarchyNode> hierachyNodesPage = null;
    	searchText = searchText + "%";
        try {
            if (contextID == CPMClientConstants.CONTEXT_LINKED) {
                StandardHierarchyNode parent = null;
                if (null != parentPanel) {
                    parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
                }
                if (null != parent) {
                    hierachyNodesPage = HierarchyNodeService.searchHierarchyNodesInChild(searchText, parent.getId(), pageNumber, columnCount * rowCount);
                } else{
                	 hierachyNodesPage = HierarchyNodeService.searchHierarchyNodesInLevel(searchText, level.getLevelName(), pageNumber, columnCount * rowCount);
                }
            } else if (contextID == CPMClientConstants.CONTEXT_ALL) {
            	hierachyNodesPage = HierarchyNodeService.searchStandardHierarchyNodesInLevel(searchText, level.getLevelName(), pageNumber, columnCount * rowCount);
            }
        } catch (ServiceFailedException ex) {
            ex.printStackTrace();
        }
        return hierachyNodesPage;
    }

    @Override
    public long[] getItemIds() {
        long[] parentIds = null;
        if (null != currentPage) {
            List<? extends HierarchyItem> hiearchNodes = currentPage.getContent();
            if (null != hiearchNodes) {
                parentIds = new long[hiearchNodes.size()];
                int i = 0;
                for (HierarchyItem hierarchyNode : hiearchNodes) {
                    parentIds[i] = hierarchyNode.getId();
                    i++;
                }
            } else {
                parentIds = new long[0];
            }
        } else {
            parentIds = new long[0];
        }
        return parentIds;
    }

    @Override
    public void loadInitialData() {
    	//clearSearch();
        long[] parentIds = null;        
        HierarchyItem parentItem = parentPanel.getSelectedItem();
        if(null != parentItem){
        	parentIds = new long[1];
        	parentIds[0] = parentItem.getId();
        }else{
        	parentIds = parentPanel.getItemIds();
        }        
        PageItr<? extends StandardHierarchyNode> page = null;
        try {
            if (parentPanel.isStandardHierarchy()) {
                standardHierarchy = true;
                page = HierarchyNodeService.getChildStandardHierarchyNode(parentIds, 0, columnCount * rowCount);
            } else {
                standardHierarchy = false;
                page = HierarchyNodeService.getChildHierarchyNodes(parentIds, 0, columnCount * rowCount);
            }
        } catch (ServiceFailedException ex) {
            ex.printStackTrace();
        }
        loadData(page, true);
//        if(null != childPanel){
//            childPanel.loadInitialData();
//        }
    }

    public void selectFocusedCell() {
        int row = sheet.getSelectionModel().getFocusedCell().getRow();
        int column = sheet.getSelectionModel().getFocusedCell().getColumn();
        SpreadsheetColumn sheetColumn = sheet.getColumns().get(column);
        sheet.getSelectionModel().clearAndSelect(row, sheetColumn);
    }

    public void selectFristCell() {
        SpreadsheetColumn sheetColumn = sheet.getColumns().get(0);
        sheet.getSelectionModel().clearAndSelect(0, sheetColumn);
        jobPanel.showData(3);
    }

    public HierarchyItemPanel getParentPanel() {
        return parentPanel;
    }

    public void setParentPanel(HierarchyItemPanel parentPanel) {
        this.parentPanel = parentPanel;
    }

    @Override
    public void linkToParent(HierarchyItem parentItem) {
        try {
            long parentID = parentItem.getId();
            List<? extends HierarchyItem> hierarchyItems = getSelectedItems();
            long[] nodeIds = new long[hierarchyItems.size()];
            int i = 0;
            for (HierarchyItem item : hierarchyItems) {
                nodeIds[i] = item.getId();
                i++;
            }
            HierarchyNodeService.linkHierarchyNodes(parentID, nodeIds);
            AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Selected items linked to " + parentItem.getName());
			alertDialog.show();
        } catch (ServiceFailedException ex) {
            Message errorMessage = ex.getErroMessage();
            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
            alertDialog.show();
        }
    }
    
    @Override
    public void removeHierarchyLink(){
    	List<? extends HierarchyItem> items = getSelectedItems();
    	if(null != items){
    		try {
				HierarchyNodeService.removeHierarchyNodeLink(items);
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, ClientMessages.LINK_SUCCESSFULLY_REMOVED);
		        alertDialog.show();
		        refreshPage();
			} catch (ServiceFailedException e) {
				Message errorMessage = e.getErroMessage();
	            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
	            alertDialog.show();
			}
    	}
    }

    @Override
    public List<? extends HierarchyItem> getSelectedItems() {
        ObservableList<TablePosition> list = sheet.getSelectionModel().getSelectedCells();
        List<HierarchyItem> hierarchyItems = new ArrayList<>();
        if (null != list && list.size() > 0) {
            for (TablePosition tablePosition : list) {
                if (tablePosition != null) {
                    int row = tablePosition.getRow();
                    int column = tablePosition.getColumn();
                    HierarchyItem item = (HierarchyItem) sheet.getGrid().getRows().get(row).get(column).getItem();
                    hierarchyItems.add(item);
                }
            }

        }
        return hierarchyItems;
    }

    @Override
    public void showAllStandardHierarchyNodes() {
        try {
            //        if (null == parentPanel) {
//                showLinkedHierarchyNodes();
//        } else {
            contextID = CPMClientConstants.CONTEXT_ALL;
            standardHierarchy = true;
           
            loadData(null, true);
            PageItr<? extends StandardHierarchyNode> hierachyNodesPage = HierarchyNodeService.getAllStandardHierarchyNodes(level.getLevelName(), 0, columnCount * rowCount);
            loadData(hierachyNodesPage, true);
            this.controller.setToggle(contextID);
//        }
        } catch (ServiceFailedException ex) {
            Message errorMessage = ex.getErroMessage();
            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
            alertDialog.show();
        }

    }

    @Override
    public void showLinkedNodes() {
    	clearSearch();
        if (null == parentPanel) {
            showLinkedHierarchyNodes();
        } else {
            if (parentPanel.isStandardHierarchy()) {
                showLinkedStandardHierarchyNodes();
            } else {
                showLinkedHierarchyNodes();
            }
        }
    }

    public void showLinkedStandardHierarchyNodes() {
        contextID = CPMClientConstants.CONTEXT_ALL;
        standardHierarchy = true;
        
        loadData(null, true);
        StandardHierarchyNode parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
        PageItr<? extends StandardHierarchyNode> hierachyNodesPage = null;
        if (null != parent) {
            try {
                hierachyNodesPage = HierarchyNodeService.getLinkedStandardHierarchyNodes(parent.getId(), 0, columnCount * rowCount);

            } catch (ServiceFailedException ex) {
                Message errorMessage = ex.getErroMessage();
                AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
                alertDialog.show();
            }
            loadData(hierachyNodesPage, true);
        }
        this.controller.setToggle(contextID);
    }

    public void showLinkedHierarchyNodes() {
        contextID = CPMClientConstants.CONTEXT_LINKED;
        standardHierarchy = false;
       
        loadData(null, true);
        StandardHierarchyNode parent = null;
        if (null == parentPanel) {
            parent = ClientConfig.getInstance().getPlant();
            jobPanel.clearSearch();
        } else {
            parent = (StandardHierarchyNode) parentPanel.getSelectedItem();
        }

        PageItr<? extends StandardHierarchyNode> hierachyNodesPage = null;
        if (null != parent) {
            try {
                hierachyNodesPage = HierarchyNodeService.getLinkedHierarchyNodes(parent.getId(), 0, columnCount * rowCount);
            } catch (ServiceFailedException ex) {
                Message errorMessage = ex.getErroMessage();
                AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
                alertDialog.show();
            }
            loadData(hierachyNodesPage, true);
        }
        this.controller.setToggle(contextID);
    }

//    @Override
//    public void showDefaultContextData(){
//        this.contextID = CONTEXT_LINKED; 
//        showData(contextID);
//    }
//    @Override
//    public void showData(int contextID) {
//            this.contextID = contextID;
//        PageItr<? extends HierarchyItem> data = null;
//        if(contextID == CONTEXT_ALL){
//            data = showAllItems();
//        }else if(contextID == CONTEXT_LINKED){
//             data = showLinkedData();
//        }
//        sheet.getSelectionModel().clearSelection();
//        loadData(null);
//        loadData(data);
//    }
    @Override
    public void setJobPanel(HierarchyJobPanel jobPanel) {
        this.jobPanel = jobPanel;
    }

    @Override
    public HierarchyItem[] getItems() {
        HierarchyItem[] hierarchyItems = null;
        if (null != currentPage) {
            List<? extends HierarchyItem> hiearchNodes = currentPage.getContent();
            if (null != hiearchNodes) {
                hierarchyItems = new HierarchyItem[hiearchNodes.size()];
                int i = 0;
                for (HierarchyItem hierarchyNode : hiearchNodes) {
                    hierarchyItems[i] = hierarchyNode;
                    i++;
                }
            } else {
                hierarchyItems = new HierarchyItem[0];
            }
        } else {
            hierarchyItems = new HierarchyItem[0];
        }
        return hierarchyItems;
    }

    @Override
    public void schedule(ScheduleMode mode) {
       viewschedulePane(mode);
    }

    @Override
    public void loadParentPanelData() {
        if (null != parentPanel) {
            long[] itemIds = getItemIds();
            PageItr<HierarchyNode> hierachyNodes = null;
            if (null != itemIds && itemIds.length > 0) {
                try {
                    hierachyNodes = HierarchyNodeService.getParentHierarchyNodes(itemIds, 0, parentPanel.rowCount * parentPanel.columnCount);
                } catch (ServiceFailedException ex) {
                    ex.printStackTrace();
                }
            }
            parentPanel.loadData(null, false);
            parentPanel.loadData(hierachyNodes, false);
            parentPanel.loadParentPanelData();
            
        }
       // clearSearch();
    }

	@Override
	public void approveSchedule() {
		viewschedulePane(ScheduleMode.SCHEDULING);		
	}
	
	private void viewschedulePane(ScheduleMode mode){
		 ObservableList<TablePosition> list = sheet.getSelectionModel().getSelectedCells();
	        if (nodeType == NodeCategory.hierarchy) {
	            List<HierarchyNode> hierarchyItems = new ArrayList<>();
	            if (null != list && list.size() > 0) {
	                for (TablePosition tablePosition : list) {
	                    if (tablePosition != null) {
	                        int row = tablePosition.getRow();
	                        int column = tablePosition.getColumn();
	                        HierarchyNode item = (HierarchyNode) sheet.getGrid().getRows().get(row).get(column).getItem();
	                        hierarchyItems.add(item);
	                    }
	                }

	                if (null != hierarchyItems && hierarchyItems.size() > 0) {
	                    HierarchyItemSchedulePane schedulePane = new HierarchyItemSchedulePane(!approved,hierarchyItems,mode, NodeCategory.hierarchy);
	                    FXMLDialog dialog = new FXMLDialog(schedulePane, ScreensConfiguration.getInstance().getPrimaryStage());
	                    dialog.displayAndWait();
	                    refreshPage();
	                }
	            }
	        }
	}

	@Override
	public void refreshPage() {
		if (null != currentPage) {		
			int pageNumber = currentPage.getCurrentPage();
			PageItr<? extends HierarchyItem> pageData = getPage(pageNumber);		
			loadData(pageData, true);
		}
		
	}

	@Override
	public void loadFilterData(int context, HierarchyFilter filter,
			PageItr<? extends HierarchyItem> hierachyNodesPage) {
//		 clearSearch();
		 this.currentFilter = filter;
		 controller.setToggle(context);
		 loadData(null, true);;
		 loadData(hierachyNodesPage, true);
         loadParentPanelData();
         controller.setFilterLabel(filter);
//         ScreensConfiguration.getInstance().getMainScreen().scrollToTop();
	}
    
  

}
