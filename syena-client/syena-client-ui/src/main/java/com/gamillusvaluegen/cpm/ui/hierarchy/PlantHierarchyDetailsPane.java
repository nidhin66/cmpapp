/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.io.IOException;
import java.util.List;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.checklist.CheckListPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.dependency.DependencyPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.search.AdvancedSearchPopup;
import com.gamillusvaluegen.cpm.ui.hierarchy.search.QuickSearchPopup;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;

/**
 *
 * @author hp
 */
public class PlantHierarchyDetailsPane extends BorderPane {

    @FXML
    private AnchorPane hierarchyPane;

    @FXML
    private AnchorPane unitPane;

    @FXML
    private AnchorPane systemPane;

    @FXML
    private AnchorPane subSystemPane;

    @FXML
    private AnchorPane loopPane;

    @FXML
    private AnchorPane jobPane;

//    @FXML
//    private AnchorPane breadCrumbPane;

    @FXML
    private TabPane dependencyTab;

//    @FXML
//    private Button advancedSearchBtn;
    
//    @FXML
//    private Button quickSearchBtn;
    
    @FXML
    private TitledPane collapsibleNode;
    
    private HierarchyNodePanel panel1 = null;
    HierarchyJobPanel jobPanel =  null;
    
    
    private boolean approved = true;

    public PlantHierarchyDetailsPane(boolean approved) {
    	this.approved = approved;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/plant_hierarchy_details.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        collapsibleNode.setExpanded(false);
    }

    @FXML
    private void initialize() {

        HierarchyLevel level = null;
        try {
            level = HierarchyNodeService.getHierarchyLevels("Unit");
        } catch (ServiceFailedException ex) {
           ex.printStackTrace();
        }
        ClientConfig.getInstance().setHierarchyLevel(level);

        
        HierarchyNodePanel panel2 = null;
        HierarchyNodePanel panel3 = null;
        HierarchyNodePanel panel4 = null;

       
        panel1 = new HierarchyNodePanel(level, approved,CPMClientConstants.UNIT_PANEL_ROW_COUNT, CPMClientConstants.UNIT_PANEL_COLUMN_COUNT,CPMClientConstants.UNIT_COLUMN_WIDTH);
        unitPane.getChildren().add(panel1.getRootPane());
        if(approved){
        	ClientConfig.getInstance().setHierarchyItemPanel(level.getLevelName(), panel1);
        }
        

        for (HierarchyLevel systemLevel : level.getChildren()) {
            panel2 = new HierarchyNodePanel(systemLevel,approved, CPMClientConstants.SYSTEM_PANEL_ROW_COUNT, CPMClientConstants.SYSTEM_PANEL_COLUMN_COUNT, CPMClientConstants.SYSTEM_COLUMN_WIDTH);
            if(approved){
            	ClientConfig.getInstance().setHierarchyItemPanel(systemLevel.getLevelName(), panel2);
            }
            
            systemPane.getChildren().add(panel2.getRootPane());
            for (HierarchyLevel sysbsystemLevel : systemLevel.getChildren()) {
                panel3 = new HierarchyNodePanel(sysbsystemLevel,approved, CPMClientConstants.SUB_SYSTEM_PANEL_ROW_COUNT, CPMClientConstants.SUB_SYSTEM_PANEL_COLUMN_COUNT);
                if(approved){
                	 ClientConfig.getInstance().setHierarchyItemPanel(sysbsystemLevel.getLevelName(), panel3);
                }
               
                subSystemPane.getChildren().add(panel3.getRootPane());
                for (HierarchyLevel loopLevel : sysbsystemLevel.getChildren()) {
                    panel4 = new HierarchyNodePanel(loopLevel,approved, CPMClientConstants.LOOP_PANEL_ROW_COUNT, CPMClientConstants.LOOP_PANEL_COLUMN_COUNT);
                    if(approved){
                    	 ClientConfig.getInstance().setHierarchyItemPanel(loopLevel.getLevelName(), panel4);
                    }                   
                    loopPane.getChildren().add(panel4.getRootPane());
                }
            }
        }

      
        jobPanel =	new HierarchyJobPanel("Job",approved, CPMClientConstants.JOB_PANEL_ROW_COUNT, CPMClientConstants.JOB_PANEL_COLUMN_COUNT);
      
      
        if(approved){
        	  
        	  ClientConfig.getInstance().setHierarchyJobPanel(jobPanel);
        }
        jobPane.getChildren().add(jobPanel.getRootPane());

//        hierarchyPane.setPrefHeight(1000);
//        hierarchyPane.setPrefWidth(1300);

        panel1.setChildPanel(panel2);
        panel1.setJobPanel(jobPanel);

        panel2.setParentPanel(panel1);
        panel2.setChildPanel(panel3);
        panel2.setJobPanel(jobPanel);

        panel3.setParentPanel(panel2);
        panel3.setChildPanel(panel4);
        panel3.setJobPanel(jobPanel);

        panel4.setParentPanel(panel3);
        panel4.setJobPanel(jobPanel);
//        panel4.setChildPanel(jobPanel);

        jobPanel.setParentPanel(panel4);
        if(approved){
        	  loadPlantHierarchy();
        }      

        if(approved){
        	  Tab checkListTab = new Tab("Check List");
              checkListTab.getStyleClass().add("dependency-tab");
              CheckListPane checkListPane = new CheckListPane();
              checkListTab.setContent(checkListPane);
              checkListPane.setJobPanel(jobPanel);
              jobPanel.setCheckListPane(checkListPane);

              Tab dependenciesJobTab = new Tab("Dependencies");
//            dependenciesJobTab.getStyleClass().add("dependency-tab");
              DependencyPane dependencyPane = new DependencyPane();
              dependenciesJobTab.setContent(dependencyPane);

              ClientConfig.getInstance().setDependencyPane(dependencyPane);

              dependencyTab.getTabs().addAll(checkListTab, dependenciesJobTab);

//              breadCrumbPane.getChildren().add(panel1.createHierarchyBreadCrumb());

        }
      
    }

//    @FXML
//    private void showAdvancedSearch() {
//        AdvancedSearchPopup popup = new AdvancedSearchPopup(advancedSearchBtn);
//        popup.show(ScreensConfiguration.getInstance().getPrimaryStage());     
//    }
//    
//    @FXML
//    private void showQuickSearch() {
//        QuickSearchPopup popup = new QuickSearchPopup(quickSearchBtn);
//        popup.show(ScreensConfiguration.getInstance().getPrimaryStage());
//    }
    
    
    public void loadUnApprovedData(){
//    	ClientConfig.getInstance().getHierarchyJobPanel().loadUnApprovedJobData();
//    	jobPanel.loadUnApprovedJobData();
    	jobPanel.showData(3);
    	panel1.loadUnApprovedData();
    }
    
    public void loadPlantHierarchy(){
           try {
            PageItr<? extends HierarchyNode> hierachyNodesPage = HierarchyNodeService.getAllHierarchyNodes("Plant", 0, 5 * 3);
            List<? extends HierarchyNode> plants = hierachyNodesPage.getContent();
            HierarchyNode plant = plants.get(0);
            ClientConfig.getInstance().setPlant(plant);
            panel1.loadData(HierarchyNodeService.getLinkedHierarchyNodes(plant.getId(), 0, 5 * 3), true);

//            panel2.loadInitialData();
//            panel3.loadInitialData();
//            panel4.loadInitialData();

            panel1.selectFristCell();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
