/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.CheckListManagementServiceNames;

/**
 *
 * @author hp
 */
public class CheckListClientService {

    private static final String url = ScreensConfiguration.getUrl();

//    private static RestCaller checkListManagementClient = new RestCaller(url+"/checkListManagement");
    private static RestCaller createRestCallser() {
        RestCaller checkListManagementClient = new RestCaller(url);
        checkListManagementClient.path("checkListManagement");
        return checkListManagementClient;
    }

    public static void createCheckLists(List<CheckList> checkLists) throws ServiceFailedException {
        RestCaller checkListManagementClient = createRestCallser();
        checkListManagementClient.path(CheckListManagementServiceNames.createCheckLists);
        try {
            checkListManagementClient.post(checkLists, List.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_001, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static PageItr<CheckList> getAllStandardCheckList(int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller checkListManagementClient = createRestCallser();
        checkListManagementClient.path(CheckListManagementServiceNames.getAllStandardCheckList);
        checkListManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<CheckList> checklist = null;
        try {
            TypeReference<PageItr<CheckList>> type = new TypeReference<PageItr<CheckList>>() {
            };
            checklist = checkListManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_002, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
            
        }
        return checklist;
    }
    
    public static PageItr<CheckList> getCheckListByParent(long parentId, int pageNumber,int pageSize) throws ServiceFailedException {
    	RestCaller checkListManagementClient = createRestCallser();
        checkListManagementClient.path(CheckListManagementServiceNames.getCheckListByParent);
        checkListManagementClient.param("parentId",parentId).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<CheckList> checklist = null;
        try {
            TypeReference<PageItr<CheckList>> type = new TypeReference<PageItr<CheckList>>() {
            };
            checklist = checkListManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_004, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
            
        }
        return checklist;
	}
    
	public static PageItr<CheckList> findInStandardCheckList(String name,  int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller checkListManagementClient = createRestCallser();
        checkListManagementClient.path(CheckListManagementServiceNames.findInStandardCheckList);
        checkListManagementClient.param("name",name).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<CheckList> checklist = null;
        try {
            TypeReference<PageItr<CheckList>> type = new TypeReference<PageItr<CheckList>>() {
            };
            checklist = checkListManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_004, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
            
        }
        return checklist;
	}

	
	
	public static PageItr<CheckList> findInCheckListWithParent(long parentId,  String name, int pageNumber,int pageSize) throws ServiceFailedException {
		RestCaller checkListManagementClient = createRestCallser();
        checkListManagementClient.path(CheckListManagementServiceNames.findInCheckListWithParent);
        checkListManagementClient.param("parentId",parentId).param("name",name).param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<CheckList> checklist = null;
        try {
            TypeReference<PageItr<CheckList>> type = new TypeReference<PageItr<CheckList>>() {
            };
            checklist = checkListManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_004, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
            
        }
        return checklist;
	}   
    

    public static void updateCheckList(long checkListId, long checkListItemId, boolean status,
            Date completedDate, String completedPersonId) throws ServiceFailedException {
        RestCaller checkListManagementClient = createRestCallser();
        checkListManagementClient.path(CheckListManagementServiceNames.updateCheckList);
        checkListManagementClient.param("checkListId", checkListId).param("checkListItemId", checkListItemId)
                .param("status", status).param("completedDate", Utilities.dateToString_yyyy_MM_dd(completedDate))
                .param("completedPersonId", completedPersonId);

        try {
            checkListManagementClient.post(null,CheckListItem.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_003, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

}
