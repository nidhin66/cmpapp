/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.schedule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.ClientMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.ScheduleManagementService;
import com.gamillusvaluegen.cpm.ui.util.WFMClientUtil;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.time.PersonDays;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.exception.SchedulingException;

/**
 *
 * @author hp
 */
public class HierarchyItemSchedulePane extends BorderPane implements DialogController {

	private FXMLDialog dialog;

	@FXML
	private GridPane dataPane;
	
	@FXML
	private Label paneLabel;

	private NodeCategory nodeType;

	private List<Job> jobs;
	
	private List<HierarchyNode> hierarchyItems;
	
	private List<JobGroup> jobGroups;

	private List<SchedulePaneItem> itemPanes = null;
	
	private boolean approve;
	
	private ScheduleMode mode;

	public HierarchyItemSchedulePane(boolean approve, List<? extends HierarchyItem> hierarchyItems,ScheduleMode mode,
			NodeCategory category) {
		this.approve = approve;
		this.mode = mode;
		loadUI(mode);
		itemPanes = new ArrayList<>();
		this.nodeType = category;
		if (category == NodeCategory.job) {
			this.jobs = (List<Job>) hierarchyItems;
			if (null != jobs) {
				int i = 0;
				for (HierarchyItem job : jobs) {
					SchedulePaneItem schedulePaneItem = new SchedulePaneItem((Job) job, approve, mode);
					itemPanes.add(schedulePaneItem);
					dataPane.add(schedulePaneItem, 0, i);
					i++;
				}
			}
		} else if (category == NodeCategory.hierarchy) {
			this.hierarchyItems = (List<HierarchyNode>) hierarchyItems;
			if (null != this.hierarchyItems) {
				int i = 0;
				for (HierarchyItem hierarchyItem : this.hierarchyItems) {
					SchedulePaneItem schedulePaneItem = new SchedulePaneItem((HierarchyNode) hierarchyItem, approve, mode);
					itemPanes.add(schedulePaneItem);
					dataPane.add(schedulePaneItem, 0, i);
					i++;
				}
			}
		}else if (category == NodeCategory.jobgroup) {
			this.jobGroups = (List<JobGroup>) hierarchyItems;
			if (null != jobGroups) {
				int i = 0;
				for (HierarchyItem jobGroup : jobGroups) {
					SchedulePaneItem schedulePaneItem = new SchedulePaneItem((JobGroup)jobGroup, approve, mode);
					itemPanes.add(schedulePaneItem);
					dataPane.add(schedulePaneItem, 0, i);
					i++;
				}
			}
		}

	}

	private void loadUI(ScheduleMode mode) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/hierarchy_item_schedule.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		if(mode == ScheduleMode.SCHEDULING){
			paneLabel.setText("Scheduling");
		}else if(mode == ScheduleMode.STATUS_UPDATE){
			paneLabel.setText("Status Update");
		}else if(mode == ScheduleMode.VIEW_SCHEDULE){
			paneLabel.setText("View Schedule");
		}
		dataPane.getStyleClass().add("default-pane-background");
	}

	// public HierarchyItemSchedulePane(List<? extends HierarchyItem>
	// hierarchyItems) {
	//
	// FXMLLoader fxmlLoader = new
	// FXMLLoader(getClass().getResource("/fxml/hierarchy_item_schedule.fxml"));
	// fxmlLoader.setRoot(this);
	// fxmlLoader.setController(this);
	//
	// try {
	// fxmlLoader.load();
	// } catch (IOException exception) {
	// throw new RuntimeException(exception);
	// }
	// if(null != hierarchyItems){
	// int i = 0;
	// for(HierarchyItem hierarchyItem : hierarchyItems){
	// // hierarchyItem.
	// dataPane.add(new SchedulePaneItem(hierarchyItem), 0, i);
	// i++;
	// }
	// }
	//
	// dataPane.getStyleClass().add("default-pane-background");
	//
	// }
	@FXML
	private void closeDialog() {
		ScreensConfiguration.getInstance().setCurrentParentWindow(ScreensConfiguration.getInstance().getPrimaryStage());
		this.dialog.close();
	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;
	}

	@FXML
	private void submitData() {
		if(mode == ScheduleMode.VIEW_SCHEDULE){
			closeDialog();
		}else{
			if (nodeType == NodeCategory.job) {
				List<Job> jobs = new ArrayList<>();
				try {
					String infoMessage = "";
					for (SchedulePaneItem schedulePaneItem : itemPanes) {

						Job job = schedulePaneItem.getJob();
						if (null != job.getWorkSchedule() && null != job.getDuration()) {
							job.getWorkSchedule().validate(job.getDuration().getCalender());
						}
						PersonDays personDays = (PersonDays) job.getDuration();
						int duration = 0;
						if (null != personDays) {
							duration = personDays.getNumberofDays();
						}
						if (approve) {
							if (schedulePaneItem.isApproved()) {
								ScheduleManagementService.validateJobSchedule(job.getId(), job.getWorkSchedule(), duration);
								ScheduleManagementService.setJobSchedule(job.getId(), job.getWorkSchedule(), duration);
								infoMessage =  ClientMessages.SCHEDULE_APPROVED;
							} else {
								ScheduleManagementService.rejectUnApprovedJobSchedule(job.getId());
							}

						} else {				
							ScheduleManagementService.validateJobSchedule(job.getId(), job.getWorkSchedule(), duration);
							if(mode == ScheduleMode.SCHEDULING){
								if(!WFMClientUtil.approvalFlowRequired()){
									ScheduleManagementService.setJobSchedule(job.getId(), job.getWorkSchedule(), duration);
									infoMessage =  ClientMessages.SCHEDULE_UPDATED;
								}else{
									ScheduleManagementService.setUnApprovedJobSchedule(job.getId(), job.getWorkSchedule(),
											duration);
									infoMessage = ClientMessages.SCHEDULING_SEND_APRROVE;
								}
							}else{
								ScheduleManagementService.setJobSchedule(job.getId(), job.getWorkSchedule(), duration);
								infoMessage = ClientMessages.STATUS_UPDATE;
							}	
						}
						dataPane.getChildren().remove(itemPanes);
					}
					closeDialog();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO,infoMessage);
					alertDialog.show();
				} catch (SchedulingException ex) {
					ex.printStackTrace();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, ex.getMessage());
					alertDialog.show();
				} catch (ServiceFailedException ex) {
					Message errorMessage = ex.getErroMessage();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
					alertDialog.show();
				}

			} else {
				try {
					String infoMessage = "";
					for (SchedulePaneItem schedulePaneItem : itemPanes) {
						if(nodeType == NodeCategory.jobgroup){
							JobGroup jobGroup = schedulePaneItem.getJobGroup();		
							if (null != jobGroup.getWorkSchedule() && null != jobGroup.getDuration()) {
								jobGroup.getWorkSchedule().validate(jobGroup.getDuration().getCalender());
							}
							PersonDays personDays = (PersonDays) jobGroup.getDuration();
							int duration = 0;
							if (null != personDays) {
								duration = personDays.getNumberofDays();
							}
							if (approve) {
								if (schedulePaneItem.isApproved()) {
									ScheduleManagementService.validateJobGroupWorkSchedule(jobGroup.getId(), jobGroup.getWorkSchedule(), duration);
									ScheduleManagementService.setJobGroupSchedule(jobGroup.getId(),
											jobGroup.getWorkSchedule(), duration);
									infoMessage =  ClientMessages.SCHEDULE_APPROVED;
								} else {
									ScheduleManagementService.rejectUnApprovedJobGroupSchedule(jobGroup.getId());
								}
							} else {
								ScheduleManagementService.validateJobGroupWorkSchedule(jobGroup.getId(), jobGroup.getWorkSchedule(), duration);
								if(mode == ScheduleMode.SCHEDULING){
									if(!WFMClientUtil.approvalFlowRequired()){
										ScheduleManagementService.setJobGroupSchedule(jobGroup.getId(),
												jobGroup.getWorkSchedule(), duration);
										infoMessage =  ClientMessages.SCHEDULE_UPDATED;
									}else{
										ScheduleManagementService.setUnApprovedJobGroupSchedule(jobGroup.getId(),
												jobGroup.getWorkSchedule(), duration);
										 infoMessage =  ClientMessages.SCHEDULING_SEND_APRROVE;
									}								
								}else{
									ScheduleManagementService.setJobGroupSchedule(jobGroup.getId(),
											jobGroup.getWorkSchedule(), duration);
									infoMessage = ClientMessages.STATUS_UPDATE;
								}
								
							}
						}else{
							HierarchyNode hierarchyNode = schedulePaneItem.getHierarchyNode();					
							if (approve) {
								if (schedulePaneItem.isApproved()) {
									
									ScheduleManagementService.setHierarchybSchedule(hierarchyNode.getId(),
											hierarchyNode.getWorkTime());
									infoMessage =  ClientMessages.SCHEDULE_APPROVED;
								} else {
									ScheduleManagementService.rejectUnApprovedHierarchybSchedule(hierarchyNode.getId());
								}

							} else {							
								if(mode == ScheduleMode.SCHEDULING){
									if(!WFMClientUtil.approvalFlowRequired()){
										ScheduleManagementService.setHierarchybSchedule(hierarchyNode.getId(),
												hierarchyNode.getWorkTime());
										 infoMessage =  ClientMessages.SCHEDULE_UPDATED;
									}else{
										ScheduleManagementService.setUnApprovedHierarchybSchedule(hierarchyNode.getId(),
												hierarchyNode.getWorkTime());
										 infoMessage =  ClientMessages.SCHEDULING_SEND_APRROVE;
									}
								}else{
									ScheduleManagementService.setHierarchybSchedule(hierarchyNode.getId(),
											hierarchyNode.getWorkTime());
									infoMessage =  ClientMessages.STATUS_UPDATE;		
								}							
							}
						}
						
						dataPane.getChildren().remove(itemPanes);
					}
					closeDialog();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO,infoMessage);
					alertDialog.show();
				} catch (SchedulingException ex) {
					ex.printStackTrace();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, ex.getMessage());
					alertDialog.show();
				}catch (ServiceFailedException ex) {
					Message errorMessage = ex.getErroMessage();
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
					alertDialog.show();
				}

			}
		}
		

	}

}
