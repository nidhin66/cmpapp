/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.statusview;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;

/**
 *
 * @author hp
 */
public class JobsStatusList extends BorderPane{
    
    @FXML
    private Label headerLabel;
    
    @FXML
    private ListView<CriticalJobList> jobList;
    
    
    public JobsStatusList(List<CriticalJobList> criticalJobs){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/jobs_status_list.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        
         jobList.setCellFactory(new Callback<ListView<CriticalJobList>, ListCell<CriticalJobList>>() {

            @Override
            public ListCell<CriticalJobList> call(ListView<CriticalJobList> param) {
                 ListCell<CriticalJobList> cell = new ListCell<CriticalJobList>() {
                    @Override
                    protected void updateItem(CriticalJobList t, boolean bln) {
                        super.updateItem(t, bln);
                        if (bln) {
                            setText(null);
                            setGraphic(null);
                        }
                        if (t != null) {
                        	if(null != t.getJobIds() && t.getJobIds().length > 0 ){
                        		setText(""+t.getJobIds().length);
                        		setGraphic(t.getStatusGraphics());
                        	}
//                            setText(t.toString());
                        }
                    }

                 };
                 cell.setOnMouseClicked(new EventHandler<MouseEvent>() {
                     @Override
                     public void handle(MouseEvent event) {
                         if (event.getClickCount() == 2) {
                              CriticalJobList criticalJobList = jobList.getSelectionModel().getSelectedItem();
                             try {
                                PageItr<Job> jobs =  HierarchyJobService.getJobs(criticalJobList.getJobIds(), 0, 30);
                                 loadJobPanel(jobs);
                             } catch (ServiceFailedException ex) {
                                 Logger.getLogger(JobsStatusList.class.getName()).log(Level.SEVERE, null, ex);
                             }
                              
                         }
                     }
                 });
                 return cell;
            }
             
         });
        
        
        jobList.setItems(FXCollections.observableArrayList(criticalJobs));
        
        
    }
    
    
   public void setHeaderLabel(String str){
       headerLabel.setText(str);
   }
   
    private void loadJobPanel( PageItr<Job> jobs){
        
         String screenId = CPMClientConstants.MENU_ID_PLANT_HIERARCHY;
         Parent screen = ClientConfig.getInstance().getScreenById(screenId);
         ScreensConfiguration.getInstance().getMainScreenController().setmainDataPane(screen);
        
          HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
         jobPanel.loadData(jobs, false);
          jobPanel.loadParentPanelData();
    }
}
