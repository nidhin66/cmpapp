/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.CheckListClientService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class CheckListPane extends AnchorPane {

	private HierarchyJobPanel jobPanel;

	private CheckListPaneController controller;

	private static final int pageSize = 10;
	private static final int CONTEXT_STANDARD = 1;
	private static final int CONTEXT_LINKED = 2;

	int contextID = CONTEXT_LINKED;

	private PageItr<CheckList> currentPage;

	public CheckListPane() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/checklist_data_pane.fxml"));
		try {
			BorderPane root = loader.load();
			controller = loader.getController();
			this.getChildren().add(root);
			controller.setParentPane(this);
			this.getStyleClass().add("dependency-tab");
		} catch (IOException ex) {
			Logger.getLogger(CheckListPane.class.getName()).log(Level.SEVERE, null, ex);
		}
		// this.getStyleClass().add("checklist-pane");
	}

	public void loadData(PageItr<CheckList> checkListPage) {
		List<CheckList> checkLists = null;
		String labelString = "";
		currentPage = checkListPage;
		if (null != checkListPage) {
			checkLists = currentPage.getContent();
			labelString = ( currentPage.getCurrentPage() + 1 ) + " of " + currentPage.getTotalPages() ; 
		}
		controller.setPaginationLabel(labelString);		
		controller.loadCheckListData(checkLists);
	}

	public void linkToJob(CheckListType checkListType) {
		JobItem jobItem = (JobItem) jobPanel.getSelectedItem();
		List<CheckList> checkLists = controller.getSelectedCheckLists();
		try {
			if (!jobPanel.isStandardContext() && jobPanel.isJobGroupContext()) {
				checkLists = HierarchyJobService.linkFromStandardCheckListToJobGroup(jobItem.getId(), checkListType,
						checkLists);

			} else if (!jobPanel.isStandardContext() && !jobPanel.isJobGroupContext()) {
				checkLists = HierarchyJobService.linkFromStandardCheckListToJob(jobItem.getId(), checkListType,
						checkLists);
			}
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		showLinkedCheckList();
	}

	public void removeJobLink() {
		try {
			JobItem jobItem = (JobItem) jobPanel.getSelectedItem();
			List<CheckList> checkLists = controller.getSelectedCheckLists();
			HierarchyJobService.unlinkCheckListItem(jobItem.getId(), checkLists);
			for (Iterator<CheckList> it = jobItem.getCheckList().iterator(); it.hasNext();) {
				CheckList checkList = it.next();
				for (CheckList removedCheckList : checkLists) {
					if (checkList.getId() == removedCheckList.getId()) {
						it.remove();
					}
				}
			}

		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		showLinkedCheckList();
	}

	public void showLinkedCheckList() {
		contextID = CONTEXT_LINKED;
		getLinkedCheckList(0);
		controller.disableCheckListItemDetails(false);
	}

	public void ShowAllStandardCheckList() {
		contextID = CONTEXT_STANDARD;
		getAllStandardCheckList(0);
		controller.disableCheckListItemDetails(true);
	}

	public void loadFirstPage() {
		loadPage(0);
	}

	public void loadPreviousPage() {
		if (null != currentPage) {
			if ((currentPage.getCurrentPage()) > 0) {
				int pageNumber = currentPage.getCurrentPage() - 1;
				loadPage(pageNumber);
			}
		}
	}

	public void loadNextPage() {
		if (null != currentPage) {
			if ((currentPage.getCurrentPage() + 1) < currentPage.getTotalPages()) {
				int pageNumber = currentPage.getCurrentPage() + 1;
				loadPage(pageNumber);
			}
		}
	}

	public void loadLastPage() {
		if (null != currentPage) {			
			int pageNumber = currentPage.getTotalPages() - 1;
			loadPage(pageNumber);
		}
	}

	private void loadPage(int pageNumber) {
		if (contextID == CONTEXT_LINKED) {
			getLinkedCheckList(pageNumber);
		} else {
			getAllStandardCheckList(pageNumber);
		}
	}

	private void getLinkedCheckList(int pageNumber) {
		JobItem jobItem = (JobItem) jobPanel.getSelectedItem();
		PageItr<CheckList> checkListPage = null;
		if (null != jobItem) {
			try {
				checkListPage = CheckListClientService.getCheckListByParent(jobItem.getId(), pageNumber, pageSize);
			} catch (ServiceFailedException e) {
				Message errorMessage = e.getErroMessage();
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
				alertDialog.show();
			}
			// if (null != jobItem.getCheckList()) {
			// checkLists.addAll(jobItem.getCheckList());
			// }
		}
		this.loadData(checkListPage);
	}

	private void getAllStandardCheckList(int pageNumber) {
		PageItr<CheckList> checkListPage = null;
		try {
			checkListPage = CheckListClientService.getAllStandardCheckList(pageNumber, pageSize);
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		this.loadData(checkListPage);
	}
	
	public  List<CheckList> searchData(String text){
		  List<CheckList> resultList = new ArrayList<>();
		PageItr<CheckList> checkListPage = null;
		try {
			
				text = text + "%";
				if (contextID == CONTEXT_LINKED) {
					JobItem jobItem = (JobItem) jobPanel.getSelectedItem();
					if (null != jobItem) {
						checkListPage = CheckListClientService.findInCheckListWithParent(jobItem.getId(),text, 0, pageSize);
					}			
					
				}else{
					checkListPage = CheckListClientService.findInStandardCheckList(text, 0, pageSize);	
				}
			
		}catch (ServiceFailedException ex) {
			
		}		
		this.loadData(checkListPage);
		if(null != checkListPage){
			resultList = checkListPage.getContent();
		}
		return resultList;
	}

	// public SpreadsheetView getCheckListItemSheet(CheckList newValue) {
	// int columnCount = 2;
	// int itemSize = newValue.getCheckListItems().size();
	// int rowCount = (itemSize / columnCount) + itemSize % columnCount;
	// List<CheckListItem> items = new ArrayList<>();
	// items.addAll(newValue.getCheckListItems());
	// GridBase grid = new GridBase(rowCount, columnCount);
	//
	// ObservableList<ObservableList<SpreadsheetCell>> rows =
	// FXCollections.observableArrayList();
	// for (int row = 0; row < grid.getRowCount(); ++row) {
	// final ObservableList<SpreadsheetCell> list =
	// FXCollections.observableArrayList();
	// for (int column = 0; column < grid.getColumnCount(); ++column) {
	// int index = (row * grid.getColumnCount()) + column;
	// if (null != items) {
	// if (index < items.size()) {
	// list.add(generateCell(items.get(index), row, column, 1, 1));
	// } else {
	// list.add(generateCell(null, row, column, 1, 1));
	// }
	// } else {
	// list.add(generateCell(null, row, column, 1, 1));
	// }
	// }
	// rows.add(list);
	// }
	// grid.setRows(rows);
	//
	// SpreadsheetView spv = new SpreadsheetView();
	// spv.setGrid(grid);
	//
	// spv.setShowColumnHeader(false);
	// spv.setShowRowHeader(false);
	// spv.setEditable(false);
	// spv.setPrefSize(150 * columnCount, 250);
	// this.getChildren().add(spv);
	//
	// for (SpreadsheetColumn column : spv.getColumns()) {
	// column.setPrefWidth(150);
	// }
	// this.setPrefSize(150 * columnCount + 2, 250 + 2);
	// return spv;
	// }
	//
	// private SpreadsheetCell generateCell(CheckListItem obj, int row, int
	// column, int rowSpan, int colSpan) {
	// SpreadsheetCell cell = new CheckListItemCell(row, column, rowSpan,
	// colSpan, obj);
	//// cell.getStyleClass().add("row_header");
	// return cell;
	// }

	public void setJobPanel(HierarchyJobPanel jobPanel) {
		this.jobPanel = jobPanel;
	}

}
