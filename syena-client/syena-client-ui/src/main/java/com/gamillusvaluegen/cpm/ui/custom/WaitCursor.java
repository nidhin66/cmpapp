/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.custom;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;

/**
 *
 * @author hp
 */
public class WaitCursor extends BorderPane implements DialogController{
    
    private FXMLDialog dialog;
    
    public WaitCursor(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/wait_cursor.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
    }

    @Override
    public void setDialog(FXMLDialog dialog) {
       this.dialog = dialog;
    }
    
    
}
