/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

/**
 *
 * @author hp
 */
public class CheckListViewMenu extends ContextMenu {

    private CheckListPane mainPane;

    public CheckListViewMenu(final CheckListPane mainPane) {
        this.mainPane = mainPane;
        MenuItem item1 = new MenuItem("Show All");
        MenuItem item2 = new MenuItem("Show Linked");
        this.getItems().addAll(item1, item2);
         item1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainPane.ShowAllStandardCheckList();
            }
        });
         
          item2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainPane.showLinkedCheckList();
            }
        });
    }
}
