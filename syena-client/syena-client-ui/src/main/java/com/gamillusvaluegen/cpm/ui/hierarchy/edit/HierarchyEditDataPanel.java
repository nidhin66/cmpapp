/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import java.util.List;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;


/**
 *
 * @author hp
 */
public class HierarchyEditDataPanel extends GridPane {

    private static final int columnWidth = 240;
    private int totalLevelCount = 0;
    private HierarchyEditItemFieldPanel rootItemPanel;

    public HierarchyEditDataPanel(HierarchyLevel hierarchy) {
        this.setHgap(50);        
        totalLevelCount = createHeader(hierarchy);
        rootItemPanel = new HierarchyEditItemFieldPanel(hierarchy,totalLevelCount);
        this.add(rootItemPanel, 0, 1,totalLevelCount,1);
    }

    private int createHeader(HierarchyLevel hierarchy) {
        HBox header = new HBox();
        header.setSpacing(100);
        header.setStyle("-fx-background-color : #64545E;");
        int labelcount  = createHeaderLabels(hierarchy, header);      
        this.add(header, 0, 0, labelcount, 1);       
        return labelcount;
    }

    private int createHeaderLabels(HierarchyLevel level, HBox hBox) {
        int i = 1;
        HierarchyEditPanelLabel label = new HierarchyEditPanelLabel(level.getLevelName());
        hBox.getChildren().add(label);
        hBox.setPrefWidth(hBox.getPrefWidth() + columnWidth);
        if (null != level.getChildren()) {
            for(HierarchyLevel childLevel : level.getChildren()) {
                i = i + createHeaderLabels(childLevel, hBox);
            }
        }
        return i;
    }

//    public void addRowContraint() {
//        RowConstraints rc = new RowConstraints();
//        rc.setVgrow(Priority.ALWAYS);
//        this.getRowConstraints().add(rc);
//    }
    
    public List<? extends StandardHierarchyNode> getContent(){
        return this.rootItemPanel.getContent();
    }
    
    public void loadData(List<StandardHierarchyNode> hierarchyNodes){
        rootItemPanel.loadData(hierarchyNodes);        
//         FXMLDialog addNodeItemDialog = new FXMLDialog(new HierarchyNodeItemController(rootItemPanel.getItems().get(0)), getClass().getResource("/fxml/node_item.fxml"), ScreensConfiguration.getInstance().getCurrentParentWindow(), StageStyle.UNDECORATED);
//         addNodeItemDialog.show();
    }

}
