/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.main;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import com.gamillusvaluegen.cpm.ui.notification.NotificationButton;
import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.hierarchy.search.AdvancedSearchPopup;
import com.gamillusvaluegen.cpm.ui.hierarchy.search.QuickSearchPopup;

/**
 *
 * @author hp
 */
public class MainScreenController {
    
    @FXML
    private BorderPane topPane;
    
    @FXML
    private BorderPane leftMenupane;    
    
    @FXML
    private ScrollPane mainDataPane;
    
    @FXML
    private VBox menuBox;
    
//   private MainMenuBar mainMenuBar;
   
   @FXML
   private HBox topMenu;
   
   @FXML
   private VBox legendBox;
   
   @FXML
   private Button advancedSearchBtn;
   
   @FXML
   private Button quickSearchBtn;
   
    
    @FXML
    private void initialize() {
        double maxWidth = ScreensConfiguration.getInstance().getWidth();
        double maxHeight = ScreensConfiguration.getInstance().getHeight();
//        topPane.setPrefWidth(maxWidth);
        leftMenupane.setPrefHeight(maxHeight - topPane.getHeight());
        mainDataPane.setPrefSize(maxWidth - leftMenupane.getWidth(), maxHeight - topPane.getHeight());
        
//        mainMenuBar = new MainMenuBar(this);
//        menuBox.getChildren().add(mainMenuBar);
        
        ToolBar leftToolBar = new ToolBar();
        leftToolBar.setStyle("-fx-background-color : transparent;");
        leftToolBar.setOrientation(Orientation.VERTICAL);
        
        final Button plantHierarchyButton = new Button("Plant Hierarchy");
        plantHierarchyButton.getStyleClass().add("main-menu-item");
       
        
        plantHierarchyButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String screenId = CPMClientConstants.MENU_ID_PLANT_HIERARCHY;
				if (!ClientConfig.getInstance().getCurrentScreenId().equalsIgnoreCase(screenId)) {
					Parent screen = ClientConfig.getInstance().getScreenById(screenId);
					setmainDataPane(screen);
				}

			}
		});
        
        Button statusViewButton = new Button("Status View");
        statusViewButton.getStyleClass().add("main-menu-item");
        
        statusViewButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String screenId = CPMClientConstants.MENU_ID_STATUS_VIEW;
				if (!ClientConfig.getInstance().getCurrentScreenId().equalsIgnoreCase(screenId)) {
					Parent screen = ClientConfig.getInstance().getScreenById(CPMClientConstants.MENU_ID_STATUS_VIEW);
					setmainDataPane(screen);
				}

			}
		});

        Button unApprovedButton = new Button("UnApproved Data");
        unApprovedButton.getStyleClass().add("main-menu-item");
        
        
        unApprovedButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String screenId = CPMClientConstants.MENU_ID_UNAPPROVED_DATA;
				if (!ClientConfig.getInstance().getCurrentScreenId().equalsIgnoreCase(screenId)) {
					Parent screen = ClientConfig.getInstance()
							.getScreenById(CPMClientConstants.MENU_ID_UNAPPROVED_DATA);
					setmainDataPane(screen);
				}

			}
		});
        
        MenuItem unapprovedData = new MenuItem(CPMClientConstants.MENU_LABEL_UNAPPROVED_DATA);
        
        leftToolBar.getItems().addAll(
                //new Separator(),
        		plantHierarchyButton,
        		statusViewButton,
        		unApprovedButton
                //,new Separator()
            );

        menuBox.getChildren().add(leftToolBar);
    
        
        ScreensConfiguration.getInstance().setMainScreenController(this);
        
        NotificationButton notificationButton = new NotificationButton();
        topMenu.getChildren().add(notificationButton);
    
    }
    
    public void setmainDataPane(Parent pane){ 
//       AnchorPane testPane = new AnchorPane();
//       testPane.setStyle("-fx-background-color:  rgb(91,99,110);");
//       testPane.setPrefSize(1400, 1000);
    	
       mainDataPane.setContent(pane);        
       mainDataPane.setVvalue(Double.MIN_VALUE);
    }
    
   public void setScrollValue(double value){
	   mainDataPane.setVvalue(value);
   }
   
   @FXML
   private void showAdvancedSearch() {
       AdvancedSearchPopup popup = new AdvancedSearchPopup(advancedSearchBtn);
       popup.show(ScreensConfiguration.getInstance().getPrimaryStage());     
   }
   
   @FXML
   private void showQuickSearch() {
       QuickSearchPopup popup = new QuickSearchPopup(quickSearchBtn);
       popup.show(ScreensConfiguration.getInstance().getPrimaryStage());
   }
    
   
}
