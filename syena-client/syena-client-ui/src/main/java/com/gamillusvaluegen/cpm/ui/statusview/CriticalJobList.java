/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.statusview;

import javafx.scene.shape.Rectangle;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gvg.syena.core.api.entity.common.WorkStatus;
import com.gvg.syena.core.api.entity.common.WorkStatusType;

/**
 *
 * @author hp
 */
public class CriticalJobList {
    
    private WorkStatusType workStatus;
    private long[] jobIds;
    
    public CriticalJobList(WorkStatusType workStatus, long[] jobids){
        this.workStatus = workStatus;
        this.jobIds = jobids;
    }
    
    public String toString(){       
        String str = "";
        if(null != workStatus ){
            WorkStatusType statusType = workStatus;
            str = statusType.toString();
        }
        str = str +  "       " +jobIds.length;
        return str;
    }

    public long[] getJobIds() {
        return jobIds;
    }
    
    public Rectangle getStatusGraphics(){
    	WorkStatusType statusType = workStatus;
    	return ClientConfig.getStatusGraphics(statusType);
    }
    
    
}
