/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.search;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.stage.Popup;

import com.gamillusvaluegen.cpm.ScreensConfiguration;

/**
 *
 * @author hp
 */
public class AdvancedSearchPopup extends Popup {
	
	private AdvancedSearchController controller = null;
    public AdvancedSearchPopup(Parent parentNode) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/advanced_search.fxml"));

        Parent root = null;
        try {
            root = loader.load();
            controller = loader.getController();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.getContent().addAll(root);

        Point2D point = parentNode.localToScene(0.0, 0.0);

        this.setAutoHide(true);
        double x = ScreensConfiguration.getInstance().getPrimaryStage().getX() + point.getX() - 460;
        double y = ScreensConfiguration.getInstance().getPrimaryStage().getY() + point.getY() + 60;
        this.setX(x);
        this.setY(y);
        controller.setPopUp(this);

    }

}
