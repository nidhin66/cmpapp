package com.gamillusvaluegen.cpm.ui.wiki;

import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.WikiManagementClientService;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.wiki.HierarchyItemWiki;
import com.gvg.syena.core.api.entity.wiki.WikiContent;

public class WikiPagePane extends BorderPane implements DialogController {

	private GridPane mainContentPane;
	private ScrollPane scrollPane;
	private Button addButton;
	private Button closeButton;
	private FXMLDialog dialog;
	private HierarchyItemWiki wiki;
	private HierarchyItem item;

	public WikiPagePane(HierarchyItem item) {
		this.item = item;

		try {
			this.wiki = WikiManagementClientService.getHierarchyWiki(item.getId());
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.setStyle("-fx-background-color:#ffffff");
		createHeaderLabel();

		scrollPane = new ScrollPane();
		scrollPane.setStyle("-fx-background-color:#ffffff");
		mainContentPane = new GridPane();
		mainContentPane.setStyle("-fx-background-color:#ffffff");
		mainContentPane.setAlignment(Pos.CENTER);
		scrollPane.setContent(mainContentPane);
		this.setCenter(scrollPane);
		// this.setMinWidth(852);
		// this.setMaxWidth(852);
		this.setMinSize(860, 600);
		this.setMaxSize(860, 600);

		addButton = new Button();
		addButton.setPrefWidth(100);
		addButton.setPrefHeight(30);
		addButton.setVisible(false);
		addButton.getStyleClass().add("wiki-add-button");
		addButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				boolean saved = true;
				for (Node wikiNode : mainContentPane.getChildren()) {
					if (wikiNode instanceof WikiContentPane) {
						WikiContentPane contentPane = (WikiContentPane) wikiNode;
						if (contentPane.isEditMode()) {
							AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR,
									"Save or cancel the current edited item");
							alertDialog.show();
							saved = false;
						}
					}
				}
				if (saved) {
					createEditPane(WikiContentType.SUB);
					scrollPane.setVvalue(1.0);
				}

			}
		});

		closeButton = new Button();
		closeButton.setPrefWidth(100);
		closeButton.setPrefHeight(30);
		closeButton.getStyleClass().add("wiki-close-button");
		closeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				closeAction();
			}
		});

		if (null == wiki) {
			setCreateWikiPane();
		} else {
			loadWiki();
		}

		HBox bottomPane = new HBox();
		bottomPane.setSpacing(25);
		bottomPane.getChildren().addAll(addButton, closeButton);
		HBox.setMargin(addButton, new Insets(10, 0, 10, 10));
		HBox.setMargin(closeButton, new Insets(10, 0, 10, 10));

		this.setBottom(bottomPane);

		this.setStyle("-fx-background-color:#ffffff");
	}

	private void setCreateWikiPane() {
		VBox vBox = new VBox();
		vBox.setAlignment(Pos.CENTER);
		vBox.setSpacing(20);

		HBox bannerBox = new HBox();
		bannerBox.setPrefHeight(30);
		bannerBox.setPrefWidth(850);
		bannerBox.setAlignment(Pos.CENTER);
		bannerBox.getStyleClass().add("wiki-banner");
		ImageView image = new ImageView();
		image.setFitWidth(30);
		image.setFitHeight(30);
		image.getStyleClass().add("wiki-banner-image");
		bannerBox.getChildren().add(image);

		Label label1 = new Label("Wiki page is not created for this particular item.");
		label1.getStyleClass().add("wiki-message-label");

		Label label2 = new Label("Do you want to create a new one ?");
		label2.getStyleClass().add("wiki-message-label");

		HBox buttonBox = new HBox();
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setSpacing(25);

		Button yesButton = new Button();
		yesButton.setPrefSize(65, 25);
		yesButton.getStyleClass().add("wiki-create-yes-button");
		yesButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				mainContentPane.getChildren().remove(0);
				createEditPane(WikiContentType.MAIN);
				addButton.setVisible(true);
			}
		});

		Button noButton = new Button();
		noButton.getStyleClass().add("wiki-create-no-button");
		noButton.setPrefSize(65, 25);
		noButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				closeAction();

			}
		});

		buttonBox.getChildren().addAll(yesButton, noButton);
		vBox.getChildren().addAll(bannerBox, label1, label2, buttonBox);
		mainContentPane.add(vBox, 0, 0);
		closeButton.setVisible(false);
		addButton.setVisible(false);
	}

	private void loadWiki() {
		List<Node> nodes = new ArrayList<>();
		for (Node node : mainContentPane.getChildren()) {
			nodes.add(node);
		}
		if (nodes.size() > 0) {
			mainContentPane.getChildren().removeAll(nodes);
		}
		WikiContentPane contentViewPane = new WikiContentPane(this, wiki.getMainContent(), WikiContentType.MAIN);
		int index = addRow(contentViewPane);
		contentViewPane.setEditable(false);
		if (null != wiki.getSubContentList() && wiki.getSubContentList().size() > 0) {
			for (WikiContent wikiContent : wiki.getSubContentList()) {
				WikiContentPane subContentPane = new WikiContentPane(this, wikiContent, WikiContentType.SUB);
				int indexSub = addRow(subContentPane);
				subContentPane.setEditable(false);
			}
		}
		addButton.setVisible(true);
		closeButton.setVisible(true);
	}

	private void createEditPane(WikiContentType type) {
		WikiContentPane contentEditPane = new WikiContentPane(this, null, type);
		int index = addRow(contentEditPane);
		contentEditPane.setEditable(true);
	}

	private int addRow(Pane pane) {
		int row = mainContentPane.getRowConstraints().size();
		mainContentPane.add(pane, 0, row + 1);
		mainContentPane.getRowConstraints().add(new RowConstraints());
		mainContentPane.setPrefHeight(pane.getHeight());
		return row;
	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;
	}

	public void removeContent(WikiContentPane contentEditPane) {
		if (null != contentEditPane.getContent()) {
			wiki.removeSubContent(contentEditPane.getContent());
		}
		saveWiki();
		mainContentPane.getChildren().remove(contentEditPane);
	}

	public void cancel(WikiContentPane contentEditPane) {
		if (null == wiki) {
			mainContentPane.getChildren().remove(contentEditPane);
			setCreateWikiPane();
		} else {
			if (contentEditPane.getContent().getId() == -1) {
				mainContentPane.getChildren().remove(contentEditPane);
			} else {
				contentEditPane.setEditable(false);
			}
			addButton.setVisible(true);
		}
	}

	public void saveContent(WikiContentPane editPane) {
		if (null == wiki) {
			wiki = new HierarchyItemWiki(item.getId());
		}
		WikiContent content = editPane.getContent();
		if (editPane.getType() == WikiContentType.MAIN) {
			wiki.setMainContent(content);
		} else {
			wiki.addOrUpdateSubContent(content);
		}
		saveWiki();
		if (null != editPane.getContent()) {
			editPane.setEditable(false);
		}
		addButton.setVisible(true);
	}

	private void saveWiki() {
		try {
			WikiManagementClientService.createOrUpdateWiki(wiki);
			wiki = WikiManagementClientService.getHierarchyWiki(item.getId());
			loadWiki();
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void editContent(WikiContentPane viewPane) {
		if (null != viewPane.getContent()) {
			viewPane.setEditable(true);
		}
		addButton.setVisible(false);
	}

	private void createHeaderLabel() {
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.CENTER);
		Label pageLabel = new Label("Wiki for " + item.getName());
		pageLabel.getStyleClass().add("wiki-page-label");
		hBox.getChildren().add(pageLabel);
		HBox.setMargin(pageLabel, new Insets(15, 0, 0, 0));
		setTop(hBox);
	}

	private void closeAction() {
		dialog.close();
	}

}
