/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.dependency;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

/**
 *
 * @author hp
 */
public class DependentJobContextMenu extends ContextMenu {
    
    private String context ;
    
    private DependentItemPane dependentItemPane;

    public DependentJobContextMenu(final DependentItemPane dependentItemPane, final String context) {
        this.setAutoHide(true);
        this.getStyleClass().add("hierarchy-context-menu");
        this.context = context;
        this.dependentItemPane = dependentItemPane;
        if ("P".equalsIgnoreCase(context)) {
            MenuItem item1 = new MenuItem("Add as Predecessor");
            item1.getStyleClass().add("hierarchy-context-menu-item");
            this.getItems().addAll(item1);
            item1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    dependentItemPane.addAsPredecessor();
                }
            });
        } else {
            MenuItem item1 = new MenuItem("Add as Successor");
            item1.getStyleClass().add("hierarchy-context-menu-item");
            this.getItems().addAll(item1);
            item1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dependentItemPane.addAsSuccessor();;
                }
            });            
        }
        MenuItem item2 = new MenuItem("Remove Dependency");
        this.getItems().addAll(item2);
        item2.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if("S".equalsIgnoreCase(context)){
                        dependentItemPane.removeSuccessor();
                    }else{
                        dependentItemPane.removePredecessor();
                    }
                    
                }
            });       
    }
    
    @Override
    public void show(Node node, double screenX , double screenY){
        if(dependentItemPane.getContextID() == DependentItemPane.CONTEXT_DEPENDENT || dependentItemPane.getContextID() == 3 || dependentItemPane.getContextID() == 4){
            this.getItems().get(0).setVisible(false);
            this.getItems().get(1).setVisible(true);
        }else{
            this.getItems().get(0).setVisible(true);
            this.getItems().get(1).setVisible(false);
        }
        super.show(node, screenX, screenY);
    }

}
