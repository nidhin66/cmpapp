package com.gamillusvaluegen.cpm.ui.server;



import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.NotificationManagementServiceNames;
import com.gvg.syena.core.api.services.notification.Notification;
import com.gvg.syena.core.api.services.notification.NotificationType;

public class NotificationClientService {
	public static final String url = ScreensConfiguration.getUrl();

	private static RestCaller createRestCallser() {
		RestCaller notificationManagementClient = new RestCaller(url);
		notificationManagementClient.path("notificationManagement");
		return notificationManagementClient;
	}
	
	public static PageItr<Notification> getNotifications(String userId, NotificationType notificationType ,int pageNumber, int pageSize) throws ServiceFailedException{
		 RestCaller notificationManagementClient = createRestCallser();
		 notificationManagementClient.path(NotificationManagementServiceNames.getNotifications);

		 notificationManagementClient.param("userId", userId);
		 notificationManagementClient.param("pageNumber", pageNumber);
		 notificationManagementClient.param("pageSize", pageSize);
		 notificationManagementClient.param("notificationType", notificationType);
	        PageItr<Notification> notifications = null;
	        try {
	            TypeReference<PageItr<Notification>> type = new TypeReference<PageItr<Notification>>() {
	            };
	            notifications = notificationManagementClient.get(type);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_001, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	        return notifications;
	}
	
	public static long getNotificationCount(String userId) throws ServiceFailedException{
		 RestCaller notificationManagementClient = createRestCallser();
		 notificationManagementClient.path(NotificationManagementServiceNames.getNotificationCount);
		 notificationManagementClient.param("userId", userId);
		 long count = 0;
		 try {
			 count = notificationManagementClient.get(long.class);
		 }catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_001, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
		 return count;

	}
}
