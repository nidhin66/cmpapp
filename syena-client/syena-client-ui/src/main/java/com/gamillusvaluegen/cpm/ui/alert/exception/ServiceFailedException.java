/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.alert.exception;

import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.exception.ApplicationException;

/**
 *
 * @author hp
 */
public class ServiceFailedException extends ApplicationException {

	private Message erroMessage = null;

	public ServiceFailedException(Message errorMessage) {
		super(errorMessage.getErrorCode(), errorMessage.getStrackTrace());
		this.erroMessage = errorMessage;
	}

	public Message getErroMessage() {
		return erroMessage;
	}

}
