/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.main;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;

import com.gamillusvaluegen.cpm.ScreensConfiguration;

/**
 *
 * @author hp
 */
public class MainScreen extends AnchorPane{
    
    private MainScreenController controller;
    
    public MainScreen(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main_pane.fxml"));
        SplitPane root = null;
        try {
            root = loader.load();
            controller = loader.getController();
        } catch (IOException ex) {
           ex.printStackTrace();;
        }
        this.getChildren().add(root);        
        this.setPrefSize(ScreensConfiguration.getInstance().getWidth(), ScreensConfiguration.getInstance().getHeight());        
    }
    
    public void setMainDataPane(Parent parent){
        controller.setmainDataPane(parent);
    }
    
    public void scrollToBottom(){
    	controller.setScrollValue(Double.MAX_VALUE);
    }
    
    public void scrollToTop(){
    	controller.setScrollValue(Double.MIN_VALUE);
    }
}
