package com.gamillusvaluegen.cpm.ui.server;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.diary.ThreadDiscussion;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.diary.DiaryFilterProperties;
import com.gvg.syena.core.api.services.names.ProjectDiaryServiceNames;



public class DiaryClientService {

	public static final String url = ScreensConfiguration.getUrl();

	private static RestCaller createRestCallser() {
		RestCaller diaryManagmentController = new RestCaller(url);
		diaryManagmentController.path("projectDiary");
		return diaryManagmentController;
	}
	
	
	public static DiaryThread createNewThread(DiaryThread diaryThread) throws ServiceFailedException {
		 RestCaller diaryManagmentController = createRestCallser();
		 diaryManagmentController.path(ProjectDiaryServiceNames.createNewThread);    
		
         try {
        	 diaryThread = diaryManagmentController.post(diaryThread,DiaryThread.class);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
		return diaryThread;
	}
	
	public static void addDiscussion(long threadId, ThreadDiscussion threadDiscussion) throws ServiceFailedException{
		 RestCaller diaryManagmentController = createRestCallser();
		 diaryManagmentController.path(ProjectDiaryServiceNames.addDiscussion);    
		 diaryManagmentController.param("threadId", threadId);
         try {
        	  diaryManagmentController.post(threadDiscussion, String.class);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
	}
	
	public static PageItr<DiaryThread> getLatestThreads(int pageNumber,int pageSize) throws ServiceFailedException{
		 RestCaller diaryManagmentController = createRestCallser();
		 diaryManagmentController.path(ProjectDiaryServiceNames.getLatestThreads);    
		 diaryManagmentController.param("pageNumber", pageNumber).param("pageSize", pageSize);
		 PageItr<DiaryThread> diaryThreadPage = null;
         try {
        	 TypeReference<PageItr<DiaryThread>>  type = new TypeReference<PageItr<DiaryThread>>() { };
        	 diaryThreadPage = diaryManagmentController.get(type);
         }catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
		return diaryThreadPage;
	}
	
	public static PageItr<ThreadDiscussion> getDiscussions(long threadId,int pageNumber,int pageSize) throws ServiceFailedException{
		 RestCaller diaryManagmentController = createRestCallser();
		 diaryManagmentController.path(ProjectDiaryServiceNames.getDiscussions);    
		 diaryManagmentController.param("threadId", threadId).param("pageNumber", pageNumber).param("pageSize", pageSize);
		 PageItr<ThreadDiscussion> discussionPage = null;
         try {
        	 TypeReference<PageItr<ThreadDiscussion>>  type = new TypeReference<PageItr<ThreadDiscussion>>() { };
        	 discussionPage = diaryManagmentController.get(type);
         }catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
		return discussionPage;
	}
	
	public static PageItr<DiaryThread> searchDiscussionThreads( DiaryFilterProperties filterProperties,int pageNumber,int pageSize) throws ServiceFailedException {
		 RestCaller diaryManagmentController = createRestCallser();
		 diaryManagmentController.path(ProjectDiaryServiceNames.searchDiscussionThreads);   
		 diaryManagmentController.param("pageNumber", pageNumber).param("pageSize", pageSize);
		 PageItr<DiaryThread> diaryThreadPage = null;
         try {
        	 TypeReference<PageItr<DiaryThread>>  type = new TypeReference<PageItr<DiaryThread>>() { };
        	 diaryThreadPage = diaryManagmentController.post(filterProperties,type);
         }catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.DEPENDENCY_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
		return diaryThreadPage;
	}
}
