/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.JobManagementServiceNames;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.util.Configuration;

/**
 *
 * @author hp
 */
public class HierarchyJobService {

    public static final String url = ScreensConfiguration.getUrl();

//    private static RestCaller jobManagementClient = new RestCaller(url+"/jobManagement");
    private static RestCaller createRestCallser() {
        RestCaller jobManagementClient = new RestCaller(url);
        jobManagementClient.path("jobManagement");
        return jobManagementClient;
    }

    public static PageItr<JobGroup> getAllStandardJobGroup(int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient
                .path(JobManagementServiceNames.getAllStandardJobGroup);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<JobGroup> jobgroups = null;
        try {
            TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
            };
            jobgroups = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_001, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobgroups;
    }

    public static PageItr<Job> getAllStandardJobs(int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient.path(JobManagementServiceNames.getAllStandardJobs);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = jobManagementClient.get(type);
        } catch (RestCallException ex) {
        	ex.printStackTrace();
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_002, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<JobGroup> getAllJobGroups(int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient.path(JobManagementServiceNames.getAllJobGroups);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<JobGroup> jobgroups = null;
        try {
            TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
            };
            jobgroups = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_003, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobgroups;
    }

    public static PageItr<Job> getAllJobs(int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient
                .path(JobManagementServiceNames.getAllJobs)
                .param("pageNumber", pageNumber).param("pageSize", pageSize);

        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_004, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public PageItr<CheckList> getCheckListItem(long jobId, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient
                .path(JobManagementServiceNames.getCheckListsOfJob)
                .param("jobId", jobId).param("pageNumber", pageNumber).param("pageSize", pageSize); // HTTP GET request is being made

        PageItr<CheckList> checkListPage = null;
        try {
            TypeReference<PageItr<CheckList>> type = new TypeReference<PageItr<CheckList>>() {
            };
            checkListPage = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_005, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return checkListPage;
    }

    public static List<CheckList> linkFromStandardCheckListToJob(long jobId,CheckListType checkListType, List<CheckList> standardCheckListItems) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient.path(JobManagementServiceNames.createAndLinkFromStandardCheckListWithTypeToJob);
        jobManagementClient.param("jobId", jobId).param("checkListType", checkListType);
        if (null != standardCheckListItems) {
            for (CheckList standardCheckListItem : standardCheckListItems) {
                jobManagementClient.param("standardCheckListItems", standardCheckListItem.getId());
            }
        } else {
            jobManagementClient.param("standardCheckListItems", "");
        }

        List<CheckList> checkLists = null;
        try {
            TypeReference<List<CheckList>> type = new TypeReference<List<CheckList>>() {
            };
            checkLists = jobManagementClient.post(null, type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_006, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }

        return checkLists;
    }

    public static List<CheckList> linkFromStandardCheckListToJobGroup(long jobGroupId,CheckListType checkListType,List<CheckList> standardCheckListItems) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient.path(JobManagementServiceNames.createAndLinkFromStandardCheckListWithTypeToJobGroup);
        jobManagementClient.param("jobGroupId", jobGroupId).param("checkListType", checkListType);
        if (null != standardCheckListItems) {
            for (CheckList standardCheckListItem : standardCheckListItems) {
                jobManagementClient.param("standardCheckListItems", standardCheckListItem.getId());
            }
        } else {
            jobManagementClient.param("standardCheckListItems", "");
        }

        List<CheckList> checkLists = null;
        try {
            TypeReference<List<CheckList>> type = new TypeReference<List<CheckList>>() {
            };
            checkLists = jobManagementClient.post(null,type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_007, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }

        return checkLists;
    }

    public static void unlinkCheckListItem(long jobId, List<CheckList> checkLists) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient.path(JobManagementServiceNames.unlinkCheckList);
        jobManagementClient.param("jobId", jobId);

        if (null != checkLists) {
            for (CheckList checkList : checkLists) {
                jobManagementClient.param("checkListItemId", checkList.getId());
            }
        } else {
            jobManagementClient.param("checkListItemId", "");
        }

        try {
            jobManagementClient.post(null);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_008, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }

    }

    public static void createStandardJob(StandardJob job) throws ServiceFailedException {
    	Configuration.childEnable = true;
        RestCaller jobManagementClient = createRestCallser();
        try {
            jobManagementClient
                    .path(JobManagementServiceNames.createStandardJob)
                    .post(job, StandardJob.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_009, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
    	Configuration.childEnable = false;

    }
    
   
	public static void createJob(Job job) throws ServiceFailedException {
		Configuration.childEnable = true;
        RestCaller jobManagementClient = createRestCallser();
        try {
            jobManagementClient
                    .path(JobManagementServiceNames.createJobAndConnect)
                    .post(job, Job.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_017, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
    	Configuration.childEnable = false;
	}

    public static void updateStandardJob(StandardJob job) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        try {
            jobManagementClient
                    .path(JobManagementServiceNames.updateStandardJob)
                    .post(job, StandardJob.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_010, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
    }
    
    public static void updateJob(Job job) throws ServiceFailedException  {
    	 RestCaller jobManagementClient = createRestCallser();
         try {
             jobManagementClient
                     .path(JobManagementServiceNames.updateJob)
                     .post(job, Job.class);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_020, ex.getMessage());
             throw new ServiceFailedException(erroMessage);
         }
  	}

    public static void createStandardJobGroup(JobGroup<StandardJob> jobGroup) throws ServiceFailedException {
    	Configuration.childEnable = true;
        RestCaller jobManagementClient = createRestCallser();
        try {
            jobManagementClient
                    .path(JobManagementServiceNames.createStandardJobGroup)
                    .post(jobGroup, JobGroup.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_011, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        Configuration.childEnable = false;
    }
    
    public static void createJobGroup(JobGroup<Job> jobGroup)  throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        try {
            jobManagementClient
                    .path(JobManagementServiceNames.createJobGroup)
                    .post(jobGroup, JobGroup.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_018, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
    }
    
	public static void updateJobGroup(JobGroup<Job> jobGroup) throws ServiceFailedException {
		Configuration.childEnable = true;
		  RestCaller jobManagementClient = createRestCallser();
	        try {
	            jobManagementClient
	                    .path(JobManagementServiceNames.updateJobGroup)
	                    .post(jobGroup, JobGroup.class);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_019, ex.getMessage());
	            throw new ServiceFailedException(erroMessage);
	        }
	        Configuration.childEnable = false;
	}
  

    public static void updateStandardJobGroup(JobGroup<StandardJob> jobGroup) throws ServiceFailedException {
    	Configuration.childEnable = true;
        RestCaller jobManagementClient = createRestCallser();
        try {
            jobManagementClient
                    .path(JobManagementServiceNames.updateStandardJobGroup)
                    .post(jobGroup, JobGroup.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_012, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        Configuration.childEnable = false;
    }

    public static PageItr<HierarchyNode> getConnectedHierarchyNode(long[] jobIds, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient.path(JobManagementServiceNames.getConnectedHierarchyNode_JoB);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        if (null != jobIds) {
            for (long jobId : jobIds) {
                jobManagementClient.param("jobIds", jobId);
            }
        } else {
            jobManagementClient.param("jobIds", "");
        }
        PageItr<HierarchyNode> hierachyNodes = null;
        try {
            TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
            };
            hierachyNodes = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_013, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return hierachyNodes;
    }

    public static PageItr<Job> getJobsByConnectedNodeIds(Long[] parentIds, int pageNumber, int pageSize) throws ServiceFailedException {

        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient
                .path(JobManagementServiceNames.getJobsByConnectedNodeIds);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        if (null != parentIds) {
            for (long parentId : parentIds) {
                jobManagementClient.param("parentIds", parentId);
            }
        } else {
            jobManagementClient.param("parentIds", "");
        }

        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_014, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;

    }

    public static PageItr<JobGroup> getJobGroupssByConnectedNodeIds(Long[] parentIds, int pageNumber, int pageSize) throws ServiceFailedException {

        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient
                .path(JobManagementServiceNames.getJobGroupsByConnectedNodeIds);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        if (null != parentIds) {
            for (long parentId : parentIds) {
                jobManagementClient.param("parentIds", parentId);
            }
        } else {
            jobManagementClient.param("parentIds", "");
        }

        PageItr<JobGroup> jobGroups = null;
        try {
            TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
            };
            jobGroups = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_015, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobGroups;

    }

    public static PageItr<Job> getJobs(long[] jobIds, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller jobManagementClient = createRestCallser();
        jobManagementClient
                .path(JobManagementServiceNames.getJobs);
        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        if (null != jobIds) {
            for (long jobId : jobIds) {
                jobManagementClient.param("jobIds", jobId);
            }
        } else {
            jobManagementClient.param("jobIds", "");
        }

        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = jobManagementClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_016, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;

    }
    
    public static PageItr<Job> searchJobWithProperties(JobProperties jobSearchOptions, int pageNumber,int pageSize) throws ServiceFailedException {
    	  RestCaller jobManagementClient = createRestCallser();
    	  jobManagementClient.path(JobManagementServiceNames.searchJobWithProperties);
    	  jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
          PageItr<Job> jobs = null;
          try {
              TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
              };
              jobs = jobManagementClient.post(jobSearchOptions, type);
          } catch (RestCallException ex) {
              Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
              throw new ServiceFailedException(erroMessage);
          }
          return jobs;
	}
    
    public static PageItr<JobGroup> searchJobGroupWithProperties(JobGroupProperties jobGroupProperties,  int pageNumber,int pageSize) throws ServiceFailedException {
    	RestCaller jobManagementClient = createRestCallser();
  	  jobManagementClient.path(JobManagementServiceNames.searchJobGroupWithProperties);
  	  jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<JobGroup> jobGroups = null;
        try {
            TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
            };
            jobGroups = jobManagementClient.post(jobGroupProperties, type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobGroups;
	}
    
    public static Set<StandardJob> getStandardJobInJobGroup(long jobGroupId) throws ServiceFailedException {
    	 RestCaller jobManagementClient = createRestCallser();
      	  jobManagementClient.path(JobManagementServiceNames.getStandardJobInJobGroup);
      	  jobManagementClient.param("jobGroupId", jobGroupId);
      	 Set<StandardJob> jobs = null;
            try {
                TypeReference<Set<StandardJob>> type = new TypeReference<Set<StandardJob>>() {
                };
                jobs = jobManagementClient.get( type);
            } catch (RestCallException ex) {
                Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
                throw new ServiceFailedException(erroMessage);
            }
            return jobs;
	}
    
   
	public static Set<Job> getJobsInJobGroup(long jobGroupId) throws ServiceFailedException {
    	 RestCaller jobManagementClient = createRestCallser();
   	  jobManagementClient.path(JobManagementServiceNames.getJobsInJobGroup);
   	  jobManagementClient.param("jobGroupId", jobGroupId);
   	 Set<Job> jobs = null;
         try {
             TypeReference<Set<Job>> type = new TypeReference<Set<Job>>() {
             };
             jobs = jobManagementClient.get( type);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
         return jobs;
	}
	
	
	public static PageItr<HierarchyNode> getConnectedHierarchyNodeJobGroup( long[] jobGroupIds,  int pageNumber, int pageSize) throws ServiceFailedException {
		 RestCaller jobManagementClient = createRestCallser();
	        jobManagementClient.path(JobManagementServiceNames.getConnectedHierarchyNode_JobGroup);
	        jobManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
	        if (null != jobGroupIds) {
	            for (long jobGroupID : jobGroupIds) {
	                jobManagementClient.param("jobGroupIds", jobGroupID);
	            }
	        } else {
	            jobManagementClient.param("jobGroupIds", "");
	        }
	        PageItr<HierarchyNode> hierachyNodes = null;
	        try {
	            TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
	            };
	            hierachyNodes = jobManagementClient.get(type);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_013, ex.getMessage());
	            throw new ServiceFailedException(erroMessage);
	        }
	        return hierachyNodes;

	}

    

}
