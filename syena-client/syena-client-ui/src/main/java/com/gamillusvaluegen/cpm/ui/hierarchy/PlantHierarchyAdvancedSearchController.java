/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.fxml.FXML;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;

/**
 *
 * @author hp
 */
public class PlantHierarchyAdvancedSearchController implements DialogController {

     private FXMLDialog dialog;
    
     @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }
    
      @FXML
    private void cancelsearch(){
        dialog.close();
    }
    
}
