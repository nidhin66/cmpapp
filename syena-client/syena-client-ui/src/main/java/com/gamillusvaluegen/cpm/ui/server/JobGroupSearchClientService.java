/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.Date;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.SearchServiceNames;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;

/**
 *
 * @author hp
 */
public class JobGroupSearchClientService {

	private static final String url = ScreensConfiguration.getUrl();

	private static RestCaller createRestCallser() {
		RestCaller searchClient = new RestCaller(url);
		searchClient.path("searchJobGroup");
		return searchClient;
	}

	public static PageItr<JobGroup> getOnGoingJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.getOnGoingJobGroups);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber)
				.param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_001, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup> completedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.completedJobGroups);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber)
				.param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_002, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup> plannedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.plannedJobGroups);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber)
				.param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_003, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup> delayedJobGroups(Date date, String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.delayedJobGroups);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber)
				.param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_004, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup> jobsInitiatedBy(String personId, String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsInitiatedBy);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("personId", personId).param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_005, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup> jobsOwnedBy(String personId, String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsOwnedBy);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("personId", personId).param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_006, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	 public static PageItr<JobGroup> searchJobs(JobProperties jobSearchOption, int
	 pageNaumber, int pageSize) throws ServiceFailedException {
			 RestCaller searchClient = createRestCallser();
			 searchClient.path(SearchServiceNames.searchJobs);
			 searchClient.param("pageNaumber", pageNaumber).param("pageSize",
			 pageSize);
			 PageItr<JobGroup> jobs = null;
			 try {
			 TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			 };
			 jobs = searchClient.post(jobSearchOption, type);
			 } catch (RestCallException ex) {
			 Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007,
			 ex.getMessage() );
			 throw new ServiceFailedException(erroMessage);
			 }
	 return jobs;
	 }

	public static PageItr<JobGroup> jobsToBeScheduled( String jobGroupName, int pageNumber,int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsToBeScheduled);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup> jobsWithRevisedDates(String jobGroupName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsWithRevisedDates);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static PageItr<JobGroup<Job>> jobGroupwithDelyedStartJobs(String jobGroupName, long hierarchyNodeId,
			ValueComparitor valueComparitor, float percentage, int pageNumber, int pageSize)
					throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobGroupwithDelyedStartJobs);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("hierarchyNodeId", hierarchyNodeId).param("percentage", percentage).param("valueComparitor",
				valueComparitor);
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup<Job>> jobGroups = null;
		try {
			TypeReference<PageItr<JobGroup<Job>>> type = new TypeReference<PageItr<JobGroup<Job>>>() {
			};
			jobGroups = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobGroups;
	}

	public static PageItr<JobGroup<Job>> jobGroupwithIncreasedDurationJobs(String jobGroupName, long hierarchyNodeId,
			ValueComparitor valueComparitor, float percentage, int pageNumber, int pageSize)
					throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobGroupwithIncreasedDurationJobs);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("hierarchyNodeId", hierarchyNodeId).param("percentage", percentage).param("valueComparitor",
				valueComparitor);
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup<Job>> jobGroups = null;
		try {
			TypeReference<PageItr<JobGroup<Job>>> type = new TypeReference<PageItr<JobGroup<Job>>>() {
			};
			jobGroups = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobGroups;
	}

	public static PageItr<JobGroup> jobGroupsInCriticalPath(String jobGroupName, long hierarchyNodeId, int pageNumber,
			int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobGroupsInCriticalPath);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("hierarchyNodeId", hierarchyNodeId);
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}
	
	public static PageItr<JobGroup> jobsGroupsStartInNDays(String jobGroupName, Date fromDate,Date toDate,int pageNumber,
			int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsGroupsStartInNDays);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("fromDate", Utilities.dateToString_yyyy_MM_dd(fromDate)).param("toDate", Utilities.dateToString_yyyy_MM_dd(toDate));
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}
	
	public static PageItr<JobGroup> jobsGroupsCompleteInNDays(String jobGroupName, Date fromDate,Date toDate,int pageNumber,
			int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsGroupsCompleteInNDays);
		if (!Utilities.isNullOrEmpty(jobGroupName)) {
			searchClient.param("jobGroupName", jobGroupName);
		}
		searchClient.param("fromDate", Utilities.dateToString_yyyy_MM_dd(fromDate)).param("toDate", Utilities.dateToString_yyyy_MM_dd(toDate));
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<JobGroup> jobs = null;
		try {
			TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

}
