/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

/**
 *
 * @author hp
 */
public class UIUtilities {

    public static Date showDateDialog() {
        Dialog<Date> dialog = new Dialog<>();
        dialog.setTitle("Test");
        dialog.setHeaderText("This is a custom dialog. Enter info and \n"
                + "press Okay (or click title bar 'X' for cancel).");
        dialog.setResizable(true);

        HBox hBox = new HBox();
        hBox.setSpacing(10);
        final DatePicker datePicker = new DatePicker();

        ButtonType buttonTypeOk = new ButtonType("Okay", ButtonData.OK_DONE);

        dialog.getDialogPane().setContent(hBox);

        dialog.setResultConverter(new Callback<ButtonType, Date>() {
            @Override
            public Date call(ButtonType b) {
                LocalDate localDate = datePicker.getValue();
                Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                return date;

            }
        });

        Optional<Date> result = dialog.showAndWait();

        if (result.isPresent()) {

            return result.get();
        } else {
            return null;
        }

//         hBox.getChildren().addAll(datePicker,buttonTypeOk);
    }

}
