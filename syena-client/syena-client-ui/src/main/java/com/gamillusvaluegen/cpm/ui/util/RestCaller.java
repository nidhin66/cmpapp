package com.gamillusvaluegen.cpm.ui.util;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.util.View;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RestCaller {

    private Client client;

    private WebResource webResource = null;

    private ObjectWriter objectWriter;

    public RestCaller(String target) {
//    	DefaultClientConfig defaultClientConfig = new DefaultClientConfig();
//    	defaultClientConfig.getClasses().add(JacksonJsonProvider.class);
        this.client = Client.create();
        this.webResource = client.resource(target);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        this.objectWriter = mapper.writerWithView(View.ClientSerilize.class);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        mapper.setDateFormat(dateFormat);
    }

    public RestCaller path(String path) {
        webResource = webResource.path(path);
        return this;

    }

    public RestCaller param(String paramName, Object value) {
        if (value != null) {
            webResource = this.webResource.queryParam(paramName, value.toString());
        }
        return this;
    }

    public void get() throws RestCallException {

        try {

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            if (response.getStatus() != 200) {
                throw new Exception("Failed : HTTP error code : " + response.getStatus());
            }
        } catch (Exception e) {
            throw new RestCallException(e);
        }

    }

    public <T> T get(Class<T> responseClassType) throws RestCallException, ServiceFailedException {
        String output = null;
        try {

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            if (response.getStatus() != 200) {
                throw new Exception("Failed : HTTP error code : " + response.getStatus());
            }
            output = response.getEntity(String.class);

            if (null != responseClassType && !Utilities.isNullOrEmpty(output)) {

                ObjectMapper mapper = new ObjectMapper();
                T obj = mapper.readValue(output, responseClassType);
                return obj;
            } else {
                return null;
            }

         } catch (JsonMappingException e) {
           handleJsonMappingException(output);
        }   catch (Exception e) {
            throw new RestCallException(e);
        }
        return null;
    }

    public <T> T get(TypeReference<T> responseTypeReference) throws RestCallException, ServiceFailedException {
        String output = null;
        try {

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            if (response.getStatus() != 200) {
                throw new Exception("Failed : HTTP error code : " + response.getStatus());
            }
            output = response.getEntity(String.class);
            if (responseTypeReference != null && !Utilities.isNullOrEmpty(output)) {
                ObjectMapper mapper = new ObjectMapper();
                Object obj = mapper.readValue(output, responseTypeReference);

                return (T) obj;
            }
            return null;

        } catch (JsonMappingException e) {
        	e.printStackTrace();
           handleJsonMappingException(output);
        }   catch (Exception e) {
            throw new RestCallException(e);
        }
        return null;
    }

    public <T> T post(Object input, TypeReference<T> responseTypeReference) throws RestCallException, ServiceFailedException {
        String output = null;
        try {

            String inputStr = objectWriter.writeValueAsString(input);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, inputStr);

//			if (response.getStatus() != 201) {
//				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
//			}
            if (response.getStatus() != 200) {
                throw new Exception("Failed : HTTP error code : " + response.getStatus());
            }

            output = response.getEntity(String.class);

            if (responseTypeReference != null && !Utilities.isNullOrEmpty(output)) {

                ObjectMapper mapper = new ObjectMapper();
                Object obj = mapper.readValue(output, responseTypeReference);

                return (T) obj;
            }
            return null;
        } catch (JsonMappingException e) {
        	e.printStackTrace();
           handleJsonMappingException(output);
        }  catch (Exception e) {
            throw new RestCallException(e);
        }
        return null;

    }
    
    
    public <T> T uploadFile(Object input, Class<T> c) throws RestCallException, ServiceFailedException {
    	 String output = null;
         try {

             // WebResource webResource = client.resource(getCompleteURL());
//             String inputStr = objectWriter.writeValueAsString(input);
             ClientResponse response = webResource.type(MediaType.MULTIPART_FORM_DATA).post(ClientResponse.class, input);

 			// if (response.getStatus() != 201) {
             // throw new RuntimeException("Failed : HTTP error code : " +
             // response.getStatus());
             // }
             if (response.getStatus() != 200) {
                 throw new Exception("Failed : HTTP error code : " + response.getStatus());
             }
             T obj = null;
             output = response.getEntity(String.class);
             if (c != null && !Utilities.isNullOrEmpty(output)) {
                 ObjectMapper mapper = new ObjectMapper();			//
                 obj = mapper.readValue(output, c);
             }
             return obj;
         } catch (JsonMappingException e) {
            handleJsonMappingException(output);
         } catch (Exception e) {
             throw new RestCallException(e);
         }
         return null;
    }
    
    

    // private <T> T post(Object input, Class<T> outputClass) {
    public <T> T post(Object input, Class<T> c) throws RestCallException, ServiceFailedException {
        String output = null;
        try {

            // WebResource webResource = client.resource(getCompleteURL());
            String inputStr = objectWriter.writeValueAsString(input);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, inputStr.toString());

			// if (response.getStatus() != 201) {
            // throw new RuntimeException("Failed : HTTP error code : " +
            // response.getStatus());
            // }
            if (response.getStatus() != 200) {
                throw new Exception("Failed : HTTP error code : " + response.getStatus());
            }
            T obj = null;
            output = response.getEntity(String.class);
            if (c != null && !Utilities.isNullOrEmpty(output)) {
                ObjectMapper mapper = new ObjectMapper();			//
                obj = mapper.readValue(output, c);
            }
            return obj;
        } catch (JsonMappingException e) {
           handleJsonMappingException(output);
        } catch (Exception e) {
        	e.printStackTrace();
            throw new RestCallException(e);
        }
        return null;
    }

    // private <T> T post(Object input, Class<T> outputClass) {
    public <T> T post(Class<T> c) throws RestCallException, ServiceFailedException {
        String output = null;
        try {

			// WebResource webResource = client.resource(getCompleteURL());
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, null);

            if (response.getStatus() != 200) {
                throw new Exception("Failed : HTTP error code : " + response.getStatus());
            }

            output = response.getEntity(String.class);

            ObjectMapper mapper = new ObjectMapper();
            if (null != c) {
                T obj = mapper.readValue(output, c);
                return obj;
            } else {
            	if(!Utilities.isNullOrEmpty(output)){
            		handleJsonMappingException(output);
            	}else{
            		return null;
            	}                
            }

         } catch (JsonMappingException e) {
        	handleJsonMappingException(output);
        }  catch (ServiceFailedException e){
        	throw e;
    	}   catch (Exception e) {
            throw new RestCallException(e);
        }
        return null;

    }
    
    
        
    
    private void handleJsonMappingException(String outPut) throws RestCallException, ServiceFailedException{
         ObjectMapper mapper = new ObjectMapper();
            if (outPut != null) {
                try {
                    Message errorMessage = mapper.readValue(outPut, Message.class);
//                    AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getExceptionMsg());
//                    alertDialog.show();
                    throw new ServiceFailedException(errorMessage);
                } catch (IOException ex) {
                    throw new RestCallException(ex);
                }
            }
    }

    public static void main(String[] args) throws RestCallException, ServiceFailedException {

        // RestCaller restCaller = new RestCaller("http://localhost:8080/cpm");
        RestCaller restCaller = new RestCaller("http://52.74.44.39:8080/cpm");
		// restCaller.path("hierarchyManagement/getHierarchyNodes");

		// TypeReference<PageItr<HierarchyNode>> tyeRef = new
        // TypeReference<PageItr<HierarchyNode>>() {
        // };
        // PageItr page = restCaller.param("nodeIds", "216").param("pageNumber",
        // 0).param("pageSize", 1).get(tyeRef);
        // System.out.println(page);
        restCaller.path("hierarchyManagement/getHierarchyLevels");
        HierarchyLevel out = restCaller.param("hierarchyName", "Plant").get(HierarchyLevel.class);
        System.out.println();
		// RestCaller restCaller = new RestCaller("http://localhost:8080/cpm");
        // restCaller.path("hierarchyManagement/getHierarchyNodes");
        // PageItr page = restCaller.param("nodeIds", "216").param("pageNumber",
        // 0).param("pageSize", 1).get(PageItr.class);
        // System.out.println(page);
    }

}
