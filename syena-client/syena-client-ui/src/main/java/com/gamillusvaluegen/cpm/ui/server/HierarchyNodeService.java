/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.services.HierarchyDoc;
import com.gvg.syena.core.api.services.names.HierarchyManagementServiceNames;
import com.gvg.syena.core.api.util.Configuration;

/**
 *
 * @author hp
 */
public class HierarchyNodeService {

	// public static final String url = "http://52.74.44.39:8080/cpm/";
	public static final String url = ScreensConfiguration.getUrl();

	private static RestCaller createRestCallser() {
		RestCaller hierarchyManagementClient = new RestCaller(url);
		hierarchyManagementClient.path("hierarchyManagement");
		return hierarchyManagementClient;
	}

	public static PageItr<HierarchyNode> getLinkedHierarchyNodes(long parentId, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getLinkedHierarchyNodes);
		hierarchyManagementClient.param("parentId", parentId).param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<HierarchyNode> hierarchyNodes = null;
		try {
			TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
			hierarchyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_001, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierarchyNodes;

	}

	public static PageItr<HierarchyNode> getLinkedStandardHierarchyNodes(long parentId, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getLinkedStandardHierarchyNodes);
		hierarchyManagementClient.param("parentId", parentId).param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<HierarchyNode> hierachyNodes = null;
		try {
			TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_002, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierachyNodes;
	}

	public static HierarchyLevel getHierarchyLevels(String hierarchyName) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getHierarchyLevels);
		hierarchyManagementClient.param("hierarchyName", hierarchyName);
		HierarchyLevel level = null;
		try {
			level = hierarchyManagementClient.get(HierarchyLevel.class);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_003, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return level;
	}

	public static PageItr<StandardHierarchyNode<StandardHierarchyNode>> getAllStandardHierarchyNodes(String hierarchyLevel, int pageNumber, int pageSize)
			throws ServiceFailedException {
		
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getAllStandardHierarchyNodes);
		hierarchyManagementClient.param("hierarchyLevelName", hierarchyLevel).param("pageNumber", pageNumber).param("pageSize", pageSize);

		PageItr<StandardHierarchyNode<StandardHierarchyNode>> hierachyNodes = null;
		try {
			TypeReference<PageItr<StandardHierarchyNode<StandardHierarchyNode>>> type = new TypeReference<PageItr<StandardHierarchyNode<StandardHierarchyNode>>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_004, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		
		return hierachyNodes;
	}

	public static PageItr<HierarchyNode> searchHierarchyNodesInLevel(String hierarchyNodename, String hierarchyLevelName, int pageNumber, int pageSize)
			throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.searchHierarchyNodesInLevel);
		hierarchyManagementClient.param("hierarchyNodename", hierarchyNodename).param("hierarchyLevelName", hierarchyLevelName).param("pageNumber", pageNumber)
				.param("pageSize", pageSize);

		PageItr<HierarchyNode> hierachyNodes = null;
		try {
			TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_005, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierachyNodes;
	}

	public static PageItr<HierarchyNode> searchHierarchyNodesInChild(String hierarchyNodename, long parentId, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.searchHierarchyNodesInChild);
		hierarchyManagementClient.param("hierarchyNodename", hierarchyNodename).param("parentId", parentId).param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<HierarchyNode> hierachyNodes = null;
		try {
			TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_006, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierachyNodes;
	}

	public static PageItr<HierarchyNode> getChildHierarchyNodes(long[] parentIds, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getChildHierarchyNodes);

		if (null != parentIds && parentIds.length > 0) {
			for (long parentId : parentIds) {
				hierarchyManagementClient.param("parentIds", parentId);
			}
			hierarchyManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);

			PageItr<HierarchyNode> hierachyNodes = null;
			try {
				TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
				};
				hierachyNodes = hierarchyManagementClient.get(type);
			} catch (RestCallException ex) {
				Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_007, ex.getMessage());
				throw new ServiceFailedException(erroMessage);
			}

			return hierachyNodes;
		} else {
			return null;
		}
	}

	public static PageItr<StandardHierarchyNode> getChildStandardHierarchyNode(long[] parentIds, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getChildStandardHierarchyNode);

		if (null != parentIds && parentIds.length > 0) {
			for (long parentId : parentIds) {
				hierarchyManagementClient.param("parentIds", parentId);
			}
			hierarchyManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);

			PageItr<StandardHierarchyNode> hierachyNodes = null;
			try {
				TypeReference<PageItr<StandardHierarchyNode>> type = new TypeReference<PageItr<StandardHierarchyNode>>() {
				};
				hierachyNodes = hierarchyManagementClient.get(type);
			} catch (RestCallException ex) {
				Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_008, ex.getMessage());
				throw new ServiceFailedException(erroMessage);
			}

			return hierachyNodes;
		} else {
			return null;
		}
	}

	public static List<JobGroup<Job>> createAndlinkJobGroups(long hierarchyNodeId, long[] standardJobGroupIds) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.createAndlinkJobGroups);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
		if (null != standardJobGroupIds) {
			for (long jobGroupId : standardJobGroupIds) {
				hierarchyManagementClient.param("standardJobGroupIds", jobGroupId);
			}
		} else {
			hierarchyManagementClient.param("standardJobGroupIds", "");
		}
		List<JobGroup<Job>> jobGroups = null;
		try {
			TypeReference<List<JobGroup<Job>>> type = new TypeReference<List<JobGroup<Job>>>() {
			};
			jobGroups = hierarchyManagementClient.post(null, type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_009, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobGroups;
	}

	public static List<Job> createAndlinkJob(long hierarchyNodeId, long[] standardJobIds) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.createAndlinkJob);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
		if (null != standardJobIds) {
			for (long standardJobId : standardJobIds) {
				hierarchyManagementClient.param("standardJobIds", standardJobId);
			}
		} else {
			hierarchyManagementClient.param("standardJobIds", "");
		}
		List<Job> jobs = null;
		try {
			TypeReference<List<Job>> type = new TypeReference<List<Job>>() {
			};
			jobs = hierarchyManagementClient.post(null, type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_010, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}

	public static void linkHierarchyNodes(long parentNodeId, long[] standardHierarchyNodeIds) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.createAndLinkHierarchyNodes);
		hierarchyManagementClient.param("parentNodeId", parentNodeId);

		if (null != standardHierarchyNodeIds) {
			for (long nodeId : standardHierarchyNodeIds) {
				hierarchyManagementClient.param("standardHierarchyNodeIds", nodeId);
			}
		} else {
			hierarchyManagementClient.param("standardHierarchyNodeIds", "");
		}

		try {
			hierarchyManagementClient.post(null);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_011, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}

	}

	public static PageItr<Job> getLinkedJob(long hierarchyNodeId) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getLinkedJob);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
		PageItr<Job> jobNodes = null;
		try {
			TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
			};
			jobNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_012, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobNodes;
	}

	public static PageItr<JobGroup<Job>> getLinkedJobGroups(long hierarchyNodeId) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getLinkedJobGroups);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);

		PageItr<JobGroup<Job>> jobGroups = null;
		try {
			TypeReference<PageItr<JobGroup<Job>>> type = new TypeReference<PageItr<JobGroup<Job>>>() {
			};
			jobGroups = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_013, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobGroups;
	}

	public static StandardHierarchyNode[] createStandardHierarchyNodes(List<? extends StandardHierarchyNode> hierarchyNodes) throws ServiceFailedException {
		Configuration.childEnable = true;
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.createStandardHierarchyNodes);
		hierarchyManagementClient.param("ownerId", ClientConfig.getInstance().getLoginUser().getId());
		StandardHierarchyNode[] standardNodes = null;
		try {
			standardNodes = hierarchyManagementClient.post(hierarchyNodes, StandardHierarchyNode[].class);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_014, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		Configuration.childEnable = false;
		return standardNodes;
	}
	
  
	public static void createHierarchyNodes( List<? extends StandardHierarchyNode> hierarchyNodes) throws ServiceFailedException {
		Configuration.childEnable = true;
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.createHierarchyNodes);
		hierarchyManagementClient.param("owner", ClientConfig.getInstance().getLoginUser().getId());
		
		try {
			hierarchyManagementClient.post(hierarchyNodes, HierarchyNode[].class);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_014, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		Configuration.childEnable = false;
	}


	public static StandardHierarchyNode[] updateStandardHierarchyNodes(List<? extends StandardHierarchyNode> hierarchyNodes) throws ServiceFailedException {
		Configuration.childEnable = true;
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.createStandardHierarchyNodes);		
		hierarchyManagementClient.param("ownerId", ClientConfig.getInstance().getLoginUser().getId());
		StandardHierarchyNode[] standardNodes = null;
		try {
			standardNodes = hierarchyManagementClient.post(hierarchyNodes, StandardHierarchyNode[].class);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_015, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		Configuration.childEnable = false;
		return standardNodes;
	}
	
	public static void updateHierarchyNode(List<? extends StandardHierarchyNode> hierarchyNodes) throws ServiceFailedException  {		
		Configuration.childEnable = true;
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.updateHierarchyNode);
		hierarchyManagementClient.param("owner", ClientConfig.getInstance().getLoginUser().getId());
		
		try {
			hierarchyManagementClient.post(hierarchyNodes, HierarchyNode[].class);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_014, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		Configuration.childEnable = false;
		
	}

	public static PageItr<HierarchyNode> getAllHierarchyNodes(String hierarchyLevelName, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getAllHierarchyNodes);
		hierarchyManagementClient.param("hierarchyLevelName", hierarchyLevelName).param("pageNumber", pageNumber).param("pageSize", pageSize);

		PageItr<HierarchyNode> hierachyNodes = null;
		try {
			TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_016, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierachyNodes;
	}

	public static PageItr<HierarchyNode> getParentHierarchyNodes(long[] childIds, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getParentHierarchyNodes);

		hierarchyManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		if (null != childIds) {
			for (long childId : childIds) {
				hierarchyManagementClient.param("childIds", childId);
			}
		} else {
			hierarchyManagementClient.param("childIds", "");
		}
		PageItr<HierarchyNode> hierachyNodes = null;
		try {
			TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_017, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierachyNodes;
	}
	
	
	public static PageItr<StandardHierarchyNode> searchStandardHierarchyNodesInLevel(String hierarchyNodename, String hierarchyLevelName,
			 int pageNumber, int pageSize) throws ServiceFailedException  {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.searchStandardHierarchyNodesInLevel);
		hierarchyManagementClient.param("hierarchyNodename", hierarchyNodename).param("hierarchyLevelName", hierarchyLevelName).param("pageNumber", pageNumber)
				.param("pageSize", pageSize);

		PageItr<StandardHierarchyNode> hierachyNodes = null;
		try {
			TypeReference<PageItr<StandardHierarchyNode>> type = new TypeReference<PageItr<StandardHierarchyNode>>() {
			};
			hierachyNodes = hierarchyManagementClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_005, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return hierachyNodes;

	}
	
//	public BufferedImage getHierarchyNodeImage(long hierarchyNodeId,String imageName,DocumentType documentType) {
//		 
//	}
	
	public static HierarchyDoc getHierarchyNodeImage(long hierarchyNodeId,String imageName,DocumentType documentType)  {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getHierarchyNodeImage);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId).param("imageName", imageName).param("documentType", documentType);   
		HierarchyDoc image = null;
		  try {
			  image = hierarchyManagementClient.get(HierarchyDoc.class);
			} catch (RestCallException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return image;  
	}
	
	public static void uploadHierarchyNodeImage(long hierarchyNodeId, HierarchyDoc image) {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.uploadHierarchyNodeImage);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);   
	  
	    try {
	    	hierarchyManagementClient.post(image,String.class);
		} catch (RestCallException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void uploadHierarchyNodeDoc(long hierarchyNodeId, HierarchyDoc doc) {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.uploadHierarchyNodeDoc);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);   
	  
	    try {
	    	hierarchyManagementClient.post(doc,String.class);
		} catch (RestCallException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static List<HierarchyDoc> getHierarchyNodeDocs(long hierarchyNodeId, DocumentType documentType){
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.getHierarchyNodeDocs);
		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId).param("documentType", documentType);   
		List<HierarchyDoc> docs = null;
		 try {
			 TypeReference<List<HierarchyDoc>> type = new TypeReference<List<HierarchyDoc>>() {
				};
				docs = hierarchyManagementClient.get(type);
			} catch (RestCallException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 return docs;
	}
	
	
	public static Job createPuchListJob(long parentIds, Job job) throws ServiceFailedException{
		Configuration.childEnable = true;
        RestCaller hierarchyManagementClient = createRestCallser();
        hierarchyManagementClient.param("parentIds", parentIds);
        Job newJob = null;
        try {
        	newJob = hierarchyManagementClient
                    .path(HierarchyManagementServiceNames.createPuchListJob)
                    .post(job, Job.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_011, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        Configuration.childEnable = false;
        return newJob;
	}

	
	public static JobGroup createPuchListJobGroup(long parentIds,JobGroup jobGroup) throws ServiceFailedException {
		Configuration.childEnable = true;
        RestCaller hierarchyManagementClient = createRestCallser();
        hierarchyManagementClient.param("parentIds", parentIds);
        JobGroup newJob = null;
        try {
        	newJob = hierarchyManagementClient
                    .path(HierarchyManagementServiceNames.createPuchListJobGroup)
                    .post(jobGroup, JobGroup.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.JOB_SERVICE_011, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
        Configuration.childEnable = false;
        return newJob;
	}
	
	public static void removeHierarchyNodeLink(List<? extends HierarchyItem> hierarchyItems) throws ServiceFailedException {
		Configuration.childEnable = true;
        RestCaller hierarchyManagementClient = createRestCallser();       
        if (null != hierarchyItems) {
			for (HierarchyItem hierarchyItem : hierarchyItems) {
				hierarchyManagementClient.param("hierarchyNodeIds", hierarchyItem.getId());
			}
		} else {
			hierarchyManagementClient.param("hierarchyNodeIds", "");
		}		
        try {
        	 hierarchyManagementClient.path(HierarchyManagementServiceNames.removeHierarchyNodeLink).post(null);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_018, ex.getMessage());
            throw new ServiceFailedException(erroMessage);
        }
	}
	
	public static void removeLinkedJobs(List<? extends HierarchyItem> jobs) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.removeLinkedJobs);
//		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
		if (null != jobs) {
			for (HierarchyItem job : jobs) {
				hierarchyManagementClient.param("jobIds", job.getId());
			}
		} else {
			hierarchyManagementClient.param("jobIds", "");
		}		
		try {			
			hierarchyManagementClient.post(null);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_019, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
	}
	
	public static void removeLinkedJobGroups( List<? extends HierarchyItem> jobGroups) throws ServiceFailedException {
		RestCaller hierarchyManagementClient = createRestCallser();
		hierarchyManagementClient.path(HierarchyManagementServiceNames.removeLinkedJobGroups);
//		hierarchyManagementClient.param("hierarchyNodeId", hierarchyNodeId);
		if (null != jobGroups) {
			for (HierarchyItem jobGroup : jobGroups) {
				hierarchyManagementClient.param("jobGroupIds", jobGroup.getId());
			}
		} else {
			hierarchyManagementClient.param("jobGroupIds", "");
		}		
		try {			
			hierarchyManagementClient.post(null);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.NODE_SERVICE_020, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
	}
	

}
