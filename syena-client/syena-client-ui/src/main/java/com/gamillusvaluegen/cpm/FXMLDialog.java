package com.gamillusvaluegen.cpm;

import java.io.IOException;
import java.net.URL;

import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.Duration;

public class FXMLDialog extends Stage {
	
	private Parent node;
    
    public FXMLDialog(DialogController controller, URL fxml, Window owner) {
        this(controller, fxml, owner, StageStyle.DECORATED,0,0);
    }
    
    public FXMLDialog(Parent node, Window owner){
          super(StageStyle.TRANSPARENT);
          if(null != node){
                DialogController controller = (DialogController) node;
                controller.setDialog(this);
          }
        
          init(owner);
          createScene(node);
    }
    
    private void createScene(Parent node){
         this.node = node;
         StackPane layout = new StackPane();
           layout.getChildren().setAll(
                    createGlassPane()
                    );
           if(null != node){
               layout.getChildren().add(node);
           }
           
           Scene scene = new Scene(layout, Color.TRANSPARENT);            
           scene.getStylesheets().add("/styles/Styles.css");     
           scene.getStylesheets().add("/styles/schedule.css");      
           setScene(scene);  
        
    }
    
    private void init(Window owner){
        ScreensConfiguration.getInstance().setCurrentParentWindow(this);
        initOwner(owner);
        initModality(Modality.WINDOW_MODAL);
    }
    
    
    public FXMLDialog(final DialogController controller, URL fxml, Window owner, StageStyle style,double x, double y) {
        super(StageStyle.TRANSPARENT);        
        init(owner);
        FXMLLoader loader = new FXMLLoader(fxml);
        try {
            loader.setControllerFactory(new Callback<Class<?>, Object>() {
                @Override
                public Object call(Class<?> aClass) {
                    return controller;
                }
            });
            controller.setDialog(this);
            Parent node = loader.load();
//            parent.setLayoutX(x);
//            parent.setLayoutY(y);
            createScene(node);
          
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
    }
    
    private Pane createGlassPane() {
        final Pane glassPane = new Pane();
        glassPane.setPrefSize(ScreensConfiguration.getInstance().getWidth(), ScreensConfiguration.getInstance().getHeight());
        glassPane.getStyleClass().add(
                "modal-dialog-glass"
        );
        
        return glassPane;
    }
    
    @Override
    public void close() {    
    	ScreensConfiguration.getInstance().setCurrentParentWindow(ScreensConfiguration.getInstance().getPrimaryStage());
        super.close();        
    }
    
    public void displayAndWait(){
    	if(null != node){
    		setOpenTransition(node);
    	}
    	super.showAndWait();
    }
    
    public void display(){
    	if(null != node){
    		setOpenTransition(node);
    	}
    	super.show();
    }
    
    public void display(PathTransition transition){
    	if(null != node){
    		transition.setNode(node);
    		transition.play();
    	}
    	super.show();
    }
    
    private void setCloseTransition(Parent screen){
    	FadeTransition ft = new FadeTransition(Duration.millis(750), screen);    	
    	ft.setFromValue(1.0);
    	ft.setToValue(0.0);
    	ft.setCycleCount(1);
    	ft.setAutoReverse(true);
    	ft.play();  
    }
    
    private void setOpenTransition( Parent screen){
    	FadeTransition ft = new FadeTransition(Duration.millis(750), screen);    	
    	ft.setFromValue(0.0);
    	ft.setToValue(1.0);
    	ft.setCycleCount(1);
    	ft.setAutoReverse(true);
    	ft.play();   	
    }
    
   
    
}
