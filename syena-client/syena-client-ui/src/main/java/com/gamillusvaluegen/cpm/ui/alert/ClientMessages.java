package com.gamillusvaluegen.cpm.ui.alert;

public interface ClientMessages {

	public String SCHEDULING_SEND_APRROVE = "Schedule send for approval";
	public String STATUS_UPDATE = "Status updated successfully";
	public String SCHEDULE_APPROVED = "Schedule Approved successfully";
	public String SCHEDULE_UPDATED = "Schedule Updated successfully";
	public String LINK_SUCCESSFULLY_REMOVED = "Link Removed Successfully";
	public String INVALID_NODE_DAIRY = "Select valid node to create new thread";
	public String INVALID_TOPIC_DAIRY = "Please enter a meaningful topic";
	public String INVALID_HEADING_WIKI = "Please enter a meaningful heading";
	public String INVALID_CONTENT_WIKI = "Content must not empty";
	
	public String SEARCH_FUTURE_DATE = "Please give the date before the application date.";
	
}
