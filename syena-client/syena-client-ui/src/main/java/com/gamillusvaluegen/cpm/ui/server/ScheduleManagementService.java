/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import static com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService.url;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.schedule.WorkSchedule;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.ScheduleManagmentServiceNames;

/**
 *
 * @author hp
 */
public class ScheduleManagementService {

    public static void setJobSchedule(long jobId, WorkSchedule workSchedule,int personDays) throws ServiceFailedException {
        RestCaller scheduleManagementClient = createRestCallser();
        scheduleManagementClient.path(ScheduleManagmentServiceNames.setJobSchedule);
        scheduleManagementClient.param("jobId", jobId).param("personDays", personDays);
        try {
            scheduleManagementClient.post(workSchedule, WorkSchedule.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_001, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }

    }
    
	public static void validateJobSchedule(long jobId,  WorkSchedule workSchedule,int personDays) throws ServiceFailedException  {
		RestCaller scheduleManagementClient = createRestCallser();
        scheduleManagementClient.path(ScheduleManagmentServiceNames.validateJobSchedule);
        scheduleManagementClient.param("jobId", jobId).param("personDays", personDays);
        try {
            scheduleManagementClient.post(workSchedule, WorkSchedule.class);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_001, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
	}

    public static void setHierarchybSchedule(long hierarchyId, WorkTime workTime) throws ServiceFailedException {
        RestCaller scheduleManagementClient = createRestCallser();
        scheduleManagementClient.path(ScheduleManagmentServiceNames.setHierarchybSchedule);
         scheduleManagementClient.param("hierarchyId", hierarchyId);
        try {
            scheduleManagementClient.post(workTime,WorkTime.class);
        } catch (RestCallException ex) {
              Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_002, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }

    public static void setJobGroupSchedule(long jobGrouId, WorkSchedule workSchedule,int personDays) throws ServiceFailedException {
        RestCaller scheduleManagementClient = createRestCallser();
        scheduleManagementClient.path(ScheduleManagmentServiceNames.setJobGroupSchedule);
        scheduleManagementClient.param("jobGrouId", jobGrouId);
        scheduleManagementClient.param("personDays", personDays);
      
        try {
            scheduleManagementClient.post(workSchedule,WorkSchedule.class);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_003, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
    }   
    
    public static void validateJobGroupWorkSchedule(long jobGrouId, WorkSchedule workSchedule,int personDays) throws ServiceFailedException  {
    	 RestCaller scheduleManagementClient = createRestCallser();
         scheduleManagementClient.path(ScheduleManagmentServiceNames.validateJobGroupWorkSchedule);
         scheduleManagementClient.param("jobGrouId", jobGrouId);
         scheduleManagementClient.param("personDays", personDays);
       
         try {
             scheduleManagementClient.post(workSchedule,WorkSchedule.class);
         } catch (RestCallException ex) {
              Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_003, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
}
      
	public static void setUnApprovedJobSchedule(long jobId, WorkSchedule workSchedule, int personDays) throws ServiceFailedException {
    	 RestCaller scheduleManagementClient = createRestCallser();
         scheduleManagementClient.path(ScheduleManagmentServiceNames.setUnApprovedJobSchedule);
         scheduleManagementClient.param("jobId", jobId).param("personDays", personDays);
         try {
             scheduleManagementClient.post(workSchedule, WorkSchedule.class);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_001, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
	}

	
	public static void setUnApprovedHierarchybSchedule(long hierarchyId,WorkTime workTime) throws ServiceFailedException {
		   RestCaller scheduleManagementClient = createRestCallser();
	        scheduleManagementClient.path(ScheduleManagmentServiceNames.setUnApprovedHierarchybSchedule);
	         scheduleManagementClient.param("hierarchyId", hierarchyId);
	        try {
	            scheduleManagementClient.post(workTime,WorkTime.class);
	        } catch (RestCallException ex) {
	              Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_002, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	}

	
	public static void setUnApprovedJobGroupSchedule(long jobGrouId,WorkSchedule workSchedule, int personDays) throws ServiceFailedException  {
		 RestCaller scheduleManagementClient = createRestCallser();
	        scheduleManagementClient.path(ScheduleManagmentServiceNames.setUnApprovedJobGroupSchedule);
	        scheduleManagementClient.param("jobGrouId", jobGrouId);
	        scheduleManagementClient.param("personDays", personDays);
	        try {
	            scheduleManagementClient.post(workSchedule,WorkSchedule.class);
	        } catch (RestCallException ex) {
	             Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_003, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	}

	
	public static PageItr<HierarchyNode> getUnApprovedHierarchySchedules(String level, int pageNumber, int pageSize) throws ServiceFailedException{
		 RestCaller scheduleManagementClient = createRestCallser();
		 scheduleManagementClient.path(ScheduleManagmentServiceNames.getUnApprovedHierarchySchedules);
		 scheduleManagementClient.param("level", level).param("pageNumber", pageNumber).param("pageSize", pageSize);
	        PageItr<HierarchyNode> hierarchyNodes = null;
	        try {
	            TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
	            };
	            hierarchyNodes = scheduleManagementClient.get(type);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_004, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	        return hierarchyNodes;
	}

	
	public static PageItr<JobGroup> getUnApprovedJobGroupSchedules(int pageNumber, int pageSize) throws ServiceFailedException {
		 RestCaller scheduleManagementClient = createRestCallser();
		 scheduleManagementClient
	                .path(ScheduleManagmentServiceNames.getUnApprovedJobGroupSchedules);
		 scheduleManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
	        PageItr<JobGroup> jobgroups = null;
	        try {
	            TypeReference<PageItr<JobGroup>> type = new TypeReference<PageItr<JobGroup>>() {
	            };
	            jobgroups = scheduleManagementClient.get(type);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_005, ex.getMessage());
	            throw new ServiceFailedException(erroMessage);
	        }
	        return jobgroups;
	}

	public static PageItr<Job> getUnApprovedJobSchedules(int pageNumber, int pageSize) throws ServiceFailedException  {
		  RestCaller scheduleManagementClient = createRestCallser();
		  scheduleManagementClient.path(ScheduleManagmentServiceNames.getUnApprovedJobSchedules);
		  scheduleManagementClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
	        PageItr<Job> jobs = null;
	        try {
	            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
	            };
	            jobs = scheduleManagementClient.get(type);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_006, ex.getMessage());
	            throw new ServiceFailedException(erroMessage);
	        }
	        return jobs;
	}
	
	
	

	
	public static void rejectUnApprovedJobSchedule(long jobId) throws ServiceFailedException {
		 RestCaller scheduleManagementClient = createRestCallser();
	        scheduleManagementClient.path(ScheduleManagmentServiceNames.rejectUnApprovedJobSchedule);
	        scheduleManagementClient.param("jobId", jobId);
	        try {
	            scheduleManagementClient.post(null);
	        } catch (RestCallException ex) {
	            Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_007, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	}

	public static void rejectUnApprovedHierarchybSchedule(long hierarchyId) throws ServiceFailedException {
		 RestCaller scheduleManagementClient = createRestCallser();
	        scheduleManagementClient.path(ScheduleManagmentServiceNames.rejectUnApprovedHierarchybSchedule);
	         scheduleManagementClient.param("hierarchyId", hierarchyId);
	        try {
	            scheduleManagementClient.post(null);
	        } catch (RestCallException ex) {
	              Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_007, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	}

	
	public static void rejectUnApprovedJobGroupSchedule(long jobGrouId) throws ServiceFailedException {
		 RestCaller scheduleManagementClient = createRestCallser();
	        scheduleManagementClient.path(ScheduleManagmentServiceNames.rejectUnApprovedJobGroupSchedule);
	        scheduleManagementClient.param("jobGrouId", jobGrouId);
	        try {
	            scheduleManagementClient.post(null);
	        } catch (RestCallException ex) {
	             Message erroMessage = new Message(ExceptionMessages.SCHEDULE_SERVICE_007, ex.getMessage() );
	            throw new ServiceFailedException(erroMessage);
	        }
	}
    

    private static RestCaller createRestCallser() {
        RestCaller scheduleManagementClient = new RestCaller(url);
        scheduleManagementClient.path("scheduleManagment");
        return scheduleManagementClient;
    }

}
