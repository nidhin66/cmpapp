/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;

/**
 *
 * @author hp
 */
public class HierarchyEditItemField extends HBox{
    
    private TextField hierarchyText;
    private Button detailsButton;
    
    private HierarchyItem hierarchyNode;
    
    private HierarchyLevel level;
    
    private List<HierarchyEditItemFieldPanel> childPanels = null;
    
    private String mode = "E";
    
    public HierarchyEditItemField(HierarchyLevel level,StandardHierarchyNode hierarchyNode, String mode){
        this(level);
        this.hierarchyNode = hierarchyNode;
        this.mode = mode;
    }
    
    public HierarchyEditItemField(HierarchyLevel level){
        this.level = level;
        hierarchyText = new TextField();
        hierarchyText.setEditable(false);
        hierarchyText.setMinSize(100, 25);
        hierarchyText.setMaxSize(100, 25);
        hierarchyText.setStyle("-fx-border-width:  1 0 1 1;-fx-border-color: #C6B2BB;-fx-border-radius: 0 0 0 0;"
                + "-fx-background-radius: 0 0 0 0;-fx-background-color:transparent;");
        
        
        detailsButton = new Button();
        detailsButton.setMinSize(25, 25);
        detailsButton.setMaxSize(25, 25);
        detailsButton.setStyle("-fx-border-width:  1 1 1 0;-fx-border-color: #C6B2BB;-fx-border-radius: 0 0 0 0;"
                + "-fx-background-radius: 0 0 0 0;-fx-background-color:transparent;");
        detailsButton.getStyleClass().add("hierarchy-edit-details-button");
        HierarchyEditItemDetailsActionHandler actionHandler = new HierarchyEditItemDetailsActionHandler(this);
        detailsButton.setOnAction(actionHandler);
      
        
       
        this.setSpacing(0);
        this.setAlignment(Pos.CENTER);
        this.getChildren().addAll(hierarchyText,detailsButton);
    }

    public HierarchyItem getHierarchyNode() {
        return hierarchyNode;
    }

    public void setHierarchyNode(HierarchyItem hierarchyNode) {
        if(null != hierarchyNode){
            hierarchyText.setText(hierarchyNode.getName());
        }
        this.hierarchyNode = hierarchyNode;
    }

    public List<HierarchyEditItemFieldPanel> getChildPanels() {
        return childPanels;
    }

    
     public void addChildPanel(HierarchyEditItemFieldPanel childPanel) {
         if(null == childPanels){
             childPanels = new ArrayList<>();
         }
         childPanels.add(childPanel);
     }

    public HierarchyLevel getLevel() {
        return level;
    }

    public void setViewMode() {
        this.mode = "V";        
    }

    public String getMode() {
        return mode;
    }
    
    
    
    
     
     
    
}
