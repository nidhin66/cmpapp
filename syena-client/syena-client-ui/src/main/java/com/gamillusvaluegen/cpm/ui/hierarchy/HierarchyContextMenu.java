/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.StageStyle;

import com.gamillusvaluegen.cpm.ui.hierarchy.schedule.ScheduleMode;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.hierarchy.edit.HierarchyEditController;
import com.gamillusvaluegen.cpm.ui.hierarchy.edit.HierarchyEditItemField;
import com.gamillusvaluegen.cpm.ui.hierarchy.edit.HierarchyNodeItemController;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.JobGroupPane;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.JobPane;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.JobItem;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.entity.hierarchy.StandardHierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;

/**
 *
 * @author hp
 */
public class HierarchyContextMenu extends ContextMenu {

    HierarchyItemPanel hierarchyItemPanel;

    public HierarchyContextMenu(final HierarchyItemPanel hierarchyItemPanel) {
        this.hierarchyItemPanel = hierarchyItemPanel;
        this.setAutoHide(true);
        this.getStyleClass().add("hierarchy-context-menu");

    }

    public void setMenuItems(Map<String, HierarchyItem> selectedParentNodes) {
        if (null != getItems()) {
            getItems().clear();
        }

//        for(Iterator<MenuItem> it = getItems().iterator();it.hasNext();){
//            getItems().remove(it.next());
//        }
        createDefaultManuItems();
        createContextSpecificMenus(selectedParentNodes);
    }

    private void createContextSpecificMenus(Map<String, HierarchyItem> selectedParentNodes) {
        if (null != selectedParentNodes) {
            for (Iterator<String> it = selectedParentNodes.keySet().iterator(); it.hasNext();) {
                String levelName = it.next();
                HierarchyItem item = selectedParentNodes.get(levelName);
                HierarchyLinkMenuItem menuItem = new HierarchyLinkMenuItem(hierarchyItemPanel, item);
                menuItem.setText("Link to " + item.getName());
                menuItem.getStyleClass().add("hierarchy-context-menu-item");
                this.getItems().add(menuItem);
            }
        }
    }

    private void createDefaultManuItems() {
        MenuItem scheduleMenuItem = new MenuItem("Schedule");
        scheduleMenuItem.getStyleClass().add("hierarchy-context-menu-item");
        
        MenuItem statusUpdateMenuItem = new MenuItem("Status Update");
        statusUpdateMenuItem.getStyleClass().add("hierarchy-context-menu-item");
        
        MenuItem approveScheduleMenu = new MenuItem("Approve Schedule");
        approveScheduleMenu.getStyleClass().add("hierarchy-context-menu-item");
        
        MenuItem viewScheduleMenu = new MenuItem("View Schedule");
        viewScheduleMenu.getStyleClass().add("hierarchy-context-menu-item");
        
        
        MenuItem cmItem2 = new MenuItem("Update");
        cmItem2.getStyleClass().add("hierarchy-context-menu-item");
        
        MenuItem cmItem3 = new MenuItem("Details");
        cmItem3.getStyleClass().add("hierarchy-context-menu-item");
        
        MenuItem removeLinkMenu = new MenuItem("Remove Hierarchy Link");
        removeLinkMenu.getStyleClass().add("hierarchy-context-menu-item");
        
        removeLinkMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				hierarchyItemPanel.removeHierarchyLink();
			}
		});
        
        
        approveScheduleMenu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                       
                hierarchyItemPanel.approveSchedule();
            }
        });
        
        scheduleMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                       
                hierarchyItemPanel.schedule(ScheduleMode.SCHEDULING);
            }
        });
        
        statusUpdateMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                       
                hierarchyItemPanel.schedule(ScheduleMode.STATUS_UPDATE);
            }
        });
        viewScheduleMenu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                       
                hierarchyItemPanel.schedule(ScheduleMode.VIEW_SCHEDULE);
            }
        });
        

        cmItem2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (null != hierarchyItemPanel.getLevel()) {
                    List<? extends HierarchyItem> selectedItems = hierarchyItemPanel.getSelectedItems();
                    StandardHierarchyNode hierarchyNode = null;
                    List<StandardHierarchyNode> hierarchyNodes = null;
                    if (null != selectedItems && selectedItems.size() > 0) {
                        hierarchyNodes = new ArrayList<>();

                        hierarchyNode = (StandardHierarchyNode) selectedItems.get(0);
                        hierarchyNodes.add(hierarchyNode);
                    }
                    boolean standard = false;
                    
                	if(hierarchyItemPanel.getContextID() == 1){
                		standard = true;
                	}
                    FXMLDialog addNodeItemDialog = new FXMLDialog(new HierarchyEditController(hierarchyItemPanel.getLevel(), hierarchyNodes, standard), getClass().getResource("/fxml/plant_hierarchy_edit.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,0,0);
                    addNodeItemDialog.display();
                }else {
                     List<? extends HierarchyItem> selectedItems = hierarchyItemPanel.getSelectedItems();
                   
                    JobItem jobItem = null;
                    if (null != selectedItems && selectedItems.size() > 0) {
                    	 HierarchyItem parentItem = null;
                        if(hierarchyItemPanel.getContextID() == 1 || hierarchyItemPanel.getContextID() == 3){
                            jobItem = (JobGroup) selectedItems.get(0);
                            if(hierarchyItemPanel.getContextID() == 3){
                   			 parentItem = hierarchyItemPanel.getImmediateSelectedParent();
                   		    }
                            FXMLDialog editJobDialog = new FXMLDialog(new JobGroupPane(parentItem,(JobGroup) jobItem), ScreensConfiguration.getInstance().getPrimaryStage());
                            editJobDialog.display();
                        }else{
                             jobItem = (Job) selectedItems.get(0);
                             if(hierarchyItemPanel.getContextID() == 4){
                    			 parentItem = hierarchyItemPanel.getImmediateSelectedParent();
                    		 }
                             FXMLDialog editJobDialog = new FXMLDialog(new JobPane(parentItem,(Job) jobItem), ScreensConfiguration.getInstance().getPrimaryStage());
                             editJobDialog.display();
                        }
                       
                    }
//                     FXMLDialog editJobDialog = new FXMLDialog(new HierarchyJobEditController(hierarchyItemPanel,jobItem), getClass().getResource("/fxml/job_edit_pane.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,0,0);
//                     editJobDialog.display();
                }

            }
        });
         cmItem3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (null != hierarchyItemPanel.getLevel()) {

                    HierarchyEditItemField itemField = new HierarchyEditItemField(hierarchyItemPanel.getLevel(), (StandardHierarchyNode) hierarchyItemPanel.getSelectedItem(), "V");
                                 FXMLDialog addNodeItemDialog = new FXMLDialog(new HierarchyNodeItemController(itemField), getClass().getResource("/fxml/node_item.fxml"), ScreensConfiguration.getInstance().getCurrentParentWindow(), StageStyle.UNDECORATED,0,0);
                                 addNodeItemDialog.display();      
                }else{
                	 List<? extends HierarchyItem> selectedItems = hierarchyItemPanel.getSelectedItems();
                     
                     JobItem jobItem = null;
                     if (null != selectedItems && selectedItems.size() > 0) {
                    	 HierarchyItem parentItem = null;
                         if(hierarchyItemPanel.getContextID() == 1 || hierarchyItemPanel.getContextID() == 3){
                             jobItem = (JobGroup) selectedItems.get(0);
                             if(hierarchyItemPanel.getContextID() == 3){
                       			 parentItem = hierarchyItemPanel.getImmediateSelectedParent();
                       		    }
                                FXMLDialog editJobDialog = new FXMLDialog(new JobGroupPane(parentItem,(JobGroup) jobItem,"V"), ScreensConfiguration.getInstance().getPrimaryStage());
                                editJobDialog.display();
                         }else{
                              jobItem = (Job) selectedItems.get(0);
                              if(hierarchyItemPanel.getContextID() == 4){
                     			 parentItem = hierarchyItemPanel.getImmediateSelectedParent();
                     		 }
                              FXMLDialog editJobDialog = new FXMLDialog(new JobPane(parentItem,(Job) jobItem,"V"), ScreensConfiguration.getInstance().getPrimaryStage());
                              editJobDialog.display();
                         }
                        
                     }
//                      FXMLDialog editJobDialog = new FXMLDialog(new HierarchyJobEditController(hierarchyItemPanel,jobItem), getClass().getResource("/fxml/job_edit_pane.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,0,0);
//                      editJobDialog.display();
                }

                
            }
        });
         
         
        this.getItems().addAll(cmItem2,cmItem3);
        
        if(hierarchyItemPanel.isApproved()){
        	HierarchyItem selectedItem = hierarchyItemPanel.getSelectedItem();
        	if(null != selectedItem.getStatus()){
        		WorkStatusType statusType = WorkStatusType.getWorkStatusType(selectedItem.getStatus());
            	if(statusType != WorkStatusType.Completed_on_schedule && statusType !=  WorkStatusType.Completed_with_delays){
            		this.getItems().add(scheduleMenuItem);
            		if(statusType != WorkStatusType.Not_scheduled){
            			this.getItems().add(statusUpdateMenuItem);
            		}            		
            	}
            	if(statusType == WorkStatusType.Not_scheduled){
            			this.getItems().add(removeLinkMenu);
            	}else{
            		this.getItems().add(viewScheduleMenu);
            	}            	
        	}      	
        	
        }else{
        	this.getItems().add(approveScheduleMenu);
        }
               
        
        
    }

}
