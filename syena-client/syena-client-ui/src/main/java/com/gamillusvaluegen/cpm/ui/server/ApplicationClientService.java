package com.gamillusvaluegen.cpm.ui.server;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.application.ApplicationConfiguration;
import com.gvg.syena.core.api.services.names.ApplicationServiceServiceNames;

public class ApplicationClientService {
	
	private static final String url = ScreensConfiguration.getUrl();
	
	   private static RestCaller createRestCallser() {
	        RestCaller applicationServiceClient = new RestCaller(url);
	        applicationServiceClient.path("application");
	        return applicationServiceClient;
	    }
	   
	   
	   public static List<ApplicationConfiguration> getApplicationConfigs()  {
		   RestCaller applicationServiceClient = createRestCallser();
		   applicationServiceClient.path(ApplicationServiceServiceNames.getApplicationConfigs);
		  

		   List<ApplicationConfiguration> applicationconfigs = null;
	        try {
	            TypeReference<List<ApplicationConfiguration>> type = new TypeReference<List<ApplicationConfiguration>>() {
	            };
	            applicationconfigs = applicationServiceClient.get(type);
	        } catch (RestCallException ex) {
	        	ex.printStackTrace();
//	            Message erroMessage = new Message(ExceptionMessages.CHECKLIST_SERVICE_002, ex.getMessage() );
//	            throw new ServiceFailedException(erroMessage);
	            
	        } catch (ServiceFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return applicationconfigs;
		}


}
