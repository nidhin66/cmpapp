/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.search;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.DateFormatConverter;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilterUtility;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.JobSearchClientService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.common.WorkStatusType;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.serach.DateRange;
import com.gvg.syena.core.api.services.serach.JobProperties;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Popup;

/**
 *
 * @author hp
 */
public class AdvancedSearchController {

	// @FXML
	// private TextField tagNumber;

	@FXML
	private TextField name;

	@FXML
	private TextArea description;

	@FXML
	private TextField jobContactor;

	// @FXML
	// private TextField scheduleId;
	//
	// @FXML
	// private TextField scheduleName;
	//
	// @FXML
	// private TextField jobId;

	// @FXML
	// private TextField jobName;

	@FXML
	private ComboBox<WorkStatusType> jobstatus;

	@FXML
	private TextField jobInitiator;

	@FXML
	private TextField jobOwner;

	@FXML
	private TextField mileStones;

	@FXML
	private DatePicker plannedStartDate;

	@FXML
	private DatePicker plannedEndDate;
	
	@FXML
	private DatePicker actualStartDate;

	@FXML
	private DatePicker actualEndDate;

	private Popup popUp;

	@FXML
	private void initialize() {
		ObservableList<WorkStatusType> items = FXCollections.observableArrayList(WorkStatusType.values());
		jobstatus.setItems(items);
		
		
		plannedStartDate.setConverter(new DateFormatConverter());
		plannedEndDate.setConverter(new DateFormatConverter());
		actualStartDate.setConverter(new DateFormatConverter());
		actualStartDate.setConverter(new DateFormatConverter());
	}
	
	@FXML
	private void searchJobGroup(){
		try {

			JobProperties jobSearchOption = getJobSearchCriteria();
			
			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.ADVANCED_SEARCH_JOB);
			filter.setJobProperties(jobSearchOption);
			PageItr<? extends HierarchyItem> pageData = HierarchyFilterUtility.getPage(NodeCategory.jobgroup,0, 30, filter);

			HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
			jobPanel.loadFilterData(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, pageData);
			this.popUp.hide();
			
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@FXML
	private void searchJob() {
		try {

			JobProperties jobSearchOption = getJobSearchCriteria();
			
			HierarchyFilter filter = new HierarchyFilter(HierarchyFilterUtility.ADVANCED_SEARCH_JOB);
			filter.setJobProperties(jobSearchOption);
			PageItr<? extends HierarchyItem> pageData = HierarchyFilterUtility.getPage(NodeCategory.job,0, 30, filter);

			HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
			jobPanel.loadFilterData(CPMClientConstants.CONTEXT_LINKED_JOBS, filter, pageData);
			this.popUp.hide();
			
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}
	
	private JobProperties getJobSearchCriteria(){
		JobProperties jobSearchOption = new JobProperties();
		if (!Utilities.isNullOrEmpty(description.getText())) {
			jobSearchOption.setDescription(description.getText());
		}
		
		if (null != plannedStartDate.getValue() || null != plannedEndDate.getValue()) {
			DateRange plannedDateRange = new DateRange();
			if(null != plannedStartDate.getValue()){
				plannedDateRange.setFromDate(Utilities.LocalDateToDate(plannedStartDate.getValue()));
			}
			if(null != plannedEndDate.getValue()){
				plannedDateRange.setToDate(Utilities.LocalDateToDate(plannedEndDate.getValue()));
			}
			jobSearchOption.setPlannedDateRange(plannedDateRange);
		}
		
		if (null != actualStartDate.getValue() || null != actualEndDate.getValue()) {
			DateRange actualDateRange = new DateRange();
			if(null != actualStartDate.getValue()){
				actualDateRange.setFromDate(Utilities.LocalDateToDate(actualStartDate.getValue()));
			}
			if(null != actualEndDate.getValue()){
				actualDateRange.setToDate(Utilities.LocalDateToDate(actualEndDate.getValue()));
			}
			jobSearchOption.setActualDateRange(actualDateRange);
		}
		
		if (!Utilities.isNullOrEmpty(jobInitiator.getText())) {
			jobSearchOption.setInitiatorId(jobInitiator.getText());
		}
		// if(!Utilities.isNullOrEmpty(jobId.getText())){
		// jobSearchOption.setJobId(Long.valueOf(jobId.getText()));
		// }
		if (!Utilities.isNullOrEmpty(mileStones.getText())) {
			jobSearchOption.setMilestoneDescription(mileStones.getText());
		}
		
		
		
		if (!Utilities.isNullOrEmpty(name.getText())) {
			jobSearchOption.setName(name.getText());
		}
		
		if (!Utilities.isNullOrEmpty(jobOwner.getText())) {
			jobSearchOption.setOwnerId(jobOwner.getText());
		}
		

		if (null != jobstatus.getSelectionModel().getSelectedItem()) {
			jobSearchOption.setWorkStatus(jobstatus.getSelectionModel().getSelectedItem().getWorkStatus());
		}
		
		if (!Utilities.isNullOrEmpty(jobContactor.getText())) {
			jobSearchOption.setExecutor(jobContactor.getText());
		}
		return jobSearchOption;
	}

	@FXML
	private void cancelAction() {
		this.popUp.hide();
	}

	public void setPopUp(Popup popUp) {
		this.popUp = popUp;
	}

}
