
package com.gamillusvaluegen.cpm.ui.notification;

import java.io.IOException;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.server.NotificationClientService;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;


public class NotificationButton extends AnchorPane{
	
	@FXML
	private Label notificationLabel;
	
		
	public NotificationButton(){
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/notification_button.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		notificationLabel.setVisible(false);
		createTask();
	}
	
	 private Task<String> createTask() {
		
		 Task task = new Task<Void>() {
			  @Override
			  public Void call() throws Exception {
				
			    while (true) {
			    	
			    	
			      final long count = NotificationClientService.getNotificationCount(ClientConfig.getInstance().getLoginUser().getId());
			     
			      if(count > 0){
			    	  notificationLabel.setVisible(true);
			      }else{
			    	  notificationLabel.setVisible(false);
			      }
			      Platform.runLater(new Runnable() {
			        public void run() {
			          notificationLabel.setText("" + count);
			        }
			      });
			      
			      Thread.sleep(60 * 1000 * 15);
			      			      
			    }
			  }
			};
			Thread th = new Thread(task);
			th.setDaemon(true);
			th.start();
			return task; 
     }

	 @FXML
	 private void showNotifications(){
		 NotificationWindow notificationWindow = new NotificationWindow();
		 FXMLDialog dialog = new FXMLDialog(notificationWindow, ScreensConfiguration.getInstance().getPrimaryStage());
		 dialog.show();
	 }
}
