package com.gamillusvaluegen.cpm.ui.notification;

import java.io.IOException;
import java.util.Date;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.NotificationClientService;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.services.notification.Notification;
import com.gvg.syena.core.api.services.notification.NotificationType;


public class NotificationListPane extends BorderPane {

	@FXML
	private TableView<Notification> notificationListTable;

	@FXML
	private TableColumn<Notification, String> notificationNamecolumn;

	@FXML
	private TableColumn<Notification, String> notificationSubjectColumn;

	@FXML
	private TableColumn<Notification, Date> notifiactionDateColumn;
	
	private NotificationDataPane dataPane;
	
	private NotificationType notificationType;
	
	private PageItr<Notification> currentPage;
	
	@FXML
	private Label pagingLabel;
	
	public NotificationListPane(final NotificationDataPane dataPane) {
		this.dataPane = dataPane;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/notification_list_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		
		notificationNamecolumn.setCellValueFactory(new PropertyValueFactory<Notification, String>("name"));
		notificationNamecolumn
				.setCellFactory(new Callback<TableColumn<Notification, String>, TableCell<Notification, String>>() {

					public TableCell<Notification, String> call(TableColumn<Notification, String> param) {
						TableCell<Notification, String> cell = new TableCell<Notification, String>() {
							@Override
							protected void updateItem(String item, boolean empty) {
								super.updateItem(item, empty);
								if (!empty) {
									setText(item);
								} else {
									setText(null);
								}
							}
						};
						return cell;
					}
				});
		
		notificationSubjectColumn.setCellValueFactory(new PropertyValueFactory<Notification, String>("subject"));
		notificationSubjectColumn
				.setCellFactory(new Callback<TableColumn<Notification, String>, TableCell<Notification, String>>() {

					public TableCell<Notification, String> call(TableColumn<Notification, String> param) {
						TableCell<Notification, String> cell = new TableCell<Notification, String>() {
							@Override
							protected void updateItem(String item, boolean empty) {
								super.updateItem(item, empty);
								if (!empty) {
									setText(item);
								} else {
									setText(null);
								}
							}
						};
						return cell;
					}
				});
		
		notifiactionDateColumn.setCellValueFactory(new PropertyValueFactory<Notification, Date>("date"));
		notifiactionDateColumn
				.setCellFactory(new Callback<TableColumn<Notification, Date>, TableCell<Notification, Date>>() {

					public TableCell<Notification, Date> call(TableColumn<Notification, Date> param) {
						TableCell<Notification, Date> cell = new TableCell<Notification, Date>() {
							@Override
							protected void updateItem(Date item, boolean empty) {
								super.updateItem(item, empty);
								if (!empty) {
									setText(item.toString());
								} else {
									setText(null);
								}
							}
						};
						return cell;
					}
				});
		
		notificationListTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Notification>() {

			public void changed(ObservableValue<? extends Notification> observable, Notification oldValue, Notification newValue) {
			    if (newValue != null) {
			    	dataPane.setMessagePaneData(newValue);
			    }
				
			}
		});
		
		
	}
	
	private void loadData(PageItr<Notification> notificationPage){
		if(null != notificationListTable.getItems()){
			notificationListTable.getItems().clear();
		}
		String labelString = "";
		if(null != notificationPage){
			if(null != notificationPage.getContent() && notificationPage.getContent().size() > 0){
				ObservableList<Notification> data = FXCollections.observableArrayList(notificationPage.getContent());
				notificationListTable.setItems(data);
				notificationListTable.getSelectionModel().selectFirst();
			}
			if(notificationPage.getTotalPages() > 0){
				labelString = (notificationPage.getCurrentPage() +1) + " of " + notificationPage.getTotalPages();
			}
		}
		pagingLabel.setText(labelString);
		currentPage = notificationPage;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	@FXML
	private void loadFirstPage(){
		loadPage(0);
	}
	
	@FXML
	private void loadPreviousPage(){
		if (null != currentPage) {
			if ((currentPage.getCurrentPage()) > 0) {
				int pageNumber = currentPage.getCurrentPage() - 1;
				loadPage(pageNumber);
			}
		}
	}
	
	@FXML
	private void loadNextPage(){
		if (null != currentPage) {
			if ((currentPage.getCurrentPage() + 1) < currentPage.getTotalPages()) {
				int pageNumber = currentPage.getCurrentPage() + 1;
				loadPage(pageNumber);
			}
		}
		
	}
	
	@FXML
	private void loadLastPage(){
		if (null != currentPage) {			
			int pageNumber = currentPage.getTotalPages() - 1;
			loadPage(pageNumber);
		}
		
	}
	
	public void loadPage(int pageNumber){
		PageItr<Notification> notificationPage = null;
		try {
			notificationPage = NotificationClientService.getNotifications(ClientConfig.getInstance().getLoginUser().getId(),notificationType, pageNumber, 10);			
		} catch (ServiceFailedException e) {
		
		}
		loadData(notificationPage);
	}
	
	
}
