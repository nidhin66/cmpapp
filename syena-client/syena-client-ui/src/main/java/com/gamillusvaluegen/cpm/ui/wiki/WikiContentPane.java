package com.gamillusvaluegen.cpm.ui.wiki;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.ClientMessages;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.wiki.WikiContent;

public class WikiContentPane extends BorderPane {
	
	@FXML
	private ImageView wikiImage;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button saveButton;
	
	@FXML
	private Button cancelButton;
	
	@FXML
	private TextField headingText;
	
	@FXML
	private HTMLEditor detailsText;
	
	@FXML
	private WebView webView;
	
	@FXML
	private Button editButton;
	
	@FXML
	private Button changeButton;
	
	@FXML
	private VBox imageBox;
	
	@FXML
	private Button removeImageButton;
	
	private boolean editMode;
	
	private WikiPagePane parent;
	
	private WikiContent content;
	
	private WikiContentType type;
	
	public WikiContentPane(WikiPagePane parentPage,WikiContent content, WikiContentType type){
		this.content = content;
		this.parent = parentPage;
		this.type = type;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/wiki_content_edit_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
//		wikiImage.setPreserveRatio(true);
//		wikiImage.setCache(true);
//		wikiImage.fitWidthProperty().bind(imageBox.widthProperty());
//		wikiImage.fitHeightProperty().bind(imageBox.heightProperty());

		if(null != content){
			loadContent();
		}
	}
	
	private void loadContent(){
		headingText.setText(content.getHeading());
		detailsText.setHtmlText(content.getTextContent());
		webView.getEngine().loadContent(content.getTextContent());
		loadImage(content.getImage());
	}
	
	public void loadImage(byte[] imageInByte){
		if(null != imageInByte){
			InputStream in = new ByteArrayInputStream(imageInByte);
			BufferedImage bf;
			try {
				bf = ImageIO.read(in);
				Image image = SwingFXUtils.toFXImage(bf, null);
				wikiImage.setImage(image);
				wikiImage.setFitWidth(180);
				wikiImage.setFitHeight(275);
				wikiImage.setPreserveRatio(true);
				wikiImage.setCache(true);
//				 WritableImage wr = null;
//		         if (bf != null) {
//		             wr = new WritableImage(bf.getWidth(), bf.getHeight());
//		             PixelWriter pw = wr.getPixelWriter();
//		             for (int x = 0; x < bf.getWidth(); x++) {
//		                 for (int y = 0; y < bf.getHeight(); y++) {
//		                     pw.setArgb(x, y, bf.getRGB(x, y));
//		                 }
//		             }
//		         }		  
//		         wikiImage.setImage(wr);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
 		
	}
	
	@FXML
	private void onRemove(){
		if(null == content){
			content = new WikiContent();	
			content.setId(-1);
		}
		parent.removeContent(this);
	}
	
	@FXML
	public void onCancel(){
		if(null == content){
			content = new WikiContent();		
			content.setId(-1);
		}
		parent.cancel(this);
	}

	@FXML
	public void saveContent(){
		if(Utilities.isNullOrEmpty(headingText.getText())){
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, ClientMessages.INVALID_HEADING_WIKI);
			dialog.show();
		}else if (Utilities.isNullOrEmpty(detailsText.getHtmlText())){
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, ClientMessages.INVALID_CONTENT_WIKI);
			dialog.show();
		}else{
			if(null == content){
				content = new WikiContent();			
			}
			content.setHeading(headingText.getText());
			content.setTextContent(detailsText.getHtmlText());
			content.setImage(getImageByteArray());
			content.setUpdatedBy(ClientConfig.getInstance().getLoginUser().getId());
			content.setUpdatedOn(Utilities.getDate());
			parent.saveContent(this);
		}
		
	
	}
	
	private byte[] getImageByteArray(){
		if(null != wikiImage.getImage()){
			BufferedImage bImage = SwingFXUtils.fromFXImage(wikiImage.getImage(), null);
			ByteArrayOutputStream s = new ByteArrayOutputStream();
			byte[] res = null;
			try {
				ImageIO.write(bImage, "jpg", s);
				res  = s.toByteArray();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return res;
		}else { 
			return null;
		}
		
		
	}
	
	@FXML
	public void editContent(){
		parent.editContent(this);
	}

	public WikiContent getContent() {
		return content;
	}

	public WikiContentType getType() {
		return type;
	}
	
	public void setEditable(boolean editable){
		
		this.editMode = editable;
		
		if(type == WikiContentType.MAIN){
			removeButton.setVisible(false);
		}else{
			removeButton.setVisible(editable);
		}
		
		editButton.setVisible(!editable);
		changeButton.setVisible(editable);
		removeImageButton.setVisible(editable);
		cancelButton.setVisible(editable);
		saveButton.setVisible(editable);
		
		detailsText.setVisible(editable);
		webView.setVisible(!editable);
		
		
		headingText.setDisable(!editable);
		detailsText.setDisable(!editable);		
		
		
		detailsText.lookup(".top-toolbar").setManaged(editable);
		detailsText.lookup(".top-toolbar").setVisible(editable);

		detailsText.lookup(".bottom-toolbar").setManaged(editable);
		detailsText.lookup(".bottom-toolbar").setVisible(editable);
		
		
	}

	public boolean isEditMode() {
		return editMode;
	}
	
	@FXML
	public void uploadImage(){
		 FileChooser fileChooser = new FileChooser();
		 File file = fileChooser.showOpenDialog(ScreensConfiguration.getInstance().getCurrentParentWindow());
		 if(null != file){
			 try {
				BufferedImage originalImage = ImageIO.read(file);
				 WritableImage wr = null;
		         if (originalImage != null) {
		             wr = new WritableImage(originalImage.getWidth(), originalImage.getHeight());
		             PixelWriter pw = wr.getPixelWriter();
		             for (int x = 0; x < originalImage.getWidth(); x++) {
		                 for (int y = 0; y < originalImage.getHeight(); y++) {
		                     pw.setArgb(x, y, originalImage.getRGB(x, y));
		                 }
		             }
		         }		  
		         wikiImage.setImage(wr);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		 	
	}
	
	@FXML
	public void onRemoveImage(){
		wikiImage.setImage(null);
	}
	
	
	
	@Override
	public boolean equals(Object other) {
		 if (!(other instanceof WikiContentPane)) {
		        return false;
		    }else{
		    	WikiContentPane wikiContentPane = (WikiContentPane) other;
		    	WikiContent wikiContent = wikiContentPane.getContent();
		    	if(null == wikiContent){
		    		return true;
		    	}else{
//		    		 if(wikiContent.getId() > 0 ){
		    			    if(this.getContent() == null){
		    			    	return true;
		    			    }else{
		    			    	if(this.getContent().getId() == wikiContent.getId()){
									return true;
								}else{
									return false;
								}
		    			    }
							
//						 }else{
//							 return false;
//						 }
		    	}
				
		    }
		
	}

}
