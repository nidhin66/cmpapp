package com.gamillusvaluegen.cpm.ui.hierarchy.filter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;

import javafx.stage.Popup;

public class HierarchyFilter {
	
	private String filterIdentifier;
	
	private String searchString;
	
	private Map<String,Object> parameters = new HashMap<>();
	
	private JobProperties jobProperties = null;
	
	
	private Popup conditionPopup;
	
	
	public HierarchyFilter(String filterIdentifier){
		this.filterIdentifier = filterIdentifier;
	}
	
	public void addParameter(String key, Object value){
		parameters.put(key, value);
	}
	
	public Object getParameterValue(String key){
		return parameters.get(key);
	}

	public JobProperties getJobProperties() {
		return jobProperties;
	}

	public void setJobProperties(JobProperties jobProperties) {
		this.jobProperties = jobProperties;
	}
	

	public String getFilterIdentifier() {
		return filterIdentifier;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}
	
	public String getStyleClass(){
		return HierarchyFilterUtility.getStyleClass(this);
	}
	
	
	
	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getLabelString(){
		String label = "";		
		if(!filterIdentifier.equalsIgnoreCase(HierarchyFilterUtility.QUICK_CONSISTENTLY_DELAYED_SYSTEM) && !filterIdentifier.equalsIgnoreCase(HierarchyFilterUtility.QUICK_CONSISTENTLY_DELAYED_UNIT)){
			if(filterIdentifier.equalsIgnoreCase(HierarchyFilterUtility.ADVANCED_SEARCH_JOB)){
				label = "Advanced Search";
			}else if(filterIdentifier.equalsIgnoreCase(HierarchyFilterUtility.QUICK_JOBS_TO_START_IN_N_DAYS)){
				int days =  (int) getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS);
				label = "Start in " + days + " days";
			}else if(filterIdentifier.equalsIgnoreCase(HierarchyFilterUtility.QUICK_JOBS_TO_COMPLETE_IN_N_DAYS)){
				int days =  (int) getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS);
				label = "Complete in " + days + " days";			
			}else{
				if(null != this.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE)){
					Date date = (Date) this.getParameterValue(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DATE);
					label = Utilities.dateToStringdd_MMM_yyyy(date);
				}
			}
			
		}		 
		return label;		
	}
	
	public Popup getCondtionPopup(){
		if(null == conditionPopup){
			conditionPopup = HierarchyFilterUtility.getPopup(this);
		}
		return conditionPopup;
	}
	
	
	public String getToolTipString(){
		return HierarchyFilterUtility.getToolTipString(this);
	}

}
