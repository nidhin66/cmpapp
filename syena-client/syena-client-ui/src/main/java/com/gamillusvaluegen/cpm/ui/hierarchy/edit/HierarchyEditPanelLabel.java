/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author hp
 */
public class HierarchyEditPanelLabel extends HBox{
    
    public HierarchyEditPanelLabel(String levelName){
        this.setSpacing(0);      
        this.setPadding(new Insets(5, 5, 5, 5));
        this.setAlignment(Pos.CENTER);
        Text text = new Text( "["+ levelName + "]");
        text.setFont(Font.font("Century Gothic", FontWeight.BOLD, FontPosture.ITALIC, 15));
        text.setFill(Paint.valueOf("#91C2C7"));
        getChildren().addAll(text);
        this.setPrefSize(150, 30);
        this.setStyle("-fx-background-color : #64545E;");
    }
}
