package com.gamillusvaluegen.cpm.ui.hierarchy.job;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.job.Job;

public class JobPane extends BorderPane implements DialogController{
	
	@FXML
	private Label titleLabel;
	
	@FXML
	private Button submitButton;
	
	
	private JobAttributePane<Job> jobAttributePane;
	
	private FXMLDialog dialog;
	
	private HierarchyItem parentItem;
	
	private Job data;
	private String mode;
	
	public JobPane(HierarchyItem parentItem,Job job, String mode){
		this.parentItem = parentItem;
		this.data = job;
		this.mode = mode;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/job/job_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		jobAttributePane = new JobAttributePane<Job>(job, NodeCategory.job, mode);
		setCenter(jobAttributePane);
		if(null != parentItem){
			
			if(null != data){
				if(data.isInPunchList()){
					titleLabel.setText("PunchList Job for " + parentItem.getName());
				}else{

					titleLabel.setText("Job");
				}
			}else{
				titleLabel.setText("PunchList Job for " + parentItem.getName());
			}
		}else{
			
				titleLabel.setText("Standard Job");
			
		}
		if("V".equalsIgnoreCase(mode)){
			submitButton.setVisible(false);
		}
	}
	
	public JobPane(HierarchyItem parentItem,Job job){
		this(parentItem, job, "E");
	}
	
	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;		
	}
	
	@FXML
	private void submitData(){
		Job job = (Job) jobAttributePane.getItem();		
		try {
			if(null == parentItem){
				if(null != data){
					HierarchyJobService.updateStandardJob(job);
				}else{
					HierarchyJobService.createStandardJob(job);
				}				
			}else{
				if(null != data){
					HierarchyJobService.updateJob(job);
				}else{					
					HierarchyNodeService.createPuchListJob(parentItem.getId(), job);
				}				
			}			
			dialog.close();
		} catch (ServiceFailedException e) {
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, e.getMessage());
			dialog.show();
		}
	}
	
	@FXML
	private void closeDialog(){
		this.dialog.close();
	}

}
