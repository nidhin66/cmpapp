/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.StageStyle;

import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;

/**
 *
 * @author hp
 */
public class PlantHierarchySearchController {
    
    @FXML
    private Button advancedSearchBtn;
    
    @FXML
    private Button closeSearchBtn;
    
    private FXMLDialog searchDialog;
    
    @FXML
    private void showAdvancedSearchOption(){
       searchDialog =   new FXMLDialog(new PlantHierarchyAdvancedSearchController(), getClass().getResource("/fxml/advanced_search.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,0,0);
       searchDialog.centerOnScreen();
       searchDialog.display();
    }
    
  
}
