/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm;

/**
 *
 * @author hp
 */
public class CPMClientConstants {
    
    public static final String MENU_LABEL_PLANT_HIERARCHY = "Plant Hierarchy";
    public static final String MENU_LABEL_STATUS_VIEW = "Status View";
    public static final String MENU_LABEL_UNAPPROVED_DATA = "View UnApproved Data";
    
    public static final String MENU_ID_PLANT_HIERARCHY = "plant_hierarchy";
    public static final String MENU_ID_STATUS_VIEW = "status_view";
    public static final String MENU_ID_UNAPPROVED_DATA = "unapproved_data";
    
    public static final int CONTEXT_ALL_GROUP = 1;
    public static final int CONTEXT_ALL_JOBS = 2;
    public static final int CONTEXT_LINKED_GROUPS = 3;
    public static final int CONTEXT_LINKED_JOBS = 4;
    
    public static final int CONTEXT_ALL = 1;
    public static final int CONTEXT_LINKED = 2;
    
    public static final int DEFAULT_COLUMN_WIDTH = 240;
    
    public static final int UNIT_PANEL_ROW_COUNT = 3;
    public static final int UNIT_PANEL_COLUMN_COUNT = 2;
    public static final int UNIT_COLUMN_WIDTH = 290;
    
    public static final int SYSTEM_PANEL_ROW_COUNT = 3;
    public static final int SYSTEM_PANEL_COLUMN_COUNT = 2;
    public static final int SYSTEM_COLUMN_WIDTH = 290;
   
    
    public static final int SUB_SYSTEM_PANEL_ROW_COUNT = 5;
    public static final int SUB_SYSTEM_PANEL_COLUMN_COUNT = 5;
    
    
    public static final int LOOP_PANEL_ROW_COUNT = 5;
    public static final int LOOP_PANEL_COLUMN_COUNT = 5;
    
    public static final int JOB_PANEL_ROW_COUNT = 5;
    public static final int JOB_PANEL_COLUMN_COUNT = 5;
    
    public static final String LABEL_JOB_NAME = "Name";
    public static final String LABEL_JOB_DESCRIPTION = "Description";
    public static final String LABEL_JOB_STATUS = "Job Status";
    public static final String LABEL_JOB_OWNER = "Owner";
    public static final String LABEL_JOB_INITIATOR = "Initiator";
    public static final String LABEL_JOB_EXECUTOR= "Contractor";
    public static final String LABEL_JOB_MILESTONE= "MileStones";
    public static final String LABEL_JOB_PLANNED_START= "Planned Start Date";
    public static final String LABEL_JOB_PLANNED_END= "Planned End Date";
    public static final String LABEL_JOB_ACTUAL_START= "Actual Start Date";
    public static final String LABEL_JOB_ACTUAL_END = "Actual End Date";
    
    
    
}
