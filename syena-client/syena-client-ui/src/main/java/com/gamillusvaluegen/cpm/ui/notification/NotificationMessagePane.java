package com.gamillusvaluegen.cpm.ui.notification;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;

import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.services.notification.Notification;

public class NotificationMessagePane extends BorderPane{
	@FXML
	private WebView webView;
	
	@FXML 
	private ToggleButton onToggle;
	
	@FXML
	private ToggleButton offToggle;
	
	private ToggleGroup toggleGroup;
	
	private Notification notification;
	
	@FXML
	private Label nameLabel;
	
	@FXML
	private Label dateLabel;
	
	@FXML
	private Label subjectLabel;
	
	
	
	public NotificationMessagePane(){
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/notification_message_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		
//			htmlEditor.setDisable(true);
//		  	htmlEditor.lookup(".top-toolbar").setManaged(false);
//		    htmlEditor.lookup(".top-toolbar").setVisible(false);
//
//		    htmlEditor.lookup(".bottom-toolbar").setManaged(false);
//		    htmlEditor.lookup(".bottom-toolbar").setVisible(false);
		    
		    toggleGroup = new ToggleGroup();
		    
		    offToggle.setToggleGroup(toggleGroup);
		    onToggle.setToggleGroup(toggleGroup);
	}
	
	public void setData(Notification notification){
		clear();
		if(null != notification){
			nameLabel.setText(notification.getName());
			subjectLabel.setText(notification.getSubject());
			dateLabel.setText(Utilities.dateToString_yyyy_MM_dd(notification.getDate()));
//			htmlEditor.setHtmlText(notification.getMessage());
			webView.getEngine().loadContent(notification.getMessage());
		}
		
	}
	
	private void clear(){
		nameLabel.setText("");
		subjectLabel.setText("");
		dateLabel.setText("");
//		htmlEditor.setHtmlText("");
		webView.getEngine().loadContent("");
	}
}
