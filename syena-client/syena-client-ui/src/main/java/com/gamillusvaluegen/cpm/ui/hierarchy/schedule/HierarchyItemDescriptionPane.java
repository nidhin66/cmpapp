/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.schedule;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

import com.gvg.syena.core.api.entity.common.HierarchyItem;

/**
 *
 * @author hp
 */
public class HierarchyItemDescriptionPane extends BorderPane {
    
    @FXML
    private Label itemNameLabel;
    
    @FXML
    private TextArea itemDescTextArea;
    


  
    public HierarchyItemDescriptionPane(HierarchyItem item) {     
        loadUI();
        itemNameLabel.setText(item.getName());
        itemDescTextArea.setWrapText(true);
        itemDescTextArea.setText(item.getDescription());
    }
    
    private void loadUI(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/hierarchy_item_description.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

}
