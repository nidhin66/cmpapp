package com.gamillusvaluegen.cpm.ui.hierarchy.search;

import java.io.IOException;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilter;
import com.gamillusvaluegen.cpm.ui.hierarchy.filter.HierarchyFilterUtility;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.util.Message;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Popup;

public class FilterInputWindow extends BorderPane {
	
	private Popup popUp;
	private Popup parentPopUp;
	private HierarchyFilter filter;
	
	@FXML
	private TextField daysInputField;
	
	@FXML
	private Label headerLabel;
	
	public FilterInputWindow(HierarchyFilter filter,Popup popUp,Popup parentPopUp){
		this.popUp = popUp;
		this.filter = filter;
		this.parentPopUp = parentPopUp;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/filter_input.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		headerLabel.setText(filter.getToolTipString());
		addTextLimiter(daysInputField, 2);
		popUp.getContent().add(this);
	}
	
	@FXML
	private void onSubmit(){
		try {
			int days = 0;
			if(!Utilities.isNullOrEmpty(daysInputField.getText())){
				days = Integer.valueOf(daysInputField.getText());
			}			
			filter.addParameter(HierarchyFilterUtility.SEARCH_ATTRIBUTE_DAYS, days);
			PageItr<? extends HierarchyItem> jobs = HierarchyFilterUtility.getPage(NodeCategory.jobgroup, 0,
					CPMClientConstants.JOB_PANEL_COLUMN_COUNT * CPMClientConstants.JOB_PANEL_ROW_COUNT, filter);
			HierarchyFilterUtility.loadJobPanel(CPMClientConstants.CONTEXT_LINKED_GROUPS, filter, jobs);
			this.popUp.hide();
			if(null != parentPopUp){
				parentPopUp.hide();
			}
		}catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		
	}
	
	@FXML
	private void onCancel(){
		this.popUp.hide();
	}
	
	public static void addTextLimiter(final TextField tf, final int maxLength) {
	    tf.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
	            if (tf.getText().length() > maxLength) {
	                String s = tf.getText().substring(0, maxLength);
	                tf.setText(s);
	            }
	        }
	    });
	}
}
