package com.gamillusvaluegen.cpm.ui.notification;

import javafx.scene.layout.BorderPane;

import com.gvg.syena.core.api.services.notification.Notification;
import com.gvg.syena.core.api.services.notification.NotificationType;

public class NotificationDataPane extends BorderPane{
	
	private NotificationListPane listPane;
	
	private NotificationMessagePane messagePane;
	
	
	public NotificationDataPane(){		
		listPane = new NotificationListPane(this);
		messagePane = new NotificationMessagePane();
		this.setLeft(listPane);
		this.setRight(messagePane);
		setPrefHeight(450);
		
	}
	
	public void setMessagePaneData(Notification notification){
		messagePane.setData(notification);
	}
	
	public void loadData(NotificationType type){
		setMessagePaneData(null);
		listPane.setNotificationType(type);
		listPane.loadPage(0);
	}

}
