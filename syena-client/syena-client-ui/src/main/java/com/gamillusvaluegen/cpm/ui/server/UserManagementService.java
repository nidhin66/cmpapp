package com.gamillusvaluegen.cpm.ui.server;



import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.OrganizationManagemnetServiceNames;

public class UserManagementService {
	
	  public static final String url = ScreensConfiguration.getUrl();


	    private static RestCaller createRestCaller() {
	        RestCaller userManagementController = new RestCaller(url);
	        userManagementController.path("userManagement");
	        return userManagementController;
	    }
	    
	    
	    public static void loginUser(String userId,  String password) throws ServiceFailedException   {
	    	System.out.println("URL : " + url);
	    	 RestCaller userManagementController = createRestCaller();
	    	 userManagementController.path(OrganizationManagemnetServiceNames.login);	    	
	    	 userManagementController.param("userId", userId);
	    	 try {
				userManagementController.post(password, String.class);
			} catch (RestCallException e) {
				Message erroMessage = new Message(ExceptionMessages.USER_MANAGEMENT_SERVICE_001, e.getMessage() );
	            throw new ServiceFailedException(erroMessage);
			} 
		}
	    
	    public static Person getPerson(String userId) throws ServiceFailedException {
	    	 RestCaller userManagementController = createRestCaller();
	    	 userManagementController.path(OrganizationManagemnetServiceNames.getPerson);
	    	 userManagementController.param("userId", userId);	    	
	    	 Person person = null;
	    	 try {
	    		 person = userManagementController.post(Person.class);
			} catch (RestCallException e) {
				Message erroMessage = new Message(ExceptionMessages.USER_MANAGEMENT_SERVICE_002, e.getMessage() );
	            throw new ServiceFailedException(erroMessage);
			} 
	    	 return person;
		}
	    
	    public static PageItr<Person> searchPerson( String personName, int pageNumber,int pageSize) throws ServiceFailedException {
	    	 RestCaller userManagementController = createRestCaller();
	    	 userManagementController.path(OrganizationManagemnetServiceNames.searchPerson);
	    	 userManagementController.param("personName", personName).param("pageNumber", pageNumber).param("pageSize", pageSize);	  
	    	 PageItr<Person> persons = null;
	    	 try {
	    		 TypeReference<PageItr<Person>> type = new TypeReference<PageItr<Person>>() {
	             };
	    		 persons = userManagementController.get(type);
			} catch (RestCallException e) {
				Message erroMessage = new Message(ExceptionMessages.USER_MANAGEMENT_SERVICE_002, e.getMessage() );
	            throw new ServiceFailedException(erroMessage);
			}
			return persons; 
		}


}
