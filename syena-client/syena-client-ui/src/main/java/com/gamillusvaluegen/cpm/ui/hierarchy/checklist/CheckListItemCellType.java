/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import org.controlsfx.control.spreadsheet.SpreadsheetCellEditor;
import org.controlsfx.control.spreadsheet.SpreadsheetCellType;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;

/**
 *
 * @author hp
 */
public class CheckListItemCellType extends SpreadsheetCellType<CheckListItem>{

    @Override
    public SpreadsheetCellEditor createEditor(SpreadsheetView view) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString(CheckListItem object) {
       return "";
    }

    @Override
    public boolean match(Object value) {
         if(null != value){
            return value instanceof CheckListItem;
        }
        return true;
    }

    @Override
    public CheckListItem convertValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
