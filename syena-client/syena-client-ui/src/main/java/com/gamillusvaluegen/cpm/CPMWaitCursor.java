/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author hp
 */
public class CPMWaitCursor extends Stage {
    public CPMWaitCursor(){
           super(StageStyle.TRANSPARENT);         
          init();
          createScene(); 
    }
    
        
    private void init(){
//        ScreensConfiguration.getInstance().setCurrentParentWindow(this);
        initOwner(ScreensConfiguration.getInstance().getPrimaryStage());
        initModality(Modality.WINDOW_MODAL);
    }

    
     private Pane createGlassPane() {
        final Pane glassPane = new Pane();
        glassPane.setPrefSize(ScreensConfiguration.getInstance().getWidth(), ScreensConfiguration.getInstance().getHeight());
        glassPane.getStyleClass().add(
                "modal-dialog-glass"
        );
        
        return glassPane;
    }
     
        private void createScene(){
        
         StackPane layout = new StackPane();
           layout.getChildren().setAll(
                    createGlassPane()
                    );
        
           Scene scene = new Scene(layout, Color.TRANSPARENT);            
           scene.getStylesheets().add("/styles/Styles.css");     
           scene.getStylesheets().add("/styles/schedule.css");      
           setScene(scene);  
    }
 
     
}
