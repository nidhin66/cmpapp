/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import com.gvg.syena.core.api.entity.job.checklist.CheckList;

/**
 *
 * @author hp
 */
public class CheckListDataSelectionChangeListner implements ChangeListener<CheckList>{

    private final CheckListPaneController controller;
    
    public CheckListDataSelectionChangeListner(CheckListPaneController controller){
        this.controller = controller;
    }
    
    @Override
    public void changed(ObservableValue<? extends CheckList> observable, CheckList oldValue, CheckList newValue) {
        controller.loadCheckListItems(newValue);
    }
    
}
