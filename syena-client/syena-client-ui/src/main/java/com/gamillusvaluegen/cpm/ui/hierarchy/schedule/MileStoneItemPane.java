/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.schedule;

import java.io.IOException;
import java.time.LocalDate;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.DateFormatConverter;
import com.gamillusvaluegen.cpm.ui.custom.DisabledDateCell;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.job.schedule.ScheduledStage;
import com.gvg.syena.core.api.entity.job.schedule.WorkTime;

/**
 *
 * @author hp
 */
public class MileStoneItemPane extends BorderPane {

	@FXML
	private TextField mileStone;

	@FXML
	private Label actualDateLabel;

	@FXML
	private DatePicker mileStoneDatePicker;

	@FXML
	private DatePicker plannedDatePicker;

	@FXML
	private DatePicker actualDatePicker;

	@FXML
	private CheckBox mileStoneCheckBox;

	private MileStoneType mileStoneType;

	public MileStoneType getMileStoneType() {
		return mileStoneType;
	}

	private DateFormatConverter formatConverter = new DateFormatConverter();

	private ScheduledStage scheduledStage;

	private WorkTime workTime;

	public MileStoneItemPane(String description, MileStoneType type, WorkTime workTime,ScheduleMode mode) {
		loadUI(type);
		this.workTime = workTime;
		this.mileStone.setText(description);
		if (type == MileStoneType.START) {
		
			if (null != workTime && null != workTime.getPlannedStartTime()) {
				plannedDatePicker.setValue(Utilities.dateToLocalDate(workTime.getPlannedStartTime()));
			}
			if (null != workTime && null != workTime.getActualStartTime()) {
				actualDatePicker.setValue(Utilities.dateToLocalDate(workTime.getActualStartTime()));
			}
		} else if (type == MileStoneType.FINISH) {
			
			if (null != workTime && null != workTime.getPlannedFinishTime()) {
				plannedDatePicker.setValue(Utilities.dateToLocalDate(workTime.getPlannedFinishTime()));
			}
			if (null != workTime && null != workTime.getActualFinishTime()) {
				actualDatePicker.setValue(Utilities.dateToLocalDate(workTime.getActualFinishTime()));
			}
		}

		mileStoneCheckBox.setVisible(false);
		mileStoneDatePicker.setVisible(false);
		if(mode == ScheduleMode.SCHEDULING){
			plannedDatePicker.setDisable(false);
			actualDatePicker.setDisable(true);
			if(mileStoneType == MileStoneType.INTERMEDIATE){
				mileStone.setEditable(true);
			}			
		}else if (mode == ScheduleMode.STATUS_UPDATE){
			plannedDatePicker.setDisable(true);
			actualDatePicker.setDisable(false);
			if(mileStoneType == MileStoneType.INTERMEDIATE){
				mileStone.setEditable(false);
			}			
		}else{
			plannedDatePicker.setDisable(true);
			actualDatePicker.setDisable(true);
			mileStone.setEditable(false);
		}
		
		// actualDateLabel.setVisible(false);
	}

	public MileStoneItemPane(MileStoneType type, ScheduledStage scheduledStage,ScheduleMode mode) {

		loadUI(type);
		
		if (type == MileStoneType.START) {
			this.mileStone.setText("Start");
		}else if (type == MileStoneType.FINISH){
			this.mileStone.setText("Finish");
		}else{			
			if (!Utilities.isNullOrEmpty(scheduledStage.getDescription())) {
				this.mileStone.setText(scheduledStage.getDescription());
			} else {
				this.mileStone.setText(String.valueOf(scheduledStage.getPercentageOfWork()));
			}
		}
	
		this.scheduledStage = scheduledStage;
		
		if(mode == ScheduleMode.SCHEDULING){
			plannedDatePicker.setDisable(false);
			actualDatePicker.setDisable(true);
			mileStoneDatePicker.setDisable(true);
			mileStoneCheckBox.setDisable(true);
			if(mileStoneType == MileStoneType.INTERMEDIATE){
				mileStone.setEditable(true);
			}			
		}else if (mode == ScheduleMode.STATUS_UPDATE){
			plannedDatePicker.setDisable(true);
			actualDatePicker.setDisable(true);
			mileStoneDatePicker.setDisable(false);
			mileStoneCheckBox.setDisable(false);
			if(mileStoneType == MileStoneType.INTERMEDIATE){
				mileStone.setEditable(false);
			}
			
		}else{
			plannedDatePicker.setDisable(true);
			actualDatePicker.setDisable(true);
			mileStoneDatePicker.setDisable(true);
			mileStoneCheckBox.setDisable(true);
			mileStone.setEditable(false);
		}


		if (null != scheduledStage.getPlannedTime()) {
			plannedDatePicker.setValue(Utilities.dateToLocalDate(scheduledStage.getPlannedTime()));
		} else {
//			mileStoneCheckBox.setDisable(true);
//			mileStoneDatePicker.setDisable(true);
		}

		if (null != scheduledStage.getActualTime()) {
			actualDatePicker.setValue(Utilities.dateToLocalDate(scheduledStage.getActualTime()));
			mileStoneDatePicker.setValue(Utilities.dateToLocalDate(scheduledStage.getActualTime()));
			mileStoneCheckBox.setSelected(true);
		}
		
		
	}

	private void loadUI(MileStoneType type) {
		this.mileStoneType = type;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/milestone_item.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		this.setStyle("-fx-border-width:  0 0 1 0;-fx-border-color: #828A95;");

		mileStoneDatePicker.setConverter(formatConverter);
		mileStoneDatePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {			
			@Override
			public DateCell call(DatePicker param) {
				return new DisabledDateCell();
			}
		});
		
		plannedDatePicker.setConverter(formatConverter);
		actualDatePicker.setConverter(formatConverter);
		if (type == MileStoneType.START) {
			mileStone.setEditable(false);
			mileStoneDatePicker.getStyleClass().add("start-milestone-date-picker");
			mileStoneCheckBox.getStyleClass().add("start-milestone-box");
		} else if (type == MileStoneType.INTERMEDIATE) {
			mileStoneDatePicker.getStyleClass().add("intermediate-milestone-date-picker");
			mileStoneCheckBox.getStyleClass().add("intermediate-milestone-box");
		} else if (type == MileStoneType.FINISH) {
			mileStone.setEditable(false);
			mileStoneDatePicker.getStyleClass().add("finish-milestone-date-picker");
			mileStoneCheckBox.getStyleClass().add("finish-milestone-box");
		}
	}

	@FXML
	private void completeMileStone() {
		if (mileStoneCheckBox.isSelected()) {
			mileStoneDatePicker.setValue(LocalDate.now());
		}
	}

	@FXML
	private void updateActual() {
		actualDatePicker.setValue(mileStoneDatePicker.getValue());
	}

	@FXML
	private void updatePlanned() {
//		mileStoneCheckBox.setDisable(false);
//		mileStoneDatePicker.setDisable(false);
	}

	public ScheduledStage getScheduledStage() {
		// scheduledStage.
		if (null != actualDatePicker.getValue()) {
			scheduledStage.setActualTime(Utilities.LocalDateToDate(actualDatePicker.getValue()));
			// if(mileStoneType == MileStoneType.START){
			// scheduledStage.setActualTime(Utilities.LocalDateToDate(actualDatePicker.getValue()));
			// }else{
			// scheduledStage.setActualEndDate(Utilities.LocalDateToDate(actualDatePicker.getValue()));
			// }
			
			if(mileStoneType == MileStoneType.INTERMEDIATE){
				if(null == scheduledStage.getPlannedTime()){
					scheduledStage.setPlannedTime(Utilities.LocalDateToDate(actualDatePicker.getValue()));
				}
			}

		} else {
			scheduledStage.setActualTime(null);
		}
		if (null != plannedDatePicker.getValue()) {
			scheduledStage.setPlannedTime(Utilities.LocalDateToDate(plannedDatePicker.getValue()));
			// if(mileStoneType == MileStoneType.START){
			// scheduledStage.setEstimatedStartDate(Utilities.LocalDateToDate(plannedDatePicker.getValue()));
			// }else{
			// scheduledStage.setEstimatedEndDate(Utilities.LocalDateToDate(plannedDatePicker.getValue()));
			// }

		} else {
			scheduledStage.setPlannedTime(null);
		}
		scheduledStage.setDescription(mileStone.getText());
		return scheduledStage;
	}

	public WorkTime getWorkTime() {
		if (null == workTime) {
			workTime = new WorkTime();
		}
		if (mileStoneType == MileStoneType.START) {
			if (null != plannedDatePicker.getValue()) {
				workTime.setPlanedStartTime(Utilities.LocalDateToDate(plannedDatePicker.getValue()));
			} else {
				workTime.setPlanedStartTime(null);
			}
			if (null != actualDatePicker.getValue()) {
				workTime.setActualStartTime(Utilities.LocalDateToDate(actualDatePicker.getValue()));
			} else {
				workTime.setActualStartTime(null);
			}
		} else if (mileStoneType == MileStoneType.FINISH) {
			if (null != plannedDatePicker.getValue()) {
				workTime.setPlanedFinishTime(Utilities.LocalDateToDate(plannedDatePicker.getValue()));
			} else {
				workTime.setPlanedFinishTime(null);
			}
			if (null != actualDatePicker.getValue()) {
				workTime.setActualFinishTime(Utilities.LocalDateToDate(actualDatePicker.getValue()));
			} else {
				workTime.setActualFinishTime(null);
			}
		}
		return workTime;
	}

}
