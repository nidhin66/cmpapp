/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.alert.exception;

import com.gvg.syena.core.api.exception.ApplicationException;

/**
 *
 * @author hp
 */
public class ServerException extends ApplicationException{
    public ServerException(String messageKey, String messageString, Exception e) {
        super(messageKey, messageString, e);
    }
}
