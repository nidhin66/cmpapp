/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.main;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

import com.gamillusvaluegen.cpm.CPMClientConstants;
import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.PermissionClientService;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.services.ActivityArea;
import com.gvg.syena.core.api.services.ActivityOperation;

/**
 *
 * @author hp
 */
public class MainMenuBar extends MenuBar {

	private MainScreenController mainScreenController;

	public MainMenuBar(final MainScreenController mainScreenController) {
		this.mainScreenController = mainScreenController;
		Menu mainMenu = new Menu("                 ");
		mainMenu.getStyleClass().add("main-menu-button");
		this.getStyleClass().add("main-menu-bar");
		this.getMenus().add(mainMenu);
		MenuItem plantHierarchyMenu = new MenuItem(CPMClientConstants.MENU_LABEL_PLANT_HIERARCHY);
		plantHierarchyMenu.getStyleClass().add("main-menu-item");
		plantHierarchyMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String screenId = CPMClientConstants.MENU_ID_PLANT_HIERARCHY;
				if (!ClientConfig.getInstance().getCurrentScreenId().equalsIgnoreCase(screenId)) {
					Parent screen = ClientConfig.getInstance().getScreenById(screenId);
					mainScreenController.setmainDataPane(screen);
				}

			}
		});
		MenuItem statusViewMenu = new MenuItem(CPMClientConstants.MENU_LABEL_STATUS_VIEW);
		statusViewMenu.getStyleClass().add("main-menu-item");
		statusViewMenu.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String screenId = CPMClientConstants.MENU_ID_STATUS_VIEW;
				if (!ClientConfig.getInstance().getCurrentScreenId().equalsIgnoreCase(screenId)) {
					Parent screen = ClientConfig.getInstance().getScreenById(CPMClientConstants.MENU_ID_STATUS_VIEW);
					mainScreenController.setmainDataPane(screen);
				}

			}
		});

		MenuItem unapprovedData = new MenuItem(CPMClientConstants.MENU_LABEL_UNAPPROVED_DATA);
		unapprovedData.getStyleClass().add("main-menu-item");
		unapprovedData.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String screenId = CPMClientConstants.MENU_ID_UNAPPROVED_DATA;
				if (!ClientConfig.getInstance().getCurrentScreenId().equalsIgnoreCase(screenId)) {
					Parent screen = ClientConfig.getInstance()
							.getScreenById(CPMClientConstants.MENU_ID_UNAPPROVED_DATA);
					mainScreenController.setmainDataPane(screen);
				}

			}
		});

		mainMenu.getItems().addAll(plantHierarchyMenu, statusViewMenu);
		Person person = ClientConfig.getInstance().getLoginUser();
		try {
			boolean approvalPermited = PermissionClientService.checkPermission(person.getId(), "",
					ActivityArea.approval, ActivityOperation.create_approve);
			if (approvalPermited) {
				mainMenu.getItems().add(unapprovedData);
			}
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
