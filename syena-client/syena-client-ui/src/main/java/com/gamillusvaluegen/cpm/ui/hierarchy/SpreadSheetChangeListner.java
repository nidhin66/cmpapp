/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;

import org.controlsfx.control.spreadsheet.SpreadsheetCell;
import org.controlsfx.control.spreadsheet.SpreadsheetView;

/**
 *
 * @author hp
 */
public class SpreadSheetChangeListner implements ChangeListener<ObservableList<SpreadsheetCell>> {
//public class SpreadSheetChangeListner implements ChangeListener<Number> {
 
    
    private SpreadsheetView spreadSheetView;
    private HierarchyNodePanel panel;
    
    
    
    public SpreadSheetChangeListner ( HierarchyNodePanel panel,  SpreadsheetView spreadSheetView){
        this.spreadSheetView = spreadSheetView;
        this.panel = panel;
    }
    
    @Override
    public void changed(ObservableValue<? extends ObservableList<SpreadsheetCell>> observable, ObservableList<SpreadsheetCell> oldValue, ObservableList<SpreadsheetCell> newValue) {        
//        panel.getChildPanel().showData(2);
    }

//    @Override
//    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
//        System.out.println("New Value : " + newValue.toString());
//        HierarchyNode node = (HierarchyNode) this.spreadSheetView.getSelectionModel().getTableView().getFocusModel().getFocusedItem().get(0).getItem();
//        panel.getChildPanel().loadData(HierarchyNodeService.getLinkedHierarchyNodes(node.getId(), 0, 15));
//    }
//    
}
