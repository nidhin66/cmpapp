package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Popup;

import javax.imageio.ImageIO;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.services.DocumentType;
import com.gvg.syena.core.api.services.HierarchyDoc;

public class NodeDetailsPane extends BorderPane implements DialogController{
	
	private FXMLDialog dialog;
	
	@FXML
	private Button uploadButton;
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Button docButton;
	
	@FXML
	private Button drawingButton;
	
	@FXML
	private ImageView nodeImage;
	
	private HierarchyItem item;
	
	public NodeDetailsPane(HierarchyItem item){
		 this.item = item;
		 FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
				 "/fxml/node_details.fxml"));
	        fxmlLoader.setRoot(this);
	        fxmlLoader.setController(this);

	        try {
	            fxmlLoader.load();
	        } catch (IOException exception) {
	            throw new RuntimeException(exception);
	        }
	        
	        HierarchyDoc image = HierarchyNodeService.getHierarchyNodeImage(item.getId(), item.getName()+"_Image", DocumentType.Images);
	        if(null != image && null != image.getContent()){
	        	 WritableImage wr = null;
	        	 InputStream in = new ByteArrayInputStream(image.getContent());
	 			BufferedImage bImageFromConvert;
				try {
					bImageFromConvert = ImageIO.read(in);
					 wr = new WritableImage(bImageFromConvert.getWidth(), bImageFromConvert.getHeight());
		             PixelWriter pw = wr.getPixelWriter();
		             for (int x = 0; x < bImageFromConvert.getWidth(); x++) {
		                 for (int y = 0; y < bImageFromConvert.getHeight(); y++) {
		                     pw.setArgb(x, y, bImageFromConvert.getRGB(x, y));
		                 }
		             }
		             nodeImage.setImage(wr); 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		            
	        }
	        
	        docButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if(event.getButton()==MouseButton.PRIMARY )
				    {
						Popup popup = new Popup();
						popup.setAutoHide(true);
						popup.getContent().add(getDocumentList(DocumentType.Documents));
						popup.show(docButton,event.getScreenX() + 25,event.getScreenY());                   
				    }

					
				}
			});
	        
	        drawingButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if(event.getButton()==MouseButton.PRIMARY )
				    {
						Popup popup = new Popup();
						popup.setAutoHide(true);
						popup.getContent().add(getDocumentList(DocumentType.Drawings));
						popup.show(drawingButton,event.getScreenX() + 25,event.getScreenY());                   
				    }

					
				}
			});
	        
	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog =  dialog;		
	}
	
	@FXML
	public void close(){
		this.dialog.close();
	}
	
	@FXML
	public void upload(){
		 FileChooser fileChooser = new FileChooser();
		 File file = fileChooser.showOpenDialog(dialog);
		 if(null != file){
			 try {
				BufferedImage originalImage = ImageIO.read(file);
				 WritableImage wr = null;
		         if (originalImage != null) {
		             wr = new WritableImage(originalImage.getWidth(), originalImage.getHeight());
		             PixelWriter pw = wr.getPixelWriter();
		             for (int x = 0; x < originalImage.getWidth(); x++) {
		                 for (int y = 0; y < originalImage.getHeight(); y++) {
		                     pw.setArgb(x, y, originalImage.getRGB(x, y));
		                 }
		             }
		         }		  
		         nodeImage.setImage(wr);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 FileInputStream inputStream;
			
			 HierarchyDoc imageDoc = new HierarchyDoc();
			 imageDoc.setDocumentName(item.getName()+"_Image");
			 imageDoc.setContent(getImageByteArray());
			 imageDoc.setDocumentType(DocumentType.Images);
			 HierarchyNodeService.uploadHierarchyNodeImage(item.getId(), imageDoc);
			
				
		 }
//		 byte[] image = getImageByteArray();
//		 if(null != image){
//			
//		 }
		
	}
	
	private VBox getDocumentList(final DocumentType documentType){
		VBox vbBox = new VBox();
		vbBox.setStyle("-fx-background-color:#ffffff;");
		vbBox.setSpacing(10);
		Label label = new Label(documentType.name());
		vbBox.getChildren().add(label);
		List<HierarchyDoc> docs = HierarchyNodeService.getHierarchyNodeDocs(item.getId(),documentType);
		if(null != docs){
			for(final HierarchyDoc doc : docs){
				HBox hbBox = new HBox();
				hbBox.setStyle("-fx-background-color:#ffffff;");
				hbBox.setSpacing(10);
				Label docLabel  = new Label(doc.getDocumentName());
				Button duButton = new Button("Download");
				duButton.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						FileChooser fileChooser = new FileChooser();
						fileChooser.setTitle("Save " + doc.getDocumentName());
						fileChooser.setInitialFileName(doc.getDocumentName());
						File file = fileChooser.showSaveDialog(dialog);
			            if (file != null) {
//			            	File saveFile = new File(file, doc.getDocumentName());
			            	HierarchyDoc hierarchyDoc = HierarchyNodeService.getHierarchyNodeImage(item.getId(), doc.getDocumentName(), documentType);
			    			BufferedOutputStream stream;
							try {
								stream = new BufferedOutputStream(new FileOutputStream(file));
								stream.write(hierarchyDoc.getContent());
				    			stream.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			    			
			            }						
					}
				});
				hbBox.getChildren().addAll(docLabel,duButton);
				vbBox.getChildren().add(hbBox);
			}
		}
		Button button = new Button("Upload");
		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				 File file = fileChooser.showOpenDialog(dialog);
				 if(null != file){
					 HierarchyDoc doc = new HierarchyDoc();
					 doc.setDocumentName(file.getName());
					 doc.setDocumentType(documentType);
					 doc.setContent(fileToByteArray(file));
					 HierarchyNodeService.uploadHierarchyNodeDoc(item.getId(), doc);
				 }
				
			}
		});
		vbBox.getChildren().add(button);
		return vbBox;
	}
	
	private byte[] fileToByteArray(File file){
		FileInputStream fileInputStream=null;
		 byte[] bFile = new byte[(int) file.length()];
		 try {
			fileInputStream = new FileInputStream(file);
			 fileInputStream.read(bFile);
			    fileInputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bFile;
		   

	}
	
	private byte[] getImageByteArray(){
		if(null != nodeImage.getImage()){
			BufferedImage bImage = SwingFXUtils.fromFXImage(nodeImage.getImage(), null);
			ByteArrayOutputStream s = new ByteArrayOutputStream();
			byte[] res = null;
			try {
				ImageIO.write(bImage, "jpg", s);
				res  = s.toByteArray();
				s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return res;
		}else { 
			return null;
		}
		
		
	}
}
