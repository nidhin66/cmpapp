/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.StageStyle;

import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.diary.DiaryPane;
import com.gamillusvaluegen.cpm.ui.wiki.WikiPagePane;
import com.gvg.syena.core.api.entity.common.HierarchyItem;

/**
 *
 * @author hp
 */
public class MoreOptionsContextMenu extends ContextMenu{
    
    public MoreOptionsContextMenu(final HierarchyItemPanel nodePanel){
        
        MenuItem cmItem1 = new MenuItem("                                     ");  
        cmItem1.getStyleClass().add("advanced-search-menu-item");   
         cmItem1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               showAdvancedSearch();
            }
        });

        MenuItem cmItem2 = new MenuItem("                                       ");
        cmItem2.getStyleClass().add("project-diary-menu-item");
        cmItem2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FXMLDialog dialog = new FXMLDialog(new DiaryPane(),ScreensConfiguration.getInstance().getPrimaryStage());					
				dialog.show();
				
			}
		});
        
        
        MenuItem cmItem3 = new MenuItem("");
        cmItem3.getStyleClass().add("view-plant-menu-item");
        
        cmItem3.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				HierarchyItem item  = nodePanel.getSelectedItem();
				if(null != item){					
					FXMLDialog dialog = new FXMLDialog(new WikiPagePane(item),ScreensConfiguration.getInstance().getPrimaryStage());					
					dialog.show();
				}else{
					AlertDialog alertDialog = new AlertDialog(AlertDialogType.WARN, "Select any node");
					alertDialog.show();
				}
			}
		});
        
//        this.getItems().add(cmItem1);
        this.getItems().add(cmItem2);
        this.getItems().add(cmItem3);
//        this.setPrefWidth(150);
        this.getStyleClass().add("more-option-menu");
    }
    
    private void showAdvancedSearch(){
       FXMLDialog searchDialog =   new FXMLDialog(new PlantHierarchyAdvancedSearchController(), getClass().getResource("/fxml/advanced_search.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED,0,0);
       searchDialog.centerOnScreen();
       searchDialog.display();
    }
}
