package com.gamillusvaluegen.cpm.ui.diary;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Accordion;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.HierarchySearchController;
import com.gamillusvaluegen.cpm.ui.custom.PersonSearchField;
import com.gamillusvaluegen.cpm.ui.custom.SearchTextField;
import com.gamillusvaluegen.cpm.ui.server.DiaryClientService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.diary.DiaryThread;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.diary.DiaryFilterProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;

public class DiaryPane extends BorderPane implements DialogController, HierarchySearchController {

	private FXMLDialog dialog;

	@FXML
	private ToggleButton unitButton;

	@FXML
	private ToggleButton systemButton;

	@FXML
	private ToggleButton subSystemButton;

	@FXML
	private ToggleButton loopButton;

	@FXML
	private ToggleButton jobButton;

	@FXML
	private SearchTextField searchBox;

	private ToggleGroup searchBtnGroup;

	@FXML
	private Accordion threadListBox;
	
	@FXML
	private Label paginationLabel;

	private NodeCategory category;
	
	@FXML
	private PersonSearchField userField;
	
	@FXML
	private DatePicker fromDatePicker;
	
	@FXML
	private DatePicker toDatePicker;
	
	@FXML
	private ScrollPane scrollPane;
	
	private DiaryFilterProperties currentFilter;

	private PageItr<DiaryThread> currentPage;

	public DiaryPane() {
		searchBtnGroup = new ToggleGroup();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/project_diary_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		loadPage(0);
		threadListBox.expandedPaneProperty().addListener(new ChangeListener<TitledPane>() {

			public void changed(ObservableValue<? extends TitledPane> observable, TitledPane oldValue,
					TitledPane newValue) {
				if (null != newValue) {
					DiaryThreadPane diaryThreadPane = (DiaryThreadPane) newValue;
					if (null == diaryThreadPane.getContent()) {
						diaryThreadPane.loadContent();
					}
				}
			}
		});

		unitButton.setToggleGroup(searchBtnGroup);
		unitButton.setUserData("Unit");
		unitButton.setTooltip(new Tooltip("Unit"));

		systemButton.setToggleGroup(searchBtnGroup);
		systemButton.setUserData("System");
		systemButton.setTooltip(new Tooltip("System"));

		subSystemButton.setToggleGroup(searchBtnGroup);
		subSystemButton.setUserData("Sub system");
		subSystemButton.setTooltip(new Tooltip("Sub system"));

		loopButton.setToggleGroup(searchBtnGroup);
		loopButton.setUserData("Loop/Equipments");
		loopButton.setTooltip(new Tooltip("Loop/Equipments"));

		jobButton.setToggleGroup(searchBtnGroup);
		jobButton.setUserData("Job");
		jobButton.setTooltip(new Tooltip("Job"));

		searchBtnGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle) {
				if (null != new_toggle) {
					searchBox.clear();
					searchBox.setPromptText("Search " + new_toggle.getUserData());
					if ("Job".equalsIgnoreCase((String) new_toggle.getUserData())) {
						category = NodeCategory.job;
					} else {
						category = NodeCategory.hierarchy;
					}
				}
			}
		});
		searchBox.setController(this);
	}

	private void loadData(PageItr<DiaryThread> diaryThreadPage) {

		
		if(null != threadListBox.getPanes()){
			threadListBox.getPanes().clear();
		}
		if (null != diaryThreadPage) {
			if (null != diaryThreadPage.getContent()) {
				for (DiaryThread diaryThread : diaryThreadPage.getContent()) {
					DiaryThreadPane diaryThreadPane = new DiaryThreadPane(diaryThread);
					threadListBox.getPanes().add(diaryThreadPane);

				}
			}

		}

	}

	private PageItr<DiaryThread> getLatestThreads(int pageNmuber) {
		PageItr<DiaryThread> diaryThreadPage = null;
		try {
			diaryThreadPage = DiaryClientService.getLatestThreads(pageNmuber, 10);
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return diaryThreadPage;
	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;

	}

	@FXML
	private void closeDialog() {
		this.dialog.close();
	}

	private void loadPage(int pageNumber) {
		PageItr<DiaryThread> diaryThreadPage = null;
		if(null != currentFilter){
			diaryThreadPage = searchThreads(currentFilter, pageNumber);
		}else{
			diaryThreadPage = getLatestThreads(pageNumber);
		}			
		currentPage = diaryThreadPage;
		loadData(diaryThreadPage);
		String labelText = "";
		if(null != currentPage){
			if(currentPage.getTotalPages() > 0){
				labelText = (currentPage.getCurrentPage() +1) + " of " + currentPage.getTotalPages();
			}
		}
		paginationLabel.setText(labelText);
		
	}

	@FXML
	private void getFirstPage() {
		loadPage(0);
	}

	@FXML
	private void getPreviousPage() {
		if (null != currentPage) {
			if ((currentPage.getCurrentPage()) > 0) {
				int pageNumber = currentPage.getCurrentPage() - 1;
				loadPage(pageNumber);
			}
		}
	}

	@FXML
	private void getNextPage() {
		if (null != currentPage) {
			if ((currentPage.getCurrentPage() + 1) < currentPage.getTotalPages()) {
				int pageNumber = currentPage.getCurrentPage() + 1;
				loadPage(pageNumber);
			}
		}
	}

	@FXML
	private void getLastPage() {
		if (null != currentPage) {
			int pageNumber = currentPage.getTotalPages() - 1;
			loadPage(pageNumber);
		}
	}


	@FXML
	private void createThread() {
		FXMLDialog dialog = new FXMLDialog(new NewThreadPane(this),
				ScreensConfiguration.getInstance().getCurrentParentWindow());
		dialog.show();
//		onClearFilter();
	}

	@Override
	public List<? extends HierarchyItem> searchData(String text) {
		List<? extends HierarchyItem> hierarchyNodes = new ArrayList<>();
		Toggle selectedToggle = searchBtnGroup.getSelectedToggle();
		if (null != selectedToggle) {
			try {
				String level = (String) selectedToggle.getUserData();
				PageItr<? extends HierarchyItem> hierarchyNodesPage = null;
				if ("Job".equalsIgnoreCase(level)) {
					JobProperties jobSearchOption = new JobProperties();
					jobSearchOption.setStandard(false);
					jobSearchOption.setName(text + "%");
					try {
						hierarchyNodesPage = HierarchyJobService.searchJobWithProperties(jobSearchOption, 0, 25);
					} catch (ServiceFailedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					hierarchyNodesPage = HierarchyNodeService.searchHierarchyNodesInLevel(text + "%", level, 0, 25);
				}

				if (null != hierarchyNodesPage) {
					hierarchyNodes = hierarchyNodesPage.getContent();
				}
			} catch (ServiceFailedException ex) {
				ex.printStackTrace();
			}
		}
		return hierarchyNodes;
	}
	
	private PageItr<DiaryThread> searchThreads(DiaryFilterProperties filterProperties, int pageNumber){
		PageItr<DiaryThread> diaryThreadPage = null;
		try {
			diaryThreadPage = DiaryClientService.searchDiscussionThreads(filterProperties, pageNumber, 10);
		} catch (ServiceFailedException e) {
			Message errorMessage = e.getErroMessage();
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			dialog.show();
		}
		return diaryThreadPage;
	}
	
	@FXML
	private void onSearch(){
		DiaryFilterProperties filterProperties = new DiaryFilterProperties();
		HierarchyItem item =  searchBox.getHierarchyItem();
		if(null != item ){
			filterProperties.setHierarchyItemId(item.getId());
			filterProperties.setCategory(category);
		}
		if(null != userField.getPerson()){
			filterProperties.setUser(userField.getPerson().getId());
		}
		if(null != toDatePicker.getValue()){
			filterProperties.setEndDate(Utilities.LocalDateToDate(toDatePicker.getValue()) );
		}else{
			filterProperties.setEndDate(null);
		}
		if(null != fromDatePicker.getValue()){
			filterProperties.setStartDate(Utilities.LocalDateToDate(fromDatePicker.getValue()) );
		}else{
			filterProperties.setStartDate(null);
		}
		
		PageItr<DiaryThread> diaryThreadPage = searchThreads(filterProperties, 0);
		loadData(diaryThreadPage);
		currentFilter = filterProperties;
		scrollPane.setVvalue(0.0);
	}
	
	@FXML
	public void onClearFilter(){
		userField.setPerson(null);
		searchBox.clear();
		fromDatePicker.setValue(null);
		toDatePicker.setValue(null);
		currentFilter = null;
		loadPage(0);
	}

}
