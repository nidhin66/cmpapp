/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;

/**
 *
 * @author hp
 */
public class AddCheckListItemPane extends GridPane {

    private int minHieght = 20;

    private Button addButton;

    private TextField checkListText;

    private Map<Integer, TextField> checkLitItemsTexts = new HashMap<>();

    private Pane parentPane;

    public AddCheckListItemPane(Pane parentPane) {
        this.parentPane = parentPane;
        this.setHgap(25);
        this.setVgap(10);
        this.setPadding(new Insets(10, 5, 0, 10));
        this.setHeight(25);
        checkListText = new TextField();
        checkListText.setPrefWidth(125);
        this.add(checkListText, 0, 0,2,1);

        addButton = new Button("");
        addButton.setPrefWidth(30);
        addButton.setPrefHeight(30);
        addButton.getStyleClass().add("add-checklist-item-button");
        addRow();

        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                addRow();

            }
        });
//        this.setPrefWidth(300);
    }

    public void addRow() {

        int numRows = this.getRowConstraints().size();
        TextField checklistItemTxt = new TextField();
        checklistItemTxt.setPrefWidth(125);
        this.add(checklistItemTxt, 2, numRows);
        checkLitItemsTexts.put(numRows, checklistItemTxt);
        this.getChildren().remove(addButton);
        this.add(addButton, 3, numRows);
        RowConstraints rc = new RowConstraints();
        rc.setVgrow(Priority.ALWAYS);
        this.getRowConstraints().add(rc);
        this.setHeight(this.getHeight() + 25);
        this.parentPane.setPrefHeight(parentPane.getPrefHeight() + 25);
//           this.resize(200, this.getPrefHeight() + 25);

    }

    public CheckList getCheckList() {
        CheckList checkList = new CheckList(checkListText.getText(), true);

        for (Iterator<Integer> it = checkLitItemsTexts.keySet().iterator(); it.hasNext();) {
            int key = it.next();
            TextField checkListItemTxt = checkLitItemsTexts.get(key);
            if (!Utilities.isNullOrEmpty(checkListItemTxt.getText())) {
                CheckListItem checkListItem = new CheckListItem(checkListItemTxt.getText());
                checkList.addCheckListItem(checkListItem);
            }

        }

        return checkList;
    }

}
