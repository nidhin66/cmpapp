package com.gamillusvaluegen.cpm.ui.hierarchy.job;

import java.io.IOException;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.HierarchySearchController;
import com.gamillusvaluegen.cpm.ui.custom.SearchTextField;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.serach.JobProperties;

public class JobSearchPane extends BorderPane implements HierarchySearchController{
	
	@FXML
	private SearchTextField searchBox;
	
	private static final int pageSize = 20;
	
	@FXML
	private ListView<Job> jobList;
	
	@FXML
	private Label paginationLabel;
	
	private PageItr<Job> currentPage;

	public JobSearchPane(){
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/job/job_search_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		searchBox.setController(this);
		
		jobList.setCellFactory(new Callback<ListView<Job>, ListCell<Job>>() {
			@Override
			public ListCell<Job> call(ListView<Job> param) {
				ListCell<Job> cell = new ListCell<Job>() {
					 @Override
	                    protected void updateItem(Job item, boolean empty) {
						 super.updateItem(item, empty);
							if (null != getListView().getItems() && getListView().getItems().size() > 0) {
								if (getIndex() >= 0 && getIndex() < getListView().getItems().size()) {
									Job job = getListView().getItems().get(getIndex());
									if (null != job) {
										if (Utilities.isNullOrEmpty(job.getDescription())) {
											setText(job.getName());
										} else {
											setText(job.getDescription());
										}
									} else {

										setText(null);
										setGraphic(null);
									}
								} else {

									setText(null);
									setGraphic(null);
								}
							} else {

								setText(null);
								setGraphic(null);
							}

					 }
				  };
	                return cell;
	            }
	        });
		getFirstPage();
	}
	
	@FXML
	private void getFirstPage(){
		getPage(0);
	}
	@FXML
	private void getPreviousPage(){
		if (null != currentPage) {
			if ((currentPage.getCurrentPage()) > 0) {
				int pageNumber = currentPage.getCurrentPage() - 1;
				getPage(pageNumber);
			}
		}
	}
	@FXML
	private void getNextPage(){
		if (null != currentPage) {
			if ((currentPage.getCurrentPage() + 1) < currentPage.getTotalPages()) {
				int pageNumber = currentPage.getCurrentPage() + 1;
				getPage(pageNumber);
			}
		}
	}
	@FXML
	private void getLastPage(){
		if (null != currentPage) {
			int pageNumber = currentPage.getTotalPages() - 1;
			getPage(pageNumber);
		}
	}
	
	@FXML
	private void searchAction(){
		
	}
	
	private void getPage(int pageNumber){
		PageItr<Job> pageJobs = getAllJobs(pageNumber);
		loadJobList(pageJobs);
	}
	
	private void loadJobList(PageItr<Job> pageJobs){
		if(null != jobList.getItems()){
			jobList.getItems().clear();
		}
		if(null != pageJobs){
			ObservableList<Job> items = FXCollections.observableArrayList(pageJobs.getContent());
			jobList.setItems(items);
		}
		currentPage = pageJobs;
		String labelText = "";
		if(null != currentPage){
			labelText = (currentPage.getCurrentPage() +1) + " of " + currentPage.getTotalPages();
		}
		paginationLabel.setText(labelText);
	}

	@Override
	public List<? extends HierarchyItem> searchData(String text) {
		String searchText = text + "%";
		List<? extends HierarchyItem> jobs = null;
		PageItr<Job> pageHierarchyNodes = null;
		JobProperties jobSearchOption = new JobProperties();
		jobSearchOption.setName(searchText);
		jobSearchOption.setStandard(true);
		try {
			pageHierarchyNodes = HierarchyJobService.searchJobWithProperties(jobSearchOption, 0, pageSize);
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(null != pageHierarchyNodes){
			jobs = pageHierarchyNodes.getContent();
		}
		loadJobList(pageHierarchyNodes); 
		return jobs;
	}
	
	private PageItr<Job> getAllJobs(int pageNumber) {
		PageItr<Job> pageJobs = null;
		try {
			
			pageJobs = HierarchyJobService.getAllStandardJobs(pageNumber, pageSize);
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		return pageJobs;
	}
	
	public List<Job> getSelectedJobs(){
		return jobList.getSelectionModel().getSelectedItems();
	}
	
}
