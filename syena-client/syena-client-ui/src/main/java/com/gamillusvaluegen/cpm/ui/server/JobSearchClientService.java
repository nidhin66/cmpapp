/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.server;

import java.util.Date;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.exception.ExceptionMessages;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.util.RestCallException;
import com.gamillusvaluegen.cpm.ui.util.RestCaller;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyNode;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.names.SearchServiceNames;
import com.gvg.syena.core.api.services.serach.JobProperties;
import com.gvg.syena.core.api.services.serach.ValueComparitor;

/**
 *
 * @author hp
 */
public class JobSearchClientService {

    private static final String url = ScreensConfiguration.getUrl();

    private static RestCaller createRestCallser() {
        RestCaller searchClient = new RestCaller(url);
        searchClient.path("searchJob");
        return searchClient;
    }

    public static PageItr<Job> onGoingJobs(Date date, String jobName, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.onGoingJobs);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_001, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<Job> completedJobs(Date date, String jobName, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.completedJobs);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_002, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<Job> plannedJobs(Date date, String jobName, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.plannedJobs);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_003, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<Job> delayedJobs(Date date, String jobName, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.delayedJobs);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("date", Utilities.dateToString_yyyy_MM_dd(date)).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
              Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_004, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<Job> jobsInitiatedBy(String personId, String jobName, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.jobsInitiatedBy);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("personId", personId).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
              Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_005, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<Job> jobsOwnedBy(String personId, String jobName, int pageNumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.jobsOwnedBy);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("personId", personId).param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_006, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }

    public static PageItr<Job> searchJobs(JobProperties jobSearchOption, int pageNaumber, int pageSize) throws ServiceFailedException {
        RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.searchJobs);
        
        searchClient.param("pageNaumber", pageNaumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.post(jobSearchOption, type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
    }
    
  
	public static PageItr<Job> jobsToBeScheduled( String jobName,int pageNumber, int pageSize) throws ServiceFailedException {
    	  RestCaller searchClient = createRestCallser();
          searchClient.path(SearchServiceNames.jobsToBeScheduled);
          if (!Utilities.isNullOrEmpty(jobName)) {
  			searchClient.param("jobName", jobName);
  		  }
          searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
          PageItr<Job> jobs = null;
          try {
              TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
              };
              jobs = searchClient.get(type);
          } catch (RestCallException ex) {
              Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
              throw new ServiceFailedException(erroMessage);
          }
          return jobs;
	}

	
	public static PageItr<Job> jobsWithRevisedDates( String jobName,int pageNumber,int pageSize) throws ServiceFailedException {
		 RestCaller searchClient = createRestCallser();
         searchClient.path(SearchServiceNames.jobsWithRevisedDates);
         if (!Utilities.isNullOrEmpty(jobName)) {
 			searchClient.param("jobName", jobName);
 		}
         searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
         PageItr<Job> jobs = null;
         try {
             TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
             };
             jobs = searchClient.get(type);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
         return jobs;
	}

	
	public static PageItr<HierarchyNode> nodeWithDelayed( String jobName,String nodeLevel, Date when, int delayedPercentage, ValueComparitor valueComparitor,
			 int pageNumber, int pageSize) throws  ServiceFailedException {
		 RestCaller searchClient = createRestCallser();
         searchClient.path(SearchServiceNames.nodeWithDelayed);
         if (!Utilities.isNullOrEmpty(jobName)) {
 			searchClient.param("jobName", jobName);
 		}
         searchClient.param("nodeLevel", nodeLevel).param("when", Utilities.dateToString_yyyy_MM_dd(when)).param("delayedPercentage", delayedPercentage).param("valueComparitor", valueComparitor);
         searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
         PageItr<HierarchyNode> hierarchyNodes = null;
         try {
             TypeReference<PageItr<HierarchyNode>> type = new TypeReference<PageItr<HierarchyNode>>() {
             };
             hierarchyNodes = searchClient.get(type);
         } catch (RestCallException ex) {
             Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
             throw new ServiceFailedException(erroMessage);
         }
         return hierarchyNodes;
	}
	

	public static PageItr<JobGroup<Job>> JobsInDelyedStartJobGroups( String jobName,long hierarchyNodeId,ValueComparitor valueComparitor,
			 float percentage,  int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.JobsInDelyedStartJobGroups);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("hierarchyNodeId", hierarchyNodeId).param("percentage", percentage).param("valueComparitor", valueComparitor);
        searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<JobGroup<Job>> jobGroups = null;
        try {
            TypeReference<PageItr<JobGroup<Job>>> type = new TypeReference<PageItr<JobGroup<Job>>>() {
            };
            jobGroups = searchClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobGroups;
	}
	
	
	public static PageItr<JobGroup<Job>> JobsInIncreasedDurationJobGroups( String jobName, long hierarchyNodeId,ValueComparitor valueComparitor,float percentage,int pageNumber,int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.JobsInIncreasedDurationJobGroups);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("hierarchyNodeId", hierarchyNodeId).param("percentage", percentage).param("valueComparitor", valueComparitor);
        searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<JobGroup<Job>> jobGroups = null;
        try {
            TypeReference<PageItr<JobGroup<Job>>> type = new TypeReference<PageItr<JobGroup<Job>>>() {
            };
            jobGroups = searchClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobGroups;
	}
	
	public static  PageItr<Job> jobsInCriticalPath( String jobName,long hierarchyNodeId, int pageNumber, int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
        searchClient.path(SearchServiceNames.jobsInCriticalPath);
        if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
        searchClient.param("hierarchyNodeId", hierarchyNodeId);
        searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
        PageItr<Job> jobs = null;
        try {
            TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
            };
            jobs = searchClient.get(type);
        } catch (RestCallException ex) {
            Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage() );
            throw new ServiceFailedException(erroMessage);
        }
        return jobs;
	}
	
	public static PageItr<Job> jobsStartInNDays(String jobName, Date fromDate,Date toDate,int pageNumber,
			int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsStartInNDays);
		if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobName", jobName);
		}
		searchClient.param("fromDate", Utilities.dateToString_yyyy_MM_dd(fromDate)).param("toDate", Utilities.dateToString_yyyy_MM_dd(toDate));
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<Job> jobs = null;
		try {
			TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}
	
	public static PageItr<Job> jobsCompleteInNDays(String jobName, Date fromDate,Date toDate,int pageNumber,
			int pageSize) throws ServiceFailedException {
		RestCaller searchClient = createRestCallser();
		searchClient.path(SearchServiceNames.jobsCompleteInNDays);
		if (!Utilities.isNullOrEmpty(jobName)) {
			searchClient.param("jobGroupName", jobName);
		}
		searchClient.param("fromDate", Utilities.dateToString_yyyy_MM_dd(fromDate)).param("toDate", Utilities.dateToString_yyyy_MM_dd(toDate));
		searchClient.param("pageNumber", pageNumber).param("pageSize", pageSize);
		PageItr<Job> jobs = null;
		try {
			TypeReference<PageItr<Job>> type = new TypeReference<PageItr<Job>>() {
			};
			jobs = searchClient.get(type);
		} catch (RestCallException ex) {
			Message erroMessage = new Message(ExceptionMessages.SEARCH_SERVICE_007, ex.getMessage());
			throw new ServiceFailedException(erroMessage);
		}
		return jobs;
	}


}
