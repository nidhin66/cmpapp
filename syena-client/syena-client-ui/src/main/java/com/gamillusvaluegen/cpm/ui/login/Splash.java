package com.gamillusvaluegen.cpm.ui.login;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class Splash extends VBox implements DialogController {
	
	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;
	}
	
	private FXMLDialog dialog;

	private ProgressBar loadProgress;
	private Label progressText;
	private static final int SPLASH_WIDTH = 700;
	private static final int SPLASH_HEIGHT = 227;
	
	public void Splash(){
		ImageView splash = new ImageView(new Image("images/SplashScreen.png"));
		loadProgress = new ProgressBar();
		loadProgress.setPrefWidth(SPLASH_WIDTH + 20);
		progressText = new Label("Loading plant hierachy and project details . . .");
		
		this.getChildren().addAll(splash, loadProgress, progressText);
		progressText.setAlignment(Pos.CENTER);
		this.setStyle("-fx-padding: 5; -fx-background-color: transparent;");
		this.setEffect(new DropShadow());
	}
}
