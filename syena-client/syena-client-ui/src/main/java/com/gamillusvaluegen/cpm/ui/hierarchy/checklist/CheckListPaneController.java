/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.checklist;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.custom.CheckListSearchField;
import com.gamillusvaluegen.cpm.ui.server.CheckListClientService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.entity.job.checklist.CheckList;
import com.gvg.syena.core.api.entity.job.checklist.CheckListItem;
import com.gvg.syena.core.api.entity.job.checklist.CheckListType;
import com.gvg.syena.core.api.entity.organization.Person;
import com.gvg.syena.core.api.entity.util.Message;

/**
 *
 * @author hp
 */
public class CheckListPaneController {

    private CheckListPane parentPane;

    @FXML
    private ListView<CheckList> checkListData;

//    @FXML
//    private BorderPane checkListItemPane;
    @FXML
    private TableView checkListItemDetails;

    @FXML
    private Button addCheckListBtn;

    @FXML
    private Button searchBtn;

    @FXML
    private Button viewCheckListBtn;

    @FXML
    private CheckListSearchField searchTextField;

    @FXML
    private TableColumn<CheckListItem, Date> dateColumn;

    @FXML
    private TableColumn<CheckListItem, String> itemNameColumn;

    @FXML
    private TableColumn<CheckListItem, Person> userColumn;

    @FXML
    private TableColumn<CheckListItem, Boolean> statusColumn;
    
    @FXML
    private Label paginationLabel;

    CheckListContextMenu contextMenu;
    CheckListViewMenu checkListViewMenu;
       
   

    @FXML
    private void initialize() {
        searchTextField.setController(this);
        
        checkListData.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        
        
        CheckListDataSelectionChangeListner checkListDataSelectionChangeListner = new CheckListDataSelectionChangeListner(this);
        checkListData.getSelectionModel().selectedItemProperty().addListener(checkListDataSelectionChangeListner);

        checkListData.setCellFactory(new Callback<ListView<CheckList>, ListCell<CheckList>>() {
            @Override
            public ListCell<CheckList> call(ListView<CheckList> param) {
                ListCell<CheckList> cell = new ListCell<CheckList>() {
                    @Override
                    protected void updateItem(CheckList t, boolean bln) {
                        super.updateItem(t, bln);
                        if (bln) {
                            setText(null);
                            setGraphic(null);
                        }
                        if (t != null) {
                            setText(t.getName());
                            if(null != t.getCheckListType()){                   
                            	Button button = new Button(); 
                            	button.setPrefWidth(20);
//                            	button.setDisable(true);
                            	CheckListType checkListType = t.getCheckListType();
                            	if(checkListType == CheckListType.completionCheckList){
                            		button.getStyleClass().add("completion-checklist-icon");
                            	}else if(checkListType == CheckListType.initiationCheckList){
                            		button.getStyleClass().add("initiation-checklist-icon");
                            	}else if(checkListType == CheckListType.infoCheckList){
                            		button.getStyleClass().add("info-checklist-icon");
                            	}                           	
                            	setGraphic(button);
                            }
                        }
                    }

                };
                return cell;
            }
        });

        dateColumn.setCellValueFactory(new PropertyValueFactory<CheckListItem, Date>("completedDate"));
        dateColumn.setCellFactory(new Callback<TableColumn<CheckListItem, Date>, TableCell<CheckListItem, Date>>() {
            @Override
            public TableCell<CheckListItem, Date> call(TableColumn<CheckListItem, Date> param) {
                TableCell<CheckListItem, Date> cell = new TableCell<CheckListItem, Date>() {
                    @Override
                    public void updateItem(final Date item, boolean empty) {
                        super.updateItem(item, empty);
                        if (null != item) {
//                            setText(Utilities.dateToString_yyyy_MM_dd(item));
                            LocalDate localDate = Utilities.dateToLocalDate(item);
                            final DatePicker datePicker = new DatePicker(localDate);
                            datePicker.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    final CheckListItem checkListItem = getTableView().getItems().get(getIndex());
                                    CheckList selectedCheckList = checkListData.getSelectionModel().getSelectedItem();
                                    try {
                                    	Person person = ClientConfig.getInstance().getLoginUser();
                                        CheckListClientService.updateCheckList(selectedCheckList.getId(), checkListItem.getId(), checkListItem.isCompleted(), Utilities.LocalDateToDate(datePicker.getValue()), person.getId());
                                        checkListItem.setCompletedDate(Utilities.LocalDateToDate(datePicker.getValue()));
                                        AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "CheckList item updated successfully.");
                                        alertDialog.show();
                                    } catch (ServiceFailedException ex) {
                                    	if(null != checkListItem.getCompletedDate()){
                                    		LocalDate localDate = Utilities.dateToLocalDate(checkListItem.getCompletedDate());
    										datePicker.setValue(localDate);    
                                    	}else{
                                    		  setText(null);
                                              setGraphic(null);
                                    	}                                    	                                	
                                        Message errorMessage = ex.getErroMessage();
                                        AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
                                        alertDialog.show();
                                    }

                                }
                            });
                            this.setGraphic(datePicker);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };
//                cell.setGraphic(new DatePicker());
                cell.getStyleClass().add("checklist-item-details-cell");
                return cell;
            }
        });

        itemNameColumn.setCellValueFactory(new PropertyValueFactory<CheckListItem, String>("name"));
        itemNameColumn.setCellFactory(new Callback<TableColumn<CheckListItem, String>, TableCell<CheckListItem, String>>() {
            @Override
            public TableCell<CheckListItem, String> call(TableColumn<CheckListItem, String> param) {
                TableCell<CheckListItem, String> cell = new TableCell<CheckListItem, String>() {
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);
//                        if (item == null) {
//                            return;
//                        }
                        if (Utilities.isNullOrEmpty(item)) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item);
                        }

                    }
                };
                cell.getStyleClass().add("checklist-item-details-cell");
                return cell;
            }
        });

        userColumn.setCellValueFactory(new PropertyValueFactory<CheckListItem, Person>("completedPerson"));
        userColumn.setCellFactory(new Callback<TableColumn<CheckListItem, Person>, TableCell<CheckListItem, Person>>() {
            @Override
            public TableCell<CheckListItem, Person> call(TableColumn<CheckListItem, Person> param) {
                TableCell<CheckListItem, Person> cell = new TableCell<CheckListItem, Person>() {
                    @Override
                    public void updateItem(final Person item, boolean empty) {
                        super.updateItem(item, empty);
                        if (null == item) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getId());

                        }
//                        if (item == null) {
//                            return;
//                        }

                    }
                };
                cell.getStyleClass().add("checklist-item-details-cell");
                return cell;
            }
        });

        statusColumn.setCellValueFactory(new PropertyValueFactory<CheckListItem, Boolean>("completed"));
        statusColumn.setCellFactory(new Callback<TableColumn<CheckListItem, Boolean>, TableCell<CheckListItem, Boolean>>() {
            @Override
            public TableCell<CheckListItem, Boolean> call(TableColumn<CheckListItem, Boolean> param) {
                TableCell<CheckListItem, Boolean> cell = new TableCell<CheckListItem, Boolean>() {
                    @Override
                    public void updateItem(final Boolean item, boolean empty) {
                        super.updateItem(item, empty);
                        this.getStyleClass().add("checklist-item-details-cell");
//                        if (item == null) {
//                            return;
//                        }
                        if (!isEmpty()) {
                            final CheckListItem checkListItem = getTableView().getItems().get(getIndex());
                            if (null != checkListItem) {
                                CheckBox checkBox = new CheckBox();
                                checkBox.getStyleClass().add("checklist-item-checkbox");
                                checkBox.setSelected(checkListItem.isCompleted());
                                checkBox.setTranslateX(30);
                                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                                    public void changed(ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) {
                                        CheckList selectedCheckList = checkListData.getSelectionModel().getSelectedItem();
                                        try {
                                        	Person person = ClientConfig.getInstance().getLoginUser();
                                            CheckListClientService.updateCheckList(selectedCheckList.getId(), checkListItem.getId(), new_val, Utilities.getDate(), person.getId());
                                            checkListItem.setCompleted(true);
                                            checkListItem.setCompletedDate(Utilities.getDate());                                            
                                            checkListItem.setCompletedPerson(person);
                                            loadCheckListItems(selectedCheckList);
                                            AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "CheckList item updated successfully.");
                                            alertDialog.show();
                                        } catch (ServiceFailedException ex) {
                                            Message errorMessage = ex.getErroMessage();
                                            AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
                                            alertDialog.show();
                                        }

//                                  checkListItem.setCompletedPerson(null);
                                    }
                                });//                                checkBox.setDisable(true);
                                setGraphic(checkBox);
                            } else {
                                setGraphic(null);
                            }

                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                };

                return cell;
            }
        });

        checkListData.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        if (e.getButton() == MouseButton.SECONDARY) {
                            if (contextMenu.isShowing()) {
                                contextMenu.hide();
                            }
                            contextMenu.show(checkListData, e.getScreenX(), e.getScreenY());
                        }
                    }
                });

        viewCheckListBtn.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        if (e.getButton() == MouseButton.PRIMARY) {
                            if (checkListViewMenu.isShowing()) {
                                checkListViewMenu.hide();
                            }
                            checkListViewMenu.show(viewCheckListBtn, e.getScreenX(), e.getScreenY());
                        }

                    }
                });

    }

    public void loadCheckListData(List<CheckList> checkLists) {
        if (null != checkListItemDetails.getItems()) {
            checkListItemDetails.getItems().clear();
        }
        if (null != checkListData.getItems()) {
            checkListData.getItems().clear();
        }
        if (null != checkLists) {
            ObservableList<CheckList> items = FXCollections.observableArrayList(checkLists);
            checkListData.setItems(items);
            if (items.size() > 0) {
                checkListData.getSelectionModel().select(0);
            }
        }

    }

    public void setParentPane(CheckListPane parentPane) {
        this.parentPane = parentPane;
        contextMenu = new CheckListContextMenu(parentPane);
        checkListViewMenu = new CheckListViewMenu(parentPane);
    }

    public void loadCheckListItems(CheckList checkList) {
        if (null != checkListItemDetails.getItems()) {
            checkListItemDetails.getItems().clear();
        }
        if (null != checkList) {
            ObservableList<CheckListItem> items = FXCollections.observableArrayList(checkList.getCheckListItems());
            checkListItemDetails.setItems(items);
        } else {
            checkListItemDetails.setItems(null);
        }

    }

    public List<CheckList> getSelectedCheckLists() {
        ObservableList<CheckList> items = checkListData.getSelectionModel().getSelectedItems();
        List<CheckList> checkLists = new ArrayList<>();
        for (CheckList checkList : items) {
            checkLists.add(checkList);
        }
        return checkLists;
    }

    @FXML
    public void addCheckListAction() {
        Bounds bound = addCheckListBtn.getLayoutBounds();
        double x = bound.getMinX();
        double y = bound.getMinY();
        FXMLDialog addCheckListDialog = new FXMLDialog(new AddCheckListController(parentPane), getClass().getResource("/fxml/add_checklist_pane.fxml"), ScreensConfiguration.getInstance().getPrimaryStage(), StageStyle.UNDECORATED, x, y);
        addCheckListDialog.display();
    }

    @FXML
    private void searchAction() {
        String searchText = searchTextField.getText();
        searchData(searchText);
    }

    public List<CheckList> searchData(String result) {
        List<CheckList> resultList = new ArrayList<>();
        if(!Utilities.isNullOrEmpty(result)){
        	resultList = this.parentPane.searchData(result);
        }else{
        	getFirstPage();
        }
        
//        ObservableList<CheckList> items = checkListData.getItems();
//        if (null != items) {
//            for (CheckList checkList : items) {
//                if (checkList.getName().toUpperCase().contains(result.toUpperCase())) {
//                    resultList.add(checkList);
//                }
//            }
//        }
//        loadCheckListData(resultList);
        return resultList;
    }
    
    public void disableCheckListItemDetails(boolean disbale){
    	checkListItemDetails.setDisable(disbale);
    }
    
    @FXML
    private void getFirstPage(){
    	parentPane.loadFirstPage();
    }
    
    
    @FXML
    private void getPreviousPage(){
    	parentPane.loadPreviousPage();
    }
    
    @FXML
    private void getNextPage(){
    	parentPane.loadNextPage();
    }
    
    @FXML
    private void getLastPage(){
    	parentPane.loadLastPage();
    }

	public void setPaginationLabel(String labelString) {
		this.paginationLabel.setText(labelString);
	}
   

}
