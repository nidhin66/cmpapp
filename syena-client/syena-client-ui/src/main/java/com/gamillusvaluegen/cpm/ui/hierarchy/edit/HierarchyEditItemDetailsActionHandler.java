/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.edit;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.StageStyle;

import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ScreensConfiguration;

/**
 *
 * @author hp
 */
public class HierarchyEditItemDetailsActionHandler implements EventHandler<ActionEvent>{
    
    private HierarchyEditItemField field;
    
    public HierarchyEditItemDetailsActionHandler(HierarchyEditItemField field){
        this.field = field;
    }

    @Override
    public void handle(ActionEvent event) {
           FXMLDialog addNodeItemDialog = new FXMLDialog(new HierarchyNodeItemController(field), getClass().getResource("/fxml/node_item.fxml"), ScreensConfiguration.getInstance().getCurrentParentWindow(), StageStyle.UNDECORATED,0,0);
           addNodeItemDialog.display();
    }
    
}
