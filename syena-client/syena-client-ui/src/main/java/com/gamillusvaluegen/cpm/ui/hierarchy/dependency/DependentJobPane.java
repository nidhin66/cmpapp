/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy.dependency;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

import com.gamillusvaluegen.cpm.ClientConfig;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.hierarchy.HierarchyItemPanel;
import com.gamillusvaluegen.cpm.ui.hierarchy.job.HierarchyJobPanel;
import com.gamillusvaluegen.cpm.ui.server.DependencyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.PageItr;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.util.Message;
import com.gvg.syena.core.api.services.serach.JobGroupProperties;
import com.gvg.syena.core.api.services.serach.JobProperties;

/**
 *
 * @author hp
 */
public class DependentJobPane extends DependentItemPane {

	@FXML
	private HBox toggleOptionBox;

	private static final int CONTEXT_JOB_GROUPS = 1;
	private static final int CONTEXT_JOBS = 2;
	private static final int CONTEXT_LINKED_JOB_GROUPS = 3;
	private static final int CONTEXT_LINKED_JOBS = 4;

	ToggleButton showDependentJobGroups;
	
	ToggleButton showDependentJobs;

	public DependentJobPane(HierarchyItemPanel hierarchyItemPanel, HierarchyLevel level) {

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/dependency_item_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		// this.getStyleClass().add("dependency-tab");

		predecessorJobsTable.getStyleClass().add("dependency-table");
		successorJobsTable.getStyleClass().add("dependency-table");

		// successorJobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		// predecessorJobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		predecessorDependentJobContextMenu = new DependentJobContextMenu(this, "P");
		successorDependentJobContextMenu = new DependentJobContextMenu(this, "S");
		successorJobsTable.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {

					if (successorDependentJobContextMenu.isShowing()) {
						successorDependentJobContextMenu.hide();
					}
					List<? extends HierarchyItem> jobs = getSelectedSuccessorItems();
					if (null != jobs && jobs.size() > 0) {
						successorDependentJobContextMenu.show(successorJobsTable, e.getScreenX(), e.getScreenY());
					}

				}
			}
		});

		predecessorJobsTable.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {
					if (predecessorDependentJobContextMenu.isShowing()) {
						predecessorDependentJobContextMenu.hide();
					}
					List<? extends HierarchyItem> jobs = getSelectedPredecessorItems();
					if (null != jobs && jobs.size() > 0) {
						predecessorDependentJobContextMenu.show(predecessorJobsTable, e.getScreenX(), e.getScreenY());
					}

				}
			}
		});

		toggleOptionBox.getChildren().remove(allItemsButton);
		toggleOptionBox.getChildren().remove(dependentButton);

		ToggleButton showAllJobGroups = new ToggleButton("");
		showAllJobGroups.setTooltip(new Tooltip("Show All Job Groups"));
		showAllJobGroups.setToggleGroup(group);
		showAllJobGroups.setPrefSize(25, 25);
		showAllJobGroups.getStyleClass().add("show-all-jobgroups-button");
		showAllJobGroups.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				clearTables();
				contextID = CONTEXT_JOB_GROUPS;
				loadPredecessorFirstPage();
				loadSuccessorFirstPage();
			}
		});

		ToggleButton showAllJobs = new ToggleButton("");
		showAllJobs.setTooltip(new Tooltip("Show All Jobs"));
		showAllJobs.setToggleGroup(group);
		showAllJobs.setPrefSize(25, 25);
		showAllJobs.getStyleClass().add("show-all-jobs-button");
		showAllJobs.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				clearTables();
				contextID = CONTEXT_JOBS;
				loadPredecessorFirstPage();
				loadSuccessorFirstPage();
			}
		});

		showDependentJobGroups = new ToggleButton("");
		showDependentJobGroups.setTooltip(new Tooltip("Show Dependent Job Groups"));
		showDependentJobGroups.setToggleGroup(group);
		showDependentJobGroups.setPrefSize(25, 25);
		showDependentJobGroups.getStyleClass().add("show-linked-job-groups-button");
		showDependentJobGroups.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				showDependedntJobGroups();
			}
		});

		showDependentJobs = new ToggleButton();
		showDependentJobs.setTooltip(new Tooltip("Show Dependent Jobs"));
		showDependentJobs.setToggleGroup(group);
		showDependentJobs.setPrefSize(25, 25);
		showDependentJobs.getStyleClass().add("show-linked-jobs-button");
		showDependentJobs.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				showDependedntJobs();
			}
		});

		toggleOptionBox.getChildren().addAll(showDependentJobGroups, showDependentJobs, showAllJobGroups, showAllJobs);

		showAllJobGroups.setTooltip(new Tooltip("Show All Job Groups"));
		showAllJobs.setTooltip(new Tooltip("Show All Jobs"));

	}
	
	private void showDependedntJobGroups(){
		clearTables();
		contextID = CONTEXT_LINKED_JOB_GROUPS;
		loadPredecessorFirstPage();
		loadSuccessorFirstPage();
	}
	
	private void showDependedntJobs(){
		clearTables();
		contextID = CONTEXT_LINKED_JOBS;
		loadPredecessorFirstPage();
		loadSuccessorFirstPage();
	}

	@Override
	public void loadDependentData() {
		contextID = CONTEXT_LINKED_JOB_GROUPS;
		clearTables();
		group.selectToggle(showDependentJobGroups);
		loadPredecessorFirstPage();
		loadSuccessorFirstPage();
	}

	@Override
	public void addAsSuccessor() {
		try {
			List<? extends HierarchyItem> jobs = getSelectedSuccessorItems();
			HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
			HierarchyItem jobItem = jobPanel.getSelectedItem();
			if (null != jobs && null != jobItem) {
				if (contextID == CONTEXT_JOBS) {
					if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
						DependencyJobService.addSuccessorJobs(jobItem.getId(), jobs, NodeCategory.jobgroup);
						group.selectToggle(showDependentJobGroups);
						showDependedntJobGroups();
					} else {
						DependencyJobService.addSuccessorJobs(jobItem.getId(), jobs, NodeCategory.job);
						group.selectToggle(showDependentJobs);
						showDependedntJobs();
					}
				} else {
					if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
						DependencyJobService.addSuccessorJobGroups(jobItem.getId(), jobs, NodeCategory.jobgroup);
						group.selectToggle(showDependentJobGroups);
						showDependedntJobGroups();
					} else {
						DependencyJobService.addSuccessorJobGroups(jobItem.getId(), jobs, NodeCategory.job);
						group.selectToggle(showDependentJobs);
						showDependedntJobs();
					}

				}
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Successor dependency added successfully");
				alertDialog.show();
			}else{
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, "Please select any item from Job to add dependency");
				alertDialog.show();
			}
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@Override
	public void addAsPredecessor() {
		try {
			List<? extends HierarchyItem> jobs = getSelectedPredecessorItems();
			HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
			HierarchyItem jobItem = jobPanel.getSelectedItem();
			if (null != jobs && null != jobItem) {
				if (contextID == CONTEXT_JOBS) {
					if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
						DependencyJobService.addPredecessorJobs(jobItem.getId(), jobs, NodeCategory.jobgroup);
						group.selectToggle(showDependentJobGroups);
						showDependedntJobGroups();
					} else {
						DependencyJobService.addPredecessorJobs(jobItem.getId(), jobs, NodeCategory.job);
						group.selectToggle(showDependentJobs);
						showDependedntJobs();
					}

				} else {
					if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
						DependencyJobService.addPredecessorJobGroups(jobItem.getId(), jobs, NodeCategory.jobgroup);
						group.selectToggle(showDependentJobGroups);
						showDependedntJobGroups();
					} else {
						DependencyJobService.addPredecessorJobGroups(jobItem.getId(), jobs, NodeCategory.job);
						group.selectToggle(showDependentJobs);
						showDependedntJobs();
					}

				}
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Predecessor dependency added successfully");
				alertDialog.show();
			}else{
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, "Please select any item from Job to add dependency");
				alertDialog.show();
			}
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
	}

	@Override
	public void removePredecessor() {
		List<? extends HierarchyItem> jobs = getSelectedPredecessorItems();
		HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
		HierarchyItem jobItem = jobPanel.getSelectedItem();
		try {
			if (null != jobs && null != jobItem) {
				if (contextID == CONTEXT_LINKED_JOB_GROUPS) {

					DependencyJobService.removePredecessorJobGroups(jobItem.getId(), jobs, jobPanel.getNodeType());

				} else {
					DependencyJobService.removePredecessorJobs(jobItem.getId(),jobs, jobPanel.getNodeType());
				}
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Predecessor dependency removed successfully");
				alertDialog.show();
			}
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void removeSuccessor() {
		List<? extends HierarchyItem> jobs = getSelectedSuccessorItems();
		HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
		HierarchyItem jobItem = jobPanel.getSelectedItem();
		try {
			if (null != jobs && null != jobItem) {
				if (contextID == CONTEXT_LINKED_JOB_GROUPS) {

					DependencyJobService.removeSuccessorJobGroups(jobItem.getId(), jobs, jobPanel.getNodeType());

				} else {
					DependencyJobService.removeSuccessorJobs(jobItem.getId(), jobs, jobPanel.getNodeType());
				}
				AlertDialog alertDialog = new AlertDialog(AlertDialogType.INFO, "Successor dependency removed successfully");
				alertDialog.show();
			}
		} catch (ServiceFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<? extends HierarchyItem> searchData(String context, String searchText) {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		List<? extends HierarchyItem> hierarchyNodes = new ArrayList<>();
		try {
			HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
			HierarchyItem jobItem = jobPanel.getSelectedItem();
			if (!Utilities.isNullOrEmpty(searchText)) {
				searchText = searchText + "%";
				NodeCategory category = null;
				if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
					category = NodeCategory.jobgroup;
				} else {
					category = NodeCategory.job;
				}
				if (contextID == CONTEXT_JOB_GROUPS) {
					JobGroupProperties jobGroupProperties = new JobGroupProperties();
					jobGroupProperties.setDescription(searchText);
					jobGroupProperties.setStandard(false);
					pageHierarchyNodes = HierarchyJobService.searchJobGroupWithProperties(jobGroupProperties, 0,
							pageSize);
					if ("P".equalsIgnoreCase(context)) {
						loadPredecessorTable(pageHierarchyNodes);
					} else {
						loadSuccessorTable(pageHierarchyNodes);
					}
				} else if (contextID == CONTEXT_JOBS) {
					JobProperties jobSearchOption = new JobProperties();
					jobSearchOption.setDescription(searchText);
					jobSearchOption.setStandard(false);
					pageHierarchyNodes = HierarchyJobService.searchJobWithProperties(jobSearchOption, 0, pageSize);
					if ("P".equalsIgnoreCase(context)) {
						loadPredecessorTable(pageHierarchyNodes);
					} else {
						loadSuccessorTable(pageHierarchyNodes);
					}
				} else if (contextID == CONTEXT_LINKED_JOB_GROUPS) {
					if (null != jobItem) {
						if ("P".equalsIgnoreCase(context)) {
							pageHierarchyNodes = DependencyJobService.searchPredecessorJobGroups(jobItem.getId(),
									category, searchText, 0, pageSize);
							loadPredecessorTable(pageHierarchyNodes);
						} else {
							pageHierarchyNodes = DependencyJobService.searchSuccessorJobGroups(jobItem.getId(),
									category, searchText, 0, pageSize);
							loadSuccessorTable(pageHierarchyNodes);
						}
					}

				} else if (contextID == CONTEXT_LINKED_JOBS) {
					if (null != jobItem) {
						if ("P".equalsIgnoreCase(context)) {
							pageHierarchyNodes = DependencyJobService.searchPredecessorJobs(jobItem.getId(), category,
									searchText, 0, pageSize);
							loadPredecessorTable(pageHierarchyNodes);
						} else {
							pageHierarchyNodes = DependencyJobService.searchSuccessorJobs(jobItem.getId(), category,
									searchText, 0, pageSize);
							loadSuccessorTable(pageHierarchyNodes);
						}
					}

				}
			} else if (Utilities.isNullOrEmpty(searchText)) {
				if ("P".equalsIgnoreCase(context)) {
					loadPredecessorFirstPage();
				} else {
					loadSuccessorFirstPage();
				}
			}

		} catch (ServiceFailedException ex) {
			ex.printStackTrace();
		}
		if (null != pageHierarchyNodes) {
			hierarchyNodes = pageHierarchyNodes.getContent();
		}
		return hierarchyNodes;

	}

	@Override
	public void loadPredecessorFirstPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (contextID == CONTEXT_JOB_GROUPS) {
			pageHierarchyNodes = getAllJobGroups(0);
		} else if (contextID == CONTEXT_JOBS) {
			pageHierarchyNodes = getAllJobs(0);
		} else {
			pageHierarchyNodes = getPredecessorItems(0);
		}
		loadPredecessorTable(pageHierarchyNodes);
	}

	@Override
	public void loadPredecessorPreviousPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentPredecessorPage) {
			if ((currentPredecessorPage.getCurrentPage()) > 0) {
				int pageNumber = currentPredecessorPage.getCurrentPage() - 1;
				if (contextID == CONTEXT_JOB_GROUPS) {
					pageHierarchyNodes = getAllJobGroups(pageNumber);
				} else if (contextID == CONTEXT_JOBS) {
					pageHierarchyNodes = getAllJobs(pageNumber);
				} else {
					pageHierarchyNodes = getPredecessorItems(pageNumber);
				}
				loadPredecessorTable(pageHierarchyNodes);
			}
		}
	}

	@Override
	public void loadPredecessorNextPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentPredecessorPage) {
			if ((currentPredecessorPage.getCurrentPage() + 1) < currentPredecessorPage.getTotalPages()) {
				int pageNumber = currentPredecessorPage.getCurrentPage() + 1;
				if (contextID == CONTEXT_JOB_GROUPS) {
					pageHierarchyNodes = getAllJobGroups(pageNumber);
				} else if (contextID == CONTEXT_JOBS) {
					pageHierarchyNodes = getAllJobs(pageNumber);
				} else {
					pageHierarchyNodes = getPredecessorItems(pageNumber);
				}
				loadPredecessorTable(pageHierarchyNodes);
			}
		}

	}

	@Override
	public void loadPredecessorLastPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentPredecessorPage) {
			int pageNumber = currentPredecessorPage.getTotalPages() - 1;
			if (contextID == CONTEXT_JOB_GROUPS) {
				pageHierarchyNodes = getAllJobGroups(pageNumber);
			} else if (contextID == CONTEXT_JOBS) {
				pageHierarchyNodes = getAllJobs(pageNumber);
			} else {
				pageHierarchyNodes = getPredecessorItems(pageNumber);
			}
			loadPredecessorTable(pageHierarchyNodes);

		}

	}

	@Override
	public void loadSuccessorFirstPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (contextID == CONTEXT_JOB_GROUPS) {
			pageHierarchyNodes = getAllJobGroups(0);
		} else if (contextID == CONTEXT_JOBS) {
			pageHierarchyNodes = getAllJobs(0);
		} else {
			pageHierarchyNodes = getSuccessorItems(0);
		}
		loadSuccessorTable(pageHierarchyNodes);
	}

	@Override
	public void loadSuccessorPreviousPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentSuccessorPage) {
			if ((currentSuccessorPage.getCurrentPage()) > 0) {
				int pageNumber = currentSuccessorPage.getCurrentPage() - 1;
				if (contextID == CONTEXT_JOB_GROUPS) {
					pageHierarchyNodes = getAllJobGroups(pageNumber);
				} else if (contextID == CONTEXT_JOBS) {
					pageHierarchyNodes = getAllJobs(pageNumber);
				} else {
					pageHierarchyNodes = getSuccessorItems(pageNumber);
				}
				loadSuccessorTable(pageHierarchyNodes);
			}
		}

	}

	@Override
	public void loadSuccessorNextPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentSuccessorPage) {
			if ((currentSuccessorPage.getCurrentPage() + 1) < currentSuccessorPage.getTotalPages()) {
				int pageNumber = currentSuccessorPage.getCurrentPage() + 1;
				if (contextID == CONTEXT_JOB_GROUPS) {
					pageHierarchyNodes = getAllJobGroups(pageNumber);
				} else if (contextID == CONTEXT_JOBS) {
					pageHierarchyNodes = getAllJobs(pageNumber);
				} else {
					pageHierarchyNodes = getSuccessorItems(pageNumber);
				}
				loadSuccessorTable(pageHierarchyNodes);
			}
		}

	}

	@Override
	public void loadSuccessorLastPage() {
		PageItr<? extends HierarchyItem> pageHierarchyNodes = null;
		if (null != currentSuccessorPage) {
			int pageNumber = currentSuccessorPage.getTotalPages() - 1;
			if (contextID == CONTEXT_JOB_GROUPS) {
				pageHierarchyNodes = getAllJobGroups(pageNumber);
			} else if (contextID == CONTEXT_JOBS) {
				pageHierarchyNodes = getAllJobs(pageNumber);
			} else {
				pageHierarchyNodes = getSuccessorItems(pageNumber);
			}
			loadSuccessorTable(pageHierarchyNodes);
		}

	}

	private PageItr<? extends HierarchyItem> getPredecessorItems(int pageNumber) {
		PageItr<? extends HierarchyItem> predecessorNodesPage = null;
		HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
		HierarchyItem jobItem = jobPanel.getSelectedItem();
		if (null != jobItem) {
			if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
				if (contextID == CONTEXT_LINKED_JOB_GROUPS) {
					try {
						predecessorNodesPage = DependencyJobService.getPredecessorJobGroups(jobItem.getId(),
								NodeCategory.jobgroup, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				} else {
					try {
						predecessorNodesPage = DependencyJobService.getPredecessorJobs(jobItem.getId(),
								NodeCategory.jobgroup, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				}

			} else {
				if (contextID == CONTEXT_LINKED_JOB_GROUPS) {
					try {
						predecessorNodesPage = DependencyJobService.getPredecessorJobGroups(jobItem.getId(),
								NodeCategory.job, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				} else {
					try {
						predecessorNodesPage = DependencyJobService.getPredecessorJobs(jobItem.getId(),
								NodeCategory.job, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				}

			}
		}

		return predecessorNodesPage;
	}

	private PageItr<? extends HierarchyItem> getSuccessorItems(int pageNumber) {
		PageItr<? extends HierarchyItem> predecessorNodesPage = null;
		HierarchyJobPanel jobPanel = ClientConfig.getInstance().getHierarchyJobPanel();
		HierarchyItem jobItem = jobPanel.getSelectedItem();
		if (null != jobItem) {
			if (jobPanel.getContextID() == 1 || jobPanel.getContextID() == 3) {
				if (contextID == CONTEXT_LINKED_JOB_GROUPS) {
					try {
						predecessorNodesPage = DependencyJobService.getSuccessorJobGroups(jobItem.getId(),
								NodeCategory.jobgroup, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				} else {
					try {
						predecessorNodesPage = DependencyJobService.getSuccessorJobs(jobItem.getId(),
								NodeCategory.jobgroup, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				}

			} else {
				if (contextID == CONTEXT_LINKED_JOB_GROUPS) {
					try {
						predecessorNodesPage = DependencyJobService.getSuccessorJobGroups(jobItem.getId(),
								NodeCategory.job, pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				} else {
					try {
						predecessorNodesPage = DependencyJobService.getSuccessorJobs(jobItem.getId(), NodeCategory.job,
								pageNumber, pageSize);
					} catch (ServiceFailedException ex) {
						ex.printStackTrace();
					}
				}

			}
		}

		return predecessorNodesPage;
	}

	private PageItr<? extends HierarchyItem> getAllJobGroups(int pageNumber) {
		PageItr<? extends HierarchyItem> pageJobGroups = null;
		try {
			pageJobGroups = HierarchyJobService.getAllJobGroups(pageNumber, pageSize);
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		return pageJobGroups;
	}

	private PageItr<? extends HierarchyItem> getAllJobs(int pageNumber) {
		PageItr<? extends HierarchyItem> pageJobs = null;
		try {
			pageJobs = HierarchyJobService.getAllJobs(pageNumber, pageSize);
		} catch (ServiceFailedException ex) {
			Message errorMessage = ex.getErroMessage();
			AlertDialog alertDialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			alertDialog.show();
		}
		return pageJobs;
	}

}
