/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author hp
 */
public class PlantHierarchySearchPane extends TitledPane{

    public PlantHierarchySearchPane() {
        this.setCollapsible(true);
        this.setAnimated(true);
        this.setText("Search");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/plant_hierarchy_search.fxml"));
         
        BorderPane root = null;
        try {
            root = loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.setContent(root);
        this.getStyleClass().add("common-search-pane");
         
    }
    
    
}
