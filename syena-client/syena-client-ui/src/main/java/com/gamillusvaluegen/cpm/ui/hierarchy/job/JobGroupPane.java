package com.gamillusvaluegen.cpm.ui.hierarchy.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gamillusvaluegen.cpm.DialogController;
import com.gamillusvaluegen.cpm.FXMLDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialog;
import com.gamillusvaluegen.cpm.ui.alert.AlertDialogType;
import com.gamillusvaluegen.cpm.ui.alert.exception.ServiceFailedException;
import com.gamillusvaluegen.cpm.ui.server.HierarchyJobService;
import com.gamillusvaluegen.cpm.ui.server.HierarchyNodeService;
import com.gamillusvaluegen.cpm.ui.util.Utilities;
import com.gvg.syena.core.api.NodeCategory;
import com.gvg.syena.core.api.entity.common.HierarchyItem;
import com.gvg.syena.core.api.entity.common.NodeType;
import com.gvg.syena.core.api.entity.hierarchy.HierarchyLevel;
import com.gvg.syena.core.api.entity.job.Job;
import com.gvg.syena.core.api.entity.job.JobGroup;
import com.gvg.syena.core.api.entity.job.StandardJob;
import com.gvg.syena.core.api.entity.util.Message;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

public class JobGroupPane extends BorderPane implements DialogController{
	
	@FXML
	private TabPane tabPane;
	
	private FXMLDialog dialog;
	
	private static final String SEARCH_JOB_TAB= "search_tab";
	private static final String JOB_TAB= "job_tab";
	
	private JobSearchPane searchPane;
	private JobAttributePane<StandardJob> jobAttributePane;
	private JobAttributePane<JobGroup> jobGroupAttributePane;
	
	@FXML
	private ListView<StandardJob> jobList;
	
	private JobGroup data;
	
	@FXML
	private Label titleLabel;
	
	@FXML
	private VBox topBox;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button submitButton;
	
	private HierarchyItem parentItem;
	
	String mode = "E";
	
	public JobGroupPane(HierarchyItem parentItem,JobGroup jobGroup,String mode){
		this.mode = mode;
		this.parentItem = parentItem;
		this.data = jobGroup;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/job/job_group_pane.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		jobGroupAttributePane = new JobAttributePane(jobGroup,NodeCategory.jobgroup,mode);
		topBox.getChildren().add(jobGroupAttributePane);
		
		searchPane = new JobSearchPane();
		Tab searchTab = new Tab("Search Job");		
		searchTab.setId(SEARCH_JOB_TAB);
		searchTab.setContent(searchPane);
		
		Tab jobTab = new Tab("Job");
		jobAttributePane = new JobAttributePane(null,NodeCategory.job,mode);
		jobTab.setContent(jobAttributePane);
		jobTab.setId(JOB_TAB);
		if("E".equalsIgnoreCase(mode)){
			
			tabPane.getTabs().addAll(jobTab,searchTab);
		}else{
			removeButton.setVisible(false);
			addButton.setVisible(false);
			tabPane.getTabs().addAll(jobTab);
			submitButton.setVisible(false);
		}
	
		
		if(null != jobGroup){	
			if(jobGroup.getNodeType() == NodeType.standard){
				Set<StandardJob> jobs = null;
				try {
				jobs = HierarchyJobService.getStandardJobInJobGroup(jobGroup.getId());
				} catch (ServiceFailedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				addToJobList(jobs);
			}else{
				Set<Job> jobs = null;
				try {
					jobs = HierarchyJobService.getJobsInJobGroup(jobGroup.getId());
				} catch (ServiceFailedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				addToJobList(jobs);
			}
			
		}
		if("V".equalsIgnoreCase(mode)){
//			jobList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Job>() {
//				@Override
//				public void changed(ObservableValue<? extends Job> arg0, Job oldValue, Job newValue) {
//					if(null != newValue){
//						jobAttributePane.clear();
//						jobAttributePane.setItem(newValue);
//					}
//					
//				}
//			});
			
			jobList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StandardJob>() {
				@Override
				public void changed(ObservableValue<? extends StandardJob> arg0, StandardJob oldValue, StandardJob newValue) {
					if(null != newValue){
						jobAttributePane.clear();
						jobAttributePane.setItem(newValue);
					}
					
				}
			});
			
			
		}
//		
		
		jobList.setCellFactory(new Callback<ListView<StandardJob>, ListCell<StandardJob>>() {
			@Override
			public ListCell<StandardJob> call(ListView<StandardJob> param) {
				ListCell<StandardJob> cell = new ListCell<StandardJob>() {
					 @Override
	                    protected void updateItem(StandardJob item, boolean empty) {
						 super.updateItem(item, empty);
							if (null != getListView().getItems() && getListView().getItems().size() > 0) {
								if (getIndex() >= 0 && getIndex() < getListView().getItems().size()) {
									StandardJob job = getListView().getItems().get(getIndex());
									if (null != job) {
										if (Utilities.isNullOrEmpty(job.getDescription())) {
											setText(job.getName());
										} else {
											setText(job.getDescription());
										}
									} else {

										setText(null);
										setGraphic(null);
									}
								} else {

									setText(null);
									setGraphic(null);
								}
							} else {

								setText(null);
								setGraphic(null);
							}

					 }
				  };
	                return cell;
	            }
	        });
		
		if(null != parentItem){
		
			if(null != data){
				if(data.isInPunchList()){
					titleLabel.setText("PunchList Job Group for " + parentItem.getName());
				}else{

					titleLabel.setText("Job Group");
				}
			}else{
				titleLabel.setText("PunchList Job Group for " + parentItem.getName());
			}
		}else{
			
				titleLabel.setText("Standard Job Group");
			
		}
		
		
	}
	
	public JobGroupPane(HierarchyItem parentItem,JobGroup jobGroup){
		this(parentItem,jobGroup,"E");
	}

	@Override
	public void setDialog(FXMLDialog dialog) {
		this.dialog = dialog;		
	}
	
	@FXML
	private void submitData(){
		JobGroup jobGroup = (JobGroup) jobGroupAttributePane.getItem();
		if(null != jobGroup.getJobs()){
			jobGroup.getJobs().clear();
		}
		Set<StandardJob> jobSet = new HashSet<>();
		jobSet.addAll(jobList.getItems());
		jobGroup.setJobs(jobSet);
		try {
			if(null != data){
				if(data.getNodeType() == NodeType.standard){
					HierarchyJobService.updateStandardJobGroup(jobGroup);
				}else{
					HierarchyJobService.updateJobGroup(jobGroup);
				}
			}else{
				if(null == parentItem){
					HierarchyJobService.createStandardJobGroup(jobGroup);
				}else{
					HierarchyNodeService.createPuchListJobGroup(parentItem.getId(), jobGroup);
				}
			}			
			dialog.close();
		} catch (ServiceFailedException e) {
			Message errorMessage = e.getErroMessage();
			AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, errorMessage.getMessage());
			dialog.show();
		}
	}
	
	@FXML
	private void closeDialog(){
		this.dialog.close();
	}
	
	@FXML
	private void onAddJob(){
		List<StandardJob> jobs = null;
		if (tabPane.getSelectionModel().getSelectedItem().getId().equalsIgnoreCase(SEARCH_JOB_TAB)){
			jobs = new ArrayList<>();
			
				
			
			for(StandardJob standardJob : searchPane.getSelectedJobs()){
				if(parentItem != null){
					jobs.add(new Job(standardJob, standardJob.getDescription()));
				}else{
					jobs.add(standardJob);
				}
			}
//			jobs = searchPane.getSelectedJobs();
			
		}else{
			Object obj = jobAttributePane.getItem();
			if(null != obj){
				Job job = (Job) obj;
				jobs = new ArrayList<StandardJob>();
				jobs.add(job);
				
			}
			
		}		
		if(null != jobs && jobs.size() > 0 ){
			if(!verifyDuplicateJob(jobs)){
				addToJobList(jobs);
				jobAttributePane.clear();
			}else{
				AlertDialog dialog = new AlertDialog(AlertDialogType.ERROR, "Duplicate Job cannot be added");
				dialog.show();
			}
			
		}
	}
	
	@FXML
	private void onRemoveJob(){
		if(null != jobList.getSelectionModel().getSelectedItems()){
			jobList.getItems().removeAll(jobList.getSelectionModel().getSelectedItems());
		}
	}
	
	private boolean verifyDuplicateJob(List<StandardJob> jobs){
		boolean duplicate = false;
		if(null != jobList.getItems()){
			for(StandardJob newJob : jobs){
				for(StandardJob esistingJob : jobList.getItems()){
					if(esistingJob.getId() == newJob.getId() && esistingJob.getName().equalsIgnoreCase(newJob.getName())){
						duplicate = true;
						break;
					}
				}
				if(duplicate){
					break;
				}
			}
		}
		return duplicate;
	}
	
	private void addToJobList(Collection<? extends StandardJob> jobs){
		if(null != jobs && jobs.size() > 0){
			if(null == jobList.getItems()){
				ObservableList<StandardJob> items = FXCollections.observableArrayList(jobs);
				jobList.setItems(items);
			}else{
				jobList.getItems().addAll(jobs);
			}
		}
		
	}
	

	
}
