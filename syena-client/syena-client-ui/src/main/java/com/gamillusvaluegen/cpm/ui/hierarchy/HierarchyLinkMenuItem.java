/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamillusvaluegen.cpm.ui.hierarchy;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

import com.gvg.syena.core.api.entity.common.HierarchyItem;

/**
 *
 * @author hp
 */
public class HierarchyLinkMenuItem extends MenuItem{
    
    private HierarchyItemPanel pareItemPanel;
    private HierarchyItem parentItem;
    public HierarchyLinkMenuItem(final HierarchyItemPanel parentPanel,final HierarchyItem parentItem){
        this.pareItemPanel = parentPanel;
        this.parentItem = parentItem;
        this.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               parentPanel.linkToParent(parentItem);
            }
        });
    }
    
}
