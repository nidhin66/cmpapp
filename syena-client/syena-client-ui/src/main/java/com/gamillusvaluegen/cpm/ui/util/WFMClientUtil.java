package com.gamillusvaluegen.cpm.ui.util;

import com.gamillusvaluegen.cpm.ClientConfig;

public class WFMClientUtil {
	
	 public static final String APPROVAL_FLOW_REQUIRED = "APPROVAL_FLOW_REQUIRED";
	 
	 public static boolean approvalFlowRequired(){
		 String propertyValue = ClientConfig.getInstance().getApplicationConfig(APPROVAL_FLOW_REQUIRED);
		 boolean approvalRequired = true;
		 if(Utilities.isNullOrEmpty(propertyValue)){
			 approvalRequired = true;
		 }else {
			 if("Y".equalsIgnoreCase(propertyValue)){
				 approvalRequired = true;
			 }else{
				 approvalRequired = false;

			 }
		 }
		return approvalRequired;
	 }

}
